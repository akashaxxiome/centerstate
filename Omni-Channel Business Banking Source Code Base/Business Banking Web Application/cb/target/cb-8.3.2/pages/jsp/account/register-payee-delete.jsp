<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/web'/>/js/account/accountRegister_grid.js" type="text/javascript"></script>
<%
   session.setAttribute("SetRegisterPayeeId", request.getParameter("SetRegisterPayeeId"));
%>
<ffi:object id="SetRegisterPayee" name="com.ffusion.tasks.register.SetRegisterPayee" scope="session"/>
	<ffi:setProperty name="SetRegisterPayee" property="Id" value="${SetRegisterPayeeId}" />
<ffi:process name="SetRegisterPayee"/>

<div align=center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="11"></td>
				<td class="sectionhead_grey"></td>
				<td width="11"></td>
			</tr>
			<tr>
				<td width="11"><br><br></td>
				<td align="center" class="sectionhead_grey">
				<s:form id="deletePayeeForm" namespace="/pages/jsp/account" action="DeleteRegisterPayeeAction" method="post" name="deletePayeeForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<table width="96%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="sectionhead tbrd_b" colspan="2" height="20">&gt; <s:text name="jsp.default_166"/></td>
						</tr>
						<tr>
							<td class="columndata" colspan="2" height="20"><s:text name="jsp.account_240" /></td>
						</tr>
						<tr>
							<td class="sectionsubhead" width="20%" align="right" height="20"><s:text name="jsp.default_313"/> </td>
							<td class="columndata" width="80%"><ffi:getProperty name="RegisterPayee" property="Name"/></td>
						</tr>
						<tr>
							<td class="sectionsubhead" align="right" height="20"><s:text name="jsp.account_45"/></td>
							<ffi:setProperty name="RegisterCategories" property="Current" value="${RegisterPayee.DefaultCategory}" />
		
							<td class="columndata"><ffi:getProperty name="RegisterCategories" property="ParentName"/><ffi:getProperty name="Parent${RegisterCategories.HasParent}"/><ffi:getProperty name="RegisterCategories" property="Name"/></td>
						</tr>
						<tr>
							<td class="columndata" colspan="2" align="center" height="20">
							      <sj:a id="cancelDeletePayeeFormButton" 
											    button="true" 
												onClickTopics="closeDialog"
											    ><s:text name="jsp.default_82"/></sj:a>
								  &nbsp;&nbsp;
								  <sj:a id="deletePayeeButton" 
								        formIds="deletePayeeForm" 
								        targets="deletePayeeresultmessage" 
								        button="true"   
										onSuccessTopics="deletePayeeSuccessTopicss" 
										effectDuration="1500"
										><s:text name="jsp.default_162"/></sj:a>
						</tr>
					</table>
					</s:form>
				</td>
				<td align="right" width="11"><br><br></td>
			</tr>
			<tr>
				<td width="11"></td>
				<td class="sectionhead_grey"></td>
				<td width="11"></td>
			</tr>
		</table>
</div>
<ffi:object id="DeleteRegisterPayee" name="com.ffusion.tasks.register.DeleteRegisterPayee" scope="session"/>
	<ffi:setProperty name="DeleteRegisterPayee" property="Id" value="${RegisterPayee.Id}" />
	<%
		session.setAttribute("FFIDeleteRegisterPayee", session.getAttribute("DeleteRegisterPayee"));
	%>