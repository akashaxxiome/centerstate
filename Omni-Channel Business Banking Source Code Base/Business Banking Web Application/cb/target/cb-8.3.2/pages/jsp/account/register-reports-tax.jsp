<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/web/js/account/account%{#session.minVersion}.js'/>"></script>
<DIV align="center">
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
  <TBODY>
  
  <TR>
    <TD class="ui-widget ui-widget-content ui-corner-all">
      <TABLE class="ui-widget ui-widget-content ui-corner-all" cellSpacing="0" cellPadding="5" width="100%" border="0">
        <TBODY>
        <TR>
          <TD class="sectionsubhead" colSpan="2" align="left"><s:text name="jsp.default_408"/></TD>
          <TD class="sectionsubhead" align="right"><ffi:getProperty name="Business" property="BusinessName"/></TD>
        </TR>
        <TR>
          <TD class="sectionsubhead" colSpan="3" height="15"></TD>
        </TR>
        <TR>
		  <ffi:cinclude value1="${GetRegisterTransactions.FromDate}" value2="" operator="equals">
		  	<s:set var="tmpI18nStr" value="%{getText('jsp.account_149')}" scope="request" /><ffi:setProperty name="FromDate" value="${tmpI18nStr}"/>
		  </ffi:cinclude>
		  <ffi:cinclude value1="${GetRegisterTransactions.FromDate}" value2="" operator="notEquals">
		  	<ffi:setProperty name="FromDate" value="${GetRegisterTransactions.FromDate}"/>
		  </ffi:cinclude>
		  <ffi:cinclude value1="${GetRegisterTransactions.ToDate}" value2="" operator="equals">
		  	<s:set var="tmpI18nStr" value="%{getText('jsp.account_147')}" scope="request" /><ffi:setProperty name="ToDate" value="${tmpI18nStr}"/>
		  </ffi:cinclude>
		  <ffi:cinclude value1="${GetRegisterTransactions.ToDate}" value2="" operator="notEquals">
		  	<ffi:setProperty name="ToDate" value="${GetRegisterTransactions.ToDate}"/>
		  </ffi:cinclude>
		  <TD class="sectionsubhead" colSpan="3" align="left"><s:text name="jsp.account_180"/>&nbsp;&nbsp;
		    <ffi:getProperty name="Account" property="DisplayText"/>
		    <ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
		     - <ffi:getProperty name="Account" property="NickName"/>
		    </ffi:cinclude>
		     - <ffi:getProperty name="Account" property="CurrencyCode"/>&nbsp;&nbsp;(<ffi:getProperty name="FromDate"/> - <ffi:getProperty name="ToDate"/>)
		  </TD>
		  <ffi:removeProperty name="FromDate"/>
		  <ffi:removeProperty name="ToDate"/>
        </TR>
        <TR>
          <TD class="tbrd_b" colSpan="3"><P class="sectionsubhead" align="left"><s:text name="jsp.account_182"/> &nbsp;</P></TD>
        </TR>
        <TR>
          <TD class="tbrd_b" colSpan="3">
            <TABLE cellSpacing="0" cellPadding="3" border="0" width="100%" >
              <TBODY>

<%-- INCOME SECTION --%>
              <TR valign="center" align="left">
                <TD vAlign="top" align="right" colSpan="7">
                  <DIV align="left"><STRONG class="sectionsubhead"><u><s:text name="jsp.account_106"/></u></DIV>
				 </TD>
			  </TR>
              <TR valign="center" align="left">
                <TD class="sectionsubhead" vAlign="top" width="135" nowrap><s:text name="jsp.account_52"/></TD>
                <TD class="sectionsubhead" vAlign="top" width="90" nowrap><s:text name="jsp.account_67"/></TD>
                <TD class="sectionsubhead" vAlign="top" width="90" nowrap><s:text name="jsp.account_65"/></TD>
                <TD class="sectionsubhead" vAlign="top" width="190" nowrap><s:text name="jsp.account_163"/></TD>
                <TD class="sectionsubhead" vAlign="top" width="60" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.default_444"/></TD>
                <TD class="sectionsubhead" vAlign="top" width="50" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.account_177"/></TD>
                <TD class="sectionsubhead" vAlign="top" width="75" align="right" nowrap><s:text name="jsp.default_169"/></TD>
			  </TR>

<ffi:object name="com.ffusion.beans.util.FloatMath" id="adder1"/>
	<ffi:setProperty name="adder1" property="Value1" value="0.00"/>
	<ffi:setProperty name="adder1" property="Value2" value="0.00"/>

<ffi:object name="com.ffusion.beans.common.Currency" id="temp"/>

<ffi:setProperty name="RegisterReport" property="Filter" value="TYPE=INCOME,TAX=true,and"/>
<ffi:setProperty name="RegisterReport" property="FilterSortedBy" value="CATEGORY_NAME"/>
<ffi:list collection="RegisterReport" items="Cat">
              <TR valign="center" align="left">
                <TD class=plainsubhead vAlign="top" colspan="7"><ffi:getProperty name="Cat" property="FullName"/></TD>
		      </TR>
<ffi:list collection="Cat" items="Item">
		      <TR valign="center" align="left">
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="FullName"/></TD>
                <ffi:setProperty name="Item" property="DateFormat" value="${UserLocale.DateFormat}"/>
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="DateIssued"/></TD>
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="Date"/></TD>
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="PayeeName"/></TD>
                <TD class=columndata vAlign="top" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="Item" property="RegisterTypeName"/></TD>
                <TD class=columndata vAlign="top" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="Item" property="ReferenceNumber"/></TD>
                <TD class=columndata vAlign="top" align="right"><ffi:getProperty name="Item" property="AmountValue.CurrencyStringNoSymbol"/></TD>
		      </TR>
</ffi:list>
			  <TR valign="center" align="left">
                <TD class=columndata vAlign="top" align="right" colspan=6><ffi:getProperty name="Cat" property="FullName"/> <s:text name="jsp.default_431"/></TD>
				<TD class=columndata align="right"><ffi:getProperty name="Cat" property="AmountValue"/></TD>
		      </TR>

<%-- <!--We set the item format here, we are dropping the dollar sign and commas, so we can do math on the balances	-->--%>
<ffi:setProperty name="temp" property="Format" value="CURRENCY"/>
<ffi:setProperty name="temp" property="Amount" value="${Cat.AmountValue.AmountValue}"/>
<ffi:setProperty name="temp" property="CurrencyCode" value="${Account.CurrencyCode}"/>
<ffi:setProperty name="temp" property="Format" value="#.00"/>

<%-- <!--Here we are getting the Account balances and adding them to each other to get the adder1 total-->--%>
<ffi:setProperty name="adder1" property="Value1" value="${temp.AmountValue.AmountValue}"/>
<ffi:setProperty name="adder1" property="Value2" value="${adder1.Add}"/>

</ffi:list>
			  <TR valign="center" align="left">
                <TD class="sectionsubhead" vAlign="top" align="right" colspan=6 nowrap><u><s:text name="jsp.account_108"/> (<ffi:getProperty name="Account" property="CurrencyCode"/>)</u></TD>
                <TD class="sectionsubhead" vAlign="top" align="right">
                    <ffi:object name="com.ffusion.beans.common.Currency" id="result"/>
					<ffi:setProperty name="result" property="Amount" value="${adder1.Value2}"/>
					<ffi:setProperty name="result" property="CurrencyCode" value="${Account.CurrencyCode}"/>
					<ffi:getProperty name="result" property="String"/>
                </TD>
		      </TR>
		      <TR valign="center" align="left">
                <TD vAlign="top" align="right" colSpan="7" height="20"></TD>
			  </TR>

<%-- EXPENSE SECTION --%>
			  <TR valign="center" align="left">
                <TD vAlign="top" align="right" colSpan="7">
                  <DIV align="left"><STRONG class="sectionsubhead"><u><s:text name="jsp.account_84"/></u></DIV>
				 </TD>
			  </TR>
              <TR valign="center" align="left">
                <TD class="sectionsubhead" vAlign="top" nowrap><s:text name="jsp.account_52"/></TD>
                <TD class="sectionsubhead" vAlign="top" nowrap><s:text name="jsp.account_67"/></TD>
                <TD class="sectionsubhead" vAlign="top" nowrap><s:text name="jsp.account_65"/></TD>
                <TD class="sectionsubhead" vAlign="top" nowrap><s:text name="jsp.account_159"/></TD>
                <TD class="sectionsubhead" vAlign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.default_444"/></TD>
                <TD class="sectionsubhead" vAlign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.account_177"/></TD>
                <TD class="sectionsubhead" vAlign="top" align="right" nowrap><s:text name="jsp.default_169"/></TD>
			  </TR>

<ffi:object name="com.ffusion.beans.util.FloatMath" id="adder2"/>
	<ffi:setProperty name="adder2" property="Value1" value="0.00"/>
	<ffi:setProperty name="adder2" property="Value2" value="0.00"/>

<ffi:object name="com.ffusion.beans.common.Currency" id="temp2"/>

<ffi:setProperty name="RegisterReport" property="Filter" value="TYPE=EXPENSE,TAX=true,and"/>
<ffi:setProperty name="RegisterReport" property="FilterSortedBy" value="CATEGORY_NAME"/>
<ffi:list collection="RegisterReport" items="Cat">
              <TR valign="center" align="left">
                <TD class=plainsubhead vAlign="top" colspan=7><ffi:getProperty name="Cat" property="FullName"/></TD>
		      </TR>
<ffi:list collection="Cat" items="Item">
		      <TR valign="center" align="left">
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="FullName"/></TD>
               	<ffi:setProperty name="Item" property="DateFormat" value="${UserLocale.DateFormat}"/>
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="DateIssued"/></TD>
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="Date"/></TD>
                <TD class=columndata vAlign="top" align="left"><ffi:getProperty name="Item" property="PayeeName"/></TD>
                <TD class=columndata vAlign="top" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="Item" property="RegisterTypeName"/></TD>
                <TD class=columndata vAlign="top" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="Item" property="ReferenceNumber"/></TD>
                <TD class=columndata vAlign="top" align="right"><ffi:getProperty name="Item" property="AmountValue.CurrencyStringNoSymbol"/></TD>
		      </TR>
</ffi:list>
			  <TR valign="center" align="left">
                <TD class=columndata vAlign="top" align="right" colspan=6><ffi:getProperty name="Cat" property="FullName"/> <s:text name="jsp.default_431"/></TD>
				<TD class=columndata align="right"><ffi:getProperty name="Cat" property="AmountValue"/></TD>
		      </TR>

<%-- <!--We set the item format here, we are dropping the dollar sign and commas, so we can do math on the balances	-->--%>
<ffi:setProperty name="temp2" property="Format" value="CURRENCY"/>
<ffi:setProperty name="temp2" property="Amount" value="${Cat.AmountValue.AmountValue}"/>
<ffi:setProperty name="temp2" property="CurrencyCode" value="${Account.CurrencyCode}"/>
<ffi:setProperty name="temp2" property="Format" value="#.00"/>

<%-- <!--Here we are getting the Account balances and adding them to each other to get the adder1 total-->--%>
<ffi:setProperty name="adder2" property="Value1" value="${temp2.AmountValue.AmountValue}"/>
<ffi:setProperty name="adder2" property="Value2" value="${adder2.Add}"/>

</ffi:list>
			  <TR valign="center" align="left">
                <TD class="sectionsubhead" vAlign="top" align="right" colspan="6" nowrap><u><s:text name="jsp.account_85"/> (<ffi:getProperty name="Account" property="CurrencyCode"/>)</u></TD>
                <TD class="sectionsubhead" vAlign="top" align="right">
                    <ffi:object name="com.ffusion.beans.common.Currency" id="result"/>
					<ffi:setProperty name="result" property="Amount" value="${adder2.Value2}"/>
					<ffi:setProperty name="result" property="CurrencyCode" value="${Account.CurrencyCode}"/>
					<ffi:getProperty name="result" property="String"/>
                </TD>
		      </TR>
          </TBODY>
        </TABLE>
      </TD></TR>
	  <tr>
		<td colspan="3" align="center">
			<script>
				ns.account.backReportButtonURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-reports.jsp'/>";
			</script>
			<sj:a 
				id="canceltaxReportdown"
				button="true" 
				onclick="ns.account.backReportButton(ns.account.backReportButtonURL)"
				><s:text name="jsp.default_57"/></sj:a>
		&nbsp;&nbsp;
		</td>
	  </tr>
	  </TBODY></TABLE>
     </TD>
  </TR>
  <TR>
    <TD align="left" class="ui-widget-header ui-corner-all" width="100%" height="10"></TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
