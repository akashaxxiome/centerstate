<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="" value2="${GetTransactionSearchSettings}" operator="equals">
	<ffi:object name="com.ffusion.tasks.user.GetTransactionSearchSettings" id="GetTransactionSearchSettings" scope="session"/>
	<ffi:process name="GetTransactionSearchSettings"/>
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="PageSize" value="${TRANSACTION_SEARCH_MAXIMUM_MATCHES}"/>
<ffi:setProperty name="GetPagedTransactions" property="Page" value="<%=com.ffusion.tasks.banking.GetPagedTransactions.PAGE_FIRST%>"/>

	<%-- set the extra search criteria --%>
	<%	String[] values = request.getParameterValues("TSTransactionType");
		StringBuffer comma = new StringBuffer();
		if (values != null) {
			for(int i=0; i<values.length; i++) {
				if (i != 0) {
					comma.append(",");
				}
				String tranTy = null;  %>
		    <ffi:setProperty name="tempVar" value="<%= values[i] %>"/>
	 		  <ffi:getProperty name="tempVar" assignTo="tranTy"/>
	<%
				comma.append( tranTy );
			}
			session.setAttribute("TSTransactionType", comma.toString());
		}
	%>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_GROUPS%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSTransactionType}"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSZBADisplay}"/>

	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSReferenceStart}"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSReferenceEnd}"/>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSTransactionType}"/>
	
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MIN%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSReferenceStart}"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MAX%>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSReferenceEnd}"/>
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN%>"/>
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSMinimumAmount}"/>
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX%>"/>
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSMaximumAmount}"/>
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION%>"/>
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSDescription}"/>
