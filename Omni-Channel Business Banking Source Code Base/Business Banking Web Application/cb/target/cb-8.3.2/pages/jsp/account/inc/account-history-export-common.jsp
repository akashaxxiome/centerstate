<%@page import="com.ffusion.beans.user.UserLocale"%>
<%@page import="com.ffusion.util.UserLocaleConsts"%>
<%@page import="com.ffusion.util.DateFormatUtil"%>
<%@page import="java.text.Format"%>
<%@page import="com.ffusion.tasks.util.GetDatesFromDateRange"%>
<%@page import="com.ffusion.beans.accounts.GenericBankingRptConsts"%>
<%@page import="com.ffusion.filters.DecryptionFilter"%>
<%@page import="com.ffusion.jtf.encryption.interfaces.UrlEncryptor"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.reporting.ExportFormats"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.accountconfig.constants.AccountConstants"%>
<ffi:author page="account-history-export.jsp"/>
<ffi:setProperty name="topMenu" value="accounts"/>
<ffi:setProperty name="subMenu" value="history"/>
<ffi:setProperty name="sub2Menu" value="viewhistory"/>
<ffi:setL10NProperty name="PageTitle" value="Account History Export"/>
<ffi:help id="acctMgmt_history_export" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
	<title><!--L10NStart-->Financial Fusion<!--L10NEnd--></title>
</head>

<script>

//clears the 2 text fields
function clearDateTextFields() {
	$("#exportHistoryStartDate").val('');
	$("#exportHistoryEndDate").val('');
}

// resets the selected index of the given dropdown to 0
function clearDateRangeDropDown() {
	$("#dateSelectBox").selectmenu('value','');

}

function exportHistorySubmit (){
	$("#formExportHistory").attr("action","/cb/pages/jsp/account/ExportHistoryCommonAction.action");//set form action to generate report
	var formDom = $('#formExportHistory')[0];
	formDom.target = "ifrmDownload";
	formDom.submit();
	$("#formExportHistory").attr("action","/cb/pages/jsp/account/ExportHistoryCommonAction_verify.action");//reset form action to validate first
}

$.subscribe('generateHistoryReport', function(event,data) {
	$('#resultMessageDivTest').html("");
	//updateSelectedAccounts();
	exportHistorySubmit();
	$.publish('closeDialog');
});

$.subscribe('beforeGenerateHistoryReport', function(event,data) {
	formExport();//update date range type
	updateSelectedAccounts();
	removeValidationErrors();
});


function updateSelectedAccounts (){
	var transactionSearch = "<ffi:getProperty name='TransactionSearch' />";
	if(transactionSearch=='true'){
		if($("#consumerExportAccountsMultiSelectDropDown").length>0){
			var selectedAccounts = $("#consumerExportAccountsMultiSelectDropDown").val();//get the multiple selected accounts
			if(selectedAccounts){
				$("#currentSelectedExportAccounts").val(selectedAccounts);
			}
		}
		if($("#corporateExportAccountsMultiSelectDropDown").length>0){
			var selectedAccounts = $("#corporateExportAccountsMultiSelectDropDown").val();//get the multiple selected accounts
			if(selectedAccounts){
				$("#currentSelectedExportAccounts").val(selectedAccounts);
			}
		}
	}
}


function setRadioButton( fieldName, formName ) {

	//document.formExportHistory["date1"].checked=false;
	if(document.formExportHistory["date2"]){
	document.formExportHistory["date2"].checked=false;
	}
	document.formExportHistory["date3"].checked=false;
	formName[fieldName].checked = true;

	/* if(fieldName=='date1'){
		clearDateTextFields();
	}
	else */ 
	if(fieldName=='date2'){
		clearDateTextFields();
		clearDateRangeDropDown();
	}
	else if(fieldName=='date3'){
		clearDateRangeDropDown();
	}
	formExport();
}


function formExport() {
	<%-- if(document.formExportHistory["date1"].checked ) {
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue"].value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>";
	} else --%> 
		
	if( document.formExportHistory["date2"] && document.formExportHistory["date2"].checked ) { 
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue"].value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_SINCE_LAST_EXPORT %>";
	} else {
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue"].value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>";
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>&ExportHistory.SearchCriteriaValue"].value = document.formExportHistory["ExportHistory.StartDate"].value;
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>&ExportHistory.SearchCriteriaValue"].value = document.formExportHistory["ExportHistory.EndDate"].value;
	}
}


function showLastExportDate(accountText) {
	var urlString = '/cb/pages/jsp/account/inc/account-last-export-date.jsp';
	$.ajax({
		   type: 'POST',
		   url:  urlString,
		   data:"acctText="+accountText+"&CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>',
		   success: function(data){
		    data=$.trim(data);
		   	$("#lastExportDateId").html(data);
			if(data == ""){
				$("#lastexportcheck").attr('disabled', 'disabled');
				$("#dateRange3").attr('checked',true);
				$("#lastexportcheck").attr('checked',false);
				$("#selectDateRange").attr('checked',false);
				$("#exportHistoryStartDate").val($("#StartDateRangeSessionValue").val());
				$("#exportHistoryEndDate").val($("#EndDateRangeSessionValue").val());
		   }else{
				$("#lastexportcheck").removeAttr("disabled");
		   }
		   
	   }
	});
	
 }


</script>

<body>
 
<%
session.setAttribute("TransactionSearch",session.getAttribute("TransactionSearch"));
session.setAttribute("ProcessAccount", request.getParameter("ProcessAccount"));
session.setAttribute("setaccount", request.getParameter("setaccount"));
String TransactionSearch = (String)session.getAttribute("TransactionSearch");
if (TransactionSearch == null || TransactionSearch.isEmpty()) {
	TransactionSearch = request.getParameter("TransactionSearch") ==  null ? "false" : request.getParameter("TransactionSearch"); 
}

String getPagedTransactionsStartDate = null;
String getPagedTransactionsEndDate = null;
String getPagedTransactionsDateRangeValue = null;
String getPagedTransactionsDataClassification = null;

if(TransactionSearch.equalsIgnoreCase("true")) {
 	getPagedTransactionsStartDate = request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_START_DATE); 
 	getPagedTransactionsEndDate = request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_END_DATE);
 	getPagedTransactionsDateRangeValue = request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_DATE_RANGE); 
 	getPagedTransactionsDataClassification = request.getParameter(AccountConstants.TRANS_DATA_CLASSIFICATION);
} 

String accountGroup = request.getParameter("accountGroup");
String corporateAccount = request.getParameter("corporateAccount");
String consumerAccount = request.getParameter("consumerAccount");
String setAccountID = request.getParameter("SetAccount.ID");
String setAccountBankID = request.getParameter("SetAccount.BankID");
String setAccountRoutingNum = request.getParameter("SetAccount.RoutingNum");
String SetAccountExportID = request.getParameter("SetAccountExportID");

	

//Taken from DecryptionRequestWrapper to decrypt url params to retrieve account IDs
UrlEncryptor urlEncryptor = (UrlEncryptor) request.getAttribute(DecryptionFilter.ATT_ENCRYPTOR);
%>

<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="<%= getPagedTransactionsStartDate %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="<%= getPagedTransactionsEndDate %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="DateRangeValue" value="<%= getPagedTransactionsDateRangeValue %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="<%= getPagedTransactionsDateRangeValue %>" />
	<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="<%= getPagedTransactionsDataClassification %>"/> 
</ffi:cinclude>
<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
	<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${accountHistorySearchCriteria.TransactionSearchCriteria.StartDate}"/>
	<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${accountHistorySearchCriteria.TransactionSearchCriteria.EndDate}"/>
	<ffi:setProperty name="GetPagedTransactions" property="DateRangeValue" value="${accountHistorySearchCriteria.TransactionSearchCriteria.DateRangeValue}"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${accountHistorySearchCriteria.TransactionSearchCriteria.DateRangeValue}" />
	<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="${accountHistorySearchCriteria.DataClassification} "/> 
</ffi:cinclude>

<% boolean isConsumer = false; %>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<%isConsumer = false; %>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<%isConsumer = true; %>
</ffi:cinclude>


<%-- Set Selected Account values --%>
<%
String currentSelectedAccounts = "";
String exportAccountHistoryValues = "";
if(SetAccountExportID!=null){//coming from account summary portlet
	setAccountID = SetAccountExportID;
	session.setAttribute("currentSelectedAccounts", setAccountID);
	session.setAttribute("TransactionSearch","false");
%>
<ffi:removeProperty name="DefaultStartDate"/>
<ffi:removeProperty name="DefaultEndDate"/>
<%} 
else{//coming from account history or transaction search menu
	if(isConsumer && TransactionSearch.equalsIgnoreCase("true")){
		String[] consumerAccountsMultiSelectDropDown = (String[]) request.getParameterValues(AccountConstants.TRANSACTION_HISTORY_SEARCH_MULTI_SELECT_ACCOUNTS);
		
		if(null!=consumerAccountsMultiSelectDropDown){
		
		for(int i=0; i < consumerAccountsMultiSelectDropDown.length; i++) {
				String[] selectedAccountValues = consumerAccountsMultiSelectDropDown[i].split(",");
				String selectedAccountID = selectedAccountValues[0];
				currentSelectedAccounts += (currentSelectedAccounts.length()==0 ? "" : ",") + selectedAccountID;
				if(i==0){
					setAccountID  = selectedAccountID; //required to show selected account
				}
	%>
				<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
				<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
				<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
				<ffi:setProperty name="SetAccount" property="ID" value="<%= selectedAccountID %>"/>
				<ffi:process name="SetAccount"/>

				<ffi:setProperty name="ExportAccountHistoryValue" value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}"/>

	<%
			exportAccountHistoryValues += (exportAccountHistoryValues.length()==0 ? "" : ",") + session.getAttribute("ExportAccountHistoryValue");
	%>
		<%-- Sets comma delimited list of Account details (ID,BankID,RoutingNum,NickName,CurrencyCode) --%>
		<ffi:setProperty name="ExportAccountHistoryValues" value="<%=exportAccountHistoryValues%>"/>
	<%
			}
			}
		session.setAttribute("currentSelectedAccounts", currentSelectedAccounts);
	}
	else if(isConsumer && TransactionSearch.equalsIgnoreCase("false")){
		String consumerSingleAccountDropDown = request.getParameter("accountHistorySearchCriteria.account.ID"); //consumerAccount
		String[] selectedAccountValues = consumerSingleAccountDropDown.split(",");
		String selectedAccountID = selectedAccountValues[0];
		session.setAttribute("currentSelectedAccounts", selectedAccountID);
		setAccountID  = selectedAccountID; //required to show selected account
	}
	else if(!isConsumer && TransactionSearch.equalsIgnoreCase("true")){
		String[] corporateAccountMultiSelectDropDown = (String[]) request.getParameterValues(AccountConstants.TRANSACTION_HISTORY_SEARCH_MULTI_SELECT_ACCOUNTS);
		if(null!=corporateAccountMultiSelectDropDown){
		for(int i=0; i < corporateAccountMultiSelectDropDown.length; i++) {
				String[] selectedAccountValues = corporateAccountMultiSelectDropDown[i].split(",");
				String selectedAccountID = selectedAccountValues[0];
				currentSelectedAccounts += (currentSelectedAccounts.length()==0 ? "" : ",") + selectedAccountID;
				if(i==0){
					setAccountID  = selectedAccountID; //required to show selected account
				}
		%>
				<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
				<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
				<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
				<ffi:setProperty name="SetAccount" property="ID" value="<%= selectedAccountID %>"/>
				<ffi:process name="SetAccount"/>

				<ffi:setProperty name="ExportAccountHistoryValue" value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}"/>

		<%
				exportAccountHistoryValues += (exportAccountHistoryValues.length()==0 ? "" : ",") + session.getAttribute("ExportAccountHistoryValue");
		%>
		<%-- Sets comma delimited list of Account details (ID,BankID,RoutingNum,NickName,CurrencyCode) --%>
		<ffi:setProperty name="ExportAccountHistoryValues" value="<%=exportAccountHistoryValues%>"/>
		<%
				}
				}
			session.setAttribute("currentSelectedAccounts", currentSelectedAccounts);
			String corporateAccountGroupDropDown = request.getParameter("accountGroup");
			String selectedAccountGroupID = corporateAccountGroupDropDown;
			session.setAttribute("AccountGroupID", selectedAccountGroupID);	//required to show selected account group
	}else if(!isConsumer && TransactionSearch.equalsIgnoreCase("false")){
		String corporateSingleAccountDropDown = request.getParameter("accountHistorySearchCriteria.account.ID");
		String[] selectedAccountValues;
		String selectedAccountID=null;
		if (corporateSingleAccountDropDown != null) {
			System.out.println("corporateSingleAccountDropDown===" + corporateSingleAccountDropDown );
			selectedAccountID = corporateSingleAccountDropDown;
			System.out.println("selectedAccountValues===" +  selectedAccountID );
		}
		String corporateAccountGroupDropDown = request.getParameter("accountGroup");
		String selectedAccountGroupID = null;
		if (corporateAccountGroupDropDown != null) {
			String[] selectedAccountGroupValues = corporateAccountGroupDropDown.split(",");
			selectedAccountGroupID = selectedAccountGroupValues[0];
		}
		session.setAttribute("currentSelectedAccounts", selectedAccountID);
		session.setAttribute("DisplayAccountID", selectedAccountID);//required to show selected account
		session.setAttribute("AccountGroupID", selectedAccountGroupID);	//required to show selected account group
		setAccountID  = selectedAccountID; //required to show selected account
	}
}
 
%>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
<ffi:setProperty name="SetAccount" property="ID" value="<%= setAccountID %>"/>
<ffi:process name="SetAccount"/>


<% session.removeAttribute("FFIExportHistory");%>
<% session.removeAttribute("ExportHistory");%>

	
	<ffi:cinclude value1="${ExportHistory}" value2="" operator="equals">
		<ffi:object name="com.ffusion.tasks.banking.ExportHistory" id="ExportHistory" scope="session" />
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<ffi:setProperty name="ExportHistory" property="IsConsumerBanking" value="true"/>
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>"/>
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_ALL %>"/>
			<ffi:setProperty name="ExportHistory" property="DataClassification" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_ALL %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>"/>
			<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${accountHistorySearchCriteria.DataClassification}"/>
				<ffi:setProperty name="ExportHistory" property="DataClassification" value="${accountHistorySearchCriteria.DataClassification}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= getPagedTransactionsDataClassification %>"/>
				<ffi:setProperty name="ExportHistory" property="DataClassification" value="<%= getPagedTransactionsDataClassification %>"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:setProperty name="ExportHistory" property="bankingServiceName" value="com.ffusion.services.banking.interfaces.BankingService"/>
		<%--If the maximum number of days allowed in a search needs to be modified, both the variables below need to be changed. --%>
		<ffi:setProperty name="ExportHistory" property="MaxDaysInDateRange" value="180"/>
		<ffi:setProperty name="ExportHistory" property="EnforceMaxDateRangeCheck" value="true"/>
		<%--In order to improve customer experience, we will not allow the user to enter a start date or end date beyond the current date/time. --%>
		<ffi:setProperty name="ExportHistory" property="EnforceNoFutureDatesCheck" value="true"/>
		<ffi:setProperty name="ExportHistory" property="DateFormat" value="${UserLocale.DateFormat}"/>

		<%-- <ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals"> --%>
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
			<ffi:cinclude value1="${GetPagedTransactions.StartDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="StartDate" value="${GetPagedTransactions.StartDate}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.StartDate}"/>
			</ffi:cinclude>

			<ffi:cinclude value1="${GetPagedTransactions.EndDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="EndDate" value="${GetPagedTransactions.EndDate}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.EndDate}"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
			<ffi:cinclude value1="${accountHistorySearchCriteria.TransactionSearchCriteria.StartDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="StartDate" value="${accountHistorySearchCriteria.TransactionSearchCriteria.StartDate}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${accountHistorySearchCriteria.TransactionSearchCriteria.StartDate}"/>
			</ffi:cinclude>

			<ffi:cinclude value1="${accountHistorySearchCriteria.TransactionSearchCriteria.EndDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="EndDate" value="${accountHistorySearchCriteria.TransactionSearchCriteria.EndDate}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${accountHistorySearchCriteria.TransactionSearchCriteria.EndDate}"/>
			</ffi:cinclude>
		</ffi:cinclude>
		
		<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
		<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${GetPagedTransactions}" value2="" operator="equals">
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="equals">
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<%
		session.setAttribute("FFIExportHistory", session.getAttribute("ExportHistory"));
	%>

	<%-- set account(s) string for reports --%>
	<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
	<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${ExportAccountHistoryValues}" />
	</ffi:cinclude>

	<%-- set default export format --%>
	<ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="" operator="equals">
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<ffi:setProperty name="ExportHistory" property="ExportFormat" value="<%= ExportFormats.FORMAT_HTML %>" />
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:setProperty name="ExportHistory" property="ExportFormat" value="<%= ExportFormats.FORMAT_COMMA_DELIMITED %>" />
	</ffi:cinclude>
	</ffi:cinclude>

	<ffi:setProperty name="ExportHistory" property="SortCriteriaOrdinal" value="1"/>
	<ffi:setProperty name="ExportHistory" property="SortCriteriaName" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DATE %>"/>
	<ffi:setProperty name="ExportHistory" property="SortCriteriaAsc" value="False"/>


	<ffi:object name="com.ffusion.tasks.banking.GetLastExportedDate" id="GetLastExportedDate" scope="session" />
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:setProperty name="GetLastExportedDate" property="ReportType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>"/>
	</ffi:cinclude>		
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<ffi:setProperty name="GetLastExportedDate" property="ReportType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_ACCT_HISTORY %>"/>
	</ffi:cinclude>		
	<ffi:setProperty name="GetLastExportedDate" property="DateFormat" value="${UserLocale.DateTimeFormat}"/>
	<ffi:process name="GetLastExportedDate"/>
	



		<%
		
		   com.ffusion.beans.user.UserLocale userLocale = (com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale");
			Format formatter;
			String formatPattern = null;
			if( userLocale != null ) {
				formatPattern = userLocale.getDateFormat();
			    formatter = DateFormatUtil.getFormatter( formatPattern );
			}
			else {
				formatPattern = "MM/dd/yyyy";
			    formatter = DateFormatUtil.getFormatter( formatPattern  );
			}
			Calendar begin = Calendar.getInstance();
			begin.setTime( new Date() );
			Calendar end = Calendar.getInstance();
			begin.add( Calendar.DATE, -180 );
			end.add( Calendar.DATE, -1 );
			
		
			String startDate = "";
			com.ffusion.tasks.banking.ExportHistory exHistory= (com.ffusion.tasks.banking.ExportHistory)session.getAttribute("ExportHistory");
			if(exHistory!=null)
			{
				startDate = exHistory.getStartDate();
				
				if(startDate==null || "".equals(startDate))
				{
					if(session.getAttribute("DefaultStartDate") != null) {
						startDate = (String)session.getAttribute("DefaultStartDate");
					} else {
						startDate = formatter.format( begin.getTime() );
					}
				}
			}

			request.setAttribute("startDate",startDate);
			
			String endDate = "";
			com.ffusion.tasks.banking.ExportHistory exHistory2= (com.ffusion.tasks.banking.ExportHistory)session.getAttribute("ExportHistory");
			if(exHistory2!=null)
			{
				endDate = exHistory2.getEndDate();
				if(endDate==null || "".equals(endDate))
				{
					if(session.getAttribute("DefaultEndDate") != null) {
						endDate = (String)session.getAttribute("DefaultEndDate");
					} else {
						endDate = formatter.format( end.getTime());
					}
				}
			}

			request.setAttribute("endDate",endDate);
		%>
		

				<div style="padding-left:10px">

				<s:form namespace="/pages/jsp/account" action="ExportHistoryCommonAction_verify" method="post"
				name="formExportHistory" id="formExportHistory" theme="simple">
				
				<s:hidden id="StartDateRangeSessionValue" name="StartDateRangeSessionValue" value="%{#attr.startDate}"/>
				<s:hidden id="EndDateRangeSessionValue" name="EndDateRangeSessionValue" value="%{#attr.endDate}"/>

                <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
					<input type="hidden" name="GetPagedTransactions.DataClassification" id="reportOnExport" value="<ffi:getProperty name='accountHistorySearchCriteria' property='DataClassification'/>">
				</ffi:cinclude>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
					<input type="hidden" name="GetPagedTransactions.DataClassification" id="reportOnExport" value="<ffi:getProperty name='transactionHistorySearchCriteria' property='DataClassification'/>">
				</ffi:cinclude>
                <input type="hidden" name="fromExportWindow" id="fromExportWindow" value="true">
                <input type="hidden" name="TransactionSearch" id="TransactionSearch" value="<%=TransactionSearch%>">
				<input type="hidden" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue" value="">
				<input type="hidden" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>&ExportHistory.SearchCriteriaValue" value="">
				<input type="hidden" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>&ExportHistory.SearchCriteriaValue" value="">

			<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
				<input type="hidden" id="currentSelectedExportAccounts" name="ExportHistory.SearchCriteriaKey=<%= GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>&ExportHistory.SearchCriteriaValue" value="">
			</ffi:cinclude>


				<ul id="formerrors"></ul>
				<table border="0" cellspacing="0" cellpadding="0" class="tableData">
					<tr>
						<td align="center" colspan="10" width="100%">
						<s:include value="/pages/jsp/account/inc/accounthistory_accounts_dropdown.jsp"/>
						</td>
					</tr>
				<tr height="10px"></tr>
				
				<tr>
					<td colspan="2">
						<span class="sectionhead">&nbsp;<s:text name="jsp.default_143"/>&nbsp;</span>
					</td>
				</tr>
				<tr>
					<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">	
						<td>
							<input  id="lastexportcheck" type="radio" name="date2" onClick="setRadioButton( 'date2', document.formExportHistory );" <ffi:cinclude value1="${GetLastExportedDate.LastExportedDate}" value2="" operator="equals"> disabled </ffi:cinclude>>
							<span class="sectionhead"><s:text name="jsp.default.label.since.last.export"/>&nbsp;</span>
							&#40; <span id="lastExportDateId">
								<ffi:cinclude value1="" value2="${GetLastExportedDate.LastExporteddate}" operator="equals">
									<s:text name="jsp.default.label.not.previously.exported"/>
								</ffi:cinclude>
								<ffi:cinclude value1="" value2="${GetLastExportedDate.LastExporteddate}" operator="notEquals">
									<ffi:getProperty name="GetLastExportedDate" property="LastExportedDate"/>
								</ffi:cinclude>
							</span> &#41;
						</td>
					</ffi:cinclude>		
					<td>
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
						<!-- <img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="40" height="6" border="0"> -->
						<input id="dateRange3" type="radio" name="date3" <ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>" operator="equals"> checked </ffi:cinclude> onClick="setRadioButton( 'date3', document.formExportHistory );">
						<span class="marginRight10"><s:text name="jsp.default.label.date.range"/></span>
					
						<input type="text" id="acntHistoryDateRangeBox" onfocus="setRadioButton( 'date3', document.formExportHistory );" />
						
						<input type="hidden" value="<s:property value='#attr.startDate'/>" id="exportHistoryStartDate" name="ExportHistory.StartDate" />
						<input type="hidden" value="<s:property value='#attr.endDate'/>" id="exportHistoryEndDate" name="ExportHistory.EndDate" />
						
						<span id="ExportHistory.StartDateError"></span>
						<span id="ExportHistory.EndDateError"></span>
						
					</td>
				</tr>
			</table>
			<table style="margin-top: 10px" border="0" cellspacing="0" cellpadding="0" class="tableData" width="70%">
				<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
					<tr>
						<td nowrap class="sectionhead_grey" align="left">
							<span class="sectionhead"><s:text name="jsp.default.label.export.format"/>&nbsp;</span>
						</td>
						<td  align="left" >
							<img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0">
							<ffi:setProperty name="StringUtil" property="Value1" value="${ExportHistory.ExportFormat}" />

							<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
								<select class="txtbox" name="ExportHistory.exportFormat" id="consumerExportFormatID">
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QIF%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QIF%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default.exportFormat.QIF"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_323"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QFX%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QFX%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default.exportFormat.QFX"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_325"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_OFX%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_OFX%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default.exportFormat.OFX"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_105"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_402"/></option>
								</select>
							</ffi:cinclude>
							<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
								<select class="txtbox" name="ExportHistory.exportFormat" id="consumerExportFormatID">
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_105"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_323"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_402"/></option>
										<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>
											<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>" />
											<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_325"/></option>
								</select>
							</ffi:cinclude>
						</td>
					</tr>
				</ffi:cinclude>
				<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
					<!-- <tr height="10px"></tr> -->
					<tr>
						<td nowrap class="sectionhead_grey" height="30">
							<span class="sectionhead">&nbsp;<s:text name="jsp.account_87"/>:&nbsp;</span>
						</td>
						<td nowrap class="sectionhead_grey" >
							<img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0">
							<span class="sectionhead"><s:text name="jsp.default.label.export.format"/>&nbsp;</span>
						</td>
					</tr>
					<tr>			
						<td nowrap class="sectionhead_grey" >
							<select class="txtbox" name="ExportHistory.debitCreditOption" id="corporateDebitCreditOptionID">
									<option value=<%= com.ffusion.tasks.banking.ExportHistory.ALL_LINES %> selected><s:text name="jsp.default_39"/></option>
									<option value=<%= com.ffusion.tasks.banking.ExportHistory.DEBIT_LINES %>><s:text name="jsp.account_69"/></option>
									<option value=<%= com.ffusion.tasks.banking.ExportHistory.CREDIT_LINES %>><s:text name="jsp.account_59"/></option>
							</select>
						</td>
						<td nowrap class="sectionhead_grey" >
							<ffi:setProperty name="StringUtil" property="Value1" value="${ExportHistory.ExportFormat}" />
							<img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0">
							<select class="txtbox" name="ExportHistory.exportFormat" id="corporateExportFormatID">
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_231"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_105"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_402"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_323"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_59"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_325"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QIF%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QIF%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.account_172"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QFX%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QFX%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.account_171"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QBO%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QBO%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.account_170"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_IIF%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_IIF%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.account_100"/></option>
								<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV%>
									<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV%>" />
									<ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_124"/></option>
							</select>
						</td>
					</tr>
					<tr>
						<td height="30">&nbsp;</td>
					</tr>
				</ffi:cinclude>
			</table>

					<!-- <table  border="0" cellspacing="0" cellpadding="0" align="center" class="tableData">
						<tr height=40px"></tr>
						<tr>
							<td  colspan="5"> -->
								<div align="center" class="ui-widget-header customDialogFooter" id="userprofileButtonID">
								<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
										<s:text name="jsp.default_82" />
									</sj:a>

									<sj:a
									id="exportHistorySubmitID"
										formIds ="formExportHistory"
										button="true"
										cssStyle="margin-left: 10px"
										targets="resultMessageDivTest"
										validate="true"
										onBeforeTopics="beforeGenerateHistoryReport"
										onSuccessTopics="generateHistoryReport"
					  					validateFunction="customValidation"
									 >
									<s:text name="jsp.default_195" />
									</sj:a>
							</div>
							<!-- </td>
						</tr>
					</table> -->
		</div>
		<div align="center"><br></div>
		


			</td>
			<td width="1%"></td>
		</tr>
	</table>
	<!-- </form> -->
	</s:form>

<%--
 	This Div is required to display validation errors,
	if not specified as targets in sj:a,
	it throws error "No result defined for action com.ffusion.struts.account.RevampExportHistoryAction and result input"
--%>
<div id="resultMessageDivTest">

</div>
<% // } catch (Exception e) { 
	//e.printStackTrace();
//} %>
</body>
</html>

<ffi:setProperty name="BackURL" value="${SecurePath}account-history-export.jsp"/>
<ffi:removeProperty name="GetLastExportedDate"/>
<ffi:removeProperty name="SetAccountID"/>
<ffi:removeProperty name="GetTransactionTypes"/>


<script type="text/javascript">
$(document).ready(function() {
$("#dateSelectBox").selectmenu({width: 250});
$("#corporateExportFormatID").selectmenu({width: 150});
$("#consumerExportFormatID").selectmenu({width: 200});
$("#corporateDebitCreditOptionID").selectmenu({width: 200});

formExport();

var mergedRanges= {};
mergedRanges = ns.account.overrideDateRanges();                    
var aConfig = {
	startDateBox:$("#exportHistoryStartDate"),
	endDateBox:$("#exportHistoryEndDate"),
    presetRanges:mergedRanges
}
$("#acntHistoryDateRangeBox").extdaterangepicker(aConfig);

});
</script>