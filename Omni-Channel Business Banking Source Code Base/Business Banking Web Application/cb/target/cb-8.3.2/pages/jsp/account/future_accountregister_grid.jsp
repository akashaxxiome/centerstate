<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<div id="future_accountregister_gridID">
<ffi:help id="account_register_pendingTrans" />
<ffi:setGridURL grid="GRID_accountFutureRegister" name="EditURL" url="/cb/pages/jsp/account/register-rec-edit-wait.jsp?SetRegisterTransactionId={0}&CollectionSessionName={1}" parm0="RegisterId" parm1="collectionType"/>
<ffi:setGridURL grid="GRID_accountFutureRegister" name="DeleteURL" url="/cb/pages/jsp/account/register-rec-delete.jsp?SetRegisterTransactionId={0}&CollectionSessionName={1}" parm0="RegisterId" parm1="collectionType"/>

<ffi:setProperty name="futureAccountRegisterSummaryTempURL" value="/pages/jsp/account/GetFutureRegisterTransactionsAction.action?showMemoOrNot=${showMemoOrNot}&GridURLs=GRID_accountFutureRegister" URLEncrypt="true"/>
<s:url id="futureAccountRegisterSummaryURL" value="%{#session.futureAccountRegisterSummaryTempURL}" escapeAmp="false"/>
<sjg:grid
     id="futureAccountRegisterSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{futureAccountRegisterSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 shrinkToFit="true"
	 scroll="false"
	 scrollrows="true"
     onGridCompleteTopics="addGridControlsEvents,futureAccountRegisterSummaryEvent"
     >
     
     <sjg:gridColumn name="registerTypeName" index="registerTypeName" title="%{getText('jsp.default_444')}" width="80"  sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="referenceNumber" index="referenceNumber" title="%{getText('jsp.account_177')}" width="90"  sortable="false" cssClass="datagrid_numberColumn"/>
     <sjg:gridColumn name="dateIssued" index="dateIssued" title="%{getText('jsp.account_67')}" width="60"  sortable="false" cssClass="datagrid_dateColumn"/>
     <sjg:gridColumn name="map.payeeName" index="payeeName" title="%{getText('jsp.account_50')}" width="150" formatter="ns.account.payeeNameCategoryFormatter"  sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValue.currencyStringNoSymbol" title="%{getText('jsp.default_43')}" formatter="ns.account.amountFormatter" width="120"  sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.antiBalance" index="map.antiBalance" title="%{getText('jsp.account_29')}" width="150"  formatter="ns.account.antiBalanceFormatter" sortable="false" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="registerId" index="registerId" title="%{getText('jsp.default_27')}" width="50"  formatter="ns.account.accountRegisterActionFormatter" sortable="false" cssClass="datagrid_textColumn"/>     
     
     <sjg:gridColumn name="map.registerStatusType" index="map.registerStatusType" title="%{getText('jsp.account_133')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.collectionType" index="map.collectionType" title="%{getText('jsp.account_123')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.categoryName" index="map.categoryName" title="%{getText('jsp.account_122')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.showMemoOrNot" index="map.showMemoOrNot" title="%{getText('jsp.account_135')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="memo" index="memo" title="%{getText('jsp.account_141')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="status" index="status" title="%{getText('jsp.account_191')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.amountValue" index="map.amountValue" title="%{getText('jsp.account_121')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="amountValue.amountValue" index="amountValue.amountValue" title="%{getText('jsp.account_28')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
</sjg:grid>

</div>
<script>	
					
	$('#future_accountregister_gridID').portlet({
		generateDOM: true,
		helpCallback: function(){
			
			var helpFile = $('#future_accountregister_gridID').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
		
	});

	$('#future_accountregister_gridID').portlet('title', js_acc_future_portlet_title);

	$("#futureAccountRegisterSummaryID").jqGrid('setColProp','registerId',{title:false});
</script>
