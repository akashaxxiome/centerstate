<%@page import="java.util.Locale"%>
<%@page import="com.ffusion.util.UserLocaleConsts"%>
<%@page import="com.ffusion.beans.user.UserLocale"%>
<%@page import="com.ffusion.beans.accounts.GenericBankingRptConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.tasks.util.GetDatesFromDateRange"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>

<style>
	#corporateAccountGroup-button {margin: 3px 0 5px 5px; margin-top: 4px \9}
	#corporateAccountGroup  {margin: 0px 0px 0px 10px; }
</style>

<script type="text/javascript">
$(function(){
	$("#corporateAccountGroup").selectmenu({width: 140});
	//$("#corporateExportAccountID").combobox({'size':'60'});
	
	var accountGrpID = $("#corporateAccountGroup").val() || "";
	
	//need to send the source page so that formatting of vlaues can be done according to the page its triggered from.
	$("#corporateExportAccountID").lookupbox({
		"source":"/cb/pages/jsp/account/AccountHistoryLookupBoxAction.action?accountGroupId="+accountGrpID+"&srcPage=exportDD",
		"controlType":"server",
		"collectionKey":"1",
		"size":"55",
		"module":"accounthistoryexport",
		"dataReceived":function(){
            $('#corporateExportAccountID option:eq(1)').selected();
            var optLabel= $('#corporateExportAccountID option:selected').text();
            var optValue= $('#corporateExportAccountID option:selected').val();
            var optionValue = {value:optValue, label:optLabel};
            $('#corporateExportAccountID').lookupbox("value",optionValue);
		}
	});
	
	$("#consumerAccountsDropDownExport").selectmenu({width: 350,select:accountSelect});
   });

function accountSelect() {
	showLastExportDate($("#consumerAccountsDropDownExport").val());
}
function corporateAccountSelect() {
	showLastExportDate($("#corporateExportAccountID").val());
}

function getAccountsByAccountGroupType() {
	var urlString = "/cb/pages/jsp/account/GetAccountsFromGroupAction.action";
    $.ajax({
        type: "POST",
        url:  urlString,
        data: $('#formExportHistory').serialize(),
        success: function(data) {
        	var isTransactionSearch = $("#isTransactionSearch").val();
        	var accountDropdownID = "";
        	if(isTransactionSearch=="true"){
				accountDropdownID = "#corporateExportAccountsMultiSelectDropDown";
				$(accountDropdownID).get(0).options.length = 0;
				$.each(data.accountList, function(index, item) {
					$(accountDropdownID).get(0).options[$(accountDropdownID).get(0).options.length] = new Option(item.value, item.key);
				});
				$($(accountDropdownID+' option').get(0)).attr('selected', 'selected');
				$(accountDropdownID).extmultiselect('refresh');
        	}else{
        		$("#corporateExportAccountID").lookupbox("option","source","/cb/pages/jsp/account/AccountHistoryLookupBoxAction.action?accountGroupId="+$("#corporateAccountGroup").val()+"&srcPage=exportDD");
				$("#corporateExportAccountID").lookupbox("refresh");
        		/*$("#corporateExportAccountID").get(0).options.length = 0;
                $.each(data.accountList, function(index, item) {
                    $("#corporateExportAccountID").get(0).options[$("#corporateExportAccountID").get(0).options.length] = new Option(item.value, item.key);
                });
                $($('#corporateExportAccountID option').get(0)).attr('selected', 'selected');
                $("#corporateExportAccountID_autoComplete").val($("#corporateExportAccountID").get(0).options[0].text);
                
                showLastExportDate($("#corporateExportAccountID").val());
                */
        	}
        },
        error: function() {
            alert("Failed to load accounts");
        }
    });
}

</script>

			
			
			<%-- AccountGroupID: AccountGroup to show in drop-down menus --%>
			<%-- Default is the AccountGroup for the current account --%>
			<ffi:setProperty name="StringUtil" property="Value1" value="${AccountGroupID}" />
			<ffi:setProperty name="StringUtil" property="Value2" value="${Account.AccountGroup}" />
			<ffi:setProperty name="AccountGroupID" value="${StringUtil.NotEmpty}" />

			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
				<ffi:setProperty name="corporateFieldStyle" value="display:block" />
				<ffi:setProperty name="consumerFieldStyle" value="display:none" />
			</ffi:cinclude>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
				<ffi:setProperty name="corporateFieldStyle" value="display:none" />
				<ffi:setProperty name="consumerFieldStyle" value="display:block" />
			</ffi:cinclude>

			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<div id="corporateOnly" align="left" style="<ffi:getProperty name="corporateFieldStyle" />">
			<table>
			<tr>
				<td>
					<%-- Account --%>
	 				<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
	 				<%-- This is a detail page so we will filter accounts on the detail view entitlement --%>
	 				<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
	 				<%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
	 				<%-- entitled to view under that data classification --%>
	 				<ffi:cinclude value1="${GetPagedTransactions.DataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
	 					<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
	 				</ffi:cinclude>
	 				<ffi:cinclude value1="${GetPagedTransactions.DataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
	 					<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
	 				</ffi:cinclude>
	
					<ffi:setProperty name="AccountEntitlementFilterTask" property="FilteredAccountsName" value="EntitlementFilteredAccounts"/>
	 				<ffi:process name="AccountEntitlementFilterTask"/>
	
					<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="All"/>
					<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
					<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="EntitlementFilteredAccounts"/>
					<ffi:cinclude value1="${AccountFilter}" value2="" operator="notEquals">
					<ffi:setProperty name="SearchAcctsByNameNumType" property="GeneralSearch" value="${AccountFilter}"/>
					<%-- we are using the SAME list of accounts passed in as we are changing --%>
					<ffi:setProperty name="SearchAcctsByNameNumType" property="FilteredName" value="EntitlementFilteredAccounts"/>
					<ffi:process name="SearchAcctsByNameNumType"/>
					</ffi:cinclude>
					<ffi:removeProperty name="SearchAcctsByNameNumType,AccountFilter"/>
	
						<span class="sectionhead"><s:text name="jsp.default_21"/></span>
						<img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="1" height="6" border="0">
							<%-- StringUtil element to set selected option attribute --%>
							<ffi:setProperty name="StringUtil" property="Value1" value="${AccountGroupID}" />
	
							<% String defaultGroupID = null; %>
							<% String nonEmptyGroupIDs = ""; String emptyGroupIDs = ""; String thisGroupID = ""; %>

				</td>
			</tr>
			<tr>	
				<td>
					<select class="txtbox" name="accountGroup" id="corporateAccountGroup"
					onChange="getAccountsByAccountGroupType();">
					<%-- NOTE on the cincludes --%>
					<%-- we will filter the banking list for each of the account groups and then if the size of the filtered list is not 0 we will include that account type in this list --%>
	
					<ffi:setProperty name="EntitlementFilteredAccounts"
						property="Filter" value="ACCOUNTGROUP=1" />
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="notEquals">
						<%
							nonEmptyGroupIDs += "1";
						%>
						<ffi:cinclude value1="${defaultGroupID}" value2=""
							operator="equals">
							<ffi:setProperty name="defaultGroupID" value="1" />
						</ffi:cinclude>
	
						<ffi:setProperty name="StringUtil" property="Value2" value="1" />
						<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="1"><s:text name="jsp.account_76" /></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="equals">
						<%
							emptyGroupIDs += "1";
						%>
						<ffi:cinclude value1="${AccountGroupID}" value2="1"
							operator="equals">
							<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
							<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}" />
						</ffi:cinclude>
					</ffi:cinclude>
	
					<ffi:setProperty name="EntitlementFilteredAccounts"
						property="Filter" value="ACCOUNTGROUP=2" />
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="notEquals">
						<%
							nonEmptyGroupIDs += "2";
						%>
						<ffi:cinclude value1="${defaultGroupID}" value2=""
							operator="equals">
							<ffi:setProperty name="defaultGroupID" value="2" />
						</ffi:cinclude>
						<ffi:setProperty name="StringUtil" property="Value2" value="2" />
						<ffi:setProperty name="tmp_url"
							value="${URL2}?TransactionSearch=${TransactionSearch}&AccountGroupID=2&setaccount=true&LastGoodTSZBADisplay="
							URLEncrypt="true" />
						<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="2"><s:text name="jsp.account_30" /></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="equals">
						<%
							emptyGroupIDs += "2";
						%>
						<ffi:cinclude value1="${AccountGroupID}" value2="2"
							operator="equals">
							<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
							<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}" />
						</ffi:cinclude>
					</ffi:cinclude>
	
					<ffi:setProperty name="EntitlementFilteredAccounts"
						property="Filter" value="ACCOUNTGROUP=4" />
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="notEquals">
						<%
							nonEmptyGroupIDs += "4";
						%>
						<ffi:cinclude value1="${defaultGroupID}" value2=""
							operator="equals">
							<ffi:setProperty name="defaultGroupID" value="1" />
						</ffi:cinclude>
						<ffi:setProperty name="StringUtil" property="Value2" value="4" />
						<ffi:setProperty name="tmp_url"
							value="${URL4}?TransactionSearch=${TransactionSearch}&AccountGroupID=4&setaccount=true&LastGoodTSZBADisplay="
							URLEncrypt="true" />
							<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="4"><s:text name="jsp.account_58" /></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="equals">
						<%
							emptyGroupIDs += "4";
						%>
						<ffi:cinclude value1="${AccountGroupID}" value2="4"
							operator="equals">
							<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
							<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}" />
						</ffi:cinclude>
					</ffi:cinclude>
	
					<ffi:setProperty name="EntitlementFilteredAccounts"
						property="Filter" value="ACCOUNTGROUP=3" />
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="notEquals">
						<%
							nonEmptyGroupIDs += "3";
						%>
						<ffi:cinclude value1="${defaultGroupID}" value2=""
							operator="equals">
							<ffi:setProperty name="defaultGroupID" value="1" />
						</ffi:cinclude>
						<ffi:setProperty name="StringUtil" property="Value2" value="3" />
						<ffi:setProperty name="tmp_url"
							value="${URL3}?TransactionSearch=${TransactionSearch}&AccountGroupID=3&setaccount=true&LastGoodTSZBADisplay="
							URLEncrypt="true" />
							<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="3"><s:text name="jsp.account_116" /></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="equals">
						<%
							emptyGroupIDs += "3";
						%>
						<ffi:cinclude value1="${AccountGroupID}" value2="3"
							operator="equals">
							<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
							<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}" />
						</ffi:cinclude>
					</ffi:cinclude>
	
					<ffi:setProperty name="EntitlementFilteredAccounts"
						property="Filter" value="ACCOUNTGROUP=5" />
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="notEquals">
						<%
							nonEmptyGroupIDs += "5";
						%>
						<ffi:cinclude value1="${defaultGroupID}" value2=""
							operator="equals">
							<ffi:setProperty name="defaultGroupID" value="1" />
						</ffi:cinclude>
						<ffi:setProperty name="StringUtil" property="Value2" value="5" />
						<ffi:setProperty name="tmp_url"
							value="${URL5}?TransactionSearch=${TransactionSearch}&AccountGroupID=5&setaccount=true&LastGoodTSZBADisplay="
							URLEncrypt="true" />
							<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="5"><s:text name="jsp.account_155" /></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}"
						value2="0" operator="equals">
						<%
							emptyGroupIDs += "5";
						%>
						<ffi:cinclude value1="${AccountGroupID}" value2="5"
							operator="equals">
							<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
							<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}" />
						</ffi:cinclude>
					</ffi:cinclude>
				</select>


									<ffi:getProperty name="AccountGroupID" assignTo="thisGroupID"/>
									<% if (thisGroupID != null && emptyGroupIDs.length() > 0 && emptyGroupIDs.indexOf(thisGroupID) >= 0)
									{
										if (nonEmptyGroupIDs.length() > 0)
											thisGroupID = nonEmptyGroupIDs.substring(0,1);      // AccountGroupID was pointing to empty list
									} %>
									<ffi:setProperty name="AccountGroupID" value="<%=thisGroupID%>"/>
						<td/>									
						<td>
						<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
						
								<ffi:setProperty name="EntitlementFilteredAccounts"	property="Filter" value="ALL" />
					
									<ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
										<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="<%= SessionNames.ENTITLEMENTACCOUNTS %>"/>
										<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
									<ffi:process name="SetAccountsCollectionDisplayText"/>
									<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>
									
									<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
										<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.ENTITLEMENTACCOUNTS %>"/>
										<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.ENTITLEMENTACCOUNTS_INDEX %>"/>
										<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
										<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
										<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
									<ffi:process name="BuildIndex"/>
									<ffi:removeProperty name="BuildIndex"/>
								
									
									<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
										<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.ENTITLEMENTACCOUNTS %>"/>
										<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.ENTITLEMENTACCOUNTS_INDEX_BY_CURRENCY %>"/>
										<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
										<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
										<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
									<ffi:process name="BuildIndex"/>
									<ffi:removeProperty name="BuildIndex"/>
									
									<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
										<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.ENTITLEMENTACCOUNTS %>"/>
										<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.ENTITLEMENTACCOUNTS_INDEX_BY_TYPE %>"/>
										<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
										<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
										<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
										<ffi:process name="BuildIndex"/>
									<ffi:removeProperty name="BuildIndex"/>
						
										<div class="selectBoxHolder">
											<select class="txtbox"
											name="ExportHistory.SearchCriteriaKey=<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>&ExportHistory.SearchCriteriaValue"
											id="corporateExportAccountID" width="300px" onchange="corporateAccountSelect()">
											
											
	
											<%-- StringUtil element to set selected option attribute --%>
											<%--if it is redirected from other module --%>
											<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
											<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="DisplayAccountID" />
											<ffi:process name="CriterionListChecker" />
											
											
											<ffi:cinclude value1="${DisplayAccountID}" value2="" operator="notEquals">
										    	<ffi:setProperty name="StringUtil" property="Value1" value="${DisplayAccountID}" />
											</ffi:cinclude>
											
											<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="All" />
	
											<ffi:setProperty name="SetAccount" property="AccountsName" value="EntitlementFilteredAccounts"/>
											<ffi:cinclude value1="${Account.ID}" value2="" operator="notEquals">
												<ffi:setProperty name="SetAccount" property="ID" value="${Account.ID}"/>
												<ffi:setProperty name="SetAccount" property="IsDefault" value="false"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${Account.ID}" value2="">
												<ffi:setProperty name="SetAccount" property="IsDefault" value="true"/>
											</ffi:cinclude>
											
											
											<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
											<ffi:process name="SetAccount"/>
										
											<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
											<s:set var="AccountDisplayTextTaskBean" value="#attr.SelectedAccount" scope="request" />
											<ffi:setProperty name="AccountDisplayTextTask" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
											<ffi:process name="AccountDisplayTextTask"/>
											<ffi:setProperty name="selectedAccDisText" value="${AccountDisplayTextTask.AccountDisplayText}" />	
	
											<ffi:cinclude value1="${setaccount}" value2="true" operator="equals">
												<ffi:setProperty name="setaccount" value=""/>
												<ffi:list collection="EntitlementFilteredAccounts" items="acct" startIndex="1" endIndex="1">
													<ffi:setProperty name="SetAccount" property="ID" value="${acct.ID}" />
													<ffi:setProperty name="SetAccount" property="RoutingNum" value="${acct.RoutingNum}" />
													<ffi:setProperty name="SetAccount" property="BankID" value="${acct.BankID}" />
													<ffi:process name="SetAccount"/>
												</ffi:list>
											</ffi:cinclude>
	
											<ffi:setProperty name="selectAccountValue" value="${SelectedAccount.ID}:${SelectedAccount.BankID}:${SelectedAccount.RoutingNum}:${SelectedAccount.NickName}:${SelectedAccount.CurrencyCode}"/>
											<option  value="<ffi:getProperty name='selectAccountValue'/>" ><ffi:getProperty name="selectedAccDisText"/> </option>
										</select>
									</div>
									<span id="corporateExportAccountIDError"></span>
					</ffi:cinclude>									
					<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">									
									<select class="" style="width:370px; margin-bottom: 5px;"
										id="corporateExportAccountsMultiSelectDropDown" name="corporateExportAccountsMultiSelectDropDown"
										  size="4" multiple="multiple">
										
										<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
										<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentSelectedAccounts" />
										<ffi:process name="CriterionListChecker" />
										
										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=${AccountGroupID}" />

										<ffi:cinclude value1="${setaccount}" value2="true" operator="equals">
											<ffi:setProperty name="setaccount" value=""/>
											<ffi:list collection="EntitlementFilteredAccounts" items="acct" startIndex="1" endIndex="1">
												<ffi:setProperty name="SetAccount" property="ID" value="${acct.ID}" />
												<ffi:setProperty name="SetAccount" property="RoutingNum" value="${acct.RoutingNum}" />
												<ffi:setProperty name="SetAccount" property="BankID" value="${acct.BankID}" />
												<ffi:process name="SetAccount"/>
											</ffi:list>
										</ffi:cinclude>

										<ffi:list collection="EntitlementFilteredAccounts" items="Account1">
												<ffi:setProperty name="selectAccountValue" value="${Account1.ID}:${Account1.BankID}:${Account1.RoutingNum}:${Account1.NickName}:${Account1.CurrencyCode}"/>
												<option value="<ffi:getProperty name='selectAccountValue'/>"
												<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${Account1.ID}" />
												<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected
												</ffi:cinclude>>
													<ffi:cinclude value1="${Account1.RoutingNum}" value2="" operator="notEquals"><ffi:getProperty name="Account1" property="RoutingNum"/>:</ffi:cinclude><ffi:getProperty name="Account1" property="DisplayText"/><ffi:cinclude value1="${Account1.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="Account1" property="NickName"/></ffi:cinclude>	- <ffi:getProperty name="Account1" property="CurrencyCode"/>
												</option>
												
										</ffi:list>
									</select>
							<br>
							<span id="corporateExportAccountsMultiSelectDropDownError"></span>
					</ffi:cinclude>
							<td/>
							</tr>
						</table>
						
					</div>
					</ffi:cinclude>
					<%-- corporate only row ends test --%>

					<%-- consumer account field starts --%>
					<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
					<div id="consumerOnly" align="left" style="<ffi:getProperty name="consumerFieldStyle" />">
						<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
							<ffi:setProperty name="URL1" value="/cb/pages/jsp/account/accounthistory.jsp" />
							<% String strURLConsumerValue="";%>
							<ffi:getProperty name="URL1" assignTo="strURLConsumerValue" />

							<span class="sectionhead" style="white-space: nowrap;"><s:text name="jsp.default_21"/></span>
							<img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="8" height="6" border="0">
 							<%-- <select class="txtbox" name="consumerAccountsDropDownExport"  id="consumerAccountsDropDownExport" onChange="ns.account.reloadAccount('consumer');" > --%>
 							<select class="txtbox" name="ExportHistory.SearchCriteriaKey=<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>&ExportHistory.SearchCriteriaValue"
 							  id="consumerAccountsDropDownExport" onChange="ns.account.reloadAccount('consumer'),ns.common.adjustSelectMenuWidth('consumerAccountsDropDownExport')" >


								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
								<ffi:setProperty name="BankingAccounts" property="SortedBy" value="NICKNAME" />
								<ffi:setProperty name="StringUtil" property="Value1" value="${currentSelectedAccounts}" />

								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
								<ffi:list collection="BankingAccounts" items="Account1">
									<%
										UserLocale userLocale = (UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE);
										Locale locale = userLocale.getLocale();
									%>
									<ffi:setProperty name="Account1" property="Locale" value="${locale}"/>
									<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
							      	<!-- Find Account Group from Account ID -->
									<ffi:getProperty name="URL${Account1.AccountGroup}" assignTo="strURLConsumerValue" />
									<ffi:setProperty name="tmp_url" value="${strURLConsumerValue}?TransactionSearch=${TransactionSearch}&SetAccount.ID=${Account1.ID}&SetAccount.BankID=${Account1.BankID}&SetAccount.RoutingNum=${Account1.RoutingNum}&ProcessAccount=true&GetPagedTransactions.Page=first&ProcessTransactions=true&TransactionSearch=${TransactionSearch}&LastGoodTSZBADisplay=" URLEncrypt="false" />
									<ffi:setProperty name="selectAccountValue" value="${Account1.ID}:${Account1.BankID}:${Account1.RoutingNum}:${Account1.NickName}:${Account1.CurrencyCode}"/>
									<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='selectAccountValue' />"
									><ffi:getProperty name="Account1" property="accountDisplayText"/></option>
								</ffi:list>
								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1"/>
							</select>
						</ffi:cinclude>
						<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">

							<ffi:setProperty name="URL1" value="/pages/jsp/account/accounthistory.jsp" />
							<% String strURLConsumerValue="";%>
							<ffi:getProperty name="URL1" assignTo="strURLConsumerValue" />

							<span class="sectionhead" style=""><s:text name="jsp.default_21"/></span>
							<img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="8" height="6" border="0">
							<select style="width:370px; margin-bottom: 5px;"
							name="consumerExportAccountsMultiSelectDropDown"
							<%-- name="ExportHistory.SearchCriteriaKey=<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>&ExportHistory.SearchCriteriaValue" --%>
							id="consumerExportAccountsMultiSelectDropDown" size="3" multiple="multiple">

							<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
							<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentSelectedAccounts" />
							<ffi:process name="CriterionListChecker" />

							<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
							<ffi:setProperty name="BankingAccounts" property="SortedBy" value="NICKNAME" />
							<ffi:setProperty name="StringUtil" property="Value1" value="${Account.ID}" />

							<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>

							<ffi:list collection="BankingAccounts" items="Account1">
								<!-- Find Account Group from Account ID -->
								<ffi:getProperty name="URL${Account1.AccountGroup}" assignTo="strURLConsumerValue" />

								<ffi:setProperty name="selectAccountValue" value="${Account1.ID}:${Account1.BankID}:${Account1.RoutingNum}:${Account1.NickName}:${Account1.CurrencyCode}"/>
								<option value="<ffi:getProperty name='selectAccountValue'/>"
								<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${Account1.ID}" />
								<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected
								</ffi:cinclude>><ffi:getProperty name="Account1" property="accountDisplayText"/></option>
							</ffi:list>

								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1"/>
							</select>
							<br>
							<span id="consumerExportAccountsMultiSelectDropDownError"></span>
						</ffi:cinclude>
			</div>
			</ffi:cinclude>
			<%--consumer account field ends --%>


<script>
$(document).ready(function() {
	$("#corporateExportAccountsMultiSelectDropDown").extmultiselect().multiselectfilter();
	
	$("#consumerExportAccountsMultiSelectDropDown").extmultiselect().multiselectfilter();
});
</script>




