<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@page import="com.ffusion.beans.register.RegisterRptConsts"%>
<%@page import="com.ffusion.util.FilteredList"%>
<%
    String showMemoOrNot = request.getParameter("showRegMemos");
    if(showMemoOrNot!=null){
    	request.setAttribute("showMemoOrNot",showMemoOrNot);
    	%>
    	<ffi:setProperty name="showRegMemos" value="<%=showMemoOrNot %>"/>
    	<%
    }
%>
<%-- Object used to get entitlement object ID, processed in the include page "specifyregaccountdate.jsp" --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetEntitlementObjectID"/>

<%-- vvvvv Calculate Register Balance vvvvv --%>
<ffi:object name="com.ffusion.beans.util.FloatMath" id="adder1Id" scope="session"/>
    <ffi:setProperty name="adder1" property="Value1" value="0.00"/>
    <ffi:setProperty name="adder1" property="Value2" value="0.00"/>

<ffi:object name="com.ffusion.beans.common.Currency" id="temp1" scope="session"/>
<ffi:object name="com.ffusion.beans.common.Currency" id="result"/>

<%-- Set the currency codes for these Currency beans --%>
<ffi:setProperty name="temp1" property="CurrencyCode" value="${Account.CurrencyCode}"/>
<ffi:setProperty name="result" property="CurrencyCode" value="${Account.CurrencyCode}"/>

<ffi:setProperty name="temp1" property="Format" value="CURRENCY"/>
<ffi:setProperty name="temp1" property="Amount" value="${Account.CurrentBalance}"/>
<ffi:setProperty name="temp1" property="Format" value="#.00"/>
<%
   session.setAttribute("FFItemp1", session.getAttribute("temp1"));
%>
<%-- <!--Here we are getting the Account balances and adding them to each other to get the adder1 total-->--%>
<ffi:setProperty name="adder1" property="Value1" value="${RegisterTransactions.CurrentOutstandingTransactionsAmountValue.AmountValue}"/>

<ffi:setProperty name="adder1" property="Value2" value="${temp1.CurrencyStringNoSymbolNoComma}"/>
<%
   session.setAttribute("FFIadder1", session.getAttribute("adder1"));
%>
<ffi:setProperty name="result" property="Amount" value="${adder1.Add}"/>
<ffi:setProperty name="SessRegBalance" value="${result.String}"/>
<%-- ^^^^^ Calculate Register Balance ^^^^^ --%>

<ffi:setProperty name="activeMemofalse" value="false"/>
<ffi:setProperty name="activeMemotrue" value="${showRegMemos}"/>
<ffi:setProperty name="Compare" property="Value1" value="${showRegMemos}"/>
<ffi:setProperty name="Compare" property="Value2" value=""/>
<% String s = ""; %>
<ffi:getProperty name="activeMemo${Compare.NotEquals}" assignTo="s"/>
<ffi:setProperty name="showRegMemos" value="<%= s %>"/>
<%-- <!--
We use these tags to make an amount show up red if it's negative.-->--%>
<ffi:object name="com.ffusion.beans.util.FloatMath" id="FloatMath"/>
<ffi:setProperty name="negativetrue" value="<span style='color:#FF0000'>"/>
<ffi:setProperty name="negativefalse" value="<span>"/>

<ffi:object name="com.ffusion.beans.common.Currency" id="Currency" scope="session"/>
    <%
		session.setAttribute("FFICurrency", session.getAttribute("Currency"));
	%>
<SCRIPT type=text/javascript>
    $(function(){
    	$("#ExportRegisterTransactions\\.DebitCreditOption").selectmenu({width: 100});
    	$("#ExportRegisterTransactions\\.IncludeType").selectmenu({width: 280});
    	$("#ExportRegisterTransactions\\.Format").selectmenu({width: 180});
        });
<!--
function CSClickReturn () {
    var bAgent = window.navigator.userAgent;
    var bAppName = window.navigator.appName;
    if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
        return true; // dont follow link
    else return false; // dont follow link
}
userAgent = window.navigator.userAgent;
browserVers = parseInt(userAgent.charAt(userAgent.indexOf("/")+1),10);
mustInitImg = true;
function initImgID() {di = document.images; if (mustInitImg && di) { for (var i=0; i<di.length; i++) { if (!di[i].id) di[i].id=di[i].name; } mustInitImg = false;}}
function findElement(n,ly) {
    d = document;
    if (browserVers < 4)        return d[n];
    if ((browserVers >= 6) && (d.getElementById)) {initImgID; return(d.getElementById(n))};
    var cd = ly ? ly.document : d;
    var elem = cd[n];
    if (!elem) {
        for (var i=0;i<cd.layers.length;i++) {
            elem = findElement(n,cd.layers[i]);
            if (elem) return elem;
        }
    }
    return elem;
}
function changeImages() {
    d = document;
    if (d.images) {
        var img;
        for (var i=0; i<changeImages.arguments.length; i+=2) {
            img = null;
            if (d.layers) {img = findElement(changeImages.arguments[i],0);}
            else {img = d.images[changeImages.arguments[i]];}
            if (img) {img.src = changeImages.arguments[i+1];}
        }
    }
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
    var result;
    for (var i=0;i<array.length;i++) {
        if(CSStopExecution) return false;
        var aa = fct[array[i]];
        if (aa == null) return false;
        var ta = new Array;
        for(var j=1;j<aa.length;j++) {
            if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
                if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
                else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
                else ta[j]=aa[j];}
            } else ta[j]=aa[j];
        }
        result=aa[0](ta);
    }
    return result;
}
CSAct = new Object;
function CSOpenWindow(action) {
    var wf = "";
    wf = wf + "width=" + action[3];
    wf = wf + ",height=" + action[4];
    wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
    wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
    wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
    wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
    wf = wf + ",directories=" + (action[9] ? "yes" : "no");
    wf = wf + ",location=" + (action[10] ? "yes" : "no");
    wf = wf + ",status=" + (action[11] ? "yes" : "no");
    window.open(action[1],action[2],wf);
}
function CSGotoLink(action) {
    if (action[2].length) {
        var hasFrame=false;
        for(i=0;i<parent.frames.length;i++) { if (parent.frames[i].name==action[2]) { hasFrame=true; break;}}
        if (hasFrame==true)
            parent.frames[action[2]].location = action[1];
        else
            window.open (action[1],action[2],"");
    }
    else location = action[1];
}

<%-- added for checkimaging --%>
function CSOpenImageWindow(url) {
	alert(url);
    window.open(url, "CheckImage", "width=600, height=550, location=no, menubar=no, status=no, toolbar=no, scrollbars=no, resizable=no");
}

// --></script>

<SCRIPT type=text/javascript>
<!--
var preloadFlag = true;
CSAct[/*CMP*/ 'B9E5718F1'] = new Array(CSOpenWindow,/*URL*/ 'checkimage.htm','',550,600,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E5718F5'] = new Array(CSOpenWindow,/*URL*/ 'checkimage.htm','',550,600,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E5718F9'] = new Array(CSOpenWindow,/*URL*/ 'checkimage.htm','',550,600,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E5718F11'] = new Array(CSOpenWindow,/*URL*/ 'checkimage.htm','',550,600,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E5761213'] = new Array(CSOpenWindow,/*URL*/ 'checkimage.htm','',550,600,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E582EE43'] = new Array(CSGotoLink,/*URL*/ 'accountrecon.htm','');CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="/cb/servlet/cb/calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.StartDate"/>','',350,300,false,false,false,false,false,false,false);

CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=changeAccount&calTarget=GetRegisterTransactions.FromDate"/>','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8166'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=changeAccount&calTarget=GetRegisterTransactions.ToDate"/>','',350,300,false,false,false,false,false,false,false);

// -->
</SCRIPT>


<SCRIPT language=javascript>
<!--
function popNav()
{
    var optionValue;
    optionValue = document.navForm.popNav.options[document.navForm.popNav.selectedIndex].value;
    document.location = optionValue;
}
-->
</SCRIPT>

<div align=center>

<ffi:setProperty name="subMenuSelected" value="account register"/>
<%-- include page header --%>
<s:include value="/pages/jsp/account/inc/specifyregaccountdate.jsp" />

<%-- <!-- START MANUAL/CLEARED TRANSACTIONS -->--%>
<ffi:setProperty name="Math" property="Value1" value="3"/>
<ffi:setProperty name="Math" property="Value2" value="1"/>
<ffi:setProperty name="reverse${RegisterTransactions.SortedBy}" value=",REVERSE" />


<%-- <!--
Based on what kind of status a transaction has, display a reconciled icon, a matched and reconciled icon, or a
matched and reconciled with discrepancy icon. This is setting up strings to be used to create the anchor tags, and
the appropriate icon.
-->--%>
<s:set var="tmpI18nStr" value="%{getText('jsp.account_119')}" scope="request" /><ffi:setProperty name="RecImage0" value="<img src='/cb/web/multilang/grafx/account/i_in_process.gif' border='0' width='16' height='16' alt='${tmpI18nStr}'>"/>
<ffi:setProperty name="RecImage1" value="---"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.account_174')}" scope="request" /><ffi:setProperty name="RecImage2" value="<img src='/cb/web/multilang/grafx/account/gen-checkmark.gif' border='0' width='16' height='16' alt='${tmpI18nStr}'>"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.account_139')}" scope="request" /><ffi:setProperty name="RecImage3" value="<img src='/cb/pages/${ImgExt}grafx/account/gen-checkmarkauto.gif' border='0' width='16' height='16' alt='${tmpI18nStr}'>"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.account_140')}" scope="request" /><ffi:setProperty name="RecImage4" value="<img src='/cb/pages/${ImgExt}grafx/account/gen-checkmarkautodisc.gif' border='0' width='16' height='16' alt='${tmpI18nStr}'>"/>
<ffi:object name="com.ffusion.beans.util.StringTable" id="CatLookupTable"/>
<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
<ffi:list collection="RegisterCategories" items="Categories">
    <ffi:setProperty name="CatLookupTable" property="Key" value="${Categories.Id}"/>
    <ffi:setProperty name="CatLookupTable" property="Value" value="${Categories.Name}"/>
</ffi:list>

<ffi:list collection="RegisterTransactions" items="Trans1">
    <ffi:setProperty name="CatLookupTable" property="Key" value="${Trans1.RegisterCategoryId}"/>
    <ffi:setProperty name="Trans1" property="CategoryName" value="${CatLookupTable.Value}"/>
</ffi:list>

<ffi:setProperty name="NoSortSettrue" value="DATE_ISSUED"/>
<ffi:setProperty name="NoSortSetfalse" value="${RegisterTransactions.SortedBy}"/>
<ffi:setProperty name="Compare" property="Value1" value=""/>
<ffi:setProperty name="Compare" property="Value2" value="${RegisterTransactions.SortedBy}"/>
<ffi:cinclude value1="${Compare.Equals}" value2="true">
	<ffi:setProperty name="RegisterTransactions" property="SortedBy" value="${NoSortSettrue}"/>
</ffi:cinclude>

<%-- Refresh the collection used by the sort image task --%>
<ffi:process name="RegisterTransactionsSortImage"/>
<%-- INITIALIZING TRANSACTION SEARCH IMAGE TASK --%>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>

	<%
		session.setAttribute("FFISearchImage", session.getAttribute("SearchImage"));
	%>

<%--display the Account register data grid    --%>
   <div id="accoungRegisterDeleteSuccess"></div>
   <s:include value="/pages/jsp/account/accountregister_accountregister_grid.jsp"/>
   <br/>
   <br/>
   <br/>
<%-- 
The AntBalance (Anticipated Balance) is calculated by subtracting the future transaction amount from the
current amount.
--%>
<ffi:object name="com.ffusion.beans.common.Currency" id="AntBalance" scope="session"/>
<ffi:setProperty name="AntBalance" property="Format" value="CURRENCY"/>
<ffi:setProperty name="AntBalance" property="Amount" value="${SessRegBalance}"/>
<ffi:setProperty name="AntBalance" property="Format" value="#.00"/>
<%
 session.setAttribute("FFIAntBalance", session.getAttribute("AntBalance"));
%>
<%--display the Future Account register data grid  --%> 
   <div id="accoungPendingRegisterDeleteSuccess"></div>
   <s:include value="/pages/jsp/account/future_accountregister_grid.jsp"/>
    
<%--<!-- START OF EXPORT -->--%>
<ffi:object id="ExportRegisterTransactions" name="com.ffusion.tasks.register.ExportRegisterTransactions" scope="session"/>
	<%
		session.setAttribute("FFIExportRegisterTransactions", session.getAttribute("ExportRegisterTransactions"));
	%>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
<br>
<table width="750" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <div align="center">
                <table width="571" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="center" class="sectionhead_grey">
				            <s:form id="exportAccountRegister" name="exportform" method="post" namespace="/pages/jsp/account" action="ExportRegisterTransactionsAction" theme="simple" target="_blank">
			                    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<input type="hidden" name="ExportRegisterTransactions.startDate" value="<ffi:getProperty name="GetRegisterTransactions" property="FromDate"/>">
								<input type="hidden" name="ExportRegisterTransactions.endDate" value="<ffi:getProperty name="GetRegisterTransactions" property="ToDate"/>">
								<input type="hidden" name="ExportRegisterTransactions.ReferenceNumber" value="<ffi:getProperty name="GetRegisterTransactions" property="ReferenceNumber"/>">
								<table width="418" border="0" cellspacing="0" cellpadding="3" class="sectionhead_grey">
									<tr>
										<td nowrap class="lightbackground" width="60"><img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0"><span class="sectionsubhead"><s:text name="jsp.account_153"/></span></td>
										<td nowrap class="lightbackground" width="95">
											<select id="ExportRegisterTransactions.DebitCreditOption" class="txtbox" name="ExportRegisterTransactions.DebitCreditOption">
													<option value=<%= com.ffusion.tasks.register.ExportRegisterTransactions.ALL_LINES %> selected><s:text name="jsp.default_39"/></option>
													<option value=<%= com.ffusion.tasks.register.ExportRegisterTransactions.DEBIT_LINES %>><s:text name="jsp.account_69"/></option>
													<option value=<%= com.ffusion.tasks.register.ExportRegisterTransactions.CREDIT_LINES %>><s:text name="jsp.account_59"/></option>
											</select>
										</td>
									    <td nowrap class="lightbackground">
						                    <select id="ExportRegisterTransactions.IncludeType" class=txtbox name=ExportRegisterTransactions.IncludeType>
						                        <option value="<%=RegisterRptConsts.VALUE_STATUS_RECONCILED%>"><s:text name="jsp.account_104"/></option>
						                        <option value="<%=RegisterRptConsts.VALUE_STATUS_UNRECONCILED%>"><s:text name="jsp.account_105"/></option>
						                        <option value="<%=RegisterRptConsts.VALUE_STATUS_ALL%>"><s:text name="jsp.account_103"/></option>
						                    </select>
						                </td>
										<td nowrap class="lightbackground" width="107"><img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0"><span class="sectionsubhead"><s:text name="jsp.account_86"/></span></td>
										<td nowrap class="lightbackground" width="108">
											<select id="ExportRegisterTransactions.Format" class="txtbox" name="ExportRegisterTransactions.Format">
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML%>><s:text name="jsp.default_231"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>><s:text name="jsp.default_105"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>><s:text name="jsp.default_402"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>><s:text name="jsp.default_323"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>><s:text name="jsp.default_325"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QIF%>><s:text name="jsp.account_172"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QFX%>><s:text name="jsp.account_171"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QBO%>><s:text name="jsp.account_170"/></option>
												<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_IIF%>><s:text name="jsp.account_100"/></option>
											</select>
										</td>
										
										<td nowrap class="lightbackground" width="78">
										<sj:a 
										    id="exportAccountRegisterSubmit1"
										    formIds="exportAccountRegister"
											button="true" 
										><s:text name="jsp.default_195"/></sj:a>
										</td>
									</tr>
								</table>
							</s:form>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
<br>
</ffi:cinclude>
<%--<!-- END OF EXPORT -->--%>
</div>

<ffi:removeProperty name="GetEntitlementObjectID" />
<ffi:removeProperty name="RecImage0"/>
<ffi:removeProperty name="RecImage1"/>
<ffi:removeProperty name="RecImage2"/>
<ffi:removeProperty name="RecImage3"/>
<ffi:removeProperty name="RecImage4"/>
<ffi:removeProperty name="AntBalance"/>
<ffi:removeProperty name="temp1"/>
<ffi:removeProperty name="adder1"/>
<ffi:removeProperty name="Currency"/>