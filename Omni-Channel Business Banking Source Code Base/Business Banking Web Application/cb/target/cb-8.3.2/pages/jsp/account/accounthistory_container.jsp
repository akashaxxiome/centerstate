<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="account_accounthistory" />

<script type="text/javascript">
	<!--
	$.ajaxSetup({
	    async: false
	});
	//-->
</script>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
			<ffi:setProperty name="TransactionSearchConsumer" value="true" />
			<% 
				int accountIndex = 1;
				//CR #698790, #698794 irrrespective of Account Group accounthistory.jsp will be shown for consumer
				String strURLConsumerValue = (String)session.getAttribute("URL1");
				String exportAccountHistoryValues = "";
			%>
			<ffi:list collection="SelectedAccounts" items="Account">
				 
				<ffi:setProperty name="setAccountID" value="${Account.ID}" />
				<ffi:setProperty name="accountIndex" value="<%=String.valueOf(accountIndex)%>" />
				<ffi:setProperty name="ExportAccountHistoryValue" value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}"/>
				<%
					exportAccountHistoryValues += (exportAccountHistoryValues.length()==0 ? "" : ",") + session.getAttribute("ExportAccountHistoryValue");
					session.setAttribute(("SetAccount" + accountIndex), session.getAttribute("SetAccount"));
				%>
				
				<ffi:setProperty name="accountURL" value="<%=strURLConsumerValue%>"/>
				<s:set var="accountURL" value="%{#session.accountURL}" />
				<ffi:setProperty name="accountHistoryGridID${accountIndex}" value="accountHistoryGrid${accountIndex}"/>
				<s:include value="%{#accountURL}"/>
				<%
					accountIndex = accountIndex + 1;
				%>
			</ffi:list>
			<%-- Sets comma delimited list of Account details (ID,BankID,RoutingNum,NickName,CurrencyCode) --%>
			<ffi:setProperty name="ExportAccountHistoryValues" value="<%=exportAccountHistoryValues%>"/>
			
		</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
					<% 
						int accountIndex = 1;
						String selectedAccountGroup = (String)session.getAttribute("selectedAccountGroup");
						String strURLCorporateValue = (String)session.getAttribute("URL"+selectedAccountGroup);
						String exportAccountHistoryValues = "";
					%>
					<ffi:list collection="SelectedAccounts" items="Account">
						<ffi:setProperty name="setAccountID" value="${Account.ID}" />
						<ffi:setProperty name="accountIndex" value="<%=String.valueOf(accountIndex)%>" />
						<ffi:setProperty name="ExportAccountHistoryValue" value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}"/>
						<%
							exportAccountHistoryValues += (exportAccountHistoryValues.length()==0 ? "" : ",") + session.getAttribute("ExportAccountHistoryValue");
							session.setAttribute(("SetAccount" + accountIndex), session.getAttribute("SetAccount"));
						%>
						
						<ffi:setProperty name="accountURL" value="<%=strURLCorporateValue%>"/>
						<s:set var="accountURL" value="%{#session.accountURL}" />
						<ffi:setProperty name="accountHistoryGridID${accountIndex}" value="accountHistoryGrid${accountIndex}"/>
						<s:include value="%{#accountURL}"/>
						<%
							accountIndex = accountIndex + 1;
						%>
					</ffi:list>
					<%-- Sets comma delimited list of Account details (ID,BankID,RoutingNum,NickName,CurrencyCode) --%>
					<ffi:setProperty name="ExportAccountHistoryValues" value="<%=exportAccountHistoryValues%>"/>
</ffi:cinclude>
<script type="text/javascript">
	<!--
	$.ajaxSetup({
	    async: true
	});
	//-->
</script>