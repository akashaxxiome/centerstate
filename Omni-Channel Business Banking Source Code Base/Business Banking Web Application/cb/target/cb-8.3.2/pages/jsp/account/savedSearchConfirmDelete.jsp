<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set name="reportsName" value="%{#parameters.reportName}" scope="session"/>
<s:set name="reportID" value="%{#parameters.reportID}" scope="session"/>

<ffi:help id="reports_rptconfirmdelete" />

<div align="center">
			<s:form id="deleteSavedSearchForm" action="DeleteReportAction" namespace="/pages/jsp/reports" method="post" theme="simple">

			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

			<table width="400" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><img src="/cb/web/multilang/grafx/spacer.gif" height="11" width="1" border="0"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><span class="sectionhead errorcolor"><s:text name="jsp.confirm_delete_saved_search"/></span></td>
				</tr>
				<tr>
					<td class="columndata" valign="middle" align="right" width="50%">
						<s:hidden name="IdentificationID" value="%{#parameters.reportID}"/>
						<span class="sectionhead"><s:text name="jsp.reports_652"/> </span>
					</td>
					<td>
						<s:property value="%{#parameters.reportName}"/>
					</td>
				</tr>
				<tr>
					<td class="columndata" valign="middle" align="right">
						<s:hidden name="IdentificationName" value="%{#parameters.reportName}"/>
						<s:hidden name="IdentificationDesc" value="%{#parameters.reportDesc}"/>
						<span class="sectionhead"><s:text name="jsp.default_171"/> </span>
					</td>
					<td>
						<s:property value="%{#parameters.reportDesc}"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="30">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" valign="middle" align="center" class="ui-widget-header customDialogFooter">

						<sj:a button="true" onClickTopics="errorDeleteSavedSearch"><s:text name="jsp.default_82"/></sj:a>

						<sj:a
							formIds="deleteSavedSearchForm"
							id="deleteSavedSearchFormSubmit"
							targets="resultmessage"
							button="true"
							onBeforeTopics="beforeDeleteSavedSearch"
							onSuccessTopics="successDeleteSavedSearch"
							onErrorTopics="errorDeleteSavedSearch"
							onCompleteTopics="completeDeleteSavedSearch"><s:text name="jsp.default_162"/></sj:a>
					</td>
				</tr>
			</table>
			<br>
			</s:form>
</div>
<ffi:removeProperty name="reportNames"/>
<ffi:removeProperty name="reportName"/>
<ffi:removeProperty name="category"/>
<ffi:removeProperty name="fromSavedReportsJsp"/>
