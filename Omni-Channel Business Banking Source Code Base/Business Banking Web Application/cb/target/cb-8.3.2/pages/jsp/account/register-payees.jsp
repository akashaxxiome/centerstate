<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@page import="com.ffusion.util.FilteredList"%>
<% 
   session.setAttribute("RegisterPayees.Filter",request.getParameter("RegisterPayees.Filter"));
   session.setAttribute("RegisterPayees.FilterSortedBy",request.getParameter("RegisterPayees.FilterSortedBy"));
   session.setAttribute("SetEditRegisterPayeeId",request.getParameter("SetEditRegisterPayeeId"));
%>
<script type="text/javascript">
   $(function(){
      $("#RegisterPayee\\.DefaultCategory").selectmenu({width: 190});
	   });
</script>
<% String v151_4a = ""; String v151_2b = ""; %>
<ffi:cinclude value1="${register_init_touched}" value2="true" operator="notEquals" >
    <s:include value="/pages/jsp/inc/init/register-init.jsp" />
</ffi:cinclude>

<%-- Prepare Sorting Functionality --%>
<ffi:cinclude value1="${GetRegisterPayeesImage}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.GetSortImage" id="GetRegisterPayeesImage" scope="session"/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetRegisterPayeesImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/account/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetRegisterPayeesImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/account/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetRegisterPayeesImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/account/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <ffi:setProperty name="GetRegisterPayeesImage" property="Collection" value="RegisterPayees"/>
    <ffi:setProperty name="GetRegisterPayeesImage" property="Compare" value="NAME"/>
    <ffi:process name="GetRegisterPayeesImage"/>
</ffi:cinclude>

<ffi:cinclude value1="${CatLookupTable.Size}" value2="0">
    <ffi:setProperty name="RegisterPayees" property="Filter" value="All" />
    <ffi:list collection="RegisterPayees" items="RegisterPayee1">
        <ffi:setProperty name="CatLookupTable" property="Key" value="${RegisterPayee1.DefaultCategory}" />
        <ffi:setProperty name="RegisterPayee1" property="CategoryName" value="${CatLookupTable.Value}" />
    </ffi:list>
</ffi:cinclude>

<%-- Edit the register payee if the SetEditRegisterPayeeId is not empty set it --%>

<ffi:object id="SetRegisterPayee" name="com.ffusion.tasks.register.SetRegisterPayee" scope="session"/>
    <ffi:cinclude value1="${SetEditRegisterPayeeId}" value2="">
            <ffi:setProperty name="SetRegisterPayee" property="Id" value="" />
    </ffi:cinclude>
    <ffi:cinclude value1="${SetEditRegisterPayeeId}" value2="" operator="notEquals">
            <ffi:setProperty name="SetRegisterPayee" property="Id" value="${SetEditRegisterPayeeId}" />
    </ffi:cinclude>
<ffi:process name="SetRegisterPayee"/>

<div id="register_payeesID" align=center>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="11"></td>
        <td width="11"></td>
        <td width="11"></td>
    </tr>
    <tr>
        <td width="11"><br>
            <br>
        </td>
        <td align="center">
  
    <ffi:cinclude value1="${SetEditRegisterPayeeId}" value2="">
		<ffi:help id="account_register-payee-add" />
            <s:form id="addpayee" action="AddPayeeAction" namespace="/pages/jsp/account" name="addpayee" method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
            <input type="hidden" name="SaveAndAdd" value="true">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">

            <tr><td height="14"></td></tr>
             <tr>
                    <td class="sectionsubhead" width="40%" align="right"><s:text name="jsp.default_314"/><span class="required">*</span></td>
                    <td class="columndata" width="60%" align="left"><input class="ui-widget-content ui-corner-all" type="text" name="RegisterPayee.Name" size="40" maxlength="60" value="<ffi:getProperty name="RegisterPayee" property="Name" />">
                    <span id="RegisterPayee.NameError"></span>
                    </td>
                </tr>
                <tr>
                    <td class="sectionsubhead" align="right"><s:text name="jsp.account_44"/></td>
                    <td class="columndata" align="left">
							
					<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
						<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
						<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
					<ffi:process name="CopyCollection"/>
					<ffi:list collection="RegisterCategories" items="Category">
						<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
							<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
						</ffi:cinclude>
					</ffi:list>
					
                    <select id="RegisterPayee.DefaultCategory" name="RegisterPayee.DefaultCategory" class="txtbox" >
                    <ffi:setProperty name="Compare" property="Value1" value="${RegisterPayee.DefaultCategory}" />
                        <option value="0"><s:text name="jsp.default_445"/></option>
                        <ffi:setProperty name="RegisterCategories" property="Filter" value="ParentCategory=-1,Id!0,Id!${RegisterCategory.Id},AND" />
                        <ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />
                        <ffi:list collection="RegisterCategories" items="Category">
                        <ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />
                        <option value="<ffi:getProperty name="Category" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Category" property="Name"/></option>
                        <ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="ParentCategory=${Category.Id}" />
                        <ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
                        <ffi:list collection="RegisterCategoriesCopy" items="Category1">
                        <ffi:setProperty name="Compare" property="Value2" value="${Category1.Id}" />
                        <option value="<ffi:getProperty name="Category1" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;<ffi:getProperty name="Category" property="Name"/>: <ffi:getProperty name="Category1" property="Name"/></option>
                        </ffi:list>
                    </ffi:list>
                    </select>
                    </td>
                </tr>
		<tr>
		    <td colspan="2"><br></td>
		</tr>
		<tr>
		    <td align="center" colspan="2"><span class="required">* <s:text name="jsp.default_240"/></span></td>
		</tr>
		<tr>
		    <td colspan="2"><br></td>
		</tr>
		<tr>
                    <td class="columndata" colspan="2" align="center">
                    <script>
						ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
					</script>
                    <sj:a 
                       button="true"
                       onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"><s:text name="jsp.default_82"/></sj:a>&nbsp;&nbsp;
                       <sj:a 
                       formIds="addpayee"
                       button="true"
                       buttonIcon="ui-icon-check"
                       targets="accountRegisterDashbord"
                       validateFunction="customValidation"
                       validate="true"
                       ><s:text name="jsp.default_366"/></sj:a>
                  </td>
                </tr>

            </table>
            </s:form>
    </ffi:cinclude>
    <ffi:cinclude value1="${SetEditRegisterPayeeId}" value2="" operator="notEquals">
		<ffi:help id="account_register-payee-edit-save" />
             <s:form id="editRegisterPayee" action="EditPayeeAction" namespace="/pages/jsp/account" name="editRegisterPayee" method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			     <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr><td height="14"></td></tr>
                 <tr>
                    <td class="sectionsubhead" width="40%" align="right"><s:text name="jsp.default_314"/><span class="required">*</span></td>
                    <td class="columndata" width="60%" align="left"><input class="ui-widget-content ui-corner-all" type="text" name="RegisterPayee.Name" size="40" maxlength="40" value="<ffi:getProperty name="RegisterPayee" property="Name" />">
                    <span id="RegisterPayee.NameError"></span>
                    </td>
                </tr>
                <tr>
                    <td class="sectionsubhead" align="right"><s:text name="jsp.account_44"/></td>
                    <td class="columndata" align="left">
							
					<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
						<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
						<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
					<ffi:process name="CopyCollection"/>
					<ffi:list collection="RegisterCategories" items="Category">
						<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
							<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
						</ffi:cinclude>
					</ffi:list>
			
                    <select id="RegisterPayee.DefaultCategory" name="RegisterPayee.DefaultCategory" class="txtbox" >
                    <ffi:setProperty name="Compare" property="Value1" value="${RegisterPayee.DefaultCategory}" />
                        <option value="0"><s:text name="jsp.default_445"/></option>
                        <ffi:setProperty name="RegisterCategories" property="Filter" value="ParentCategory=-1,Id!0,Id!${RegisterCategory.Id},AND" />
                        <ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />
                        <ffi:list collection="RegisterCategories" items="Category">
                        <ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />
                        <option value="<ffi:getProperty name="Category" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Category" property="Name"/></option>
                        <ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="ParentCategory=${Category.Id}" />
                        <ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
                        <ffi:list collection="RegisterCategoriesCopy" items="Category1">
                        <ffi:setProperty name="Compare" property="Value2" value="${Category1.Id}" />
                        <option value="<ffi:getProperty name="Category1" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;<ffi:getProperty name="Category" property="Name"/>: <ffi:getProperty name="Category1" property="Name"/></option>
                        </ffi:list>
                    </ffi:list>
                    </select>
                    </td>
                </tr>
		<tr>
		    <td colspan="2"><br></td>
		</tr>
		<tr>
		    <td align="center" colspan="2"><span class="required">* <s:text name="jsp.default_240"/></span></td>
		</tr>
		<tr>
		    <td colspan="2"><br></td>
		</tr>
		<tr>
                    <td class="columndata" colspan="2" align="center">
                    <script>
						ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
					</script>
                     <sj:a 
                       button="true"
                       onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"><s:text name="jsp.default_82"/></sj:a>&nbsp;&nbsp;
                       <sj:a 
                       formIds="editRegisterPayee"
                       button="true"
                       buttonIcon="ui-icon-check"
                       targets="accountRegisterDashbord"
                       validateFunction="customValidation"
                       validate="true"
                       ><s:text name="jsp.default_366"/></sj:a>
                    </td>
                </tr>
            </table>
            </s:form>
    </ffi:cinclude>
               
        </td>
        <td align="right" width="11"><br>
            <br>
        </td>
    </tr>
    <tr>
        <td width="11"></td>
        <td width="11"></td>
        <td width="11"></td>
    </tr>
</table>
</div>
<br>

       <ffi:object id="CatLookupTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
       <% 
        session.setAttribute("FFICatLookupTable", session.getAttribute("CatLookupTable"));
       %>
       <ffi:setProperty name="RegisterCategories" property="Filter" value="All" />
       <ffi:list collection="RegisterCategories" items="Categories">
           <ffi:setProperty name="CatLookupTable" property="Key" value="${Categories.Id}" />
           <ffi:setProperty name="CatLookupTable" property="Value" value="${Categories.Name}" />
       </ffi:list>
<div id="deletePayeeresultmessage">
</div>
<div id="filterLink">
<a href="#" onclick="ns.account.redirectA('NAME>>a,AND,NAME<< b')"><span class=sectionsubhead><s:text name="jsp.account_5"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>b,AND,NAME<< c')"><span class=sectionsubhead><s:text name="jsp.account_34"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>c,AND,NAME<< d')"><span class=sectionsubhead><s:text name="jsp.account_43"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>d,AND,NAME<< e')"><span class=sectionsubhead><s:text name="jsp.account_63"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>e,AND,NAME<< f')"><span class=sectionsubhead><s:text name="jsp.default_177"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>f,AND,NAME<< g')"><span class=sectionsubhead><s:text name="jsp.account_88"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>g,AND,NAME<< h')"><span class=sectionsubhead><s:text name="jsp.account_91"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>h,AND,NAME<< i')"><span class=sectionsubhead><s:text name="jsp.account_92"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>i,AND,NAME<< j')"><span class=sectionsubhead><s:text name="jsp.account_96"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>j,AND,NAME<< k')"><span class=sectionsubhead><s:text name="jsp.account_112"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>k,AND,NAME<< l')"><span class=sectionsubhead><s:text name="jsp.account_113"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>l,AND,NAME<< m')"><span class=sectionsubhead><s:text name="jsp.account_114"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>m,AND,NAME<< n')"><span class=sectionsubhead><s:text name="jsp.account_118"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>n,AND,NAME<< o')"><span class=sectionsubhead><s:text name="jsp.account_143"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>o,AND,NAME<< p')"><span class=sectionsubhead><s:text name="jsp.account_151"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>p,AND,NAME<< q')"><span class=sectionsubhead><s:text name="jsp.account_157"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>q,AND,NAME<< r')"><span class=sectionsubhead><s:text name="jsp.account_169"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>r,AND,NAME<< s')"><span class=sectionsubhead><s:text name="jsp.account_173"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>s,AND,NAME<< t')"><span class=sectionsubhead><s:text name="jsp.account_184"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>t,AND,NAME<< u')"><span class=sectionsubhead><s:text name="jsp.account_195"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>u,AND,NAME<< v')"><span class=sectionsubhead><s:text name="jsp.account_219"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>v,AND,NAME<< w')"><span class=sectionsubhead><s:text name="jsp.account_222"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>w,AND,NAME<< x')"><span class=sectionsubhead><s:text name="jsp.account_224"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>x,AND,NAME<< y')"><span class=sectionsubhead><s:text name="jsp.account_225"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>y,AND,NAME<< z')"><span class=sectionsubhead><s:text name="jsp.account_226"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('NAME>>z')"><span class=sectionsubhead><s:text name="jsp.account_228"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectA('ALL')"><span class=sectionsubhead><s:text name="jsp.account_17"/></span></a>
</div>
<div id="payeeSummaryDiv">
   <s:include value="/pages/jsp/account/payees_grid.jsp"/>
</div>
<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />

<ffi:object id="EditRegisterPayee" name="com.ffusion.tasks.register.EditRegisterPayee" scope="session"/>
	<ffi:setProperty name="EditRegisterPayee" property="Validate" value="NAME" />
	<%
		session.setAttribute("FFIEditRegisterPayee", session.getAttribute("EditRegisterPayee"));
	%>
	
	
<script>	
					
		$('#register_payeesID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#register_payeesID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});
		<ffi:cinclude value1="${SetEditRegisterPayeeId}" value2="">
			$('#register_payeesID').portlet('title', js_acc_addpayee_portlet_title);
		</ffi:cinclude>	
		<ffi:cinclude value1="${SetEditRegisterPayeeId}" value2="" operator="notEquals">
			$('#register_payeesID').portlet('title', js_acc_editpayee_portlet_title);
			<ffi:removeProperty name="SetEditRegisterPayeeId" />
		</ffi:cinclude>
</script>
