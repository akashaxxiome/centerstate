<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web'/>/css/wires/wires.css" />
<div id="desktop" align="center">
   <div id="operationresult">
	   <div id="resultmessage"><s:text name="jsp.default_498"/></div>
   </div>
   <div id="appdashboard">
		<s:include value="/pages/jsp/account/fileupload_dashboard.jsp"/>
	</div>
   <div id="fileuploaddesktop">
       <s:include value="/pages/jsp/account/fileupload.jsp"/>
   </div>
</div>
<script type="text/javascript">
	
	$(document).ready(function(){
		
		//load import dialog if not loaded
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "false";
		ns.common.openDialog("fileImport");
		
		var configImportResults = ns.common.dialogs["fileImportResults"];
		configImportResults.autoOpen = "false";
		ns.common.openDialog("fileImportResults");	
		});

</script>