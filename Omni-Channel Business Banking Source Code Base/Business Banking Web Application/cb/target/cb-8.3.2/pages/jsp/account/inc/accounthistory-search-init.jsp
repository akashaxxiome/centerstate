<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
<%
	String collectionsName = request.getParameter("collectionsName");
	String accountIndex = request.getParameter("accountIndex");
	String setAccountID = request.getParameter("setAccountID");

	if(accountIndex != null) {
		session.setAttribute("collectionsName", collectionsName);
		session.setAttribute("accountIndex", accountIndex);
		session.setAttribute("setAccountID", setAccountID);

		if(session.getAttribute("Transactions" + accountIndex) != null) {
			Object o = session.getAttribute("Transactions" + accountIndex);
			session.setAttribute("Transactions", session.getAttribute("Transactions" + accountIndex));
		}
		if(session.getAttribute("SetAccount" + accountIndex) != null) {
			Object o2 = session.getAttribute("SetAccount" + accountIndex);
			session.setAttribute("SetAccount", session.getAttribute("SetAccount" + accountIndex));
		} 
%>

 
		<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
		<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
		<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
		<ffi:setProperty name="SetAccount" property="ID" value="<%= setAccountID %>"/>
		<ffi:process name="SetAccount"/>   
<%
	 }
%>
</ffi:cinclude> 
 