<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%
    String reportType = (String)session.getAttribute("report");
    String id = (String)session.getAttribute("id");
%>
<ffi:setProperty name="Account" property="ID" value="<%=id %>"/>
<ffi:setProperty name="reportType" value="<%=reportType %>"/>
<ffi:setProperty name="SetAccount" property="ID" value="" />
<ffi:setProperty name="GetCurrentDate" property="AdditionalDays" value="-30" />
<ffi:cinclude value1="${ReportDate}" value2="">
	<ffi:setProperty name="GetRegisterTransactions" property="FromDate" value="${GetCurrentDate.DateWithAdditionalDays}" />
	<ffi:setProperty name="GetRegisterTransactions" property="ToDate" value="${GetCurrentDate.Date}" />
	<ffi:setProperty name="ReportDate" value="${GetCurrentDate.Date}"/>
</ffi:cinclude>
<ffi:setProperty name="GetRegisterTransactions" property="IncludeUnreconciled" value="false" />
<ffi:setProperty name="GetRegisterTransactions" property="ReferenceNumber" value=""/>

<SCRIPT type="text/javascript">
	$(function(){
		$("#SetAccount\\.ID").selectmenu({width: 480});
		$("#reportType").selectmenu({width: 200});
		   });

	ns.account.generateReportSubmit = function(){
	   document.reportForm.report.value=document.reportForm.reportType.options[document.reportForm.reportType.selectedIndex].value;
		   }
<!--
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}
userAgent = window.navigator.userAgent;
browserVers = parseInt(userAgent.charAt(userAgent.indexOf("/")+1),10);
mustInitImg = true;
function initImgID() {di = document.images; if (mustInitImg && di) { for (var i=0; i<di.length; i++) { if (!di[i].id) di[i].id=di[i].name; } mustInitImg = false;}}
function findElement(n,ly) {
	d = document;
	if (browserVers < 4)		return d[n];
	if ((browserVers >= 6) && (d.getElementById)) {initImgID; return(d.getElementById(n))};
	var cd = ly ? ly.document : d;
	var elem = cd[n];
	if (!elem) {
		for (var i=0;i<cd.layers.length;i++) {
			elem = findElement(n,cd.layers[i]);
			if (elem) return elem;
		}
	}
	return elem;
}
function changeImages() {
	d = document;
	if (d.images) {
		var img;
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			img = null;
			if (d.layers) {img = findElement(changeImages.arguments[i],0);}
			else {img = d.images[changeImages.arguments[i]];}
			if (img) {img.src = changeImages.arguments[i+1];}
		}
	}
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta);
	}
	return result;
}
CSAct = new Object;
function CSOpenWindow(action) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	window.open(action[1],action[2],wf);
}
function CSGotoLink(action) {
	if (action[2].length) {
		var hasFrame=false;
		for(i=0;i<parent.frames.length;i++) { if (parent.frames[i].name==action[2]) { hasFrame=true; break;}}
		if (hasFrame==true)
			parent.frames[action[2]].location = action[1];
		else
			window.open (action[1],action[2],"");
	}
	else location = action[1];
}

// --></SCRIPT>

<SCRIPT type="text/javascript">
<!--
var preloadFlag = true;
CSAct[/*CMP*/ 'B9E5F385127'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=reportForm&calTarget=GetRegisterTransactions.FromDate"/>','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E5F385129'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=reportForm&calTarget=GetRegisterTransactions.ToDate"/>','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E979ED11'] = new Array(CSGotoLink,/*URL*/ 'accountreportingadv.htm','');
// -->
</SCRIPT>

<s:form id="accountRegisterReportform" name="reportForm" method="post" action="/pages/jsp/account/register-reports-report.jsp" theme="simple">
<div id="register_reportsID" align="center">
      <ffi:help id="account_register-reports" />
	  <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
      <input type="hidden" name="report">
      <table cellSpacing="6" cellPadding="0" width="100%" border="0">
        <TR>
          <TD class="sectionsubhead" valign="center" noWrap align="right"><s:text name="jsp.default_15"/></TD>

          <TD valign="center" noWrap align="left" colspan="2">
            <select class="txtbox" id="SetAccount.ID" name="SetAccount.ID">
				<%-- StringUtil element to set selected option attribute --%>
				<ffi:setProperty name="StringUtil" property="Value1" value="${Account.ID}" />
				<ffi:setProperty name="<%= com.ffusion.tasks.banking.Task.ACCOUNTS %>" property="Filter" value="REG_ENABLED=true"/>
				<ffi:list collection="<%= com.ffusion.tasks.banking.Task.ACCOUNTS %>" items="acct">
					<ffi:cinclude value1="${acct.TypeValue}" value2="14" operator="notEquals">
						<ffi:setProperty name="StringUtil" property="Value2" value="${acct.ID}" />
						<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value='<ffi:getProperty name="acct" property="ID"/>'>
							<ffi:getProperty name="acct" property="RoutingNum"/> :<ffi:getProperty name="acct" property="DisplayText"/><ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals">- <ffi:getProperty name="acct" property="NickName"/></ffi:cinclude>- <ffi:getProperty name="acct" property="CurrencyCode"/>
						</option>
					</ffi:cinclude>
				</ffi:list>
			</select>
          </TD>
          <TD noWrap align="left"></TD>

        </TR>
        <TR vAlign=top>
          <TD noWrap align="left" class="sectionsubhead">

          </TD>
          <TD>
          </TD>
          <TD noWrap align="right">

          </TD>
        </TR>
        <TR vAlign=top>
          <TD valign="center" noWrap align="right"><SPAN class="sectionsubhead"><s:text name="jsp.default_352"/></SPAN></TD>
          <TD valign="center" noWrap align="left">
            <%-- StringUtil element to set selected option attribute --%>
			<ffi:setProperty name="StringUtil" property="Value1" value="${reportType}"/>
            <SELECT class="txtbox" id="reportType" name=reportType>
                <ffi:setProperty name="StringUtil" property="Value2" value="cash"/>
                <OPTION value="cash" <ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_87"/></OPTION>
                <ffi:setProperty name="StringUtil" property="Value2" value="category"/>
                <OPTION value="category" <ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_89"/></OPTION>
                <ffi:setProperty name="StringUtil" property="Value2" value="payee"/>
                <OPTION value="payee" <ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_315"/></OPTION>
                <ffi:setProperty name="StringUtil" property="Value2" value="reconcile"/>
                <OPTION value="reconcile" <ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_341"/></OPTION>
                <ffi:setProperty name="StringUtil" property="Value2" value="tax"/>
                <OPTION value="tax" <ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_408"/></OPTION>
                <ffi:setProperty name="StringUtil" property="Value2" value="type"/>
                <OPTION value="type" <ffi:getProperty name="StringUtil" property="SelectedIfEquals" />><s:text name="jsp.default_440"/></OPTION>
            </SELECT>
          </TD>

          <TD noWrap align="left" class="sectionsubhead" width="70%"><s:text name="jsp.default_137"/>&nbsp;&nbsp;
			  <ffi:cinclude value1="${GetRegisterTransactions.FromDate}" value2="" operator="equals">
				<ffi:setProperty name="FromDate" value="${UserLocale.DateFormat}"/>
			  </ffi:cinclude>
			  <ffi:cinclude value1="${GetRegisterTransactions.FromDate}" value2="" operator="notEquals">
				<ffi:setProperty name="FromDate" value="${GetRegisterTransactions.FromDate}"/>
			  </ffi:cinclude>
			  <ffi:cinclude value1="${GetRegisterTransactions.ToDate}" value2="" operator="equals">
				<ffi:setProperty name="ToDate" value="${UserLocale.DateFormat}"/>
			  </ffi:cinclude>
			  <ffi:cinclude value1="${GetRegisterTransactions.ToDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ToDate" value="${GetRegisterTransactions.ToDate}"/>
			  </ffi:cinclude>
            <sj:datepicker value="%{#session.FromDate}" name="GetRegisterTransactions.FromDate1" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
            <SPAN class="sectionsubhead">&nbsp;<s:text name="jsp.default_423.1"/></SPAN>&nbsp;
            <sj:datepicker value="%{#session.ToDate}" name="GetRegisterTransactions.ToDate1" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
			  <ffi:removeProperty name="FromDate"/>
			  <ffi:removeProperty name="ToDate"/>
          </TD></TR>
        <tr>
        <td height="3"></td>
        <td height="3"></td>
        <td height="3"></td>
        </tr>
         <tr>
        <td height="3"></td>
        <td height="3"></td>
        <td height="3"></td>
        </tr>
        <TR vAlign="top">
          <TD noWrap align="right" colSpan=3>
            <DIV align="center">
				<script>
						ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
				</script>
                <sj:a
				    id="cancelReport"
					button="true"
					onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"
				><s:text name="jsp.default_82"/></sj:a>
			  <sj:a
				    formIds="accountRegisterReportform"
				    targets="accountRegisterDashbord"
					button="true"
					onclick="ns.account.generateReportSubmit()"
				><s:text name="jsp.default_224"/></sj:a>
			</DIV>
		  </TD>
		</TR>
		</table>


</div>
</s:form>

<script>

		$('#register_reportsID').portlet({
			generateDOM: true,
			helpCallback: function(){

				var helpFile = $('#register_reportsID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}

		});

			$('#register_reportsID').portlet('title', js_acc_report_portlet_title);

</script>