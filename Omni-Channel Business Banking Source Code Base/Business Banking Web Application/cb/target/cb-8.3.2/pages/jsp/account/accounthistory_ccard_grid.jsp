<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:help id="account_accounthistory_ccard" />

	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<%-- Set 'usePagination' property as true for using pagination, else old behavior of non-paginated grid will be maintained. --%>
		<ffi:setProperty name="usePagination" value="true"/>
	</ffi:cinclude>
		
	<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
			<%
				String accountIndex = request.getParameter("accountIndex");
				String setAccountID = request.getParameter("setAccountID");
			%>

			<ffi:setProperty name="accountIndex" value="<%= accountIndex %>"/>
			<ffi:setProperty name="setAccountID" value="<%= setAccountID %>"/>

			<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="All"/> --%>
	</ffi:cinclude>

	<s:include value="inc/account-history-wait.jsp"/>

	<ffi:cinclude value1="${accountIndex}" value2="" operator="equals">
		<%-- Set a default accountIndex variable value in session --%>
		<ffi:setProperty name="gridIndex" value="0"/>
		<ffi:setGridURL grid="GRID_accountHistoryCcard${gridIndex}" name="InquireURL" url="/cb/pages/jsp/account/sendAccountHistoryMessageAction_initAccountHistoryInquiry.action?collectionName=Transactions&tranIndex={0}&Subject=Account History Inquiry" parm0="accIndex"/> 
		<ffi:setGridURL grid="GRID_accountHistoryCcard${gridIndex}" name="ViewURL" url="/cb/pages/jsp/account/transactiondetail.jsp?collectionName=Transactions&tranIndex={0}" parm0="accIndex"/>
		<ffi:setGridURL grid="GRID_accountHistoryCcard${gridIndex}" name="LinkURL" url="/cb/pages/jsp/account/SearchImageAction.action?module=TRANSACTION&transID={0}" parm0="ID"/>
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
			<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetCreditCardAccountTransactionHistoryAction.action?collectionName=Transactions&GridURLs=GRID_accountHistoryCcard${gridIndex}" URLEncrypt="true"/> 
		</ffi:cinclude>
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
			<ffi:setProperty name="gridIndex" value="0"/>
			<ffi:setProperty name="tempURL" value="/pages/jsp/account/getCreditCardAccountHistory.action?accountIndex=0&collectionName=Transactions&GridURLs=GRID_accountHistoryCcard${gridIndex}&useBOImplementation=true" URLEncrypt="true"/>
		</ffi:cinclude>
    </ffi:cinclude>
    <ffi:cinclude value1="${accountIndex}" value2="" operator="notEquals">
		<ffi:setProperty name="gridIndex" value="${accountIndex}"/>
		<ffi:setGridURL grid="GRID_accountHistoryCcard${gridIndex}" name="InquireURL" url="/cb/pages/jsp/account/sendAccountHistoryMessageAction_initAccountHistoryInquiry.action?accountIndex=${accountIndex}&setAccountID=${setAccountID}&collectionName=Transactions&tranIndex={0}&Subject=Transaction Search Inquiry" parm0="accIndex"/>
		<ffi:setGridURL grid="GRID_accountHistoryCcard${gridIndex}" name="ViewURL" url="/cb/pages/jsp/account/transactiondetail.jsp?accountIndex=${accountIndex}&setAccountID=${setAccountID}&collectionName=Transactions&tranIndex={0}" parm0="accIndex"/>
		<ffi:setGridURL grid="GRID_accountHistoryCcard${gridIndex}" name="LinkURL" url="/cb/pages/jsp/account/TransactionSearchImageAction.action?accountIndex=${accountIndex}&setAccountID=${setAccountID}&module=TRANSACTION&transID={0}" parm0="ID"/>
	
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
			 <ffi:setProperty name="tempURL" value="/pages/jsp/account/GetCreditCardAccountTransactionHistoryAction.action?accountIndex=${accountIndex}&setAccountID=${setAccountID}&collectionName=Transactions&GridURLs=GRID_accountHistoryCcard${gridIndex}" URLEncrypt="true"/> 
		</ffi:cinclude>
		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
			<ffi:setProperty name="gridIndex" value="0"/>
			<ffi:setProperty name="tempURL" value="/pages/jsp/account/getCreditCardAccountHistory.action?accountIndex=0&setAccountID=${setAccountID}&collectionName=Transactions&GridURLs=GRID_accountHistoryCcard${gridIndex}&useBOImplementation=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<s:set var="gridIndex" value="%{#session.gridIndex}"/>

	<s:url id="accountHistoryCcardSummaryURL" value="%{#session.tempURL}" escapeAmp="false"/>

	 <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	 	<ffi:cinclude value1="${usePagination}" value2="true" operator="equals">
	 		<s:set var="gridPageSize" value="%{#session.StdGridRowNum}"/>
			<s:set var="gridRowList" value="%{#session.StdGridRowList}"/>
	 	</ffi:cinclude>
	 	<ffi:cinclude value1="${usePagination}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="gridPageSize" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.LARGE_PAGE_SIZE %>"/>
			<ffi:setProperty name="gridRowList" value=""/>
			<s:set var="gridPageSize" value="%{#session.gridPageSize}"/>
			<s:set var="gridRowList" value=""/>
	 	</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<s:set var="gridPageSize" value="%{#session.StdGridRowNum}"/>
		<s:set var="gridRowList" value="%{#session.StdGridRowList}"/>
	</ffi:cinclude>

	<sjg:grid
		id="ccard_accountHistorySummaryID_%{#gridIndex}"
		caption=""
		sortable="true"
		dataType="json"
		href="%{accountHistoryCcardSummaryURL}"
		pager="true"
		gridModel="gridModel"
		rowNum="%{#gridPageSize}"
		rowList="%{#gridRowList}"
		rownumbers="false"
		sortname="date"
		navigator="true"
	    navigatorAdd="false"
	    navigatorDelete="false"
	    navigatorEdit="false"
	    navigatorRefresh="false"
	    navigatorSearch="false"
	    navigatorView="false"
	    viewrecords="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		onGridCompleteTopics="addGridControlsEvents,addShowMemoButtonEvent,accountHistorySummaryEvent"
		>

		<sjg:gridColumn name="date" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="80" cssClass="datagrid_dateColumn"/>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<sjg:gridColumn name="dataClassification" index="dataClassification" title="" search="false" sortable="false" hidedlg="true" formatter="ns.account.formatDataClassificationColumn" width="20"/>
		</ffi:cinclude>
		<sjg:gridColumn name="type" index="type" title="%{getText('jsp.default_444')}" sortable="true" width="65"/>
		<sjg:gridColumn name="description" index="description" title="%{getText('jsp.default_170')}" sortable="true" width="200"  cssClass="datagrid_textColumn"/>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<sjg:gridColumn name="referenceNumber" index="referenceNumber" title="%{getText('jsp.account_175')}" align="right" sortable="true" formatter="ns.account.formatConsumerReferenceNumberColumn" width="120"/>
			<sjg:gridColumn name="debit" index="debit" title="%{getText('jsp.default_146')}" align="right" sortable="true" formatter="ns.account.formatAmountColumn" width="70"/>
			<sjg:gridColumn name="credit" index="credit" title="%{getText('jsp.default_120')}" align="right" sortable="true" formatter="ns.account.formatAmountColumn" width="70"/>
			<sjg:gridColumn name="runningBalance" index="runningBalance" title="%{getText('jsp.default_60')}" align="right" sortable="true" width="90" formatter="ns.account.formatRunningBalance"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<sjg:gridColumn name="debitNoSymbol" index="debitNoSymbol" title="%{getText('jsp.account_54')}" sortable="true" align="right" formatter="ns.account.formatAmountColumn" width="120"  cssClass="datagrid_textColumn"/>
			<sjg:gridColumn name="creditNoSymbol" index="creditNoSymbol" title="%{getText('jsp.default_318')}" sortable="true" align="right" formatter="ns.account.formatAmountColumn" width="120"  cssClass="datagrid_textColumn"/>
		</ffi:cinclude>
		<sjg:gridColumn name="transactionNum" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.account.formatAccountHistoryActionLinks" width="70" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		<sjg:gridColumn name="memo" index="memo" title="%{getText('jsp.default_279')}" sortable="false" hidedlg="true" hidden="true" width="60"  cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="ID" index="id" title="%{getText('jsp.account_97')}" sortable="false" width="70" hidden="true" hidedlg="true"  cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="InquireURL" index="InquireURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.accIndex" index="accIndex" title="%{getText('jsp.account_6')}" sortable="true" width="60" hidden="true" hidedlg="true"/>
	</sjg:grid>
	<br>
	<br>
	<script type="text/javascript">
	<!--		
		$("#ccard_accountHistorySummaryID_<ffi:getProperty name='gridIndex'/>").jqGrid('setColProp','transactionNum',{title:false});
	//-->
	</script>
	<%-- Hides Pagination for Consumer User --%>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:cinclude value1="${usePagination}" value2="true" operator="notEquals">
		<script type="text/javascript">
		<!--
			$( "#ccard_accountHistorySummaryID_<ffi:getProperty name='gridIndex'/>_pager_center" ).hide();
		//-->
		</script>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:removeProperty name="gridIndex"/>
	<ffi:removeProperty name="gridPageSize"/>
	<ffi:removeProperty name="gridRowList"/>