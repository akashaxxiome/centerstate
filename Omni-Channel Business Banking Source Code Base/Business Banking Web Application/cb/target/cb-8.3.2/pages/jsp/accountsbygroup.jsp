<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%
	session.setAttribute("AccountGroupID",request.getParameter("AccountGroupID"));
   session.setAttribute("AccountDisplayCurrencyCode",request.getParameter("AccountDisplayCurrencyCode"));
   session.setAttribute("AccountBalancesDataClassification",request.getParameter("DataClassificationVariable"));
   session.setAttribute("DataClassificationVariable",request.getParameter("DataClassificationVariable")); 
%>
<ffi:cinclude
	ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="true" />
</ffi:cinclude>
<ffi:cinclude
	ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="false" />
</ffi:cinclude>

<script type="text/javascript">
	$(document).ready(function(){
		ns.common.updatePortletTitle("conBalHolder","<ffi:getProperty name="AccountsType"/>",false);
		$('.customContainerCls').hide();
	});
	
    $(function(){
        $("#displayCurrencyCode").selectmenu({width: 200});
        });
	function changeAccountDisplayCurrency()
	{
		var AccountDisplayCurrencyCode = document.ChangeDisplayCurrencyForm.AccountDisplayCurrencyCode.options[document.ChangeDisplayCurrencyForm.AccountDisplayCurrencyCode.selectedIndex].value;
		var urlString = "/cb/pages/jsp/accountsbygroup.jsp?AccountGroupID="+"<ffi:getProperty name="AccountGroupID"/>"+"&changeAccountDisplayCurrency=true&DataClassificationVariable="+"<ffi:getProperty name="DataClassificationVariable"/>"+"&AccountDisplayCurrencyCode="+AccountDisplayCurrencyCode;
		$.ajax({
			url: urlString,
			success: function(data){
			    $('#querybord').html(data);
				$('#consolidatedBalanceSummary').html('');
				$.publish("common.topics.tabifyDesktop");					
				if(accessibility){
					accessibility.setFocusOnDashboard();
				}
		}
		});
	}
</script>

<div class="marginTop10 marginBottom10"><ffi:setProperty name="DataType"
	value="Summary" /> <ffi:setProperty name="AccountsCollectionName"
	value="BankingAccounts" /> <%-- We want to check the entitlements on only those accounts that will displayed by this page, so we will filter --%>
<ffi:setProperty name="BankingAccounts" property="Filter"
	value="ACCOUNTGROUP=${AccountGroupID}" /> <ffi:object
	id="CheckPerAccountReportingEntitlements"
	name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements"
	scope="request" /> <ffi:setProperty
	name="CheckPerAccountReportingEntitlements" property="AccountsName"
	value="BankingAccounts" /> <ffi:process
	name="CheckPerAccountReportingEntitlements" /> <%-- The specify criteria page sets variables and removes the variables set (and also the properties --%>
<%-- specified by those variables --%> <ffi:setProperty name="SubmitURL"
	value="/pages/jsp/accountsbygroup.jsp?AccountGroupID=${AccountGroupID}&subMenuSelected=${subMenuSelected}"
	URLEncrypt="flase" />
	<ffi:setProperty name="flag" value="accountbygroup"/>
	 <ffi:setProperty name="DataClassificationVariable"
	value="AccountBalancesDataClassification" /> <ffi:cinclude
	value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}"
	value2="true" operator="equals">
	<%-- we must determine the default data classification based on whether the user can see intra or previous day for any accounts--%>
	<ffi:cinclude value1="${AccountBalancesDataClassification}" value2=""
		operator="equals">
		<ffi:cinclude
			value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}"
			value2="true" operator="equals">
			<ffi:setProperty name="AccountBalancesDataClassification"
				value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" />
		</ffi:cinclude>
		<ffi:cinclude
			value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}"
			value2="false" operator="equals">
			<ffi:setProperty name="AccountBalancesDataClassification"
				value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" />
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${AccountBalancesDataClassification}"
		value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"
		operator="equals">
		<ffi:cinclude
			value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}"
			value2="true" operator="notEquals">
			<ffi:setProperty name="AccountBalancesDataClassification"
				value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" />
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${AccountBalancesDataClassification}"
		value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"
		operator="equals">
		<ffi:cinclude
			value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}"
			value2="true" operator="notEquals">
			<ffi:setProperty name="AccountBalancesDataClassification"
				value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" />
		</ffi:cinclude>
	</ffi:cinclude>


	<ffi:cinclude value1="${GetTargetCurrencies}" value2=""
		operator="equals">
		<ffi:object name="com.ffusion.tasks.fx.GetTargetCurrencies"
			id="GetTargetCurrencies" scope="session" />
		<ffi:process name="GetTargetCurrencies" />
	</ffi:cinclude>


	<ffi:cinclude value1="${GetDisplaySummariesForAccount}" value2=""
		operator="equals">
		<ffi:object
			name="com.ffusion.tasks.banking.GetDisplaySummariesForAccount"
			id="GetDisplaySummariesForAccount" scope="session" />
	</ffi:cinclude>
	<ffi:setProperty name="GetDisplaySummariesForAccount"
		property="DisplayCurrency" value="${AccountDisplayCurrencyCode}" />

	<style>
   		/* #ChangeDisplayCurrencyFormDivID {float: left; margin-left: 5px; margin-top: 5px; font-size: 10px;}
   		#querybord #reportOnDivID {float: right; margin-right: 5px;}
   		#querybord #reportOnDivID table{width: auto;}
   		#querybord #reportOnDivID table #reportOn-button{margin-left: 5px;} */
   		#specifycriterianodateDivID{vertical-align:bottom;}
   		#querybord { margin-left:10px;}

   	</style>
<div class="quickSearchAreaCls" style="float: none;" >
<div class="acntDashboard_masterItemHolder" >
	<ffi:cinclude value1="true" value2="${showselectcurrency}"
		operator="equals">
		<div id="ChangeDisplayCurrencyFormDivID" class="acntDashboard_itemHolder">
		<s:form name="ChangeDisplayCurrencyForm"
			method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		    <table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap align="left"><span class="sectionsubhead" >
					<s:text name="jsp.account_189"/>&nbsp;</span>
					<div style="margin-bottom:5px"></div>
					<select id="displayCurrencyCode" name="AccountDisplayCurrencyCode" class="txtbox" onchange="changeAccountDisplayCurrency();">
							<option <ffi:cinclude value1='' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value=''><s:text name="jsp.default_127"/></option>
							<ffi:list collection="FXCurrencies" items="currency">
								<option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value='<ffi:getProperty name="currency" property="CurrencyCode"/>'><ffi:getProperty name="currency" property="Description" /></option>
							</ffi:list>
						</select> 
						<!-- <br/> -->
					</td>
					<!-- <td align="left">
						<select id="displayCurrencyCode" name="AccountDisplayCurrencyCode" class="txtbox" onchange="changeAccountDisplayCurrency();">
							<option <ffi:cinclude value1='' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value=''><s:text name="jsp.default_127"/></option>
							<ffi:list collection="FXCurrencies" items="currency">
								<option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value='<ffi:getProperty name="currency" property="CurrencyCode"/>'><ffi:getProperty name="currency" property="Description" /></option>
							</ffi:list>
						</select> <br/>
					</td> -->
				</tr>
			</table>
		</s:form>
		</div>
	</ffi:cinclude>
	<div id="specifycriterianodateDivID"  class="acntDashboard_itemHolder">
		<s:include value="/pages/jsp/cash/inc/specifycriterianodate.jsp" />
	</div>
</div>	

</div>

	<%-- <span class="ui-helper-clearfix">&nbsp;</span> --%>
	<ffi:cinclude value1="${AccountGroupID}" value2="1" operator="equals">
		<s:include value="/pages/jsp/account/inc/accountsdeposit.jsp" />
	</ffi:cinclude>

	<ffi:cinclude value1="${AccountGroupID}" value2="2" operator="equals">
		<s:include value="/pages/jsp/account/inc/accountsasset.jsp" />
	</ffi:cinclude>

	<ffi:cinclude value1="${AccountGroupID}" value2="4" operator="equals">
		<s:include value="/pages/jsp/account/inc/accountsccard.jsp" />
	</ffi:cinclude>

	<ffi:cinclude value1="${AccountGroupID}" value2="3" operator="equals">
		<s:include value="/pages/jsp/account/inc/accountsloan.jsp" />
	</ffi:cinclude>

	<ffi:cinclude value1="${AccountGroupID}" value2="5" operator="equals">
		<s:include value="/pages/jsp/account/inc/accountsother.jsp" />
	</ffi:cinclude>
</ffi:cinclude> <ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<ffi:removeProperty name="AccountBalancesDataClassification" /> <ffi:removeProperty
	name="AccountDisplayCurrencyCode" /> <ffi:removeProperty
	name="DataClassificationVariable" /></div>
