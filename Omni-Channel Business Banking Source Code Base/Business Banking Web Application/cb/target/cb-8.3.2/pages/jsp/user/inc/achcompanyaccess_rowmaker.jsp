<%@ page import="com.ffusion.csil.beans.entitlements.Limit"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%@page import="com.ffusion.beans.util.LastRequest"%>
<ffi:setProperty name="Compare" property="Value1" value="TRUE"/>
<%     int indentLevelOffset = 0;
       int initCount=-1,idCount=0; 
	   String limit=null, limit_label=null, exceedLimit=null;
%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
<ffi:object id="GetRequireApprovalFromDA" name="com.ffusion.tasks.dualapproval.GetRequireApprovalFromDA" scope="session"/>
</ffi:cinclude>

<ffi:list collection="${collectionName}" items="EntType">
		<% String hideInitBox; %>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_HIDE_BOX %>"/>
		<ffi:getProperty name="EntType" property="Value" assignTo="hideInitBox"/>

        <% String indentLevel = "";
           boolean isSectionHeading = false;
        %>
        <ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
        <ffi:getProperty name="EntType" property="Value" assignTo="indentLevel"/>

        <ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
        <ffi:cinclude value1="${EntType.Value}" value2="0" operator="notEquals">
            <ffi:setProperty name="isSectionHeading" value="false"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.Value}" value2="0" operator="equals">
            <%-- indent level of 0 indicate the start of a new section --%>
            <%-- which requires the name of the Entitlement type to be bolded and a line to be draw across the from --%>
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.OperationName}" value2="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACH_BATCH %>" operator="equals">
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.OperationName}" value2="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.CCD_TXP %>" operator="equals">
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.OperationName}" value2="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.CCD_DED %>" operator="equals">
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${isSectionHeading}" value2="true" operator="equals">
            <% isSectionHeading = true; %>
        </ffi:cinclude>

		<ffi:setProperty name="hideEnt" value="false" />
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:cinclude value1="${hideInitBox}" value2="" operator="notEquals">
				<ffi:setProperty name="hideEnt" value="true" />
			</ffi:cinclude>
		</ffi:cinclude>
<ffi:cinclude value1="${hideEnt}" value2="false">
	<TR>

		<%	String classParam="columndata";
			String space = "";
			int indentLevelAsInt = 0;
			if ( indentLevel != null && !indentLevel.equals( "" ) ) {
				indentLevelAsInt = Integer.parseInt( indentLevel );
                if (isSectionHeading)
                    indentLevelOffset = indentLevelAsInt;
				if ( indentLevelAsInt > 0 ) {
					classParam = "indent" + Integer.toString( indentLevelAsInt - indentLevelOffset ) + " columndata";
				}
			}
			%>

		<ffi:cinclude value1="${isSectionHeading}" value2="true" operator="equals">
				<TD colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top"><hr noshade size="1"></TD>
			</TR>
			<TR>
		</ffi:cinclude>
		
		<ffi:setProperty name="EntType" property="CurrentProperty" value="form element name"/>
	   	<ffi:setProperty name="EntTypeLabel" value="${EntType.Value}"/>

		<%-- check if the admin checkbox for the entitlement type should be checked --%>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		<TD class="adminBackground" align="center" valign="middle">
		</ffi:cinclude>
		<% String hideAdminBox; %>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_HIDE_ADMIN_BOX %>"/>
		<ffi:getProperty name="EntType" property="Value" assignTo="hideAdminBox"/>

		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="equals">
			<%-- if no admin partner property is set --%>
			<ffi:setProperty name="LastRequest" property="Name" value="admin${EntTypeLabel}"/>
		        <ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.Value}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
			<ffi:removeProperty name="admin${EntTypeLabel}"/>
		        <input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
			&nbsp;
		</ffi:cinclude>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
			<%-- check if the entity is entitled to the admin partner --%>
			<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
				<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		        	<ffi:setProperty name="CheckEntitlementByGroup" property="OperationName" value="${EntType.Value}"/>
			       	<%-- needs to set objectID and objectType for specific ACH company --%>
				<ffi:cinclude value1="${collectionName}" value2="perACHEntLists" operator="equals">
					<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
					<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${ACHCompanyID}"/>
				</ffi:cinclude>
			       	<ffi:process name="CheckEntitlementByGroup"/>
			</s:if>
	    	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
				<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${EntType.Value}"/>
				<%-- needs to set objectID and objectType for specific ACH company --%>
				<ffi:cinclude value1="${collectionName}" value2="perACHEntLists" operator="equals">
					<ffi:setProperty name="CheckEntitlementByMember" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
					<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${ACHCompanyID}"/>
				</ffi:cinclude>
	            		<ffi:process name="CheckEntitlementByMember"/>
			</s:if>

			<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>

			<%-- display box if it should be visible, else put hidden input in the form --%>
			<% if( hideAdminBox == null || hideAdminBox.equals( "" ) ) { %>
				<ffi:setProperty name="LastRequest" property="Name" value="admin${EntTypeLabel}"/>
		        	<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.Value}"/>
				<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
				<ffi:removeProperty name="admin${EntTypeLabel}"/>
                        <ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="equals">
                        	<input type="<ffi:getProperty name="AdminCheckBoxType"/>" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0" <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( <ffi:getProperty name="ACHFormName"/>, this )"</ffi:cinclude>>
                        </ffi:cinclude>
                        <ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="notEquals">
                        	<input id="admin<%=++initCount%>" type="<ffi:getProperty name="AdminCheckBoxType"/>" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0" <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( <ffi:getProperty name="ACHFormName"/>, this )"</ffi:cinclude>>
                        </ffi:cinclude>
			<% } else { %>
				<ffi:setProperty name="LastRequest" property="Name" value="admin${EntTypeLabel}"/>
		        	<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.Value}"/>
				<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
				<ffi:removeProperty name="admin${EntTypeLabel}"/>
		                <input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
			 	&nbsp;
			<% } %>
		</ffi:cinclude>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		</TD>
		</ffi:cinclude>

		<%-- check if the init checkbox for the entitlement type should be checked --%>
		<TD class="adminBackground" align="center" valign="middle">
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	        	<ffi:setProperty name="CheckEntitlementByGroup" property="OperationName" value="${EntType.OperationName}"/>
	            	<%-- needs to set objectID and objectType for specific ACH company --%>
			<ffi:cinclude value1="${collectionName}" value2="perACHEntLists" operator="equals">
				<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
				<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${ACHCompanyID}"/>
			</ffi:cinclude>
	            	<ffi:process name="CheckEntitlementByGroup"/>
		</s:if>
    	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${EntType.OperationName}"/>
		   	<%-- needs to set objectID and objectType for specific ACH company --%>
			<ffi:cinclude value1="${collectionName}" value2="perACHEntLists" operator="equals">
				<ffi:setProperty name="CheckEntitlementByMember" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
				<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${ACHCompanyID}"/>
			</ffi:cinclude>
	            	<ffi:process name="CheckEntitlementByMember"/>
		</s:if>

	        <ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>

		<%-- display box if it should be visible, else put hidden input in the form --%>
		<% if( hideInitBox == null || hideInitBox.equals( "" ) ) { %>
			<ffi:setProperty name="LastRequest" property="Name" value="init${EntTypeLabel}"/>
		        <ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.OperationName}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
			<ffi:removeProperty name="init${EntTypeLabel}"/>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<ffi:process name="GetEntitlementsFromDA" />
			</ffi:cinclude>
                <ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="equals">
                    <input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( <ffi:getProperty name="ACHFormName"/>, this )"</ffi:cinclude>>
                </ffi:cinclude>
                <ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="notEquals">
	                <input id="init<%=initCount%>" type="checkbox" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( <ffi:getProperty name="ACHFormName"/>, this )"</ffi:cinclude>>
					<% limit="perBatch_limit"; limit_label="perBatch_limitLabel"; exceedLimit="perBatchExceed"; %>
				</ffi:cinclude>
		<% } else { %>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:setProperty name="LastRequest" property="Name" value="init${EntTypeLabel}"/>
			        <ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.OperationName}"/>
				<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
				<ffi:removeProperty name="init${EntTypeLabel}"/>
	        	        <input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
		 		&nbsp;
		 		</ffi:cinclude>
		<% } %>
		</TD>

<%
	    String[] periods 			= { "" + Limit.PERIOD_TRANSACTION,
    					 		  		"" + Limit.PERIOD_DAY,
								  		"" + Limit.PERIOD_TRANSACTION,
								  		"" + Limit.PERIOD_DAY};
    	String[] displaylimit 		= { com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_238", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()),
    						   	  		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_101", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()),
								  		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_239", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()),
								  		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_102", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale())};
    	String[] limitname 			= { "perBatchCredit",
    						   	  		"dailyCredit",
								  		"perBatchDebit",
								  		"dailyDebit"};
    	String[] approvelimit 		= { "approvePerBatchCredit",
    						   	  		"approveDailyCredit",
								  		"approvePerBatchDebit",
								  		"approveDailyDebit"};
		String[] creditdebitstrings = { "Credit",
                                        "Credit",
										"Debit",
                                        "Debit"};
%>

	<%-- we only need to display the limit if the init box is visible --%>
	<% if( hideInitBox == null || hideInitBox.equals( "" ) ) {
		boolean hasDebit = false;
		boolean hasCredit = false;%>
		<%-- set up the variables for display limits --%>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.DEBIT_NAME %>"/>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
			<% hasDebit = true; %>
		</ffi:cinclude>

		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CREDIT_NAME %>"/>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
			<% hasCredit = true; %>
		</ffi:cinclude>

		<ffi:setProperty name="GetMaxACHLimitForPeriod" property="ObjectID" value="${EntType.OperationName}"/>

		<% int count = 0;		// if count = 2, make a new row
		   boolean isFirst = true;
		   String displayValue;
		   for (int j = 0; j < displaylimit.length; j++) {
			 displayValue = displaylimit[j];
			 if (isFirst) {
				 isFirst = false; %>
		<ffi:removeProperty name="DARequest"/>
		<TD class='<ffi:getPendingStyle fieldname="${EntType.OperationName}"
			defaultcss='<%=( isSectionHeading ? "sectionsubhead" : classParam ) %>'
						dacss='<%=classParam + " columndataDA" %>'
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'
		valign="middle" style="text-align: left;" <%=( !hasCredit && !hasDebit ? "colspan='7'" : "" )%> nowrap>
             		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:getProperty name="EntType" property="Value"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="notEquals">
						<ffi:getProperty name="EntType" property="OperationName"/>
					</ffi:cinclude>
				</TD>
				
				<%-- Requires Approval Processing for Per Account Permissions --%>
				<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="OperationName" value="${EntType.OperationName}" />	
				<ffi:process name="CheckRequiresApprovalPerAcct" /> 

				<%-- Requires Approval Processing --%>
			<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
			 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
				<ffi:setProperty name="CheckRequiresApproval" property="OperationName" value="${EntType.OperationName}" />
				<ffi:setProperty name="GetRequireApprovalFromDA" property="operationName" value="${EntType.OperationName}"/>
				<ffi:process name="CheckRequiresApproval" />
				<ffi:process name="GetRequireApprovalFromDA" />
				<TD align="center" valign="middle">&nbsp;&nbsp;
					<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="true" operator="equals">

					<%-- When user selects RA and go ahead to verify page and clicks Back button then
					we should retain selected values--%>
					<ffi:setProperty name="LastRequest" property="Name" value="req_appr_${CheckRequiresApproval.OpCounter}"/>
				    <ffi:setProperty name="LastRequest" property="CheckboxValue" value="${CheckRequiresApproval.OperationName}"/>
					<ffi:cinclude value1="${CheckRequiresApproval.Enabled}" value2="">
						<ffi:setProperty name="LastRequest" property="DisabledDefault" value="false"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${CheckRequiresApproval.Enabled}" value2="" operator="notEquals">
						<ffi:setProperty name="LastRequest" property="DisabledDefault" value="true"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${CheckRequiresApproval.Selected}" value2="">
						<ffi:setProperty name="Compare" property="Value2" value="FALSE"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${CheckRequiresApproval.Selected}" value2="" operator="notEquals">
						<ffi:setProperty name="Compare" property="Value2" value="TRUE"/>
					</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
						<input <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> type="checkbox"
							value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>"
						 	onclick="handleRAClick(this.form,this);if(document.getElementById('RequiresApprovalModified')) {document.getElementById('RequiresApprovalModified').value='true';}"

						  	  <ffi:cinclude value1="${CheckRequiresApproval.Enabled}" value2="" operator="notEquals">
								<ffi:getProperty name="CheckRequiresApproval" property="Enabled"/>
							 </ffi:cinclude>
							 <ffi:cinclude value1="${LastRequest.DisabledDefault}" value2="" operator="notEquals">
								<ffi:getProperty name="LastRequest" property="DisabledDefault"/>
							 </ffi:cinclude>

							 <ffi:cinclude value1="${LastRequest.Checked}" value2="">
							  <ffi:cinclude value1="${CheckRequiresApproval.Enabled}" value2="" operator="notEquals">
								<ffi:getProperty name="CheckRequiresApproval" property="Selected"/>
							  </ffi:cinclude>
							 </ffi:cinclude>
							 <ffi:cinclude value1="${LastRequest.Checked}" value2="" operator="notEquals">
								<ffi:getProperty name="LastRequest" property="Checked"/>
							 </ffi:cinclude>
							 <%-- Check if we are processing view only mode --%>
							 <ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="equals">
								 		disabled
							 </ffi:cinclude>
						 />

					<ffi:setProperty name="EditRequiresApproval" property="ReqApprOpName" value="${EntType.OperationName}" />
					<ffi:setProperty name="DisableExceed" value="${LastRequest.Checked}" />

					<ffi:setProperty name="LastRequest" property="DisabledDefault" value="false"/>
					<ffi:setProperty name="LastRequest" property="Name" value="init${EntTypeLabel}"/>
			        <ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.OperationName}"/>

						<%-- Set display properties into javascript --%>
					<ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="notEquals">
						<script type="text/javascript">
							appr_names['req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />'] = 'init<ffi:getProperty name="EntTypeLabel"/>';
							init_names['init<ffi:getProperty name="EntTypeLabel"/>'] = 'req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />';
						</script>
					</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="false" operator="equals">
						&nbsp;
					</ffi:cinclude>
				</TD>
				</ffi:cinclude>
			</ffi:cinclude>
		<%-- END Requires Approval Processing --%>
			 <% }
			 if (limitname[j].indexOf("Credit") > 0 && !hasCredit)
			 	continue;
			 if (limitname[j].indexOf("Debit") > 0 && !hasDebit)
			 	continue;
			 count++;
			 if (count == 3) { %>
				</TR>
				<TR>
					<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
						<TD class="adminBackground" colspan="3"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
					</ffi:cinclude>
					<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
						<TD class="adminBackground" colspan="2"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
					</ffi:cinclude>
			 <% } %>
				<ffi:getProperty name="EntType" property="OperationName" assignTo="displayValue"/>
				<ffi:setProperty name="GetGroupLimits" property="OperationName" value="<%=displayValue%>"/>
					<ffi:setProperty name="GetGroupLimits" property="Period" value="<%=periods[j]%>"/>
					<ffi:process name="GetGroupLimits"/>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${EntType.OperationName}"/>
					<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="<%=periods[j]%>"/>
					<ffi:process name="GetLimitsFromDA" />
				</ffi:cinclude>
				<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
				<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
				<ffi:getProperty name="EntTypeLabel" assignTo="displayValue"/>
				<% displayValue = limitname[j] + displayValue; %>
				<ffi:removeProperty name="<%=displayValue%>"/>
				<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1" >
					<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
						<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
						<ffi:setProperty name="<%=displayValue%>" value="${LimitItem.Data}"/>
				</ffi:list>

				<ffi:setProperty name="LastRequest" property="Name" value="<%=displayValue%>"/>

				<TD align="center" valign="middle">
			       <ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="equals">
						<INPUT disabled class="ui-widget-content ui-corner-all" type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
					</ffi:cinclude>
					<ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="notEquals">
				        <INPUT id="<%= limit %><%=initCount%>" class="ui-widget-content ui-corner-all" type="text" style="width:85px;" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
						<% limit = "perDay_limit"; %>
					</ffi:cinclude>
			    </TD>
			    <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				    <ffi:cinclude value1="<%=String.valueOf(j) %>" value2="0">
				    	<ffi:cinclude value1="<%=String.valueOf(hasCredit) %>" value2="true">
						    <ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y">
							    <TD class="columndataDA" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    	<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals">
							    <TD class="columndata" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    </ffi:cinclude>
			    	 </ffi:cinclude>

					<ffi:cinclude value1="<%=String.valueOf(j) %>" value2="1">
						<ffi:cinclude value1="<%=String.valueOf(hasCredit) %>" value2="true">
						    <ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y">
							    <TD class="columndataDA" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    	<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals">
							    <TD class="columndata" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    </ffi:cinclude>
				    </ffi:cinclude>
				    <ffi:cinclude value1="<%=String.valueOf(j) %>" value2="2">
						<ffi:cinclude value1="<%=String.valueOf(hasDebit) %>" value2="true">
						    <ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y">
							    <TD class="columndataDA" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    	<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals">
							    <TD class="columndata" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    </ffi:cinclude>
				    </ffi:cinclude>
				    <ffi:cinclude value1="<%=String.valueOf(j) %>" value2="3">
					  <ffi:cinclude value1="<%=String.valueOf(hasDebit) %>" value2="true">
						    <ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y">
							    <TD class="columndataDA" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    	<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals">
							    <TD class="columndata" valign="middle" align="left" nowrap>
									<%=displaylimit[j]%>
								</TD>
					    	</ffi:cinclude>
					    </ffi:cinclude>
				    </ffi:cinclude>
			    </ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				  <TD id="<%=limit_label%><%=initCount%>" class="columndata" valign="middle" align="left" nowrap>
						<%limit_label="perDay_limitLabel";%>
						<%=displaylimit[j]%>
					</TD>
				</ffi:cinclude>

				<TD align="center" valign="middle">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
				<ffi:getProperty name="EntTypeLabel" assignTo="displayValue"/>
				<% displayValue = approvelimit[j] + displayValue; %>
							<ffi:setProperty name="LastRequest" property="Name" value="<%=displayValue %>"/>
							<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
							<ffi:removeProperty name="<%=displayValue%>"/>

							<INPUT id="<%=exceedLimit%><%=initCount%>" type="checkbox" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/>
							value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>"
							<% exceedLimit="perDayExceed"; %>
							<ffi:getProperty name="LastRequest" property="Checked"/>
					<%-- Requires Approval Relation Processing --%>
							<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
								<ffi:cinclude value1="${DisableExceed}" value2="checked ">
								disabled
								</ffi:cinclude>

								<ffi:cinclude value1="${CheckRequiresApproval.Enabled}" value2="" operator="notEquals">
								<ffi:cinclude value1="${CheckRequiresApproval.Selected}" value2="checked">
								 disabled
								</ffi:cinclude>
							   </ffi:cinclude>

							</ffi:cinclude>
							<%-- Check if we are processing view only mode --%>
								<ffi:cinclude value1="${permissionsViewOnly}" value2="true" operator="equals">
								 		disabled
							 	</ffi:cinclude>
								<ffi:cinclude value1="${CheckRequiresApprovalPerAcct.ValidReqApprOperation}" value2="true" operator="equals"> 
									<ffi:cinclude value1="${CheckRequiresApprovalPerAcct.Selected}" value2="checked">
										disabled
									</ffi:cinclude>
								</ffi:cinclude>

					<%--END Requires Approval Relation Processing --%>
							>
							<% space = ""; %>
					<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
					 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
							<script type="text/javascript">
								var tmp_arr = ach_ra_limits['req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />'];
								if(tmp_arr) {
									tmp_arr[tmp_arr.length] = '<ffi:getProperty name="LastRequest" property="Name" />';
								} else {
									ach_ra_limits['req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />']
										=new Array('<ffi:getProperty name="LastRequest" property="Name" />');
								}
							</script>
					  </ffi:cinclude>
					 </ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>

				<ffi:setProperty name="NumColumnsToSpan" value="4"/>
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
					<ffi:setProperty name="NumColumnsToSpan" value="3"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<ffi:setProperty name="NumColumnsToSpan" value="2"/>
				</ffi:cinclude>

				<% if( count == 2 ) {
					if( hasCredit && hasDebit ) { %>

						<TR>
						<%-- Requires Approval Processing --%>
							<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
							 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
							 <TD align="center" valign="middle">
							 	&nbsp;
							 </TD>
							 </ffi:cinclude>
							</ffi:cinclude>
							<TD class="adminBackground" colspan="<ffi:getProperty name="NumColumnsToSpan"/>"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
							<TD colspan="2">
								<span class="columndata">
									<ffi:setProperty name="GetMaxACHLimitForPeriod" property="ObjectType" value="Credit"/>
									(<ffi:getProperty name="GetMaxACHLimitForPeriod" property="TransactionLimitDisplay"/>)
								</span>
							</TD>
							<TD class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
							<TD colspan="2">
								<span class="columndata">
									<ffi:setProperty name="GetMaxACHLimitForPeriod" property="ObjectType" value="Debit"/>
									(<ffi:getProperty name="GetMaxACHLimitForPeriod" property="TransactionLimitDisplay"/>)
								</span>
							</TD>
						</TR>
					<% } else if( hasCredit || hasDebit ) { %>
						<TR>
						<%-- Requires Approval Processing --%>
							<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
							 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
							 <TD align="center" valign="middle">
							 	&nbsp;
							 </TD>
							 </ffi:cinclude>
							</ffi:cinclude>
							<ffi:setProperty name="GetMaxACHLimitForPeriod" property="ObjectType" value="<%= creditdebitstrings[ j ] %>"/>
							<TD class="adminBackground" colspan="<ffi:getProperty name="NumColumnsToSpan"/>"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
							  <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							    <ffi:cinclude value1="<%=String.valueOf(j) %>" value2="1">
							    	<ffi:cinclude value1="<%=String.valueOf(hasCredit) %>" value2="true">
									    <td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerTransaction" /></span></td>
								    </ffi:cinclude>
						    	 </ffi:cinclude>
								 <ffi:cinclude value1="<%=String.valueOf(j) %>" value2="3">
									<ffi:cinclude value1="<%=String.valueOf(hasDebit) %>" value2="true">
										<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerTransaction" /></span></td>
									</ffi:cinclude>
								 </ffi:cinclude>
						    </ffi:cinclude>
					    	<TD style="text-align: left;"><span class="columndata">(<ffi:getProperty name="GetMaxACHLimitForPeriod" property="TransactionLimitDisplay"/>)</span></TD>
							<TD class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:cinclude value1="<%=String.valueOf(j) %>" value2="1">
									<ffi:cinclude value1="<%=String.valueOf(hasCredit) %>" value2="true">
								    	<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerDay" /></span></td>
							    	</ffi:cinclude>
						    	</ffi:cinclude>
						    	<ffi:cinclude value1="<%=String.valueOf(j) %>" value2="3">
									<ffi:cinclude value1="<%=String.valueOf(hasDebit) %>" value2="true">
										<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerDay" /></span></td>
									</ffi:cinclude>
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<TD class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
							</ffi:cinclude>
							<TD colspan="2" style="text-align: left;"><span class="columndata">(<ffi:getProperty name="GetMaxACHLimitForPeriod" property="DailyLimitDisplay"/>)</span></TD>
						</TR>
					<% }
				   } else if( count == 4 ) { %>
					<TR>
					<%-- Requires Approval Processing --%>
						<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
						 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
						 <TD align="center" valign="middle">
						 	&nbsp;
						 </TD>
						 </ffi:cinclude>
						</ffi:cinclude>
						<TD class="adminBackground" colspan="<ffi:getProperty name="NumColumnsToSpan"/>"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
						<TD colspan="2">
							<span class="columndata">
								<ffi:setProperty name="GetMaxACHLimitForPeriod" property="ObjectType" value="Credit"/>
								(<ffi:getProperty name="GetMaxACHLimitForPeriod" property="DailyLimitDisplay"/>)
							</span>
						</TD>
						<TD class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
						<TD colspan="2">
							<span class="columndata">
								<ffi:setProperty name="GetMaxACHLimitForPeriod" property="ObjectType" value="Debit"/>
								(<ffi:getProperty name="GetMaxACHLimitForPeriod" property="DailyLimitDisplay"/>)
							</span>
						</TD>
					</TR>
				<% } %>
				<ffi:removeProperty name="NumColumnsToSpan"/>

	<% } // end For Statement %>
	<ffi:removeProperty name="DisableExceed"/>

	<%-- closing the if statement for the displayInitBox check --%>
	<% } else { %>
		  <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:cinclude value1="${isSectionHeading}" value2="true" operator="equals">
				<TD class="sectionsubhead" valign="middle" colspan="7" nowrap style="text-align:left;">
			</ffi:cinclude>
			<ffi:cinclude value1="${isSectionHeading}" value2="false" operator="equals">
				<TD class='<%=isSectionHeading?"sectionsubhead":classParam %>' valign="middle" colspan="7" nowrap style="text-align:left;">
			</ffi:cinclude>
				<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
				<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
					<ffi:getProperty name="EntType" property="Value"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="notEquals">
					<ffi:getProperty name="EntType" property="OperationName"/>
				</ffi:cinclude>
			</TD>
		</ffi:cinclude>
	<% } %>
	</TR>
		<% String errorEntTypeLabel = null; %>
		<ffi:getProperty name="EntTypeLabel" assignTo="errorEntTypeLabel"/>
		<% errorEntTypeLabel = errorEntTypeLabel.replace('(', '_');
			errorEntTypeLabel = errorEntTypeLabel.replace(')', '_');
		%>
		<ffi:setProperty name="NumColumnsToSpan" value="3"/>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
			<ffi:setProperty name="NumColumnsToSpan" value="2"/>
		</ffi:cinclude>
		<TR>
			<TD colspan="<ffi:getProperty name="NumColumnsToSpan"/>">&nbsp;</TD>
				<TD colspan="6" valign="middle" align="left" class="errortext">
					<span id='limit_errorCredit<%=errorEntTypeLabel%>Error'></span>
					<span id='limit_errorDebit<%=errorEntTypeLabel%>Error'></span>
				</TD>
			</TR>
		<ffi:removeProperty name="limit_errorCredit${EntTypeLabel}"/>
		<ffi:removeProperty name="limit_errorDebit${EntTypeLabel}"/>
		<ffi:removeProperty name="NumColumnsToSpan"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${hideEnt}" value2="false" operator="notEquals">

		<% String hideAdminBox; %>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="form element name"/>
		<ffi:setProperty name="LastRequest" property="Name" value="admin${EntType.Value}"/>
		<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.Value}"/>
		<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
	    <input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
	    <ffi:setProperty name="LastRequest" property="Name" value="init${EntType.Value}"/>
		<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${EntType.Value}"/>
		<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
	    <input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
	</ffi:cinclude>
</ffi:list>
<ffi:removeProperty name="isSectionHeading"/>
<ffi:removeProperty name="EntTypeLabel"/>
<ffi:removeProperty name="hasCredit"/>
<ffi:removeProperty name="hasDebit"/>
<ffi:removeProperty name="allowApproval"/>
<ffi:removeProperty name="creditError"/>
<ffi:removeProperty name="debitError"/>

<%-- Remove the session attributes for the limit textboxes --%>
<%
	java.util.Enumeration enumer = session.getAttributeNames();
	while( enumer.hasMoreElements() ) {
		String name = (String)(enumer.nextElement());
		if( name.startsWith("perBatchCredit") || name.startsWith("perBatchDebit") || name.startsWith("dailyCredit") || name.startsWith("dailyDebit") ) {
			session.removeAttribute(name);
		} //if
	} //while
%>