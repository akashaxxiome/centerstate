<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- This file was removed from accountaccess.jsp to keep the page from getting too big to compile --%>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>"/>
</ffi:cinclude>
<%-- Dual Approval Processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<s:if test="%{#session.Section == 'Users'}">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${Business.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Business.Id}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
					<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>
					<ffi:setProperty name="EditAccountAccessPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
				</ffi:cinclude>
		</s:if>
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
					<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>
					<ffi:setProperty name="EditAccountAccessPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
					<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
				</ffi:cinclude>
				<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountAccessPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountAccessPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountAccessPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountAccessPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditAccountAccessPermissions" name="com.ffusion.tasks.admin.EditAccountAccessPermissions" scope="session"/>
			</ffi:cinclude>
			<ffi:setProperty name="EditAccountAccessPermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="EditAccountAccessPermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="EditAccountAccessPermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		</s:if>

		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditAccountAccessPermissions" property="ProfileId" value="${Business.Id}"/>
		</ffi:cinclude>
	
		<ffi:setProperty name="EditAccountAccessPermissions" property="GroupId" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="EditAccountAccessPermissions" property="SaveLastRequest" value="false"/>
    	<ffi:setProperty name="EditAccountAccessPermissions" property="AccountsName" value="FilteredAccounts"/>
      <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
	</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
    <ffi:setProperty name="GetGroupLimits" property="GroupId" value="${EditGroup_GroupId}"/>
    <ffi:setProperty name="GetGroupLimits" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
    <ffi:setProperty name="GetGroupLimits" property="ObjectType" value="Account"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>

<ffi:cinclude value1="${AccountTypesList}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.ResourceList" id="AccountTypesList" scope="session"/>
		<ffi:setProperty name="AccountTypesList" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources"/>
		<ffi:setProperty name="AccountTypesList" property="ResourceID" value="AccountTypesList"/>
	<ffi:process name="AccountTypesList" />
	<ffi:getProperty name="AccountTypesList" property="Resource"/>
</ffi:cinclude>

<ffi:cinclude value1="${runSearch}" value2="">
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:process name="GetRestrictedEntitlements"/>
		<ffi:removeProperty name="GetRestrictedEntitlements"/>

		<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
		<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="ParentsAccounts"/>
		<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
		<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
		<ffi:process name="GetAccountsForGroup"/>
	</s:if>
	<s:else>
		<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
		<ffi:process name="GetEntitlementGroup"/>

		<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:process name="GetRestrictedEntitlements"/>
		<ffi:removeProperty name="GetRestrictedEntitlements"/>

		<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
		<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${Entitlement_EntitlementGroup.ParentId}"/>
		<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="ParentsAccounts"/>
		<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
		<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
		<ffi:process name="GetAccountsForGroup"/>
	</s:else>

	<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="ParentsAccounts"/>
	
	<ffi:cinclude value1="${SortOperation}" value2="true" operator="notEquals">
	<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="ParentsAccounts"/>
	<ffi:process name="SearchAcctsByNameNumType" />
	<ffi:setProperty name="SearchResultsPage" value="/pages/jsp/user/accountaccess.jsp"/>	
	</ffi:cinclude>
	
	<ffi:removeProperty name="SortOperation"/>
	
</ffi:cinclude>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<ffi:object name="com.ffusion.tasks.admin.GetMaxAccountAccessLimits" id="GetMaxAccountAccessLimits" scope="session"/>
<ffi:setProperty name="GetMaxAccountAccessLimits" property="AccountsName" value="FilteredAccounts"/>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
    <ffi:setProperty name="GetMaxAccountAccessLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
</s:if>
<s:elseif test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
    <ffi:setProperty name="GetMaxAccountAccessLimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
    <ffi:setProperty name="GetMaxAccountAccessLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="GetMaxAccountAccessLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="GetMaxAccountAccessLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:elseif>
<ffi:setProperty name="GetMaxAccountAccessLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxAccountAccessLimits" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxAccountAccessLimits" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxAccountAccessLimits" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxAccountAccessLimits"/>
<ffi:removeProperty name="GetMaxAccountAccessLimits"/>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="FilteredAccounts"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="EntitlementPrefix" value="account"/>
<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}user/accountaccess-verify-init.jsp?PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}user/accountaccess.jsp?UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
		<ffi:process name="GetDACategoryDetails" />

		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
		<ffi:process name="GetDACategoryDetails" />
</ffi:cinclude>

<ffi:object id="GetEntitlementTypePropertyList" name="com.ffusion.efs.tasks.entitlements.GetEntitlementTypePropertyList"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="ListName" value="Access"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="TypeToLookup" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
<ffi:process name="GetEntitlementTypePropertyList"/>

<ffi:object id="HandleAccountAccessRowDisplay" name="com.ffusion.tasks.admin.HandleAccountAccessRowDisplay"/>
<ffi:setProperty name="HandleAccountAccessRowDisplay" property="AccountsName" value="FilteredAccounts"/>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
    <ffi:setProperty name="HandleAccountAccessRowDisplay" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
</s:if>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
    <ffi:setProperty name="HandleAccountAccessRowDisplay" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
    <ffi:setProperty name="HandleAccountAccessRowDisplay" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="HandleAccountAccessRowDisplay" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="HandleAccountAccessRowDisplay" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<ffi:process name="HandleAccountAccessRowDisplay"/>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object id="CheckEntByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
	<ffi:setProperty name="CheckEntByMember" property="GroupId" value="${BusinessEmployee.EntitlementGroupMember.EntitlementGroupId}"/>
	<ffi:setProperty name="CheckEntByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckEntByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CheckEntByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
</s:if>
<s:elseif test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:object id="CheckEntByGroup" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup"/>
</s:elseif>

<ffi:object name="com.ffusion.tasks.admin.GetEntitlementObjectID" id="GetEntitlementObjectID"/>
