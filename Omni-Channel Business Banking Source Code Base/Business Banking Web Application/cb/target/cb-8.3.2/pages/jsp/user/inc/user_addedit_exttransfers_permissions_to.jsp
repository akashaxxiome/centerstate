<%-- This page should only be included by jsp/user/inc/user_addedit_exttransfers_permissions.jsp. --%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<%
String IsObjectEntitled = null;
String ObjectLimitValue = null;
String ErrorMessage = null;
String ErrorsDetected = null;
%>

                        <tr class="header">
                            <td><div align="center"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-2" parm0="${EnableOperationDisplayValue}"/></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;">Account</td>
                            <td colspan="2">Limit Per Transaction</td>
                            <td colspan="2">Limit Per Day</td>
                        </tr>
    <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
    <ffi:setProperty name="Calculator" property="Value1" value="3"/>
    <ffi:setProperty name="Calculator" property="Value2" value="1"/>

    <ffi:setProperty name="HasAccount" value=""/>

    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementOperationName" value="${ToOperationName}"/>
    <ffi:setProperty name="${TransferAccessAndLimitsTaskName}" property="EntitlementOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_TO %>'/>

    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="AllowApproval" value="FALSE"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="OperationName" value="${ToOperationName}"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>

    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
        <ffi:setProperty name="AccountsToFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsToFilters.Value}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="CheckParentEntitlements" property="GroupId" value="${SecureUser.EntitlementID}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="CheckParentEntitlements" property="OperationName" value="${ToOperationName}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:process name="CheckParentEntitlements"/>
            
            <%-- QTS 756842. If primary user is not entitled to account setup, don't show accounts. --%>
			<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_SETUP %>' checkParent="TRUE">
				<ffi:setProperty name="IsParentEntitled" value="FALSE"/>
			</ffi:cinclude>

            <ffi:removeProperty name="IsObjectEntitled"/>
            <ffi:cinclude value1="${IsParentEntitled}" value2="TRUE" operator="equals">
                <ffi:setProperty name="${TransferAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:removeProperty name="IsObjectEntitled"/>
                <ffi:getProperty name="${TransferAccessAndLimitsTaskName}" property="IsEntitledObject" assignTo="IsObjectEntitled"/>

                <ffi:cinclude value1="${AccountsCollectionName}" value2="ExternalTransferAccounts" operator="equals">
                    <ffi:setProperty name="TempIsObjectEntitled" value="true"/>
                    <ffi:getProperty name="TempIsObjectEntitled" assignTo="IsObjectEntitled"/>
                </ffi:cinclude>
            </ffi:cinclude>

            <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="equals">
                <ffi:setProperty name="HasAccount" value="true"/>

                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddObjectIDToList" value='${GetAccountEntitlementObjectID.ObjectID}'/>

                <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
                <ffi:setProperty name="FAALMLSessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_MAX_LIMIT_PREFIX %>'/>
                <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
                <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${ToOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="FAALMLBaseName" value="${FAALMLSessionPrefix}!${ToOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>

                <ffi:setProperty name="${TransferAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>

                        <tr style="padding: 0;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:removeProperty name="IsObjectEntitled"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="IsEntitledObject" assignTo="IsObjectEntitled"/>
                            <td><div align="center"><input enabledisable="true" name='<ffi:getProperty name="FAALBaseName"/>' type="checkbox" value="TRUE" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="equals">checked</ffi:cinclude>></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;"><ffi:getProperty name="TempAccount" property="ConsumerDisplayText"/></td>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                            <td><input inputfor='<ffi:getProperty name="FAALBaseName"/>' name='<ffi:getProperty name="FAALBaseName"/>!<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>' type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="15" value="<ffi:getProperty name='ObjectLimitValue'/>" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="notEquals">disabled</ffi:cinclude>></td>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>

                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${TransferAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:getProperty name="${TransferAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>

                <ffi:setProperty name="MaxLimit" value=""/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="${FAALMLBaseName}!${TempPeriod}!OperationName"/>
                <ffi:removeProperty name="${FAALMLBaseName}!${TempPeriod}!GroupType"/>

                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <ffi:setProperty name="Comparator" property="Value1" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                        <ffi:setProperty name="Comparator" property="Value2" value="${ObjectLimitValue}"/>

                        <ffi:cinclude value1="${Comparator.Greater}" value2="true" operator="equals">
                            <ffi:setProperty name="MaxLimit" value="${Comparator.Value2}"/>
                            <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!OperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'/>
                            <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!GroupType" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.ENT_GROUP_USER_MEMBER %>'/>
                        </ffi:cinclude>
                        <ffi:cinclude value1="${Comparator.Greater}" value2="true" operator="notEquals">
                            <ffi:setProperty name="MaxLimit" value="${Comparator.Value1}"/>
                        </ffi:cinclude>
                    </ffi:cinclude>
                </ffi:cinclude>

                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="equals">
                        <ffi:setProperty name="MaxLimit" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                    </ffi:cinclude>
                </ffi:cinclude>

                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="equals">
                    <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <ffi:setProperty name="MaxLimit" value="${ObjectLimitValue}"/>
                        <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!OperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'/>
                        <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!GroupType" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.ENT_GROUP_USER_MEMBER %>'/>
                    </ffi:cinclude>
                </ffi:cinclude>

                <ffi:cinclude value1="${MaxLimit}" value2="" operator="notEquals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value="${MaxLimit}"/>
                            <td style="padding-left: 5px;"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-3" parm0="${MaxLimit}"/></td>
                </ffi:cinclude>

                <ffi:cinclude value1="${MaxLimit}" value2="" operator="equals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value=""/>
                            <td style="padding-left: 5px;">(no maximum)</td>
                </ffi:cinclude>

                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                            <td><input inputfor='<ffi:getProperty name="FAALBaseName"/>' name='<ffi:getProperty name="FAALBaseName"/>!<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>' type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="15" value="<ffi:getProperty name='ObjectLimitValue'/>" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="notEquals">disabled</ffi:cinclude>></td>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>

                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${TransferAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:getProperty name="${TransferAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>

                <ffi:setProperty name="MaxLimit" value=""/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="${FAALMLBaseName}!${TempPeriod}!OperationName"/>
                <ffi:removeProperty name="${FAALMLBaseName}!${TempPeriod}!GroupType"/>

                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <ffi:setProperty name="Comparator" property="Value1" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                        <ffi:setProperty name="Comparator" property="Value2" value="${ObjectLimitValue}"/>

                        <ffi:cinclude value1="${Comparator.Greater}" value2="true" operator="equals">
                            <ffi:setProperty name="MaxLimit" value="${Comparator.Value2}"/>
                            <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!OperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'/>
                            <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!GroupType" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.ENT_GROUP_USER_MEMBER %>'/>
                        </ffi:cinclude>
                        <ffi:cinclude value1="${Comparator.Greater}" value2="true" operator="notEquals">
                            <ffi:setProperty name="MaxLimit" value="${Comparator.Value1}"/>
                        </ffi:cinclude>
                    </ffi:cinclude>
                </ffi:cinclude>

                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="equals">
                        <ffi:setProperty name="MaxLimit" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                    </ffi:cinclude>
                </ffi:cinclude>

                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="equals">
                    <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <ffi:setProperty name="MaxLimit" value="${ObjectLimitValue}"/>
                        <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!OperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'/>
                        <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}!GroupType" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.ENT_GROUP_USER_MEMBER %>'/>
                    </ffi:cinclude>
                </ffi:cinclude>

                <ffi:cinclude value1="${MaxLimit}" value2="" operator="notEquals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value="${MaxLimit}"/>
                            <td style="padding-left: 5px;"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-3" parm0="${MaxLimit}"/></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${MaxLimit}" value2="" operator="equals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value=""/>
                            <td style="padding-left: 5px;">(no maximum)</td>
                </ffi:cinclude>
                        </tr>

                <ffi:object name="com.ffusion.beans.util.StringList" id="ErrorMessages" scope="request"/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALMLBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALMLBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:cinclude value1="${ErrorMessages.Size}" value2="0" operator="notEquals">
                        <tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                            <td></td>
                            <td colspan="5" style="padding-left: 20px; padding-right: 20px;">
                                <div class="limitCheckError">
                                    <br>
                    <ffi:list collection="ErrorMessages" items="TempErrorMessage">
                                    <ffi:getProperty name="TempErrorMessage"/><br>
                    </ffi:list>
                                    <br>
                                </div>
                            </td>
                        </tr>
                </ffi:cinclude>

                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
    <ffi:cinclude value1="${HasAccount}" value2="" operator="equals">
                        <tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                            <td colspan="6"><div style="text-align: center"><ffi:getProperty name="NoAccountsMessage"/></div></td>
                        </tr>
    </ffi:cinclude>

