<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>


<ffi:help id="user_corpadminlocdelete-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_89')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="notEquals">
	<ffi:object id="DeleteLocation" name="com.ffusion.tasks.cashcon.DeleteLocation"/>
	<ffi:setProperty name="DeleteLocation" property="LocationBPWID" value="${DeleteLocation_LocationBPWID}"/>
	<ffi:process name="DeleteLocation"/>
	<ffi:removeProperty name="DeleteLocation"/>
</ffi:cinclude>


<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">

	<ffi:object id="AddLocationToDA" name="com.ffusion.tasks.dualapproval.AddLocationToDA" scope="session"/>
	<ffi:setProperty name="AddLocationToDA" property="CategoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>
	<ffi:setProperty name="AddLocationToDA" property="UserAction" value="<%= String.valueOf( com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.USER_ACTION_DELETED ) %>"/>
	<ffi:setProperty name="AddLocationToDA" property="ItemId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="AddLocationToDA" property="objectId" value="${DeleteLocation_LocationBPWID}"/>
	<ffi:setProperty name="AddLocationToDA" property="objectType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>
	<ffi:setProperty name="AddLocationToDA" property="childSequence" value="${childSequence}"/>

	<%-- sendForApproval property will decide message to be shown on this page --%>
	<ffi:process name="AddLocationToDA"/>
	<ffi:removeProperty name="AddLocationToDA"/>
	<ffi:removeProperty name="childSequence"/>

</ffi:cinclude>

<ffi:removeProperty name="DeleteLocation_LocationBPWID"/>
<div align="center">
	<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center">
							
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">									
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocdelete-confirm.jsp-1" parm0="${DeleteLocation_LocationName}"/>
							
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">									
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocdelete-confirm.jsp-2" parm0="${DeleteLocation_LocationName}"/>
								</ffi:cinclude>
							
							</TD>
						</TR>
						<TR>
							<TD align="center" class="ui-widget-header customDialogFooter">
								<sj:a button="true"
									  onClickTopics="closeDialog,closeDeleteLocationDialog" title="%{getText('Close_Window')}" 
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_175" /></sj:a>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		</div>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminlocations-pre.jsp"/>
