<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="userprefs_menus"/>
<script src="<s:url value='/web'/>/js/toggles.js" type="text/javascript"></script>
<style type="text/css" media="print">
	#displayTabs, #hiddenTabs { float: none;}
</style>

<input type="hidden" name="CSRF_TOKEN" id="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="ffivisible"  style="max-height:400px; overflow-y:auto; width:100%;">	
	<div id="siteNavigation" style="text-align:left">
		<table width="85%" border="0" cellpadding="0" cellspacing="0" align="center">
                 <%--  <tr>
                     <td>
                        <span><s:text name="jsp.user_120"/></span>
                    </td>
                 </tr> --%>
                 <tr>
                    <td valign="top">
                         <div id="displayTabs" class="connectedSortable ui-widget ui-widget-content ui-corner-all">
                         	<!-- <div class="menu-item-excluded" style="height:0px"></div> -->
                           	<ffi:list collection="DisplayedMenuTabs" items="Tab">
								<ffi:cinclude value1="${Tab.Key}" value2="" operator="notEquals">
									<ffi:cinclude value1="${Tab.Key}" value2="preferences" operator="notEquals">
										<div id="<ffi:getProperty name='Tab' property='Key'/>" class="ui-state-hover">
											<span class="sapUiIconCls icon-sort"></span>
											<span>
												<a href="#">
												<ffi:getProperty name="Tab" property="Value"/>
												</a>
											</span>
											<%-- <span class="ui-icon ui-icon-home" style="display:inline-block"></span> --%>
												<div class="toggle-dark" style="float: right;">
													<div class="toggle on"></div>
												</div>
										</div>
									</ffi:cinclude>
								</ffi:cinclude>
                           	</ffi:list>
                           	
                           	<ffi:list collection="HiddenMenuTabs" items="Tab">
							
								<div id="<ffi:getProperty name='Tab' property='Key'/>" class="ui-state-hover">
										<span class="sapUiIconCls icon-sort"></span>
										<span>
											<a href="#">
											<ffi:getProperty name="Tab" property="Value"/>
											</a>
										</span>
										<%-- <span class="ui-icon ui-icon-home" style="display:inline-block"></span> --%>
											<div class="toggle-dark" style="float: right;">
												<div class="toggle on"></div>
											</div>
								</div>
								
                           </ffi:list>
                           
                           
                         </div>
                         <div class="default-page-note" style="float:left;"><span class="sapUiIconCls icon-home"></span> <i><s:text name="jsp.user.sitenavigation.defaultpage" /></i></div>
                    </td>
                    <%--<td width="10%" valign="middle" align="center">&nbsp;
                    </td>
                     <td width="35%" valign="top">
                  		<div id="hiddenTabs" class="connectedSortable ui-widget ui-widget-content ui-corner-all" style="height:170px">
                        	<!-- <div class="menu-item-excluded" style="height:0px"></div> -->
                        	<ffi:list collection="HiddenMenuTabs" items="Tab">
                           	<div id="<ffi:getProperty name='Tab' property='Key'/>" class="ui-state-highlight ui-corner-all">
                           		<span class="ui-icon ui-icon-arrow-2-n-s" style="height:0px"></span>
                           			<span>
										<a href="#">
											<ffi:getProperty name="Tab" property="Value"/>
										</a>	
                           			</span>
							</div>
                           </ffi:list>
                       	</div>
                    </td> --%>
                 </tr>
             </table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center" colspan="3" width="100%">&nbsp;
					<div id="menuPreferenceButton" class="ui-widget-header customDialogFooter">
						<sj:a id="cancelUserpreferenceButtonId" button="true" onclick="ns.common.closeDialog('siteNavigationDialogId');" >
							<s:text name="jsp.default_82"/>
						</sj:a>
						<%-- <sj:a id="saveMenuPrefButton" onclick="ns.userPreference.saveMenuPreference();" button="true"/> --%> 
						<sj:a id="saveMenuPrefButton" button="true">
							<s:text name="jsp.default_366"/></sj:a>
					</div>
	            </td>
			</tr>
		</table>
	</div>
</div>
	<!-- this element hold the start page value -->	
	<input type="hidden" id="startPageSelect">

<span id="resultmessage" style="display:none">
	<s:text name="jsp.user_390"/>
</span>

	<script type="text/javascript">
	
	function setTargetAsDefault(event) {
		$("#displayTabs div").find("span.icon-home").remove();
		$(event.target).parent().append( $("<span class='sapUiIconCls icon-home'></span>") );
		
		var show_nav = jQuery.i18n.prop("site_navigation_show");
		var hide_nav = jQuery.i18n.prop("site_navigation_hide");
		//Assigning new value to startpage variable
		startPage=$(event.target).parent().attr('id');
		$('#startPageSelect').val(startPage);
		
		$("#displayTabs div.ui-state-hover").each(function(){
			if($(event.target).attr('id')!='home'){
				$(this).find(".toggle-dark > div.disabledToggleBtn").removeClass('disabledToggleBtn').addClass('toggle on').css('width', '0').toggles({text:{on:show_nav,off:hide_nav}, 'type': 'select', 'width': 70, 'height': 22});
			}
		});
		
		if($(event.target).parent().find(".toggle-dark > div").hasClass("prevSaveDisabledToggleBtn")){
			$(this).parent().find(".toggle-dark > div").removeClass('prevSaveDisabledToggleBtn');
		}
		$(event.target).parent().find(".toggle-dark > div").addClass("disabledToggleBtn");
		$('.disabledToggleBtn').css('width', '0').toggles({text:{on:show_nav,off:hide_nav}, 'drag': false, 'click':false, 'type': 'select', 'width': 70, 'height': 22});
		
		$(event.target).parent().find( "span.set-default-home" ).remove();
	};
	
		$(document).ready(function()
		{
			$('#startPageSelect').val('<s:property value="%{#session.StartPage}"/>');
			$("#displayTabs, #hiddenTabs").sortable({
				connectWith: ".connectedSortable",
				items: "div:not(.menu-item-excluded)",
				cancel: ".menu-item-canceled",
				placeholder: 'ui-state-active',
				receive: function(event, ui)
				{
					var container = ui.item.parent();
					var moveItem = ui.item;
					var startPageValue = $selectCore.val();
					if (container.attr("id") == 'displayTabs') {
						var icon = moveItem.children('.ui-icon:first');
						if (icon) {
							icon.remove();
						}
						moveItem.removeClass('ui-state-highlight');
						moveItem.addClass('ui-state-hover');
					} else if (container.attr("id") == 'hiddenTabs') {
						//moveItem.prepend($('<span class="ui-icon ui-icon-arrow-2-n-s" style="float:left" />'));
						moveItem.removeClass('ui-state-hover');
						moveItem.addClass('ui-state-highlight');
					}
					moveItem.find("a").focus();
					$selectCore.children().remove();
					$selectCore.width(0).css('hight', '0').css('opacity', '0').show();

					var matched = false;
					$('#displayTabs').children('.ui-state-hover').each(function()
					{
						var optVal = $(this).attr('id').substr(0, $(this).attr('id').length);
						var optText = $(this).find('span').html();

						if (optVal == startPageValue) {
							$selectCore.append('<option value="' + optVal + '" selected>' + optText + '</option>');
						} else {
							$selectCore.append('<option value="' + optVal + '" >' + optText + '</option>');
						}
					});
					$selectCore.selectmenu('destroy');
					$selectCore.selectmenu({width: '20em'});
					$.publish("common.topics.tabifyNotes");
				}
			}).disableSelection();

			/* $('#siteNavigation').portlet({
				generateDOM: true,
				title: js_site_nevigation}); */
			
			// Assigning the startpage variable for default home page
			var startPage; 
			
			startPage= $('#startPageSelect').val();
			if(startPage != "home"){
				$("#"+startPage).find('.toggle-dark > div').removeClass('toggle');
				$("#"+startPage).find('.toggle-dark > div').addClass('disabledToggleBtn');
			}
			
			//Preferences menu should not have Show/Hide options, as it will be always visible. 
			$("#preferences").find('.toggle-dark > div').removeClass('toggle');
			$("#preferences").find('.toggle-dark > div').hide();
			
			
			$("#"+startPage).append( $("<span class='sapUiIconCls icon-home'></span>") );
			//Set Home toggle buttons disabled
			$("#displayTabs > div").each(function(){
				//tabsToDisplay.push($(this).attr('id'));
				if($(this).attr('id') == 'home'){
					$(this).find('.toggle-dark > div').css('width', '0').removeClass('toggle').addClass('defaultDisabled');
				}
			});
			
			var defLinkHolderRef = "";
			if(ns.home.isDevice) {
				$("#displayTabs div.ui-state-hover").on("click", function() {
					// removing the set default home link form the previous holder
					if(defLinkHolderRef) {
						$(defLinkHolderRef).find( "span.set-default-home" ).remove();
					}
					defLinkHolderRef = this;
					showDefaultLink(this);
				}); 
			} else {
				$("#displayTabs div.ui-state-hover").hover(function(){
						showDefaultLink(this);
				    }, function(){
				    	$(this).find( "span.set-default-home" ).remove();
				});
			}
			
			function showDefaultLink(param) {
				if(($(param).children('span.icon-home').length == 0) && ($(param).children('span.set-default-home').length == 0) ){								
					//Preferences menu should not have set as Default option, as subitems in this menu appear in dialog.
					if($(param).prop('id')!='preferences'){
						$( param ).append( $("<span class='set-default-home' onclick='setTargetAsDefault(event)'>(set as default test)</span>") );
					}
				} 
			}
			
			/* $("span.set-default-home").click(function(){ */
			
							/* if(($(this).children('span.icon-home').length == 0) && ($(this).children('span.set-default-home').length == 0) ){								
								//Preferences menu should not have set as Default option, as subitems in this menu appear in dialog.
								if($(this).prop('id')!='preferences'){
									$( this ).append( $("<span class='set-default-home'>(set as default)</span>") );
								}
							 } 
							$("span.set-default-home").click(function(){
								$("#displayTabs div").find("span.icon-home").remove();
								$(this).parent().append( $("<span class='sapUiIconCls icon-home'></span>") );
								
								//Assigning new value to startpage variable
								startPage=$(this).parent().attr('id');
								$('#startPageSelect').val(startPage);
								
								$("#displayTabs div.ui-state-hover").each(function(){
									if($(this).attr('id')!='home'){
										$(this).find(".toggle-dark > div.disabledToggleBtn").removeClass('disabledToggleBtn').addClass('toggle on').css('width', '0').toggles({text:{on:jQuery.i18n.prop("site_navigation_show"),off:jQuery.i18n.prop("site_navigation_hide")}, 'type': 'select', 'width': 70, 'height': 22});
									}
								});
								
								if($(this).parent().find(".toggle-dark > div").hasClass("prevSaveDisabledToggleBtn")){
									$(this).parent().find(".toggle-dark > div").removeClass('prevSaveDisabledToggleBtn');
								}
								$(this).parent().find(".toggle-dark > div").addClass("disabledToggleBtn");
								$('.disabledToggleBtn').css('width', '0').toggles({text:{on:jQuery.i18n.prop("site_navigation_show"),off:jQuery.i18n.prop("site_navigation_hide")}, 'drag': false, 'click':false, 'type': 'select', 'width': 70, 'height': 22});
								
								
								$(this).parent().find( "span.set-default-home" ).remove();
								
							});
							}, function() {
								$(this).find( "span.set-default-home" ).remove();
				}
			); */
			
			
			// Creating array with all tabs
			//var tabsToDisplay = [];
			 /* var tabsToDisplay = ["acctMgmt","pmtTran","approvals","reporting"];  */
			var test = '<s:property value="%{tabs}"/>';
			
			var tabsToDisplay = new Array();
			tabsToDisplay = test.split(", ");
			
			var divId="";
			 var client =  new Array();
			 $("#displayTabs > div").each(function(){
					divId= $(this).attr('id');
					client.push(divId);
			 });
			 
			 for(var r=0;r<client.length;r++){
				
				 var str1 = String($.trim(client[r]));
				 var str2 = String($.trim(tabsToDisplay));
				 var lFoundIndex = str2.indexOf(str1);
				 
				 //console.log("JJ"+);
			 	
				 if(lFoundIndex == -1) {
					  if(!($("#"+client[r]).find('.toggle-dark > div').hasClass('disabledToggleBtn')))
						{
					    	$("#"+client[r]).find('.toggle-dark > div').addClass('prevSaveDisabledToggleBtn');
							
						}
				 }
				 else {
					  //Hiding...
				 }
			 }
			 
			addToggleSwitchBtn();
			
			//alert(tabsToDisplay);
			$('.toggle').on('toggle', function (e, active) {
				if($(this).hasClass('prevSaveDisabledToggleBtn') && $(this).find('.toggle-off').hasClass('active')){
					$(this).removeClass('prevSaveDisabledToggleBtn');
				}
				
			    if (active) {
			        var removeTab = $(this).parent().parent().attr('id');
			        //tabsToDisplay.remove(addTab);
			       
			        tabsToDisplay.splice($.inArray(removeTab, tabsToDisplay),1);
			        //alert(tabsToDisplay);
			    } else {
			    	var addTab = $(this).parent().parent().attr('id');
			    	var divPos = $(this).parent().parent().index();
			    	tabsToDisplay.splice(divPos, 0, addTab);
			    	tabsToDisplay.join();
			    	//alert(tabsToDisplay);
			    }
			});
			
			
			$('.toggle, .disabledToggleBtn, .defaultDisabled').droppable({
			    drop: function(event, ui) {
			        ui.draggable.draggable("disable", 1); // *not* ui.draggable("disable", 1);
			    }
			});
			$('.toggle, .disabledToggleBtn, .defaultDisabled').draggable({ disabled: false });
			$('.toggle, .disabledToggleBtn, .defaultDisabled').on('drag', function() {
			    return false;
			  });
			//$("#displayTabs div.ui-state-hover").last().css('border','none');
			$("#saveMenuPrefButton").click(function(){
				while (tabsToDisplay.length > 0) {
						tabsToDisplay.pop();
				    }
				$("#displayTabs > div").each(function(){
					if($(this).find('.toggle-dark > div .toggle-off ').hasClass('active')){
						tabsToDisplay.push($(this).attr('id').trim());
					}
					
				});
				
				//Always show Preferences tab
				tabsToDisplay.push("preferences");
				
				// Get tabs to be displayed as decided by logged in user
				var ids = tabsToDisplay
				// Get start page value selected by User.
				var startPageValue = $('#startPageSelect').val();	

				// Construct URL and Post data using AJAX call
				var urlString = '/cb/pages/jsp/user/QuickEditPreferenceAction_updateMenuSettings.action';
				$.ajax({
					type: "POST",
					url: urlString,
					dataType: 'JSON',
					data: {
						tabsToDisplay:ids,
						startPage:startPageValue,
						CSRF_TOKEN: document.getElementById('CSRF_TOKEN').value	},
					success: function(data) {
						$.publish('quickSaveSiteNavigationSetting');
						
						/* $("#resultDialog").dialog({
							   beforeClose: function(event, ui) {
								   // Reload/refresh whole page to apply changes.
								   ns.userPreference.reloadUserPreference();
								}
						});
						$("#resultDialog").dialog('open');
						$("#resultDialog").attr("style", "min-height:0px"); */
					}
				});
				return false;
				
				return false;
			});
		});
		
		function addToggleSwitchBtn(){
			var show_nav = jQuery.i18n.prop("site_navigation_show");
			var hide_nav = jQuery.i18n.prop("site_navigation_hide");
			$('.toggle').toggles({text:{on:show_nav,off:hide_nav}, 'type': 'select', 'width': 70, 'height': 22});
			$('.disabledToggleBtn').toggles({text:{on:show_nav,off:hide_nav}, 'drag': false, 'click':false,'type': 'select',  'width': 70, 'height': 22});
			$('.defaultDisabled').toggles({text:{on:show_nav,off:hide_nav}, 'drag': false, 'click':false,'type': 'select',  'width': 70, 'height': 22});
			if($("#displayTabs > div").find('.toggle-dark > div').hasClass('prevSaveDisabledToggleBtn')){
				$('.prevSaveDisabledToggleBtn').trigger('click');
			}
			
		}
		
	</script>