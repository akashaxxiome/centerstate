<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_clone-user-permissions-init" className="moduleHelpClass"/>
<div id='PageHeading' style="display:none;"><s:text name="jsp.user_71"/></div>

<%
	session.setAttribute("AddUser", request.getParameter("AddUser"));
	session.setAttribute("TargetBusinessEmployeeId", session.getAttribute("BusinessEmployeeId"));
%>

<ffi:setProperty name="BackURL" value="${SecurePath}user/permissions.jsp?Section=Users&BusinessEmployeeId=${TargetBusinessEmployeeId}" URLEncrypt="true"/>
<ffi:removeProperty name="SourceBusinessEmployeeId"/>

<%-- initializing if necessary --%>
<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="%{#session.PagesPath}user/inc/admin-init.jsp" />
</ffi:cinclude>

<ffi:cinclude value1="${OneAdmin}" value2="" operator="equals">
	<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="request"/>
		<ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
	<ffi:process name="GetAdministratorsForGroups"/>

	<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
	<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="request"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
	<ffi:process name="GetBusinessEmployeesByEntGroups"/>
	<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
	<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
	<% int AdminCount = 0; %>
	<ffi:list collection="BusinessEmployees" items="admin">
	<% AdminCount++; %>
	</ffi:list>
	<% if( AdminCount == 1 ) { %>
		<ffi:setProperty name="OneAdmin" value="TRUE"/>
	<% } else { %>
		<ffi:setProperty name="OneAdmin" value="FALSE"/>
	<% }  %>
</ffi:cinclude>

<%-- If entry is from add user need to refresh BusinessEmployees to include the new user --%>
<ffi:cinclude value1="${AddUser}" value2="TRUE" operator="equals">
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>

	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="request" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:process name="GetBusinessEmployees"/>

	<ffi:removeProperty name="SearchBusinessEmployee"/>
</ffi:cinclude>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${TargetBusinessEmployeeId}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetSourceBusinessEmployeesForMemberClone" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId" scope="request"/>
<ffi:setProperty name="GetSourceBusinessEmployeesForMemberClone" property="EntitlementGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
<ffi:setProperty name="GetSourceBusinessEmployeesForMemberClone" property="BusinessEmployeesSessionName" value="BusinessEmployeesForClone"/>
<ffi:process name="GetSourceBusinessEmployeesForMemberClone"/>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="request" />
<ffi:list collection="BusinessEmployeesForClone" items="BusinessEmployee1">
	<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${BusinessEmployee1.EntitlementGroupId}"/>
	<ffi:process name="GetEntitlementGroup"/>
	<ffi:setProperty name="BusinessEmployee1" property="Group" value="${Entitlement_EntitlementGroup.GroupName}"/>
</ffi:list>

<ffi:object id="GetSourceEntitlementProfileForMemberClone" name="com.ffusion.tasks.business.GetBusinessEntitlementProfilesByGroupId" scope="request"/>
<ffi:setProperty name="GetSourceEntitlementProfileForMemberClone" property="EntitlementGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
<ffi:setProperty name="GetSourceEntitlementProfileForMemberClone" property="EntitlementProfilesSessionName" value="EntitlementProfilesForClone"/>
<ffi:process name="GetSourceEntitlementProfileForMemberClone"/>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="request" />
<ffi:list collection="EntitlementProfilesForClone" items="profile1">
	<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${profile1.EntitlementGroupId}"/>
	<ffi:process name="GetEntitlementGroup"/>
	<ffi:setProperty name="profile1" property="Group" value="${Entitlement_EntitlementGroup.GroupName}"/>
</ffi:list>


<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" scope="request"/>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${TargetBusinessEmployeeId}"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeeSessionName" value="TargetBusinessEmployee"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="BusinessEmployeesForClone"/>
<ffi:process name="SetBusinessEmployee"/>


<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:object id="ClonePermissions" name="com.ffusion.efs.tasks.entitlements.ClonePermissions" scope="session"/>
	<ffi:setProperty name="ClonePermissions" property="CloneAutoEntitement" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneGroupAdminEntitlement" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneLimit" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneEntitlement" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneEntitlementAdminEntitlement" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object id="ClonePermissions" name="com.ffusion.tasks.dualapproval.ClonePermissionsDA" scope="session"/>
	<ffi:setProperty name="ClonePermissions" property="CloneAutoEntitement" value="false"/>
	<ffi:setProperty name="ClonePermissions" property="CloneGroupAdminEntitlement" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneLimit" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneEntitlement" value="true"/>
	<ffi:setProperty name="ClonePermissions" property="CloneEntitlementAdminEntitlement" value="false"/>
</ffi:cinclude>
<ffi:setProperty name="ClonePermissions" property="CloneType" value="<%=String.valueOf(com.ffusion.efs.tasks.entitlements.ClonePermissions.GROUP_MEMBER_CLONE)%>"/>
<ffi:setProperty name="ClonePermissions" property="SourceUserSessionName" value="SourceBusinessEmployee"/>
<ffi:setProperty name="ClonePermissions" property="TargetUserSessionName" value="TargetBusinessEmployee"/>

<ffi:setProperty name="ClonePermissions" property="AdminedGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="BusinessEmployeesForClone"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="SourceBusinessEmployeeId"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="TargetBusinessEmployeeId"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="EntitlementProfilesForClone"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="runSearch"/>
<ffi:setProperty name="ClonePermissions" property="SuccessURL" value="${SecurePath}user/permissions.jsp?Section=Users&BusinessEmployeeId=${TargetBusinessEmployeeId}&OneAdmin=${OneAdmin}" URLEncrypt="true"/>

<ffi:setProperty name="BackURL" value="${ClonePermissions.SuccessURL}" URLEncrypt="false"/>

<s:include value="/pages/jsp/user/clone-user-permissions.jsp" />