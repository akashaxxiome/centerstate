<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:object id="EditAccountGroupAccessPermissionsDA" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>

<ffi:setProperty name="EditAccountGroupAccessPermissionsDA" property="approveAction" value="true" />

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditAccountGroupAccessPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditAccountGroupAccessPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditAccountGroupAccessPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditAccountGroupAccessPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="EditAccountGroupAccessPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>

<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
<ffi:setProperty name="approveCategory" property="categorySubType" value="" />

<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
<ffi:setProperty name="GetCategories" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>"/>
<ffi:removeProperty name="categories" />
<ffi:process name="GetCategories" />

<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:process name="EditAccountGroupAccessPermissionsDA" />
	<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
		<ffi:setProperty name="ApprovePendingChanges" property="CategorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>"/>
		<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
		<ffi:process name="ApprovePendingChanges"/>
	</ffi:list>

</ffi:cinclude>

<ffi:removeProperty name="EditAccountGroupAccessPermissionsDA" />