<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<%-- DETERMINE WHETHER TO DISPLAY ACH OR NOT --%>
	<ffi:setProperty name="displayACH" value="false"/>
	<%-- check ACH Batch --%>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" id="checkAchEntitlement"/>
		<ffi:setProperty name="checkAchEntitlement" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.REC_ACH_BATCH %>"/>
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
		</s:if>
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<s:if test="%{#session.UsingEntProfiles == 'true'}">
				<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				<ffi:setProperty name="checkAchEntitlement" property="MemberId" value="${BusinessEmployeeId}"/>
				<ffi:setProperty name="checkAchEntitlement" property="UsingEntProfiles" value="true"/>
				<ffi:setProperty name="checkAchEntitlement" property="ChannelId" value="${ChannelId}"/>
			</s:if>
		</s:if>
		<s:if test="%{#session.Section == 'UserProfile'}">
			<ffi:setProperty name="checkAchEntitlement" property="ChannelGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		</s:if>
	<ffi:process name="checkAchEntitlement"/>
	<ffi:cinclude value1="${checkAchEntitlement.Entitled}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayACH" value="true"/>
	</ffi:cinclude>
	<%-- check ACH File Upload --%>
	<ffi:setProperty name="checkAchEntitlement" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_FILE_UPLOAD %>"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<s:if test="%{#session.UsingEntProfiles == 'true'}">
				<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				<ffi:setProperty name="checkAchEntitlement" property="MemberId" value="${BusinessEmployeeId}"/>
			</s:if>
	</s:if>
	<s:if test="%{#session.Section == 'UserProfile'}">
			<ffi:setProperty name="checkAchEntitlement" property="ChannelGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		</s:if>
	<ffi:process name="checkAchEntitlement"/>
	<ffi:cinclude value1="${checkAchEntitlement.Entitled}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayACH" value="true"/>
	</ffi:cinclude>
	<%-- check Child Support --%>
	<ffi:setProperty name="checkAchEntitlement" property="OperationName" value="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.CHILD_SUPPORT %>"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<s:if test="%{#session.UsingEntProfiles == 'true'}">
				<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				<ffi:setProperty name="checkAchEntitlement" property="MemberId" value="${BusinessEmployeeId}"/>
			</s:if>
	</s:if>
	<s:if test="%{#session.Section == 'UserProfile'}">
			<ffi:setProperty name="checkAchEntitlement" property="ChannelGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</s:if>
	<ffi:process name="checkAchEntitlement"/>
	<ffi:cinclude value1="${checkAchEntitlement.Entitled}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayACH" value="true"/>
	</ffi:cinclude>
	<%-- check Tax Payments --%>
	<ffi:setProperty name="checkAchEntitlement" property="OperationName" value="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TAX_PAYMENTS %>"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<s:if test="%{#session.UsingEntProfiles == 'true'}">
				<ffi:setProperty name="checkAchEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				<ffi:setProperty name="checkAchEntitlement" property="MemberId" value="${BusinessEmployeeId}"/>
			</s:if>
	</s:if>
	<s:if test="%{#session.Section == 'UserProfile'}">
			<ffi:setProperty name="checkAchEntitlement" property="ChannelGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</s:if>
	<ffi:process name="checkAchEntitlement"/>
	<ffi:cinclude value1="${checkAchEntitlement.Entitled}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayACH" value="true"/>
	</ffi:cinclude>
	<%-- END OF DETERMINING WHETHER TO DISPLAY ACH --%>
