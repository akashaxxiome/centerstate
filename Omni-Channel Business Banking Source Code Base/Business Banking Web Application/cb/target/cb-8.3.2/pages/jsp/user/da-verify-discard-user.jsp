<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setL10NProperty name='PageHeading' value='Verify Discard User'/>
<%
	session.setAttribute("itemId", request.getParameter("itemId"));
    session.setAttribute("itemType", request.getParameter("itemType"));
    session.setAttribute("category", request.getParameter("category"));
    session.setAttribute("userAction", request.getParameter("userAction"));
%>
<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
<ffi:process name="SetBusinessEmployee"/>
<ffi:removeProperty name="SetBusinessEmployee"/>

<ffi:object id="DiscardPendingChangeTask" name="com.ffusion.tasks.dualapproval.DiscardPendingChange" scope="session"/>
<% 
	session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("DiscardPendingChangeTask")); 
%>
<ffi:object id="RemoveBusinessUser" name="com.ffusion.tasks.admin.RemoveBusinessUser" scope="session"/>
<% session.setAttribute("FFIRemoveBusinessUser", session.getAttribute("RemoveBusinessUser")); %>

	<div align="center">
		<table class="adminBackground" border="0" cellspacing="0" cellpadding="0">
			<tr>
				
				<td>
					<s:form id="verifyDiscardUserFormID" namespace="/pages/dualapproval" validate="false" action="dualApprovalAction-discardChanges" method="post" name="VerifyDiscardUserForm" theme="simple" >
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>"/>
						<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>"/>
						<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>"/>
						<input type="hidden" name="module" value="User"/>
						<input type="hidden" name="daAction" value="Discarded"/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						
						<tr>
							<td class="columndata" align="center">
								<s:text name="jsp.user.discardUserChangesForApproval">
									<s:param><ffi:getProperty name="BusinessEmployee" property="FullName"/></s:param>
								</s:text>
							</td>
							<td></td>
						</tr>
						
						<tr>
							<td>
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a id="cancelVerifyDiscardUserID" 
									button="true" 
									onClickTopics="closeDialog"
								>
									<s:text name="jsp.default_82" />
								</sj:a>
								 <sj:a 
									id="verifyDiscardUserID"
									formIds="verifyDiscardUserFormID"
									targets="result"
	                                button="true"
									onSuccessTopics=""
	                                onCompleteTopics="verifyDiscardUserCompleteTopics,closeDialog"
									><s:text name="jsp.default_Discard" />
					            </sj:a>								
							</div>
							</td>
						</tr>
					</table>
					</s:form>
				</td>
				
			</tr>
		</table>
			<p></p>
	</div>
