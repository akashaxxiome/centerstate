<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set var="Section" value="#parameters.Section[0]" scope="session"/>
<s:if test="%{#parameters.ViewUserPermissions[0] != null}">
	<s:set var="ViewUserPermissions" value="#parameters.ViewUserPermissions[0]" scope="session" />
</s:if>
<s:else>
	<ffi:removeProperty name="ViewUserPermissions"/>
</s:else>

<%-- user permissions --%>
<s:if test="%{#session.Section == 'Users'}">
	<% 
		session.setAttribute("BusinessEmployeeId", request.getParameter("BusinessEmployeeId"));
		session.setAttribute("OneAdmin", request.getParameter("OneAdmin"));	
		session.setAttribute("UsingEntProfiles", request.getParameter("UsingEntProfiles"));
		session.setAttribute("ChannelId", request.getParameter("ChannelId"));
	%>
	<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${BusinessEmployeeId}"/>
</s:if>
<%-- profile permissions --%>
<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<% 
		session.setAttribute("BusinessEmployeeId", request.getParameter("BusinessEmployeeId"));
		session.setAttribute("OneAdmin", request.getParameter("OneAdmin"));	
		session.setAttribute("UsingEntProfiles", request.getParameter("UsingEntProfiles"));
		session.setAttribute("ChannelId", request.getParameter("ChannelId"));
	%>
	<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${BusinessEmployeeId}"/>
</ffi:cinclude>
<%-- group permissions --%>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<% session.setAttribute("EditGroup_GroupId", request.getParameter("EditGroup_GroupId")); %>
</ffi:cinclude>
<%-- division permissions --%>
<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<% session.setAttribute("EditGroup_GroupId", request.getParameter("EditGroup_GroupId")); %>
</ffi:cinclude>
<%-- UserProfile permissions --%>
<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
	<%  session.setAttribute("EditGroup_GroupId", request.getParameter("EditGroup_GroupId"));
		session.setAttribute("UsingEntProfiles", request.getParameter("UsingEntProfiles"));
		session.setAttribute("ChannelId", request.getParameter("ChannelId"));  %>
</ffi:cinclude>
<s:include value="inc/permissions-init.jsp"/>
