<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<s:include value="inc/admin-init.jsp"/>

<script src="<s:url value='/web/js/user/admin%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/admin_grid%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/groups%{#session.minVersion}.js'/>" type="text/javascript"></script>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT%>">
<div id="desktop" align="center">

    <style type="text/css">
        .indent1 { padding-left: 2em; }
        .indent2 { padding-left: 4em; }
        .indent3 { padding-left: 6em; }
        .indent4 { padding-left: 8em; }
    </style>

	<%-- <div id="appdashboard" style="display:none">
		<s:include value="admin_dashboard.jsp"/>
	</div> --%>

	<div id="appdashboard">
		<s:include value="groups_dashboard.jsp"/> 
	</div>
	
	<div id="operationresult">
		<div id="resultmessage"></div>
	</div>
	<div id="details">
		<s:include value="admin_details.jsp"/>
	</div>
	<div id="permissionsDiv" class="adminBackground sectionsubhead"/>
	<div id="summary">
		<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
			<s:include value="admin_groups_summary.jsp"/>
		</ffi:cinclude>
	</div>
</div>

<sj:dialog id="editAdminerDialogId" cssClass="adminDialog" title="%{getText('jsp.user_Edit_Adminer')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="850"></sj:dialog>

<sj:dialog id="editAdminerAutoEntitleDialogId" cssClass="adminDialog" title="%{getText('jsp.user_Edit_Adminer')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650"></sj:dialog>

<sj:dialog id="viewGroupPendingDialogID" cssClass="adminDialog" title="View Group" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="650">
</sj:dialog>

<sj:dialog id="verifyDiscardGroupPendingDialogID" cssClass="adminDialog" title="Verify Discard Group" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto">
</sj:dialog>

<sj:dialog id="DAGroupSubmitForApprovalDialogId" cssClass="adminDialog" title="Verify Submit Group for Approval" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto">
</sj:dialog>

<sj:dialog id="approvalWizardDialogID" cssClass="adminDialog" title="%{getText('jsp.user.ApprovalWizard')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="700" height="500" maxHeight="500">
</sj:dialog>

 <sj:dialog id="deleteGroupDialogID" cssClass="adminDialog" title="%{getText('jsp.user_105')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500" onCloseTopics="closeDeleteGroupDialog">
</sj:dialog>

<sj:dialog id="viewAdminInViewDiv" cssClass="adminDialog" title="View Adminstrator(s)" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto">
</sj:dialog>


<script>
	$(document).ready(function(){
		var $editAdminerDialogIdCloseXButton =$('#editAdminerDialogId').parent().find('.ui-dialog-titlebar-close');
		$editAdminerDialogIdCloseXButton.unbind('click');
		$editAdminerDialogIdCloseXButton.bind('click', function(){
			ns.admin.editAdminerCancelButtonOnClick();
		});
		
		var $editAdminerAutoEntitleDialogCloseXButton =$('#editAdminerAutoEntitleDialogId').parent().find('.ui-dialog-titlebar-close');
		$editAdminerAutoEntitleDialogCloseXButton.unbind('click');
		$editAdminerAutoEntitleDialogCloseXButton.bind('click', function(){
			ns.admin.editAdminerAutoEntitleCancelButtonOnClick();
		});
		
		ns.groups.showAdminGroupDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
	</script>
</ffi:cinclude>
