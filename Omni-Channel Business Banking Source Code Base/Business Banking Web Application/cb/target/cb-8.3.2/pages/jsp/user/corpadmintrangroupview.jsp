<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name='PageHeading' value='Company Profile'/>
<%
	request.setAttribute("EditTransactionGroup_GroupName", request.getParameter("EditTransactionGroup_GroupName"));
%>

<%-- initializing if necessary --%>
<ffi:removeProperty name="GroupDivAdminEdited"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="modifiedAdmins"/>
<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>


<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>

<ffi:setProperty name='PageText' value=''/>

<%--get company profile info from the business bean--%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<%--get the company's BAI export preferences--%>
<ffi:object id="GetBAIExportSettings" name="com.ffusion.tasks.user.GetBAIExportSettings" scope="request"/>
<ffi:setProperty name="GetBAIExportSettings" property="BusinessSessionName" value="Business"/>
<ffi:process name="GetBAIExportSettings"/>

<%--get service package info from GetEntitlementGroup, which stores info in session under Entitlement_EntitlementGroup--%>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Business.ServicesPackageId}" />
<ffi:process name="GetEntitlementGroup"/>

<%--get contact using GetPrimaryBusinessEmployees, which stores info for primary contact in session under PrimaryBusinessEmployee--%>
<ffi:object id="GetPrimaryBusinessEmployees" name="com.ffusion.tasks.user.GetPrimaryBusinessEmployees" scope="session"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BankId" value="${Business.BankId}"/>
<ffi:process name="GetPrimaryBusinessEmployees"/>
<ffi:removeProperty name="GetPrimaryBusinessEmployees" />

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
<ffi:process name="GetBusinessEmployeesByEntGroups"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
 								
<%--get accounts info from GetAccountsByBusinessEmployee, stores results in session as BusEmpAccounts--%>
<ffi:object id="GetAccountsByBusinessEmployee" name="com.ffusion.tasks.accounts.GetAccountsByBusinessEmployee" scope="session"/>
	<ffi:setProperty name="GetAccountsByBusinessEmployee" property="AccountsName" value="BusEmpAccounts"/>
<ffi:process name="GetAccountsByBusinessEmployee"/>
<ffi:removeProperty name="GetAccountsByBusinessEmployee"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />

<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>


<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<%-- Dual Approval for Transaction Groups --%>
<ffi:removeProperty name="AddTransactionGroupToDA" />
<ffi:removeProperty name="DA_TRANSACTION_GROUP" />
<ffi:object id="AddTransactionGroupToDA" name="com.ffusion.tasks.dualapproval.AddTransactionGroupToDA" />
<ffi:setProperty name="AddTransactionGroupToDA" property="ReadFromDA" value="true" />
<ffi:setProperty name="AddTransactionGroupToDA" property="OldTransactionGroupName" value="${EditTransactionGroup_GroupName}" />
<ffi:process name="AddTransactionGroupToDA" />
<%-- END Dual Approval for Transaction Groups --%>


<div align="center">
	<ffi:setProperty name="subMenuSelected" value="company"/>
		<table border="0" cellspacing="0" cellpadding="3" class="tabledata" width="100%">
			<tr>
								<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="notEquals">
									<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="${AddTransactionGroupToDA.OldTransactionGroupName}" operator="notEquals">
										<td colspan="2" width="30%" align="left" class="sectionheadDA">					
									</ffi:cinclude>
								    <ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="${AddTransactionGroupToDA.OldTransactionGroupName}" operator="equals">
								    	<td colspan="2" width="30%" align="left" class="sectionsubhead">
								    </ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="equals">
									<td colspan="2" width="50%" align="left" class="sectionsubhead">
								</ffi:cinclude>
										<s:text name="jsp.user_344"/>
									</td>
									<td valign="baseline" class="columndata adminBackground">
										<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="equals">
											&nbsp;&nbsp;<ffi:getProperty name="AddTransactionGroupToDA" property="OldTransactionGroupName" />
										</ffi:cinclude>
										<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="notEquals">
							    			&nbsp;&nbsp;<ffi:getProperty name="AddTransactionGroupToDA" property="TransactionGroupName" />
							    			<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="${AddTransactionGroupToDA.OldTransactionGroupName}" operator="notEquals">
							    				&nbsp;&nbsp;<span class="sectionhead_greyDA">
							    				<ffi:getProperty name="AddTransactionGroupToDA" property="OldTransactionGroupName" />
							    				<ffi:cinclude value1="${AddTransactionGroupToDA.UserAction}" value2="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.USER_ACTION_DELETED%>" operator="equals">
													<ffi:getProperty name="AddTransactionGroupToDA" property="UserAction" />
												</ffi:cinclude>
							    				</span>
							    			</ffi:cinclude>
								    	</ffi:cinclude>
									</td>
								</tr>
							<%-- Display assigned typecodes changes if any --%>
							<ffi:cinclude value1="${AddTransactionGroupToDA.TypeCodes}" value2="" operator="notEquals">
								<tr><td></td></tr>
								<tr><td></td></tr>
								<tr>
								    <td class="sectionheadDA adminBackground" colspan="2" width="30%" align="left" valign="top"><s:text name="jsp.user_48"/>:</td>
								    <td valign="top" class="columndata adminBackground">
								    <table class="administratorViewTable"><tr>
								    	<td class="columndata" valign="top">
									    <ffi:list collection="AddTransactionGroupToDA.TypeCodeList" items="typeCode">											
									    	<span>
											<ffi:getProperty name="typeCode" property="Code"/>: <ffi:getProperty name="typeCode" property="Description"/>&#44;
											</span>											
										</ffi:list>
										</td>
										<td class="columndata" valign="top">
											<ffi:list collection="AddTransactionGroupToDA.OldTypeCodeList" items="typeCode">
										    <span class="sectionhead_greyDA"><ffi:getProperty name="typeCode" property="Code"/>: <ffi:getProperty name="typeCode" property="Description"/>&#44;</span>
										    </ffi:list>
										</td>
									</tr></table>
								    </td>
								</tr>
							</ffi:cinclude>								
							<%-- Display assigned typecodes for the group --%>
							<ffi:cinclude value1="${AddTransactionGroupToDA.TypeCodes}" value2="" operator="equals">
								<%-- Retrieve the original type codes for the transaction group --%>
								<ffi:object name="com.ffusion.tasks.business.GetTypeCodesForGroup" id="GetTypeCodesForGroup" scope="request"/>
								<ffi:setProperty name="GetTypeCodesForGroup" property="TransactionGroup" value="${AddTransactionGroupToDA.OldTransactionGroupName}"/>
								<ffi:cinclude value1="${AddTransactionGroupToDA.UserAction}" value2="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.USER_ACTION_DELETED%>" operator="equals">
									<ffi:setProperty name="GetTypeCodesForGroup" property="TransactionGroup" value="${AddTransactionGroupToDA.TransactionGroupName}"/>
								</ffi:cinclude>
								<ffi:process name="GetTypeCodesForGroup"/>
								<tr>
								    <td class="sectionsubhead adminBackground" colspan="2" width="30%" align="left" ><s:text name="jsp.user_48"/>:</td>
								    <td valign="top" class="columndata adminBackground">
								    <table class="administratorViewTable"><tr>
								    	<td class="columndata" valign="top">
									    <ffi:list collection="GetTypeCodesForGroup.CodeInfoList" items="typeCode">											
									    	<span>
											<ffi:getProperty name="typeCode" property="Code"/>: <ffi:getProperty name="typeCode" property="Description"/>&#44;
											</span>											
										</ffi:list>
										</td>
									</tr></table>
								    </td>
								</tr>
							</ffi:cinclude>
							</table>
		<div class="ffivisible" style="height:50px;">&nbsp;</div>
		<div class="ui-widget-header customDialogFooter">
				<sj:a button="true" title="Close" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a>
		</div>
	<ffi:flush/>
	<!-- Restore Business Employee in session to the original values -->
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="Business"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="restoredObject" value="y"/>
 	<ffi:process name="PopulateObjectWithDAValues"/>
</div>


<ffi:setProperty name="onCancelGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp"/>
<ffi:removeProperty name="BusEmpAccounts" />
<ffi:removeProperty name="BusinessEmployees" />
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<ffi:removeProperty name="DAITEM"/>
