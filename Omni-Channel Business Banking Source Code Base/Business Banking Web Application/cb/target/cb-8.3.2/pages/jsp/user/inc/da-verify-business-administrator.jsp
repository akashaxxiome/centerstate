<%-- This page displays old values and new values for administrator section in da-business-verify.jsp --%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:cinclude value1="${isAdministratorChanged}" value2="Y">

<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
<ffi:process name="GetAdminsForGroup"/>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="TempBusinessEmployee"/>

<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
<ffi:process name="GenerateUserAndAdminLists"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="GenerateUserAndAdminLists"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
<ffi:process name="GenerateListsFromDAAdministrators"/>

<ffi:object id="GenerateCommaSeperatedAdminList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedAdminList"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminEmployees" value="OldAdminEmployees"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminGroups" value="OldAdminGroups"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="sessionName" value="OldAdminStringList"/>
<ffi:process name="GenerateCommaSeperatedAdminList"/>

<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminEmployees" value="AdminEmployees"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminGroups" value="AdminGroups"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="sessionName" value="NewAdminStringList"/>
<ffi:process name="GenerateCommaSeperatedAdminList"/>

<div  class="blockHead tableData"><span><strong><!--L10NStart-->Administrator<!--L10NEnd--></strong></span></div>
<div class="paneWrapper">
 	<div class="paneInnerWrapper">
   		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
   			<tr class="header">
				<td class="sectionsubhead adminBackground" width="40%">Old Administrators</td>
				<td class="sectionsubhead adminBackground" width="40%">New Administrators</td>
			    <td class="sectionsubhead adminBackground" width="20%">Action</td>
			<tr>
			<tr>
				<td class="columndata">
					<ffi:getProperty name="OldAdminStringList"/>
				</td>
				<td class="columndata sectionheadDA">
					<ffi:getProperty name="NewAdminStringList"/>
				</td>
			    <td class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="UserAction" /></td>
			</tr>
   		</table>
 </div>
</div>
<%-- <tr><td colspan="7"><hr noshade size="1" style="width: 90%"></td></tr>
<tr>
	<td colspan="1" class="sectionsubhead adminBackground" ></td>
	<td colspan="2" class="sectionsubhead adminBackground" >Old Administrators</td>
	<td colspan="3" class="sectionsubhead adminBackground" >New Administrators</td>
    <td class="sectionsubhead adminBackground" >Action</td>
</tr>
<tr>
	<td colspan="1" class="sectionsubhead adminBackground" ></td>
	<td colspan="2"  class="columndata">
		<ffi:getProperty name="OldAdminStringList"/>
	</td>
	<td colspan="3"  class="columndata">
		<ffi:getProperty name="NewAdminStringList"/>
	</td>
    <td  class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="UserAction" /></td>
</tr> --%>
</ffi:cinclude>

<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>