<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%-- if we added or canceled adding of a new group, we need to do a bit of clean up --%>
<ffi:removeProperty name="GroupDivAdminEdited"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="modifiedAdmins"/>
<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>
<ffi:removeProperty name="DivisionId"/>
<ffi:removeProperty name="DivisionName"/>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="%{#session.PagesPath}user/inc/admin-init.jsp" />
</ffi:cinclude>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<%-- put Entitlement_EntitlementGroups into the session --%>
<ffi:object id="GetChildrenByGroupType" name="com.ffusion.efs.tasks.entitlements.GetChildrenByGroupType" scope="session"/>
    <ffi:setProperty name="GetChildrenByGroupType" property="GroupType" value="Division"/>
<ffi:setProperty name="GetChildrenByGroupType" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="GetChildrenByGroupType"/>
<ffi:removeProperty name="GetChildrenByGroupType"/>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:setProperty name="CanAdministerAnyGroup" property="GroupType" value="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>"/>
<ffi:process name="CanAdministerAnyGroup"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<%-- put Descendants in session --%>
<ffi:object id="GetDescendantsByType" name="com.ffusion.efs.tasks.entitlements.GetDescendantsByType" scope="session"/>
<ffi:setProperty name="GetDescendantsByType" property="GroupID" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="GetDescendantsByType" property="GroupType" value="Group"/>
<ffi:process name="GetDescendantsByType"/>
<ffi:removeProperty name="GetDescendantsByType" />
<%-- put BusinessEmployee in session, which is the current user --%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee"/>

<%-- put Entitlement_EntitlementGroups into the session --%>
<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="BusinessEmployee"/>
<ffi:process name="GetGroupsAdministeredBy"/>
<ffi:removeProperty name="GetGroupsAdministeredBy" />

<ffi:object id="GetSupervisorFor" name="com.ffusion.tasks.admin.GetSupervisorFor" scope="session" />

<%-- Add some properties to the EntitlementGroup hashmap for use in sorting. --%>
<ffi:object id="GetAncestorGroupByType" name="com.ffusion.tasks.admin.GetAncestorGroupByType" scope="session"/>
<ffi:list collection="Descendants" items="DescendantItem">
    <ffi:setProperty name="GetAncestorGroupByType" property="GroupType" value="Division"/>
    <ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="DescendantItemDivision"/>
    <ffi:setProperty name="GetAncestorGroupByType" property="GroupID" value="${DescendantItem.GroupId}"/>
    <ffi:process name="GetAncestorGroupByType"/>
    <ffi:setProperty name="DescendantItem" property="Division" value="${DescendantItemDivision.GroupName}"/>

    <ffi:setProperty name="GetSupervisorFor" property="GroupId" value="${DescendantItem.GroupId}"/>
    <ffi:process name="GetSupervisorFor"/>
    <ffi:setProperty name="DescendantItem" property="Supervisor" value="${GetSupervisorFor.Name}"/>

	<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
	<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
   		<ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${DescendantItem.GroupId}" />
	<ffi:process name="GetAdministratorsForGroups"/>
	<ffi:removeProperty name="GetAdministratorsForGroups"/>

	<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
	<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
	<ffi:process name="GetBusinessEmployeesByEntGroups"/>
	<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />

	<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
		<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
	<ffi:process name="FilterNotEntitledEmployees"/>

    <ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
   	<%--display name of each employee in BusinessEmployees--%>
	<%
		boolean isFirst= true;
		String temp = "";
   	%>
	<ffi:list collection="BusinessEmployees" items="emp">
		<%--display separator if this is not the first one--%>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
			<ffi:setProperty name="empName" value="${emp.Name}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
			<ffi:setProperty name="empName" value="${emp.LastName}"/>
		</ffi:cinclude>
		<%
			if( isFirst ) {
				isFirst = false;
				temp = temp + session.getAttribute( "empName" );
			} else {
				temp = temp + ", " + session.getAttribute( "empName" );
			}
		%>
	</ffi:list>
 	<%
 		if( temp.length() > 40 ) {
 			temp = temp.substring( 0, 40 ); 			
 			int index = temp.lastIndexOf( ',' );
 			if(index != -1){
 				temp = temp.substring( 0, index ) + "...";
 			}else{
 				temp = temp + "...";
 			}
 		}
 		session.setAttribute( "AdministratorStr", temp );%>

	<ffi:setProperty name="DescendantItem" property="Administrator" value="${AdministratorStr}"/>
	<ffi:removeProperty name="AdministratorStr"/>
 	<ffi:removeProperty name="empName"/>

	<ffi:setProperty name="DescendantItem" property="IsAnAdminOf" value="false"/>
	<ffi:list collection="Entitlement_EntitlementGroups" items="EntitlementGroupItem">
		<ffi:cinclude value1="${DescendantItem.GroupId}" value2="${EntitlementGroupItem.GroupId}" operator="equals">
			<ffi:setProperty name="DescendantItem" property="IsAnAdminOf" value="true"/>
		</ffi:cinclude>
	</ffi:list>

</ffi:list>

<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
    <ffi:setProperty name="Descendants" property="SortedBy" value="${SortedBy}"/>
</ffi:cinclude>

<ffi:object id="GetDescendantsByTypeDA" name="com.ffusion.tasks.dualapproval.GetDescendantsByTypeDA" scope="session" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="UserSessionName" value="BusinessEmployees" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="DecendantsSessionName" value="Descendants" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="MaxAdminStringLength" value="40" />
<ffi:process name="GetDescendantsByTypeDA" />
<ffi:removeProperty name="GetDescendantsByTypeDA" />

<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
    <ffi:setProperty name="PendingApprovalGroups" property="SortedBy" value="${SortedBy}"/>
</ffi:cinclude>

<%-- display pending approval grid if it has content --%>
<ffi:cinclude value1="${PendingApprovalGroups.size}" value2="0" operator="equals">
	<script>
		ns.admin.pendingApprovalGroupsNum = "0";
		$('#group_PendingApprovalTab').attr('style','display:none');
	</script>
</ffi:cinclude>

<ffi:cinclude value1="${PendingApprovalGroups.size}" value2="0" operator="notEquals">
	<script>
		ns.admin.pendingApprovalGroupsNum = '<ffi:getProperty name="PendingApprovalGroups" property="Size"/>';
		$('#group_PendingApprovalTab').removeAttr('style');
		/* if($("#pendingGroupChangesID").jqGrid('getGridParam', 'reccount') != ns.admin.pendingApprovalGroupsNum){
			$('#pendingGroupChangesID').trigger("reloadGrid");
		} */
	</script>
</ffi:cinclude>
