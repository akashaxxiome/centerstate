<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/web/js/user/secondaryUsers%{#session.minVersion}.js'/>"></script>
<ffi:object name="com.ffusion.tasks.multiuser.TermsAccepted" id="FFITermsAccepted" scope="session"/>
<ffi:object name="com.ffusion.tasks.multiuser.SetSecondaryUsersActiveState" id="FFISetSecondaryUsersActiveState" scope="session"/>

<link rel="stylesheet" href="<s:url  value='/web/css/FFretail.css'/>" type="text/css">
<%-- <div id="appdashboard" >
	<div style="float:right;">	
		<sj:a
				id="addSecondaryUserBtn"
				button="true"
				buttonIcon="ui-icon-plus"><s:text name="jsp.user_28"/></sj:a>
	</div>	
	
</div> --%>
<div id="appdashboard">
	<s:include value="manage_users_dashboard.jsp" />
</div>
<div id="secondaryUsersContainer">
	<div style="padding:10px 10px 10px 10px;" class="instructions">
		<s:text name="secondaryUser.setupInstructions"/>
		<br>
	</div>
	<div id="summary">	
			<s:include value="/pages/jsp/user/secondary-users-grid.jsp" />
	</div>
</div>

<div id="addEditSecondaryUsersContainer" class="hidden">
	
</div>

<div id="deleteSecondaryUserDialog"></div>
<div id="userAddEditConfirmDialog" style="width:800px; height:570px; overflow: hidden; display:none;"></div>
<div id="pendingTxPayementsDialog"></div>

<script type="text/javascript">
$(document).ready(function(){
	ns.secondaryUsers.showSecondaryUsersDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
});
</script>