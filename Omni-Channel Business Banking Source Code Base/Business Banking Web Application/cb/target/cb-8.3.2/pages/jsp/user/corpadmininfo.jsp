<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<span id="pageHeading" style="display:none"><s:text name="jsp.default_106"/> <s:text name="jsp.user_404"/> </span>
<%-- Dual approval island include - move to own tab --%>
<%--    <s:include value="%{#session.PathExt}user/inc/da-business-island.jsp"/> --%>
<div align="center" id="corpadmininfoDiv">
<ffi:help id="user_corpadmininfo" className="moduleHelpClass"/>
	<s:include value="inc/corpadmininfo-pre.jsp" />
	<!-- <table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData"> -->
		<!-- <tr>
			<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
		</tr> -->
		<!-- <tr> -->
			<%-- <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="10%" border="0"></td>
			<td align="left" colspan="2" class="adminBackground sectionhead">&gt;<s:text name="jsp.default_106"/>  --%>
				<%-- <ffi:cinclude
					value1="${Entitlement_CanAdminister}" value2="TRUE"
					operator="equals">
					<ffi:setProperty name="viewOnly" value="false" />
					<ffi:removeProperty name="ViewUserPermissions" />
					<ffi:cinclude value1="${DAItem.status}" value2="Pending Approval">
						<ffi:setProperty name="viewOnly" value="true" />
						<ffi:setProperty name="ViewUserPermissions" value="TRUE" />
					</ffi:cinclude>
					<ffi:cinclude value1="${DAItem.status}" value2="Rejected">
						<ffi:setProperty name="viewOnly" value="true" />
						<ffi:setProperty name="ViewUserPermissions" value="TRUE" />
					</ffi:cinclude>
					<ffi:cinclude value1="${viewOnly}" value2="true">
						
						<ffi:link url="${SecurePath}user/corpadminbusinessview.jsp">
							<img src="/cb/web/multilang/grafx/user/i_view.gif" alt="<s:text name="jsp.default_460"/>" border="0" hspace="3">
						</ffi:link>
						
						<s:url id="CompanyViewURL" value="%{#session.PagesPath}user/corpadminbusinessview.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							href="%{CompanyViewURL}"
							targets="DACompanyProfileViewDialogId"
							button="true" 
							buttonIcon="ui-icon-info"
							onClickTopics="beforeLoad"
							onSuccessTopics="openDACompanyProfileViewDialog"
							><s:text name="jsp.default_459"/></sj:a>			
											
					</ffi:cinclude>
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">

						<s:url id="CompanyEditURL" value="%{#session.PagesPath}user/corpadminbusinessedit.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							id="editCompany"
							href="%{CompanyEditURL}"
							targets="companyContent"
							button="true" 
							buttonIcon="ui-icon-pencil"
							onClickTopics="beforeLoad"
							onCompleteTopics="completeCompanyProfileLoad"
							><s:text name="jsp.default_178"/></sj:a>

					</ffi:cinclude>
				</ffi:cinclude> 
			</td>
			<ffi:removeProperty name="editURL" />
			<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="10%" border="0"></td>
		</tr>
	</table>--%>
	<table width="100%" border="0" cellspacing="3" cellpadding="3" class="tableData">
		<tr><td colspan="6"><h3 class="transactionHeading">Company Information</h3></td></tr>
		<tr>
			<td class="sectionsubhead adminBackground" valign="baseline" align="right" width="11%"><s:text name="jsp.default_283"/>:</td>
			<td align="left" valign="baseline" class="columndata adminBackground" width="22%"><ffi:getProperty name="Business" property="BusinessName" /></td>
			<td class="adminBackground sectionsubhead" valign="baseline" align="right" width="11%"><s:text name="jsp.user_296"/>:</td>
			<td align="left" class="columndata" valign="baseline" width="22%"><ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName" /></td>
			<td class="adminBackground sectionsubhead" valign="baseline" align="right" width="11%"><s:text name="jsp.user_297"/>:</td>
			<ffi:setProperty name="Business" property="DateFormat" value="${UserLocale.DateFormat}" />
			<td align="left" class="columndata" valign="baseline" width="22%"><ffi:getProperty name="Business" property="ActivationDate" /></td>
		</tr>
		<ffi:cinclude value1="${Business.ACH_IAT_UseTaxID}" value2="Y" operator="equals">
		<tr>
			<td class="sectionsubhead adminBackground" valign="baseline" align="right"><s:text name="jsp.user_302"/>:</td>
			<td align="left" valign="baseline" class="columndata adminBackground"><ffi:getProperty name="Business" property="TaxId" /></td>
		</tr>
		</ffi:cinclude>
		<tr>
		<ffi:list collection="CATEGORY_BEAN.DaItems" items="daItemRow">
			<ffi:cinclude value1="${daItemRow.FieldName}" value2="street" operator="equals">
				<ffi:setProperty name="highlightAddress" value="y" />
			</ffi:cinclude>
			<ffi:cinclude value1="${daItemRow.FieldName}" value2="street2" operator="equals">
				<ffi:setProperty name="highlightAddress" value="y" />
			</ffi:cinclude>
			<ffi:cinclude value1="${daItemRow.FieldName}" value2="city" operator="equals">
				<ffi:setProperty name="highlightAddress" value="y" />
			</ffi:cinclude>
			<ffi:cinclude value1="${daItemRow.FieldName}" value2="state" operator="equals">
				<ffi:setProperty name="highlightAddress" value="y" />
			</ffi:cinclude>
			<ffi:cinclude value1="${daItemRow.FieldName}" value2="zipCode" operator="equals">
				<ffi:setProperty name="highlightAddress" value="y" />
			</ffi:cinclude>
			<ffi:cinclude value1="${daItemRow.FieldName}" value2="country" operator="equals">
				<ffi:setProperty name="highlightAddress" value="y" />
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${highlightAddress}" value2="y" operator="equals">
			<td class="sectionheadDA" valign="baseline" align="right"><s:text name="jsp.default_35"/>:</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${highlightAddress}" value2="y" operator="notEquals">
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.default_35"/>:</td>
		</ffi:cinclude>
		<ffi:removeProperty name="highlightAddress" />
			<td align="left" class="columndata" valign="baseline"><ffi:getProperty name="Business" property="Street" /><br>
				<ffi:cinclude value1="${Business.Street2}" value2="" operator="notEquals">
					<ffi:getProperty name="Business" property="Street2" /><br>
				</ffi:cinclude> <ffi:cinclude value1="${Business.City}" value2="" operator="notEquals">
					<ffi:getProperty name="Business" property="City" />,
				</ffi:cinclude> 
				<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Business.Country}" /> 
				<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Business.State}" /> 
				<ffi:process name="GetStateProvinceDefnForState" /> 
				<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
					<ffi:getProperty name="Business" property="State" />
				</ffi:cinclude> 
				<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
					<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name" />
				</ffi:cinclude> 
			<ffi:cinclude value1="${Business.State}" value2="" operator="notEquals">
			<ffi:cinclude value1="${Business.ZipCode}" value2="" operator="notEquals">
			</ffi:cinclude>
			</ffi:cinclude> 
				<ffi:getProperty name="Business" property="ZipCode" /> 
			<ffi:cinclude value1="${Business.State}${Business.ZipCode}" value2="" operator="notEquals">
				<br>
			</ffi:cinclude> 
			<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Business.Country}" /> 
				<ffi:getProperty name='CountryResource' property='Resource' /> 
				<span class="sectionhead_greyDA"> 
					<br/>
					<ffi:cinclude value1="${oldDAObject.Street}" value2="" operator="notEquals">
						<ffi:getProperty name="oldDAObject" property="Street" /><br/>
					</ffi:cinclude> 
					<ffi:cinclude value1="${oldDAObject.Street2}" value2="" operator="notEquals">
						<ffi:getProperty name="oldDAObject" property="Street2" /><br/>
					</ffi:cinclude> 
					<ffi:cinclude value1="${oldDAObject.City}" value2="" operator="notEquals">
						<ffi:getProperty name="oldDAObject" property="City" />,
					</ffi:cinclude> 
						<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${oldDAObject.Country}" /> 
						<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${oldDAObject.State}" /> 
					<ffi:process name="GetStateProvinceDefnForState" /> 
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
						<ffi:getProperty name="oldDAObject" property="State" />
					</ffi:cinclude> 
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
						<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name" />
					</ffi:cinclude> 
					<ffi:cinclude value1="${oldDAObject.State}" value2="" operator="notEquals">
						<ffi:cinclude value1="${oldDAObject.ZipCode}" value2="" operator="notEquals">
									  
						</ffi:cinclude>
					</ffi:cinclude> 
						<ffi:getProperty name="oldDAObject" property="ZipCode" /> 
					<ffi:cinclude value1="${oldDAObject.State}${oldDAObject.ZipCode}" value2="" operator="notEquals">
					</ffi:cinclude> 
						<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${oldDAObject.Country}" /> 
						<ffi:getProperty name='CountryResource' property='Resource' /> 
				</span>
			</td>
			<td class="adminBackground sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_249"/>:</td>
			<td align="left" class="columndata" align="left" valign="baseline">
				<ffi:cinclude value1="${Business.PersonalBanker}" value2="0" operator="notEquals">
					<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmininfo.jsp-1" parm0="${PersonalBanker.FullName}" />
					<ffi:cinclude value1="${PersonalBanker.Phone}" value2="" operator="notEquals"> <s:text name="jsp.user_304"/>: <ffi:getProperty name="PersonalBanker" property="Phone" />
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.PersonalBanker}" value2="0" operator="equals">
					<s:text name="jsp.default_296"/>
				</ffi:cinclude>
			</td>
			<td class="adminBackground sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_8"/>:</td>
			<td class="columndata" align="left" valign="baseline">
			<ffi:cinclude value1="${Business.AccountRep}" value2="0" operator="notEquals">
				<ffi:cinclude value1="${AccountRep.Phone}" value2="" operator="notEquals">, <s:text name="jsp.user_304"/>: <ffi:getProperty name="AccountRep" property="Phone" />
				</ffi:cinclude>
				<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmininfo.jsp-1" parm0="${AccountRep.FullName}" />
				<ffi:cinclude value1="${AccountRep.Phone}" value2="" operator="notEquals">, <s:text name="jsp.user_304"/>: <ffi:getProperty name="PersonalBanker" property="Phone" />
				</ffi:cinclude>
			</ffi:cinclude> <ffi:cinclude value1="${Business.AccountRep}" value2="0" operator="equals">
				<s:text name="jsp.default_296"/>
			</ffi:cinclude>
			</td>
		</tr>
		<tr>
			<td class='<ffi:getPendingStyle fieldname="phone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' valign="baseline" align="right"><s:text name="jsp.user_250"/>:</td>
			<td align="left" class="columndata" valign="baseline"><ffi:getProperty name="Business" property="Phone" /> 
				<span class="sectionhead_greyDA">
					<br />
					<ffi:getProperty name="oldDAObject" property="Phone" />
				</span>
			</td>
			<td class='<ffi:getPendingStyle fieldname="faxPhone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' valign="baseline" align="right"><s:text name="jsp.user_156"/>:</td>
			<td align="left" class="columndata" valign="baseline"><ffi:getProperty name="Business" property="FaxPhone" /> 
					<ffi:getProperty name="oldDAObject" property="FaxPhone" />
			</td>
			<td class='<ffi:getPendingStyle fieldname="email" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' valign="baseline" align="right"><s:text name="jsp.user_131"/>:</td>
			<td align="left" class="columndata" valign="baseline">
				<ffi:getProperty name="Business" property="Email" /> 
				<span class="sectionhead_greyDA">
					<ffi:getProperty name="oldDAObject" property="Email" />
				</span>
			</td>
		</tr>
		<tr><td colspan="6"><h3 class="transactionHeading"><s:text name="jsp.user_266"/></h3></td></tr>
		<tr>
			<td class=" adminBackground sectionsubhead" valign="baseline" align="right"><s:text name="jsp.default_283"/>:</td>
			<td class="columndata" align="left" valign="baseline"><ffi:getProperty name="PrimaryBusinessEmployee" property="Name" /></td>
			
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.default_35"/>:</td>
			<td class="columndata" align="left" valign="baseline"><ffi:getProperty name="PrimaryBusinessEmployee" property="Street" /><br>
			<ffi:cinclude value1="${PrimaryBusinessEmployee.Street2}" value2="" operator="notEquals">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="Street2" /><br>
			</ffi:cinclude> <ffi:cinclude value1="${PrimaryBusinessEmployee.City}" value2="" operator="notEquals">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="City" />,
			</ffi:cinclude> <ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${PrimaryBusinessEmployee.Country}" />
			<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${PrimaryBusinessEmployee.State}" />
			<ffi:process name="GetStateProvinceDefnForState" />
			<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="State" />
			</ffi:cinclude> 
			<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
				<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name" />
			</ffi:cinclude>
			<ffi:cinclude value1="${PrimaryBusinessEmployee.State}" value2="" operator="notEquals">
				<ffi:cinclude value1="${PrimaryBusinessEmployee.ZipCode}" value2="" operator="notEquals">
							  
				</ffi:cinclude>
			</ffi:cinclude> 
			<ffi:getProperty name="PrimaryBusinessEmployee" property="ZipCode" /> 
			<ffi:cinclude value1="${PrimaryBusinessEmployee.State}${PrimaryBusinessEmployee.ZipCode}" value2="" operator="notEquals">
				<br>
			</ffi:cinclude> 
			<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${PrimaryBusinessEmployee.Country}" /> 
			<ffi:getProperty name='CountryResource' property='Resource' /></td>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_95"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:cinclude value1="${PrimaryBusinessEmployee.PreferredContactMethod}" value2="email" operator="equals"> <s:text name="jsp.user_131"/>
				</ffi:cinclude> 
				<ffi:cinclude value1="${PrimaryBusinessEmployee.PreferredContactMethod}" value2="email" operator="notEquals">
					<s:text name="jsp.user_305"/>
				</ffi:cinclude>
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_250"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="Phone" />
			</td>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_156"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="FaxPhone" />
			</td>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_131"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="Email" />
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.default_136"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="PrimaryBusinessEmployee" property="DataPhone" />
			</td>
		</tr>
		<tr><td colspan="6"><h3 class="transactionHeading"><s:text name="jsp.user_282"/></h3></td></tr>
		<tr>
			<td class="adminBackground sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_201"/></td>
			<td class="columndata" align="left" valign="baseline"><ffi:getProperty name="SecondaryBusinessEmployee" property="Name" /></td>
			
			<td class="adminBackground sectionsubhead" valign="baseline" align="right"><s:text name="jsp.default_35"/>:</td>
			<td class="columndata" align="left" valign="baseline">
			<ffi:cinclude value1="${SecondaryBusinessEmployee.Street}" value2="" operator="notEquals">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="Street" /><br>
			</ffi:cinclude> 
			<ffi:cinclude value1="${SecondaryBusinessEmployee.Street2}" value2="" operator="notEquals">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="Street2" /><br>
			</ffi:cinclude> 
			<ffi:cinclude value1="${SecondaryBusinessEmployee.City}" value2="" operator="notEquals">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="City" />,
			</ffi:cinclude> 
			<ffi:setProperty name="GetStateProvinceDefnForState"
				property="CountryCode"
				value="${SecondaryBusinessEmployee.Country}" /> 
				<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${SecondaryBusinessEmployee.State}" /> 
				<ffi:process name="GetStateProvinceDefnForState" /> 
				<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="State" />
			</ffi:cinclude> 
			<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
				<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name" />
			</ffi:cinclude>
			<ffi:cinclude value1="${SecondaryBusinessEmployee.State}" value2="" operator="notEquals">
				<ffi:cinclude value1="${SecondaryBusinessEmployee.ZipCode}" value2="" operator="notEquals">
							  
				</ffi:cinclude>
			</ffi:cinclude> 
			<ffi:getProperty name="SecondaryBusinessEmployee" property="ZipCode" /> 
			<ffi:cinclude value1="${SecondaryBusinessEmployee.State}${SecondaryBusinessEmployee.ZipCode}" value2="" operator="notEquals">
				<br>
			</ffi:cinclude>
			<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${SecondaryBusinessEmployee.Country}" /> 
			<ffi:getProperty name='CountryResource' property='Resource' /></td>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_95"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:cinclude value1="${SecondaryBusinessEmployee.PreferredContactMethod}" value2="email" operator="equals">
					<s:text name="jsp.user_131"/>
				</ffi:cinclude> 
				<ffi:cinclude value1="${SecondaryBusinessEmployee.PreferredContactMethod}" value2="email" operator="notEquals">
					<s:text name="jsp.user_305"/>
				</ffi:cinclude>
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_250"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="Phone" />
			</td>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_156"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="FaxPhone" />
			</td>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.user_131"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="Email" />
			</td>
		</tr>
		<%-- <tr>
			<td height="30px" colspan="2" align="center" class="sectionhead" style="background: #A5A4A5; color:#fff; font-size: 14px; font-weight: bold; font-family: arial;"><s:text name="jsp.user_266"/></td>
			<td height="30px" colspan="2" align="center" class="sectionhead" style="background: #A5A4A5; color:#fff; font-size: 14px; font-weight: bold; font-family: arial;"><s:text name="jsp.user_282"/></td>
		</tr> --%>
		<tr>
			<td class="sectionsubhead" valign="baseline" align="right"><s:text name="jsp.default_136"/>:</td>
			<td class="columndata" align="left" valign="baseline">
				<ffi:getProperty name="SecondaryBusinessEmployee" property="DataPhone" />
			</td>
		</tr>
		<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
			<ffi:object id="PopulateObjectWithDAValues" name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session" />
			<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="Business" />
			<ffi:setProperty name="PopulateObjectWithDAValues" property="restoredObject" value="y" />
			<ffi:process name="PopulateObjectWithDAValues" />
		</ffi:cinclude>
	</table>
</div>