<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.user_73')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:setProperty name="onCancelGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp"/>

<%-- initializing if necessary --%>
<ffi:removeProperty name="GroupDivAdminEdited"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="modifiedAdmins"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="highLightFieldDA"/>
<ffi:removeProperty name="userAction"/>

<ffi:setProperty name='PageText' value=''/>

<%--get company profile info from the business bean--%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<%--get the company's BAI export preferences--%>
<ffi:object id="GetBAIExportSettings" name="com.ffusion.tasks.user.GetBAIExportSettings" scope="request"/>
<ffi:setProperty name="GetBAIExportSettings" property="BusinessSessionName" value="Business"/>
<ffi:process name="GetBAIExportSettings"/>

<%--get service package info from GetEntitlementGroup, which stores info in session under Entitlement_EntitlementGroup--%>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Business.ServicesPackageId}" />
<ffi:process name="GetEntitlementGroup"/>

<%--get contact using GetPrimaryBusinessEmployees, which stores info for primary contact in session under PrimaryBusinessEmployee--%>
<ffi:object id="GetPrimaryBusinessEmployees" name="com.ffusion.tasks.user.GetPrimaryBusinessEmployees" scope="session"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BankId" value="${Business.BankId}"/>
<ffi:process name="GetPrimaryBusinessEmployees"/>
<ffi:removeProperty name="GetPrimaryBusinessEmployees" />

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
<ffi:process name="GetBusinessEmployeesByEntGroups"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>

<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
  	<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.BUSINESS_ADMIN%>" />
<ffi:process name="FilterNotEntitledEmployees"/>

<%--get accounts info from GetAccountsByBusinessEmployee, stores results in session as BusEmpAccounts--%>
<ffi:object id="GetAccountsByBusinessEmployee" name="com.ffusion.tasks.accounts.GetAccountsByBusinessEmployee" scope="session"/>
	<ffi:setProperty name="GetAccountsByBusinessEmployee" property="AccountsName" value="BusEmpAccounts"/>
<ffi:process name="GetAccountsByBusinessEmployee"/>
<ffi:removeProperty name="GetAccountsByBusinessEmployee"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />

<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>


<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<!-- Dual Approval -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="Business"/>
	<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetDAItem" />
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_BAI_EXPORT_SETTINGS%>"/>
	<ffi:process name="GetDACategoryDetails" />	
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="notEquals">
	 <script>
	  	$("#baiExportSettings >span").addClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyBAISummary >span").addClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="equals">
	 <script>
	  	$("#baiExportSettings >span").removeClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyBAISummary >span").removeClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:removeProperty name="MultipleCategories"/>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
	<ffi:process name="GetDACategoryDetails" />	
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="notEquals">
	 <script>
	  	$("#administrators >span").addClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyAdministratorSummary >span").addClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="equals">
	 <script>
	  	$("#administrators >span").removeClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyAdministratorSummary >span").removeClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:removeProperty name="MultipleCategories"/>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ACCOUNT_CONFIGURATION%>"/>
	<ffi:process name="GetDACategoryDetails" />	
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="notEquals">
	 <script>
	  	$("#accountConfiguration >span").addClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyAccConfSummary >span").addClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="equals">
	 <script>
	  	$("#accountConfiguration >span").removeClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyAccConfSummary >span").removeClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:removeProperty name="MultipleCategories"/>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_TRANSACTION_GROUPS%>"/>
	<ffi:process name="GetDACategoryDetails" />	
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="notEquals">
	 <script>
	  	$("#tranGroups >span").addClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyTransactionGrpSummary >span").addClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="equals">
	 <script>
	  	$("#tranGroups >span").removeClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyTransactionGrpSummary >span").removeClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:removeProperty name="MultipleCategories"/>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT%>"/>
	<ffi:process name="GetDACategoryDetails" />	
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="notEquals">
	 <script>
	  	$("#permissions >span").addClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyPermissionsSummary >span").addClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:cinclude value1="${MultipleCategories.size}" value2="0" operator="equals">
	 <script>
	  	$("#permissions >span").removeClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyPermissionsSummary >span").removeClass("columndataDA");
	  </script>
	</ffi:cinclude>
	<ffi:removeProperty name="MultipleCategories"/>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails" />
	
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	  <ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="Business"/>
	  <ffi:process name="PopulateObjectWithDAValues"/>
	  <script>
	  	$("#profile >span").addClass("columndataDA");
		$("#dashboardSubmenuItemListId li>#showdbAdminCompanyProfileSummary >span").addClass("columndataDA");
	  </script>	 	 
	</ffi:cinclude>
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
		<script>
			$("#profile >span").removeClass("columndataDA");
			$("#dashboardSubmenuItemListId li>#showdbAdminCompanyProfileSummary >span").removeClass("columndataDA");
		</script>
	</ffi:cinclude>

        <%-- Read BAI Export settings from DA --%>
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.OLD_BAIEXPORT_SETTINGS %>" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.NEW_BAIEXPORT_SETTINGS %>" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS %>" />
	<ffi:object name="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA" id="AddBAIExportSettingsToDA" scope="request" />
	<ffi:setProperty name="AddBAIExportSettingsToDA" property="ReadFromDA" value="true" />
	<ffi:setProperty name="AddBAIExportSettingsToDA" property="TmpBankIdDisplayText" value="${TempBankIdentifierDisplayText}" />
	<ffi:process name="AddBAIExportSettingsToDA" />

	<ffi:cinclude value1="${DAItem}" value2="" operator="equals">
		<script> ns.admin.pendingApprovalCompanyChangeNum = "0"; $('#company_PendingApprovalTab').hide();</script>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
		<script> 
			ns.admin.pendingApprovalCompanyChangeNum = "1"; 
			ns.company.showPendingCompanyGrid();
			ns.company.updateCompanyDashboardEditLinks('<ffi:getProperty name="DAItem" property="status"/>');
		</script>
	</ffi:cinclude>

	<ffi:removeProperty name="GetDAItem"/>
	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="PopulateObjectWithDAValues"/>
</ffi:cinclude>

<%-- End Dual Approval --%>
