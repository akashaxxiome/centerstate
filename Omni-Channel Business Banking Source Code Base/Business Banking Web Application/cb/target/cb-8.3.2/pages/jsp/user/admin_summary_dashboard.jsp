<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-adminSummary"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_197" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/adminSubmenuDropdown.jsp" />
    	</div>
    	
    	<!-- dashboard items listing for transfers -->
        <div id="dbAdministrableAreasSummary" class="dashboardSummaryHolderDiv">
	            <sj:a id="summaryBtnLink" href='<ffi:getProperty name="tab" property="Href"/>' button="true" onClickTopics="" onCompleteTopics="" cssClass="summaryLabelCls">
					<s:text name="jsp.default_400" />
				</sj:a>
		</div>
</div>
<script>    
	$("#summaryBtnLink").find("span").addClass("dashboardSelectedItem");
</script>

