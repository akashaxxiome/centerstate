<%@page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<s:include value="inc/admin-init.jsp"/>

<s:include value="inc/index-pre.jsp"/>

<script src="<s:url value='/web/js/user/admin%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/admin_grid%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/users%{#session.minVersion}.js'/>" type="text/javascript"></script>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.USER_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.USER_ADMIN%>">
	
<div id="desktop" align="center">
    <style type="text/css">
        .indent1 { padding-left: 2em; }
        .indent2 { padding-left: 4em; }
        .indent3 { padding-left: 6em; }
        .indent4 { padding-left: 8em; }
    </style>

	<div id="appdashboard">
		<s:include value="users_dashboard.jsp"/> 
	</div>
    <div id="operationresult">
        <div id="resultmessage"></div>
    </div>
	<div id="details">
		<s:include value="admin_details.jsp"/>
	</div>
	<div id="permissionsDiv" class="adminBackground sectionsubhead"/>
	<div id="summary">
		<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
			<ffi:cinclude value1="${summaryToLoad}" value2="ProfilesSummary"	operator="notEquals">
					<s:include value="/pages/jsp/user/admin_users_summary.jsp"/>
			</ffi:cinclude>
		</ffi:cinclude>	
		</div>
	
		<div id="profileSummary">
		<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
			<ffi:cinclude value1="${summaryToLoad}" value2="ProfilesSummary"	operator="equals">
				<ffi:cinclude value1="${Business.EntSysType}" value2="<%= ProfileDefines.ENT_SYS_TYPE_PROFILE%>" operator="equals">
					<s:include value="/pages/jsp/user/businessuserprofiles_summary.jsp"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.EntSysType}" value2="<%= ProfileDefines.ENT_SYS_TYPE_PROFILE%>" operator="notEquals">
					<s:include value="/pages/jsp/user/admin_profiles_summary.jsp"/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>		
		</div>
	
	<!-- <div id="cloneUsersSummary">
		
	</div> -->

</div>

<script type="text/javascript">
	/* $('#appdashboard').pane({
		title: js_dashboard,
		minmax: true,
		close: false
	});
	$('#appdashboard').pane('hide'); */
</script>

<sj:dialog id="deleteUserDialogID" cssClass="adminDialog" title="%{getText('jsp.user_109')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" height="auto" position="['middle','center']">
</sj:dialog>
<sj:dialog id="deleteProfileDialogID" cssClass="adminDialog" title="%{getText('jsp.user_107')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500" position="['middle','center']">
</sj:dialog>

<sj:dialog id="approvalWizardDialogID" cssClass="adminDialog" title="%{getText('jsp.user.ApprovalWizard')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="785" height="500"  position="['middle','center']" cssStyle="overflow:hidden;">
</sj:dialog>
<sj:dialog id="viewUserInformationDialogID" cssClass="adminDialog" title="%{getText('jsp.user_View_User_Information')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="810" height="auto">
</sj:dialog>
<sj:dialog id="viewProfileInformationDialogID" cssClass="adminDialog" title="%{getText('jsp.user.viewProfileInformation')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>

<sj:dialog id="verifyDiscardUserDialogID" cssClass="adminDialog" title="%{getText('jsp.user_555')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>
<sj:dialog id="verifyDiscardProfileDialogID" cssClass="adminDialog" title="%{getText('jsp.user.verifyDiscardProfile')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>

<sj:dialog id="submitUserForApprovalDialogID" cssClass="adminDialog" title="%{getText('jsp.user_556')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>
<sj:dialog id="submitProfileForApprovalDialogID" cssClass="adminDialog" title="%{getText('jsp.user.verifySubmitProfileForApproval')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>

<sj:dialog id="rejectUserChangeDialogID" cssClass="adminDialog" title="%{getText('jsp.user.profilesWithPendingChanges')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>
<sj:dialog id="rejectProfileChangeDialogID" cssClass="adminDialog" title="%{getText('jsp.user.profilesWithPendingChanges')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>


<script type="text/javascript">

	$(document).ready(function(){
		ns.admin.showUsersDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
		
		$("#approvalWizardDialogID").parent().css("max-width", "785px")
	});
	
	$('#deleteUserDialogID').addHelp(function(){
		var helpFile = $('#deleteUserDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		});
	$('#deleteProfileDialogID').addHelp(function(){
		var helpFile = $('#deleteProfileDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
 	});
	
	</script>
</ffi:cinclude>
