<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setL10NProperty name='PageHeading' value='Verify Submit User for Approval'/>

<%
	session.setAttribute("itemId", request.getParameter("itemId"));
    session.setAttribute("itemType", request.getParameter("itemType"));
    session.setAttribute("category", request.getParameter("category"));
%>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
<ffi:process name="SetBusinessEmployee"/>
<ffi:removeProperty name="SetBusinessEmployee"/>

<ffi:object name="com.ffusion.tasks.dualapproval.SubmitForApproval" id="SubmitForApprovalTask" />
<% 
	session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("SubmitForApprovalTask")); 
%>


		<div align="center">
		<s:form id="submitUserForApprovalFormID" namespace="/pages/dualapproval" validate="false" action="dualApprovalAction-submitForApproval" method="post" name="SubmitUserForApprovalForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>"/>
			<input type="hidden" name="module" value="<ffi:getProperty name='itemType'/>"/>
		<table class="adminBackground" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						
						<tr>
							<td class="columndata" align="center">
								<s:text name="jsp.user.submitUserChangesForApproval">
									<s:param><ffi:getProperty name="BusinessEmployee" property="FullName"/></s:param>
								</s:text>
							</td>
							<td></td>
						</tr>
						
						<tr>
							<td><img src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></td>
							<td></td>
						</tr>
						<tr>
							<td>
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a id="cancelSubmitForApprovalUserID" 
									button="true" 
									onClickTopics="closeDialog"
								>
									<s:text name="jsp.default_82" />
								</sj:a>
								 <sj:a 
									id="verifySubmitForApprovalUserID"
									formIds="submitUserForApprovalFormID"
									targets="result"
	                                button="true"
									onCompleteTopics="submitUserForApprovalCompleteTopics,closeDialog"
									><s:text name="jsp.default_175" />
					            </sj:a>								
							</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</s:form>
			<p></p>
		</div>

