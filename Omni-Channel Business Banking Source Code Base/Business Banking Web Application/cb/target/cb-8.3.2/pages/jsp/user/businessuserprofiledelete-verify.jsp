<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminprofiledelete-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_370')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<div id="wholeworld">
		<div align="center">

		<div class="overflowXAuto">
		<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminprofiledelete-verify.jsp-1" parm0="${BusinessUserProfile.ProfileName}"/></div>
		<div style="height:50px;" class="ffivisible">&nbsp;</div>
			<div align="center" class="ui-widget-header customDialogFooter">
				<sj:a id="cancelDeleteUserProfileLink" 
				      button="true" 
					  summaryDivId="profileSummary" 
					  buttonType="cancel"
					  onClickTopics="showSummary,closeDeleteProfileDialog"
					  title="%{getText('jsp.default_83')}" 
					  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
				<s:url id="deleteUserProfileUrl" escapeAmp="false" value="/pages/jsp/businessuserprofile/deleteBusinessUserProfile_execute.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="BusinessUserProfile.ProfileEntGroupId" value="%{BusinessUserProfile.ProfileEntGroupId}"></s:param>
				</s:url>
				<sj:a id="deleteUserProfileLink" href="%{deleteUserProfileUrl}"
					  targets="wholeworld"
					  button="true" 
					  title="%{getText('jsp.user_397')}"
					  onCompleteTopics="completeDeleteUserProfileVerify" onErrorTopics="errorDeleteUser"
					  effectDuration="1500"
					  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_162" /></sj:a>
			</div>
		</div>
</div>
