<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

    <div id="appdashboard-left" class="formLayout">
		<%-- user permission (Clone user permissions) button, it would be shown only in user permission, otherwise hidden. --%>
	    <s:url id="cloneUserPermissionsUrl" value="%{#session.PagesPath}user/clone-user-permissions-init.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
	    <sj:a 
			id="cloneUserPermissionsButton" 
			href="%{cloneUserPermissionsUrl}" 
			targets="inputDiv" 
			button="true" 
			buttonIcon="ui-icon-copy" 
	    	title="%{getText('jsp.user_71')}"
			onClickTopics="beforeCloneUserAndAccountPermLoad"
			onCompleteTopics="completeLoad" 
			onErrorTopics="errorLoad"
			>
		  	&nbsp;<s:text name="jsp.user_71.1"/></sj:a>
		<%-- user permission (Clone account permissions) button, it would be shown only in user permission, otherwise hidden. --%>
	    <s:url id="cloneAccountPermissionsUrl" value="%{#session.PagesPath}user/clone-account-permissions-init.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
		</s:url>
	    <sj:a 
			id="cloneAccountPermissionsButton" 
			href="%{cloneAccountPermissionsUrl}" 
			targets="inputDiv" 
			button="true" 
			buttonIcon="ui-icon-copy" 
	    	title="%{getText('jsp.user_392')}"
			onClickTopics="beforeCloneUserAndAccountPermLoad"
			onCompleteTopics="completeLoad" 
			onErrorTopics="errorLoad"
			>
		  	&nbsp;<s:text name="jsp.user_392.1" /></sj:a>
		<s:url id="editDivisionUrl" value="/pages/jsp/user/corpadmindivedit.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a id="editDivisionLink" href="%{editDivisionUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
			cssStyle="display:none;" title="%{getText('jsp.user_141')}"
			onClickTopics="beforeLoad" onCompleteTopics="completeDivisionLoad" onErrorTopics="errorLoad"><s:text name="jsp.user_141"/></sj:a>
		<s:url id="editGroupUrl" value="/pages/jsp/user/corpadmingroupedit.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a id="editGroupLink" href="%{editGroupUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
			cssStyle="display:none;" title="%{getText('jsp.user_143')}"
			onClickTopics="beforeLoad" onCompleteTopics="completeLoad" onErrorTopics="errorLoad"><s:text name="jsp.user_143"/></sj:a>
		<s:url id="locationsUrl" value="/pages/jsp/user/corpadminlocations.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a id="locationsLink" href="%{locationsUrl}" targets="locationsDiv" button="true" buttonIcon="ui-icon-wrench"
			cssStyle="display:none;" title="%{getText('jsp.default_563')}"
			onClickTopics="beforeLocLoad" onCompleteTopics="completeLocLoad" onErrorTopics="errorLoad"><s:text name="jsp.default_563"/></sj:a>
		<s:url id="editAdminUrl" value="/pages/jsp/user/corpadminadminedit.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a id="editAdminLink" href="%{editAdminUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
			cssStyle="display:none;" title="%{getText('jsp.user_393')}"
			onClickTopics="beforeLoad" onCompleteTopics="completeLoad" onErrorTopics="errorLoad"><s:text name="jsp.user_393" /></sj:a>

    </div>
    <div id="appdashboard-right" >
		<span class="ui-helper-clearfix">&nbsp;</span>
    </div>
	<span class="ui-helper-clearfix">&nbsp;</span>

	<%-- Following three dialogs are used for view, delete and inquiry transfer --%>
	