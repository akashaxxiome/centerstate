<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
<style>
/* #selectBoxHolder{float:none;} */
</style>
<div class="dashboardUiCls">
   	<div class="moduleSubmenuItemCls">
   		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-adminDivision"></span>
   		<span class="moduleSubmenuLbl">
   			<s:text name="jsp.home_83" />
   		</span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
		<!-- dropdown menu include -->    		
   		<s:include value="/pages/jsp/home/inc/adminSubmenuDropdown.jsp" />
  	</div>
   	
   	<!-- dashboard items listing for admin -->
	<div id="dbDivisionSummary" class="dashboardSummaryHolderDiv">
		
		<s:url id="divionsSummaryUrl" value="/pages/jsp/user/admin_divisions_summary.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		
		<sj:a id="divisionsLink" href="%{divionsSummaryUrl}" targets="summary" button="true" 
			title="%{getText('jsp.home_197')}" cssClass="summaryLabelCls" 
			onClickTopics="" onCompleteTopics="" onErrorTopics="" onclick="ns.admin.closeDivisionDetails()"><s:text name="jsp.home_197"/></sj:a>
		
		 <ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
              <!-- <s:url id="addDivisionURL" value="/pages/jsp/user/corpadmindivadd.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="UseLastRequest">FALSE</s:param>
			 </s:url> -->
			 <s:url id="addDivisionURL" value="/pages/user/AddDivision_init.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="UseLastRequest">FALSE</s:param>
			 </s:url>
              <sj:a id="addDivisionLink"
                 href="%{addDivisionURL}"
                 targets="inputDiv"
                 onClickTopics="beforeLoad" onCompleteTopics="completeDivisionLoad" onErrorTopics="errorLoad"
                 button="true"><s:text name="jsp.user_15"/></sj:a>
          </ffi:cinclude> 
          
          <s:url id="editDivisionUrl" value="/pages/jsp/user/corpadmindivedit.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a id="editDivisionLink" href="%{editDivisionUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
			cssStyle="display:none;" title="%{getText('jsp.user_141')}"
			onClickTopics="beforeLoad" onCompleteTopics="completeDivisionLoad" onErrorTopics="errorLoad"><s:text name="jsp.user_141"/></sj:a>
			
			<s:url id="divisionPermissionsUrl" value="/pages/jsp/user/admin_permissions.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="divisionPermissionsLink" href="%{divisionPermissionsUrl}" targets="permissionsDiv" button="true" cssStyle="display:none;" title="%{getText('jsp.default_569')}"
				onClickTopics="beforePermLoad" onCompleteTopics="completeDivisionPermLoad" onErrorTopics="errorLoad">
			  <span class="ui-icon ui-icon-wrench"></span>
			</sj:a>
	</div>
	
	<!-- dashboard items listing for requested images, hidden by default -->
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>" >
		<div id="dbLocationSummary" class="dashboardSummaryHolderDiv">
			
			<div id="divisionButtonPanel" style="display: inline-block;" class="dashboardCustomDiv">
			   	<s:url id="locationsSummaryUrl" value="/pages/jsp/user/admin_locations_summary.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				
				<sj:a id="locationsLink" href="%{locationsSummaryUrl}" targets="summary" button="true" 
					title="%{getText('user.location.summary')}" cssClass="summaryLabelCls" cssStyle="float:left" 
					onClickTopics="beforeLocLoad" onCompleteTopics="completeLocLoad" onErrorTopics="errorLoad"><s:text name="user.location.summary"/></sj:a>
				
				
					<%-- <span class="sectionsubhead marginTop10 marginRight10" style="display:inline-block; float:left;"><!--L10NStart-->Division<!--L10NEnd--> </span> --%>
					<select id="dashBoardDivisionId" class="txtbox" name="" onchange="loadLocationData()" >
						<ffi:cinclude value1="" value2="<ffi:getProperty name='EditGroup_DivisionName'/>" operator="notEquals">
						</ffi:cinclude>
					</select>
					
				<s:url id="addLocationURL" value="/pages/jsp/user/corpadminlocadd.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="addlocation-reload" value="true"/>
				</s:url>
				<sj:a id="addLocationLink"
					  href="%{addLocationURL}"
					  targets="inputDiv"
					  onClickTopics="beforeLoad"  cssStyle="margin-left:10px"
					  onCompleteTopics="completeDivisionLoad"
					  button="true"><s:text name="jsp.user_20"/></sj:a>	
				
			</div>
				  
			<s:url id="editLocationUrl" value="/pages/jsp/user/corpadminlocedit.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="editLocationLink" href="%{editLocationUrl}" targets="inputDiv" cssStyle="display:none" button="true" title="%{getText('jsp.user_144')}"
				  onClickTopics="beforeLoad" onCompleteTopics="completeDivisionLoad" onErrorTopics="errorLoad">
				<span class="ui-icon ui-icon-wrench"></span>
			</sj:a> 
		</div>
	</ffi:cinclude>
	
	
	<!-- More list option listing -->
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>" >
		<div id="divisionMoreOptions" class="dashboardSubmenuItemCls">
			<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
	   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
			<!-- UL for rendering tabs -->
			<ul class="dashboardSubmenuItemList">
				<li class="dashboardSubmenuItem"><a id="showdbDivisionSummary" onclick="ns.common.refreshDashboard('showdbDivisionSummary',true)"><s:text name="jsp.home_83"/></a></li>
				<li class="dashboardSubmenuItem"><a id="showdbLocationSummary" onclick="loadLocationData()"><s:text name="jsp.user_190"/></a></li>
			</ul>
		</div>
	</ffi:cinclude>

</div> 


<script>
	$("#divisionsLink").find("span").addClass("dashboardSelectedItem");
	$(function(){
        $("#dashBoardDivisionId").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"size":"30",
			"source":"/cb/pages/user/DivisionLookupBoxAction.action?CollectionName=Descendants&dualApprovalMode=2"
		});
       // $("#dashBoardDivisionId").selectmenu({width:'9em'});
    });
	
</script>
