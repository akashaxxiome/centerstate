<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_86')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<%@page import="com.ffusion.util.logging.TrackingIDGenerator" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:setProperty name="CanAdministerAnyGroup" property="GroupType" value="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>"/>
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<%
	String ItemId = request.getParameter("ItemId");
	if(ItemId!= null){
		session.setAttribute("ItemId", ItemId);
	}
%>

<ffi:help id="user_corpadmingroupadd-confirm" className="moduleHelpClass"/>
<%-- Add to DA table if dual approval is enabled. --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="AddBusinessGroup" property="ValidateOnly" value="true"/>
	<ffi:process name="AddBusinessGroup"/>
	<% String trackingId = null; %>
	<ffi:cinclude value1="${ItemId}" value2="">
		<% trackingId = TrackingIDGenerator.GetNextID(); %>
	</ffi:cinclude>
	<ffi:cinclude value1="${ItemId}" value2="" operator="notEquals">
		<% trackingId = (String) session.getAttribute("ItemId"); %>
	</ffi:cinclude>
	<ffi:cinclude value1="${adminMembers}" value2="" operator="notEquals">
		<ffi:object id="GetAdminIds" name="com.ffusion.tasks.dualapproval.GetAdminIds" scope="session"/>
		<ffi:setProperty name="GetAdminIds" property="AdminIdsSessionName" value="adminIds"/>
		<ffi:setProperty name="GetAdminIds" property="AdminListName" value="adminMembers"/>
		<ffi:process name="GetAdminIds"/>
	</ffi:cinclude>
	<ffi:object id="AddBusinessGroupToDA" name="com.ffusion.tasks.dualapproval.AddBusinessGroupToDA" scope="session"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="UserAction" value="<%= String.valueOf(IDualApprovalConstants.USER_ACTION_ADDED) %>"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="NewEntitlementGroupSessionName" value="NewEntitlementGroupDA"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="ItemId" value="<%= trackingId %>"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="CategoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
	<ffi:process name="AddBusinessGroupToDA"/>
	<ffi:cinclude value1="${adminMembers}" value2="" operator="notEquals">
		<ffi:object id="SetAdministratorsInDA" name="com.ffusion.tasks.dualapproval.SetAdministratorsInDA"/>
		<ffi:setProperty name="SetAdministratorsInDA" property="AddedMemberIds" value="adminIds"/>
		<ffi:setProperty name="SetAdministratorsInDA" property="ItemId" value="<%= trackingId %>"/>
		<ffi:setProperty name="SetAdministratorsInDA" property="UserAction" value="<%= String.valueOf(IDualApprovalConstants.USER_ACTION_ADDED) %>"/>
		<ffi:setProperty name="SetAdministratorsInDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:process name="SetAdministratorsInDA"/>
	</ffi:cinclude>
	<ffi:removeProperty name="AddBusinessGroupToDA"/>
	<ffi:removeProperty name="NewEntitlementGroupDA"/>
	<ffi:removeProperty name="adminIds"/>
	<ffi:removeProperty name="adminMembers"/>
	<ffi:removeProperty name="GetAdminIds"/>
	<ffi:removeProperty name="SetAdministratorsInDA"/>
	<ffi:removeProperty name="ItemId"/>
</ffi:cinclude>
<div align="center">

	<TABLE class="adminBackground" width="490" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
					<TR>
						<TD class="columndata textWrapInTd" align="center" style="width:120px;">
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmingroupadd-confirm.jsp-1" parm0="${AddBusinessGroup.GroupName}"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmingroupadd-confirm.jsp-2" parm0="${AddBusinessGroup.GroupName}"/>
							</ffi:cinclude>
						</TD>
					</TR>
					<TR>
						<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
					</TR>
					<TR>
						<td align="center">
							<sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,cancelGroupForm"><s:text name="jsp.default_175"/></sj:a>
						</td>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
	<p></p>
</div>
<ffi:removeProperty name="AddBusinessGroup"/>
<ffi:removeProperty name="DivisionEntGroup"/>
<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadmingroups-pre.jsp"/>
