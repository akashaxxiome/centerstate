<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<ffi:setL10NProperty name='PageHeading' value='Verify Discard Business'/>

<% session.setAttribute("itemId",request.getParameter("itemId"));%>
<% session.setAttribute("itemType",request.getParameter("itemType"));%>

<ffi:object id="DiscardPendingChangeTask" name="com.ffusion.tasks.dualapproval.DiscardPendingChange"  />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemType" value="${itemType}" />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemId" value="${itemId}" />
<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("DiscardPendingChangeTask") ); %>
		<div align="center">

		<table class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="columndata" align="center" height="30"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-verify-discard-business.jsp-1" parm0="${Business.BusinessName}"/></td>
							<td></td>
						</tr>
						<tr>
							<td>
							<div align="center" class="ui-widget-header customDialogFooter">
							<%-- 
								<input class="submitbutton" tabindex="15" type="button" value="Discard" onclick="document.location='<ffi:urlEncrypt url="da-discard-changes.jsp?itemId=${itemId}&successUrl=${successUrl}&itemType=${itemType}"/>'">
								<input class="submitbutton" tabindex="12" type="button" value="Cancel" onclick="document.location='<ffi:getProperty name='SecurePath'/>user/corpadmininfo.jsp'">
							--%>
									<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>

									<s:url id="discardChangesUrl" escapeAmp="false" value="/pages/dualapproval/dualApprovalAction-discardChanges.action?module=Business&daAction=Discarded">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
									</s:url>

									<sj:a id="discardChangesLink"
									href="%{discardChangesUrl}"
								  	targets="resultmessage"
									button="true"
									title="%{getText('jsp.default_Discard')}"
									onClickTopics=""
									onSuccessTopics="DACompanyDiscardChangesCompleteTopics"
									onCompleteTopics="submitPendingApprovalForCompany"
									onErrorTopics="">
									<s:text name="jsp.default_Discard" />
									</sj:a>

							</td>
							</div>
						</tr>
					</table>
				</td>
			</tr>
		</table>
			<p></p>
		</div>
