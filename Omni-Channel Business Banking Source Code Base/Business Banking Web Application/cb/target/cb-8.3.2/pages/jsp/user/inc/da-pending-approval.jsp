<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- 
If user has navigated to this page from home page then DACompanyCount has value >=0. If user made some changes to
company,division,group or user page and then clicked Summary page then we need to calculate the count again. 
--%>

<ffi:cinclude value1="${DACompanyCount}" value2="">
	<ffi:setProperty name="DACompanyCount" value="0"/>
	<ffi:setProperty name="DADivisionCount" value="0"/>
	<ffi:setProperty name="DAGroupCount" value="0"/>
	<ffi:setProperty name="DAUserCount" value="0"/>
	<ffi:object id="GetPendingDAItemsCount" name="com.ffusion.tasks.dualapproval.GetPendingDAItemsCount" scope="request"/>
	<ffi:setProperty name="GetPendingDAItemsCount" property="ItemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:process name="GetPendingDAItemsCount"/>
	<ffi:setProperty name="DACompanyCount" value="${GetPendingDAItemsCount.PendingCount}"/>
	<ffi:setProperty name="GetPendingDAItemsCount" property="ItemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:process name="GetPendingDAItemsCount"/>
	<ffi:setProperty name="DADivisionCount" value="${GetPendingDAItemsCount.PendingCount}"/>
	<ffi:setProperty name="GetPendingDAItemsCount" property="ItemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
	<ffi:process name="GetPendingDAItemsCount"/>
	<ffi:setProperty name="DAGroupCount" value="${GetPendingDAItemsCount.PendingCount}"/>
	<ffi:setProperty name="GetPendingDAItemsCount" property="ItemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
	<ffi:process name="GetPendingDAItemsCount"/>
	<ffi:setProperty name="DAUserCount" value="${GetPendingDAItemsCount.PendingCount}"/>
	<ffi:removeProperty name="GetPendingDAItemsCount"/>
</ffi:cinclude>
<% 
	int count = Integer.parseInt((String)session.getAttribute("DACompanyCount"));
	count += Integer.parseInt((String)session.getAttribute("DADivisionCount"));
	count += Integer.parseInt((String)session.getAttribute("DAGroupCount"));
	count += Integer.parseInt((String)session.getAttribute("DAUserCount"));
	session.setAttribute("count", String.valueOf(count));
%>
<ffi:cinclude value1="${count}" value2="0" operator="notEquals">
<ffi:setProperty name="transtabs" property="Tab" value="completed"/>
<ffi:setProperty name="transtabs" property="Href" value="/cb/pages/jsp/wires/wires_completed_summary.jsp"/>
<ffi:setProperty name="transtabs" property="LinkId" value="completed"/>
<ffi:setProperty name="transtabs" property="Title" value="Completed"/>
<ffi:process name="transtabs"/>

<div id="pendingItems_SummaryTab" class="portlet">
<%-- 	<ul class="portlet-header">
		<li>
			<a href="#pendingAdminItemsSummaryDiv"><s:text name="jsp.user_558"/></a>
		</li>
	</ul> --%>
	<div id="pendingAdminItemsSummaryDiv" class="portlet-content">
	<div class="paneWrapper">
  		<div class="paneInnerWrapper">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData">
		
			<tr height="20" class="header">
				<td height="1">&nbsp;</td>
				<td height="1" class="sectionsubhead" align="left" width="50%"><s:text name="jsp.user_559"/></td>
				
				<td height="1" class="sectionsubhead" align="left"><s:text name="jsp.user_560"/></td>
				<td height="1">&nbsp;</td>
			</tr>
		
			<ffi:cinclude value1="${DACompanyCount}" value2="0" operator="notEquals">
				<tr class="columndata ltrow3" height="20">
					<td height="1"></td>
					<ffi:cinclude value1="${DACompanyCount}" value2="0">
						<td height="1"><s:text name="jsp.user_561" /></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${DACompanyCount}" value2="0" operator="notEquals">
						<td height="1" class="columndata">
							<a title='<s:text name="jsp.user_561" />' href='#' onClick='ns.admin.guideUserToSubmenu( "admin_company" );'>
								<s:text name="jsp.user_561" />
							</a>
						</td>
					</ffi:cinclude>
					<td class="columndata">&nbsp;&nbsp;<ffi:getProperty name="DACompanyCount"/></td>
					<td height="1">&nbsp;</td>
				</tr>
			</ffi:cinclude>
			
			<ffi:cinclude value1="${DADivisionCount}" value2="0" operator="notEquals">
				<tr class="columndata dkrow" height="20">
					<td height="1"></td>
					<ffi:cinclude value1="${DADivisionCount}" value2="0">
						<td height="1"><s:text name="jsp.user_562" /></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${DADivisionCount}" value2="0" operator="notEquals">
						<td height="1" class="columndata">
							<a title='<s:text name="jsp.user_562" />' href='#' onClick='ns.admin.guideUserToSubmenu( "admin_division" );'>
								<s:text name="jsp.user_562" />
							</a>
						</td>
					</ffi:cinclude>
					<td class="columndata">&nbsp;&nbsp;<ffi:getProperty name="DADivisionCount"/></td>
					<td height="1">&nbsp;</td>
				</tr>
			</ffi:cinclude>
			
			<ffi:cinclude value1="${DAGroupCount}" value2="0" operator="notEquals">
				<tr class="columndata ltrow3" height="20">
					<td height="1"></td>
					<ffi:cinclude value1="${DAGroupCount}" value2="0">
						<td height="1"><s:text name="jsp.user.Groups" /></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${DAGroupCount}" value2="0" operator="notEquals">
						<td height="1" class="columndata">
							<a title='<s:text name="jsp.user.Groups" />' href='#' onClick='ns.admin.guideUserToSubmenu( "admin_group" );'>
								<s:text name="jsp.user.Groups" />
							</a>
						</td>
					</ffi:cinclude>
					<td class="columndata">&nbsp;&nbsp;<ffi:getProperty name="DAGroupCount"/></td>
					<td height="1">&nbsp;</td>
				</tr>
			</ffi:cinclude>
			
			<ffi:cinclude value1="${DAUserCount}" value2="0" operator="notEquals">
				<tr class="columndata dkrow" height="20">
					<td height="1"></td>
					<ffi:cinclude value1="${DAUserCount}" value2="0">
						<td height="1"><s:text name="jsp.user_366" /></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${DAUserCount}" value2="0" operator="notEquals">
						<td height="1" class="columndata">
							<a title='<s:text name="jsp.user_366" />' href='#' onClick='ns.admin.guideUserToSubmenu( "admin_user" );'>
								<s:text name="jsp.user_366" />
							</a>
						</td>
					</ffi:cinclude>
					
					<td class="columndata">&nbsp;&nbsp;<ffi:getProperty name="DAUserCount"/><br></td>
					<td height="1">&nbsp;</td>
				</tr>
			</ffi:cinclude>
			
		</table></div></div>
	</div>
</div>
</ffi:cinclude>
<script>    
$(document).ready(function(){
	var DACompanyCount = '<s:property value="%{#session.DACompanyCount}"/>';
	var DADivisionCount = '<s:property value="%{#session.DADivisionCount}"/>';
	var DAGroupCount = '<s:property value="%{#session.DAGroupCount}"/>';
	var DAUserCount = '<s:property value="%{#session.DAUserCount}"/>';
	if(DACompanyCount == 0 && DADivisionCount ==0 && DAGroupCount==0 && DAUserCount == 0){
		$('#company_PendingApprovalTab').hide();
	} else {
		$('#company_PendingApprovalTab').show();
	}
});		
</script>
<%-- <ffi:removeProperty name="DACompanyCount"/>
<ffi:removeProperty name="DADivisionCount"/>
<ffi:removeProperty name="DAGroupCount"/>
<ffi:removeProperty name="DAUserCount"/>
<ffi:removeProperty name="count"/> --%>