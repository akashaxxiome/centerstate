<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadminaccountedit-confirm" className="moduleHelpClass"/>
<%-- This task modfies the account in session to the backend --%>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<%--Dual Approval --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="ModifyAccount" property="Process" value="false"/>
	<ffi:process name="ModifyAccount"/>
	
	<ffi:object id="AddAccountConfigurationToDA" name="com.ffusion.tasks.dualapproval.AddAccountConfigurationToDA" />
		<ffi:setProperty name="AddAccountConfigurationToDA" property="categoryType" value="Account Configuration"/>
		<ffi:setProperty name="AddAccountConfigurationToDA" property="objectId" value="${ModifyAccount.ID}-${ModifyAccount.RoutingNum}"/>
	<ffi:process name="AddAccountConfigurationToDA" />
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:setProperty name="ModifyAccount" property="Process" value="true"/>
	<ffi:process name="ModifyAccount"/>
</ffi:cinclude>
<%--Dual Approval Ends --%>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
    <ffi:object name="com.ffusion.tasks.register.EditRegisterAccountData" id="EditRegisterAccountData" scope="session" />
    <ffi:setProperty name="EditRegisterAccountData" property="BeanSessionName" value="ModifyAccount"/>
	<ffi:process name="EditRegisterAccountData"/>
</ffi:cinclude>


<ffi:removeProperty name="ModifyAccount"/>
<ffi:removeProperty name="AccountContact"/>


<div align="center"><s:text name="jsp.user_336"/></div>
<div class="btn-row">
<sj:a button="true" 
summaryDivId="companySummaryTabsContent" buttonType="done" 
onClickTopics="showSummary,cancelCompanyForm,refreshAcctConfigGrid"><s:text name="jsp.default_175"/></sj:a>
</div>	
<ffi:removeProperty name="oldNickName"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="oldState"/>
<ffi:removeProperty name="oldCountry"/>
