<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%
session.setAttribute("runSearch", request.getParameter("runSearch"));
%>

<s:include value="corpadminaccountsview-pre.jsp?runSearch=%{#session.runSearch}" />

<div id="accountConfigDiv" align="center">

	<%-- include page header --%>
	<table width="676" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<div align="center">
					<span class="sectionhead">
					<ffi:getProperty name="Business" property="BusinessName"/><br>
				</span>
				</div>
			</td>
		</tr>
	</table>
	
	<s:include value="inc/accountsearch.jsp" />
	
	<div id="companyAccountConfigGrid">
		<ffi:cinclude value1="${runSearch}" value2="true">		
			<s:include value="%{#session.PagesPath}user/corpadminaccountsview-grid.jsp"></s:include>
		</ffi:cinclude>
	</div>

</div>
<ffi:removeProperty name="AccountTypesList"/>
<ffi:removeProperty name="highLightField"/>

