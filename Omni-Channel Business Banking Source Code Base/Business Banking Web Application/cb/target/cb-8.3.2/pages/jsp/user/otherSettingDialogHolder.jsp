<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:url id="pageurl" value="/pages/jsp/user/otherSettingDialog.jsp" escapeAmp="false">

</s:url>
<sj:dialog
	id="otherSettingDialogId"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.default_Other_Settings')}"
	height="400"
	width="580"
	href="%{pageurl}"
	showEffect="fold"
	hideEffect="clip"
	resizable="false"
>
</sj:dialog>