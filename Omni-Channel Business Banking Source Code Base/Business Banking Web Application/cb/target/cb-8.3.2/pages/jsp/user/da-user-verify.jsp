<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name='PageHeading' value='Pending Approval Summary'/>

<%
session.setAttribute("Section", request.getParameter("Section"));
session.setAttribute("itemId", request.getParameter("itemId"));
session.setAttribute("rejectFlag", request.getParameter("rejectFlag"));
session.setAttribute("userName", request.getParameter("userName"));
session.setAttribute("userAction", request.getParameter("userAction"));
session.setAttribute("itemType", request.getParameter("itemType"));
session.setAttribute("category", request.getParameter("category"));
%>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="/pages/jsp/inc/init/admin-init.jsp"/>
</ffi:cinclude>
<s:include value="/pages/jsp/user/inc/da-summary-common.jsp"/>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:cinclude value1="${categories}" value2="">
	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
	<ffi:process name="GetDAItem"/>
</ffi:cinclude>

<ffi:removeProperty name="DISABLE_APPROVE"/>

<div align="center">
	<ffi:setProperty name="subMenuSelected" value="users"/>
	<ffi:setProperty name="isPendingIsland" value="TRUE"/>

	<ffi:removeProperty name="isPendingIsland"/>
	<i><s:text name="jsp.da_approvals_pending_user_loading_changes" /></i>
<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<ffi:setProperty name="SuccessUrl" value="${successUrl}" />
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >
<ffi:setProperty name="subMenuSelected" value="users"/>
<%-- include page header --%>
	<ffi:object name="com.ffusion.tasks.dualapproval.ProcessDACategories" id="ProcessDACategories" />
	<ffi:process name="ProcessDACategories" />
	<s:include value="/pages/jsp/user/inc/da-processwizardmenu.jsp"/>
	</div>

<ffi:setProperty name="BackURL" value="${SecurePath}user/da-user-verify.jsp"/>
<ffi:removeProperty name="categories"/>