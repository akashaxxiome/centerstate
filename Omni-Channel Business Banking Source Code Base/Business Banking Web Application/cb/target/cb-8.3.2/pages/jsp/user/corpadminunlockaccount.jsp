<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%-- Update the password status appropriately --%>
<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="equals">
	<ffi:setProperty name="BusinessEmployee" property="PASSWORD_STATUS" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_CHANGE )%>"/>
</ffi:cinclude>
<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="equals">
	<ffi:setProperty name="BusinessEmployee" property="PASSWORD_STATUS" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_VALID )%>"/>
</ffi:cinclude>
<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="equals">
	<ffi:setProperty name="BusinessEmployee" property="PASSWORD_STATUS" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_CHANGE )%>"/>
</ffi:cinclude>

<ffi:setProperty name="BusinessEmployee" property="PASSWORD_FAIL_COUNT" value="0"/>
<ffi:object name="com.ffusion.tasks.user.EditBusinessEmployee" id="EditBusinessEmployee" scope="session" />
<ffi:setProperty name="EditBusinessEmployee" property="Initialize" value="true"/>
<ffi:process name="EditBusinessEmployee"/>
<ffi:setProperty name="EditBusinessEmployee" property="Initialize" value="false"/>
<ffi:setProperty name="EditBusinessEmployee" property="Process" value="true"/>
<ffi:process name="EditBusinessEmployee"/>

<%-- populate BusinessEmployees with employees for the business --%>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="GetEntitlementGroupSummaries" name="com.ffusion.tasks.user.GetEntitlementGroupSummaries" scope="session" />
<ffi:setProperty name="GetEntitlementGroupSummaries" property="NumSpaces" value="3" />
<ffi:process name="GetEntitlementGroupSummaries" />
<ffi:removeProperty name="GetEntitlementGroupSummaries" />

<%-- set BusinessEmployee to be the user being edited --%>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value='<%=request.getParameter("userId")%>'/>
<ffi:process name="SetBusinessEmployee"/>

<%
	Object be = session.getAttribute("BusinessEmployee");
	be = ((com.ffusion.beans.ExtendABean)be).clone();
	session.setAttribute( "NewBusinessEmployee", be );
	session.setAttribute("FFINewBusinessEmployee", session.getAttribute("NewBusinessEmployee"));
%>
<ffi:object name="com.ffusion.tasks.user.EditBusinessEmployee" id="EditBusinessEmployee" scope="session" />
<ffi:setProperty name="EditBusinessEmployee" property="Initialize" value="true"/>
<ffi:process name="EditBusinessEmployee"/>
<ffi:object id="EditBusinessUser" name="com.ffusion.tasks.admin.EditBusinessUser" scope="session"/>
<ffi:setProperty name="EditBusinessUser" property="OldBusinessName" value="BusinessEmployee"/>
<ffi:setProperty name="EditBusinessUser" property="Process" value="true"/>
<ffi:setProperty name="EditBusinessUser" property="EntitlementsName" value="Entitlement_Entitlements"/>
<ffi:setProperty name="EditBusinessUser" property="SourcePage" value="UserInfo"/>
<ffi:setProperty name="EditBusinessUser" property="GroupName" value="Entitlement_EntitlementGroup"/>

<ffi:setProperty name="NewBusinessEmployee" property="Password" value=""/>
<ffi:setProperty name="NewPassword" value=""/>
<ffi:setProperty name="ConfirmPassword" value=""/>
<ffi:setProperty name="NewPassword2" value=""/>
<ffi:setProperty name="ConfirmPassword2" value=""/>

<ffi:cinclude value1="${NewBusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="notEquals">
	<ffi:cinclude value1="${NewBusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="notEquals">
		<ffi:cinclude value1="${NewBusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="notEquals">
		<s:text name="jsp.user_351"/>
		<ffi:setProperty name="TempLockoutStatus" value="Unlocked"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>
