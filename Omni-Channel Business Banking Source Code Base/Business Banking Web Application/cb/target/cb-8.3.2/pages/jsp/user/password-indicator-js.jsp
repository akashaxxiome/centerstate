<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 

<script>
function isNumeric(aCharacter) { 
    return (aCharacter >= '0') && (aCharacter <= '9') 
} 

function isLowerCase(aCharacter) { 
    return (aCharacter >= 'a') && (aCharacter <= 'z') 
}

function isUpperCase(aCharacter) { 
    return (aCharacter >= 'A') && (aCharacter <= 'Z') 
} 

function lowerCaseChars ( pswd ) {		
    count = 0;
    for (var i=0; i<pswd.length; i++) {
        if ( isLowerCase ( pswd.charAt(i) ) ) {
            count++;
        }
    }
    return count;
}

function upperCaseChars ( pswd ) {
    count = 0;
    for (var i=0; i<pswd.length; i++) {
        if ( isUpperCase (  pswd.charAt(i) ) ) {
            count++;
        }
    }
    return count;
}		

function numericChars ( pswd ) {
    count = 0;
    for (var i=0; i<pswd.length; i++) {
        if ( isNumeric (  pswd.charAt(i) ) ) {
            count++;
        }
    }
    return count;
}

function evalPswd( pswd ) {
    var DEFAULT = 0;
    var WEAK = 1;
    var MEDIUM = 2;
    var STRONG = 3;
    var state = DEFAULT;
     
    var _hasLower, _hasUpper, _hasDigit = false;
    
    var minPswdLength = <%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getMinPasswordLength() ) %>;
	<ffi:cinclude value1="${useBCsettings}" value2="true" operator="equals">
	    minPswdLength = <%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getBCMinPasswordLength() ) %>;
	</ffi:cinclude>
	    
    // check if password changes the state
    if (pswd != null) {
        if (pswd.length > 0) {       
            state = WEAK;
        }
        if (pswd.length >= minPswdLength) {       
            // check for lowercase and set the flag
            _hasLower = (lowerCaseChars (pswd) > 0);
            // check for uppercase and set the flag
            _hasUpper = (upperCaseChars (pswd) > 0);
            // check for digits and set the flag
            _hasDigit = (numericChars (pswd) > 0);

            if (_hasLower && _hasUpper && _hasDigit) {
                state = STRONG;
            } else if (_hasLower && _hasUpper || _hasUpper && _hasDigit || _hasDigit && _hasLower ) {
                state = MEDIUM;
            } else {
                state = WEAK;
            }
        }		            
    } 
    
    var RED = '#FF6666';
    var YELLOW = '#FFFF00';
    var GREEN = '#66FF00';
    var BLACK = '#000000';
    var LIGHT_GRAY = '#F0F0F0';
    var GRAY = '#B8B8B8';
    $(document).ready(function(){
  			
    	// update the background color and the font color appropiately
        if (state == DEFAULT) {
            /* document.getElementById('idWeak').style.backgroundColor = LIGHT_GRAY;
            document.getElementById('idMedium').style.backgroundColor = LIGHT_GRAY;
            document.getElementById('idStrong').style.backgroundColor = LIGHT_GRAY;
            document.getElementById('idWeak').style.color = GRAY;
            document.getElementById('idMedium').style.color = GRAY;
            document.getElementById('idStrong').style.color = GRAY;	 */
            $('#idWeak').removeClass("active");
        	$('#idMedium').removeClass("active");
        	$('#idStrong').removeClass("active");
        }
        else if (state == WEAK) {
        	$('#idWeak').addClass("active");
        	$('#idMedium').removeClass("active");
        	$('#idStrong').removeClass("active");
            /* $('idWeak').style.backgroundColor = RED;
            document.getElementById('idMedium').style.backgroundColor = LIGHT_GRAY;
            document.getElementById('idStrong').style.backgroundColor = LIGHT_GRAY;
            document.getElementById('idWeak').style.color = BLACK;
            document.getElementById('idMedium').style.color = GRAY;
            document.getElementById('idStrong').style.color = GRAY;		      */   
        }
        else if (state == MEDIUM) {
        	
        	$('#idWeak').removeClass("active");
        	$('#idMedium').addClass("active");
        	$('#idStrong').removeClass("active");
            /* document.getElementById('idWeak').style.backgroundColor = YELLOW;
            document.getElementById('idMedium').style.backgroundColor = YELLOW;
            document.getElementById('idStrong').style.backgroundColor = LIGHT_GRAY;
            document.getElementById('idWeak').style.color = YELLOW;
            document.getElementById('idMedium').style.color = BLACK;
            document.getElementById('idStrong').style.color = GRAY;	 */	        
        }
        else if (state == STRONG) {
        	$('#idWeak').removeClass("active");
        	$('#idMedium').removeClass("active");
        	$('#idStrong').addClass("active");
            /* document.getElementById('idWeak').style.backgroundColor = GREEN;
            document.getElementById('idMedium').style.backgroundColor = GREEN;
            document.getElementById('idStrong').style.backgroundColor = GREEN;
            document.getElementById('idWeak').style.color = GREEN;
            document.getElementById('idMedium').style.color = GREEN;
            document.getElementById('idStrong').style.color = BLACK; */		        
        }
    	
	});
    		    		    		    
}	
</script>