<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<%-- have to decide whether or not to display the per location row --%>
<ffi:setProperty name="displayPerLocation" value="FALSE"/>

<ffi:object id="CheckCumulativeEntitlementByGroup" name="com.ffusion.tasks.business.CheckCumulativeEntitlementByGroup" scope="session" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<% String savedCatSubType = null; %>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="savedCatSubType"/>
	<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
	<ffi:setProperty name="GetDACategorySubType" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>"/>
	<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
														value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>" />
	<ffi:process name="GetDACategorySubType"/>
	<ffi:removeProperty name="GetDACategorySubType"/>
	<%String catSubType = null; %>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
	
	<%-- the Entitlement_EntitlementGroup and BusinessEmployee should already be in session --%>
	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>"/>
    <ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="DaCategorySessionName" value="EntitlementBean"/>
    <ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="DaCategorySubType" value="${catSubType}"/>
    
    <s:if test="%{#session.UsingEntProfiles == 'true'}">
    	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="UsingEntProfiles" value="${UsingEntProfiles}"/>
    	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="ChannelId" value="${ChannelId}"/>
    </s:if>
    
	<ffi:process name="CheckCumulativeEntitlementByGroup"/>
	<ffi:removeProperty name="catSubType"/>
	
	<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${savedCatSubType}" />
	
	<ffi:cinclude value1="${CheckEntitlement}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayPerLocation" value="TRUE"/>
	</ffi:cinclude>

	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
       	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>"/>
	<ffi:process name="CheckCumulativeEntitlementByGroup"/>
	<ffi:cinclude value1="${CheckEntitlement}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayPerLocation" value="TRUE"/>
	</ffi:cinclude>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<%-- there should already be a CheckEntitlementByGroup object in session --%>
	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
       	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>"/>
	<ffi:process name="CheckCumulativeEntitlementByGroup"/>
	<ffi:cinclude value1="${CheckEntitlement}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayPerLocation" value="TRUE"/>
	</ffi:cinclude>
	
	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
       	<ffi:setProperty name="CheckCumulativeEntitlementByGroup" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>"/>
	<ffi:process name="CheckCumulativeEntitlementByGroup"/>
	<ffi:cinclude value1="${CheckEntitlement}" value2="TRUE" operator="equals">
		<ffi:setProperty name="displayPerLocation" value="TRUE"/>
	</ffi:cinclude>	
</s:if>
<ffi:removeProperty name="CheckCumulativeEntitlementByGroup"/>

<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" id="CheckEntitlementByGroupCB" scope="session"/>
<ffi:setProperty name="CheckEntitlementByGroupCB" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.DIVISION_MANAGEMENT %>"/>
<ffi:setProperty name="CheckEntitlementByGroupCB" property="GroupId" value="${Business.EntitlementGroupId}"/>
<s:if test="%{#session.Section == 'UserProfile'}">
			<ffi:setProperty name="CheckEntitlementByGroupCB" property="ChannelGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
</s:if>
<ffi:process name="CheckEntitlementByGroupCB"/>
