<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="user_corpadminprofileadd" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_17')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<span id="PageHeading" style="display:none;"><s:text name="jsp.user_17"/></span>
<style type="text/css">
.channelId-padding{
	padding-left: 5px;
}
</style>
<script type="text/javascript">
function groupChange(event, data) {
	var profileId;
	var entGroupId = $("#selectProfileGroupID option:selected").text();
	$("#profileNameId").val(entGroupId.trim());
}
jQuery( document ).ready(function( $ ) {
	$("#selectProfileGroupID").selectmenu({width: 280,change: groupChange});
	var entGroupId = $("#selectProfileGroupID option:selected").text();
	$("#profileNameId").val(entGroupId.trim());
});
</script>
<ffi:setProperty name="BackURL" value="${SecurePath}user/businessuserprofileadd.jsp?GoingBack=TRUE" URLEncrypt="true"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="5" align="center">
			<ul id="formerrors"></ul>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" class="adminBackground">
			<ffi:setProperty name="tmp_url" value="${SecurePath}user/businessuserprofileadd-autoentitle.jsp?id=${BusinessUserProfile.EntitlementGroupId}" URLEncrypt="true"/>
			<s:form id="addBusinessProfileFormId" namespace="/pages/jsp/businessuserprofile" action="addBusinessUserProfile_verify" validate="false" theme="simple" name="AddUserForm" method="post" >
               <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
               <input id="channelRadioId" type="hidden" name="channelTyperadio"  value="X"/>
               <input id="channelIdName" type="hidden" name="BusinessUserProfile.ChannelId"  value="X"/>
			   <ffi:removeProperty name="tmp_url"/>
					<table width="100%" border="0" cellspacing="0" cellpadding="3" align="left">
						  <td width="250px" align="left">
								<s:text name="jsp.user_267"/>:<span class="required">*</span>
						  </td>
						  <td>
						  <input tabindex="14" class="ui-widget-content ui-corner-all" type="text" name="BusinessUserProfile.ProfileName"  size="48" maxlength="200" border="0"  />
						  <input id="profileNameId" type="hidden" name="selectedGroupName" />
						  <span id="BusinessUserProfile.profileNameError"></span>
						  <span id="userNameError"></span>
						  </td>
						<tr>
							<td width="250px" align="left">
								<ffi:cinclude value1="${GroupSummaries.Size}" value2="0" operator="notEquals">
									<s:text name="jsp.default_225"/>:<span class="required">*</span>
								</ffi:cinclude>
							</td>
							<td align="left">
								<ffi:cinclude value1="{$GroupSummaries.Size}" value2="0" operator="notEquals">
									<select id="selectProfileGroupID" class="txtbox" tabindex="20" name="BusinessUserProfile.ParentEntGroupId">															
										
										<ffi:setProperty name="Compare" property="Value1" value="${BusinessUserProfile.EntitlementGroupId}" />															
										
										<ffi:list collection="GroupSummaries" items="group" >
											<ffi:list collection="group" items="spaces, groupName, groupId, numUsers" >
												<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
													<ffi:setProperty name="Compare" property="value2" value="${groupId}"/>
													<option value="<ffi:getProperty name="groupId" />" <ffi:getProperty name="selected${Compare.Equals}"/> >
														<ffi:getProperty name="spaces" encode="false"/><ffi:getProperty name="groupName" />
													</option>
											</ffi:cinclude>
											</ffi:list>
										</ffi:list>
									</select>
									<span id="BusinessUserProfile.entitlementGroupIdError"></span>
								</ffi:cinclude>
							</td>
						</tr>
						<tr>
							<td align="left"><s:text name="jsp.default_171"/> </td>
							<td align="left"><textarea cols="40" name="BusinessUserProfile.description" class="ui-widget-content" /></td>
						</tr>
						<tr>
							<td align="left"><s:text name="jsp.user.label.Supported.Channels"/>:<span class="required">*</span></td>
							<td align="left">&nbsp</td>
						</tr>
						<s:iterator value="entChannels">
						  <tr>
							<td align="right">&nbsp</td>
							<td align="left" valign="bottom">
								<input type="checkbox" name="channels.childProfiles.channelId" value="<s:property  value='id'/>"></input>
								<span class="sectionsubhead"><b><s:property  value="id"/></b></span>
								<span class="channelId-padding">&#40; <s:property  value="description"/> &#41;</span>
							</td>
						  </tr>
						</s:iterator>
						  <tr>
							  <td>&nbsp</td>
							  <td><span id="BusinessUserProfile.channelListError"></span></td>
							</tr>
						  <tr>
							<td valign="bottom" align="left">
							  Profile Status:
							</td>
							<td valign="bottom" align="left" class="sectionsubhead">
							  	<input type="radio" name="BusinessUserProfile.status" value="ACTIVE"> Active
							  	<input type="radio" name="BusinessUserProfile.status" value="INACTIVE" checked> InActive
							 </td>
						  </tr>
						<tr>
							<td colspan="2"><div align="center"><span class="required">* <s:text name="jsp.default_240"/></span></div></td>
						</tr>
						</s:form>
						<tr>
							<td colspan="2" valign="top">
								<div align="center">
									<br>
									<s:url id="resetAddProfileButtonUrl" value="/pages/jsp/businessuserprofile/addBusinessUserProfile_init.action">  
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
									</s:url>
									<sj:a id="resetAddProfileBtn"
									   href="%{resetAddProfileButtonUrl}"
									   targets="inputDiv"
									   button="true" ><s:text name="jsp.default_358"/></sj:a>
								   
									<sj:a
										  button="true"
										  summaryDivId="profileSummary" 
										  buttonType="cancel"
										  onClickTopics="showSummary,cancelProfileForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a
										id="addProfileBtnId"
										formIds="addBusinessProfileFormId"
										targets="verifyDiv"
										button="true"
										validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeVerify"
										onCompleteTopics="completeVerify"
										onErrorTopics="errorVerify"
										onSuccessTopics="successVerify"
										><s:text name="jsp.default_29"/></sj:a>
								</div>
							</td>
						</tr>
					</table>
		</td>
	</tr>
</table>
	
