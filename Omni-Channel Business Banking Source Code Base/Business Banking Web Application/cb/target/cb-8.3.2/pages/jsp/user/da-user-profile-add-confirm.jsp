<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:setL10NProperty name='PageHeading' value='Confirm Approve User'/>

		<div align="center">
		
		<table class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			
			<tr>

				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<br>
						<tr>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_ADDED %>">
								<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-user-add-confirm.jsp-2" parm0="${userName}"/></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-user-add-confirm.jsp-1" parm0="${userName}"/></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED %>">							
								<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuserdelete-confirm.jsp-1" parm0="${userName}"/></td>
							</ffi:cinclude>
						</tr>
						<tr>
							<td height="70">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" class="ui-widget-header customDialogFooter">
									<sj:a 
										id="doneWizardID"
										button="true" 
										onclick="doneWizard();"
									><s:text name="jsp.default_538" /></sj:a></td>
						</tr>
					</table>
				</td>

			</tr>
			
		</table>
			<p></p>
		</div>

<script>
function doneWizard(){
	$('#approvalWizardDialogID').dialog('close');;
}
</script>