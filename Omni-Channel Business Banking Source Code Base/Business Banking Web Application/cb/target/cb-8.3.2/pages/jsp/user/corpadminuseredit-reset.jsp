<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name="success" value="false"/>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:setProperty name="EditBusinessEmployee" property="process" value="true"/>
	<ffi:setProperty name="EditBusinessEmployee" property="ResetPasswordQA" value="true"/>
	<ffi:process name="EditBusinessEmployee"/>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="EditBusinessEmployee" property="process" value="false"/>
	<ffi:setProperty name="EditBusinessEmployee" property="ResetPasswordQA" value="true"/>
	<ffi:process name="EditBusinessEmployee"/>

	<ffi:object id="AddUserToDA" name="com.ffusion.tasks.dualapproval.AddUserToDA" scope="session"/>
	<ffi:setProperty name="AddUserToDA" property="CategoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
	<ffi:setProperty name="AddUserToDA" property="UserAction" value="<%= String.valueOf( com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.USER_ACTION_MODIFIED ) %>"/>
	<ffi:setProperty name="NewBusinessEmployee" property="passwordReminder" value=""/>
	<ffi:setProperty name="NewBusinessEmployee" property="passwordReminder2" value=""/>
	<ffi:setProperty name="NewBusinessEmployee" property="passwordClue" value=""/>
	<ffi:setProperty name="NewBusinessEmployee" property="passwordClue2" value=""/>
	<ffi:process name="AddUserToDA"/>
	<ffi:removeProperty name="AddUserToDA"/>
	<%--<ffi:removeProperty name="NewBusinessEmployee"/>--%>
	<%--<ffi:removeProperty name="BusinessEmployee"/>--%>
</ffi:cinclude>
