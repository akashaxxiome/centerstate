<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<ffi:setProperty name='PageHeading' value='Account Configuration'/>
<ffi:setProperty name='PageText' value=''/>

<%--get company profile info from the business bean--%>
<ffi:object id="GetBusiness" name="com.ffusion.tasks.business.GetBusiness" scope="session"/>
    <ffi:setProperty name="GetBusiness" property="Id" value="${User.BUSINESS_ID}" />
<ffi:process name="GetBusiness"/>
<ffi:removeProperty name="GetBusiness"/>

<%--get service package info from GetEntitlementGroup, which stores info in session under Entitlement_EntitlementGroup--%>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Business.ServicesPackageId}" />
<ffi:process name="GetEntitlementGroup"/>

<%--get contact using GetPrimaryBusinessEmployees, which stores info for primary contact in session under PrimaryBusinessEmployee--%>
<ffi:object id="GetPrimaryBusinessEmployees" name="com.ffusion.tasks.user.GetPrimaryBusinessEmployees" scope="session"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BankId" value="${Business.BankId}"/>
<ffi:process name="GetPrimaryBusinessEmployees"/>
<ffi:removeProperty name="GetPrimaryBusinessEmployees" />

<%--get global administrators from GetGroupsAdministeredBy, which stores info in session under Entitlement_EntitlementGroups--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdministratorsFor" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<%--get the employees from those groups, they will be stored under BusinessEmployees --%>
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroups" scope="session"/>
  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups" />
<ffi:process name="GetBusinessEmployeesByEntGroups"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />

<%--get accounts info from GetAccountsByBusinessEmployee, stores results in session as BusEmpAccounts--%>
<ffi:object id="GetAccountsByBusinessEmployee" name="com.ffusion.tasks.accounts.GetAccountsByBusinessEmployee" scope="session"/>
	<ffi:setProperty name="GetAccountsByBusinessEmployee" property="AccountsName" value="BusEmpAccounts"/>
	<ffi:setProperty name="GetAccountsByBusinessEmployee" property="Reload" value="true"/>
<ffi:process name="GetAccountsByBusinessEmployee"/>
<ffi:removeProperty name="GetAccountsByBusinessEmployee"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${AccountTypesList}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.ResourceList" id="AccountTypesList" scope="session"/>
		<ffi:setProperty name="AccountTypesList" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources"/>
		<ffi:setProperty name="AccountTypesList" property="ResourceID" value="AccountTypesList"/>
	<ffi:process name="AccountTypesList" />
	<ffi:getProperty name="AccountTypesList" property="Resource"/>
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>