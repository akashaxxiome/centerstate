<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- This page is used to display pending island for Account Configuration --%>

<ffi:removeProperty name="APPROVE_ACCT_CONF"/>
<ffi:cinclude value1="${isAccountConfigurationChanged}" value2="Y">
<div  class="blockHead tableData"><span><strong>Account Configuration</strong></span></div>
<div class="paneWrapper">
 	<div class="paneInnerWrapper">
   		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
			<tr class="header">
				<td class="sectionsubhead adminBackground">Account</td>
				<td class="sectionsubhead adminBackground" >Field Name</td>
				<td class="sectionsubhead adminBackground" >Old Value</td>
				<td class="sectionsubhead adminBackground" >New Value</td>
				<td class="sectionsubhead adminBackground" >Action</td>
			<tr>

<ffi:list collection="categories" items="category">
	<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_ACCOUNT_CONFIGURATION%>" operator="equals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="Business"/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="objectId" value="${category.objectId}"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
			<ffi:setProperty name="APPROVE_ACCT_CONF" value="TRUE" />
			<tr>
				<td class="columndata"><ffi:getProperty name="category" property="objectId" /></td>
				<td class="columndata" ><ffi:getProperty name="details" property="fieldName" /></td>
				<td  class="columndata"><ffi:getProperty name="details" property="oldValue" /></td>
				<td  class="columndata sectionheadDA"><ffi:getProperty name="details" property="newValue" /></td>
				<td  class="columndata"><ffi:getProperty name="category" property="userAction" /></td>
			</tr>
		</ffi:list>
	</ffi:cinclude> 
</ffi:list>
</ffi:cinclude></tr></table></div></div>