<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:help id="user_crossachcompanyaccess-verify" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Verify Administer Access and Limits to All ACH Companies'/>

<%-- we need to do auto entitlement checking for company, division and group being modified --%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

<%-- get the auto entitle settings for the business --%>
<%-- assuming the correct Entitlement_EntitlementGroup is in session --%>
<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="Entitlement_EntitlementGroup"/>
<ffi:process name="GetCumulativeSettings"/>

</s:if>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>"/>
</ffi:cinclude>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">
		<%-- Initialize -- make available number of granted entitlements --%>
		<%-- (init only mode set for task) --%>
		<ffi:setProperty name="EditACHCompanyAccess" property="InitOnly" value="true" />
		<ffi:process name="EditACHCompanyAccess"/>
	</ffi:cinclude>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- Requires Approval Processing --%>
<ffi:setProperty name="EditRequiresApproval" property="ExecutePhase1" value="true" />
<ffi:process name="EditRequiresApproval" />
<%-- END Requires Approval Processing --%>

<%-- set action start --%>
<% String action = "editCrossACHCompanyAccess-execute"; %>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">
		<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="notEquals">
			<% action = "editCrossACHCompanyAccess-autoentitle"; %>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<% action = "editCrossACHCompanyAccess-execute"; %>
	</ffi:cinclude>
</s:if>
<% 
	session.setAttribute("action",action);
	session.setAttribute("action_execute", "editCrossACHCompanyAccess-execute");
%>
<%-- set action end --%>

	<div align="center">
		<div align="center" class="marginBottom20"> <ffi:getProperty name="Context"/></div>
<s:form namespace="/pages/user" action="%{#session.action}" theme="simple" method="post" name="EditAllACHCompanyFormVerify" id="EditAllACHCompanyFormVerify">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<input type="hidden" name="doAutoEntitle" value='true' />
	</ffi:cinclude>
</s:if>
<div align="center" class="marginBottom20">
								    <s:text name="jsp.user_4"/>
								    <s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
										<ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/>
								    </s:if>
    								<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
								    	<ffi:getProperty name="BusinessEmployee" property="Name"/>
								    </s:if>
</div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData permissionsTableSection">
							
							<tr class="header">
								<% String checkBoxName = ""; %>
								<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
							 	<td class="sectionsubhead" align="center" valign="middle">
			                                          
								    <ffi:getProperty name="adminAll" assignTo="checkBoxName"/>
								    <INPUT type="checkbox" disabled <ffi:cinclude value1="<%= checkBoxName %>"  value2="adminAll" operator="equals"> checked </ffi:cinclude> border="0">  <s:text name="jsp.user_32"/>
								</td>
								</ffi:cinclude>
			                                        <td class="sectionsubhead" align="center" valign="middle">
			                                            
								    <ffi:getProperty name="initAll" assignTo="checkBoxName"/>
								    <INPUT type="checkbox" disabled <ffi:cinclude value1="<%= checkBoxName %>"  value2="initAll" operator="equals"> checked </ffi:cinclude> border="0"><s:text name="jsp.user_177"/>
			                                        </td>
			                                        <td class="sectionsubhead" valign="middle" align="left">
			                                            <s:text name="jsp.user_346"/>
			                                        </td>
			                                          <%-- Requires Approval Column --%>
		                                        <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
		                                        	<td class="sectionsubhead" valign="middle">
			                                        	<s:text name="jsp.user_278"/>
			                                        </td>
		                                        </ffi:cinclude>
								<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
								<ffi:process name="GetLimitBaseCurrency" />
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
		                                    	</tr>
							
    							<%-- set up the correct collectionName for use by the achcompanyaccess_rowmaker.jsp --%>
							<ffi:setProperty name="collectionName" value="crossACHEntLists"/>
							<s:include value="inc/achcompanyaccess_rowmaker-verify.jsp"/>
							<ffi:removeProperty name="collectionName"/>

							<%-- <tr>	
								<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
									<td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
								</ffi:cinclude>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
							</tr> --%>
							<tr>				
								<td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top"><hr noshade size="1"></td>
							</tr>
							<tr>
								<td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top">
									<div align="center" class="btn-row">
												<s:url id="inputUrl" value="%{#session.PagesPath}user/crossachcompanyaccess.jsp" escapeAmp="false">
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
													<s:param name="FromBack" value="%{'TRUE'}"></s:param>
													<s:param name="UseLastRequest" value="%{'TRUE'}"></s:param>
												</s:url>
												<sj:a
													href="%{inputUrl}"
													targets="crossachcompanyaccessDiv"
													button="true"
													onCompleteTopics="backButtonTopic"
													><s:text name="jsp.default_57"/></sj:a>
												<sj:a
													button="true" 
													onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
													><s:text name="jsp.default_102"/></sj:a>

									<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
												<sj:submit
													id="xACHSubmit"
													formIds="EditAllACHCompanyFormVerify"
													targets="resultmessage"
													button="true" 
													validate="false"
													onBeforeTopics="disableSubmit"
													onErrorTopics="errorSubmit"
													onSuccessTopics="completedXACHCompanyEdit,reloadCompanyApprovalGrid"
													value="%{getText('jsp.default_366')}"
													onCompleteTopics="onCompleteTabifyAndFocusTopic"
													cssClass="formSubmitBtn"/>
									</s:if>
									<s:if test="%{#session.Section == 'UserProfile'}">
										<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="equals">
												<sj:submit
													id="xACHSubmit"
													formIds="EditAllACHCompanyFormVerify"
													targets="resultmessage"
													button="true" 
													validate="false"
													onBeforeTopics="disableSubmit"
													onErrorTopics="errorSubmit"
													onSuccessTopics="completedXACHCompanyEdit,reloadCompanyApprovalGrid"
													value="%{getText('jsp.default_366')}"
													cssClass="formSubmitBtn"
												/>
										</ffi:cinclude>
										<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="notEquals">
											<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="notEquals">
												<ffi:setProperty name="perm_target" value="crossachcompanyaccessDiv"/>
												<ffi:setProperty name="completedTopics" value="completedXACHCompanyEdit"/>
												<sj:submit
													id="xACHSubmit"
													formIds="EditAllACHCompanyFormVerify"
													targets="crossachcompanyaccessDiv"
													button="true" 
													validate="false"
													onBeforeTopics="disableSubmit"
													onErrorTopics="errorSubmit"
													value="%{getText('jsp.default_111')}"
													cssClass="formSubmitBtn"
													/>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg9')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg10')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg11')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg27')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/crossachcompanyaccess-verify.jsp"/>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/crossachcompanyaccess-confirm.jsp"/>
											</ffi:cinclude>

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="equals">
														<sj:submit
															id="xACHSubmit"
															formIds="EditAllACHCompanyFormVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="disableSubmit"
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedXACHCompanyEdit,reloadCompanyApprovalGrid"
															value="%{getText('jsp.default_366')}"
															cssClass="formSubmitBtn"
															/>
											</ffi:cinclude>

											</ffi:cinclude>

											<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="false" operator="equals">
														<sj:submit
															id="xACHSubmit"
															formIds="EditAllACHCompanyFormVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="disableSubmit"
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedXACHCompanyEdit,reloadCompanyApprovalGrid"
															value="%{getText('jsp.default_366')}"
															cssClass="formSubmitBtn"
															/>
											</ffi:cinclude>
										</ffi:cinclude>
									</s:if>
									<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles' && #session.Section != 'UserProfile'}">

										<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="notEquals">
												<ffi:setProperty name="perm_target" value="crossachcompanyaccessDiv"/>
												<ffi:setProperty name="completedTopics" value="completedXACHCompanyEdit"/>
												<sj:submit
													id="xACHSubmit"
													formIds="EditAllACHCompanyFormVerify"
													targets="crossachcompanyaccessDiv"
													button="true" 
													validate="false"
													onBeforeTopics="disableSubmit"
													onErrorTopics="errorSubmit"
													value="%{getText('jsp.default_111')}"
													cssClass="formSubmitBtn"
													/>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg9')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg10')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg11')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg27')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/crossachcompanyaccess-verify.jsp"/>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/crossachcompanyaccess-confirm.jsp"/>
											</ffi:cinclude>

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="equals">
														<sj:submit
															id="xACHSubmit"
															formIds="EditAllACHCompanyFormVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="disableSubmit"
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedXACHCompanyEdit,reloadCompanyApprovalGrid"
															value="%{getText('jsp.default_366')}"
															cssClass="formSubmitBtn"
															/>
											</ffi:cinclude>

										</ffi:cinclude>

										<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="false" operator="equals">
														<sj:submit
															id="xACHSubmit"
															formIds="EditAllACHCompanyFormVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="disableSubmit"
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedXACHCompanyEdit,reloadCompanyApprovalGrid"
															value="%{getText('jsp.default_366')}"
															cssClass="formSubmitBtn"
															/>
										</ffi:cinclude>

									</s:if>
									</div>
								</td>
							</tr>
							<tr>
								<td height="25">&nbsp;</td>
							</tr>
						</table></div></div>
					</s:form>
	</div>

<%-- Disable the submit button so the form can't be submitted twice. Processing may be slow and the spinner doesn't always block user input after a submit. --%>
<script>
	$.subscribe('disableSubmit', function(event, data)
	{
		$('#xACHSubmit').attr('disabled', 'disabled');
	});
	
	$(document).ready(function(){
		$('#xACHSubmit').addClass("formSubmitBtn");
	})
</script>

<ffi:setProperty name="EditACHCompanyAccess" property="InitOnly" value="false" />

<ffi:removeProperty name="perBatchMax"/>
<ffi:removeProperty name="dailyMax"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckForRedundantACHLimits"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>    
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
