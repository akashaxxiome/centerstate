<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadminlocedit-verify" className="moduleHelpClass"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<s:set var="tmpI18nStr" value="%{getText('jsp.user_144')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request" />

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<ffi:cinclude value1="${EditLocation}" value2="" operator="equals">
	<ffi:object id="EditLocation" name="com.ffusion.tasks.cashcon.EditLocation" scope="session" />
	<ffi:setProperty name="EditLocation" property="AccountsName" value="BankingAccounts"/>
	<%-- setting the BPWID has the side effect of setting all the tasks's location-related properties --%>
	<ffi:setProperty name="EditLocation" property="LocationBPWID" value="${EditLocation_LocationBPWID}"/>
</ffi:cinclude>

<ffi:setProperty name="EditLocation" property="ProcessFlag" value="false" />
<%-- <ffi:process name="EditLocation" /> --%>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${EditLocation.CurrencyCode}"/>

<ffi:cinclude value1="${AffiliateBanks}" value2="" operator="equals">
	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
</ffi:cinclude>


<%-- get cashcon company list --%>
<ffi:cinclude value1="${CashConCompanies}" value2="" operator="equals">
	<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
	<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
	<ffi:process name="GetCashConCompanies"/>
	<ffi:removeProperty name="GetCashConCompanies"/>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="" operator="notEquals">
	<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
	<ffi:process name="SetCashConCompany"/>
</ffi:cinclude>

<%-- based on selected concentration account, set DepositEnabled --%>
<ffi:setProperty name="DepositEnabled" value="false"/>
<ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="" operator="notEquals">
	<ffi:setProperty name="DepositEnabled" value="true"/>
</ffi:cinclude>

<%-- based on selected disbursement account, set DisbursementEnabled --%>
<ffi:setProperty name="DisbursementEnabled" value="false"/>
<ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="" operator="notEquals">
	<ffi:setProperty name="DisbursementEnabled" value="true"/>
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>

<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
	 <ffi:cinclude value1="${CashConCompany.ConcSameDayCutOffDefined}" value2="true" operator="equals">
	 		<ffi:setProperty name="showConcSameDayPrenote" value="true"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.ConcSameDayCutOffDefined}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.DisbSameDayCutOffDefined}" value2="true" operator="equals">
			<ffi:setProperty name="showDisbSameDayPrenote" value="true"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.DisbSameDayCutOffDefined}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>
	 </ffi:cinclude>
 </ffi:cinclude>
<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="notEquals">
	<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
	<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>
</ffi:cinclude>



<div align="center">
	<s:form id="AddEditLocationVerifyFormID" namespace="/pages/user" action="editLocation-execute" theme="simple" name="AddEditLocationFormVerify" method="post">
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="EditLocation.ProcessFlag" value="true"/>
			
			<table   cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10 largeFormContent">
				<tr>
					<td width="20%" valign="top">
						<%-- <span class="ffivisible marginTop10">&nbsp;</span> --%>
						<div class="paneWrapper marginTop20">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="admin.division.summary"/>
							</div>
							<div class="paneContentWrapper">
								<table cellspacing="0" cellpadding="3" border="0" width="100%">
									<tr>
										<td><s:text name="jsp.user_121"/>:&nbsp; <ffi:getProperty name="EditGroup_DivisionName"/></td>
									</tr>
								</table>
							</div>
							</div>
						</div>
					</td>
					<td width="3%">&nbsp;</td>				
					<td align="left" class="adminBackground" width="77%" valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td>
									<div class="blockWrapper">
										<div class="blockHead"><s:text name="admin.location.summary" /></div>
										<div class="blockContent">
											<div class="blockRow">
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_267"/>:</span>
													<span class="sectionsubhead valueCls">
														<ffi:getProperty name="EditLocation" property="LocationName"/>
													</span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_189"/>:</span>
													<span class="sectionsubhead valueCls">
														<ffi:getProperty name="EditLocation" property="LocationID"/>
													</span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead"><input type="checkbox" name="EditLocation.Active" border="0" value="true" <ffi:cinclude value1="${EditLocation.Active}" value2="true" operator="equals">checked</ffi:cinclude> disabled ></span>
													<span class="sectionsubhead valueCls">
														<s:text name="jsp.default_28"/>
													</span>
												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_188"/></div>
									<div class="blockContent">
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead"><s:text name="jsp.default_61"/>:&nbsp;</span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="false" operator="equals">
															<ffi:getProperty name="EditLocation" property="LocalBankName"/> : <ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">
															<ffi:list collection="AffiliateBanks" items="Bank">
																<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">
																	<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
																</ffi:cinclude>
															</ffi:list>
														</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead"><s:text name="jsp.default_15"/>:&nbsp;</span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="false" operator="equals">
														<ffi:getProperty name="EditLocation" property="LocalAccountNumber"/> :
														<ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
															<s:text name="jsp.user_64"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
															<s:text name="jsp.user_281"/>
														</ffi:cinclude>
													</ffi:cinclude>
													<ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true" operator="equals">
														<ffi:list collection="BankingAccounts" items="Account">
															<ffi:cinclude value1="${Account.ID}" value2="${EditLocation.LocalAccountID}" operator="equals">
																<ffi:getProperty name="Account" property="DisplayText"/>
																<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="Account" property="NickName"/>
																 </ffi:cinclude>
															</ffi:cinclude>
														</ffi:list>
													</ffi:cinclude>
												</span>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_276"/></div>
									<div class="blockContent">
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead"><s:text name="jsp.user_55"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
														</ffi:cinclude>
													</ffi:list>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead"><s:text name="jsp.user_75"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
														<s:text name="jsp.default_296"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="equals">
															<s:text name="jsp.default_296"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="notEquals">
															<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
																<ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">
																	<ffi:getProperty name="ConcAccount" property="DisplayText"/> - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																</ffi:cinclude>
															</ffi:list>
														</ffi:cinclude>
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead"><s:text name="jsp.user_117"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
														<s:text name="jsp.default_296"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="equals">
															<s:text name="jsp.default_296"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="notEquals">
															<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
																<ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">
																	<ffi:getProperty name="DisbAccount" property="DisplayText"/> - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																</ffi:cinclude>
															</ffi:list>
														</ffi:cinclude>
													</ffi:cinclude>
												</span>
											</div>
										</div>
									</div>
								</div>		
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_113"/></div>
									<div class="blockContent label140">
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_112"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_111"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_44"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
										</div>
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_337"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span><input type="checkbox" name="EditLocation.ConsolidateDeposits" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.ConsolidateDeposits}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled></span>
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_94"/> </span>
											</div>
											<div class="inlineBlock" style="width:33%">
											
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="notEquals">
												<span><input type="checkbox" name="EditLocation.DepositPrenote" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.DepositPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled></span>
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_110"/> </span>
											</ffi:cinclude>
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="equals">
												<span></span><span></span>
											</ffi:cinclude>
												
											</div>
										</div>
										<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="equals">
											<div class="blockRow">
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel">
														<s:text name="jsp.user_110"/>
													</span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span>
														<input id="DepositPrenote" type="checkbox" name="EditLocation.DepositPrenote" value="true" border="0" 
														<ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals">
															<ffi:cinclude value1="${EditLocation.DepositPrenote}" value2="true" operator="equals">
																<ffi:cinclude value1="${EditLocation.DepositSameDayPrenote}" value2="true" operator="notEquals">
																	checked 
																</ffi:cinclude>
															</ffi:cinclude>
														</ffi:cinclude> 
														 disabled> 
													</span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_571"/>
													</span>
												</div>
												<div class="inlineBlock" style="width:33%">
													 <span>
														<input id="DepositSameDayPrenote" type="checkbox" name="EditLocation.DepositSameDayPrenote" value="true" border="0" 
														<ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals">
														<ffi:cinclude value1="${EditLocation.DepositSameDayPrenote}" value2="true" operator="equals">
														checked</ffi:cinclude></ffi:cinclude> disabled>
													 </span>
													 <span class="sectionsubhead sectionLabel">
															<s:text name="jsp.user_572"/>
													 </span>
												</div>
											</div>	
										</ffi:cinclude>
										<ffi:cinclude value1="${EditLocation.DepPrenoteStatus}" value2="" operator="notEquals">
											<div class="blockRow">
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel">
														<s:text name="jsp.user_574"/>:
													</span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span class="sectionhead_greyDA"><ffi:getProperty name="EditLocation" property="DepPrenoteStatus"/></span>
												</div>
												<div class="inlineBlock" style="width:33%">
												</div>
											</div>	
										</ffi:cinclude>
									</div>
								</div>		
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td >
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_119"/></div>
									<div class="blockContent">
										<div class="blockRow">
											<ffi:cinclude value1="${showDisbSameDayPrenote}" value2="true" operator="notEquals">
												<div class="inlineBlock" style="width:99%">
													<span class="sectionsubhead"><input type="checkbox" name="EditLocation.DisbursementPrenote" value="true" border="0" <ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.DisbursementPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled></span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_118"/></span>
												</div>
											 </ffi:cinclude>
											 <ffi:cinclude value1="${showDisbSameDayPrenote}" value2="true" operator="equals">
												 <div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_118"/></span>
													<span> </span>
												 </div>
												 <div class="inlineBlock" style="width:33%">
													<span>
														<input id="DisbursementPrenote" type="checkbox" name="EditLocation.DisbursementPrenote" value="true" border="0" 
														 <ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals">
														<ffi:cinclude value1="${EditLocation.DisbursementPrenote}" value2="true" operator="equals">
															<ffi:cinclude value1="${EditLocation.DisbursementSameDayPrenote}" value2="true" operator="notEquals">
																checked 
															</ffi:cinclude>
														</ffi:cinclude>
														</ffi:cinclude> 
														 disabled>
													</span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_571"/></span>
												 </div>
												 <div class="inlineBlock" style="width:33%">
												   <span>
													<input id="DisbursementSameDayPrenote" type="checkbox" name="EditLocation.DisbursementSameDayPrenote" value="true" border="0" <ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.DisbursementSameDayPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled>
												   </span>
												   <span class="sectionsubhead sectionLabel"><s:text name="jsp.user_572"/></span>
												 </div>
											 </ffi:cinclude>
										</div>
										<ffi:cinclude value1="${EditLocation.DisPrenoteStatus}" value2="" operator="notEquals">
											<div class="blockRow">
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel">
														<s:text name="jsp.user_575"/>:
													</span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span class="sectionhead_greyDA"><ffi:getProperty name="EditLocation" property="DisPrenoteStatus"/></span>
												</div>
												<div class="inlineBlock" style="width:33%">
												</div>
											</div>	
										</ffi:cinclude>
									</div>
								</div>		
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center">
								<ffi:cinclude value1="${fromDivEdit}" value2="true" operator="notEquals">
									<sj:a id="backBtn2"
										  button="true" 
										  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
									<sj:a id="cancelBtn2"
										  button="true" 
										  summaryDivId="summary" buttonType="cancel"
										  onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a id="addSubmit"
										formIds="AddEditLocationVerifyFormID"
											targets="confirmDiv"
											button="true" 
											validate="false"
											onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmitLocation,redefineLocation"
										><s:text name="jsp.user_2"/></sj:a>
								</ffi:cinclude>	
								<ffi:cinclude value1="${fromDivEdit}" value2="true" operator="equals">
										<sj:a id="addSubmit"
										formIds="AddEditLocationVerifyFormID"
											targets="confirmDiv"
											button="true" 
											validate="false"
											onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmitLocation,redefineLocation"
										><s:text name="jsp.user_2"/></sj:a>
								</ffi:cinclude>
							</td>
						</tr>
						</table>
					</td>
			 	 </tr>
			</table>
		</s:form>
	</div>
		
		<%-- <table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="adminBackground">
						<s:form id="AddEditLocationVerifyFormID" namespace="/pages/user" action="editLocation-execute" theme="simple" name="AddEditLocationFormVerify" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="EditLocation.ProcessFlag" value="true"/>
							<div align="center">
								 <table width="711" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td class="adminBackground sectionsubhead" nowrap width="120">
											<div align="right"><s:text name="jsp.default_267"/>:</div>
										</td>
										<td class="adminBackground columndata" width="120">
											<ffi:getProperty name="EditLocation" property="LocationName"/>
										</td>
										<td class="adminBackground sectionsubhead" nowrap width="120">
											<div align="right"><s:text name="jsp.user_189"/>:</div>
										</td>
										<td class="adminBackground columndata" width="120">
											<ffi:getProperty name="EditLocation" property="LocationID"/>
										</td>
										<td class="adminBackground" width="120">
											<div align="right">
												<input type="checkbox" name="EditLocation.Active" border="0" value="true" <ffi:cinclude value1="${EditLocation.Active}" value2="true" operator="equals">checked</ffi:cinclude> disabled >
											</div>
										</td>
										<td class="sectionsubhead adminBackground" width="120"><s:text name="jsp.default_28"/></td>
									</tr>
								</table>
								<br>
								<table border="0" cellspacing="0" cellpadding="3" width="710">
									<tr>
										<td>
											<table border="0" cellspacing="0" cellpadding="3" width="355">
												<tr>
													<td colspan="3" class="sectionhead adminBackground tbrd_b">
														<s:text name="jsp.user_188"/><br>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="3" align="left">
														<s:text name="jsp.default_61"/>
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td colspan="2" nowrap align="left" class="columndata">
														<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="false" operator="equals">
															<ffi:getProperty name="EditLocation" property="LocalBankName"/> : <ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">
															<ffi:list collection="AffiliateBanks" items="Bank">
																<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">
																	<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
																</ffi:cinclude>
															</ffi:list>
														</ffi:cinclude>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="3" align="left">
														<s:text name="jsp.default_15"/>
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td colspan="2">
														<div class="columndata" align="left">
															<ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="false" operator="equals">
																<ffi:getProperty name="EditLocation" property="LocalAccountNumber"/> :
																<ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
																	<s:text name="jsp.user_64"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
																	<s:text name="jsp.user_281"/>
																</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true" operator="equals">
																<ffi:list collection="BankingAccounts" items="Account">
																	<ffi:cinclude value1="${Account.ID}" value2="${EditLocation.LocalAccountID}" operator="equals">
																		<ffi:getProperty name="Account" property="DisplayText"/>
																		<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
																			 - <ffi:getProperty name="Account" property="NickName"/>
																		 </ffi:cinclude>
																	</ffi:cinclude>
																</ffi:list>
															</ffi:cinclude>
														</div>
													</td>
												</tr>
												<tr>
													<td class="tbrd_b adminBackground sectionhead" colspan="3"><s:text name="jsp.user_276"/></td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="2" nowrap align="right">
														<s:text name="jsp.user_55"/>:
													</td>
													<td class="adminBackground" align="left">
														<ffi:list collection="CashConCompanies" items="tempCompany">
															<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">
																<ffi:getProperty name="tempCompany" property="CompanyName"/>
															</ffi:cinclude>
														</ffi:list>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="2" nowrap align="right">
														<s:text name="jsp.user_75"/>:
													</td>
													<td class="adminBackground" align="left">
														<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
															<s:text name="jsp.default_296"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
															<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="equals">
																<s:text name="jsp.default_296"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="notEquals">
																<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
																	<ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">
																		<ffi:getProperty name="ConcAccount" property="DisplayText"/> - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																	</ffi:cinclude>
																</ffi:list>
															</ffi:cinclude>
														</ffi:cinclude>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="2" nowrap align="right">
														<s:text name="jsp.user_117"/>:
													</td>
													<td class="adminBackground" align="left">
														<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
															<s:text name="jsp.default_296"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
															<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="equals">
																<s:text name="jsp.default_296"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="notEquals">
																<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
																	<ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">
																		<ffi:getProperty name="DisbAccount" property="DisplayText"/> - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																	</ffi:cinclude>
																</ffi:list>
															</ffi:cinclude>
														</ffi:cinclude>
													</td>
												</tr>
												<tr>
													<td width="25">&nbsp;</td>
													<td width="140">&nbsp;</td>
													<td width="180">&nbsp;</td>
												</tr>
												</tr>
											</table>
										</td>
										<td valign="top" class="tbrd_r adminBackground">&nbsp;</td>
										<td valign="top" class="adminBackground">&nbsp;</td>
										<td>
											<table border="0" cellspacing="0" cellpadding="3" width="355">
												<tr>
													<td class="tbrd_b adminBackground" colspan="3">
														<span class="sectionhead"><s:text name="jsp.user_113"/></span>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="2" nowrap>
														<div align="right"><s:text name="jsp.user_112"/>:</div>
													</td>
													<td class="adminBackground columndata">
														<div align="left">
															<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="notEquals">
																<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
																<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="equals">
																&nbsp;
															</ffi:cinclude>
														</div>
													</td>
												</tr>
												<tr>
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_111"/>:</span></div>
													</td>
													<td class="adminBackground columndata" align="left">
														<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="notEquals">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
															<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="equals">
															&nbsp;
														</ffi:cinclude>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="2" nowrap align="right">
														<s:text name="jsp.user_44"/>:
													</td>
													<td class="adminBackground columndata" align="left">
														<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="notEquals">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
															<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="equals">
															&nbsp;
														</ffi:cinclude>
													</td>
												</tr>
												<tr>
													<td class="adminBackground sectionsubhead" colspan="2" nowrap align="right">
														<s:text name="jsp.user_337"/>:
													</td>
													<td class="adminBackground columndata" align="left">
														<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="notEquals">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
															<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="equals">
															&nbsp;
														</ffi:cinclude>
													</td>
												</tr>
												<tr>
													<td>
														<input type="checkbox" name="EditLocation.ConsolidateDeposits" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.ConsolidateDeposits}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled>
													</td>
													<td class="sectionsubhead" colspan="2"><s:text name="jsp.user_94"/></td>
												</tr>
												<tr>
													<td>
														<input type="checkbox" name="EditLocation.DepositPrenote" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.DepositPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled>
													</td>
													<td class="sectionsubhead" colspan="2"><s:text name="jsp.user_110"/></td>
												</tr>
												<tr>
													<td colspan="3" class="sectionhead tbrd_b"><s:text name="jsp.user_119"/></td>
												</tr>
												<tr>
													<td>
														<input type="checkbox" name="EditLocation.DisbursementPrenote" value="true" border="0" <ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${EditLocation.DisbursementPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled>
													</td>
													<td class="sectionsubhead" colspan="2" nowrap><s:text name="jsp.user_118"/></td>
												</tr>
												<tr>
													<td width="25">&nbsp;</td>
													<td width="140">&nbsp;</td>
													<td width="180">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="middle">
										<td width="310">&nbsp;</td>
										<td valign="top" class="adminBackground">&nbsp;</td>
										<td valign="top" class="adminBackground">&nbsp;</td>
										<td width="310">&nbsp;</td>
									</tr>
								</table>
								<ffi:cinclude value1="${fromDivEdit}" value2="true" operator="notEquals">
									<sj:a id="backBtn2"
										  button="true" buttonIcon="ui-icon-arrowreturnthick-1-w"
										  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
									<sj:a id="cancelBtn2"
										  button="true" buttonIcon="ui-icon-close"
										  onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a id="addSubmit"
										formIds="AddEditLocationVerifyFormID"
											targets="confirmDiv"
											button="true" buttonIcon="ui-icon-check"
											validate="false"
											onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmitLocation"
										><s:text name="jsp.user_2"/></sj:a>
								</ffi:cinclude>	
								<ffi:cinclude value1="${fromDivEdit}" value2="true" operator="equals">
										<sj:a id="addSubmit"
										formIds="AddEditLocationVerifyFormID"
											targets="confirmDiv"
											button="true" buttonIcon="ui-icon-check"
											validate="false"
											onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmitLocation"
										><s:text name="jsp.user_2"/></sj:a>
								</ffi:cinclude>
							</div>
						</s:form>
					</td>
				</tr>
			</table> --%>
