<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<s:include value="inc/corpadmingroups-pre.jsp"/>

<div class="dashboardUiCls">
   	<div class="moduleSubmenuItemCls">
   		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-adminGroups"></span>
   		<span class="moduleSubmenuLbl">
   			<s:text name="jsp.home_101" />
   		</span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
		<!-- dropdown menu include -->    		
   		<s:include value="/pages/jsp/home/inc/adminSubmenuDropdown.jsp" />
  	</div>
   	
   	<!-- dashboard items listing for admin -->
	<div id="dbGroupSummary" class="dashboardSummaryHolderDiv">
			<s:url id="groupSearchSummary" value="/pages/jsp/user/admin_groups_summary.jsp" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="groupsLink"
				href="%{groupSearchSummary}"
				targets="summary"
				indicator="indicator"
				button="true"
				cssClass="summaryLabelCls"
				onclick="ns.admin.closeGroupDetails()">
			<s:text name="jsp.home_197"/></sj:a>
		 	
		 	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
                <s:url id="addGroupURL" value="/pages/jsp/user/corpadmingroupadd.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="UseLastRequest">FALSE</s:param>
				</s:url>
                <sj:a id="addGroupLink"
                   href="%{addGroupURL}"
                   targets="inputDiv"
                   onClickTopics="beforeLoad" onCompleteTopics="completeGroupLoad"
                   button="true"><s:text name="jsp.user_18"/></sj:a>
             </ffi:cinclude> 
         
         	<sj:a id="editGroupLink" href="%{editGroupUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
				cssStyle="display:none;" title="%{getText('jsp.user_143')}"
				onClickTopics="beforeLoad" onCompleteTopics="completeLoad" onErrorTopics="errorLoad"><s:text name="jsp.user_143"/></sj:a>
			
	</div>
	
	
	<!-- More list option listing -->
	<%-- <div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbGroupSummary" onclick="ns.common.refreshDashboard('showdbGroupSummary',true)"><s:text name="jsp.home_83"/></a></li>
		</ul>
	</div>
 --%>
</div> 

<script>
	$("#groupsLink").find("span").addClass("dashboardSelectedItem");
</script>
