<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name='PageText2' value=''/>
<ffi:setProperty name='PageText_clone_user_permission' value=''/>
<ffi:setProperty name='PageText_clone_account_permission' value=''/>
<%-- Display Add Group Button --%>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:object id="GetChildrenByGroupType" name="com.ffusion.efs.tasks.entitlements.GetChildrenByGroupType" scope="session"/>
		<ffi:setProperty name="GetChildrenByGroupType" property="GroupType" value="Division"/>
		<ffi:setProperty name="GetChildrenByGroupType" property="GroupId" value="${SecureUser.EntitlementID}"/>
	<ffi:process name="GetChildrenByGroupType"/>

	<%-- if division size is not 0 and this secure user is entitled to adminstrator this group
		 enable the addgroup button --%>
	<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
			<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadmingroupadd.jsp"><img src="/cb/web/grafx/${ImgExt}user/i_addgroup.gif" alt="" width="67" height="16" border="0"></a>'/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<%-- Display Add User button --%>
<ffi:cinclude value1="${Section}" value2="Users" operator="equals">
    <ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
    <ffi:process name="CanAdministerAnyGroup"/>

		<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${BusinessEmployeeId}" value2="" operator="notEquals">
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="notEquals">
				<%-- If dual approval is enabled then we do not display Entitlements Profile. --%>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
						<ffi:setProperty name="targetURL" value="${SecurePath}user/clone-user-permissions-init.jsp?TargetBusinessEmployeeId=${BusinessEmployeeId}&OneAdmin=${OneAdmin}" URLEncrypt="true" />
						<ffi:setProperty name='PageText_clone_user_permission' value='<a href="${targetURL}"><img src="/cb/web/grafx/${ImgExt}user/i_clone_user_permissions.gif" alt="" width="135" height="14" border="0"></a>'/>
	
						<ffi:setProperty name="targetURL" value="${SecurePath}user/clone-account-permissions-init.jsp?BusinessEmployeeIdForClone=${BusinessEmployeeId}&OneAdmin=${OneAdmin}" URLEncrypt="true" />
						<ffi:setProperty name='PageText_clone_account_permission' value='<a href="${targetURL}"><img src="/cb/web/grafx/${ImgExt}user/i_clone_account_permissions.gif" alt="" width="153" height="14" border="0"></a>'/>
					</ffi:cinclude>	
					<ffi:removeProperty name="targetURL"/>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
					<ffi:setProperty name='PageText2' value='<a href="${SecurePath}user/permissionswizard.jsp"><img src="/cb/web/grafx/${ImgExt}user/i_permissionswizard.gif" alt="" width="99" height="14" border="0"></a>'/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>	
		<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
			<ffi:setProperty name='PageText' value=''/>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadminprofileadd.jsp"><img src="/cb/web/grafx/${ImgExt}user/i_addprofile.gif" alt="" width="77" height="16" border="0" hspace="3"></a>'/>
			</ffi:cinclude>
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">	
				<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}user/corpadminuseradd.jsp"><img src="/cb/web/grafx/${ImgExt}user/i_adduser.gif" alt="" width="67" height="16" border="0" hspace="3"></a>'/>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<%-- Display Add Division button --%>
<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
		<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadmindivadd.jsp"><img src="/cb/web/grafx/${ImgExt}user/i_adddivision.gif" alt="" width="74" height="16" border="0"></a>'/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
