<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_achcompanyaccess-verify" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Verify Administer Access and Limits to Particular ACH Company'/>

<%-- we need to do auto entitlement checking for company, division and group being modified --%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

<%-- get the auto entitle settings for the business --%>
<%-- assuming the correct Entitlement_EntitlementGroup is in session --%>
<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="Entitlement_EntitlementGroup"/>
<ffi:process name="GetCumulativeSettings"/>

</s:if>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else> 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnableACHCompanies}" value2="true" operator="equals">
		<%-- Initialize -- make available number of granted entitlements --%>
		<%-- (init only mode set for task) --%>
	    <ffi:setProperty name="EditACHCompanyAccess" property="InitOnly" value="true" /> 
		<ffi:process name="EditACHCompanyAccess"/>
	</ffi:cinclude>
</s:if>
<ffi:setProperty name="BackURL" value="${SecurePath}user/achcompanyaccess.jsp?UseLastRequest=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/> 
<ffi:setProperty name="SuccessURL" value="${SecurePath}user/achcompanyaccess-confirm.jsp?PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/> 

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- set action start --%>
<% String action = "editACHCompanyAccess-execute"; %>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnableACHCompanies}" value2="true" operator="equals">
		<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="notEquals">
		<% action = "editACHCompanyAccess-autoentitle"; %>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<% action = "editACHCompanyAccess-execute"; %>
	</ffi:cinclude>
</s:if>
<% 
	session.setAttribute("action", action); 
	session.setAttribute("action_execute", "editACHCompanyAccess-execute");
%>
<%-- set action end --%>
	<div align="center">
		<div align="center" class="marginBottom20">
				    <ffi:getProperty name="Context"/>
		</div>
<s:form namespace="/pages/user" action="%{#session.action}" validate="false" theme="simple" method="post" name="EditACHCompanyForm" id="EditACHCompanyForm">	
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<input type="hidden" name="doAutoEntitle" value='true' />
	</ffi:cinclude>
</s:if>
<div align="center" class="marginBottom10">
									<ffi:getProperty name="ACHCompany" property="CompanyName"/> - 
									<ffi:getProperty name="ACHCompany" property="CompanyID"/>
</div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">		
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData permissionsTableSection">						
						
							<tr class="header">
								<% String checkBoxName = ""; %>
							 	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
							 	<td class="sectionsubhead" align="center" valign="middle">
			                                            
								    <ffi:getProperty name="adminAll" assignTo="checkBoxName"/>
								    <INPUT type="checkbox" disabled <ffi:cinclude value1="<%= checkBoxName %>"  value2="adminAll" operator="equals"> checked </ffi:cinclude> border="0"><s:text name="jsp.user_32"/>
			                                        </td>
			                                        </ffi:cinclude>
			                                        <td class="sectionsubhead" align="center" valign="middle">
			                                           
								    <ffi:getProperty name="initAll" assignTo="checkBoxName"/>
								    <INPUT type="checkbox" disabled <ffi:cinclude value1="<%= checkBoxName %>"  value2="initAll" operator="equals"> checked </ffi:cinclude> border="0"> <s:text name="jsp.user_177"/>
			                                        </td>
			                                        <td class="sectionsubhead" valign="middle">
			                                            <s:text name="jsp.user_346"/>
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
								<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
								<ffi:process name="GetLimitBaseCurrency" />
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
			                                            <s:text name="jsp.default_261" /> (<s:text name="jsp.user_173" /> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	   <s:text name="jsp.user_153" />
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
		                                    	</tr>

							<%-- set up the correct collectionName for use by the achcompanyaccess_rowmaker.jsp --%>
							<ffi:setProperty name="collectionName" value="perACHEntLists"/>							
							<s:include value="inc/achcompanyaccess_rowmaker-verify.jsp" />
							<ffi:removeProperty name="collectionName"/>

							<%-- <tr>	
								<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
									<td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="40" height="1"></td>
								</ffi:cinclude>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="40" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="155" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="75" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="60" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="95" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="75" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="60" height="1"></td>
					                            <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="95" height="1"></td>
							</tr> 
							<tr>				
								<td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top"><hr noshade size="1"></td>
							</tr>--%>
						</table>
									<div class="btn-row" style="margin-bottom:25px !important;">
										<s:url id="inputUrl" value="%{#session.PagesPath}user/achcompanyaccess.jsp" escapeAmp="false">
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
											<s:param name="FromBack" value="%{'TRUE'}"></s:param>
											<s:param name="UseLastRequest" value="%{'TRUE'}"></s:param>
										</s:url>
										<sj:a
										    id="verifyBackPerAchButtonId"
											href="%{inputUrl}"
											targets="achcompanyaccessDiv"
											button="true"
											onCompleteTopics="backButtonTopic"
											><s:text name="jsp.default_57"/></sj:a>
										<sj:a 
										    id="verifyPerAchCloseButtonId"
											button="true"
											onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
											><s:text name="jsp.default_102"/></sj:a>
									<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
										<sj:a
										    id="verifySavePerAchButtonId"
											formIds="EditACHCompanyForm"
											targets="resultmessage"
											button="true" 
											validate="false"
											onBeforeTopics="beforeSubmit" 
											onErrorTopics="errorSubmit"
											onSuccessTopics="completedPerACHCompanyEdit"
											><s:text name="jsp.default_366"/></sj:a>
									</s:if>
									<s:if test="%{#session.Section == 'UserProfile'}">
										<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="equals">
											<sj:a
											formIds="EditACHCompanyForm"
											targets="resultmessage"
											button="true" 
											validate="false"
											onBeforeTopics="beforeSubmit" 
											onErrorTopics="errorSubmit"
											onSuccessTopics="completedPerACHCompanyEdit,reloadCompanyApprovalGrid"
											><s:text name="jsp.default_366"/></sj:a>
										</ffi:cinclude>
										<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="notEquals">
											<ffi:cinclude value1="${GetCumulativeSettings.EnableACHCompanies}" value2="true" operator="equals">

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="notEquals">
												<ffi:setProperty name="perm_target" value="achcompanyaccessDiv"/>
												<ffi:setProperty name="completedTopics" value="completedPerACHCompanyEdit"/>
												<sj:a
													formIds="EditACHCompanyForm"
													targets="achcompanyaccessDiv"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" 
													onErrorTopics="errorSubmit"
													><s:text name="jsp.default_111"/></sj:a>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg12')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg13')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg14')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg27')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/achcompanyaccess-verify.jsp"/>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/achcompanyaccess-confirm.jsp"/>
											</ffi:cinclude>

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="equals">
												<sj:a
												    id="verifyPerAchAccountPermision"
													formIds="EditACHCompanyForm"
													targets="resultmessage"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" 
													onErrorTopics="errorSubmit"
													onSuccessTopics="completedPerACHCompanyEdit,reloadCompanyApprovalGrid"
													><s:text name="jsp.default_366"/></sj:a>
											</ffi:cinclude>

										</ffi:cinclude>

										<ffi:cinclude value1="${GetCumulativeSettings.EnableACHCompanies}" value2="false" operator="equals">
												<sj:a
													formIds="EditACHCompanyForm"
													targets="resultmessage"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" 
													onErrorTopics="errorSubmit"
													onSuccessTopics="completedPerACHCompanyEdit,reloadCompanyApprovalGrid"
													><s:text name="jsp.default_366"/></sj:a>
										</ffi:cinclude>
										</ffi:cinclude>
									</s:if>
									<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles' && #session.Section != 'UserProfile'}">

										<ffi:cinclude value1="${GetCumulativeSettings.EnableACHCompanies}" value2="true" operator="equals">

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="notEquals">
												<ffi:setProperty name="perm_target" value="achcompanyaccessDiv"/>
												<ffi:setProperty name="completedTopics" value="completedPerACHCompanyEdit"/>
												<sj:a
													formIds="EditACHCompanyForm"
													targets="achcompanyaccessDiv"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" 
													onErrorTopics="errorSubmit"
													><s:text name="jsp.default_111"/></sj:a>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg12')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg13')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg14')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
														<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg27')}" scope="request" />
														<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/achcompanyaccess-verify.jsp"/>
												<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
													<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
												</ffi:cinclude>
												<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/achcompanyaccess-confirm.jsp"/>
											</ffi:cinclude>

											<ffi:cinclude value1="${EditACHCompanyAccess.NumGrantedEntitlements}" value2="0" operator="equals">
												<sj:a
												    id="verifyPerAchAccountPermision"
													formIds="EditACHCompanyForm"
													targets="resultmessage"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" 
													onErrorTopics="errorSubmit"
													onSuccessTopics="completedPerACHCompanyEdit,reloadCompanyApprovalGrid"
													><s:text name="jsp.default_366"/></sj:a>
											</ffi:cinclude>

										</ffi:cinclude>

										<ffi:cinclude value1="${GetCumulativeSettings.EnableACHCompanies}" value2="false" operator="equals">
												<sj:a
													formIds="EditACHCompanyForm"
													targets="resultmessage"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" 
													onErrorTopics="errorSubmit"
													onSuccessTopics="completedPerACHCompanyEdit,reloadCompanyApprovalGrid"
													><s:text name="jsp.default_366"/></sj:a>
										</ffi:cinclude>

									</s:if>
									</div>
					</s:form>
	</div>	

<ffi:setProperty name="EditACHCompanyAccess" property="InitOnly" value="false" />

<ffi:removeProperty name="perBatchMax"/>
<ffi:removeProperty name="dailyMax"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckForRedundantACHLimits"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>

<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
