<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<%--
<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>
--%>


<ffi:setProperty name='PageText' value=''/>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${SecureUser.BusinessID}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
<ffi:process name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
    <ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
	</ffi:cinclude>
	<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>

<%-- BAI Export Settings --%>
<ffi:object name="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA" id="AddBAIExportSettingsToDA" scope="session" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="ReadFromDA" value="true" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="TmpBankIdDisplayText" value="${TempBankIdentifierDisplayText}" /> 
<ffi:process name="AddBAIExportSettingsToDA" />
<%-- END BAI Export Settings --%>

<ffi:removeProperty name="DA_TRANSACTION_GROUPS"/>

<%-- Dual Approval for Transaction Groups --%>
<ffi:object id="GetCompanyTransGroupsDA" name="com.ffusion.tasks.dualapproval.GetCompanyTransGroupsDA" />
<ffi:setProperty name="GetCompanyTransGroupsDA" property="ReadFromDA" value="true" />								
<ffi:process name="GetCompanyTransGroupsDA" />
<%-- END Dual Approval for Transaction Groups --%>


<%@page import="com.ffusion.tasks.dualapproval.IDACategoryConstants"%>

<script type="text/javascript">
	function reject()
	{
		document.frmDualApproval.action = "rejectDualApprovalChanges.action";
		document.frmDualApproval.submit();
	}

	// Handle text area maxlength
	$(document).ready(function(){
		ns.common.handleTextAreaMaxlength("#RejectReason");
	});
</script>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >

<ffi:cinclude value1="${tempSuccessUrl}" value2="">
	<ffi:setProperty name="tempSuccessUrl" value="${SuccessUrl}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SuccessUrl}" value2="">
	<ffi:setProperty name='SuccessUrl' value="${tempSuccessUrl}"/>
</ffi:cinclude>

	<span id="formError"></span>
	<ffi:setProperty name="subMenuSelected" value="company"/>
<div class="blockWrapper">
	<div  class="blockHead tableData"><span><strong>
		<ffi:cinclude value1="${rejectFlag}" value2="y" > 
			<!--L10NStart-->Verify Pending Changes for Rejection<!--L10NEnd-->
		</ffi:cinclude>
		<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals">
			<!--L10NStart-->Verify Pending Changes for Approval<!--L10NEnd-->
		</ffi:cinclude></strong></span>
	</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead adminBackground" ><!--L10NStart-->Business:<!--L10NEnd--></span>
				<span class="sectionsubhead adminBackground"    >
					<ffi:getProperty name="Business" property="BusinessName"/>
				</span>
			</div>
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead adminBackground"   ><!--L10NStart-->Submited By:<!--L10NEnd--></span>
				<span class="sectionsubhead adminBackground"   >
					<ffi:getProperty name="SubmittedUserName" property="userName"/>
				</span>
			</div>
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead adminBackground" "><!--L10NStart-->Submited On:<!--L10NEnd--></span>
				<span class="sectionsubhead adminBackground"   >
					<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
				</span>
			</div>
		</div>
	</div>
</div>
<s:include value="inc/da-verify-business-profile.jsp" />
<s:include value="inc/da-verify-business-administrator.jsp" />
<s:include value="inc/da-verify-business-account-configuration.jsp" />

<ffi:cinclude value1="${DA_BAIEXPORT_SETTINGS}" value2="" operator="notEquals">                               
	<s:include value="inc/da-verify-business-baiexport.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${DA_TRANSACTION_GROUPS}" value2="" operator="notEquals">
	<s:include value="inc/da-verify-business-trangroups.jsp" />
</ffi:cinclude>
	<table width="750" border="0" cellspacing="0" cellpadding="0" class="marginTop10 tableData">
					<%-- <tr>
						<td align="left" class="adminBackground sectionhead"	width="750">&gt;
							<ffi:cinclude value1="${rejectFlag}" value2="y" > 
								<!--L10NStart-->Verify Pending Changes for Rejection<!--L10NEnd-->
							</ffi:cinclude>
							<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals">
								<!--L10NStart-->Verify Pending Changes for Approval<!--L10NEnd-->
							</ffi:cinclude>
						</td>

					</tr>
					<tr>
						<td >	<table width="750" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" ><!--L10NStart-->Business:<!--L10NEnd--></td>
									<td width="110">&nbsp;</td>
									
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="108" ><!--L10NStart-->Submited 
									By<!--L10NEnd--></td>
																																				<td width="110">&nbsp;</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" "><!--L10NStart-->Submited On<!--L10NEnd--></td>
																		<td width="110">&nbsp;</td>
																											<td width="110">&nbsp;</td>

								</tr>
								<tr>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" colspan="2"  >
									<ffi:getProperty name="Business" property="BusinessName"/></td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17"  colspan="2">
									<ffi:getProperty name="SubmittedUserName" property="userName"/></td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17"  >
									<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/></td>
								</tr>
								
								
								
								<ffi:include page="${PathExt}user/inc/da-verify-business-profile.jsp"/>
								<ffi:include page="${PathExt}user/inc/da-verify-business-administrator.jsp"/>
								<ffi:include page="${PathExt}user/inc/da-verify-business-account-configuration.jsp"/>
								<ffi:cinclude value1="${DA_BAIEXPORT_SETTINGS}" value2="" operator="notEquals">                               
									<ffi:include page="${PathExt}user/inc/da-verify-business-baiexport.jsp" />
								</ffi:cinclude>
								<ffi:cinclude value1="${DA_TRANSACTION_GROUPS}" value2="" operator="notEquals">
									<ffi:include page="${PathExt}user/inc/da-verify-business-trangroups.jsp" />
								</ffi:cinclude>
								
								</table>
						</td>
					</tr> --%>
					<ffi:setProperty name="entitlementTypeOrder" property="Filter" value="EnableMenu=true" />
					<ffi:object id="DAWizardUtil" name="com.ffusion.tasks.dualapproval.DAWizardUtil" />
					<ffi:setProperty name="DAWizardUtil" property="currentPage" value="<%=IDACategoryConstants.IS_PROFILE_CHANGED %>" />
					<ffi:process name="DAWizardUtil" />
					<ffi:removeProperty name="DAWizardUtil"/>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
							<tr>
								<td width="100%" >
<%--									
									<form name="frmDualApproval" method="post" >
--%>
<s:form id="frmDualApproval" name="frmDualApproval" namespace="/pages/jsp/user" validate="false" method="post" theme="simple">

										<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
										<input type="hidden" name="itemId" value="<ffi:getProperty name="SecureUser" property="BusinessID"/>"/>
										<input type="hidden" name="itemType" value="Business"/>
										<input type="hidden" name="successUrl" value="<ffi:getProperty name="SecurePath"/>user/corpadmininfo.jsp"/>
										<table width="100%" >
											<tr>
												<td align="left" width="15%" class="sectionsubhead adminBackground" ><!--L10NStart-->Reject Reason :<!--L10NEnd--><span class="required">*</span></td><td><textarea id="RejectReason" name="REJECTREASON" rows="3" cols="70" maxlength="120"></textarea></td>
											</tr>
											<tr>
												<td align="center" colspan="2"><span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span></td>
											</tr>
										</table>
									<%--</form>--%>
									</s:form>
								</td>
							</tr>
						</ffi:cinclude>
					</ffi:cinclude>
				</table>
<div class="ffivisible" style="height:40px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>
<ffi:cinclude value1="${previouspage}" value2="" operator="notEquals">
<sj:a
	id="previousPageID2"
	button="true"
	onclick="previousPage();"
><s:text name="jsp.default_58" /></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${nextpage}" value2="" operator="notEquals">
<sj:a
	id="nextPageID2"
	button="true"
	onclick="nextPage();"
><s:text name="jsp.default_112" /></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
<s:url id="approveUrl" value="/pages/jsp/user/da-business-confirm.jsp">
<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
</s:url>

<sj:a id="approveLink"
href="%{approveUrl}"
targets="resultmessage"
button="true"
title="Approve Changes"
onClickTopics=""
onSuccessTopics="DACompanyApprovalReviewCompleteTopics"
onCompleteTopics="submitPendingApprovalForCompany"
onErrorTopics="">
<s:text name="jsp.da_company_approval_button_text"/>
</sj:a>

</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${rejectFlag}" value2="y" >
<ffi:cinclude value1="${nextpage}" value2="" operator="equals">

<s:url id="rejectUrl" value="/pages/dualapproval/rejectDualApprovalChanges.action">
<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
</s:url>

<sj:a id="rejectLink"
href="%{rejectUrl}"
formIds="frmDualApproval" 
targets="resultmessage"
button="true"
title="Reject Changes"
onClickTopics=""
onSuccessTopics="DACompanyApprovalReviewCompleteTopics"
onCompleteTopics=""
onErrorTopics="">
<s:text name="jsp.da_company_reject_button_text"/>
</sj:a>


</ffi:cinclude>
</ffi:cinclude>
</div>
	<ffi:flush/>

<ffi:setProperty name="url" value="${SecurePath}user/da-summary-business-profile.jsp?PermissionsWizard=TRUE"  URLEncrypt="true" />
<ffi:setProperty name="BackURL" value="${url}"/>

<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="SubmittedUserName"/>

<script>
	function nextPage(){
		$.ajax({
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${nextpage.MenuUrl}&ParentMenu=${nextpage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${nextpage.PageKey}&SortKey=${nextpage.NextSortKey}"/>',

			success: function(data)
			{
				$('#approvalWizardDialogID').html(data);
				ns.admin.checkResizeApprovalDialog();
			}
		});
	}

	function previousPage(){
		$.ajax({
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${previouspage.MenuUrl}&ParentMenu=${previouspage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${previouspage.PageKey}&SortKey=${previouspage.NextSortKey}"/>',
			success: function(data)
			{
				$('#approvalWizardDialogID').html(data);
				ns.admin.checkResizeApprovalDialog();
			}
		});
	}
</script>