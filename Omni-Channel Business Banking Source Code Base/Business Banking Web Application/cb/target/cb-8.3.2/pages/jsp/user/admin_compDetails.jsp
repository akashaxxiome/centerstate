<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="companyDetailsPortlet" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title"><s:text name="jsp.user_72"/></span>
	</div>
	<div class="portlet-content">
        <sj:tabbedpanel id="companyContent" >

        </sj:tabbedpanel>
    </div>
    <div id="companySummaryDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('companyDetailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>
<script type="text/javascript">

	//Initialize portlet with settings & calendar icon
	ns.common.initializePortlet("companyDetailsPortlet");
</script>
