<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:setProperty name="FFISetSecondaryUsersActiveState" property="Initialize" value="FALSE"/>
<ffi:setProperty name="FFISetSecondaryUsersActiveState" property="Process" value="TRUE"/>
<ffi:setProperty name="FFISetSecondaryUsersActiveState" property="UpdateOnly" value="FALSE"/>
<ffi:setProperty name="FFISetSecondaryUsersActiveState" property="SuccessURL" value=""/>
<ffi:process name="FFISetSecondaryUsersActiveState"/>
<ffi:removeProperty name="TempURL"/>
