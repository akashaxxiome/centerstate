<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:object id="EditWireTemplatePermissionsDA" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>

<ffi:setProperty name="EditWireTemplatePermissionsDA"  property="approveAction" value="true" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditWireTemplatePermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditWireTemplatePermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditWireTemplatePermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditWireTemplatePermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditWireTemplatePermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>
<!-- Cash Management -->
<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
<ffi:setProperty name="GetCategories" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_WIRE_TEMPLATE_ACCESS_LIMITS %>"/>
<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
<ffi:setProperty name="approveCategory" property="categorySubType" value="" />
<ffi:removeProperty name="categories" />
<ffi:process name="GetCategories" />

<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:process name="EditWireTemplatePermissionsDA" />
	<ffi:list collection="categories" items="category" >
		<ffi:setProperty name="approveCategory" property="ObjectId" value="${category.ObjectId}"/>
		<ffi:setProperty name="approveCategory" property="ObjectType" value="${category.ObjectType}"/>
		<ffi:setProperty name="approveCategory" property="categorySubType" value="${category.categorySubType}"/>
		<ffi:setProperty name="approveCategory" property="categoryType" value="${category.categoryType}"/>
		<ffi:process name="ApprovePendingChanges"/>
	</ffi:list>
</ffi:cinclude>
<ffi:removeProperty name="EditWireTemplatePermissionsDA" />