<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_baiexportsettingsedit-confirm" className="moduleHelpClass"/>
<div align="center">
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"  operator="notEquals">
								<s:text name="jsp.user_57"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
								<s:text name="jsp.user.label.bai.changes.sent.for.approval"/>
						</ffi:cinclude>
</div>
	<%-- <table class="adminBackground" style="width: 100%" border="0" cellspacing="0" cellpadding="0">
					<TR>
						<TD class="columndata" style="text-align: center">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"  operator="notEquals">
								<s:text name="jsp.user_57"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
								<s:text name="jsp.user.label.bai.changes.sent.for.approval"/>
						</ffi:cinclude>
						</TD>
					</TR>
					<TR>
						<TD style="text-align: center">
							<sj:a  
							    button="true" 
								onClickTopics="cancelCompanyForm,refreshBAIExportSettings,reloadPendingBusinessIsland"
							    ><s:text name="jsp.default_175"/></sj:a>
						</TD>
					</TR>
	</TABLE> --%>
<div class="btn-row">
<sj:a  
    button="true" 
	onClickTopics="cancelCompanyForm,refreshBAIExportSettings"
    ><s:text name="jsp.default_175"/></sj:a>
</div>					


		
<ffi:removeProperty name="SetBAIExportSettings"/>
		
<%-- Dual Approval Processing Clean up --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">	
	<ffi:removeProperty name="AddBAIExportSettingsToDA" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.OLD_BAIEXPORT_SETTINGS %>" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.NEW_BAIEXPORT_SETTINGS %>" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS %>" />
</ffi:cinclude>
<%-- END Dual Approval Processing Clean up --%>

