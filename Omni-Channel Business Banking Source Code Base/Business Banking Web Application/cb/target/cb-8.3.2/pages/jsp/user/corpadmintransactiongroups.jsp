<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<span id="pageHeading" style="display:none"><s:text name="jsp.default_575"/> </span>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<s:include value="inc/corpadmininfo-pre.jsp" />
<div id="corpadmintransactiongroupsDiv" class="marginleft5">
<ffi:help id="user_corpadmintransactiongroups" className="moduleHelpClass"/>
	<ffi:object name="com.ffusion.tasks.business.GetTransactionGroups" id="GetTransactionGroups" scope="request" />
	<ffi:process name="GetTransactionGroups" />

	<ffi:removeProperty name="transactionGroupName"/>
	<%-- 
	<tr align="left">
		<!-- <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="11" border="0"></td> -->
		<td colspan="2">
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
				<td class="sectionhead" valign="baseline" align="right" height="17" >
						&gt; <s:text name="jsp.default_575"/>
					</td> --%>
					<td colspan="3" class="columndata" align="left" valign="baseline">
						<%-- <ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
			
							<s:url id="addTransactionGroupURL" value="%{#session.PagesPath}user/corpadmintrangroupaddedit.jsp" escapeAmp="false">
								<s:param name="addFlag" value="%{'true'}" />
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
							</s:url>
							<sj:a
								id="addTransactionGroupBtn"
								href="%{addTransactionGroupURL}"
								targets="companyContent"
								onClickTopics="beforeLoad" onCompleteTopics="completeCompanyProfileLoad" onErrorTopics="errorLoad"
								button="true" buttonIcon="ui-icon-add"
							><s:text name="jsp.user_18"/></sj:a>

						</ffi:cinclude> 
					</td>
				</tr>
			</table>
		</td>
		<td><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="11" border="0"></td>
	</tr>--%>
	<div class="paneWrapper">
  	<div class="paneInnerWrapper">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">
			<tr class="header">
				<td class="columndata adminBackground" valign="baseline" width="50%">Group Name</td>
				<td class="columndata" valign="baseline" align="left" width="50%">Action</td>
			</tr>
			<%-- Dual approval for transaction groups --%>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:object id="GetCompanyTransGroupsDA" name="com.ffusion.tasks.dualapproval.GetCompanyTransGroupsDA" />
				<ffi:setProperty name="GetCompanyTransGroupsDA" property="TransGroupsTaskName" value="GetTransactionGroups" />								
				<ffi:setProperty name="GetCompanyTransGroupsDA" property="PopulateFields" value="true" />
				<ffi:process name="GetCompanyTransGroupsDA" />
				
				<%-- DA transaction groups --%>	
				<ffi:list collection="GetCompanyTransGroupsDA.DaTransGroupNames" items="col1Name,col2Name">
					<tr>
					    <%-- transaction group name in first column --%>
						<td class="columndata adminBackground" valign="baseline" align="left" height="17" width="200">
							<ffi:list collection="col1Name" items="newTGDA,oldTGDA,actionDA">
								<span class="sectionheadDA"><ffi:getProperty name="newTGDA" /></span>
								<ffi:cinclude value1="${newTGDA}" value2="${oldTGDA}" operator="notEquals"> 
									&nbsp;&nbsp;<span class="sectionhead_greyDA">
									<ffi:getProperty name="oldTGDA" />
									<ffi:cinclude value1="${actionDA}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>" operator="equals">
										<ffi:getProperty name="actionDA" />
									</ffi:cinclude>
									</span>
								</ffi:cinclude>
							</ffi:list>
						</td>
						<td class="columndata" valign="baseline" align="left" >
							<ffi:setProperty name="GetCompanyTransGroupsDA" property="CurrentTranGroupName" value="${newTGDA}" />
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">								
								<s:url id="ViewTGURL" value="%{#session.PagesPath}user/corpadmintrangroupview.jsp" escapeAmp="false">							  
									<s:param name="addFlag" value="%{'false'}" />
									<s:param name="EditTransactionGroup_GroupName" value="%{#attr.newTGDA}" />
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
								</s:url>
								<sj:a href="%{ViewTGURL}" targets="DACompanyTGViewDialogId" button="true" onClickTopics="beforeLoad" onSuccessTopics="openDATGViewDialog">
									<s:text name="jsp.default_459"/>
								</sj:a>
							</ffi:cinclude>
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
								<ffi:cinclude value1="${actionDA}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>" operator="notEquals">										
									<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupaddedit.jsp?addFlag=false&EditTransactionGroup_GroupName=${newTGDA}" URLEncrypt="true"/>
									<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_179" />' href='#' onClick=
										"ns.admin.editTG('<ffi:getProperty name="tempURL"/>')">
										<s:text name="jsp.default_178"/>
									</a>
									<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupdelete-verify.jsp?DeleteTransactionGroup_GroupName=${newTGDA}" URLEncrypt="true"/>
									<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_163" />' href='#' onClick=
										"ns.admin.delTG('<ffi:getProperty name="tempURL"/>')">
										Delete
									</a>
								</ffi:cinclude>
							</ffi:cinclude>
						</td>
						</tr>
						<tr>

						<%-- transaction group name in second column, if there is one to display --%>
						<ffi:cinclude value1="${col2Name}" value2="" operator="notEquals">
						<td class="columndata adminBackground" valign="baseline" align="left" height="17" width="200">
							<ffi:list collection="col2Name" items="newTGDA, oldTGDA, actionDA">
							<span class="sectionheadDA"><ffi:getProperty name="newTGDA" /></span>
							<ffi:cinclude value1="${newTGDA}" value2="${oldTGDA}" operator="notEquals"> 
									&nbsp;&nbsp;<span class="sectionhead_greyDA"><ffi:getProperty name="oldTGDA" />
									<ffi:cinclude value1="${actionDA}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>" operator="equals">
										<ffi:getProperty name="actionDA" />
									</ffi:cinclude>
									</span>
							</ffi:cinclude>
							</ffi:list>
						</td>
						<td class="columndata" valign="baseline" align="left" >
							<ffi:setProperty name="GetCompanyTransGroupsDA" property="CurrentTranGroupName" value="${newTGDA}" />
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
								<s:url id="ViewTGURL" value="%{#session.PagesPath}user/corpadmintrangroupview.jsp" escapeAmp="false">						 <s:param name="addFlag" value="%{'false'}" />
									<s:param name="EditTransactionGroup_GroupName" value="%{#attr.newTGDA}" />
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
								</s:url>
								<sj:a href="%{ViewTGURL}" targets="DACompanyTGViewDialogId" button="true" 
									  onClickTopics="beforeLoad"
									  onSuccessTopics="openDATGViewDialog"><s:text name="jsp.default_459"/></sj:a>												
							</ffi:cinclude>
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
								<ffi:cinclude value1="${actionDA}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>" operator="notEquals">										
									<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupaddedit.jsp?addFlag=false&EditTransactionGroup_GroupName=${newTGDA}" URLEncrypt="true"/>
									<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_179" />' href='#' onClick=
										"ns.admin.editTG('<ffi:getProperty name="tempURL"/>')">
										<s:text name="jsp.default_178"/>
									</a>																			
									<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupdelete-verify.jsp?DeleteTransactionGroup_GroupName=${newTGDA}" URLEncrypt="true"/>
									<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_163" />' href='#' onClick=
										"ns.admin.delTG('<ffi:getProperty name="tempURL"/>')">
										Delete
									</a>										
								</ffi:cinclude>
							</ffi:cinclude>
						</td>
						</ffi:cinclude>

						<ffi:cinclude value1="${col2Name}" value2="" operator="equals">
						<!-- <td class="columndata adminBackground" valign="baseline" align="left" height="17" width="225"></td>
						<td class="columndata" valign="baseline" align="left" ></td> -->
						</ffi:cinclude>
					</tr>
				</ffi:list>
				
			</ffi:cinclude>
			
			<ffi:list collection="GetTransactionGroups.TransactionGroups" items="col1Name,col2Name">
			<tr>
			    <%-- transaction group name in first column --%>
				<td class="columndata adminBackground" valign="baseline" align="left" height="17" width="200">
					<ffi:getProperty name="col1Name"/></td>	
														
				<td class="columndata" valign="baseline" align="left" >
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
							<s:url id="ViewTGURL" value="%{#session.PagesPath}user/corpadmintrangroupview.jsp" escapeAmp="false">							  
									<s:param name="addFlag" value="%{'false'}" />
									<s:param name="EditTransactionGroup_GroupName" value="%{#attr.col1Name}" />
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
							</s:url>
							<sj:a 	 href="%{ViewTGURL}" targets="DACompanyTGViewDialogId" button="true" 
									  onClickTopics="beforeLoad"
									  onSuccessTopics="openDATGViewDialog"><s:text name="jsp.default_459"/></sj:a>
					</ffi:cinclude>
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">

						<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupaddedit.jsp?addFlag=false&EditTransactionGroup_GroupName=${col1Name}" URLEncrypt="true"/>
						<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_179" />' href='#' onClick=
							"ns.admin.editTG('<ffi:getProperty name="tempURL"/>')">
							<s:text name="jsp.default_178"/>
						</a>

						<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupdelete-verify.jsp?addFlag=false&DeleteTransactionGroup_GroupName=${col1Name}" URLEncrypt="true"/>
						<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_163" />' href='#' onClick=
							"ns.admin.delTG('<ffi:getProperty name="tempURL"/>')">
							Delete
						</a>													
					</ffi:cinclude>
				</td>

				</tr>
				<tr>

				<%-- transaction group name in second column, if there is one to display --%>
				<ffi:cinclude value1="${col2Name}" value2="" operator="notEquals">
					<td class="columndata adminBackground" valign="baseline" align="left" height="17" width="200"><ffi:getProperty name="col2Name"/></td>
					<td class="columndata" valign="baseline" align="left" >
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
					
							<s:url id="ViewTGURL" value="%{#session.PagesPath}user/corpadmintrangroupview.jsp" escapeAmp="false">							  
									<s:param name="addFlag" value="%{'false'}" />
									<s:param name="EditTransactionGroup_GroupName" value="%{#attr.col2Name}" />
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
							</s:url>
							<sj:a  href="%{ViewTGURL}" targets="DACompanyTGViewDialogId" button="true" 
									  onClickTopics="beforeLoad"
									  onSuccessTopics="openDATGViewDialog"><s:text name="jsp.default_459"/></sj:a>
					</ffi:cinclude>
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
						<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupaddedit.jsp?addFlag=false&EditTransactionGroup_GroupName=${col2Name}" URLEncrypt="true"/>
									<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_179" />' href='#' onClick=
										"ns.admin.editTG('<ffi:getProperty name="tempURL"/>')">
										<s:text name="jsp.default_178"/>
									</a>								
						<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpadmintrangroupdelete-verify.jsp?addFlag=false&DeleteTransactionGroup_GroupName=${col2Name}" URLEncrypt="true"/>
									<a class='ui-button ui-widget ui-state-default ui-button-text-only anchotBtn' title='<s:text name="jsp.default_163" />' href='#' onClick=
										"ns.admin.delTG('<ffi:getProperty name="tempURL"/>')">
									Delete
									</a>	
					</ffi:cinclude>
					</td>
					</ffi:cinclude>

					<ffi:cinclude value1="${col2Name}" value2="" operator="equals">
					<!-- <td class="columndata adminBackground" valign="baseline" align="left" height="17" width="225"></td>
					<td class="columndata" valign="baseline" align="left" ></td> -->
					</ffi:cinclude>
			</tr>
			</ffi:list>

			</table></div></div>

	<ffi:removeProperty name="GetTransactionGroups" />
</div>				
