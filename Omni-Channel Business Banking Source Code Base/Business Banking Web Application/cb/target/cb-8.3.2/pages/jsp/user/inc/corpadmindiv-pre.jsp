<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_195')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%-- if we added or canceled adding a new division, we need to do a bit of clean up --%>
<ffi:removeProperty name="GroupDivAdminEdited"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="modifiedAdmins"/>
<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>
<ffi:removeProperty name="DivisionId"/>
<ffi:removeProperty name="DivisionName"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />

<ffi:object id="GetDescendantsByType" name="com.ffusion.efs.tasks.entitlements.GetDescendantsByType" scope="session"/>
<ffi:setProperty name="GetDescendantsByType" property="GroupID" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="GetDescendantsByType" property="GroupType" value="Division"/>
<ffi:process name="GetDescendantsByType"/>
<ffi:removeProperty name="GetDescendantsByType" />

<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="BusinessEmployee"/>
<ffi:process name="GetGroupsAdministeredBy"/>
<ffi:removeProperty name="GetGroupsAdministeredBy" />

<ffi:object id="GetSupervisorFor" name="com.ffusion.tasks.admin.GetSupervisorFor" scope="session" />

<%-- Add some properties to the EntitlementGroup hashmap for use in sorting. --%>
<ffi:object id="GetAncestorGroupByType" name="com.ffusion.tasks.admin.GetAncestorGroupByType" scope="session"/>
<ffi:list collection="Descendants" items="DescendantItem">
    <ffi:setProperty name="GetAncestorGroupByType" property="GroupType" value="Division"/>
    <ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="DescendantItemDivision"/>
    <ffi:setProperty name="GetAncestorGroupByType" property="GroupID" value="${DescendantItem.GroupId}"/>
    <ffi:process name="GetAncestorGroupByType"/>
    <ffi:setProperty name="DescendantItem" property="Division" value="${DescendantItemDivision.GroupName}"/>

    <ffi:setProperty name="GetSupervisorFor" property="GroupId" value="${DescendantItem.GroupId}"/>
    <ffi:process name="GetSupervisorFor"/>
    <ffi:setProperty name="DescendantItem" property="Supervisor" value="${GetSupervisorFor.Name}"/>

	<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
	<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
   	<ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${DescendantItem.GroupId}" />
	<ffi:process name="GetAdministratorsForGroups"/>
	<ffi:removeProperty name="GetAdministratorsForGroups"/>

	<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
	<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
	<ffi:process name="GetBusinessEmployeesByEntGroups"/>
	<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />

	<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
		<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
	<ffi:process name="FilterNotEntitledEmployees"/>

	<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>

	<%--obtain the name of each employee in BusinessEmployees--%>
 	<%
		boolean isFirst= true;
 		String temp = "";
 	%>
 	<ffi:list collection="BusinessEmployees" items="emp">
 		<%--display separator if this is not the first one--%>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
	 		<ffi:setProperty name="empName" value="${emp.Name}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
			<ffi:setProperty name="empName" value="${emp.LastName}"/>
		</ffi:cinclude>
 		<%
 			if( isFirst ) {
 				isFirst = false;
 				temp = temp + session.getAttribute( "empName" );
 			} else {
 				temp = temp + ", " + session.getAttribute( "empName" );
 			}
 		%>
 	</ffi:list>
 	<%
 			if( temp.length() > 60 ) {
 				temp = temp.substring( 0, 60 );
 				int index = temp.lastIndexOf( ',' );
 				if(index != -1){
 					temp = temp.substring( 0, index ) + "...";	
 				}else{
 					temp = temp+ "...";
 				}
 				
 			}
 			session.setAttribute( "AdministratorStr", temp );%>

	<ffi:setProperty name="DescendantItem" property="Administrator" value="${AdministratorStr}"/>
	<ffi:removeProperty name="AdministratorStr"/>
 	<ffi:removeProperty name="empName"/>



	<ffi:setProperty name="DescendantItem" property="IsAnAdminOf" value="false"/>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
		<ffi:setProperty name="DescendantItem" property="LocationsAdmin" value="true"/>
	</ffi:cinclude>
	<ffi:list collection="Entitlement_EntitlementGroups" items="EntitlementGroupItem">
		<ffi:cinclude value1="${DescendantItem.GroupId}" value2="${EntitlementGroupItem.GroupId}" operator="equals">
			<ffi:setProperty name="DescendantItem" property="IsAnAdminOf" value="true"/>
		</ffi:cinclude>
	</ffi:list>
</ffi:list>

<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
    <ffi:setProperty name="Descendants" property="SortedBy" value="${SortedBy}"/>
</ffi:cinclude>

<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmindiv.jsp"/>

<%-- get pending approval divisions --%>
<ffi:object id="GetDescendantsByTypeDA" name="com.ffusion.tasks.dualapproval.GetDescendantsByTypeDA" scope="session" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="UserSessionName" value="BusinessEmployees" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="DecendantsSessionName" value="Descendants" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_DIVISION %>" />
<ffi:setProperty name="GetDescendantsByTypeDA" property="MaxAdminStringLength" value="60" />
<ffi:process name="GetDescendantsByTypeDA" />
<ffi:removeProperty name="GetDescendantsByTypeDA" />

<%-- display pending approval grid if it has content --%>
<ffi:cinclude value1="${PendingApprovalGroups.size}" value2="0" operator="equals">
	<script>
		ns.admin.pendingApprovalDivisionsNum = "0";
		$('#division_PendingApprovalTab').attr('style','display:none');
	</script>
</ffi:cinclude>

<ffi:cinclude value1="${PendingApprovalGroups.size}" value2="0" operator="notEquals">
	<script>
		ns.admin.pendingApprovalDivisionsNum = '<ffi:getProperty name="PendingApprovalGroups" property="Size"/>';
		$('#division_PendingApprovalTab').removeAttr('style');
		if($("#pendingDivisionChangesID").jqGrid('getGridParam', 'reccount') != ns.admin.pendingApprovalDivisionsNum){
			// $('#pendingDivisionChangesID').trigger("reloadGrid");
		}
	</script>
</ffi:cinclude>
