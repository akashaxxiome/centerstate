<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="GetEntitledComponents" name="com.ffusion.tasks.admin.GetEntitledComponents"/>
	<ffi:setProperty name="GetEntitledComponents" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_ACCOUNT %>"/>
	<ffi:setProperty name="GetEntitledComponents" property="groupID" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetEntitledComponents" property="CollectionName" value="ComponentNamesPer"/>
	<ffi:setProperty name="GetEntitledComponents" property="LocalizedCollectionName" value="LocalizedComponentNamesPer"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="GetEntitledComponents" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
		<ffi:setProperty name="GetEntitledComponents" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
		<ffi:setProperty name="GetEntitledComponents" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	</ffi:cinclude>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetEntitledComponents" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
		<ffi:setProperty name="GetEntitledComponents" property="userID" value="${BusinessEmployeeId}"/>
		<ffi:setProperty name="GetEntitledComponents" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetEntitledComponents" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetEntitledComponents" property="DaCategorySessionName" value="EntitlementBean"/>
	</s:if>
	<s:if test="%{#session.Section == 'UserProfile'}">
			<ffi:setProperty name="GetEntitledComponents" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
</s:if> 
<ffi:process name="GetEntitledComponents"/>
<ffi:removeProperty name="GetEntitledComponents"/>
