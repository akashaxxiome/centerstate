<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<ffi:setL10NProperty name='PageHeading' value='View Division'/>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>



<%
	String EditDivision_GroupId = request.getParameter("EditDivision_GroupId");
	if(EditDivision_GroupId!= null){
		session.setAttribute("EditDivision_GroupId", EditDivision_GroupId);
	}
	
	String EditGroup_GroupId = request.getParameter("EditGroup_GroupId");
	if(EditGroup_GroupId!= null){
		session.setAttribute("EditGroup_GroupId", EditGroup_GroupId);
	}
	
	String EditDivision_ACHCompID = request.getParameter("EditDivision_ACHCompID");
	if(EditDivision_ACHCompID!= null){
		session.setAttribute("EditDivision_ACHCompID", EditDivision_ACHCompID);
	}
	
	String EntGroupItemACHCompanyName = request.getParameter("EntGroupItemACHCompanyName");
	if(EntGroupItemACHCompanyName!= null){
		session.setAttribute("EntGroupItemACHCompanyName", EntGroupItemACHCompanyName);
	}
		
	String Section = request.getParameter("Section");
	if(Section!= null){
		session.setAttribute("Section", Section);
	}
%>


<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadmindivadd.jsp"><img src="/cb/cb/${ImgExt}grafx/user/i_adddivision.gif" alt="" width="74" height="16" border="0"></a>'/>
</ffi:cinclude>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
<ffi:setProperty name='PageText' value=''/>
</ffi:cinclude>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
// The user has pressed cancel, so go back to the previous version of the collections
session.setAttribute( "NonAdminEmployees", session.getAttribute("NonAdminEmployeesCopy"));
session.setAttribute( "AdminEmployees", session.getAttribute("AdminEmployeesCopy"));
session.setAttribute( "NonAdminGroups", session.getAttribute("NonAdminGroupsCopy"));
session.setAttribute( "AdminGroups", session.getAttribute("AdminGroupsCopy"));
session.setAttribute( "adminMembers", session.getAttribute("adminMembersCopy"));
session.setAttribute( "userMembers", session.getAttribute("userMembersCopy"));
session.setAttribute( "groupIds", session.getAttribute("groupIdsCopy"));
session.setAttribute( "tempAdminEmps", session.getAttribute("tempAdminEmpsCopy") );
session.setAttribute( "GroupDivAdminEdited", session.getAttribute("GroupDivAdminEditedCopy")); 
%>
</ffi:cinclude>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="CancelReload"/>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>

    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
    <ffi:process name="GetEntitlementGroup"/>
	<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
	<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
	<ffi:object id="GetSupervisorFor" name="com.ffusion.tasks.admin.GetSupervisorFor" scope="request" />
	<ffi:setProperty name="GetSupervisorFor" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:process name="GetSupervisorFor"/>
	<ffi:cinclude value1="${GetSupervisorFor.SupervisorFound}" value2="true" operator="equals">
		<ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value="${Supervisor.Id}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${GetSupervisorFor.SupervisorFound}" value2="true" operator="notEquals">
		<ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value=""/>
	</ffi:cinclude>

	<%-- Set this flag for determining if to load the Entitlement groups for SetADministrator task --%>
	<ffi:setProperty name="editDivisionTouched" value="true"/>

    <ffi:setProperty name="EditGroup_DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="request" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT%>">
	<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
	<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
	<ffi:process name="GetDisplayCount"/>
	<ffi:removeProperty name="GetDisplayCount"/>

	<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
	<ffi:setProperty name ="GetLocations" property="DivisionID" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetLocations" property="LocationName" value="${LocationSearch_Name}"/>
	<ffi:setProperty name="GetLocations" property="LocationID" value="${LocationSearch_ID}"/>
	<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
	<ffi:process name="GetLocations"/>

	<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
    	<ffi:setProperty name="Locations" property="SortedBy" value="${SortedBy}"/>
	</ffi:cinclude>

	<ffi:setProperty name="EditDivision_ShowLocSearch" value="TRUE"/> 
	<ffi:cinclude value1="${GetLocations.LocationName},${GetLocations.LocationID}" value2="," operator="equals">
		<ffi:cinclude value1="${DisplayCount}" value2="${Locations.size}" operator="notEquals">
			<ffi:setProperty name="EditDivision_ShowLocSearch" value="FALSE"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<!-- Dual Approval for profile starts-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="EditBusinessGroup"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
	
	
	
	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="PopulateObjectWithDAValues"/>
	
</ffi:cinclude>
<!-- Dual Approval ends -->

		<div align="center">
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionhead">
                                				<ffi:getProperty name="Business" property="BusinessName"/><br><br>
							</span>
			                        </div>
					</td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="adminBackground" width="708">
						<form action="<ffi:getProperty name="SecurePath"/>user/corpadmindivedit-verify.jsp" name="FormName" method="post">
							<table width="711" border="0" cellspacing="0" cellpadding="3">
                    			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<input type="hidden" name="EditBusinessGroup.ParentGroupId" value="<ffi:getProperty name="Entitlement_EntitlementGroup" property="ParentId"/>">
								<input type="hidden" name="SecurePath" value="<ffi:getProperty name="SecurePath"/>">
								<input type="hidden" name="EditBusinessGroup.SessionGroupName" value="Entitlement_EntitlementGroup">
								<input type="hidden" name="EditBusinessGroup.CheckboxesAvailable" value="false">
								<input type="hidden" name="EditBusinessGroup.SuccessURL" value="<ffi:urlEncrypt url="${SecurePath}redirect.jsp?target=${SecurePath}user/corpadmindiv.jsp" />">							
								<tr>	
									<td colspan="2" class="<ffi:getPendingStyle fieldname="groupName" defaultcss="sectionhead" />" width="150" align="left" nowrap>
										<span class="sectionheadDA"><s:text name="jsp.user_121" />:</span> &nbsp;
									<!-- </td>
									<td class="columndata" width="441"> -->
										<ffi:getProperty name="EditBusinessGroup" property="GroupName"/> <span class="sectionsubhead">(<s:text name="admin.view.new.value" />)</span>
										&nbsp;<span class="sapUiIconCls icon-arrow-right"></span><ffi:getProperty name="oldDAObject" property="GroupName"/> <span class="sectionsubhead">(<s:text name="admin.view.old.value" />)</span>
									</td>
								</tr>							
																
								<!-- Dual Approval for administrator starts -->
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								
									<ffi:removeProperty name="CATEGORY_BEAN"/>
								
									<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>
								
									<%-- based on the business employee object, get the business employees for the business --%>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
									<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
									<ffi:process name="GetBusinessEmployees"/>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:removeProperty name="TempBusinessEmployee"/>
									
									<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
										<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
									<ffi:process name="GetDACategoryDetails"/>
									
									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">		
										<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
										<ffi:process name="GenerateUserAndAdminLists"/>
										<ffi:removeProperty name="adminIds"/>
										<ffi:removeProperty name="userIds"/>
										<ffi:removeProperty name="GenerateUserAndAdminLists"/>
										
										<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
										<ffi:process name="GenerateListsFromDAAdministrators"/>
									</ffi:cinclude>
									
									<ffi:object id="GenerateUserAdminList" name="com.ffusion.tasks.dualapproval.GenerateUserAdminList"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminEmployees" value="AdminEmployees"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminGroups" value="AdminGroups"/>
									<ffi:setProperty name="GenerateUserAdminList" property="sessionName" value="NewAdminBusinessEmployees"/>
									<ffi:process name="GenerateUserAdminList"/>
									
									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="NewAdminBusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="NewAdminUserStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									
									<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="OldAdminEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="OldAdminGroups"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="tempUserName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="tempGroupName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
									<ffi:process name="GenerateUserAndAdminLists"/>
									<ffi:removeProperty name="adminIds"/>
									<ffi:removeProperty name="userIds"/>
									<ffi:removeProperty name="tempUserName"/>
									<ffi:removeProperty name="tempGroupName"/>
									<ffi:removeProperty name="GenerateUserAndAdminLists"/>
									
									<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
									<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
									  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
									<ffi:process name="GetBusinessEmployeesByEntGroups"/>
									<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
									
									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="OldAdminStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									
									<ffi:removeProperty name="GetDACategoryDetails"/>
									<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
									<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
									<ffi:removeProperty name="NewAdminStringList"/>
									<ffi:removeProperty name="BusinessEmployees"/>
									<ffi:removeProperty name="NewAdminBusinessEmployees"/>
									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
								</ffi:cinclude>
								<!-- Dual Approval for administrator ends -->
								
								<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
								<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
								  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
								<ffi:process name="GetBusinessEmployeesByEntGroups"/>
								<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
																
								<tr>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
										<td class="adminBackground sectionsubhead" align="left" valign="baseline" width="50">
												<s:text name="jsp.user_38" />:&nbsp; 
										</td>
									</ffi:cinclude>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<td class="adminBackground sectionheadDA" align="left" valign="baseline" width="50">
												<s:text name="jsp.user_38" />:&nbsp; 
										</td>
									</ffi:cinclude>
									<td class="columndata adminBackground" valign="baseline">
									<%--display name of each employee in BusinessEmployees--%>
									<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>

									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="AdministratorStr"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="maxLength" value="120"/>
									<ffi:process name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									
 									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
	 									<ffi:getProperty name="AdministratorStr"/>
	 									<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
											<!--L10NStart-->None<!--L10NEnd-->
										</ffi:cinclude>
	 								</ffi:cinclude>
	 								<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	 									<ffi:getProperty name="NewAdminUserStringList"/>
	 								</ffi:cinclude>
									<ffi:removeProperty name="empName"/>
									<%--
									<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminadminview.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
									<input type="image" src="/cb/cb/multilang/grafx/user/i_view.gif" alt="View" border="0" onclick="document.FormName.submit();document.FormName.action='<ffi:getProperty name="tmp_url"/>';">
									--%>
										&nbsp;&nbsp;
										<s:url id="DivisionAdministratorsViewURL" value="%{#session.PagesPath}user/corpadminadminview.jsp" escapeAmp="false">
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
											<s:param name="UseLastRequest" value="%{'TRUE'}"/>
										</s:url>
										<sj:a  
											href="%{DivisionAdministratorsViewURL}" 
											targets="viewAdminInViewDiv" 
											button="true" 
											title="View"
											onCompleteTopics="viewAdminInViewDivTopic"
										>
											<s:text name="jsp.default_6" />
										</sj:a>
									<ffi:removeProperty name="tmp_url"/>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<span class="sectionhead_greyDA">
												<br>
												<ffi:getProperty name="OldAdminStringList"/>
										</span>	
									</ffi:cinclude>
									</td>
								</tr>							
							</table>
						</form>
					</td>
				</tr>
			</table>
			<p></p>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT%>">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
					<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
					<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
					<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
					<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
					<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
				<ffi:process name="GetDACategoryDetails"/>
				
				<ffi:object id="GetLocationsDA" name="com.ffusion.tasks.dualapproval.GetLocationsDA"/>
				<ffi:setProperty name="GetLocationsDA" property="LocationsSessionName" value="Locations"/>
				<ffi:setProperty name="GetLocationsDA" property="DALocationsSessionName" value="DALocations"/>
				<ffi:process name="GetLocationsDA"/>
				
				<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
		   			<ffi:setProperty name="DALocations" property="SortedBy" value="${SortedBy}"/>
				</ffi:cinclude>
			</ffi:cinclude>
			
			
				<ffi:cinclude value1="${EditDivision_ShowLocSearch}" value2="TRUE" operator="equals">
		            <table width="750" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" class="adminBackground" width="708">
	                            <table width="711" border="0" cellspacing="3" cellpadding="0">
			                        <form action="<ffi:getProperty name="SecurePath" />user/corpadmindivview.jsp" method="post" name="frmFilter">
										<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		                                <tr>
		                                    <td class="tbrd_b" colspan="4" align="left" nowrap><span class="sectionsubhead"><!--L10NStart-->Location Search<!--L10NEnd--></span></td>
		                                </tr>
		                                <tr>
		                                    <td align="left" nowrap>&nbsp;</td>
		                                    <td align="right" class="sectionsubhead"><!--L10NStart-->Location Name<!--L10NEnd--></td>
		                                    <td colspan="2" align="left"><input class="txtbox" type="text" name="LocationSearch_Name" size="25" maxlength="16" border="0" value="<ffi:getProperty name="LocationSearch_Name" />"></td>
		                                </tr>
		                                <tr>
		                                    <td align="left" nowrap>&nbsp;</td>
		                                    <td align="right" class="sectionsubhead"><!--L10NStart-->Location ID<!--L10NEnd--></td>
		                                    <td colspan="2" align="left"><input class="txtbox" type="text" name="LocationSearch_ID" size="25" maxlength="15" border="0" value="<ffi:getProperty name="LocationSearch_ID" />"></td>
		                                </tr>
		                                <tr>
		                                    <td colspan="4" align="center" nowrap><input class="submitbutton" type="submit" value="SEARCH" ></td>
		                                </tr>

			                        </form>
	                            </table>		                    
	                        </td>
		                </tr>
		            </table>
		            <br>
				</ffi:cinclude>
				<ffi:cinclude value1="${DisplayCount}" value2="${Locations.Size}" operator="equals">
			
					<table width="750" border="0" cellspacing="0" cellpadding="0">
					     <tr>
					          <td align="center" ><span class="sectionsubhead"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivedit.jsp-1" parm0="${DisplayCount}" parm1="${DisplayCount}"/></span></td>
					     </tr>
					</table>
					<br>
				</ffi:cinclude>
				<ffi:cinclude value1="${MultipleCategories.Size}" value2="0" operator="equals">
				<div id="locationViewTableID">
					<div class="blockHead" align="left"><s:text name="jsp.user_190" /></div>
					<div class="paneWrapper">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tr>
										<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.user_190" /></td>
										<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.user_189" /></td>
										<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.home.column.label.action" /></td>
									</tr>
								</table>
							</div>
							<div class="paneContentWrapper">
								<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tableData">
									<%	int color = 0;	%>
									<ffi:setProperty name="EmptyLocations" value="true"/>
									<ffi:list collection="Locations" items="Location">
										<ffi:setProperty name="EmptyLocations" value="false"/>
										<tr <%= color % 2 == 0 ? "class=\"ltrow3\"" : "class=\"dkrow\"" %> >
											<td class="columndata" nowrap><ffi:getProperty name="Location" property="LocationName"/></td>
											<td class="columndata" nowrap><ffi:getProperty name="Location" property="LocationID"/></td>
											<td class="columndata" align="right">
												<%--
												<ffi:link url="${SecurePath}user/corpadminlocview.jsp?editlocation-reload=true&EditLocation_LocationBPWID=${Location.LocationBPWID}">
													<img src="/cb/cb/multilang/grafx/user/i_view.gif" width="19" height="14" hspace="3" border="0">
												</ffi:link>
												--%>
												
												<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>	
												
													<s:url id="locationViewURL" value="%{#session.PagesPath}user/corpadminlocview.jsp" escapeAmp="false">
															<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
															<s:param name="editlocationreload" value="%{'true'}"/>
															<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
													</s:url>
													<sj:a
														href="%{locationViewURL}" 
														targets="viewLocationDialogID" 
														button="true" 
														title="View"
														onClickTopics=""
														onCompleteTopics="openViewLocationDialog"
													>
															<s:text name="jsp.default_6" />
													</sj:a>	
												
											</td>
											<td class="columndata" align="right">
												&nbsp;
											</td>
										 </tr>
										<%	color++;	%>
									</ffi:list>
			
									<ffi:cinclude value1="${EmptyLocations}" value2="true" operator="equals">
										<tr class="ltrow3">
											<td colspan="4" class="columndata">
												<s:text name="admin.no.location.available" />
											</td>
										</tr>
										<%	color++;	%>
									</ffi:cinclude>
								</table>
							</div>
							</div>
						</div>
				
					
			</div>
			</ffi:cinclude>
			<ffi:cinclude value1="${MultipleCategories.Size}" value2="0" operator="notEquals">
			<div id="locationViewTableID" style="width:750px;">
				<div class="blockHead" align="left"><s:text name="jsp.user_190" /></div>
				<div class="paneWrapper">
				   	<div class="paneInnerWrapper">
						<div class="header">
							<table cellspacing="0" cellpadding="0" border="0" width="750">
								<tr>
									<td class="sectionsubhead adminBackground" width="7"><s:text name="jsp.user_190" /></td>
									<td class="sectionsubhead adminBackground" width="25%"><s:text name="jsp.user_189" /></td>
									<td class="sectionsubhead adminBackground" width="25%"><s:text name="jsp.home.column.label.action" /></td>
									<td width="25%">&nbsp;</td>
								</tr>
							</table>
						</div>
						<div class="paneContentWrapper">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tableData">
								<%	int locColor = 0;	%>
				                <ffi:setProperty name="EmptyLocations" value="true"/>
								<ffi:list collection="DALocations" items="Location">
			                        <ffi:setProperty name="EmptyLocations" value="false"/>
			                        <tr <%= locColor % 2 == 0 ? "class=\"ltrow3\"" : "class=\"dkrow\"" %> >
			                        	<ffi:cinclude value1="${Location.UserAction}" value2="" operator="notEquals">
				                            <td width="25%" class="columndataDA" nowrap><ffi:getProperty name="Location" property="LocationName"/></td>
				                            <td width="25%" class="columndataDA" nowrap><ffi:getProperty name="Location" property="LocationID"/></td>
				                            <td width="25%" class="columndataDA" nowrap><ffi:getProperty name="Location" property="UserAction"/></td>
			                            </ffi:cinclude>
			                            <ffi:cinclude value1="${Location.UserAction}" value2="" operator="equals">
				                            <td width="25%" class="columndata" nowrap><ffi:getProperty name="Location" property="LocationName"/></td>
				                            <td width="25%" class="columndata" nowrap><ffi:getProperty name="Location" property="LocationID"/></td>
				                            <td width="25%" class="columndata" nowrap><ffi:getProperty name="Location" property="UserAction"/></td>
			                            </ffi:cinclude>
			                            <td width="25%" class="columndata" align="right">
				                            	<ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED%>" operator="notEquals">
												<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>	
													<s:url id="locationViewURL" value="%{#session.PagesPath}user/corpadminlocview.jsp" escapeAmp="false">
															<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
															<s:param name="editlocationreload" value="%{'true'}"/>
															<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
													</s:url>
													<sj:a
														href="%{locationViewURL}" 
														targets="viewLocationDialogID" 
														button="true" 
														title="View"
														onClickTopics=""
														onCompleteTopics="openViewLocationDialog"
													>
															<s:text name="jsp.default_6" />
													</sj:a>	
													
				                                </ffi:cinclude>
				                                <ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED%>">
													<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>	
													<s:url id="locationViewURL" value="%{#session.PagesPath}user/corpadminlocview.jsp" escapeAmp="false">
															<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
															<s:param name="editlocationreload" value="%{'true'}"/>
															<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
															<s:param name="userAction" value="%{'Added'}"/>
													</s:url>
													<sj:a
														href="%{locationViewURL}" 
														targets="viewLocationDialogID" 
														button="true" 
														title="View"
														onClickTopics=""
														onCompleteTopics="openViewLocationDialog"
													>
														<s:text name="jsp.default_6" />
													</sj:a>	
				                                </ffi:cinclude>
			                            </td>
			                            <td class="columndata" align="right">
			                            	&nbsp;
									    </td>
			                         </tr>
			                        <%	locColor++;	%>
								</ffi:list>
				                <ffi:cinclude value1="${EmptyLocations}" value2="true" operator="equals">
				                    <tr class="ltrow3">
				                        <td colspan="5" class="columndata">
				                            <!--L10NStart-->No Locations Available<!--L10NEnd-->
				                        </td>
				                    </tr>
			                        <%	locColor++;	%>
				                </ffi:cinclude>	
							</table>
						</div>
						</div>
					</div>
				
				
			</div>
			</ffi:cinclude>
			</ffi:cinclude>
			<div>
				<table width="750" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="40">&nbsp;</td>
					</tr>
					<tr>
						<td align="center">
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a button="true" title="Close" onClickTopics="closeDialog,cancelDivisionForm"><s:text name="jsp.default_175" /></sj:a>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
		</div>
		
<script>
	/* $(document).ready(function(){
		$('#locationViewTableID').pane({
			title: 'Location'			
		});	
	}) */
</script>

<ffi:setProperty name="onCancelGoto" value="corpadmindivview.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmindivview.jsp"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmindivview.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="NewAdminUserStringList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="AdministratorStr"/>
