<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:object id="DiscardPendingChangeTask" name="com.ffusion.tasks.dualapproval.DiscardPendingChange"  />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemType" value="${itemType}" />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemId" value="${itemId}" />
	<ffi:setProperty name="DiscardPendingChangeTask" property="successURL" value="${successUrl}" />
<ffi:process name="DiscardPendingChangeTask"/>
<ffi:removeProperty name="DiscardPendingChangeTask"/>
