<%@page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<s:include value="inc/corpadminusers-pre.jsp"/>

<%-- The following div tempDivIDForMethodNsAdminReloadEmployees is used to hold temparory variables of user grids.--%>
<div id="tempDivIDForMethodNsAdminReloadEmployees" style="display: none;"></div>

<%-- Pending Approval Company island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
  <ffi:setProperty name="Section" value="Profiles" />
  <div id="admin_pendingProfilesTab" class="portlet" style="display:none;">
	  <div class="portlet-header">
	  		<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
	    </div>
	     
	   <div id="grid1Id" class="portlet-content">
	   </div>
		
		<div id="adminProfilesSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_pendingProfilesTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
  </div>
  <div class="clearBoth" id="profileBlankDiv" style="padding: 10px 0"></div>
</ffi:cinclude>
<%-- Pending Approval Company island end --%>

 <div id="profilesSummaryTabs" class="portlet">
   <div class="portlet-header">
		<span class="portlet-title">
				<s:text name="jsp.user_269"/>
		</span>
    </div>
    
		<div id="grid2Id" class="portlet-content">
	   </div>
	
	<div id="adminProfilesSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('profilesSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
 </div>
<script type="text/javascript">	
	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("profilesSummaryTabs");
	ns.common.initializePortlet("admin_pendingProfilesTab");
	 var urlString = "/cb/pages/jsp/user/inc/da-business-user-profile-island-grid.jsp";
	 var urlString2 = "/cb/pages/jsp/user/businessuserprofiles.jsp";
     $.ajax({
		url: urlString,	
		type: "post",
		success: function(data){
			$('#grid1Id').html(data);
			$.ajax({
				url: urlString2,	
				type: "post",
				success: function(data){
					$('#grid2Id').html(data);
		       }
		    });
       }
    });
</script>
