<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<div id="Auto_Entitlement">
<div id="editautoentitleDiv" class="editautoentitleHelpCSS" style="width:100%; float:left;">
<ffi:help id="user_editautoentitle" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Edit Auto-Entitlement Options'/>

<%
	String checked = "";
	session.setAttribute("FFIModifyAutoEntitleSettings",session.getAttribute("ModifyAutoEntitleSettings"));
%>

<%
    boolean disableACH = false;
    boolean disableWires = false;
    boolean disableCashCon = false;
    int viewCashConCount = 0;
%>
	<%-- get the restrictive entitlements for EFS level --%>
	<ffi:object id="RestrictiveEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session" />

    <s:if test="#session.BusinessEmployee.UsingEntProfiles">
    	<s:if test="%{#session.Section == 'Profiles'}">
    		<ffi:object id="RestrictiveEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedAutoEntitlements" scope="session" />
    		<ffi:setProperty name="RestrictiveEntitlements" property="UsingEntProfiles" value="true"/>
    		<ffi:setProperty name="RestrictiveEntitlements" property="ChannelId" value="${ChannelId}"/>
    	</s:if>
    	<s:if test="%{#session.Section == 'UserProfile'}">
    		<ffi:object id="RestrictiveEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedAutoEntitlements" scope="session" />
    		<ffi:setProperty name="RestrictiveEntitlements" property="UsingEntProfiles" value="true"/>
    		<ffi:setProperty name="RestrictiveEntitlements" property="ChannelId" value="${ChannelId}"/>
    	</s:if>
    </s:if>

    <ffi:setProperty name="RestrictiveEntitlements" property="GroupId" value="1"/>
    <ffi:process name="RestrictiveEntitlements"/>

    <ffi:list collection="Entitlement_Restricted_list" items="items">
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.WIRES%>">
            <% disableWires = true; %>
        </ffi:cinclude>
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.ACH_BATCH%>">
            <% disableACH = true; %>
        </ffi:cinclude>
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY%>">
            <% viewCashConCount++; %>
        </ffi:cinclude>
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST%>">
            <% viewCashConCount++; %>
        </ffi:cinclude>
    </ffi:list>
    <% if (viewCashConCount == 2) disableCashCon = true; %>


<%-- if we didn't come from the back button, re-init the ModifyAutoEntitleSettings to be the original values --%>
<s:if test="%{#parameters.InitAE == null || #parameters.InitAE[0] != 'skip'}">
	<ffi:setProperty name="ModifyAutoEntitleSettings" property="InitFlag" value="true"/>
	<ffi:process name="ModifyAutoEntitleSettings"/>
</s:if>
<% com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings ModifyAutoEntitleSettings = (com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings)session.getAttribute( "ModifyAutoEntitleSettings" );%>
<ffi:removeProperty name="useLastRequest"/>
<% String useLastRequest; %>
<ffi:getProperty name="LastRequest" assignTo="useLastRequest"/>
<ffi:setProperty name="ResetURL" value="/cb/pages/jsp/user/editautoentitle.jsp?reset=true&Section=${Section}"/>
<ffi:cinclude value1="${reset}" value2="true" operator="equals">
<ffi:setProperty name="reset" value=""/>
<% ModifyAutoEntitleSettings = (com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings)session.getAttribute( "ModifyAutoEntitleSettings" );
ModifyAutoEntitleSettings.reset();
%>

</ffi:cinclude>

<%-- ###################################################################### --%>
<%-- Make the page read-only if the user is not entitled to edit business   --%>
<%-- profiles.																--%>
<%-- ###################################################################### --%>

<% String disableEdit = ""; %>
<%--
<ffi:cinclude ifNotEntitled="BusinessProfileEdit">
	<% disableEdit = "disabled"; %>
</ffi:cinclude>
--%>

<SCRIPT language=JavaScript><!--

	$(document).ready(function() {
		$("#editautoResetButton").button({
			icons: {
			}			
		});
	});
	
	ns.admin.resetEditAutoEntitleForm = function ()
	{
			$("#EditAutoEntitleForm")[0].reset();
	}
//-->
</SCRIPT>
<%-- <span><s:text name="jsp.user_49"/></span> --%>
<div align="center" class="marginTop10 marginleft5">
<div><span class="sectionhead"><ffi:getProperty name="Context"/></span></div>
    <s:form id="EditAutoEntitleForm" name="EditAutoEntitleForm" namespace="/pages/user" action="modifyAutoEntitleSettings-verify" validate="false" theme="simple" method="post">
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData marginTop10 marginBottom10 marginleft5" align="center">
						<ffi:setProperty name="displayAutoEntitlement" value="true"/>
				    	<ffi:cinclude value1="${Business.UsingEntProfiles}" value2="true">
				    		<ffi:cinclude value1="${UseLastRequest}" value2="true">
				    			<% checked = (ModifyAutoEntitleSettings.getEnableProfiles().equals( "true" ) ) ? "checked" : ""; %>
				    		</ffi:cinclude>
				    		<ffi:cinclude value1="${UseLastRequest}" value2="true" operator="notEquals">
					    		<% if( ModifyAutoEntitleSettings.getEnableAccounts().equals( "true" ) || 
					    				ModifyAutoEntitleSettings.getEnableAccountGroups().equals( "true" ) || 
					    				ModifyAutoEntitleSettings.getEnableACHCompanies().equals( "true" ) || 
					    				ModifyAutoEntitleSettings.getEnablePermissions().equals( "true" ) ||
					    				ModifyAutoEntitleSettings.getEnableLocations().equals( "true" ) || 
					    				ModifyAutoEntitleSettings.getEnableWireTemplates().equals( "true" )  ) {
							       checked = (ModifyAutoEntitleSettings.getEnableProfiles().equals( "true" ) ) ? "checked" : "";
					    		}else {
					    			checked = "";
					    		}
							    %>
							</ffi:cinclude>
						    <tr>
						    	<ffi:setProperty name="displayAutoEntitlement" value="false"/>
								<td class="sectionsubhead" align="center"><div><input type="checkbox" <%= checked %> name="ModifyAutoEntitleSettings.EnableProfiles" value="true" border="0">&nbsp;
								Auto Entitlement</div></td>
						    </tr>
						</ffi:cinclude>
					    <ffi:cinclude value1="${displayAutoEntitlement}" value2="true">
							<% 
								if(ModifyAutoEntitleSettings.getParentEnableAccounts().equals( "true" ) ) {
								checked = (ModifyAutoEntitleSettings.getEnableAccounts().equals( "true" ) ) ? "checked" : "";
							%>
							<tr>
								<td width="16%"class="sectionsubhead"><input type="checkbox" <%= disableEdit %> <%= checked %> name="ModifyAutoEntitleSettings.EnableAccounts" value="true" border="0">&nbsp;<s:text name="jsp.user_13"/></td>
							<% } %>
							<%
								if(ModifyAutoEntitleSettings.getParentEnableAccountGroups().equals( "true" ) ) {
								checked = (ModifyAutoEntitleSettings.getEnableAccountGroups().equals( "true" ) ) ? "checked" : ""; %>
								<td width="16%"class="sectionsubhead"><input type="checkbox" <%= disableEdit %> <%= checked %> name="ModifyAutoEntitleSettings.EnableAccountGroups" value="true"  border="0">&nbsp;<s:text name="jsp.user_7"/></td>
							<% } %>
							<%
								if(ModifyAutoEntitleSettings.getParentEnableACHCompanies().equals( "true" ) && !disableACH) {
								checked = (ModifyAutoEntitleSettings.getEnableACHCompanies().equals( "true" ) ) ? "checked" : ""; %>
								<td width="16%"class="sectionsubhead"><input type="checkbox" <%= disableEdit %> <%= checked %> name="ModifyAutoEntitleSettings.EnableACHCompanies" value="true"  border="0">&nbsp;<s:text name="jsp.user_14"/></td>
							<% } %>
							<% if(ModifyAutoEntitleSettings.getParentEnableLocations().equals( "true" ) && !disableCashCon) {
								checked = (ModifyAutoEntitleSettings.getEnableLocations().equals( "true" ) ) ? "checked" : ""; %>
								<td width="16%"class="sectionsubhead"><input type="checkbox" <%= disableEdit %> <%= checked %> name="ModifyAutoEntitleSettings.EnableLocations" value="true"  border="0">&nbsp;<s:text name="jsp.default_563"/></td>
							<% } %>
								<% if(ModifyAutoEntitleSettings.getParentEnablePermissions().equals( "true" ) ) {
									checked = (ModifyAutoEntitleSettings.getEnablePermissions().equals( "true" ) ) ? "checked" : ""; %>
								<td width="16%"class="sectionsubhead"><input type="checkbox" <%= disableEdit %> <%= checked %> name="ModifyAutoEntitleSettings.EnablePermissions" value="true"  border="0">&nbsp;<s:text name="jsp.default_569"/></td>
							<% } %>
							<% if(ModifyAutoEntitleSettings.getParentEnableWireTemplates().equals( "true" ) && !disableWires) {
								checked = (ModifyAutoEntitleSettings. getEnableWireTemplates().equals( "true" ) ) ? "checked" : ""; %>
								<td width="16%"class="sectionsubhead"><input type="checkbox" <%= disableEdit %> <%= checked %> name="ModifyAutoEntitleSettings.EnableWireTemplates" value="true"  border="0">&nbsp;<s:text name="jsp.default_580"/></td>
							<% } %>
							</tr>
						</ffi:cinclude>
						<%-- <tr>
							<td height="20" >
								<div align="center" style="padding-top: 6px; padding-bottom: 16px;">
						
									<a  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
										title='<s:text name="jsp.default_358.1" />' 
										href='#' 
										onClick="ns.admin.resetEditAutoEntitleForm();">
										<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358"/></span>
									</a>
									
									<a id="editautoResetButton" 
										title='<s:text name="jsp.default_358.1" />' 
										href='#' 
										onClick="ns.admin.resetEditAutoEntitleForm();">
										<s:text name="jsp.default_358"/>
									</a>
								
									<sj:a
										button="true" 
										onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
										><s:text name="jsp.default_102"/></sj:a>
									<sj:a
										formIds="EditAutoEntitleForm"
										targets="Auto_Entitlement"
										button="true" 
										validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeVerify"
										onErrorTopics="errorVerify"
										onSuccessTopics="successVerify"
										onCompleteTopics="onCompleteTabifyAndFocusTopic"
										><s:text name="jsp.default_366"/></sj:a>
									<sj:a
										id="permAutoEntitleNext"
										button="true" 
										onClickTopics="nextPermissionTopic"
										><s:text name="jsp.user_203"/></sj:a>

								</div>
							</td>
						</tr> --%>
					</table>
<div class="btn-row">
<a id="editautoResetButton" 
	title='<s:text name="jsp.default_358.1" />' 
href='#' 
onClick="ns.admin.resetEditAutoEntitleForm();">
<s:text name="jsp.default_358"/>
</a>

<sj:a href="#"
	button="true" 
	summaryDivId="summary" buttonType="cancel"
	onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton,showSummary"
	><s:text name="jsp.default_102"/></sj:a>
<sj:a
	href="#"
	formIds="EditAutoEntitleForm"
	targets="Auto_Entitlement"
	button="true" 
	validate="true"
	validateFunction="customValidation"
	onBeforeTopics="beforeVerify"
	onErrorTopics="errorVerify"
	onSuccessTopics="successVerify"
	onCompleteTopics="onCompleteTabifyAndFocusTopic"
	><s:text name="jsp.default_366"/></sj:a>
<sj:a
	href="#"
	id="permAutoEntitleNext"
	button="true" 
	onClickTopics="nextPermissionTopic"
	><s:text name="jsp.user_203"/></sj:a>
</div>
	</s:form>
</div>
</div>
</div>
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="UseLastRequest"/>