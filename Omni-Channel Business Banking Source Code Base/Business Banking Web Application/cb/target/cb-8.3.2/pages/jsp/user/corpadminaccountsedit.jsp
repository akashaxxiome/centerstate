<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<% session.setAttribute("runSearch", request.getParameter("runSearch")); %>

<s:include value="inc/corpadminaccountsedit-pre.jsp?runSearch=%{#session.runSearch}" />

<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<%
		String nickName = request.getParameter("SearchAcctsByNameNumType.NickName");
		String number = request.getParameter("SearchAcctsByNameNumType.Number");
		String type = request.getParameter("SearchAcctsByNameNumType.Type");
		String id = request.getParameter("SearchAcctsByNameNumType.Id");
	%>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="NickName" value="<%=nickName%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Number" value="<%=number%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Type" value="<%=type%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=id%>"/>
	<ffi:process name="SearchAcctsByNameNumType"/>
</ffi:cinclude>

<SCRIPT language=JavaScript type="text/javascript"><!--


//--></SCRIPT>

<div id="accountConfigDiv" align="center">
<%-- 
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<div align="center">
					<span class="sectionhead">
						<ffi:getProperty name="Business" property="BusinessName"/><br>
					</span>
				</div>
			</td>
		</tr>
	</table> --%>

	<s:include value="%{#session.PagesPath}user/inc/accountsearch.jsp" />
	
	<div id="companyAccountConfigGrid">
		<ffi:cinclude value1="${runSearch}" value2="true">
			<s:include value="corpadminaccountsconfig_grid.jsp" />
		</ffi:cinclude>
	</div>

		
</div>

