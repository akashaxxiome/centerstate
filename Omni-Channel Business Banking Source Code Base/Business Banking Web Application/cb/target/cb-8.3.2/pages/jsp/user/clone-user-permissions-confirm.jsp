<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:help id="user_clone-user-permissions-confirm" className="moduleHelpClass"/>
<div align="center">
	<TABLE class="adminBackground" width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
					<TR>
						<TD>&nbsp;</TD>
					</TR>
					<TR>
						<TD class="columndata" align="center"><s:text name="jsp.user.cloneUserPermissionConfirm">
						<s:param><%= session.getAttribute("Context")%></s:param>
						</s:text></TD>
					</TR>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
						<ffi:cinclude value1="${DAAdminBusiness}" value2="true" operator="equals">
							<tr>
								<TD class="columndata" align="center"><ffi:getProperty name="Context"/> <s:text name="jsp.user_167"/></TD>
							</tr>
							<ffi:setProperty name="AdminUpdated" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${DAAdminDivision}" value2="true" operator="equals">
							<tr>
								<TD class="columndata" align="center"><ffi:getProperty name="Context"/> <s:text name="jsp.user_166"/></TD>
							</tr>
							<ffi:setProperty name="AdminUpdated" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${DAAdminGroup}" value2="true" operator="equals">
							<tr>
								<TD class="columndata" align="center"><ffi:getProperty name="Context"/> <s:text name="jsp.user_168"/></TD>
							</tr>
							<ffi:setProperty name="AdminUpdated" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${AdminUpdated}" value2="true">
							<TD class="columndata" align="center"><s:text name="jsp.user_37"/></TD>
						</ffi:cinclude>
					</ffi:cinclude>

					<TR>
						<TD>&nbsp;</TD>
					</TR>
					<TR>
						<TD align="center">
							<sj:a button="true" 
							summaryDivId="summary" 
							buttonType="done"
							onClickTopics="showSummary,cancelAddUserCloneForm"><s:text name="jsp.default_175"/></sj:a>
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
	<p></p>
</div>

<ffi:removeProperty name="DAAdminBusiness"/>
<ffi:removeProperty name="DAAdminDivision"/>
<ffi:removeProperty name="DAAdminGroup"/>
<ffi:removeProperty name="AdminUpdated"/>
<ffi:removeProperty name="AddUser"/>

