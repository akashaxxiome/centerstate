<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setL10NProperty name='PageHeading' value='Edit Administrator(s)'/>
<ffi:setProperty name='PageText' value=''/>

<%--save the information from the corpadminadminedit.jsp page--%>

<%@ page import="com.ffusion.csil.beans.entitlements.*" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="CanAdministerAnyGroup" />

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<%
// Retrieve the selected admins and build an EntitlementGroupMember for each.
// the value of the adminListItems is: MemberType-MemberSubType-MemberID-EntGroupID
java.util.ArrayList adminIds = new java.util.ArrayList();
java.util.ArrayList userIds = new java.util.ArrayList();
java.util.ArrayList groupIds = new java.util.ArrayList();
java.util.ArrayList bizEmpIds = new java.util.ArrayList();
java.util.ArrayList adminMembers = new java.util.ArrayList();
String[] userListData = request.getParameterValues( "userList" );
String[] groupListData = request.getParameterValues( "groupList" );
String[] adminListData = request.getParameterValues( "adminListName" );
EntitlementGroupMembers userMembers = new EntitlementGroupMembers();

try {
%>

<ffi:cinclude value1="${onDoneGoto}" value2="corpadmininfo.jsp" operator="notEquals">
<%
SecureUser thisUser = (SecureUser)session.getAttribute( "SecureUser" );
java.util.Locale tempLocale = thisUser.getLocale();
if( adminListData != null ) {
	BusinessEmployees userAdmins = new BusinessEmployees( tempLocale );
	java.util.ArrayList groupAdmins = new java.util.ArrayList();
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < adminListData.length ; i++ ) {
		// create a decision object
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= adminListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		String tempId = null;
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );

		if( firstIndex == 0 ){
			// the element is a group or a division
			adminMembers.add( itemData.substring( lastIndex + 1 ) );
			tempId = itemData.substring( lastIndex + 1 );

			session.setAttribute( "tempEntId", tempId );
%>
			<%-- get the BusinessEmployees for a specific EntitlementGroup --%>
			<ffi:object id="GetEmployeesInEntGroup" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId"/>
			<ffi:setProperty name="GetEmployeesInEntGroup" property="BusinessEmployeesSessionName" value="tempBizEmployees"/>
			<ffi:setProperty name="GetEmployeesInEntGroup" property="EntitlementGroupId" value="<%= tempId %>"/>
			<ffi:process name="GetEmployeesInEntGroup"/>
<%
			BusinessEmployees tempEmployees = (BusinessEmployees)session.getAttribute( "tempBizEmployees" );
			//check that the employees in the EntitlementGroup is not already in bizEmpIds
			for( int j=0; j<tempEmployees.size(); j++ ) {
				BusinessEmployee emp = (BusinessEmployee)tempEmployees.get(j);
				boolean foundId = false;
				for( int w=0; w<bizEmpIds.size(); w++ ) {
					if( ( (String)bizEmpIds.get(w) ).equals( emp.getId() ) ) {
						foundId = true;
						break;
					}
				}
				if( !foundId ) {
					bizEmpIds.add( emp.getId() );
				}
			}
			//add information about this group to groupAdmins, in case of later retreival
			EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
			java.util.ArrayList Entry = new java.util.ArrayList();
			Entry.add( group.getGroupName() );
		 	Entry.add( group.getEntGroupType() );
		 	Entry.add( Integer.toString( group.getGroupId() ) );
	         	groupAdmins.add( Entry );

			adminIds.add( itemData.substring( lastIndex + 1 ) );
		} else {
			// the element is a user
			tempId =  itemData.substring( secondIndex + 1, lastIndex );
			EntitlementGroupMember member = new EntitlementGroupMember();
			member.setMemberType( itemData.substring( 0, firstIndex ) );
			member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
			member.setId( tempId );
			member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
			adminMembers.add( member );
			adminIds.add( tempId );

			//check that this employee is not already in the bizEmpIds ArrayList
			boolean foundId = false;
			for( int j=0; j<bizEmpIds.size(); j++ ) {
			if( ( (String)bizEmpIds.get(j) ).equals( tempId ) ) {
					foundId = true;
					break;
				}
			}
			if( !foundId ) {
				bizEmpIds.add( tempId );
			}
%>
			<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
			<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
			<ffi:process name="GetBusinessEmployee"/>
			<ffi:removeProperty name="GetBusinessEmployee" />
<%
			BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
			userAdmins.add( emp );
		}
	}
	request.setAttribute( "AdminEmployees", userAdmins );
	request.setAttribute( "AdminGroups", groupAdmins );
}
session.setAttribute( "BizEmployeeIds", bizEmpIds );
BusinessEmployees tempAdminEmps = new BusinessEmployees( tempLocale );
%>
<ffi:removeProperty name="tempEntId"/>
<ffi:removeProperty name="tempBizEmployees"/>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:list collection="BizEmployeeIds" items="Id">
	<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${Id}"/>
	<ffi:process name="GetBusinessEmployee"/>
<%
	BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
	tempAdminEmps.add( emp );
%>
</ffi:list>
<%-- set BusinessEmployee object back to secured user --%>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />
<%

session.setAttribute( "tempAdminEmps", tempAdminEmps);

if( userListData != null ) {
	BusinessEmployees userNonAdmins = new BusinessEmployees( tempLocale );
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < userListData.length ; i++ ) {
		// create a decision object
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= userListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( secondIndex + 1, lastIndex );
%>
		<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
		<ffi:process name="GetBusinessEmployee"/>
		<ffi:removeProperty name="GetBusinessEmployee" />
<%
		BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
		userNonAdmins.add( emp );

		EntitlementGroupMember member = new EntitlementGroupMember();
		member.setMemberType( itemData.substring( 0, firstIndex ) );
		member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
		member.setId( tempId );
		member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
		userMembers.add( member );
		userIds.add( tempId );
	}
	request.setAttribute( "NonAdminEmployees", userNonAdmins );
}

if( groupListData != null ) {
	java.util.ArrayList groupNonAdmins = new java.util.ArrayList();
	// create the groupId array from the posted data
	for( int i = 0 ; i < groupListData.length ; i++ ) {
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= groupListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( lastIndex + 1 );
		//add information about this group to groupAdmins, in case of later retreival
		EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
		java.util.ArrayList Entry = new java.util.ArrayList();
		Entry.add( group.getGroupName() );
	 	Entry.add( group.getEntGroupType() );
	 	Entry.add( Integer.toString( group.getGroupId() ) );
         	groupNonAdmins.add( Entry );
		groupIds.add( tempId );
	}
	request.setAttribute( "NonAdminGroups", groupNonAdmins );
}
%>
</ffi:cinclude>
<ffi:cinclude value1="${onDoneGoto}" value2="corpadmininfo.jsp" operator="equals">
<%
SecureUser thisUser = (SecureUser)session.getAttribute( "SecureUser" );
java.util.Locale tempLocale = thisUser.getLocale();
if( adminListData != null ) {
	BusinessEmployees userAdmins = new BusinessEmployees( tempLocale );
	java.util.ArrayList groupAdmins = new java.util.ArrayList();
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < adminListData.length ; i++ ) {
		// create a decision object
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= adminListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		String tempId = null;
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );

		if( firstIndex == 0 ) {
			// the element is a group or a division
			adminMembers.add( itemData.substring( lastIndex + 1 ) );
			tempId = itemData.substring( lastIndex + 1 );


			//add information about this group to groupAdmins, in case of later retreival
			EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
			java.util.ArrayList Entry = new java.util.ArrayList();
			Entry.add( group.getGroupName() );
		 	Entry.add( group.getEntGroupType() );
		 	Entry.add( Integer.toString( group.getGroupId() ) );
	         	groupAdmins.add( Entry );

			adminIds.add( itemData.substring( lastIndex + 1 ) );
		} else {
			// the element is a user
			tempId =  itemData.substring( secondIndex + 1, lastIndex );
			EntitlementGroupMember member = new EntitlementGroupMember();
			member.setMemberType( itemData.substring( 0, firstIndex ) );
			member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
			member.setId( itemData.substring( secondIndex + 1, lastIndex ) );
			adminIds.add( member.getId() );
			member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
			adminMembers.add( member );
%>
			<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
			<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
			<ffi:process name="GetBusinessEmployee"/>
			<ffi:removeProperty name="GetBusinessEmployee" />
<%
			BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
			userAdmins.add( emp );

		}
	}

	request.setAttribute( "AdminEmployees", userAdmins );
	request.setAttribute( "AdminGroups", groupAdmins );
}

if( userListData != null ) {
	BusinessEmployees userNonAdmins = new BusinessEmployees( tempLocale );
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < userListData.length ; i++ ) {
		// create a decision object
		EntitlementGroupMember member = new EntitlementGroupMember();
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= userListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( secondIndex + 1, lastIndex );
%>
		<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
		<ffi:process name="GetBusinessEmployee"/>
		<ffi:removeProperty name="GetBusinessEmployee" />
<%
		BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
		userNonAdmins.add( emp );

		member.setMemberType( itemData.substring( 0, firstIndex ) );
		member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
		member.setId( itemData.substring( secondIndex + 1, lastIndex ) );
		userIds.add( member.getId() );
		member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
		userMembers.add( member );
	}
	request.setAttribute( "NonAdminEmployees", userNonAdmins );

}

if( groupListData != null ) {
	java.util.ArrayList groupNonAdmins = new java.util.ArrayList();
	// create the groupId array from the posted data
	for( int i = 0 ; i < groupListData.length ; i++ ) {
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= groupListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( lastIndex + 1 );
		//add information about this group to groupAdmins, in case of later retreival
		EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
		java.util.ArrayList Entry = new java.util.ArrayList();
		Entry.add( group.getGroupName() );
	 	Entry.add( group.getEntGroupType() );
	 	Entry.add( Integer.toString( group.getGroupId() ) );
         	groupNonAdmins.add( Entry );
		groupIds.add( itemData.substring( lastIndex + 1 ) );
	}
	request.setAttribute( "NonAdminGroups", groupNonAdmins );
}
%>
</ffi:cinclude>
<%
// put the data into the session
session.setAttribute( "adminMembersTemp", adminMembers );
session.setAttribute( "userMembersTemp", userMembers );
session.setAttribute( "adminIds", adminIds );
session.setAttribute( "userIds", userIds );
session.setAttribute( "groupIds", groupIds );

}
catch ( Exception e ) {
	e.printStackTrace();
}

%>

<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>

<%-- call the SetAdministrators task only if we are editing business or business groups --%>
<%-- remove adminMemebers only if we do not need them for adding new business groups --%>
<ffi:cinclude value1="${onDoneGoto}" value2="corpadmininfo.jsp" operator="equals">
	<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembersTemp"/>
	<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembersTemp"/>
	<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
	<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
	<%--
	<ffi:setProperty name="BackReload" value="TRUE"/>
	--%>
	<ffi:process name="SetAdministrators"/>
	<%-- only set the adminMembers in the session now that SetAdministrators completed successfully. Otherwise, original list will still be valid --%>
	<%
		session.setAttribute( "adminMembers", session.getAttribute("adminMembersTemp"));
		session.setAttribute( "userMembers", session.getAttribute("userMembersTemp"));
	%>
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="equals">
		<s:set var="autoEntitleConfirmMsg15" value="%{getText('jsp.user.autoEntitleConfirmMsg15')}" scope="request" />
		<ffi:setL10NProperty name="autoEntitleConfirmMsg" value="${autoEntitleConfirmMsg15}"/>
		<s:set var="autoEntitleConfirmMsg16" value="%{getText('jsp.user.autoEntitleConfirmMsg16')}" scope="request" />
		<ffi:setL10NProperty name="autoEntitleConfirmMsg2" value="${autoEntitleConfirmMsg16}"/>
		<ffi:setProperty name="autoEntitleBackURL" value="/cb${SecurePath}user/corpadminadminedit.jsp?BackReload=TRUE" URLEncrypt="TRUE" />
		<ffi:setProperty name="autoEntitleCancel" value="/cb${SecurePath}user/${onCancelGoto}"/>

		<ffi:setProperty name="autoEntitleFormAction" value="/cb${SecurePath}user/corpadminadminedit-done.jsp"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="false" operator="equals">
		<ffi:setProperty name="NextURL" value="/cb/pages/jsp/user/corpadminadminedit-done.jsp" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${onDoneGoto}" value2="corpadmininfo.jsp" operator="notEquals">
	<%-- validate the admin settings, make sure we have at least 1 user in it --%>
	<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembersTemp"/>
	<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembersTemp"/>
	<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
	<%-- if editing existing division load the entitlemnet groups to validate against, if adding new division, dont load entitlement group --%>
	<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
		<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${AdminEditType}" value2="Division" operator="equals">
		<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="<%=EntitlementsDefines.ENT_GROUP_DIVISION%>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${AdminEditType}" value2="Group" operator="equals">
		<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="<%=EntitlementsDefines.ENT_GROUP_GROUP%>"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${editDivisionTouched}" value2="true" operator="equals">
		<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:removeProperty name="editDivisionTouched"/>
	</ffi:cinclude>
	<%--
	<ffi:setProperty name="BackReload" value="TRUE"/>
	--%>
	<ffi:process name="SetAdministrators"/>
	<%-- only set the adminMembers in the session now that SetAdministrators completed successfully. Otherwise, original list will still be valid --%>
	<%
	session.setAttribute( "adminMembers", session.getAttribute("adminMembersTemp"));
	session.setAttribute( "userMembers", session.getAttribute("userMembersTemp"));
	session.setAttribute( "AdminsSelected", "TRUE");
	session.setAttribute( "GroupDivAdminEdited", "true" );
	%>
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="equals">
		<s:set var="autoEntitleConfirmMsg15" value="%{getText('jsp.user.autoEntitleConfirmMsg15')}" scope="request" />
		<ffi:setL10NProperty name="autoEntitleConfirmMsg" value="${autoEntitleConfirmMsg15}"/>
		<s:set var="autoEntitleConfirmMsg16" value="%{getText('jsp.user.autoEntitleConfirmMsg16')}" scope="request" />
		<ffi:setL10NProperty name="autoEntitleConfirmMsg2" value="${autoEntitleConfirmMsg16}"/>
		<ffi:setProperty name="autoEntitleBackURL" value="/cb${SecurePath}user/corpadminadminedit.jsp?BackReload=TRUE" URLEncrypt="TRUE" />

		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
			<ffi:setProperty name="autoEntitleCancel" value="/cb${SecurePath}user/${onCancelGoto}?CancelReload=TRUE"/>
			<script>
				ns.admin.editAdminerAutoEntitleCancelUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/user/${onCancelGoto}?CancelReload=TRUE"/>';
			</script>
		</ffi:cinclude>
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
			<ffi:setProperty name="autoEntitleCancel" value="/cb${SecurePath}user/${onCancelGoto}?CancelReload=TRUE&AdminsSelected=FALSE"/>
			<script>
				ns.admin.editAdminerAutoEntitleCancelUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/user/${onCancelGoto}?CancelReload=TRUE&AdminsSelected=FALSE"/>';
			</script>
		</ffi:cinclude>

		<ffi:setProperty name="autoEntitleFormAction" value="/cb${SecurePath}user/${onDoneGoto}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="false" operator="equals">
		<ffi:setProperty name="NextURL" value="/cb/pages/jsp/user/${onDoneGoto}" />
	</ffi:cinclude>
</ffi:cinclude>

<%  //kaijie: redirect to coradmindivadd.jsp/coradmindivedit.jsp/coradmingroupadd.jsp/coradmingrouped
	if(("TRUE").equals((String)session.getAttribute("AddDiv"))){
		request.setAttribute("action", "setAdministrators-addDiv");
	} else if(("TRUE").equals((String)session.getAttribute("EditDiv"))){
		request.setAttribute("action", "setAdministrators-editDiv");
	} else if(("TRUE").equals((String)session.getAttribute("AddGroup"))){
		request.setAttribute("action", "setAdministrators-addGroup");
	} else if(("TRUE").equals((String)session.getAttribute("EditGroup"))){
		request.setAttribute("action", "setAdministrators-editGroup");
	} else if(("TRUE").equals((String)session.getAttribute("EditAddDiv"))){
		request.setAttribute("action", "setAdministrators-editAddDiv");
	} else if(("TRUE").equals((String)session.getAttribute("EditAddGroup"))){
		request.setAttribute("action", "setAdministrators-editAddGroup");
	}  else {
		request.setAttribute("action", "setAdministrators-editCompany");
	}
%>

<%
	session.setAttribute("AdminEmployees", request.getAttribute("AdminEmployees"));
	session.setAttribute("AdminGroups", request.getAttribute("AdminGroups"));
	session.setAttribute("NonAdminEmployees", request.getAttribute("NonAdminEmployees"));
	session.setAttribute("NonAdminGroups", request.getAttribute("NonAdminGroups"));
%>

<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="equals">
  <ffi:cinclude value1="${SecureUser.UsingEntProfiles}" value2="true" operator="notEquals">
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">

			<DIV align="center">

				<TABLE class="adminBackground" border="0" cellspacing="0" cellpadding="0">
					<TR>

						<TD align="center" class="adminBackground">


					    		<s:form id="autoEntConfirmId" action="%{#request.action}" method="post" name="autoEntConfirm">
		                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<TABLE border="0" cellspacing="0" cellpadding="3">
									<TR>
										<TD colspan="2" style="text-align:center" nowrap><span class="columndata"><ffi:getProperty name="autoEntitleConfirmMsg"/></span></TD>
									</TR>

									<TR>
										<TD colspan="2" style="text-align:center" nowrap><span class="columndata"><ffi:getProperty name="autoEntitleConfirmMsg2"/></span></TD>
									</TR>

									<!-- <TR>
										<TD style="text-align:right"><input type="radio" name="AutoEntitleAdministrators" value="true" checked="checked"/></TD>
										<TD align="left" class="columndata">L10NStartYesL10NEnd</TD>
									</TR>
									<TR>
										<TD style="text-align:right"><input type="radio" name="AutoEntitleAdministrators" value="false"/></TD>
										<TD align="left" class="columndata">L10NStartNoL10NEnd</TD>
									</TR>  -->
									
									<TR>
										<TD colspan="2" align="center"> 
										<input type="radio" name="AutoEntitleAdministrators" value="true" checked="checked"/><s:text name="jsp.default_467"/>&nbsp;&nbsp;
										<input type="radio" name="AutoEntitleAdministrators" value="false"/><s:text name="jsp.default_295"/>
										</TD>
									</TR>
									
									<TR>
										<TD colspan="2" style="height:40px;">&nbsp;</TD>
									</TR>
							    	<TR>
										<TD colspan="2" align="center">
											<%-- <input class="submitbutton" type="button" value="BACK" onclick="document.location='<ffi:getProperty name="autoEntitleBackURL"/>';"> --%>
											<%-- <input class="submitbutton" type="button" value="CANCEL" onclick="document.location='<ffi:getProperty name="autoEntitleCancel"/>';"> --%>
											<%-- <input class="submitbutton" type="submit" value="SAVE"> --%>
											<script>
												ns.admin.editAdminerAutoEntitleBackUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/user/corpadminadminedit.jsp?BackReload=TRUE&EditGroupAmins=TRUE"/>';
											</script>
											<div align="center" class="ui-widget-header customDialogFooter">
											<sj:a id="backBtn2"
												button="true"
												onClickTopics="editAdminerAutoEntitleOnback"
											><s:text name="jsp.default_57"/></sj:a>

											<sj:a id="editAdminerAutoEntitleCancelButtonId"
												button="true"
												onClickTopics="editAdminerAutoEntitleOnCancel"
											><s:text name="jsp.default_82"/></sj:a>
										<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="notEquals">
											<sj:a
												id="administratorsSubmitbutton1"
												formIds="autoEntConfirmId"
												targets="inputDiv"
												button="true"
												onBeforeTopics="beforeSubmit"
												onErrorTopics="errorSubmit"
												onSuccessTopics="completeAdministratorsSubmit2"
												onCompleteTopics="closeEditAdminerAutoEntitleDialog"
											><s:text name="jsp.default_366"/>
											</sj:a>
										</ffi:cinclude>
										<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
											<sj:a
												id="administratorsSubmitbutton2"
												formIds="autoEntConfirmId"
												targets="inputDiv"
												button="true"
												onBeforeTopics="beforeSubmit"
												onErrorTopics="errorSubmit"
												onSuccessTopics="completeAdministratorsSubmit"
												onCompleteTopics="closeEditAdminerAutoEntitleDialog"
											><s:text name="jsp.default_366"/>
											</sj:a>
										</ffi:cinclude>
										</div>
										</TD>
									</TR>
								 </TABLE>
					   		 </s:form>
						</TD>

					</TR>
				</TABLE>
			</DIV>
			<script>
				ns.admin.canAutoEntitle = true;
			</script>
	  </ffi:cinclude>
	</ffi:cinclude>

	<%-- Don't ask for auto entitled administrative options, if Dual Approval is enabled. --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:setProperty name="tempURL" value="${autoEntitleFormAction}?AutoEntitleAdministrators=true" URLEncrypt="true"/>
		<%
	   		 //response.sendRedirect( (String)(session.getAttribute("tempURL")));
		%>
		<script>
			ns.admin.getActionUrl = '<ffi:getProperty name="action"/>';
			ns.admin.addAdminCompleteUrl = '/cb/pages/user/'+'<ffi:getProperty name="action"/>'+'.action';
			ns.admin.addAdminComplete(ns.admin.addAdminCompleteUrl);
			ns.admin.canAutoEntitle = false;
		</script>
	</ffi:cinclude>

</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.UsingEntProfiles}" value2="true" operator="notEquals">
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="notEquals">
		<script>
			ns.admin.getActionUrl = '<ffi:getProperty name="action"/>';
			ns.admin.addAdminCompleteUrl = '/cb/pages/user/'+'<ffi:getProperty name="action"/>'+'.action';
			ns.admin.addAdminComplete(ns.admin.addAdminCompleteUrl);
			ns.admin.canAutoEntitle = false;
		</script>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.UsingEntProfiles}" value2="true" operator="equals">
	<script>
		ns.admin.getActionUrl = '<ffi:getProperty name="action"/>';
		ns.admin.addAdminCompleteUrl = '/cb/pages/user/'+'<ffi:getProperty name="action"/>'+'.action';
		ns.admin.addAdminComplete(ns.admin.addAdminCompleteUrl);
		ns.admin.canAutoEntitle = false;
	</script>
</ffi:cinclude>
<%

	BusinessEmployee be1 = (BusinessEmployee)session.getAttribute("OrigBusinessEmployee");
	session.setAttribute( "BusinessEmployee", be1 );

%>
<ffi:removeProperty name="SetAdministrators"/>
<ffi:removeProperty name="BackReload"/>
<ffi:removeProperty name="NextURL"/>
<ffi:removeProperty name="AutoEntitleAdministrators"/>
<%-- <ffi:removeProperty name="AdministratorStr"/> --%>
<%-- kaijie remove "AddDiv and EditDiv" --%>
<ffi:removeProperty name="AddDiv" />
<ffi:removeProperty name="EditDiv" />
<ffi:removeProperty name="AddGroup" />
<ffi:removeProperty name="EditGroup" />
<ffi:removeProperty name="EditAddDiv" />

