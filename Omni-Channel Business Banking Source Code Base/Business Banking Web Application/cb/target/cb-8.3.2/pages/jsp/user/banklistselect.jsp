<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_banklistselect" className="moduleHelpClass"/>

<% String itemUrl = ""; %>
<ffi:cinclude value1="${AddLoc}" value2="TRUE" operator="equals">
	<% itemUrl = "/cb/pages/user/addLocation-initBankLookupItem.action"; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddLoc}" value2="TRUE" operator="notEquals">
	<% itemUrl = "/cb/pages/user/editLocation-initBankLookupItem.action"; %>
</ffi:cinclude>
<%
	session.setAttribute("TargetExternalAccountForm", request.getParameter("TargetExternalAccountForm"));
	session.setAttribute("BankNameFieldName", request.getParameter("BankNameFieldName"));
	session.setAttribute("RoutingNumberFieldName", request.getParameter("RoutingNumberFieldName"));
	session.setAttribute("AddingFreeFormAccount", request.getParameter("AddingFreeFormAccount"));
%>

<script type="text/javascript">
$(document).ready(function(){
        $("#user_bankListRowsID a").click(function(event){
        event.preventDefault();
        var url = $( this ).attr( 'href' );
        ns.admin.searchDestinationBankForm( url );
    });
 });

</script>

<ffi:setProperty name="FinancialInstitutions" property="SortedBy" value="InstitutionName" />

	<div style="width:100%; overflow-x:hidden;">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminBackground">
		<tr>
		    <td class="columndata" class="adminBackground">
			<s:form action="banklistselect.jsp" method="post" name="BankForm">
              	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table border="0" cellspacing="0" cellpadding="3" class="adminBackground tableAlerternateRowColor" width="100%">
			 	  <tr>
                   	<th align="left"><s:text name="jsp.default_61" /></th>
                   	<th align="left"><s:text name="jsp.default_35" /></th>
                   	<th align="left"><s:text name="jsp.default_401" /></th>
                   	<th align="left"><s:text name="jsp.default_201" /></th>
                   </tr>
				<ffi:list collection="FinancialInstitutions" items="Bank1">
			    <tr id="user_bankListRowsID">
					<ffi:setProperty name="itemUrl" value="<%=itemUrl%>"/>
					<td class="columndata" width="40%" height="40"><a class="link anchorText" href=<ffi:urlEncrypt url="${itemUrl}?InstitutionId=${Bank1.InstitutionId}"/>><ffi:getProperty name="Bank1" property="InstitutionName"/></a></td>
					<td class="columndata" height="40">
	                    			<ffi:cinclude value1="${Bank1.Street}" value2="" operator="notEquals">
	                        			<ffi:getProperty name="Bank1" property="Street"/>,&nbsp;
	                    			</ffi:cinclude>
	                    			<ffi:cinclude value1="${Bank1.Street2}" value2="" operator="notEquals">
	                        			<ffi:getProperty name="Bank1" property="Street2"/>,&nbsp;
	                    			</ffi:cinclude>
					    	<ffi:getProperty name="Bank1" property="City"/>,&nbsp;
					    	<ffi:cinclude value1="${Bank1.State}" value2="" operator="notEquals">
	                   			<ffi:getProperty name="Bank1" property="State"/>,&nbsp;
	               			</ffi:cinclude>
					    	<ffi:getProperty name="Bank1" property="Country"/>
					</td>
					<td class="columndata">
						<ffi:cinclude value1="${Bank1.SwiftBIC}" value2="" operator="equals">
	                   					N/A
	               					</ffi:cinclude>
	               					<ffi:cinclude value1="${Bank1.SwiftBIC}" value2="" operator="notEquals">
	                   					<ffi:getProperty name="Bank1" property="SwiftBIC"/>
	               					</ffi:cinclude>
						
					</td>
					<td class="columndata">
						<ffi:cinclude value1="${Bank1.AchRoutingNumber}" value2="" operator="equals">
           					N/A
       					</ffi:cinclude>
       					<ffi:cinclude value1="${Bank1.AchRoutingNumber}" value2="" operator="notEquals">
           					<ffi:getProperty name="Bank1" property="AchRoutingNumber"/>
       					</ffi:cinclude>
					</td>
			    </tr>
				</ffi:list>
			    <tr>
					<td colspan="4" class="columndata"><br>
					    <span style="text-align:center; float:left; width:100%">*<s:text name="jsp.user_172"/></span>
					</td>
			    </tr>
			     <tr>
					<td colspan="4" height="30" style="background-color:#FFFFFF;">&nbsp;</td>
			    </tr>
			    <tr>
					<td colspan="4" align="center">
						<div align="center" class="ui-widget-header customDialogFooter">
							<sj:a id="location_closeReSearchBankListID"
								button="true"
								onClickTopics="location_closeSelectBankTopics">
								<s:text name="jsp.default_102"/></sj:a>
						</div>
					</td>
			    </tr>
			</table>
		    </s:form>
		</td>
		</tr>
	    </table>
	</div>

<ffi:removeProperty name="SearchBankName"/>
<ffi:removeProperty name="SearchRoutingNum"/>
<ffi:removeProperty name="itemUrl"/>