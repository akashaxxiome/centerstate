<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_clone-account-permissions-init" className="moduleHelpClass"/>
<% 
	session.setAttribute("BusinessEmployeeIdForClone", session.getAttribute("BusinessEmployeeId"));
%>

<%-- initializing if necessary --%>
<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="%{#session.PagesPath}user/inc/admin-init.jsp" />
</ffi:cinclude>

<ffi:cinclude value1="${OneAdmin}" value2="" operator="equals">
	<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="request"/>
		<ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
	<ffi:process name="GetAdministratorsForGroups"/>

	<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
	<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="request"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
	<ffi:process name="GetBusinessEmployeesByEntGroups"/>
	<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
	<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
	<% int AdminCount = 0; %>
	<ffi:list collection="BusinessEmployees" items="admin">
	<% AdminCount++; %>
	</ffi:list>
	<% if( AdminCount == 1 ) { %>
		<ffi:setProperty name="OneAdmin" value="TRUE"/>
	<% } else { %>
		<ffi:setProperty name="OneAdmin" value="FALSE"/>
	<% }  %>
</ffi:cinclude>



<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${BusinessEmployeeIdForClone}"/>
<ffi:process name="GetBusinessEmployee"/>
<%
session.setAttribute("BusinessEmployeeForClone",session.getAttribute("BusinessEmployee"));
%>

<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="request"/>
<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${BusinessEmployeeForClone.EntitlementGroupId}"/>
<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="<%= SessionNames.CLONE_PERMISSION_ACCOUNTS %>"/>
<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
<ffi:process name="GetAccountsForGroup"/>

<ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="<%= SessionNames.CLONE_PERMISSION_ACCOUNTS %>"/>
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
<ffi:process name="SetAccountsCollectionDisplayText"/>
<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>

<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.CLONE_PERMISSION_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.CLONE_ACCT_PERMISSION_ACCOUNTS_INDEX %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
									
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.CLONE_PERMISSION_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.CLONE_ACCT_PERMISSION_ACCOUNTS_INDEX_BY_TYPE %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.CLONE_PERMISSION_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.CLONE_ACCT_PERMISSION_ACCOUNTS_INDEX_BY_CURRENCY %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object id="ClonePermissions" name="com.ffusion.tasks.dualapproval.ClonePermissionsDA" scope="session"/>
	<ffi:setProperty name="ClonePermissions" property="CloneEntitlementAdminEntitlement" value="false"/>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:object id="ClonePermissions" name="com.ffusion.efs.tasks.entitlements.ClonePermissions" scope="session"/>
	<ffi:setProperty name="ClonePermissions" property="CloneEntitlementAdminEntitlement" value="true"/>
</ffi:cinclude>
<ffi:setProperty name="ClonePermissions" property="CloneType" value="<%=String.valueOf(com.ffusion.efs.tasks.entitlements.ClonePermissions.OBJECT_CLONE_PER_GROUP_MEMBER)%>"/>
<ffi:setProperty name="ClonePermissions" property="SourceUserSessionName" value="BusinessEmployeeForClone"/>
<ffi:setProperty name="ClonePermissions" property="CloneLimit" value="true"/>
<ffi:setProperty name="ClonePermissions" property="CloneEntitlement" value="true"/>

<ffi:setProperty name="ClonePermissions" property="ObjectType" value="<%=com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT%>"/>
<ffi:setProperty name="ClonePermissions" property="AdminedGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
<ffi:setProperty name="ClonePermissions" property="SourceObjectsSessionName" value="AccountListForClone"/>
<ffi:setProperty name="ClonePermissions" property="TargetObjectsSessionName" value="AccountListForClone"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="runSearch"/>
<ffi:setProperty name="ClonePermissions" property="OtherResource" value="BusinessEmployeeIdForClone"/>

<s:include value="%{#session.PagesPath}user/clone-account-permissions.jsp" />