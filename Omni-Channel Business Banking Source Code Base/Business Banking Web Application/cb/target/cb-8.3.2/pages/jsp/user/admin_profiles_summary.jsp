<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<s:include value="inc/corpadminusers-pre.jsp"/>
<%-- The following div tempDivIDForMethodNsAdminReloadEmployees is used to hold temparory variables of user grids.--%>
<div id="tempDivIDForMethodNsAdminReloadEmployees" style="display: none;"></div>

<%-- Pending Approval Company island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
  <ffi:setProperty name="Section" value="Profiles" />
  <div id="admin_pendingProfilesTab" class="portlet" style="display:none;">
	  <div class="portlet-header">
	  		<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
			<%-- <div class="searchHeaderCls toggleClick expandArrow">
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<a id="adminUserPendingApprovalLink" href="javascript:void(0);" class="nameTitle"><s:text name="jsp.default.label.pending.approval.changes"/></a>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-negative floatRight"></span>
				</div>
			</div> --%>
	    </div>
	     
	   <div id="grid1Id" class="portlet-content">
	   </div>
		
		<div id="adminProfilesSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_pendingProfilesTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<!-- <div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div>	 -->
  </div>
  <div class="clearBoth" id="profileBlankDiv" style="padding: 10px 0"></div>
</ffi:cinclude>
<%-- Pending Approval Company island end --%>

<div id="profilesGridTabs" class="portlet gridPannelSupportCls">
	<div class="portlet-header">
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name='jsp.user.profile.summary.label' />
				</span>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='profiles' onclick="ns.common.showGridSummary('profiles')" >
						<s:text name='jsp.user.profile.summary.label' />
					</span>
					<ffi:cinclude value1="${Business.EntitlementGroup.Properties.ValueOfCurrentProperty}" value2="true" operator="equals">
					<span class="gridTabDropdownItem" id='businessProfiles' onclick="ns.common.showGridSummary('businessProfiles')">
						<s:text name='jsp.user.biz.profile.summary.label' />
					</span>
					</ffi:cinclude>
				</div>
			</div>
		</div>
	</div>
	
	<div class="portlet-content">
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="profilesSummaryGrid" gridId="adminProfileGrid" class="gridSummaryContainerCls" >
				<s:include value="/pages/jsp/user/corpadminprofiles.jsp"/>
			</div>
			<ffi:cinclude value1="${Business.EntitlementGroup.Properties.ValueOfCurrentProperty}" value2="true" operator="equals">
			<div id="businessProfilesSummaryGrid" gridId="businessProfileGrid" class="gridSummaryContainerCls hidden" >
				<s:include value="/pages/jsp/user/businessuserprofiles.jsp"/>
			</div>
			</ffi:cinclude>
			
		</div>
	</div>
		
	<div id="adminProfilesSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('profilesSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script>

//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("profilesGridTabs", false, false);

</script>
