<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadminaccountsconfig_grid" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Account Configuration'/>
<ffi:setProperty name='PageText' value=''/>

<div id="hidden"  style="display:none;">
    <s:url id="editAccountConfigUrl" value="%{#session.PagesPath}user/corpadminaccountedit.jsp" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
		<s:param name="runSearch" value="%{'true'}"></s:param>
    </s:url>
    <sj:a id="editAccountConfigLink" href="%{editAccountConfigUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_402')}"
        onClickTopics="beforeLoad" onCompleteTopics="completeCompanyLoad" onErrorTopics="errorLoad">
      <span class="ui-icon ui-icon-wrench"></span>
    </sj:a>

</div>

	<ffi:setGridURL grid="GRID_accountConfig" name="EditURL" url="/cb/pages/jsp/user/corpadminaccountedit-selected.jsp?aid={0}&did={1}&rnum={2}" parm0="ID" parm1="BusinessId" parm2="RoutingNum"/>

	<ffi:setProperty name="corpAdminAccountConfigGridTempURL" value="/pages/user/getACHAccounts.action?collectionName=FilteredAccounts&GridURLs=GRID_accountConfig" URLEncrypt="true"/>
	<s:url id="getSortImageUrl" value="%{#session.corpAdminAccountConfigGridTempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="accountConfigGrid"
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{getSortImageUrl}"  
		pager="true"  
		viewrecords="true" 
		gridModel="gridModel" 
 		rowList="%{#session.StdGridRowList}" 
 		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		altRows="true"
		sortorder="asc"		
		navigator="true"
		navigatorSearch="false"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		onGridCompleteTopics="addGridControlsEvents,completeAccountCongiGridLoad"
		> 
		
		<sjg:gridColumn name="bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" sortable="true" width="125"/>
		<sjg:gridColumn name="routingNum" index="ROUTINGNUM" title="%{getText('jsp.user_280')}" sortable="true" width="108"/>
		<sjg:gridColumn name="displayText" index="NUMBER" title="%{getText('jsp.default_16')}" formatter="formatAccountColumn" sortable="true" width="175"/>
		<sjg:gridColumn name="type" index="TYPE" title="%{getText('jsp.default_444')}" sortable="true" width="108"/> 
		<sjg:gridColumn name="currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.default_125')}" sortable="true" width="108"/> 
		<sjg:gridColumn name="nickName" index="NICKNAME" title="%{getText('jsp.default_294')}" sortable="true" width="125"/>
		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" formatter="formatAccountConfigActionLinks" sortable="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn" search="false" width="60"/>
		
		<sjg:gridColumn name="primaryAccount" index="primaryAccount" title="%{getText('jsp.user_263')}" sortable="false" width="10" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="coreAccount" index="coreAccount" title="%{getText('jsp.user_96')}" sortable="false" width="10" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.highLightField" index="highLightField" title="highLightField" sortable="false"  width="10" hidden="true" hidedlg="true"/>
	</sjg:grid>
	
<script type="text/javascript">
	$("#accountConfigGrid").data("supportSearch",true);
	
		var fnCustomAccountConfigRefresh = function (event) {
			ns.common.forceReloadCurrentSummary();
		};
	
		$("#accountConfigGrid").data('fnCustomRefresh', fnCustomAccountConfigRefresh);
	
</script>

<ffi:removeProperty name="AccountTypesList"/>
