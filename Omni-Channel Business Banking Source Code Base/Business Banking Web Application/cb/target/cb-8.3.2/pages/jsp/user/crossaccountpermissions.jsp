<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_crossaccountpermissions" className="moduleHelpClass"/>
<% 
	session.setAttribute("Init", request.getParameter("Init")); 
	session.setAttribute("ComponentIndex", request.getParameter("ComponentIndex")); 
	session.setAttribute("FromBack", request.getParameter("FromBack"));
	session.setAttribute("UseLastRequest", request.getParameter("UseLastRequest"));
	session.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
	session.setAttribute("perm_target", request.getParameter("perm_target"));
	session.setAttribute("completedTopics", request.getParameter("completedTopics"));
	if(request.getParameter("nextPermission") != null)
		session.setAttribute("nextPermission", request.getParameter("nextPermission"));
%>
<%-- see if the next permission button should display. If we are not on the last sub-tab, or per account, per account group, per location or wire templates tabs exist, then it should display --%>
<script type="text/javascript">
$(document).ready(function() {
	if (<ffi:getProperty name="ComponentIndex"/> == <ffi:getProperty name="xcount"/>) {
		if ($("#perAccountTab").length == 0 && $("perAccountGroupTab").length == 0 && $("perLocationTab").length == 0 && $("wireTemplateTab").length == 0) {
			$("#nextPermissionContainer").hide();
		}
	}
	
	$("#crossAccountResetButtonId").button({
		icons: {
		}			
	});
	
	$("#crossAccountResetButtonId_fromBack").button({
		icons: {
		}			
	});
});
</script>

<div id="<ffi:getProperty name="perm_target"/>">
<s:include value="inc/checkentitled-crossaccountcomponents.jsp" />

<%-- Getting the name of component from the ArrayList that should be in session from previous
	 page, based on the index that is passed in through the url --%>
<%
	if (session.getAttribute("ComponentNamesCross")!=null) {
		int index = Integer.valueOf((String)(session.getAttribute("ComponentIndex"))).intValue();
		String ComponentName = (String)(((ArrayList)(session.getAttribute("ComponentNamesCross"))).get(index));
		session.setAttribute("ComponentName", ComponentName);
	}
%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>"/>
	<ffi:removeProperty name="dependentDaEntitlements" />
</ffi:cinclude>

<ffi:cinclude value1="${PermissionsWizard}" value2="TRUE" operator="notEquals">
	<ffi:removeProperty name="ComponentNamesCross" />
</ffi:cinclude>

<%-- Set the page heading --%>

<ffi:removeProperty name="PageHeading2"/>

<%-- <ffi:include page="${PathExt}user/inc/SetPageText.jsp"/> --%>
<ffi:setProperty name="PermissionsWizardSubMenu" value="TRUE"/>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories"/>

	<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
	<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${ComponentName}"/>
	<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
					value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>" />
	<ffi:process name="GetDACategorySubType"/>
	<ffi:removeProperty name="GetDACategorySubType"/>
	<%String catSubType = null; %>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${catSubType}" />
	<ffi:removeProperty name="catSubType"/>
	
	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
	<ffi:process name="GetDACategoryDetails" />
	
	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
	<ffi:process name="GetDACategoryDetails" />
	
	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_REQUIRE_APPROVAL%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>"/>
	<ffi:process name="GetDACategoryDetails" />
</ffi:cinclude>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
		<ffi:removeProperty name="EditCrossAccountPermissions"/>
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
				<ffi:setProperty name="EditCrossAccountPermissions" property="approveAction" value="false" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
			</ffi:cinclude>
		</s:if>
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
					<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
					<ffi:setProperty name="EditCrossAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
					<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
				</ffi:cinclude>
				<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditCrossAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditCrossAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditCrossAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditCrossAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.admin.EditCrossAccountPermissions" scope="session"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="notEquals">
				<ffi:setProperty name="EditCrossAccountPermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
				<ffi:setProperty name="EditCrossAccountPermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
				<ffi:setProperty name="EditCrossAccountPermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			</ffi:cinclude>
		</s:if>
		
		<ffi:setProperty name="EditCrossAccountPermissions" property="GroupId" value="${EditGroup_GroupId}"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditCrossAccountPermissions" property="ProfileId" value="${Business.Id}"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditCrossAccountPermissions" property="EntitlementTypesMerged" value="NonAccountEntitlementsMerged"/>
		<ffi:setProperty name="EditCrossAccountPermissions" property="EntitlementTypePropertyLists" value="LimitsList"/>
		<ffi:setProperty name="EditCrossAccountPermissions" property="SaveLastRequest" value="false"/>
		<ffi:setProperty name="EditCrossAccountPermissions" property="DirectoryIdForAccounts" value="${Business.Id}"/>
      <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>

<%-- Check if SecureUser can administer the EntitlementGroup --%>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>

<%-- Display Add Group Button --%>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:object id="GetChildrenByGroupType" name="com.ffusion.efs.tasks.entitlements.GetChildrenByGroupType" scope="session"/>
		<ffi:setProperty name="GetChildrenByGroupType" property="GroupType" value="Division"/>
	<ffi:setProperty name="GetChildrenByGroupType" property="GroupId" value="${SecureUser.EntitlementID}"/>
	<ffi:process name="GetChildrenByGroupType"/>
	<ffi:removeProperty name="GetChildrenByGroupType" />

	<%-- if division size is not 0 and this secure user is entitled to adminstrator this group
		 enable the addgroup button --%>
	<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
	<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
	<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadmingroupadd.jsp"><img src="/cb/pages/${ImgExt}grafx/user/i_addgroup.gif" alt="" width="67" height="16" border="0"></a>'/>
	</ffi:cinclude>
	</ffi:cinclude>

	<%-- if division size is not 0 or this secure user is entitled to adminstrator this group
		 disable the addgroup button --%>
	<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
	<ffi:setProperty name='PageText' value=''/>
	</ffi:cinclude>

	<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name='PageText' value=''/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- Display Add User button --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
	<ffi:process name="CanAdministerAnyGroup"/>

	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${BusinessEmployeeId}" value2="" operator="notEquals">
			<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
					<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/permissionswizard.jsp"><img src="/cb/pages/${ImgExt}grafx/user/i_permissionswizard.gif" alt="" width="99" height="14" border="0"></a>'/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
			<ffi:setProperty name='PageText' value=''/>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadminprofileadd.jsp"><img src="/cb/pages/${ImgExt}grafx/user/i_addprofile.gif" alt="" width="77" height="16" border="0" hspace="3"></a>'/>
			</ffi:cinclude>
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
				<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}user/corpadminuseradd.jsp"><img src="/cb/pages/${ImgExt}grafx/user/i_adduser.gif" alt="" width="67" height="16" border="0" hspace="3"></a>'/>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name='PageText' value=''/>
	</ffi:cinclude>

	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
</s:if>

<%-- Set the Group, Division and Business Entitlement Group Names --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>

	<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:process name="GetEntitlementGroup"/>

	<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	<ffi:process name="GetEntitlementGroup"/>

	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="equals">
		<ffi:setProperty name="DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>
		<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
		<ffi:process name="GetEntitlementGroup"/>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Business" operator="equals">
			<ffi:setProperty name="BusinessName" value="${Entitlement_EntitlementGroup.GroupName}"/>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${Init}" value2="true" operator="equals">
		<ffi:setProperty name="DivisionName" value='${EditGroup_DivisionName}'/>
		<ffi:setProperty name="onlyConfirm" value="RESET"/>
	</ffi:cinclude>
</s:if>

<%-- Get the entitlementGroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<%-- Get a collection of Restricted Entitlements --%>
<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session" />
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetRestrictedEntitlements" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetRestrictedEntitlements" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetRestrictedEntitlements" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	</s:if>
	<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:process name="GetRestrictedEntitlements"/>
<ffi:removeProperty name="GetRestrictedEntitlements"/>

<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	</s:if>
    <ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
    <ffi:setProperty name="GetGroupLimits" property="ObjectType" value=""/>
    <ffi:setProperty name="GetGroupLimits" property="ObjectId" value=""/>

<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
	<ffi:setProperty name="GetTypesForEditingLimits" property="ComponentValue" value="${ComponentName}"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
		<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
		<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
		<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	</ffi:cinclude>  
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
	</s:if>
    <ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
    <ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
    <ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
    <ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
	<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountMerged" value="NonAccountEntitlementsMergedBeforeFilter"/>
<ffi:process name="GetTypesForEditingLimits"/>
<ffi:removeProperty name="GetTypesForEditingLimits"/>

<%-- filter the entitlements so that those entitlements restricted by the BusinessAdmin entitlement group 
	(as well as its parents) would not be displayed, along with the restricted entitlements' control children --%>
<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsMerged"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithLimits"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithoutLimits"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsWithLimits"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsWithoutLimits"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:removeProperty name="FilterEntitlementsForBusiness"/>

<ffi:object id="HandleParentChildDisplay" name="com.ffusion.tasks.admin.HandleParentChildDisplay"/>
<ffi:setProperty name="HandleParentChildDisplay" property="EntitlementTypePropertyLists" value="NonAccountEntitlementsMerged"/>
<ffi:setProperty name="HandleParentChildDisplay" property="PrefixName" value="entitlement"/>
<ffi:setProperty name="HandleParentChildDisplay" property="AdminPrefixName" value="admin"/>
<ffi:setProperty name="HandleParentChildDisplay" property="ParentChildName" value="ParentChildLists"/>
<ffi:setProperty name="HandleParentChildDisplay" property="ChildParentName" value="ChildParentLists"/>
<ffi:process name="HandleParentChildDisplay"/>

<ffi:object id="CheckOperationEntitlement" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<ffi:object name="com.ffusion.tasks.admin.GetMaxCrossAccountLimits" id="GetMaxCrossAccountLimits" scope="session"/>
<ffi:setProperty name="GetMaxCrossAccountLimits" property="EntitlementTypePropertyListsName" value="NonAccountEntitlementsWithLimits"/>
<ffi:setProperty name="GetMaxCrossAccountLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
    <ffi:setProperty name="GetMaxCrossAccountLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="GetMaxCrossAccountLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="GetMaxCrossAccountLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxCrossAccountLimits"/>
<ffi:removeProperty name="GetMaxCrossAccountLimits"/>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="NonAccountEntitlementsWithLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MergedListName" value="NonAccountEntitlementsMerged"/>
<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/crossaccountpermissions-verify-init.jsp?PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/crossaccountpermissions.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>

<% session.setAttribute("FFIEditCrossAccountPermissions", session.getAttribute("EditCrossAccountPermissions")); %>


<script type="text/javascript"><!--

	(function($){
		initializeParentChild();
	})(jQuery);
	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].checked = value;
			}
		}
	}
	function initializeParentChild() {
		parentChildArray = new Array();
		childParentArray = new Array();

		prefixes = new Array( "<ffi:getProperty name="HandleParentChildDisplay" property="PrefixName"/>","<ffi:getProperty name="HandleParentChildDisplay" property="AdminPrefixName"/>" );

<ffi:object id="counterMath" name="com.ffusion.beans.util.IntegerMath"/>
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:setProperty name="counterMath" property="Value2" value="1"/>

		// Populate the parentChildArray
<ffi:list collection="ParentChildLists" items="childList">
		parentChildArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array(
	<ffi:list collection="childList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="childList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);

	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />
</ffi:list>

		// Populate the childParentArray
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:list collection="ChildParentLists" items="parentList">
		childParentArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array(
	<ffi:list collection="parentList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="parentList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);

	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />
</ffi:list>
	}
	var ra_child_parent;
	var ra_parent_child;
	var ra_disp_props = new Array();
	var name_id_map = new Array();
	var ra_limits = new Array();
	
	ns.admin.nextPermissionForXAcct = function ()
	{
		<ffi:cinclude value1="${ComponentIndex}" value2="${xcount}" operator="notEquals">
			ns.admin.nextPermission('#<ffi:getProperty name="nextPermission"/>','#XAcctTabs')
		</ffi:cinclude>
		<ffi:cinclude value1="${ComponentIndex}" value2="${xcount}" operator="equals">
			ns.admin.nextPermission('#<ffi:getProperty name="nextPermission"/>','#permTabs')
		</ffi:cinclude>
		
	}
	
// --></script>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- Requires Approval Settings --%>
<ffi:object id="DASettings" name="com.ffusion.tasks.dualapproval.GetDASettings" />
<ffi:process name="DASettings" />
<ffi:removeProperty name="DASettings" />
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:removeProperty name="CheckRequiresApproval" />
	<ffi:object id="CheckRequiresApproval" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />
	<ffi:setProperty name="CheckRequiresApproval" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
	<ffi:setProperty name="CheckRequiresApproval" property="OpCounter" value="0" />
	<ffi:setProperty name="CheckRequiresApproval" property="DispPropListName" value="NonAccountEntitlementsMerged" />
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="CheckRequiresApproval" property="ModifyUserOnly" value="true" />
		<ffi:setProperty name="CheckRequiresApproval" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="CheckRequiresApproval" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="CheckRequiresApproval" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	
	<ffi:removeProperty name="EditRequiresApproval" />
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
				<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApprovalDA" />
				<ffi:setProperty name="EditRequiresApproval" property="approveAction" value="false" />
				<ffi:setProperty name="EditRequiresApproval" property="ModifyUserOnly" value="false" />
				<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
					<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
					<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${SecureUser.BusinessID}" />
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
					<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
					<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${Entitlement_EntitlementGroup.GroupId}" />
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
					<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
					<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${Entitlement_EntitlementGroup.GroupId}" />
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApprovalDA" />
				</s:if>
				<s:else>
					<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApproval" />
				</s:else>
				<ffi:setProperty name="EditRequiresApproval" property="approveAction" value="false" />
				<ffi:setProperty name="EditRequiresApproval" property="ModifyUserOnly" value="false" />
				<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
					<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
					<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${SecureUser.BusinessID}" />
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
					<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
					<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${Entitlement_EntitlementGroup.GroupId}" />
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
					<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
					<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${Entitlement_EntitlementGroup.GroupId}" />
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
				<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApproval" />
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApproval" />
		</ffi:cinclude>
	
	<ffi:setProperty name="EditRequiresApproval" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="EditRequiresApproval" property="ModifyUserOnly" value="true" />
		<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="EditRequiresApproval" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
</ffi:cinclude>
<%-- END Requires Approval Settings --%>

<% session.setAttribute("FFIEditRequiresApproval", session.getAttribute("EditRequiresApproval")); %>

<% String heading = null; %>
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/crossaccountpermissions.jsp-1" parm0="${ComponentName}" assignTo="heading"/>
</ffi:cinclude>
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/crossaccountpermissions-view.jsp-1" parm0="${ComponentName}" assignTo="heading"/>
</ffi:cinclude>
<ffi:setProperty name="PageHeading" value="<%=heading%>" />
<div class="marginTop10">
	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
		<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/crossaccountpermissions.jsp-1" parm0="${ComponentName}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
		<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/crossaccountpermissions-view.jsp-1" parm0="${ComponentName}"/>
	</ffi:cinclude>
</div>
	<s:include value="inc/parentchild_js.jsp"/>
<ul id="formerrors"></ul>
<div align="center" class="marginBottom10"><ffi:getProperty name="Context"/></div>	
<s:form id="permLimitFrm" name="permLimitFrm" namespace="/pages/user" action="editCrossAccountPermissions-verify" validate="false" theme="simple" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData permissionTblWithBorder">
<input type="hidden" name="EditGroup_AccountID" value='<ffi:getProperty name="Account" property="ID"/>'>
<input type="hidden" name="EditGroup_GroupId" value='<ffi:getProperty name="EditGroup_GroupId"/>'>
                               <tr class="header">
                               	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
                                   <td valign="middle" align="center" nowrap class="sectionsubhead">
                                       
                                       <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<input type="checkbox" value="ACTIVATE ALL" onclick="setCheckboxes(permLimitFrm,this.checked,'admin');"> <s:text name="jsp.user_32"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(permLimitFrm,this.checked,'admin');">
						</ffi:cinclude>
					</td>
                                   </ffi:cinclude>
                                   <td valign="middle" align="center" nowrap class="sectionsubhead">
                                       <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<input type="checkbox" value="ACTIVATE ALL" onclick="setCheckboxes(permLimitFrm,this.checked,'entitlement');"> <s:text name="jsp.user_177"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(permLimitFrm,this.checked,'entitlement');">
						</ffi:cinclude>
                                   </td>
                                   <td valign="middle" nowrap class="sectionsubhead" style="text-align: left;">
                                       <s:text name="jsp.user_346"/>
                                   </td>
                                                         <%-- Requires Approval Column --%>	 
                                  <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
                                   <td valign="middle" nowrap class="sectionsubhead">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<s:text name="jsp.user_278"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
							&nbsp;
						</ffi:cinclude>
					</td>
                                   <input type="hidden" id="RequiresApprovalModified" name="RequiresApprovalModified" value="false" />
                                  </ffi:cinclude>
                                   <td colspan="2" valign="middle" align="center" nowrap class="sectionsubhead">
<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
<ffi:process name="GetLimitBaseCurrency" />
                                       <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                   </td>
                                   <td valign="middle" align="center" nowrap class="sectionsubhead">
																<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                       <s:text name="jsp.user_153"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																	&nbsp;
																</ffi:cinclude>
                                   </td>
                                   <td colspan="2" valign="middle" align="center" nowrap class="sectionsubhead">
                                       <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                   </td>
                                   <td valign="top" align="center" nowrap class="sectionsubhead">
																<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                       <s:text name="jsp.user_153"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																	&nbsp;
																</ffi:cinclude>
                                   </td>
                               </tr>
<%-- Flag so that the included pages that actually display the limits and entitlements
	 will include the javascript function that makes parent/child work --%>
<ffi:setProperty name="ParentChild" value="true"/>

<%-- Flag to determine if the admin column should be drawn --%>
<ffi:setProperty name="HasAdmin" value="true"/>
<%
session.setAttribute( "limitIndex", new Integer( 0 ) );
session.setAttribute( "LimitsList", session.getAttribute("NonAccountEntitlementsMerged") );
%>
<ffi:setProperty name="DisplayedLimit" value="FALSE"/>
<ffi:setProperty name="CheckEntitlementObjectType" value=""/>
<ffi:setProperty name="CheckEntitlementObjectId" value=""/>
<ffi:flush/>

<%-- Include page to display Entitlements with limits --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
<s:include value="inc/corpadminuserlimit_merged.jsp"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
<s:include value="inc/corpadmingrouplimit_merged.jsp"/>
</s:if>

<ffi:cinclude value1="${DisplayedLimit}" value2="FALSE" operator="equals">
                                        <tr>
                                            <td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="middle" align="center" style="padding-top:8px;">
                                                <span class="columndata"><s:text name="jsp.user_215"/></span>
                                            </td>
                                        </tr>
</ffi:cinclude>
                               <tr>
                                   <td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top" align="center" class="sectionsubhead">
                                       <br>
		
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
			<ffi:cinclude value1="${onlyConfirm}" value2="TRUE" operator="notEquals" >
				<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="notEquals">
					<%-- <a  id="crossAccountResetButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
						title='<s:text name="jsp.default_358.1" />' 
						href='#'
						onClick='javascript:selectReqApprChildrensForReset(permLimitFrm, "<ffi:getProperty name="limitIndex"/>"); removeValidationErrors();'>
						<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358"/></span>
					</a> --%>
					
					<a  id="crossAccountResetButtonId"  
						title='<s:text name="jsp.default_358.1" />'
						href='#'
						onClick='javascript:selectReqApprChildrensForReset(permLimitFrm, "<ffi:getProperty name="limitIndex"/>"); removeValidationErrors();'>
						<s:text name="jsp.default_358"/>
					</a>
				</ffi:cinclude>
				<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="equals">
				<%-- QTS 760399 --%>
				<script type="text/javascript">
				selectReqApprChildrensForReset(permLimitFrm, "<ffi:getProperty name="limitIndex"/>");
				</script>
					<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?Init=${Init}&ComponentIndex=${ComponentIndex}&perm_target=${perm_target}&completedTopics=${completedTopics}&nextPermission=${nextPermission}" URLEncrypt="true"/>
					<a  id="crossAccountResetButtonId_fromBack" 
						title='<s:text name="jsp.default_358.1" />' 
						href='#'
						onClick='ns.admin.reset("<ffi:getProperty name='tempURL'/>","#<ffi:getProperty name="perm_target"/>");'>
						<s:text name="jsp.default_358"/>
					</a>
				</ffi:cinclude>
			</ffi:cinclude>
			
			<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
			
                           <ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
				<sj:a id="SAVE"
					formIds="permLimitFrm"
					targets="%{#session.perm_target}"
					button="true"
					validate="true"
					validateFunction="customValidation"
					onBeforeTopics="beforeVerify"
					onCompleteTopics="adminSaveOnCompleteTopic"
					><s:text name="jsp.default_366"/></sj:a>
    		</ffi:cinclude>			
    		<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="notEquals">
				<sj:a id="SAVE"
					formIds="permLimitFrm"
					targets="%{#session.perm_target}"
					button="true"
					validate="true"
					validateFunction="customValidation"
					onBeforeTopics="beforeVerify"
					><s:text name="jsp.default_366"/></sj:a>
    		</ffi:cinclude>
			<span id="nextPermissionContainer">
				<sj:a
					id="%{#attr.nextPermission}"
					button="true"
					onclick="ns.admin.nextPermissionForXAcct();"
					><s:text name="jsp.user_203"/></sj:a>
			</span>
		</ffi:cinclude>
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
			<br>
			<tr>
			<td align="center" colspan="8">
				<sj:a
					button="true"
					onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
					><s:text name="jsp.default_102"/>
				</sj:a>

			</td>
			</tr>
		</ffi:cinclude>
				</td>
           </tr>
           <tr>
				<td height="10">&nbsp;</td>
			</tr>
                            
                     <%-- <tr>
                            	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                              <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                </ffi:cinclude>
					<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
                              <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                 <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                </ffi:cinclude>
                     </tr> --%>

                            <!-- <tr>
                                <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
                                <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
                                <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
                                <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
                                <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
                                <td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" border="0"></td>
                            </tr> -->
                        </table>
</s:form>

<ffi:removeProperty name="GetMaxLimitForPeriod"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>
<ffi:removeProperty name="AccountEntitlementsWithLimits"/>
<ffi:removeProperty name="AccountEntitlementsWithoutLimits"/> 
<ffi:removeProperty name="limitIndex"/>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<ffi:removeProperty name="CanAdminister"/>
<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="HandleParentChildDisplay"/>
<ffi:removeProperty name="ParentChild"/>
<ffi:removeProperty name="HasAdmin"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="ChildParentLists"/>
<ffi:removeProperty name="ParentChildLists"/>

<ffi:removeProperty name="counterMath"/>

<ffi:removeProperty name="${Entitlement_EntitlementGroup.GroupName}Accounts"/>

<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="PermissionsWizardSubMenu"/>
<ffi:removeProperty name="GetCategories"/>
<ffi:removeProperty name="GetDACategoryDetails"/>

<ffi:setProperty name="FromBack" value=""/>
<%-- Requires Approval Hirarchy generation --%>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<script type="text/javascript">
		genReqApprParentChildList();
	</script>
</ffi:cinclude>
<%-- END Requires Approval Hirarchy generation --%>
</div>
<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />
