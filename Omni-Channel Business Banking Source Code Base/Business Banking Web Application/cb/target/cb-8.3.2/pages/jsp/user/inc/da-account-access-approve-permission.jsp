<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:object id="EditAccountAccessPermissionsDA" name="com.ffusion.tasks.dualapproval.EditAccountAccessPermissionsDA" scope="session"/>

<ffi:setProperty name="EditAccountAccessPermissionsDA" property="approveAction" value="true" />

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditAccountAccessPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditAccountAccessPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditAccountAccessPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditAccountAccessPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="EditAccountAccessPermissionsDA" property="AutoEntitle" value="false"/>
	<ffi:setProperty name="EditAccountAccessPermissionsDA" property="AccountsName" value="FilteredAccounts"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="EditAccountAccessPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>

<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
<ffi:setProperty name="approveCategory" property="categorySubType" value="" />

<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
<ffi:setProperty name="GetCategories" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>"/>
<ffi:removeProperty name="categories" />
<ffi:process name="GetCategories" />

<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:process name="EditAccountAccessPermissionsDA" />
	<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
		<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
		<ffi:setProperty name="ApprovePendingChanges" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>"/>
		<ffi:process name="ApprovePendingChanges"/>
	</ffi:list>
</ffi:cinclude>

<ffi:removeProperty name="EditAccountAccessPermissionsDA" />