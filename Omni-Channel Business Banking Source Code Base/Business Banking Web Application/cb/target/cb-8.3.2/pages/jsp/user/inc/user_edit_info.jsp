<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<ffi:help id="user_user_addedit_info" className="moduleHelpClass"/>
<ffi:setProperty name="PageTitle" value="Manage Users"/>
<ffi:setProperty name="topMenu" value="home"/>
<ffi:setProperty name="subMenu" value="preferences"/>
<ffi:setProperty name="sub2Menu" value="manageusers"/>

<ffi:object name="com.ffusion.beans.util.IntegerMath" id="IntegerMath" scope="request"/>

<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResources" scope="request"/>
<ffi:setProperty name="UserResources" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResources"/>

<ffi:cinclude value1="${CountryList}" value2="">
    <ffi:object name="com.ffusion.tasks.util.GetCountryList" id="GetCountryList" scope="request"/>
    <ffi:setProperty name="GetCountryList" property="CollectionSessionName" value="CountryList"/>
    <ffi:process name="GetCountryList"/>
</ffi:cinclude>

<ffi:object name="com.ffusion.tasks.util.GetStatesForCountry" id="GetStatesForCountry" scope="request"/>

<ffi:cinclude value1="${FFIEditSecondaryUser.Country}" value2="" operator="notEquals">
    <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value="${FFIEditSecondaryUser.Country}"/>
</ffi:cinclude>
<ffi:process name="GetStatesForCountry"/>

<ffi:object name="com.ffusion.tasks.util.GetLanguageList" id="GetLanguageList" scope="session"/>
<ffi:process name="GetLanguageList"/>

<%-- Set a custom mapping of password status codes to user-friendly text labels. --%>
<ffi:object name="com.ffusion.beans.util.StringTable" id="PasswordStatusLabels" scope="session"/>
<ffi:setProperty name="PasswordStatusLabels" property="Key" value='<%="" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_VALID%>'/>
<ffi:setProperty name="PasswordStatusLabels" property="Value" value="Unlocked"/>
<ffi:setProperty name="PasswordStatusLabels" property="Key" value='<%="" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED%>'/>
<ffi:setProperty name="PasswordStatusLabels" property="Value" value="Locked"/>
<ffi:setProperty name="PasswordStatusLabels" property="Key" value='<%="" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_CHANGE%>'/>
<ffi:setProperty name="PasswordStatusLabels" property="Value" value="Unlocked"/>
<ffi:setProperty name="PasswordStatusLabels" property="Key" value='<%="" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_AUTHENTICATE%>'/>
<ffi:setProperty name="PasswordStatusLabels" property="Value" value="Unlocked"/>
<ffi:setProperty name="PasswordStatusLabels" property="Key" value='<%="" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED%>'/>
<ffi:setProperty name="PasswordStatusLabels" property="Value" value="Expired"/>
<ffi:setProperty name="PasswordStatusLabels" property="Key" value='<%="" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE%>'/>
<ffi:setProperty name="PasswordStatusLabels" property="Value" value="Locked"/>

<ffi:cinclude value1="${pageRefresh}" value2="true" operator="notEquals">
	<ffi:setProperty name="CurrentPassword" value=""/>
	<ffi:setProperty name="Password" value=""/>
	<ffi:setProperty name="ConfirmPassword" value=""/>
	<ffi:setProperty name="CurrentPassword2" value=""/>
	<ffi:setProperty name="Password2" value=""/>
	<ffi:setProperty name="ConfirmPassword2" value=""/>
</ffi:cinclude>
<ffi:cinclude value1="${Password}" value2="<%= com.ffusion.tasks.user.AddUser.PASSWORD_MASK %>" operator="notEquals">
	<ffi:setProperty name="Password2" value="${Password}"/>
	<ffi:cinclude value1="${Password}" value2="" operator="notEquals">
		<ffi:setProperty name="Password" value="<%=com.ffusion.tasks.user.AddUser.PASSWORD_MASK%>"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${ConfirmPassword}" value2="<%= com.ffusion.tasks.user.AddUser.PASSWORD_MASK %>" operator="notEquals">
	<ffi:setProperty name="ConfirmPassword2" value="${ConfirmPassword}"/>
	<ffi:cinclude value1="${ConfirmPassword}" value2="" operator="notEquals">
		<ffi:setProperty name="ConfirmPassword" value="<%=com.ffusion.tasks.user.AddUser.PASSWORD_MASK%>"/>
	</ffi:cinclude>
</ffi:cinclude>

<style>
input[type="text"],input[type="password"] {
 width:237px;
 }
</style>

<s:include value="/pages/jsp/user/password-indicator-js.jsp" />
<s:include value="/pages/jsp/user/corpadmin_js.jsp" />

<div class="marginTop10" style="position:relative; overflow:hidden">
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><s:text name='secondaryUser.userInformation'/></div>
   		<div class="paneContentWrapper">
   			<div  class="instructions">
				<s:text name="secondaryUser.wizardTab.tab0.message"/>
			</div>
   		</div>
   	</div>
</div>
<div class="marginTop10"></div>
<form name="UserInfoForm"  id="UserInfoForm" action="/cb/pages/jsp/user/savePersonalInformation.action" autocomplete="OFF" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table border="0" cellpadding="" cellspacing="0" width="100%" class="tableData tdTopBottomPadding5">
	<tr><td colspan="4"><h3 class="transactionHeading"><s:text name='admin.login.information'/></h3></td></tr>
	<tr>
		<td><s:text name='jsp.user_Username'/><span class="required">*</span></td>
		<td><s:text name='user.password'/><ffi:cinclude value1="${AddEditMode}" value2="Add" operator="equals"><span class="required">*</span></ffi:cinclude></td>
		<td><s:text name='user.confirmPassword'/><ffi:cinclude value1="${AddEditMode}" value2="Add" operator="equals"><span class="required">*</span></ffi:cinclude></td>
        <td><s:text name='jsp.user_User_Status'/></td>
    </tr>
    <tr>
        <td>
			<input name="userName" type="text" class="txtbox ui-widget-content ui-corner-all" size="20" maxlength="20" tabindex="13" value="<ffi:getProperty name='FFIEditSecondaryUser' property='UserName'/>">
		</td>
		<td>
			<div style="width:242px;"  id="dvcPswdManageUser">
			<input name="newPassword" id="userEditPasswd" type="password" class="txtbox ui-widget-content ui-corner-all inputPassword" size="20" maxlength="15" tabindex="14" value="<ffi:getProperty name="Password"/>">
			</div>
		</td>
		<td>
			<input name="newConfirmPassword" type="password" class="txtbox ui-widget-content ui-corner-all" size="20" maxlength="15" tabindex="15" value="<ffi:getProperty name="ConfirmPassword"/>">
		</td>
		<td>
            <select id="secUsersAccountStatus" name="accountStatus" class="form_fields" tabindex="16">
				<ffi:setProperty name="UserResources" property="ResourceID" value="ACCOUNTSTATUS.1"/>
                <option value='<%= com.ffusion.beans.user.User.STATUS_ACTIVE %>' <ffi:cinclude value1="${FFIEditSecondaryUser.AccountStatus}" value2="<%= com.ffusion.beans.user.User.STATUS_ACTIVE %>" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="UserResources" property="Resource"/></option>
				<ffi:setProperty name="UserResources" property="ResourceID" value="ACCOUNTSTATUS.2"/>
                <option value='<%= com.ffusion.beans.user.User.STATUS_INACTIVE %>' <ffi:cinclude value1="${FFIEditSecondaryUser.AccountStatus}" value2="<%= com.ffusion.beans.user.User.STATUS_INACTIVE %>" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="UserResources" property="Resource"/></option>
            </select>
        </td>
	</tr>
	<tr>
		<td><span id="userNameError" class="errorLabel"></span></td>
		<td><span id="passwordError" class="errorLabel"></span></td>
		<td><span id="confirmPasswordError" class="errorLabel"></span></td>
		<td><span id="InvalidAction.UserEdit.RestrictedError" class="errorLabel"></span></td>
	</tr>
	<ffi:cinclude value1="${AddEditMode}" value2="Edit" operator="equals">
		<tr>
        	<td><s:text name='jsp.user_Lockout_Status'/></td>
       	 	<td><s:text name='jsp.user_279'/>:</td>
       	 	<td></td>
       	 	<td></td>
        </tr>
        <tr>
			<ffi:setProperty name="PasswordStatusLabels" property="Key" value="${FFIEditSecondaryUser.PASSWORD_STATUS}"/>
			<ffi:setProperty name="DisplayUnlockButton" value="true"/>
			<ffi:cinclude value1="${FFIEditSecondaryUser.PASSWORD_STATUS}" value2='<%= "" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_VALID %>' operator="equals">
			    <ffi:setProperty name="DisplayUnlockButton" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${FFIEditSecondaryUser.PASSWORD_STATUS}" value2='<%= "" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_CHANGE %>' operator="equals">
			    <ffi:setProperty name="DisplayUnlockButton" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${FFIEditSecondaryUser.PASSWORD_STATUS}" value2='<%= "" + com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_AUTHENTICATE %>' operator="equals">
			    <ffi:setProperty name="DisplayUnlockButton" value="false"/>
			</ffi:cinclude>
			<ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_info_save.jsp?Target=${SecurePath}user/user_addedit_info.jsp?pageRefresh=true&UnlockButton=TRUE" URLEncrypt="true"/>
	        <td style="padding-left: 0px; padding-right: 0px;" class="tabledata_white">
				<span id="unlockButtonLabel"><ffi:getProperty name="PasswordStatusLabels" property="Value"/></span>
				<ffi:cinclude value1="${DisplayUnlockButton}" value2="true" operator="equals">
					<span id="unlockButtonContainer"><sj:a id="unlockUser" button="true" onClickTopics="secondaryUsers.unlock"><s:text name="jsp.user_350"/></sj:a></span>
				</ffi:cinclude>
			</td>
			<ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_info_save.jsp?Target=${SecurePath}user/user_addedit_info.jsp?pageRefresh=true&ResetPasswordQuestionAnswerButton=TRUE" URLEncrypt="true"/>
	        <td>
				<sj:a id="resetQA" button="true" onClickTopics="secondaryUsers.resetQA"><s:text name="jsp.default_358"/></sj:a>        				
			</td>
			<td></td>
			<td></td>
		</tr>
</ffi:cinclude>
 	<tr>
		<td colspan="4" align="right"><span style="font-size:11px; font-style:italic" class="">* <ffi:include page="/pages/jsp/user/password-strength-text.jsp" /></span></td>
	</tr>
	<%-- Setting the secondary user's customer id to be the same as the primary users's --%>
 	<ffi:setProperty name="SecondaryUser" property="CustId" value="${SecureUser.PrimaryUserCustID}"/>
	<tr><td colspan="4"><h3 class="transactionHeading"><s:text name='admin.personal.information'/></h3></td></tr>
    <tr>
    	<td><s:text name='user.title'/></td>
    	<td><s:text name='jsp.user_First_Name'/><span class="required">*</span></td>
    	<td><s:text name='jsp.user_Last_Name'/><span class="required">*</span></td>
    	<td><s:text name='jsp.user_Phone'/></td>
    </tr>
    <tr>
        <td><input name="title" type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="10" tabindex="1" value="<ffi:getProperty name='FFIEditSecondaryUser' property='Title'/>"></td>
        <td>
			<input name="firstName" type="text" class="txtbox ui-widget-content ui-corner-all" size="25" maxlength="35" tabindex="2" value="<ffi:getProperty name='FFIEditSecondaryUser' property='FirstName'/>">
		</td>
		<td>
			<input name="lastName" type="text" class="txtbox ui-widget-content ui-corner-all" size="25" maxlength="35" tabindex="3" value="<ffi:getProperty name='FFIEditSecondaryUser' property='LastName'/>">
		</td>
		<td>
			<input name="phone" type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="14" tabindex="10" value="<ffi:getProperty name='FFIEditSecondaryUser' property='Phone'/>">
		</td>
   	</tr>     
    <tr>
        <td></td>
        <td><span id="firstNameError" class="errorLabel"></span></td>
        <td><span id="lastNameError" class="errorLabel"></span></td>
        <td></td>
    </tr>
    <tr>
        <td><s:text name='jsp.user_148'/></td>
        <td><s:text name='user.address1'/></td>
        <td><s:text name='user.address2'/></td>
        <td><s:text name='jsp.user_65'/></td>
    </tr>
    <tr>
        <td><input name="email" type="text" class="txtbox ui-widget-content ui-corner-all" size="25" maxlength="40" tabindex="11" value="<ffi:getProperty name='FFIEditSecondaryUser' property='Email'/>"></td>
        <td><input name="street" type="text" class="txtbox ui-widget-content ui-corner-all" size="25" maxlength="40" tabindex="4" value="<ffi:getProperty name='FFIEditSecondaryUser' property='Street'/>"></td>
        <td><input name="street2" type="text" class="txtbox ui-widget-content ui-corner-all" size="25" maxlength="40" tabindex="5" value="<ffi:getProperty name='FFIEditSecondaryUser' property='Street2'/>"></td>
        <td><input name="city" type="text" class="txtbox ui-widget-content ui-corner-all" size="25" maxlength="20" tabindex="6" value="<ffi:getProperty name='FFIEditSecondaryUser' property='City'/>"></td>
     </tr>
    <tr>
        <td><s:text name='jsp.user_97'/></td>
        <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
        	<td id="stateRowLabel"><s:text name='jsp.user_299'/></td>
        </ffi:cinclude>
         <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
        	<td id="stateRowLabel" class="hidden"><s:text name='jsp.user_299'/></td>
        </ffi:cinclude>
    	    <td><!-- L10NStart -->ZIP/Postal Code:<!-- L10NEnd --></td>
		<ffi:setProperty name="IntegerMath" property="Value1" value="${GetLanguageList.LanguagesList.Size}"/>
		<ffi:setProperty name="IntegerMath" property="Value2" value="1"/>
		<ffi:cinclude value1="${IntegerMath.Greater}" value2="true" operator="equals">
		        <td><s:text name='user.preferredLanguage'/></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${IntegerMath.Greater}" value2="true" operator="notEquals">
		<ffi:list collection="GetLanguageList.LanguagesList" items="Language">
		    <ffi:setProperty name="SecondaryUser" property="PreferredLanguage" value="${Language.Language}"/>
		</ffi:list>
		</ffi:cinclude>
    </tr>
    <tr>
        <td>
            <select id="secUsersCountry" name="country" style="width: 175px;" tabindex="7" >
                <option value=""><s:text name='jsp.common_Select'/></option>
				<ffi:list collection="CountryList" items="Country">
                <option value="<ffi:getProperty name='Country' property='CountryCode'/>" <ffi:cinclude value1="${FFIEditSecondaryUser.Country}" value2="${Country.CountryCode}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="Country" property="Name"/></option>
				</ffi:list>
            </select>
        </td>
        <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
	        <td style="vertical-align: middle;"  id="stateRow"  >
	            <select id="secUsersState" name="State" class="form_fields" tabindex="8" style="width: 121px;">
	                <option value=""><s:text name="jsp.default_376"/></option>
					<ffi:list collection="GetStatesForCountry.StateList" items="State">
	                <option value="<ffi:getProperty name='State' property='StateKey'/>" <ffi:cinclude value1="${FFIEditSecondaryUser.State}" value2="${State.StateKey}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="State" property="Name"/></option>
					</ffi:list>
	            </select>
	            <%-- <input name="zipCode" class="txtbox ui-widget-content ui-corner-all"  type="text"  size="10" maxlength="10" tabindex="9" value="<ffi:getProperty name='FFIEditSecondaryUser' property='ZipCode'/>" value="<ffi:getProperty name='FFIEditSecondaryUser' property='ZipCode'/>"> --%>
	        </td>
		</ffi:cinclude>
		   <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
		    <td style="vertical-align: middle;"  id="stateRow" class="hidden">
		    	 <select id="secUsersState" name="State"  tabindex="8" >
                </select>
		    </td>
		  </ffi:cinclude>
	
	        <td>
	            <input name="zipCode" class="txtbox ui-widget-content ui-corner-all" type="text"  size="10" maxlength="11" tabindex="9" value="<ffi:getProperty name='FFIEditSecondaryUser' property='ZipCode'/>">
	        </td>
	
		<ffi:cinclude value1="${IntegerMath.Greater}" value2="true" operator="equals">
	        <td>
	            <select id="secUsersPrefLanguage" name="preferredLanguage" class="form_fields" tabindex="12">
				<ffi:list collection="GetLanguageList.LanguagesList" items="Language">
	                <option value="<ffi:getProperty name='Language' property='Language'/>" <ffi:cinclude value1="${FFIEditSecondaryUser.PreferredLanguage}" value2="${Language.Language}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="Language" property="DisplayName"/></option>
				</ffi:list>
	            </select>
	        </td>
		</ffi:cinclude>
    </tr>
</table>
    <div align="center"><span class="required">* <s:text name='jsp.default_240'/></span></div>
    <div class="btn-row">
		<sj:a id="secUserPersonalInfoCancel" 
		button="true" 
		summaryDivId="summary" 
		buttonType="cancel" 
		onClickTopics="showSummary,secondaryUsers.cancel">
		<s:text name="jsp.default_82"/></sj:a>        
		<%--<sj:a id="secUserPersonalInfoBack" button="true" ><s:text name="jsp.default_57"/></sj:a>        --%>
		<sj:a id="secUserPersonalInfoNext" button="true" formIds="UserInfoForm" targets="targetDiv"  validate="true" validateFunction="customValidation" onBeforeTopics="secondaryUsers.clearErrors" onSuccessTopics="secondaryUsers.savePersonalInformationSuccess"><s:text name="jsp.default_291"/></sj:a>        
    </div>
</form>

</div>
<div id="targetDiv">
</div>
<ffi:removeProperty name="GetLanguageList"/>
<ffi:removeProperty name="DisplayUnlockButton"/>
<ffi:removeProperty name="UnlockButton"/>
<ffi:removeProperty name="PasswordStatusLabels"/>
<ffi:removeProperty name="TempURL"/>
<ffi:removeProperty name="ResetPasswordQuestionAnswerButton"/>
<ffi:removeProperty name="CancelButton"/>
<ffi:removeProperty name="BackButton"/>
<ffi:removeProperty name="NextButton"/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/user_addedit_info.jsp?pageRefresh=true" URLEncrypt="true"/>

<script>
$("#secUsersCountry").selectmenu({"width":"245px"});	
$("#secUsersAccountStatus").selectmenu({"width":"150px"});
$("#secUsersPrefLanguage").selectmenu({"width":"150px"});
$("#secUsersState").selectmenu({"width":"245px"});

$("#userEditPasswd").strength({
});
</script>
