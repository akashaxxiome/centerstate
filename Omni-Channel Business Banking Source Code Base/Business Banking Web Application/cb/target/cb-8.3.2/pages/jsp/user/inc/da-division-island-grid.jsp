<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadmindiv_da_pendingapproval" className="moduleHelpClass"/>

<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="EditURLForAdd" url="/cb/pages/jsp/user/da-new-division-edit.jsp?EditDivision_GroupId={0}&DisplayLocation=true&Section=Divisions&EditGroup_GroupId={1}&EditDivision_ACHCompID={2}&EntGroupItemACHCompanyName={3}" parm0="ItemId" parm1="ItemId" parm2="GroupPropertiesValue" parm3="GroupPropertiesValue"/>
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="EditURLNotForAdd" url="/cb/pages/jsp/user/corpadmindivedit.jsp?EditDivision_GroupId={0}&DisplayLocation=true&Section=Divisions&EditGroup_GroupId={1}&EditDivision_ACHCompID={2}&EntGroupItemACHCompanyName={3}" parm0="ItemId" parm1="ItemId" parm2="GroupPropertiesValue" parm3="GroupPropertiesValue"/>
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="DiscardURL" url="/cb/pages/jsp/user/da-verify-discard-division.jsp?itemId={0}&successUrl=${SecurePath}user/corpadmindiv.jsp&itemType=Division&GroupName={1}" parm0="ItemId" parm1="GroupName" />
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="ApprovalURL" url="/cb/pages/jsp/user/da-verify-submit-division.jsp?itemId={0}&successUrl=${SecurePath}user/corpadmindiv.jsp&itemType=Division&category=Profile&GroupName={1}" parm0="ItemId" parm1="GroupName" />
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Divisions&EditGroup_GroupId={0}" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="ViewPermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Divisions&EditGroup_GroupId={0}&ViewUserPermissions=TRUE" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="ViewURLForAdd" url="/cb/pages/jsp/user/da-new-division-edit.jsp?viewOnly=TRUE&EditDivision_GroupId={0}&Section=Divisions" parm0="ItemId" />
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="ViewURLNotForAdd" url="/cb/pages/jsp/user/corpadmindivview.jsp?EditDivision_GroupId={0}&Section=Divisions&EditGroup_GroupId={1}&EditDivision_ACHCompID={2}&EditDivision_ACHCompName={3}" parm0="ItemId" parm1="ItemId" parm2="GroupPropertiesValue" parm3="GroupPropertiesValue" />
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="ModifyURL" url="/cb/pages/jsp/user/da-modify-changes.jsp?itemId={0}&itemType=Division" parm0="ItemId"/>
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="ApprovalDADivisionChangeURL" url="/cb/pages/jsp/user/da-division-verify.jsp?Section=Division&itemId={0}&rejectFlag=n&GroupName={1}&userAction={2}" parm0="ItemId" parm1="GroupName" parm2="Action"/>
<ffi:setGridURL grid="GRID_adminDivisionPendingApprovalChanges" name="RejectURL" url="/cb/pages/jsp/user/da-division-verify.jsp?Section=Division&itemId={0}&itemType=Division&rejectFlag=y&GroupName={1}&userAction={2}" parm0="ItemId" parm1="GroupName" parm2="Action"/>

<ffi:setProperty name="pendingDivisionChangesTempURL" value="/pages/user/getBusinessGroups.action?CollectionName=PendingApprovalGroups&GridURLs=GRID_adminDivisionPendingApprovalChanges&dualApprovalMode=1" URLEncrypt="true"/>
<s:url id="pendingChangesUrl" value="%{#session.pendingDivisionChangesTempURL}" escapeAmp="false"/>
<sjg:grid  
	id="pendingDivisionChangesID"  
	caption=""  
	dataType="json"  
	href="%{pendingChangesUrl}"  
	pager="true"  
	gridModel="gridModel" 
	rowList="%{#session.StdGridRowList}" 
	rowNum="%{#session.StdGridRowNum}" 
	rownumbers="false"
	shrinkToFit="true"
	navigator="true"
	navigatorAdd="false"
	navigatorDelete="false"
	navigatorEdit="false"
	navigatorRefresh="false"
	navigatorSearch="false"
	navigatorView="false"
	sortable="true" 
	scroll="false"
	scrollrows="true"
	viewrecords="true"
	onGridCompleteTopics="addGridControlsEvents,pendingDAChangesGridCompleteEventsForDivision"
> 
		
	<sjg:gridColumn name="groupName" index="GroupName" title="%{getText('jsp.user_121')}" sortable="true" />
	<sjg:gridColumn name="administrators" index="Administrators" title="%{getText('jsp.user_38')}" sortable="false" />
	<sjg:gridColumn name="action" index="Action" title="%{getText('jsp.default_388')}" sortable="false" />
	<sjg:gridColumn name="" index="operation" title="%{getText('jsp.default_27')}" sortable="false" width="90" search="false" formatter="formatDAPendingChangesDivisionActionLinks"  hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	<sjg:gridColumn name="rejectReason" index="rejectReason" title="rejectReason" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="rejectedBy" index="rejectedBy" title="rejectedBy" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="status" index="status" title="status" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="groupPropertiesValue" index="GroupPropertiesValue" title="GroupPropertiesValue" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="viewEnable" index="ViewEnable" title="ViewEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="permissionEnable" index="PermissionEnable" title="PermissionEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="submitForApprovalEnable" index="SubmitForApprovalEnable" title="SubmitForApprovalEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="discardEnable" index="DiscardEnable" title="DiscardEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="modifyChangeEnable" index="ModifyChangeEnable" title="ModifyChangeEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="approveEnable" index="ApproveEnable" title="ApproveEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="rejectEnable" index="RejectEnable" title="RejectEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="editEnable" index="EditEnable" title="EditEnable" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="map.IsAdminOfDivision" index="IsAdminOfDivision" title="IsAdminOfDivision" sortable="false" width="0" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="map.CanAdminister" index="CanAdminister" title="CanAdminister" sortable="false" width="90" search="false" hidden="true" hidedlg="true"/>
</sjg:grid> 
<script>
	var fnCustomPendingDivisionRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#pendingDivisionChangesID").data('fnCustomRefresh', fnCustomPendingDivisionRefresh);
	
	$("#pendingDivisionChangesID").data("supportSearch",true);

	//disable sortable rows
	if(ns.common.isInitialized($("#pendingDivisionChangesID tbody"),"ui-sortable")){
		$("#pendingDivisionChangesID tbody").sortable("destroy");
	}

	$("#pendingDivisionChangesID").jqGrid('setColProp','ID',{title:false});
</script>

