<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
    <ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:help id="user_corpadminlocations" className="moduleHelpClass"/>
<s:include value="inc/corpadminlocations-pre.jsp"/>


<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" class="sectionsubhead">
			<s:text name="jsp.user_122"/>  <ffi:getProperty name='EditGroup_DivisionName'/>
		</td>
		<td align="right" valign="middle" class="columndata">
				 <select id="divisionId" class="txtbox" name="division" size="1" onchange="loadLocationData()">
					<ffi:cinclude value1="" value2="<ffi:getProperty name='EditGroup_DivisionName'/>" operator="notEquals">
						<option selected><ffi:getProperty name='EditGroup_DivisionName'/>
						</option>
					</ffi:cinclude>
				</select>
		</td>
	</tr>
</table> --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:setGridURL grid="GRID_adminLocation" name="DeleteURL" url="/cb/pages/jsp/user/corpadminlocdelete-verify.jsp?DeleteLocation_LocationBPWID={0}&DeleteLocation_LocationName={1}" parm0="LocationBPWID" parm1="LocationName"/>
	<ffi:setGridURL grid="GRID_adminLocation" name="EditURL" url="/cb/pages/jsp/user/corpadminlocedit.jsp?EditLocation_LocationBPWID={0}&EditDivision_GroupId=${EditDivision_GroupId}&editlocationreload=true&fromDivEdit=false" parm0="LocationBPWID"/>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setGridURL grid="GRID_adminLocation" name="DeleteURL" url="/cb/pages/jsp/user/corpadminlocdelete-verify.jsp?DeleteLocation_LocationBPWID={0}&DeleteLocation_LocationName={1}&childSequence={2}" parm0="LocationBPWID" parm1="LocationName" parm2="ChildSequenceNo"/>
	<ffi:setGridURL grid="GRID_adminLocation" name="EditURL" url="/cb/pages/jsp/user/corpadminlocedit.jsp?EditLocation_LocationBPWID={0}&EditDivision_GroupId=${EditDivision_GroupId}&editlocationreload=true&fromDivEdit=false&childSequence={1}" parm0="LocationBPWID" parm1="ChildSequenceNo"/>
</ffi:cinclude>

<ffi:setProperty name="adminLocationGridTempURL" value="getLocations.action?CollectionName=Locations&GridURLs=GRID_adminLocation" URLEncrypt="true"/>
<s:url namespace="/pages/user" id="remoteurl" action="%{#session.adminLocationGridTempURL}" escapeAmp="false"/>
<sjg:grid
		id="adminLocationGrid"
		caption=""
		sortable="true"  
		dataType="json"
		href="%{remoteurl}"
		pager="true"
		viewrecords="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		altRows="true"
		sortorder="asc"
		hiddengrid="true"
		sortname="LOCATION_NAME"
		shrinkToFit="true"
		navigator="true"
		navigatorSearch="false"
	   	navigatorAdd="false"
	   	navigatorDelete="false"
	  	navigatorEdit="false"
	  	navigatorRefresh="false"
		onGridCompleteTopics="addGridControlsEvents,shrinkLocGridToFit"
		>
	<sjg:gridColumn name="locationName" width="300" index="LOCATION_NAME" title="%{getText('jsp.default_283')}" sortable="true" cssClass="datagrid_textColumn"/>
	<sjg:gridColumn name="locationID" width="150" index="LOCATION_ID" title="%{getText('jsp.default_233')}" sortable="true" cssClass="datagrid_textColumn"/>
	<sjg:gridColumn name="map.ChildSequenceNo" index="ChildSequenceNo" title="%{getText('jsp.default_233')}" sortable="false" hidden="true" hidedlg="true" search="false" cssClass="datagrid_actionIcons"/>
	<sjg:gridColumn name="ID" index="Action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatLocationActionLinks" search="false" width="100" hidden="true" hidedlg="true" cssClass="__gridActionColumn" />
</sjg:grid>

<script type="text/javascript">
	var fnCustomLocationsRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#adminLocationGrid").data('fnCustomRefresh', fnCustomLocationsRefresh);

	$("#adminLocationGrid").data("supportSearch",true);
	$("#adminLocationGrid").jqGrid('setColProp','ID',{title:false});
</script>

<script type="text/javascript">
</script>
