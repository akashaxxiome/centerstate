<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadmininfo_da_pendingapproval" className="moduleHelpClass"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="Business"/>
	<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetDAItem" />
</ffi:cinclude>
<% session.setAttribute("FFIGetDAItem", session.getAttribute("GetDAItem")); %>

<ffi:setProperty name="daStatusPendingApproval" value="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL %>" />
<ffi:setProperty name="daStatusInProcess" value="<%=IDualApprovalConstants.STATUS_TYPE_INPROCESS %>" />
<ffi:setProperty name="daStatusRejected" value="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED %>" />

<ffi:cinclude value1="${DAItem.status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL %>" >
	<ffi:object id="CheckOBOForDA" name="com.ffusion.tasks.dualapproval.CheckOBOForDA" scope="request" />
	<ffi:setProperty name="CheckOBOForDA" property="DaItemType" value="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.ITEM_TYPE_BUSINESS%>" />
	<ffi:setProperty name="CheckOBOForDA" property="ItemSessionName" value="DAItem" />
	<ffi:process name="CheckOBOForDA" />					
</ffi:cinclude>


<ffi:object name="com.ffusion.tasks.dualapproval.ModifyDAItemStatus" id="ModifyDAItemStatus" />
<ffi:setProperty name="ModifyDAItemStatus" property="itemId" value="${Business.Id}"/>
<ffi:setProperty name="ModifyDAItemStatus" property="itemType" value="Business"/>
<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("ModifyDAItemStatus") ); %>



<ffi:setProperty name="pendingChangesTempURL" value="/pages/user/getCompanyPendingChangesAction.action?collectionName=DAItem&GridURLs=GRID_pendingChangesID" URLEncrypt="true"/>


<ffi:setProperty name="discardURLTxt" value="/cb/pages/jsp/user/da-verify-discard-business.jsp?itemId=${Business.Id}&itemType=Business" URLEncrypt="true"/>


<ffi:setProperty name="submitForApporvalURLTxt" value="/cb/pages/jsp/user/da-verify-submit-business.jsp?itemId=${Business.Id}&itemType=Business" URLEncrypt="true"/>  

<ffi:setProperty name="approveURLTxt" value="/cb/pages/jsp/user/da-business-verify.jsp?rejectFlag=n" URLEncrypt="true"/>  

<ffi:setProperty name="rejectURLTxt" value="/cb/pages/jsp/user/da-business-verify.jsp?rejectFlag=y" URLEncrypt="true"/>  

<ffi:setProperty name="modifyURLTxt" value="/cb/pages/jsp/user/da-modify-changes.jsp?itemId=${Business.Id}&itemType=Business&module=Business&daAction=modify" URLEncrypt="true"/>  



        <s:url id="pendingChangesUrl" value="%{#session.pendingChangesTempURL}" escapeAmp="false"/>
		<sjg:grid  
			id="pendingChangesID"  
			caption=""  
			dataType="json"  
			href="%{pendingChangesUrl}"  
			pager="true"  
			gridModel="gridModel" 
			rowList="%{#session.StdGridRowList}" 
			rowNum="%{#session.StdGridRowNum}" 
			rownumbers="true"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			sortable="true" 
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			onGridCompleteTopics="addGridControlsEvents,pendingDAChangesGridCompleteEvents,tabifyNotes"
			> 

		
			<sjg:gridColumn name="businessName" index="businessName" title="%{getText('jsp.default_283')}" sortable="true" />
			<sjg:gridColumn name="status" index="status" title="%{getText('jsp.default_388')}" sortable="false" />
			<sjg:gridColumn name="action" index="action" title="%{getText('jsp.default_27')}" sortable="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn" formatter="formatPendingChangesActionLinks"
			formatoptions="{discardURL : '%{#session.discardURLTxt}', submitForApporvalURL : '%{#session.submitForApporvalURLTxt}',
			approveURL : '%{#session.approveURLTxt}',
			rejectURL : '%{#session.rejectURLTxt}',
			modifyURL : '%{#session.modifyURLTxt}',
			canAdminister : '%{#session.Entitlement_CanAdminister}',
			daStatusPendingApproval : '%{#session.daStatusPendingApproval}' ,
			daStatusInProcess : '%{#session.daStatusInProcess}',
			daStatusRejected : '%{#session.daStatusRejected}',
			canApprove : '%{#request.CheckOBOForDA.canApprove}'
			}"
			search="false"/> 

			<sjg:gridColumn name="DAItemId" index="DAItemId" title="DAItemId" sortable="false" width="0" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="businessId" index="businessId" title="businessId" sortable="false" width="0" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="modifiedBy" index="modifiedBy" title="modifiedBy" sortable="false" width="0" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="createdBy" index="createdBy" title="createdBy" sortable="false" width="0" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="rejectReason" index="rejectReason" title="rejectReason" sortable="false" width="0" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="rejectedBy" index="rejectedBy" title="rejectedBy" sortable="false" width="0" hidden="true" hidedlg="true"/>


			</sjg:grid> 
		<script>
			
			var fnCustomPendingRefresh = function (event) {
				ns.common.forceReloadCurrentSummary();
			};
		
			$("#pendingChangesID").data('fnCustomRefresh', fnCustomPendingRefresh);
		
			$("#pendingChangesID").jqGrid('setColProp','DAItemId',{title:false});
		</script>



<ffi:removeProperty name="daStatusPendingApproval" />
<ffi:removeProperty name="daStatusInProcess" />
<ffi:removeProperty name="daStatusRejected" />
<ffi:removeProperty name="CheckOBOForDA" />
