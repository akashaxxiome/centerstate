<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:url id="pageurl" value="/pages/jsp/user/sessionActivityReportDialog.jsp" escapeAmp="false">

</s:url>
<sj:dialog
	id="sessionActivityDialogId"
	autoOpen="false"
	modal="false"
	title="%{getText('jsp.default_573')}"
	height="560"
	width="1050"
	href="%{pageurl}"
	showEffect="fold"
	hideEffect="clip"
	resizable="false"
	cssStyle="overflow-y:hidden"
>
</sj:dialog>