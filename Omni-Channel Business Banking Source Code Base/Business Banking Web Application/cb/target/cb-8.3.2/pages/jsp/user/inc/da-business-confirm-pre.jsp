<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%-- code added from da-verify-business-administrator.jsp starts --%>
<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
<ffi:process name="GetAdminsForGroup"/>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="TempBusinessEmployee"/>

<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
<ffi:process name="GenerateUserAndAdminLists"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="GenerateUserAndAdminLists"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
<ffi:process name="GenerateListsFromDAAdministrators"/>


<%--
</ffi:cinclude>
--%>


<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
<%-- code added from da-verify-business-administrator.jsp ends --%>


<%-- code added from da-business-confirm.jsp starts --%>
<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:removeProperty name="DISABLE_APPROVE"/>

<%-- BAI Export Settings --%>
<ffi:object name="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA" id="AddBAIExportSettingsToDA" scope="session" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="ReadFromDA" value="true" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="TmpBankIdDisplayText" value="${TempBankIdentifierDisplayText}" /> 
<ffi:process name="AddBAIExportSettingsToDA" />
<%-- END BAI Export Settings --%>

<%-- Dual Approval for Transaction Groups --%>
<ffi:object id="GetCompanyTransGroupsDA" name="com.ffusion.tasks.dualapproval.GetCompanyTransGroupsDA" />
<ffi:setProperty name="GetCompanyTransGroupsDA" property="ReadFromDA" value="true" />								
<ffi:process name="GetCompanyTransGroupsDA" />
<%-- END Dual Approval for Transaction Groups --%>



<%-- Update BAI settings if any --%>
<ffi:cinclude value1="${DA_BAIEXPORT_SETTINGS}" value2="" operator="notEquals">
	<ffi:setProperty name="AddBAIExportSettingsToDA" property="StoreInMaster" value="true" />
	<ffi:process name="AddBAIExportSettingsToDA" />
	<ffi:removeProperty name="AddBAIExportSettingsToDA" />
</ffi:cinclude>

<%-- Update Transaction Groups --%>
<ffi:cinclude value1="${DA_TRANSACTION_GROUPS}" value2="" operator="notEquals">
	<ffi:setProperty name="GetCompanyTransGroupsDA" property="StoreInMaster" value="true" />
	<ffi:process name="GetCompanyTransGroupsDA" />
	<ffi:removeProperty name="GetCompanyTransGroupsDA" />
</ffi:cinclude>

<ffi:setProperty name="EntitlementGroupParentId" value="${Business.EntitlementGroup.ParentId}" />

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	<ffi:object id="PopulateObjectWithDAValuesTask"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
	<ffi:setProperty name="PopulateObjectWithDAValuesTask" property="sessionNameOfEntity" value="Business"/>
	<ffi:process name="PopulateObjectWithDAValuesTask"/>
	
	<ffi:cinclude value1="${goback}" value2="true" operator="notEquals">
		<ffi:object id="ModifyBusiness" name="com.ffusion.tasks.user.ModifyBusiness" scope="session"/>
			<ffi:setProperty name="ModifyBusiness" property="Initialize" value="true" />
		<ffi:setProperty name="ModifyBusiness" property="Process" value="true"/>
		<ffi:process name="ModifyBusiness"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${goback}" value2="true" operator="equals">
		<ffi:cinclude value1="${ModifyBusiness}" value2="" operator="equals">
			<!-- ModifyBusiness not found - try getting a new one -->
			<ffi:object id="ModifyBusiness" name="com.ffusion.tasks.user.ModifyBusiness" scope="session"/>
				<ffi:setProperty name="ModifyBusiness" property="Initialize" value="true" />
			<ffi:setProperty name="ModifyBusiness" property="Process" value="true"/>
			<ffi:process name="ModifyBusiness"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:removeProperty name="goback"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${Business.Id}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="category" value="<%=IDualApprovalConstants.CATEGORY_BEAN %>" />
	<ffi:process name="ApprovePendingChangesTask"/>
</ffi:cinclude>

<%-- Approving Pending Account Configuration changes --%>
<ffi:cinclude value1="${APPROVE_ACCT_CONF}" value2="TRUE" operator="equals" >
	<ffi:removeProperty name="APPROVE_ACCT_CONF" />
	<ffi:object id="GetAccountsByBusinessEmployee" name="com.ffusion.tasks.accounts.GetAccountsByBusinessEmployee" scope="session"/>
		<ffi:setProperty name="GetAccountsByBusinessEmployee" property="AccountsName" value="BusEmpAccounts"/>
		<ffi:setProperty name="GetAccountsByBusinessEmployee" property="Reload" value="true"/>
	<ffi:process name="GetAccountsByBusinessEmployee"/>
	<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="BusEmpAccounts"/>
	<ffi:process name="SearchAcctsByNameNumType"/>
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetCategories" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ACCOUNT_CONFIGURATION %>"/>
	<ffi:process name="GetCategories"/>
	<ffi:list collection="categories" items="category">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="Business"/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="objectId" value="${category.objectId}"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
			<ffi:object name="com.ffusion.tasks.accounts.ModifyAccountById" id="ModifyAccount" scope="session" />
			<ffi:object name="com.ffusion.beans.Contact" id="AccountContact" scope="session" />
			<ffi:object name="com.ffusion.tasks.dualapproval.GetAccountConfigurationFromDA" id="GetAccountConfigurationFromDA" scope="session" />
				<ffi:setProperty name="GetAccountConfigurationFromDA" property="objectId" value="${category.objectId}"/>
				<ffi:setProperty name="GetAccountConfigurationFromDA" property="initOnly" value="true"/> 
			<ffi:process name="GetAccountConfigurationFromDA"/>
				<ffi:setProperty name="ModifyAccount" property="Init" value="true"/>
				<ffi:setProperty name="ModifyAccount" property="DirectoryId" value="${Business.Id}"/>
				<ffi:setProperty name="ModifyAccount" property="AccountCollection" value="FilteredAccounts"/>
			<ffi:process name="ModifyAccount"/>
				<ffi:setProperty name="GetAccountConfigurationFromDA" property="initOnly" value="false"/> 
			<ffi:process name="GetAccountConfigurationFromDA"/>
				<ffi:setProperty name="ModifyAccount" property="Process" value="true"/>
				<ffi:setProperty name="AccountContact" property="PreferredLanguage" value="${Business.PreferredLanguage}" />
			<ffi:process name="ModifyAccount"/>
		</ffi:cinclude>
	</ffi:list>
		
		<ffi:removeProperty name="GetAccountsByBusinessEmployee"/>
		<ffi:removeProperty name="SearchAcctsByNameNumType"/>
		<ffi:removeProperty name="GetCategories"/>
		<ffi:removeProperty name="GetDACategoryDetails"/>
		<ffi:removeProperty name="ModifyAccount"/>
</ffi:cinclude>
<ffi:cinclude value1="${isPermissionChanged}" value2="Y">
	<s:include value="da-user-approve-permission.jsp" />
	<ffi:removeProperty name="isPermissionChanged" />
</ffi:cinclude>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR %>"/>
<ffi:process name="GetDACategoryDetails"/>

<%-- Check if administrators are modified. If yes, then approve the administrator changes and save it into system tables. --%>
<%-- As the administrator who is approving the change may be getting removed, do adminstrators very last --%>
<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	<ffi:object name="com.ffusion.tasks.dualapproval.CreateAdministratorInputs" id="CreateAdministratorInputs" />
	<ffi:setProperty name="CreateAdministratorInputs" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="CreateAdministratorInputs" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="CreateAdministratorInputs" property="nonAdminGroups" value="NonAdminGroups"/>
	<ffi:setProperty name="CreateAdministratorInputs" property="nonAdminEmployees" value="NonAdminEmployees"/>
	<ffi:setProperty name="CreateAdministratorInputs" property="memberType" value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>"/>
	<ffi:setProperty name="CreateAdministratorInputs" property="memberSubType" value="<%= Integer.toString(SecureUser.TYPE_CUSTOMER) %>"/>
	<ffi:process name="CreateAdministratorInputs"/>

	<ffi:setProperty name="AutoEntitleAdministrators" value="true" />
	<%-- call SetAdministrators to do all the validation, with commit = false --%>
	<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
	<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
	<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
	<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
	<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
	<ffi:process name="SetAdministrators"/>

	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${Business.Id}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="category" value="<%=IDualApprovalConstants.CATEGORY_BEAN %>" />
	<ffi:process name="ApprovePendingChangesTask"/>

	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="GroupName" value="${SecureUser.BusinessName}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${Business.Id}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="TRUE" />
	<ffi:process name="ApprovePendingChangesTask" />
	<%-- now commit the administrator changes after everything else --%>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="true"/>
	<ffi:process name="SetAdministrators"/>

	<ffi:removeProperty name="AdminEmployees"/>
	<ffi:removeProperty name="AdminGroups"/>
	<ffi:removeProperty name="NonAdminGroups"/>
	<ffi:removeProperty name="NonAdminEmployees"/>
	<ffi:removeProperty name="userMembers"/>
	<ffi:removeProperty name="adminMembers"/>
	<ffi:removeProperty name="groupIds"/>
	<ffi:removeProperty name="CreateAdministratorInputs"/>
	<ffi:removeProperty name="SetAdministrators"/>
	<ffi:removeProperty name="AutoEntitleAdministrators"/>
</ffi:cinclude>
<%-- if there were administrator changes, the ApprovePendingChange for the business was called in the block above --%>
<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="GroupName" value="${SecureUser.BusinessName}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${Business.Id}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="TRUE" />
	<ffi:process name="ApprovePendingChangesTask" />
</ffi:cinclude>
<ffi:removeProperty name="EntitlementGroupParentId"/>
<ffi:removeProperty name="ApprovePendingChangesTask"/>
<%--
<ffi:include page="${PathExt}user/inc/da-session-cleanup.jsp"/>
--%>

<ffi:removeProperty name="DA_BAIEXPORT_SETTINGS" />
<ffi:removeProperty name="DA_TRANSACTION_GROUPS" />
<%-- code added from da-business-confirm.jsp ends --%>
