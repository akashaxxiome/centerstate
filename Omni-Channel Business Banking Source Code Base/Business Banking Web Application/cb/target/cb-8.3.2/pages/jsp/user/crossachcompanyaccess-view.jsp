<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<div id="crossachcompanyaccess-viewDiv" class="remoteXACHCompany" >
<ffi:help id="user_crossachcompanyaccess-view" className="moduleHelpClass"/>
<ffi:setProperty name="confirmation_done" value="false"/>

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<%-- Get the BusinessEmployee, whose permission is currently being edited --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemId" value="${Business.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		</ffi:cinclude>
	<ffi:process name="GetCategories"/>
	<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
		
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session" />
			<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session" />
		</ffi:cinclude>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
				<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
				<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
				<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
			</ffi:cinclude>
			<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
			</ffi:cinclude>
			
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditACHCompanyAccess" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="EditACHCompanyAccess" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="EditACHCompanyAccess" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	
    	<ffi:setProperty name="EditACHCompanyAccess" property="GroupId" value="${EditGroup_GroupId}"/>
    	<ffi:setProperty name="EditACHCompanyAccess" property="PrefixName" value="init"/>
	<ffi:setProperty name="EditACHCompanyAccess" property="EntsListSessionName" value="crossACHEntLists"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="EditACHCompanyAccess" property="ProfileId" value="${Business.Id}"/>
	</ffi:cinclude>
	
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- call the GetACHCompanyAccess task to put object in session for the correct display of the page --%>
<ffi:object id="GetACHCompanyAccess" name="com.ffusion.tasks.admin.GetACHCompanyAccess" scope="session"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
</ffi:cinclude>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
</s:if>
<ffi:setProperty name="GetACHCompanyAccess" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:setProperty name="GetACHCompanyAccess" property="EntsListSessionName" value="crossACHEntListsBeforeFilter"/>
<ffi:setProperty name="GetACHCompanyAccess" property="ParentChildName" value="ParentChildLists"/>
<ffi:setProperty name="GetACHCompanyAccess" property="ChildParentName" value="ChildParentLists"/>
<ffi:setProperty name="GetACHCompanyAccess" property="EntsLimitsSessionName" value="limitInfoList"/>
<ffi:process name="GetACHCompanyAccess"/>
<ffi:removeProperty name="GetACHCompanyAccess"/>

<%-- filter the entitlements so that those entitlements restricted by the BusinessAdmin entitlement group 
	(as well as its parents) would not be displayed, along with the restricted entitlements' control children --%>
<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="crossACHEntListsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="crossACHEntLists"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:removeProperty name="FilterEntitlementsForBusiness"/>

<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
</s:if>
<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:setProperty name="GetGroupLimits" property="ObjectType" value=""/>
<ffi:setProperty name="GetGroupLimits" property="ObjectId" value=""/>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>   

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object id="CheckEntitlementByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
	<ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
</s:if>

<ffi:object id="GetMaxACHCompanyLimits" name="com.ffusion.tasks.admin.GetMaxACHCompanyLimits" scope="session" />
<ffi:setProperty name="GetMaxACHCompanyLimits" property="LimitInfoListSessionName" value="limitInfoList"/>
<ffi:setProperty name="GetMaxACHCompanyLimits" property="PerTransactionMapName" value="perBatchMax"/>
<ffi:setProperty name="GetMaxACHCompanyLimits" property="PerDayMapName" value="dailyMax"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
</s:if>
<ffi:setProperty name="GetMaxACHCompanyLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:process name="GetMaxACHCompanyLimits"/>
<ffi:removeProperty name="GetMaxACHCompanyLimits"/>

<ffi:object id="GetMaxACHLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxACHLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxACHLimitForPeriod" property="PerTransactionMapName" value="perBatchMax"/>
<ffi:setProperty name="GetMaxACHLimitForPeriod" property="PerDayMapName" value="dailyMax"/>
<ffi:process name="GetMaxACHLimitForPeriod"/>
<ffi:setProperty name="GetMaxACHLimitForPeriod" property="NoLimitString" value="no"/>

<%-- this task need the perBatchMax and the dailyMax put into the session by the GetMaxACHCompanyLimits task --%>
<ffi:object id="CheckForRedundantACHLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantACHLimits" scope="session" />
<ffi:setProperty name="CheckForRedundantACHLimits" property="PerTransactionMapName" value="perBatchMax"/>
<ffi:setProperty name="CheckForRedundantACHLimits" property="PerDayMapName" value="dailyMax"/>
<ffi:setProperty name="CheckForRedundantACHLimits" property="LimitInfoListSessionName" value="limitInfoList"/>
<ffi:setProperty name="CheckForRedundantACHLimits" property="GroupId" value="${EditGroup_GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantACHLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>
<ffi:setProperty name="CheckForRedundantACHLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/crossachcompanyaccess-verify-init.jsp&PermissionsWizard=${PermissionsWizard}" URLEncrypt="true"/>

<ffi:setProperty name="CheckForRedundantACHLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/crossachcompanyaccess.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="true"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value=""/>
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value=""/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<s:if test="%{#session.Section == 'Users'}">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			</s:if>
			<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Business.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			</ffi:cinclude>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>" />
		<ffi:process name="GetDACategoryDetails" />
		
		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
		<ffi:process name="GetDACategoryDetails" />
		
		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_REQUIRE_APPROVAL%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>"/>
		<ffi:process name="GetDACategoryDetails" />
</ffi:cinclude>

<script language="JavaScript" type="text/javascript"><!--

var ach_ra_limits = new Array();

function initializeParentChild() {
	parentChildArray = new Array();
	childParentArray = new Array();
	
	// in this page, there are only two prefixes	
	prefixes = new Array( "init", "admin" );
		
<ffi:object id="counterMath" name="com.ffusion.beans.util.IntegerMath"/>		
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:setProperty name="counterMath" property="Value2" value="1"/>

<ffi:object id="prefixCounter" name="com.ffusion.beans.util.IntegerMath"/>
<ffi:setProperty name="prefixCounter" property="Value1" value="0"/>

		// Populate the parentChildArray
<ffi:list collection="ParentChildLists" items="childList">
		parentChildArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="childList" items="entry" startIndex="1" endIndex="1">
			prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="childList" items="entry" startIndex="2">
			, prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);

	<%-- Make the index of the prefixes array alternate between 0 and 1 --%>
	<ffi:setProperty name="prefixCounter" property="Value2" value="1"/>
	<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Add}"/>
	<ffi:setProperty name="prefixCounter" property="Value2" value="2"/>
	<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Modulo}"/>

	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />
</ffi:list>	

		// Populate the childParentArray
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:setProperty name="prefixCounter" property="Value1" value="0"/>

<ffi:list collection="ChildParentLists" items="parentList">
		childParentArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="parentList" items="entry" startIndex="1" endIndex="1">
			prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="parentList" items="entry" startIndex="2">
			, prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<%-- Make the index of the prefixes array alternate between 0 and 1 --%>
	<ffi:setProperty name="prefixCounter" property="Value2" value="1"/>
	<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Add}"/>
	<ffi:setProperty name="prefixCounter" property="Value2" value="2"/>
	<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Modulo}"/>
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />
</ffi:list>	

<ffi:removeProperty name="counterMath"/>
<ffi:removeProperty name="prefixCounter"/>
<ffi:removeProperty name="ParentChildLists"/>
<ffi:removeProperty name="ChildParentLists"/>
}

// --></script>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- Requires Approval Settings --%>
<ffi:object id="DASettings" name="com.ffusion.tasks.dualapproval.GetDASettings" />
<ffi:process name="DASettings" />
<ffi:removeProperty name="DASettings" />
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:removeProperty name="CheckRequiresApproval" />
	<ffi:object id="CheckRequiresApproval" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />
	<ffi:setProperty name="CheckRequiresApproval" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
	<ffi:setProperty name="CheckRequiresApproval" property="OpCounter" value="0" />
	<ffi:setProperty name="CheckRequiresApproval" property="DispPropListName" value="NonAccountEntitlementsMerged" />
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="CheckRequiresApproval" property="ModifyUserOnly" value="true" />
		<ffi:setProperty name="CheckRequiresApproval" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="CheckRequiresApproval" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="CheckRequiresApproval" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	
	<ffi:removeProperty name="EditRequiresApproval"/>	
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApprovalDA" />
		<ffi:setProperty name="EditRequiresApproval" property="approveAction" value="false" />
		<ffi:setProperty name="EditRequiresApproval" property="ModifyUserOnly" value="false" />
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
			<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${SecureUser.BusinessID}" />			
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${Entitlement_EntitlementGroup.GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
			<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${Entitlement_EntitlementGroup.GroupId}" />
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		<ffi:object id="EditRequiresApproval" name="com.ffusion.tasks.dualapproval.EditRequiresApproval" />
	</ffi:cinclude>

	<ffi:setProperty name="EditRequiresApproval" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="EditRequiresApproval" property="ModifyUserOnly" value="true" />
		<ffi:setProperty name="EditRequiresApproval" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="EditRequiresApproval" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="EditRequiresApproval" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
</ffi:cinclude>
<%-- END Requires Approval Settings --%>

<div align="center">
	<div class="errortext">
		<ul id="formerrors"></ul>
	</div>
	<div><ffi:getProperty name="Context"/></div>
	<div class="marginTop10 marginBottom10"></div>
		<s:form namespace="/pages/user" action="editCrossACHCompanyAccess-verify" theme="simple" method="post" name="EditAllACHCompanyForm" id="EditAllACHCompanyForm">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<div class="marginBottom10">
			<s:text name="jsp.user_4"/>
		    <s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/>
		    </s:if>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		    	<ffi:getProperty name="BusinessEmployee" property="Name"/>
		    </s:if>
		  </div>
			<div class="paneWrapper">
			  	<div class="paneInnerWrapper">
			  			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData permissionsTableSection">
						<%-- display the entitlements and limits if there is any --%>
						<ffi:cinclude value1="${crossACHEntLists.size}" value2="0" operator="notEquals">
							<tr class="header">
    							        <ffi:setProperty name="Compare" property="Value1" value="TRUE"/>
						                <ffi:setProperty name="Compare" property="Value2" value=""/>
								<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
							 	<td class="sectionsubhead" align="center" valign="middle">
			                                            <s:text name="jsp.user_32"/><br>
							  	    <ffi:setProperty name="LastRequest" property="Name" value="adminAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxValue" value="adminAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
								    <ffi:removeProperty name="adminAll"/>
							           <%--  <INPUT type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> onClick="setACHCheckBoxes(EditAllACHCompanyForm, 'adminAll', 'admin');"> --%>
							        </td>
							        </ffi:cinclude>
			                                        <td class="sectionsubhead" align="center" valign="middle">
			                                            <s:text name="jsp.user_177"/><br>
							 	    <ffi:setProperty name="LastRequest" property="Name" value="initAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxValue" value="initAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
								    <ffi:removeProperty name="initAll"/>
							            <%-- <INPUT type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> onClick="setACHCheckBoxes(EditAllACHCompanyForm, 'initAll', 'init');"> --%>
			                                        </td>
			                                        <td class="sectionsubhead" valign="middle" align="center">
			                                            <s:text name="jsp.user_346"/>
			                                        </td>
			                                       <%-- Requires Approval Column --%>
		                                        <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
		                                        	<td class="sectionsubhead" valign="top">
			                                        	<s:text name="jsp.user_278"/>
                                                                <input type="hidden" id="RequiresApprovalModified" name="RequiresApprovalModified" value="false" />
			                                        </td>
		                                        </ffi:cinclude>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
				<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
				<ffi:process name="GetLimitBaseCurrency" />
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
		                                    	</tr>

							<%-- set up the correct collectionName for use by the achcompanyaccess_rowmaker.jsp --%>
							<ffi:setProperty name="collectionName" value="crossACHEntLists"/>
							<ffi:setProperty name="ACHFormName" value="EditAllACHCompanyForm"/>	
                            <%-- Flag so that the included pages that actually display the limits and entitlements
                                 will include the javascript function that makes parent/child work --%>
                            <ffi:setProperty name="ParentChild" value="true"/>
                            <ffi:setProperty name="permissionsViewOnly" value="true"/>
							<s:include value="inc/achcompanyaccess_rowmaker.jsp"/>
							<ffi:removeProperty name="permissionsViewOnly"/>
							<ffi:removeProperty name="collectionName"/>
							<ffi:removeProperty name="ACHFormName"/>
                            <ffi:removeProperty name="ParentChild"/>

							<%-- <tr>
								<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
									<td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
								</ffi:cinclude>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
					                            <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
							</tr> --%>
							<tr>
								<td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top"><!-- <hr noshade size="1"> --></td>
							</tr>
							<tr>
								<td class="sectionsubhead" colspan="<ffi:getProperty name="NumTotalColumns"/>" align="center" valign="top">
									<sj:a 
										id="ACHCompanyCancel2"
										button="true"
										onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
										><s:text name="jsp.default_102"/></sj:a>
								</td>
							</tr>
						</ffi:cinclude>
						<ffi:cinclude value1="${crossACHEntLists.size}" value2="0" operator="equals">
							<tr>
								<td class="sectionhead" align="center" colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top">
								    <s:text name="jsp.user_4"/>
								    <s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
									<ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/>
								    </s:if>
    								<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
								    	<ffi:getProperty name="BusinessEmployee" property="Name"/>
								    </s:if>
								</td>
							</tr>
							<tr>
								<td class="columndata" align="center" valign="top">
									<s:text name="jsp.user_331"/>
							         	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
										<ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/>
								    	</s:if>
    								    <s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
								    		<ffi:getProperty name="BusinessEmployee" property="Name"/>
								    	</s:if>
								</td>
							</tr>
							<tr>
								<td class="sectionsubhead" align="center" valign="top">
									<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
								</td>
							</tr>
							
						</ffi:cinclude>
					<tr><td>&nbsp;</td></tr>
					</table></div></div>
					</s:form>
	</div>	
</div>
<ffi:removeProperty name="GetMaxACHLimitForPeriod"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="UseLastRequest"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:removeProperty name="CheckEntitlementByMember"/>
</s:if>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="GetGroupLimits"/>

<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/permissions.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/crossachcompanyaccess.jsp?UseLastRequest=TRUE" URLEncrypt="TRUE"/>
</ffi:cinclude>

<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
