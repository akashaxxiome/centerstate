<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:setProperty name="EditGroup_GroupId" value="${itemId}"/>
<ffi:cinclude value1="${Section}" value2="Company">
	<ffi:setProperty name="EditGroup_GroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
</ffi:cinclude>
<ffi:setProperty name="isPendingIsland" value="TRUE" />
<s:if test="%{#session.Section == 'Users'}">
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles">
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Division">
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${Section}" value2="Group">
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
</ffi:cinclude>
