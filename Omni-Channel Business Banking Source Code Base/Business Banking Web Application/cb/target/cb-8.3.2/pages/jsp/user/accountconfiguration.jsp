<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<span id="pageHeading" style="display:none">Account Configuration</span>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<div id="accountconfigurationDiv">
<s:include value="inc/corpadmininfo-pre.jsp" />
<s:include value="inc/accountconfiguration-pre.jsp"/>

<ffi:help id="user_corpadminaccountsedit" className="moduleHelpClass"/>

	<%-- Check Account Configuration categories in pending island  --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="Business"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetCategories" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ACCOUNT_CONFIGURATION %>"/>
		<ffi:process name="GetCategories"/>
		<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
			<ffi:setProperty name="highLightFieldDA" value="TRUE"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
		<s:include value="%{#session.PagesPath}user/corpadminaccountsedit.jsp?runSearch=" />
	</ffi:cinclude>
	<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
		<s:include value="%{#session.PagesPath}user/corpadminaccounts-view.jsp?runSearch=" />
	</ffi:cinclude>
</div>