<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div id="accountgroupaccessVerifyDiv" class="remoteAcctGroupAccess">
<ffi:help id="user_accountgroupaccess-verify" className="moduleHelpClass"/>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
	<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
	<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
	<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
    <ffi:setProperty name="GetGroupLimits" property="GroupId" value="${EditGroup_GroupId}"/>
    <ffi:setProperty name="GetGroupLimits" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
    <ffi:setProperty name="GetGroupLimits" property="ObjectType" value="AccountGroup"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
</ffi:cinclude>


<ffi:setProperty name='PageHeading' value='Verify Account Group Access'/>
<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request"/>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>

<ffi:object id="GetEntitlementTypePropertyList" name="com.ffusion.efs.tasks.entitlements.GetEntitlementTypePropertyList"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="ListName" value="Access"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="TypeToLookup" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
<ffi:process name="GetEntitlementTypePropertyList"/>

<%-- we need to do auto entitlement checking for company, division and group being modified --%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

	<%-- get the auto entitle settings for the business --%>
	<%-- assuming the correct Entitlement_EntitlementGroup is in session --%>
	<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
	<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="Entitlement_EntitlementGroup"/>
	<ffi:process name="GetCumulativeSettings"/>

</s:if>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">

<%-- get the auto entitle settings for the user being edited --%>
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetMember" id="GetMember" scope="session" />
<ffi:setProperty name="GetMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
<ffi:setProperty name="GetMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
<ffi:setProperty name="GetMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
<ffi:setProperty name="GetMember" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>

<ffi:process name="GetMember"/>
<ffi:removeProperty name="GetMember"/>

<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupMemberSessionKey" value="Entitlement_Group_Member"/>
<ffi:process name="GetCumulativeSettings"/>

<ffi:removeProperty name="Entitlement_Group_Member"/>

</s:if>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- set action start--%>
<% String action = "editAccountGroupAccessPermissions-execute";%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnableAccountGroups}" value2="true" operator="equals">
		<%-- Initialize -- make available number of granted accounts --%>
		<%-- (init only mode set for task) --%>
	<ffi:setProperty name="EditAccountGroupAccessPermissions" property="InitOnly" value="true"/>
	<ffi:process name="EditAccountGroupAccessPermissions"/>
		<ffi:cinclude value1="${EditAccountGroupAccessPermissions.NumGrantedAccountGroups}" value2="0" operator="notEquals">
			<% action = "editAccountGroupAccessPermissions-autoentitle";%>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>
<% 
	session.setAttribute("action",action);
	session.setAttribute("action_execute", "editAccountGroupAccessPermissions-execute");
%>
<%-- set action end--%>
<div class="marginTop20 marginleft5"><s:text name="jsp.user_260"/></div>
<div align="center" class="marginBottom20"><ffi:getProperty name="Context"/></div>
<s:form namespace="/pages/user" action="%{#session.action}" theme="simple" method="post" name="permLimitFrmVerify" id="permLimitFrmVerify">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="3"  class="tableData">
                                              <tr class="header">
                                              	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
											<td class="sectionsubhead" align="center">
												<s:text name="jsp.user_32"/>
											</td>
											</ffi:cinclude>
											<td class="sectionsubhead" align="center"><s:text name="jsp.user_177"/></td>
											<td class="sectionsubhead" align="left"><s:text name="jsp.default_17"/></td>
		<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
		<ffi:process name="GetLimitBaseCurrency" />
											<td class="sectionsubhead" align="left"><s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)</td>
											<td class="sectionsubhead" align="center">
												<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
													<s:text name="jsp.user_151"/> <s:text name="jsp.user_386"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
													&nbsp;
												</ffi:cinclude>
											</td>
											<td class="sectionsubhead" align="left"><s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)</td>
											<td class="sectionsubhead" align="center">
												<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
													<s:text name="jsp.user_151"/> <s:text name="jsp.user_386"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
													&nbsp;
												</ffi:cinclude>
											</td>
                                              </tr>
										
                                              <%  int checkboxCount = 0;  %>
										<% String checked = ""; %>
                                              <ffi:list collection="ParentsAccountGroups" items="acctGroup">
										<ffi:cinclude value1="${acctGroup.DisplayRow}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
										<ffi:setProperty name="hideEnt" value="false" />
												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
													<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="FALSE">
														<ffi:setProperty name="hideEnt" value="true" />
													</ffi:cinclude> 
												</ffi:cinclude>
											<ffi:cinclude value1="${hideEnt}" value2="false">
												<ffi:removeProperty name="hideEnt" />
												<tr>
													<% session.setAttribute( "checkboxCount", new Integer( checkboxCount ) ); %>
													<% String x = ""; %>
													<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
														<td class="columndata " valign="middle" rowspan="2">
															<ffi:cinclude value1="${acctGroup.CanAdminRow}" value2="TRUE" operator="equals">
																<ffi:getProperty name="admin${checkboxCount}" assignTo="x"/>
																<div align="center">
																	<input type="<ffi:getProperty name="AdminCheckBoxType"/>" disabled name="<ffi:getProperty name='admin${checkboxCount}'/>" value="<ffi:getProperty name="acctGroup" property="Id"/>" <ffi:cinclude value2="<%=x%>"  value1="${acctGroup.Id}" operator="equals"> checked </ffi:cinclude>  border="0">
																	<% pageContext.setAttribute("x", ""); %>
																</div>
															</ffi:cinclude>
															<ffi:cinclude value1="${acctGroup.CanAdminRow}" value2="TRUE" operator="notEquals">
																&nbsp;
															</ffi:cinclude>
														</td>
													</ffi:cinclude>
													
													<td class="columndata " valign="middle" rowspan="2">
														<% x = ""; %>
														<ffi:getProperty name="account_group${checkboxCount}" assignTo="x"/>
														  <div align="center">
														<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
																<ffi:process name="GetEntitlementsFromDA" />
															</ffi:cinclude>
															<input type="checkbox" disabled name="<ffi:getProperty name='account_group${checkboxCount}'/>" value="<ffi:getProperty name="acctGroup" property="Id"/>" <ffi:cinclude value2="<%=x%>"  value1="${acctGroup.Id}" operator="equals"> checked </ffi:cinclude>  border="0">
														</ffi:cinclude>
														<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="notEquals">
															&nbsp;
														</ffi:cinclude>
														<% pageContext.setAttribute("x", ""); %>
														</div>
													</td>
													<td width="150px;" style="max-width:180px;word-wrap:break-word;" class='<ffi:getPendingStyle fieldname="${acctGroup.Id}" defaultcss="columndata " 
															dacss="columndata  columndataDA" sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />' 
															align="left" rowspan="2">
														<ffi:getProperty name="acctGroup" property="Name"/> - <ffi:getProperty name="acctGroup" property="AcctGroupId"/>
													</td>
													<% String limitValue = ""; %>
													<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
														<td class="columndata " valign="middle" align="left">
															<ffi:getProperty name="transaction_limit${checkboxCount}" assignTo="limitValue"/>
															
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
																<ffi:removeProperty name="DARequest" />
																
																<ffi:setProperty name="GetGroupLimits" property="Period" value="1"/>
																<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${acctGroup.Id}"/>
																<ffi:process name="GetGroupLimits"/>
																<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="1"/>
																<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${acctGroup.Id}"/>
																<ffi:process name="GetLimitsFromDA" />
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_222"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_222"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_222"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="PerTransaction" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
																<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
																<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_244"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_244"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_244"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="PerTransaction" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
														</td>
														<td class="" valign="middle" align="center">
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
																					<% pageContext.setAttribute("checked", "false"); %>
																<ffi:getProperty name="transaction_exceed${checkboxCount}" assignTo="checked"/>
																<ffi:cinclude value1="${checked}" value2="true" operator="equals">
																	<input type="checkbox" checked disabled>
																</ffi:cinclude>
																<ffi:cinclude value1="${checked}" value2="true" operator="notEquals">
																	<input type="checkbox" disabled>
																</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																&nbsp;
															</ffi:cinclude>
														</td>
														<td class="columndata " valign="middle" align="left">
															<ffi:getProperty name="day_limit${checkboxCount}" assignTo="limitValue"/>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
																<ffi:setProperty name="GetGroupLimits" property="Period" value="2"/>
																<ffi:process name="GetGroupLimits"/>
																<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="2"/>
																<ffi:process name="GetLimitsFromDA" />
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_220"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_220"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_220"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="PerDay" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
																<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
																<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_242"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_242"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_242"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="PerDay" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
														</td>
														<td class="" valign="middle" align="center">
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
																					<% pageContext.setAttribute("checked", "false"); %>
																<ffi:getProperty name="day_exceed${checkboxCount}" assignTo="checked"/>
																<ffi:cinclude value1="${checked}" value2="true" operator="equals">
																	<input type="checkbox" checked disabled>
																</ffi:cinclude>
																<ffi:cinclude value1="${checked}" value2="true" operator="notEquals">
																	<input type="checkbox" disabled>
																</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																&nbsp;
															</ffi:cinclude>
														</td>
													</ffi:cinclude>
													<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="notEquals">
														<td class="" colspan="4" valign="middle" align="center">
															&nbsp;
														</td>
													</ffi:cinclude>
												</tr>
												<tr>
													<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
														<td class="columndata" valign="middle" align="left">
															<ffi:getProperty name="week_limit${checkboxCount}" assignTo="limitValue"/>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
																<ffi:setProperty name="GetGroupLimits" property="Period" value="3"/>
																<ffi:process name="GetGroupLimits"/>
																<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="3"/>
																<ffi:process name="GetLimitsFromDA" />
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_223"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_223"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_223"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="PerWeek" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
																<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
																<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_245"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_245"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_245"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="perWeek" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
														</td>
														<td valign="middle" align="center">
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
																					<% pageContext.setAttribute("checked", "false"); %>
																<ffi:getProperty name="week_exceed${checkboxCount}" assignTo="checked"/>
																<ffi:cinclude value1="${checked}" value2="true" operator="equals">
																	<input type="checkbox" checked disabled>
																</ffi:cinclude>
																<ffi:cinclude value1="${checked}" value2="true" operator="notEquals">
																	<input type="checkbox" disabled>
																</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																&nbsp;
															</ffi:cinclude>
														</td>
														<td class="columndata" valign="middle" align="left">
															<ffi:getProperty name="month_limit${checkboxCount}" assignTo="limitValue"/>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
																<ffi:setProperty name="GetGroupLimits" property="Period" value="4"/>
																<ffi:process name="GetGroupLimits"/>
																<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="4"/>
																<ffi:process name="GetLimitsFromDA" />
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_221"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_221"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_221"/></span>
																		<span class="sectionhead_greyDA">
																			<ffi:getProperty name="DARequest" property="PerMonth" />
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
																<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
																<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
																<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
																	<s:text name="jsp.user_243"/>
																</ffi:cinclude>
															<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
																<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="notEquals" >
																	<span class="columndata"><s:text name="jsp.user_243"/></span>
																</ffi:cinclude>
																<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="equals" >
																	<span class="columndataDA"><s:text name="jsp.user_243"/></span>
																		<span class="sectionhead_greyDA">
																			  <ffi:getProperty name="DARequest" property="PerMonth" />    
																		</span>
																</ffi:cinclude>
															</ffi:cinclude>
															</ffi:cinclude>
														</td>
														<td valign="middle" align="center">
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
																					<% pageContext.setAttribute("checked", "false"); %>
																<ffi:getProperty name="month_exceed${checkboxCount}" assignTo="checked"/>
																<ffi:cinclude value1="${checked}" value2="true" operator="equals">
																	<input type="checkbox" checked disabled>
																</ffi:cinclude>
																<ffi:cinclude value1="${checked}" value2="true" operator="notEquals">
																	<input type="checkbox" disabled>
																</ffi:cinclude>
															</ffi:cinclude>
															<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																&nbsp;
															</ffi:cinclude>
														</td>
													</ffi:cinclude>
												</tr>
											</ffi:cinclude>
											</ffi:cinclude>
                                                  <%  checkboxCount++;    %>
                                              </ffi:list>
</table>
<div class="btn-row">
<s:url id="inputUrl" value="%{#session.PagesPath}user/accountgroupaccess.jsp" escapeAmp="false">
<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
<s:param name="FromBack" value="%{'TRUE'}"></s:param>
<s:param name="UseLastRequest" value="%{'TRUE'}"></s:param>
</s:url>
<sj:a
	link="a6"
	href="%{inputUrl}"
	targets="Account_Groups"
	button="true"
	onCompleteTopics="backButtonTopic"
	><s:text name="jsp.default_57"/></sj:a>
<sj:a
	link="a5"
	button="true" 
	onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
	><s:text name="jsp.default_102"/></sj:a>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
<ffi:cinclude value1="${GetCumulativeSettings.EnableAccountGroups}" value2="true" operator="equals">						
<ffi:cinclude value1="${EditAccountGroupAccessPermissions.NumGrantedAccountGroups}" value2="0" operator="notEquals">
<ffi:setProperty name="perm_target" value="Account_Groups"/>
<ffi:setProperty name="completedTopics" value="completedAccountGroupsEdit"/>
<sj:a
	link="a4"
	formIds="permLimitFrmVerify"
	targets="Account_Groups"
	button="true" 
	validate="false"
	onBeforeTopics="beforeSubmit" 
	onErrorTopics="errorSubmit"
	><s:text name="jsp.default_111"/></sj:a>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg5')}" scope="request" />
<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg6')}" scope="request" />
<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg7')}" scope="request" />
<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
</ffi:cinclude>
<s:if test="%{#session.Section == 'Users'}">
<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg8')}" scope="request" />
<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg8')}" scope="request" />
<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
</ffi:cinclude>
<ffi:setProperty name="autoEntitleBackURL" value="${SecurePath}user/accountgroupaccess-verify.jsp?PermissionsWizard=${SavePermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
</ffi:cinclude>
<ffi:setProperty name="autoEntitleFormAction" value="${SuccessURL}"/>
</ffi:cinclude>
<ffi:cinclude value1="${EditAccountGroupAccessPermissions.NumGrantedAccountGroups}" value2="0" operator="equals">
<sj:a
	link="a3"
	formIds="permLimitFrmVerify"
	targets="resultmessage"
	button="true" 
	validate="false"
	onBeforeTopics="beforeSubmit" 
	onErrorTopics="errorSubmit"
	onSuccessTopics="completedAccountGroupsEdit,reloadCompanyApprovalGrid"
	><s:text name="jsp.default_366"/></sj:a>
</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${GetCumulativeSettings.EnableAccountGroups}" value2="false" operator="equals">
<sj:a
	link="a2"
	formIds="permLimitFrmVerify"
	targets="resultmessage"
	button="true" 
	validate="false"
	onBeforeTopics="beforeSubmit" 
	onErrorTopics="errorSubmit"
	onSuccessTopics="completedAccountGroupsEdit,reloadCompanyApprovalGrid"
	><s:text name="jsp.default_366"/></sj:a>
</ffi:cinclude>
</s:if>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
<sj:a
	link="a1"
	formIds="permLimitFrmVerify"
	targets="resultmessage"
	button="true" 
	validate="false"
	onBeforeTopics="beforeSubmit" 
	onErrorTopics="errorSubmit"
	onSuccessTopics="completedAccountGroupsEdit,reloadCompanyApprovalGrid"
	><s:text name="jsp.default_366"/></sj:a>
</s:if>
</div></div></div>
</s:form>
</div>
<ffi:removeProperty name="GetEntitlementTypePropertyList" />
<ffi:removeProperty name="Access" />
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="SavePermissionsWizard" />	
</div>