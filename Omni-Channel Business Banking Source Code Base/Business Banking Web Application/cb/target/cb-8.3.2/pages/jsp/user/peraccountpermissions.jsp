<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_peraccountpermissions" className="moduleHelpClass"/>
<script>
<!--
	$(function(){
		$("#accountTypeSelect").selectmenu({width: 138});
		
		if($.trim($('#hiddenAccountId').html()) != ''){
			var value = $('#hiddenAccountId').html();
			var label = $('#accountDisplayText').html();
			var optionDiv ="<option selected value="+value+">"+label+"</option>";
			$('#accountPermissionAccountID').html('');
			$('#accountPermissionAccountID').append(optionDiv);
		}
		 $("#accountPermissionAccountID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"accountPermissions",
			"size":"45",
			"source":"/cb/pages/common/EntitledAccountsLookupBoxAction.action"
		});
		accountSelect();
		$("#accountPermissionAccountID").change(accountSelect);
		
		$("#perAccountResetButtonId").button({
			icons: {
			}			
		});
		
		/* alert(<ffi:getProperty name="Account" property="ID"/>
		<ffi:getProperty name="Account" property="RoutingNum"/> : <ffi:getProperty name="Account" property="DisplayText"/>
		<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
		- <ffi:getProperty name="Account" property="NickName"/>
		</ffi:cinclude>	
		- <ffi:getProperty name="Account" property="CurrencyCode"/> );*/
	});
	
	function accountSelect() {
		//$('#currencyCode').html($('#accountPermissionAccountID :selected').attr("currencycode"));
		var selectedVal = $('#accountPermissionAccountID :selected').text();
		if(selectedVal!=null && selectedVal!="" && selectedVal!= "undefined"){
			var currency = selectedVal.split("-")[3];
			$('#currencyCode').html(currency);
			$("#perAccountSearch").trigger('click');
		} 
	}
-->
</script>
<% 
	request.setAttribute("runSearch", request.getParameter("runSearch"));
	request.setAttribute("FromBack", request.getParameter("FromBack"));
	request.setAttribute("UseLastRequest", request.getParameter("UseLastRequest"));
	request.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
	
	session.setAttribute("ComponentIndex", request.getParameter("ComponentIndex")); 
	session.setAttribute("perm_target", request.getParameter("perm_target"));
	session.setAttribute("completedTopics", request.getParameter("completedTopics"));
	if(request.getParameter("nextPermission") != null) {
		session.setAttribute("nextPermission", request.getParameter("nextPermission"));
	}
	if(request.getParameter("perAccountInit") != null){
		session.setAttribute("perAccountInit", request.getParameter("perAccountInit"));
	}
%>
<div id="<ffi:getProperty name="perm_target"/>">
<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
	<s:include value="inc/checkentitled-peraccountcomponents.jsp" />
</ffi:cinclude>
<%-- Synchronized block added for per request for a given session --%>
<ffi:synchronized>
<%-- Getting the name of component from the ArrayList that should be in session from previous
	 page, based on the index that is passed in through the url --%>
<%
	if (session.getAttribute("ComponentNamesPer")!=null) {
		int index = Integer.valueOf((String)(session.getAttribute("ComponentIndex"))).intValue();
		String ComponentName = (String)(((ArrayList)(session.getAttribute("ComponentNamesPer"))).get(index));
		session.setAttribute("ComponentName", ComponentName);
	}
%>

<ffi:cinclude value1="${runSearch}" value2="false" operator="equals">
<%
	request.setAttribute("EditGroup_AccountID", request.getParameter("EditGroup_AccountID"));
	session.setAttribute("EditGroup_AccountID", request.getParameter("EditGroup_AccountID"));
	request.setAttribute("EditGroup_GroupId", request.getParameter("EditGroup_GroupId"));
	request.setAttribute("SetBusinessEmployee.Id", request.getParameter("SetBusinessEmployee.Id"));
	request.setAttribute("EditGroup_GroupName", request.getParameter("EditGroup_GroupName"));
	request.setAttribute("EditGroup_Supervisor", request.getParameter("EditGroup_Supervisor"));
	request.setAttribute("EditGroup_DivisionName", request.getParameter("EditGroup_DivisionName"));
	request.setAttribute("EditGroup_Supervisor", request.getParameter("EditGroup_Supervisor"));
	request.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
%>
</ffi:cinclude>

<ffi:cinclude value1="${PermissionsWizard}" value2="TRUE" operator="notEquals">
	<ffi:removeProperty name="ComponentNamesPer" />
</ffi:cinclude>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_REQUIRE_APPROVAL %>"/>
</ffi:cinclude>

<%-- Set the page heading --%>
<ffi:removeProperty name="PageHeading2"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
<%--
<ffi:include page="${PathExt}user/inc/SetPageText.jsp" />
--%>
<ffi:setProperty name="PermissionsWizardSubMenu" value="TRUE"/>


<%-- Initialize Request --%>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>
<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>

<%-- get the entitlementgroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
	<ffi:process name="GetCategories"/>
	
	<%--We need to check account access from DA for GetAccountsForUser task --%>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="dependentDaEntitlements"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
	<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${ComponentName}"/>
	<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
					value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_ACCOUNT %>" />
	<ffi:process name="GetDACategorySubType"/>
	<ffi:removeProperty name="GetDACategorySubType"/>
	<%String catSubType = null; %>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${catSubType}" />
	<ffi:removeProperty name="catSubType"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.GetPendingObjectsByTypeSubType" id="getPendingObjects"/>
		<ffi:setProperty name="getPendingObjects" property="objectIdsSessionName" value="daObjectIds"/>
		<ffi:setProperty name="getPendingObjects" property="objectType" value="<%=EntitlementsDefines.ACCOUNT%>"/>
		<ffi:setProperty name="getPendingObjects" property="categorySubType" value="${Category_Sub_Session_Name}"/>
	<ffi:process name="getPendingObjects"/>
</ffi:cinclude>

<%-- Requires Approval Settings --%>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:removeProperty name="CheckRequiresApproval" />
	<ffi:removeProperty name="EditRequiresApproval"/>	
</ffi:cinclude>
<ffi:removeProperty name="REQUIRES_APPROVAL" />

<%-- 
Get Accounts for the user or group .
This task will only return accounts for which the user/group has 'Access' or 'Access (admin)' privileges, because the
CheckAdminAccess flag is set to true, and the CheckAccountGroupAccess flag is set to false.
--%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object name="com.ffusion.tasks.admin.GetAccountsForUser" id="GetAccountsForUser"/>
	<ffi:setProperty name="GetAccountsForUser" property="GroupID" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:setProperty name="GetAccountsForUser" property="UserType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetAccountsForUser" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="GetAccountsForUser" property="MemberID" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetAccountsForUser" property="GroupAccountsName" value="MyAccounts"/>
	<ffi:setProperty name="GetAccountsForUser" property="CheckAccountGroupAccess" value="false"/>
	<%-- for a DA business, everyone has admin access - just filter the account out if the user is not entitled for it --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:setProperty name="GetAccountsForUser" property="CheckAdminAccess" value="false"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		<ffi:setProperty name="GetAccountsForUser" property="CheckAdminAccess" value="true"/>
	</ffi:cinclude>
	<ffi:setProperty name="GetAccountsForUser" property="DaCategorySessionName" value="dependentDaEntitlements"/>
	<ffi:process name="GetAccountsForUser"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:object name="com.ffusion.tasks.admin.GetAccountsForGroup" id="GetAccountsForGroup"/>
	<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="MyAccounts"/>
	<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<%-- On the company page, since admin checkboxes are not shown, do not display accounts 
		for which the user has only "Access (admin)" --%>
		<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="false"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<%-- for a DA business, admin access is not relevant - just filter the account out if the division/group is not entitled for it --%>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="false"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:process name="GetAccountsForGroup"/>
</s:if>
<ffi:setProperty name="SetAccount" property="AccountsName" value="MyAccounts"/>
<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>

<ffi:cinclude value1="${AccountTypesList}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.ResourceList" id="AccountTypesList" scope="session"/>
		<ffi:setProperty name="AccountTypesList" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources"/>
		<ffi:setProperty name="AccountTypesList" property="ResourceID" value="AccountTypesList"/>
	<ffi:process name="AccountTypesList" />
	<ffi:getProperty name="AccountTypesList" property="Resource"/>
</ffi:cinclude>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<ffi:cinclude value1="${runSearch}" value2="">

	<ffi:getProperty name="Entitlement_Entitlements" property="Size"/>
		
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
					<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>
					<ffi:setProperty name="EditAccountPermissions" property="approveAction" value="false" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
			</ffi:cinclude>
		</s:if>
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
					<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>
					<ffi:setProperty name="EditAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
					<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
				</ffi:cinclude>
				<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditAccountPermissions" name="com.ffusion.tasks.admin.EditAccountPermissions" scope="session"/>
			</ffi:cinclude>
			<ffi:setProperty name="EditAccountPermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="EditAccountPermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="EditAccountPermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		</s:if>
		
		<ffi:setProperty name="EditAccountPermissions" property="AccountId" value="${EditGroup_AccountID}"/>
		<ffi:setProperty name="EditAccountPermissions" property="GroupId" value="${EditGroup_GroupId}"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditAccountPermissions" property="ProfileId" value="${Business.Id}"/>
		</ffi:cinclude>

		<ffi:setProperty name="EditAccountPermissions" property="EntitlementTypesWithLimits" value="AccountEntitlementsWithLimits"/>
		<ffi:setProperty name="EditAccountPermissions" property="EntitlementTypesWithoutLimits" value="AccountEntitlementsWithoutLimits"/>
		<ffi:setProperty name="EditAccountPermissions" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
		<ffi:setProperty name="EditAccountPermissions" property="EntitlementTypePropertyLists" value="LimitsList"/>
		<ffi:setProperty name="EditAccountPermissions" property="SaveLastRequest" value="false"/>

	<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session" />
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		</s:if>
		<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:process name="GetRestrictedEntitlements"/>
	<ffi:removeProperty name="GetRestrictedEntitlements"/>

	<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="MyAccounts"/>

	<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="ComponentValue" value="${ComponentName}"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
		</ffi:cinclude>  
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
		</s:if>
		<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="AccountMerged" value="AccountEntitlementsMergedBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
	<ffi:process name="GetTypesForEditingLimits"/>
	<ffi:removeProperty name="GetTypesForEditingLimits"/>

	<ffi:process name="SearchAcctsByNameNumType" />
</ffi:cinclude>

<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<%
		String nickName = request.getParameter("SearchAcctsByNameNumType.NickName");
		String number = request.getParameter("SearchAcctsByNameNumType.Number");
		String type = request.getParameter("SearchAcctsByNameNumType.Type");
		String id = request.getParameter("SearchAcctsByNameNumType.Id");
	%>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="MyAccounts"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="NickName" value="<%=nickName%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Number" value="<%=number%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Type" value="<%=type%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=id%>"/>
	<ffi:process name="SearchAcctsByNameNumType" />
</ffi:cinclude>

							<ffi:cinclude value1="" value2="${FilteredAccounts}" operator="notEquals">
								<ffi:setProperty name="FilteredAccounts" property="SortedBy" value='<%= com.ffusion.beans.accounts.Account.ROUTINGNUM + "," + com.ffusion.beans.accounts.Account.ID %>'/>
							</ffi:cinclude>

<%-- Only include accounts that the currently logged in user has the 'Access (admin)' entitlement for non-da or 'Access' for DA --%>
<ffi:object id="AccountEntitlementFilter" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask"/>
<ffi:setProperty name="AccountEntitlementFilter" property="AccountsName" value="FilteredAccounts"/>
<ffi:setProperty name="AccountEntitlementFilter" property="FilteredAccountsName" value="EntitledAccounts"/>
<ffi:setProperty name="AccountEntitlementFilter" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS_ADMIN %>"/>
<ffi:setProperty name="AccountEntitlementFilter" property="CheckAccountGroupEntitlment" value="false"/>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="AccountEntitlementFilter" property="CheckAccessEntitlement" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:setProperty name="AccountEntitlementFilter" property="CheckAccessEntitlement" value="false"/>
</ffi:cinclude>
<ffi:process name="AccountEntitlementFilter"/>
<ffi:removeProperty name="AccountEntitlementFilter"/>


<ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="MyAccounts"/>
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
<ffi:process name="SetAccountsCollectionDisplayText"/>
<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>

<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="MyAccounts"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.ENTITLED_ACCOUNTS_INDEX %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
									
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="MyAccounts"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.ENTITLED_ACCOUNTS_INDEX_BY_TYPE %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="MyAccounts"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.ENTITLED_ACCOUNTS_INDEX_BY_CURRENCY %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>


<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
	<ffi:cinclude value1="${perAccountInit}" value2="true" operator="equals">
		<ffi:list collection="EntitledAccounts" items="AccountItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="EditGroup_AccountID" value="${AccountItem.ID}"/>
		</ffi:list>
	</ffi:cinclude>
	<ffi:cinclude value1="${EditGroup_AccountID}" value2="" operator="equals">
		<ffi:list collection="EntitledAccounts" items="AccountItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="EditGroup_AccountID" value="${AccountItem.ID}"/>
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<ffi:list collection="EntitledAccounts" items="AccountItem" startIndex="1" endIndex="1">
		<ffi:setProperty name="EditGroup_AccountID" value="${AccountItem.ID}"/>
	</ffi:list>

</ffi:cinclude>

<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="notEquals">
	<ffi:cinclude value1="${EditGroup_AccountID}" value2="" operator="notEquals">
		<ffi:setProperty name="SetAccount" property="ID" value="${EditGroup_AccountID}"/>
		<ffi:process name="SetAccount"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="CheckOperationEntitlement" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>
<ffi:setProperty name="SearchResultsPage" value="/pages/jsp/user/peraccountpermissions.jsp"/>


<%-- set variables used to interchange row colors --%>

<%-- Figure out display settings for the account search tables --%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.ACCOUNT %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="accountDisplaySize"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>
<ffi:setProperty name="numAccounts" value="${MyAccounts.Size}"/>
<%
	int accountDisplaySize = Integer.parseInt( (String)session.getAttribute("accountDisplaySize") );
	int numAccounts = Integer.parseInt( (String)session.getAttribute("numAccounts") );
	session.setAttribute( "showSearch", "FALSE" );
	if (numAccounts > accountDisplaySize) {
		session.setAttribute( "showSearch", "TRUE" );
	}
	accountDisplaySize = (numAccounts < accountDisplaySize)? numAccounts : accountDisplaySize;
	session.setAttribute( "accountDisplaySize", String.valueOf( accountDisplaySize) );
%>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>	 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="notEquals">
			<ffi:setProperty name="SetAccount" property="ID" value="${EditGroup_AccountID}"/>
			<ffi:process name="SetAccount"/>

			<ffi:setProperty name="GetEntitlementObjectID" property="AccountAttributeName" value="Account"/>
			<ffi:process name="GetEntitlementObjectID"/>

			<%-- filter the entitlements so that those entitlements restricted by the BusinessAdmin entitlement group 
				(as well as its parents) would not be displayed, along with the restricted entitlements' control children --%>
			<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsMergedBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsMerged"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${GetEntitlementObjectID.EntitlementObjectID}"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithoutLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsWithLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsWithoutLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:removeProperty name="FilterEntitlementsForBusiness"/>
			
			<ffi:object id="HandleParentChildDisplay" name="com.ffusion.tasks.admin.HandleParentChildDisplay"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="EntitlementTypePropertyLists" value="AccountEntitlementsMerged"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="PrefixName" value="entitlement"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="AdminPrefixName" value="admin"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="ParentChildName" value="ParentChildLists"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="ChildParentName" value="ChildParentLists"/>
			<ffi:process name="HandleParentChildDisplay"/>

			<ffi:object name="com.ffusion.tasks.admin.GetMaxPerAccountLimits" id="GetMaxPerAccountLimits" scope="session"/>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="EntitlementTypePropertyListsName" value="AccountEntitlementsWithLimits"/>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="AccountId" value="${GetEntitlementObjectID.EntitlementObjectID}"/>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			    <ffi:setProperty name="GetMaxPerAccountLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			    <ffi:setProperty name="GetMaxPerAccountLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			    <ffi:setProperty name="GetMaxPerAccountLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			</s:if>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="PerDayMapName" value="PerDayLimits"/>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="PerWeekMapName" value="PerWeekLimits"/>
			<ffi:setProperty name="GetMaxPerAccountLimits" property="PerMonthMapName" value="PerMonthLimits"/>
			<ffi:process name="GetMaxPerAccountLimits"/>
			<ffi:removeProperty name="GetMaxPerAccountLimits"/>

			<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
			<ffi:process name="GetMaxLimitForPeriod"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

			<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
			<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="AccountEntitlementsWithLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MergedListName" value="AccountEntitlementsMerged"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/peraccountpermissions-verify-init.jsp?PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/peraccountpermissions.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="ObjectId" value="${GetEntitlementObjectID.EntitlementObjectID}"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			    <ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
				<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			</s:if>

			<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
				<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
				<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			</s:if>
			<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<ffi:setProperty name="GetGroupLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
			<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${GetEntitlementObjectID.EntitlementObjectID}"/>
			
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${GetEntitlementObjectID.EntitlementObjectID}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
				<ffi:process name="GetDACategoryDetails" />
				
				<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
				<ffi:process name="GetDACategoryDetails" />
			</ffi:cinclude>
			
</ffi:cinclude>

<%-- Requires Approval Processing for Per Account Permissions --%>
<ffi:removeProperty name="CheckRequiresApprovalPerAcct" />
<ffi:object id="CheckRequiresApprovalPerAcct" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="OpCounter" value="0" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="DispPropListName" value="NonAccountEntitlementsMerged" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="ModifyUserOnly" value="true" />
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>

<% String heading = null; %>
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/peraccountpermissions.jsp-1" parm0="${ComponentName}" assignTo="heading"/>
</ffi:cinclude>
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/peraccountpermissions-view.jsp-1" parm0="${ComponentName}" assignTo="heading"/>
</ffi:cinclude>
<ffi:setProperty name="PageHeading" value="<%=heading %>" />

<% session.setAttribute("FFIEditAccountPermissions", session.getAttribute("EditAccountPermissions")); %>

	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
		<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/peraccountpermissions.jsp-1" parm0="${ComponentName}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
		<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/peraccountpermissions-view.jsp-1" parm0="${ComponentName}"/>
	</ffi:cinclude>
	
<script type="text/javascript"><!--

	(function($){
		initializeParentChild();
	})(jQuery);
	
	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].checked = value;
			}
		}
	}
	
	function initializeParentChild() {
		parentChildArray = new Array();
		childParentArray = new Array();
		
		prefixes = new Array( "<ffi:getProperty name="HandleParentChildDisplay" property="PrefixName"/>","<ffi:getProperty name="HandleParentChildDisplay" property="AdminPrefixName"/>" );
		
<ffi:object id="counterMath" name="com.ffusion.beans.util.IntegerMath"/>		
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:setProperty name="counterMath" property="Value2" value="1"/>

		// Populate the parentChildArray
<ffi:list collection="ParentChildLists" items="childList">
		parentChildArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="childList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="childList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />	
</ffi:list>	

		// Populate the childParentArray
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:list collection="ChildParentLists" items="parentList">
		childParentArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="parentList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="parentList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />	
</ffi:list>	
	}
	
	ns.admin.nextPermissionForPerAcct = function ()
	{
		<ffi:cinclude value1="${ComponentIndex}" value2="${pcount}" operator="notEquals">
			ns.admin.nextPermission('#<ffi:getProperty name="nextPermission"/>','#perAcctTabs')
		</ffi:cinclude>
		<ffi:cinclude value1="${ComponentIndex}" value2="${pcount}" operator="equals">
			ns.admin.nextPermission('#<ffi:getProperty name="nextPermission"/>','#permTabs')
		</ffi:cinclude>
		
	}
	
	ns.admin.resetFormForPerAcct = function ()
	{
		$("#companyPermissionsLink").click();
	}
// --></script>

<s:include value="inc/parentchild_js.jsp"/>

<s:set name="action_execute" value="%{'editAccountPermissions-execute'}" scope="request"/>
<div align="center">
<div class="errortext"><ul id="formerrors"></ul></div>
<div align="center" class="marginTop10"><ffi:getProperty name="Context"/></div>
			<s:form id="perAcctSearchFrm" name="perAcctSearchFrm" namespace="/pages/user" action="accountAccess" validate="false" theme="simple" method="post">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="runSearch" value="true">
			<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
			<input type="hidden" name="ComponentIndex" value="<ffi:getProperty name="ComponentIndex"/>">
			<input type="hidden" name="perm_target" value="<ffi:getProperty name="perm_target"/>">
			<input type="hidden" name="completedTopics" value="<ffi:getProperty name="completedTopics"/>">
			<input type="hidden" name="nextPermission" value="<ffi:getProperty name="nextPermission"/>">
			<table width="100%" border="0" class="tableData">	
				<%-- <tr>
					<td colspan="2" class="sectionsubhead" height="20"><s:text name="jsp.default_548"/></td>
				</tr> --%>
											<%-- <tr>
												<td colspan="2" class="columndata">&nbsp;
													<s:text name="jsp.user_341"/>
												</td>
											</tr> --%>
											<%-- <tr>
												<td class="sectionsubhead" height="20" width="50%" align="right"><s:text name="jsp.default_294"/></td>
												<td class="sectionsubhead">
													<input id="nickName" class="ui-widget-content ui-corner-all" type="text" name="SearchAcctsByNameNumType.NickName" style="width:100px" size="17" maxlength="40" class="ui-widget-content ui-corner-all">
												</td>
											</tr> --%>
							<tr>
								<td class="sectionsubhead" width="6%"><s:text name="jsp.default_16"/></td>
								<td class="sectionsubhead">
									<div class="selectBoxHolder">
										<select id="accountPermissionAccountID" class="txtbox" name="SearchAcctsByNameNumType.Id" size="1">
						                    <ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="" operator="notEquals">
						                    	<ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="-1" operator="notEquals">
													<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
													<ffi:setProperty name="SetAccount" property="AccountsName" value="EntitledAccounts"/>
													<ffi:setProperty name="SetAccount" property="ID" value="${SearchAcctsByNameNumType.Id}"/>
													<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
													<ffi:process name="SetAccount"/>
													
													<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
													<ffi:setProperty name="AccountDisplayTextTask" property="AccountSessionName" value="SelectedAccount"/>
													<ffi:setProperty name="AccountDisplayTextTask" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
													<ffi:process name="AccountDisplayTextTask"/>
													<ffi:setProperty name="entitledAccDisText" value="${AccountDisplayTextTask.AccountDisplayText}" />
												</ffi:cinclude>
											</ffi:cinclude>
						                </select>
						            </div>
								</td>
								<td class="sectionsubhead">
									<sj:a
										id="perAccountSearch"
										cssStyle="display:none;"
										formIds="perAcctSearchFrm"
										targets="%{#session.perm_target}"
										button="true"
										validate="false"
										value="SEARCH"
										onCompleteTopics="onCompletePerAccountTopic"
										><span class="sapUiIconCls icon-sys-find"></span><s:text name="jsp.default_373"/></sj:a>
								</td>
							</tr>
											<%-- <tr>
												<td class="sectionsubhead" align="right"><s:text name="jsp.default_20"/></td>
												<td class="sectionsubhead">
													<select id="accountTypeSelect" name="SearchAcctsByNameNumType.Type" class="txtbox">
														<option value="-1"><s:text name="jsp.user_563"/></option>
														<ffi:list collection="AccountTypesList" items="AccountType">
															<option value="<ffi:getProperty name="AccountType"/>" <ffi:cinclude value1="${AccountType}" value2="${SearchAcctsByNameNumType.Type}">selected</ffi:cinclude>><ffi:getProperty name="AccountType"/></option>
														</ffi:list>
													</select>
												</td>
											</tr> --%>
							<%-- <tr>
								<td class="sectionsubhead" colspan="2" height="20"  align="center">
									<sj:a
										id="perAccountSearch"
										formIds="perAcctSearchFrm"
										targets="%{#session.perm_target}"
										button="true"
										validate="false"
										value="SEARCH"
										onCompleteTopics="onCompletePerAccountTopic"
										><span class="sapUiIconCls icon-sys-find"></span><s:text name="jsp.default_373"/></sj:a>
								</td>
							</tr> --%>
						</table>
					</s:form>
					<table border="0" cellspacing="0" cellpadding="3" >
						<tr>
							<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="equals">
								<td class="adminBackground" align="center">
									<span class="sectionsubhead">
										<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
											<s:text name="jsp.user_326"/><br><br>
										</ffi:cinclude>
										<ffi:cinclude value1="${runSearch}" value2="false" operator="equals">
											<s:text name="jsp.user_326"/><br><br>
										</ffi:cinclude>
										<ffi:cinclude value1="${runSearch}" value2="" operator="notEquals">
											&nbsp;<s:text name="jsp.user_207"/>
										</ffi:cinclude>
                                        <sj:a
                                            button="true"
                                            onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
                                            
                                        <sj:a
											id="%{#attr.nextPermission}"
											button="true" 
											onclick="ns.admin.nextPermissionForPerAcct();"
											><s:text name="jsp.user_203"/></sj:a>
									</span>
								</td>
							</ffi:cinclude>
							
							<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="notEquals">
								<ffi:setProperty name="EntitledAccounts" property="SortedBy" value="<%= com.ffusion.beans.accounts.Account.ROUTINGNUM + ',' + com.ffusion.beans.accounts.Account.ID %>" />
								
								<td class="adminBackground">
									
  								<s:form id="perAcctAccountsFrm" name="perAcctAccountsFrm" namespace="/pages/user" action="accountAccess" validate="false" theme="simple" method="post">
									<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
  									<input type="hidden" name="runSearch" value="false">
  									<input type="hidden" name="Section" value="<ffi:getProperty name="Section"/>">
  									<input type="hidden" name="EditGroup_GroupId" value="<ffi:getProperty name="EditGroup_GroupId"/>">
  									<input type="hidden" name="SetBusinessEmployee.Id" value="<ffi:getProperty name="BusinessEmployee" property="Id"/>">
  									<input type="hidden" name="EditGroup_GroupName" value="<ffi:getProperty name="EditGroup_GroupName"/>">
 									<input type="hidden" name="EditGroup_Supervisor" value="<ffi:getProperty name="EditGroup_Supervisor"/>">
  									<input type="hidden" name="EditGroup_DivisionName" value="<ffi:getProperty name="EditGroup_DivisionName"/>">
  									<input type="hidden" name="EditGroup_Supervisor" value="<ffi:getProperty name="EditGroup_Supervisor"/>">
  									<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">  															
									<input type="hidden" name="ComponentIndex" value="<ffi:getProperty name="ComponentIndex"/>">
									<input type="hidden" name="perm_target" value="<ffi:getProperty name="perm_target"/>">
									<input type="hidden" name="completedTopics" value="<ffi:getProperty name="completedTopics"/>">
									<input type="hidden" name="nextPermission" value="<ffi:getProperty name="nextPermission"/>">
									
									<%-- <table border="0">
									 <tr>
											<td class="columndata" height="20" align="left"><s:text name="jsp.user_285"/></td>
										</tr>
										<tr>
											<td align="center" class="sectionsubhead">&nbsp;&nbsp;&nbsp;
												<select name="EditGroup_AccountID" class="ui-widget-content ui-corner-all" size="<ffi:getProperty name="accountDisplaySize"/>">
													<ffi:list collection="EntitledAccounts" items="AccountItem">
														<% String acctId, rtn, acctIdWithRtn;
														%>
														<ffi:getProperty name="AccountItem" property="ID" assignTo="acctId"/>
														<ffi:getProperty name="AccountItem" property="RoutingNum" assignTo="rtn"/>
														<% acctIdWithRtn = rtn + "-" + acctId;%>
														<option value="<ffi:getProperty name="AccountItem" property="ID"/>"
															<ffi:list collection="daObjectIds" items="daAccountId">
																<ffi:cinclude value1="${daAccountId}" value2="<%=acctIdWithRtn%>" operator="equals"> class="columndataDA"</ffi:cinclude>
															</ffi:list>
															>
															<ffi:cinclude value1="${AccountItem.RoutingNum}" value2="" operator="notEquals"><ffi:getProperty name="AccountItem" property="RoutingNum"/>:</ffi:cinclude>
															<ffi:getProperty name="AccountItem" property="DisplayText"/> -
															<ffi:cinclude value1="${AccountItem.NickName}" value2="" operator="notEquals" >
																<ffi:getProperty name="AccountItem" property="NickName"/> -
													 		</ffi:cinclude>																
															<ffi:getProperty name="AccountItem" property="CurrencyCode"/>
														</option>
													</ffi:list>
												</select>
											</td>
										</tr>
										<tr>
											<td class="sectionsubhead" colspan="2" height="20" align="center">
													<sj:a
														id="viewPerAccount"
														formIds="perAcctAccountsFrm"
														targets="%{#session.perm_target}"
														button="true"
														validate="false"
														onCompleteTopics="onCompletePerAccountTopic"
														><s:text name="jsp.default_459"/></sj:a>
											</td>
										</tr>
									</table> --%>
								</s:form>
								
							</td>
						</ffi:cinclude>
						</tr>
					</table>
<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="notEquals">
<s:form id="permLimitFrm" name="permLimitFrm" namespace="/pages/user" action="editAccountPermissions-verify"  validate="false" theme="simple" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="EditGroup_AccountID" value='<ffi:getProperty name="EditGroup_AccountID"/>'>
<div id="perAccountName" align="center" class="marginTop10 marginBottom20 hidden">
       <s:text name="jsp.default_21"/>:
       <span id="hiddenAccountId">
      <ffi:getProperty name="Account" property="ID"/>
      </span>
      <span id="accountDisplayText">
<ffi:getProperty name="Account" property="RoutingNum"/> : <ffi:getProperty name="Account" property="DisplayText"/>
<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
- <ffi:getProperty name="Account" property="NickName"/>
</ffi:cinclude>	
- <ffi:getProperty name="Account" property="CurrencyCode"/>
</span>
</div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminBackground tableData">
                                   <tr class="header">
                                       <ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
                              <td valign="middle" align="center" nowrap class="sectionsubhead">
                                  <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
								<input type="checkbox" value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'admin');"><s:text name="jsp.user_32" />
							</ffi:cinclude>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'admin');">
							</ffi:cinclude>
								</td>
						</ffi:cinclude>
                              <td valign="middle" align="center" nowrap class="sectionsubhead">
                                  <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
								<input type="checkbox" value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'entitlement');"><s:text name="jsp.user_177" />
							</ffi:cinclude>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'entitlement');">
							</ffi:cinclude>
                              </td>
                                       <td valign="middle" nowrap class="sectionsubhead">
                                           <s:text name="jsp.user_346" />
                                       </td>
                                       <td colspan="2" valign="middle" align="center" nowrap class="sectionsubhead">
							<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
							<ffi:process name="GetLimitBaseCurrency" />
                                           <s:text name="jsp.default_261" /> (<s:text name="jsp.user_173" /> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                       </td>
                                       <td valign="middle" align="center" nowrap class="sectionsubhead">
							<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                               <s:text name="jsp.user_153" />
							</ffi:cinclude>
							<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
								&nbsp;
							</ffi:cinclude>
                                       </td>
                                       <td colspan="2" valign="middle" align="center" nowrap class="sectionsubhead">
                                           <s:text name="jsp.default_261" /> (<s:text name="jsp.user_173" /> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                       </td>
                                       <td valign="middle" align="center" nowrap class="sectionsubhead">
							<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                   <s:text name="jsp.user_153" />
							</ffi:cinclude>
							<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									&nbsp;
							</ffi:cinclude>
                                       </td>
                                   </tr>
					<%-- Flag so that the included pages that actually display the limits and entitlements
						will include the javascript function that makes parent/child work --%>
					<ffi:setProperty name="ParentChild" value="true"/>

					<%-- Flag to determine if the admin column should be drawn --%>
					<ffi:setProperty name="HasAdmin" value="true"/>

					<%-- Flag to indicate that the per account page is being displayed (and that cross account entitlements should be checked --%>
					<ffi:setProperty name="PerAccount" value="true"/>
					<%
						session.setAttribute( "limitIndex", new Integer( 0 ) );
						session.setAttribute( "LimitsList", session.getAttribute("AccountEntitlementsMerged") );
					%>
					<ffi:setProperty name="DisplayedLimit" value="FALSE"/>
					<ffi:setProperty name="CheckEntitlementObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
					<ffi:setProperty name="CheckEntitlementObjectId" value="${GetEntitlementObjectID.EntitlementObjectID}"/>
					<ffi:flush/>

					<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="notEquals">
						<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
							<s:include value="inc/corpadminuserlimit_merged.jsp"/>
						</s:if>
						<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
							<s:include value="inc/corpadmingrouplimit_merged.jsp"/>
						</s:if>
					</ffi:cinclude>

					<ffi:cinclude value1="${EntitledAccounts.size}" value2="0" operator="notEquals">
					<ffi:cinclude value1="${DisplayedLimit}" value2="FALSE" operator="equals">
                          <tr>
                              <td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="middle" align="center" style="padding-top:8px;">
                                  <span class="columndata"><s:text name="jsp.user.NoEditFeatureOrLimitForAccnt" /></span>
                              </td>
                          </tr>
					</ffi:cinclude>

                      <%--  <tr>
                           <ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                           </ffi:cinclude>
						<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                                    <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
                           </ffi:cinclude>
                       </tr> --%>
				</table>
		<div class="marginTop10"></div>
		<div align="center" class="btn-row" style="margin-bottom:25px !important;">
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">	
					<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="notEquals">
								<%-- <a  id="perAccountResetButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
									title='<s:text name="jsp.default_358.1" />' 
									href='#'														
									onClick='ns.admin.resetFormForPerAcct(); removeValidationErrors();'>
									<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358" /></span>
								</a> --%>
								
								<a  id="perAccountResetButtonId"  
									title='<s:text name="jsp.default_358.1" />' 
									href='#'														
									onClick='ns.admin.resetFormForPerAcct(); removeValidationErrors();'>
									<s:text name="jsp.default_358" />
								</a>
					</ffi:cinclude>
					<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="equals">
								<a  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' 
									title='<s:text name="jsp.default_358.1" />' 
									href='#'
									onClick='ns.admin.reset("<ffi:urlEncrypt url='/cb/pages/jsp/user/peraccountpermissions.jsp?ComponentIndex=${ComponentIndex}&perm_target=${perm_target}&completedTopics=${completedTopics}&nextPermission=${nextPermission}' />","#<ffi:getProperty name="perm_target"/>");'>
									<span class="ui-button-text"><s:text name="jsp.default_358" /></span>
								</a>
					</ffi:cinclude>
							<sj:a
								id="PerAccountCloseButtonId"
								button="true"
								onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
							<sj:a
								id="perAccountSaveButtonID"
								formIds="permLimitFrm"
								targets="%{#session.perm_target}"
								button="true"
								validate="true"
								validateFunction="customValidation"
								onBeforeTopics="beforeVerify"
								onCompleteTopics="adminSaveOnCompleteTopic"
								><s:text name="jsp.default_366"/></sj:a>
							<sj:a
								id="%{#attr.nextPermission}"
								button="true" 
								onclick="ns.admin.nextPermissionForPerAcct();"
								><s:text name="jsp.user_203"/></sj:a>

						</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<sj:a
								button="true" 
								onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
								><s:text name="jsp.default_102"/>
							</sj:a>
							
				</ffi:cinclude>
			</div>
</div></div>
</s:form>
</ffi:cinclude></div>
	<!-- End of The Account limit table -->

<ffi:removeProperty name="perAccountInit"/>
<ffi:removeProperty name="GetMaxLimitForPeriod"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>

<ffi:removeProperty name="NonAccountEntitlementsWithLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsWithoutLimits"/>
<ffi:removeProperty name="limitIndex"/>

<ffi:removeProperty name="accountDisplaySize"/>
<ffi:removeProperty name="numAccounts"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="PageHeading2"/>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<ffi:removeProperty name="AccountTypesList"/>
<ffi:removeProperty name="HandleParentChildDisplay"/>
<ffi:removeProperty name="ParentChild"/>
<ffi:removeProperty name="HasAdmin"/>
<ffi:removeProperty name="PerAccount"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="ChildParentLists"/>
<ffi:removeProperty name="ParentChildLists"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/peraccountpermissions.jsp?UseLastRequest=TRUE&PermissionsWizard=${SavePermissionsWizard}" URLEncrypt="true" />
<ffi:removeProperty name="counterMath"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="PermissionsWizardSubMenu"/>
<ffi:removeProperty name="SavePermissionsWizard"/>
<ffi:removeProperty name="GetCategories"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:setProperty name="FromBack" value=""/>

</ffi:synchronized>
<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />
