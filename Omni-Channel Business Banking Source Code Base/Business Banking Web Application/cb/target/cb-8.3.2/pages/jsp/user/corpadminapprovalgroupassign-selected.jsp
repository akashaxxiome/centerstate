<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String userID = request.getParameter("ApprovalGroupUserID");
	session.setAttribute(com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_USER_ID, userID);
	String context = request.getParameter("Context");
	session.setAttribute("Context", context);
%>
<ffi:object id="GetApprovalGroupsForUser" name="com.ffusion.tasks.approvals.GetApprovalGroupsForUser" scope="session"/>
<ffi:process name="GetApprovalGroupsForUser"/>
