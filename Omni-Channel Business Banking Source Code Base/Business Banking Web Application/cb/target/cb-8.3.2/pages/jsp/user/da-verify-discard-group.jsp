<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setL10NProperty name='PageHeading' value='Verify Discard Group'/>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<% session.setAttribute("itemId",request.getParameter("itemId"));%>
<% session.setAttribute("itemType",request.getParameter("itemType"));%>
<% session.setAttribute("GroupName",request.getParameter("GroupName"));%>

<ffi:object id="DiscardPendingChangeTask" name="com.ffusion.tasks.dualapproval.DiscardPendingChange"  />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemType" value="${itemType}" />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemId" value="${itemId}" />
<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("DiscardPendingChangeTask") ); %>

<div align="center">
	<table class="adminBackground" width="auto" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="columndata" align="center">
							<br>
							Please verify discard changes for Group&nbsp;<ffi:getProperty name="GroupName"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="center">
						<div align="center" class="ui-widget-header customDialogFooter">
							<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>

							<s:url id="discardGroupChangesUrl" escapeAmp="false" value="/pages/dualapproval/dualApprovalAction-discardChanges.action?itemId=${itemId}&itemType=${itemType}&module=Group&daAction=Discarded">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
							</s:url>

							<sj:a id="discardGroupChangesLink"
								  href="%{discardGroupChangesUrl}"
								  targets="resultmessage"
								  button="true"
								  title="%{getText('jsp.default_Discard')}"								  
								  onSuccessTopics="DAGroupDiscardChangesCompleteTopics"
								  onCompleteTopics="GroupCompleteTopic"
								  onErrorTopics="">
								  <s:text name="jsp.default_Discard" />
							</sj:a>
						</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		<p></p>
	</div>

<ffi:removeProperty name="GroupName"/>