<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<div id="Accounts">
<div id="accountaccessDiv" class="remoteAcctAccess">
<ffi:help id="user_accountaccess" className="moduleHelpClass"/>
<%
	request.setAttribute("FromBack", request.getParameter("FromBack"));
	request.setAttribute("UseLastRequest", request.getParameter("UseLastRequest"));
	request.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
%>

<ffi:removeProperty name="SortOperation" />
<ffi:setProperty name="reloadPermissions" value="true"/>
<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="equals">
	<ffi:removeProperty name="reloadPermissions" />
	<ffi:setProperty name="SortOperation" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="equals">
	<ffi:removeProperty name="reloadPermissions" />
	<ffi:setProperty name="SortOperation" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="confirmation_done" value="false"/>

<s:set var="tmpI18nStr" value="%{getText('jsp.user_134')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>

<%-- Reload accounts when user restricts any account in CB only --%>
<s:include value="/pages/jsp/home/inc/reloadAccounts.jsp"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<s:include value="inc/accountaccess-init.jsp"/>

<% session.setAttribute("FFIEditAccountAccessPermissions", session.getAttribute("EditAccountAccessPermissions")); %>
<% request.setAttribute("runSearch", request.getParameter("runSearch")); %>
<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<%
		String nickName = request.getParameter("SearchAcctsByNameNumType.NickName");
		String number = request.getParameter("SearchAcctsByNameNumType.Number");
		String type = request.getParameter("SearchAcctsByNameNumType.Type");
		String id = request.getParameter("SearchAcctsByNameNumType.Id");
	%>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="NickName" value="<%=nickName%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Number" value="<%=number%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Type" value="<%=type%>"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=id%>"/>
	<ffi:process name="SearchAcctsByNameNumType"/>
</ffi:cinclude>

<script type="text/javascript"><!--
	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].checked = value;
			}
		}
	}
	function setTextboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "text" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].value = value;
			}
		}
	}
	ns.admin.resetAccountAccessForm = function()
	{
		$("#acctAccessTab").click();
	}
// --></script>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:removeProperty name="DARequest" />

	<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
	<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
	<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
</ffi:cinclude>

<ffi:setProperty name="NumTotalColumns" value="10"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="9"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>

	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>

	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="9"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="9"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="9"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<ffi:setProperty name="accountDisplay" value="false"/>
<ffi:list collection="FilteredAccounts" items="acct">
	<ffi:cinclude value1="${acct.DisplayRow}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
		<ffi:setProperty name="accountDisplay" value="true"/>
	</ffi:cinclude>
</ffi:list>

<%-- Check whether total accounts to display are more than account display size. 
if yes then set the showSearch flag value FALSE and hide Account Search area --%>
<%
	String accountDisplaySizeStr;
	String numAccountsStr;
%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.ACCOUNT %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="accountDisplaySizeStr"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>

<ffi:getProperty name="accountDisplaySizeStr" assignTo="accountDisplaySizeStr"/>
<ffi:getProperty name="${SearchAcctsByNameNumType.AccountsName}" property="size" assignTo="numAccountsStr"/>
<%
	int accountDisplaySize = Integer.parseInt( accountDisplaySizeStr );
	int numAccounts = Integer.parseInt( numAccountsStr );

	if( accountDisplaySize <= numAccounts ) {
		session.setAttribute( "showSearch", "TRUE" );
	} else {
		session.setAttribute( "showSearch", "FALSE" );
	}
	pageContext.setAttribute( "accountDisplaySizeStr", String.valueOf( accountDisplaySize ) );
	pageContext.setAttribute( "numAccountsStr", String.valueOf( numAccounts ) );
%>
<%-- <span><s:text name="jsp.user_13"/></span> --%>
<div align="center">
<div><ffi:getProperty name="Context"/></div>
<div id="accountAccessPermissionsFilterTab">
	<%-- <ffi:cinclude value1="${showSearch}" value2="TRUE" operator="equals"> --%>
		<s:include value="inc/accountsearchperm.jsp" />
		<s:include value="inc/accountaccessform.jsp" />
	<%-- </ffi:cinclude>
	<ffi:cinclude value1="${showSearch}" value2="TRUE" operator="notEquals">
				<s:include value="inc/accountaccessform.jsp" />
	</ffi:cinclude> --%>
</div>
	<ffi:removeProperty name="showSearch"/>
</div>
<s:form namespace="/pages/user" action="editAccountAccessPermissions-verify" theme="simple" method="post" name="permLimitFrm" id="permLimitFrm" >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div align="center"><ul id="formerrors"></ul></div>
			<div id="accountAccessPermissionsDiv" width="100%">
				<s:include value="inc/accountaccess_grid.jsp" />
			</div>
	<div class="btn-row" style="margin-bottom:20px !important;">
				<%-- <a  id="accountAccessResetButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
					title='<s:text name="jsp.default_358.1" />' 
					href='#' 
					onClick="ns.admin.resetAccountAccessForm(); removeValidationErrors();">
					<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358"/></span>
				</a> --%>
				
				<a  id="accountAccessResetButtonId"  
					title='<s:text name="jsp.default_358.1" />' 
					href='#' 
					onClick="ns.admin.resetAccountAccessForm(); removeValidationErrors();">
					<s:text name="jsp.default_358"/>
				</a>
				
				<sj:a
					button="true" 
					onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
					><s:text name="jsp.default_102"/></sj:a>
				
				<sj:a 
					id="permAcctAccessVerifyButtonID"
					formIds="permLimitFrm"
					targets="accountaccessDiv"
					button="true"
					validate="true"
					validateFunction="ns.admin.showValidationErrors"
					onBeforeTopics="beforeVerify"
					onCompleteTopics="onCompleteTabifyAndFocusTopic"
					><s:text name="jsp.default_366"/></sj:a>	
					
				<sj:a
					id="permAcctAccessNext"
					button="true" 
					onclick="return ns.admin.nextPermission('#permAcctAccessNext', '#permTabs');"
					><s:text name="jsp.user_203"/></sj:a>
	</div>
	</s:form>
</div>
</div>
<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp" URLEncrypt="true"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/permissions.jsp" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/accountaccess.jsp?UseLastRequest=TRUE&PermissionsWizard=TRUE" URLEncrypt="TRUE"/>
</ffi:cinclude>

<ffi:setProperty name="FromBack" value=""/>

<%-- Set Top level CSS for DA mode, highlight those categories which have pending approval changes --%>
<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />

<script type="text/javascript">
	$(document).ready(function() {
		$("#accountAccessResetButtonId").button({
			icons: {
			}			
		});
	});
/* 
$('#accountAccessPermissionsFilterTab').portlet({
	generateDOM: true,
	title :js_account_filter
});

 $('#accountAccessPermissionsDiv').portlet({
 	generateDOM: true
 }); */
</script>
