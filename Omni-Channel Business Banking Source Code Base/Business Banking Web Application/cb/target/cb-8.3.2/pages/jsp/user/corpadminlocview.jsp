<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%
	String EditLocation_LocationBPWID = request.getParameter("EditLocation_LocationBPWID");
	if(EditLocation_LocationBPWID!= null){
		session.setAttribute("EditLocation_LocationBPWID", EditLocation_LocationBPWID);
	}
	
	String editlocationreload = request.getParameter("editlocationreload");
	if(editlocationreload!= null){
		session.setAttribute("editlocationreload", editlocationreload);
	}
	
	String userAction = request.getParameter("userAction");
	if(userAction!= null){
		session.setAttribute("userAction", userAction);
	}
	
%>

<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
<ffi:process name="GetBPWProperty"/>

<ffi:setProperty name='PageHeadingForLocation' value='View Location'/>
<script>
	ns.admin.PageHeadingForLocation="<ffi:getProperty name='PageHeadingForLocation' />";
</script>
<ffi:setL10NProperty name='PageHeading' value='View Location'/>
<ffi:setProperty name='PageText' value=''/>

<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request" />

<ffi:cinclude value1="${editlocationreload}" value2="true" operator="equals">
	<%-- Get the affiliate bank list --%>
	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
	<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />

	<%-- Get cashcon company list --%>
	<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
	<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
	<ffi:process name="GetCashConCompanies"/>
	<ffi:removeProperty name="GetCashConCompanies"/>

	<%-- Get the location to be edited --%>
	<ffi:cinclude value1="${userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED%>" operator="notEquals">
		<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation" scope="session" />
		<ffi:setProperty name="SetLocation" property="BPWID" value="${EditLocation_LocationBPWID}"/>
		<ffi:process name="SetLocation"/>
	</ffi:cinclude>
	<ffi:removeProperty name="userAction"/>
	
	<%-- Fill in the location values --%>
	<ffi:object id="EditLocation" name="com.ffusion.tasks.cashcon.EditLocation" scope="session" />
	<ffi:setProperty name="EditLocation" property="SessionKey" value="Location"/>
	<ffi:setProperty name="EditLocation" property="DivisionID" value="${Location.DivisionID}"/>
	<ffi:setProperty name="EditLocation" property="AccountsName" value="BankingAccounts"/>
	<ffi:setProperty name="EditLocation" property="LocationBPWID" value="${Location.LocationBPWID}"/>
	<ffi:setProperty name="EditLocation" property="LocationName" value="${Location.LocationName}"/>
	<ffi:setProperty name="EditLocation" property="LocationID" value="${Location.LocationID}"/>
	<ffi:setProperty name="EditLocation" property="LogId" value="${Location.LogId}"/>
	<ffi:setProperty name="EditLocation" property="SubmittedBy" value="${Location.SubmittedBy}"/>
	<ffi:setProperty name="EditLocation" property="Active" value="${Location.Active}"/>
	<%-- See if the routing number is in the affiliate bank list --%>
	<% boolean customLocalBank = false;
	   boolean customLocalAccount = false; %>
	<ffi:list collection="AffiliateBanks" items="Bank">
		<ffi:cinclude value1="${Bank.AffiliateRoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
			<% customLocalBank = true; %>
			<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
		</ffi:cinclude>
	</ffi:list>
	<% if (!customLocalBank) { %>
		<ffi:setProperty name="EditLocation" property="LocalRoutingNumber" value="${Location.LocalRoutingNumber}"/>
		<ffi:setProperty name="EditLocation" property="LocalBankName" value="${Location.LocalBankName}"/>
	<% } %>
	<ffi:setProperty name="EditLocation" property="CustomLocalBank" value="<%= String.valueOf(customLocalBank)%>"/>

	<%-- See if the account is in the accounts list --%>
	<ffi:list collection="BankingAccounts" items="Account">
		<ffi:cinclude value1="${Account.Number}" value2="${Location.LocalAccountNumber}" operator="equals">
			<% customLocalAccount = true; %>
			<ffi:setProperty name="EditLocation" property="LocalAccountID" value="${Account.ID}"/>
		</ffi:cinclude>
	</ffi:list>
	<% if (!customLocalAccount) { %>
	    <ffi:setProperty name="EditLocation" property="LocalAccountNumber" value="${Location.LocalAccountNumber}"/>
	    <ffi:setProperty name="EditLocation" property="LocalAccountType" value="${Location.LocalAccountType}"/>
	<% } %>
	<ffi:setProperty name="EditLocation" property="CustomLocalAccount" value="<%= String.valueOf(customLocalAccount)%>"/>

	<ffi:setProperty name="EditLocation" property="CashConCompanyBPWID" value="${Location.CashConCompanyBPWID}"/>
	<ffi:setProperty name="EditLocation" property="DisbAccountBPWID" value="${Location.DisbAccountBPWID}"/>
	<ffi:setProperty name="EditLocation" property="ConcAccountBPWID" value="${Location.ConcAccountBPWID}"/>
	<ffi:cinclude value1="${Location.DepositMinimum}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="DepositMinimum" value="${Location.DepositMinimum}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Location.DepositMaximum}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="DepositMaximum" value="${Location.DepositMaximum}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Location.AnticDeposit}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="AnticDeposit" value="${Location.AnticDeposit}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Location.ThreshDeposit}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="ThreshDeposit" value="${Location.ThreshDeposit}"/>
	</ffi:cinclude>
	<ffi:setProperty name="EditLocation" property="ConsolidateDeposits" value="${Location.ConsolidateDeposits}"/>
	<ffi:setProperty name="EditLocation" property="DepositPrenote" value="${Location.DepositPrenote}"/>
	<ffi:setProperty name="EditLocation" property="DisbursementPrenote" value="${Location.DisbursementPrenote}"/>
	<ffi:setProperty name="EditLocation" property="DepositSameDayPrenote" value="${Location.DepositSameDayPrenote}"/>
	<ffi:setProperty name="EditLocation" property="DisbursementSameDayPrenote" value="${Location.DisbursementSameDayPrenote}"/>
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditDivision_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="objectId" value="${EditLocation_LocationBPWID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="objectType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
		<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
			<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="EditLocation"/>
		<ffi:process name="PopulateObjectWithDAValues"/>
	</ffi:cinclude>
	
	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="PopulateObjectWithDAValues"/>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="0" operator="equals">
	<% boolean tempStart = true; %>
	<ffi:list collection="AffiliateBanks" items="Bank">
		<% if (tempStart) {
			tempStart = false; %>
			<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
		<% } %>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="0" operator="notEquals">
	<ffi:object id="SetAffiliateBank" name="com.ffusion.tasks.affiliatebank.SetAffiliateBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="CollectionSessionName" value="AffiliateBanks"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankSessionName" value="CurrentBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankID" value="${EditLocation.LocalBankID}"/>
	<ffi:process name="SetAffiliateBank"/>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="" operator="equals">
	<% boolean tempStart = true; %>
	<ffi:list collection="CashConCompanies" items="tempCompany">
		<% if (tempStart) {
			tempStart = false; %>
			<ffi:setProperty name="EditLocation" property="CashConCompanyBPWID" value="${tempCompany.BPWID}"/>
		<% } %>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="" operator="notEquals">
	<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
	<ffi:process name="SetCashConCompany"/>
</ffi:cinclude>

<%-- AFTER we have the edit location's company, then set the filter because this location could be using an inactive company QTS 406048 --%>
	<ffi:setProperty name="CashConCompanies" property="Filter" value="ACTIVE=true"/>
	<ffi:setProperty name="CashConCompanies" property="FilterOnFilter" value="CONC_ENABLED=true,DISB_ENABLED=true"/>

<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
<%-- Set the concentration account and disbursemnt account. Select first ones as selected if none was already selected. --%>
	<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="notEquals">
	<% boolean tempStart = true;
	   boolean accountFound = false;
	   String firstAccount = null; %>
		<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
			<% if (tempStart) {
				tempStart = false; %>
				<ffi:getProperty name="ConcAccount" property="BPWID" assignTo="firstAccount" />
			<% } %>
			<ffi:cinclude value1="${ConcAccount.BPWID}" value2="${EditLocation.ConcAccountBPWID}" operator="equals">
				<% accountFound = true; %>
			</ffi:cinclude>
		</ffi:list>
		<% if (!accountFound) { %>
			<ffi:setProperty name="EditLocation" property="ConcAccountBPWID" value="<%= firstAccount %>"/>
		<% } %>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="equals">
		<ffi:setProperty name="EditLocation" property="ConcAccountBPWID" value=""/>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="notEquals">
	<% boolean tempStart = true;
	   boolean accountFound = false;
	   String firstAccount = null; %>
		<ffi:setProperty name="tempStart" value="true"/>
		<ffi:setProperty name="AccountFound" value="false"/>
		<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
			<% if (tempStart) {
				tempStart = false; %>
				<ffi:getProperty name="DisbAccount" property="BPWID" assignTo="firstAccount" />
			<% } %>
			<ffi:cinclude value1="${DisbAccount.BPWID}" value2="${EditLocation.DisbAccountBPWID}" operator="equals">
				<% accountFound = true; %>
			</ffi:cinclude>
		</ffi:list>
		<% if (!accountFound) { %>
			<ffi:setProperty name="EditLocation" property="DisbAccountBPWID" value="<%= firstAccount %>"/>
		<% } %>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="equals">
		<ffi:setProperty name="EditLocation" property="DisbAccountBPWID" value=""/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- based on selected concentration account, set DepositEnabled --%>
<% boolean depositEnabled = false;
   boolean disbursementEnabled = false; %>
<ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="" operator="notEquals">
	<% depositEnabled = true; %>
</ffi:cinclude>

<%-- based on selected disbursement account, set DisbursementEnabled --%>
<ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="" operator="notEquals">
	<% disbursementEnabled = true; %>
</ffi:cinclude>

<ffi:setProperty name="EditLocation" property="DisPrenoteStatus" value="${Location.DisPrenoteStatus}" />
<ffi:setProperty name="EditLocation" property="DepPrenoteStatus" value="${Location.DepPrenoteStatus}" />

<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>

<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
	 <ffi:cinclude value1="${CashConCompany.ConcSameDayCutOffDefined}" value2="true" operator="equals">
	 		<ffi:setProperty name="showConcSameDayPrenote" value="true"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.ConcSameDayCutOffDefined}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.DisbSameDayCutOffDefined}" value2="true" operator="equals">
			<ffi:setProperty name="showDisbSameDayPrenote" value="true"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.DisbSameDayCutOffDefined}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>
	 </ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="notEquals">
	<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
	<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>


		<SCRIPT language=JavaScript><!--
<ffi:setProperty name="AffiliateBanks" property="Filter" value=""/>
/* Account lists for each of the "pre-defined" banks. */
var bankAccounts = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
<% int bankAccountsCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
    <ffi:setProperty name="BankingAccounts" property="Filter" value="ROUTINGNUM=${affiliateBank.AffiliateRoutingNum}"/>
bankAccounts[<%= Integer.toString(bankAccountsCounter) %>] = new Array(
<ffi:setProperty name="spaceOrComma" value=" "/>
    <ffi:list collection="BankingAccounts" items="bankAccount">
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='DisplayText'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='DisplayText'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
    </ffi:list>
	);
<% bankAccountsCounter++; %>
</ffi:list>

/* Mappings of banks to indices into the account lists.*/
var bankIndexMapping = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
var bankIndexMappingByID = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);

<% int bankIndexMappingCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
bankIndexMapping[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateRoutingNum'/>";
bankIndexMappingByID[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateBankID'/>";
<% bankIndexMappingCounter++; %>
</ffi:list>
<ffi:setProperty name="BankingAccounts" property="Filter" value=""/>

<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals" >
	<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals" >
		var cashConcAccountsCurrencyCode = new Array ();
		<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount" >
			cashConcAccountsCurrencyCode[ "<ffi:getProperty name="ConcAccount" property="BPWID" />" ] = "<ffi:getProperty name="ConcAccount" property="Currency" />";
			<ffi:cinclude value1="${ConcAccount.BPWID}" value2="${EditLocation.ConcAccountBPWID}" operator="equals" >
				<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${ConcAccount.Currency}"/>
			</ffi:cinclude>
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>

function updateAccountsList(routingNum)
{
	var updated = false;
	var accountIDSel = document.getElementById("selectAccountIDView");
	var selectedValue = accountIDSel.options[accountIDSel.selectedIndex].value;
	var selectedAccountID = "<ffi:getProperty name="EditLocation" property="LocalAccountID" />";
	
	for (var i = 0; i < bankIndexMapping.length; i++) {
		if (bankIndexMapping[i] == routingNum) {

			if (bankAccounts[i].length == 0) {
				accountIDSel.options.length = 1;
				accountIDSel.options[0] = new Option(js_transfer_freq_NONE, "");
			} else {
				accountIDSel.options.length = bankAccounts[i].length;

				for (var j = 0; j < bankAccounts[i].length; j++) {
					var defaultSelected = (selectedValue == bankAccounts[i][j][1] || selectedAccountID == bankAccounts[i][j][1]) ? true : false;
					accountIDSel.options[j] = new Option(bankAccounts[i][j][0],
							bankAccounts[i][j][1], defaultSelected, defaultSelected);
				}
			}

			updated = true;
			break;
		}
	}

	if (!updated) {
		accountIDSel.options.length = 1;
		accountIDSel.options[0] = new Option(js_transfer_freq_NONE, "");
	}
	if(ns.common.isInitialized($("#selectAccountIDView"),"ui-selectmenu")){
		$("#selectAccountIDView").selectmenu('destroy');
		$('#selectAccountIDView').selectmenu({width: 200});	
	}
	
}

function updateAccountsListByID(id) {
    for (var i = 0; i < bankIndexMappingByID.length; i++) {
	if (bankIndexMappingByID[i] == id) {
	    updateAccountsList(bankIndexMapping[i]);
	    break;
	}
    }
}

function callCancel()
{
	// return back to the previous page
	window.location.href="<ffi:getProperty name="BackURL"/>";
}
//--></SCRIPT>


<SCRIPT language=JavaScript type="text/javascript"><!--

function selectDestBank() {
	frm = document.AddEditLocationForm;
	destURL = "banklistselect.jsp";
	bankName = frm.elements['EditLocation.LocalBankName'].value;
	routingNumber = frm.elements['EditLocation.LocalRoutingNumber'].value;
	theChild = window.open(destURL+"?SearchBankName="+bankName+"&SearchRoutingNumber="+routingNumber+"&TargetExternalAccountForm=AddEditLocationForm&BankNameFieldName=EditLocation.LocalBankName&RoutingNumberFieldName=EditLocation.LocalRoutingNumber&AddingFreeFormAccount=true","BankList","width=360,height=420,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,directories=no,location=no,status=no");
}

function updateFields()
{
	frm = document.AddEditLocationForm;

	// Active or not
	if( frm.elements["Active"].checked ) {
		frm.elements[ "EditLocation.Active" ].value = "true";
	} else {
		frm.elements[ "EditLocation.Active" ].value = "false";
	}

	// ConsolidateDeposits
	if( frm.elements["ConsolidateDeposits"].checked ) {
		frm.elements[ "EditLocation.ConsolidateDeposits" ].value = "true";
	} else {
		frm.elements[ "EditLocation.ConsolidateDeposits" ].value = "false";
	}

	// DepositPrenote
	if( frm.elements["DepositPrenote"].checked ) {
		frm.elements[ "EditLocation.DepositPrenote" ].value = "true";
	} else {
		frm.elements[ "EditLocation.DepositPrenote" ].value = "false";
	}

	// DisbursementPrenote
	if( frm.elements["DisbursementPrenote"].checked ) {
		frm.elements[ "EditLocation.DisbursementPrenote" ].value = "true";
	} else {
		frm.elements[ "EditLocation.DisbursementPrenote" ].value = "false";
	}
}

$(document).ready(function(){

	<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="false" operator="equals">
			updateAccountsList('<ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>');
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">
			updateAccountsList('<ffi:getProperty name="CurrentBank" property="AffiliateRoutingNum"/>');
	</ffi:cinclude>
});

//--></SCRIPT>

		<div align="center">
			<table width="676" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionhead"><ffi:getProperty name="EditDivision_DivisionName"/><br>
								<br>
							</span>
						</div>
					</td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="adminBackground">
						<form action="<ffi:urlEncrypt url="${SecurePath}user/corpadminlocview.jsp?editlocation-reload=false" />" name="AddEditLocationForm" method="post" onsubmit="return updateFields();">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<div align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td><p class="transactionHeading"><s:text name="admin.location.summary"/></p></td>
									</tr>
								</table>
								<table border="0" cellspacing="0" cellpadding="3" align="left">	
									<tr class="columndataauto">
										<td class="sectionsubhead ltrow2_color" align="left">
											<span class="<ffi:getPendingStyle fieldname="locationName" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_267"/>:</span>
										</td>
										<td width="70">&nbsp;</td>
										<td class="sectionsubhead ltrow2_color" align="left">
											<span class="<ffi:getPendingStyle fieldname="locationID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_189"/>:</span>
										</td>
										<td width="20">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocationName" size="24" maxlength="16" border="0" value="<ffi:getProperty name="EditLocation" property="LocationName"/>">
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="LocationName"/>
											</span>
										</td>
										<td>&nbsp;</td>
										<td>
											<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocationID" size="24" maxlength="15" border="0" value="<ffi:getProperty name="EditLocation" property="LocationID"/>">
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="LocationID"/>
											</span>
										</td>
										<td>&nbsp;</td>
										<td>
											<input disabled="disabled" type="checkbox"  class="ui-widget-content ui-corner-all" name="Active" border="0" <ffi:cinclude value1="${EditLocation.Active}" value2="true" operator="equals">checked</ffi:cinclude> >
											<input type="hidden" name="EditLocation.Active">
											<span class="<ffi:getPendingStyle fieldname="active" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/> adminBackground" width="120"><s:text name="jsp.default_28"/></span>
										</td>
									</tr>
								</table>
								
								<table width="100%" border="0" cellspacing="0" cellpadding="3"><!-- Bank -->
									<tr  class="columndataauto" >
										<td colspan="4"><p class="transactionHeading"><s:text name="jsp.user_188"/></p></td>
									</tr>
									<tr class="columndataauto">
										<td class="sectionsubhead ltrow2_color" align="left" height="50" width="180">
											<input  class="ui-widget-content ui-corner-all" disabled="disabled" type="radio" name="EditLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="notEquals">checked</ffi:cinclude>>
											&nbsp;
											<span class="<ffi:getPendingStyle fieldname="customLocalBank" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_61"/>&nbsp;<ffi:getProperty name="TempBankIdentifierDisplayText"/>:</span>
										</td>
										<td class="sectionsubhead ltrow2_color" width="230" height="50" valign="middle">
											<input class="ui-widget-content ui-corner-all" disabled="disabled" type="text" name="EditLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value); AddEditLocationForm['EditLocation.CustomLocalBank'][0].checked = true;">
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="LocalRoutingNumber"/>
											</span>
										</td>
										<td class="sectionsubhead ltrow2_color" width="270">
											<span class="ffivisible marginRight10"><s:text name="jsp.default_306" /><span>   &nbsp; &nbsp; &nbsp; &nbsp;
											<input class="ui-widget-content ui-corner-all" disabled="disabled" type="text" name="EditLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="EditLocation" property="LocalBankName"/>">
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="LocalBankName"/>
											</span>
										</td>
										<td class="sectionsubhead ltrow2_color">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="4" height="50">
											<input type="radio"  class="ui-widget-content ui-corner-all" disabled="disabled" name="EditLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID(AddEditLocationForm['EditLocation.LocalBankID'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
											<select id="selectBankIDView" disabled="disabled" class="txtbox" name="EditLocation.LocalBankID" onclick="updateFields(); updateAccountsListByID(AddEditLocationForm['EditLocation.LocalBankID'].value); AddEditLocationForm['EditLocation.CustomLocalBank'][1].checked = true;">
												<ffi:list collection="AffiliateBanks" items="Bank">
													<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${EditLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
													</option>
												</ffi:list>
											</select>
											<span class="sectionhead_greyDA">
												<ffi:list collection="AffiliateBanks" items="Bank">
													<ffi:cinclude value1="${Bank.AffiliateBankID}" value2="${oldDAObject.LocalBankID}" operator="equals">
														<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
													</ffi:cinclude>
												</ffi:list>
											</span>
										</td>
									</tr>
									<!-- Bank  -->
									<tr>
										<td>&nbsp;</td>
									</tr>
									<!-- Account Information  -->
									<tr class="columndataauto">
										<td colspan="4">
										<table  width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="sectionsubhead ltrow2_color" align="left"  height="50" width="150">
												<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="radio" name="EditLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
												&nbsp;<span class="<ffi:getPendingStyle fieldname="localAccountNumber" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_19"/>:</span>
											</td>
											<td class="sectionsubhead ltrow2_color" width="250">
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="EditLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['EditLocation.CustomLocalAccount'][0].checked = true;">
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="LocalAccountNumber"/>
												</span>
											</td>
											<td class="sectionsubhead ltrow2_color"  width="100">
												<span class="<ffi:getPendingStyle fieldname="localAccountType" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_20"/>:</span>
											</td>
											<td class="sectionsubhead ltrow2_color">
												<select id="selectAccountTypeIDView" disabled="disabled" class="txtbox" name="EditLocation.LocalAccountType">
													<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
													<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
												</select>
												<span class="sectionhead_greyDA">
														<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
															<s:text name="jsp.user_64"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
															<s:text name="jsp.user_281"/>
														</ffi:cinclude>
												</span>
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;<span id="AddLocation.localAccountNumberError"></span></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="4" height="50">
												<input  class="ui-widget-content ui-corner-all" disabled="disabled" type="radio" name="EditLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="notEquals">checked</ffi:cinclude>>
												&nbsp;&nbsp;
												<select id="selectAccountIDView" class="txtbox" disabled="disabled" name="EditLocation.LocalAccountID" onclick="AddEditLocationForm['EditLocation.CustomLocalAccount'][1].checked = true;">
													<option value=""><s:text name="jsp.default_296"/></option>
												</select>
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="LocalAccountID"/>
												</span>
											</td>
										</tr>
										</table>
										</td>
									</tr>
									<!-- Account Information  -->
								</table>
								
								<!-- Remote Bank information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_276"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="<ffi:getPendingStyle fieldname="cashConCompanyBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_55"/>:</span></td>
										<td><span class="<ffi:getPendingStyle fieldname="concAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_75"/>:</span></td>
										<td><span class="<ffi:getPendingStyle fieldname="disbAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_117"/>:</span></td>
									</tr>
									<tr class="columndataauto" >
										<s:if test="#session.oldDAObject.cashConCompanyBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' " >
											<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
											<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${oldDAObject.CashConCompanyBPWID}"/>
											<ffi:process name="SetCashConCompany"/>
										</s:if>
										<s:elseif test="#session.oldDAObject.disbAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
											<ffi:setProperty name="oldDAObject" property="CashConCompanyBPWID" value="CashConCompanies"/>
											<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
											<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
											<ffi:process name="SetCashConCompany"/>
										</s:elseif>
										<s:elseif test="#session.oldDAObject.concAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
											<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
											<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
											<ffi:process name="SetCashConCompany"/>
										</s:elseif>	
	
										<td valign="top">
											<select id="selectCashConCompanyIDView" disabled="disabled" class="txtbox" name="EditLocation.CashConCompanyBPWID" onchange="updateFields(); document.AddEditLocationForm.submit();" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
												<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
																<option value=""><s:text name="jsp.default_296"/></option>
												</ffi:cinclude>
												<%-- Disabled CashConCompany will not be shown in collection, so display now as invalid. --%>
												<ffi:cinclude value1="${CashConCompany.Active}" value2="false" operator="equals">
												<option value=""><ffi:getProperty name="CashConCompany" property="CompanyName"/> (<s:text name="jsp.user_116"/>)</option>
												</ffi:cinclude>
												<ffi:list collection="CashConCompanies" items="tempCompany">
													<option value="<ffi:getProperty name="tempCompany" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="tempCompany" property="CompanyName"/>
													</option>
												</ffi:list>
												</select>
												<span class="sectionhead_greyDA">
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<ffi:cinclude value1="${oldDAObject.CashConCompanyBPWID}" value2="${tempCompany.BPWID}">
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
														</ffi:cinclude>
													</ffi:list>
												</span>
										</td>
										<td>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
												<select id="selectConcAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.ConcAccountBPWID" disabled>
													<option value="" selected><s:text name="jsp.default_296"/></option>
												</select>
											</ffi:cinclude>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
													<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="false" operator="equals">
												<select id="selectConcAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.ConcAccountBPWID" onchange="updateFields();" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals">
												<select id="selectConcAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.ConcAccountBPWID" onchange="updateFields();">
														<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
															<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																</ffi:cinclude>	
															</option>
														</ffi:list>
													</ffi:cinclude>
												</select>
											</ffi:cinclude>
											<span class="sectionhead_greyDA">
												<ffi:list collection="OldCashConCompany.ConcAccounts" items="ConcAccount">
													<ffi:cinclude value1="${oldDAObject.ConcAccountBPWID}" value2="${ConcAccount.BPWID}">
															<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																</ffi:cinclude>
													</ffi:cinclude>
												</ffi:list>
											</span>
										</td>
										<td>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
												<select id="selectDisbAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.DisbAccountBPWID" disabled>
													<option value="" selected><s:text name="jsp.default_296"/></option>
												</select>
											</ffi:cinclude>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="false" operator="equals">
												<select id="selectDisbAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.DisbAccountBPWID" onchange="updateFields();" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="true" operator="equals">
												<select id="selectDisbAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.DisbAccountBPWID" onchange="updateFields();">
														<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
															<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																</ffi:cinclude>	
															</option>
														</ffi:list>
													</ffi:cinclude>
												</select>
											</ffi:cinclude>
											<span class="sectionhead_greyDA">
												<ffi:list collection="OldCashConCompany.DisbAccounts" items="DisbAccount">
													<ffi:cinclude value1="${oldDAObject.DisbAccountBPWID}" value2="${DisbAccount.BPWID}">
															<ffi:getProperty name="DisbAccount" property="DisplayText"/>
															<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
															</ffi:cinclude>
													</ffi:cinclude>
												</ffi:list>
											</span>
										</td>
									</tr>
								</table>
								<!-- Remote Bank information -->
								
								<!-- Deposit-specific information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_113"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="<ffi:getPendingStyle fieldname="depositMinimum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_112"/>:</span></td>
										<td><span class="<ffi:getPendingStyle fieldname="depositMaximum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_111"/>:</span></td>
										<td><span class="<ffi:getPendingStyle fieldname="anticDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_44"/>:</span></td>
									</tr>
									<tr class="columndataauto" >
										<td>
											<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="notEquals">
												<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMinimum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="equals">
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMinimum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="DepositMinimum"/>
											</span>
										</td>
										<td>
											<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="notEquals">
												<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMaximum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="equals">
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMaximum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="DepositMaximum"/>
											</span>
										</td>
										<td>
											<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="notEquals">
												<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.AnticDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="equals">
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.AnticDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="AnticDepositString"/>
											</span>
										</td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="<ffi:getPendingStyle fieldname="threshDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_337"/>:</span></td>
										<td></td>
										<td></td>
									</tr>
									<tr class="columndataauto" >
										<td>
											<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="notEquals">
												<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.ThreshDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="equals">
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.ThreshDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
											</ffi:cinclude>
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="ThreshDepositString"/>
											</span>
										</td>
										<td>
											<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="checkbox" name="ConsolidateDeposits" border="0" <% if (depositEnabled) { %> <ffi:cinclude value1="${EditLocation.ConsolidateDeposits}" value2="true" operator="equals">checked</ffi:cinclude> <% } else { %> disabled <% } %>>
											<input type="hidden" name="EditLocation.ConsolidateDeposits">
											&nbsp;<span class="<ffi:getPendingStyle fieldname="consolidateDeposits" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_94"/></span>
										</td>
										
											<input type="hidden" name="EditLocation.DepositPrenote">
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="notEquals">
												<td>
													<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="checkbox" name="DepositPrenote" border="0" <% if (depositEnabled) { %><ffi:cinclude value1="${EditLocation.DepositPrenote}" value2="true" operator="equals">checked</ffi:cinclude> <% } else { %> disabled <% } %>>
													<input type="hidden" name="EditLocation.DepositPrenote">
													&nbsp;<span class="<ffi:getPendingStyle fieldname="depositPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_110"/></span>
												</td>	
											</ffi:cinclude>
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="equals">
												<td><span></span></td>
											</ffi:cinclude>
									</tr>
									<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="equals">
										<tr class="columndataauto" >
											<td id="depositSameDayPrenoteClass" colspan="3">
												<table width="84%">
													<tr>
														<td class="<ffi:getPendingStyle fieldname="depositPrenote" 
																				defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
															<div id="depositPrenoteLabel" >
																<span class="<ffi:getPendingStyle fieldname="depositSameDayPrenote" 
																	defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
																	<s:text name="jsp.user_110"/>
																</span>
															</div>	
														</td>
														<td>
															
															 <div id="depositPrenoteCheckbox" >
																<span>
																 <input id="DepositPrenote" disabled="disabled"  class="ui-widget-content ui-corner-all" type="checkbox" name="DepositPrenote" border="0" 
																		<% 
																			if (depositEnabled) { 
																		%>
																			<ffi:cinclude value1="${EditLocation.DepositPrenote}" value2="true" operator="equals">
																				<ffi:cinclude value1="${EditLocation.DepositSameDayPrenote}" value2="true" operator="notEquals">
																					checked 
																				</ffi:cinclude>
																			</ffi:cinclude>
																		<% 
																			} else { 
																		%>
																			disabled
																		<% 	} %>
																	>
																</span><span><s:text name="jsp.user_571"/></span>
															</div>		
														</td>
														 <td>
															<div id="depositSameDayPrenoteCheckbox" >
															  <span>
																<input id="DepositSameDayPrenote" disabled="disabled"  class="ui-widget-content ui-corner-all" type="checkbox" name="DepositSameDayPrenote" value="true" border="0" 
																<% 
																	if (depositEnabled) { 
																%>
																<ffi:cinclude value1="${EditLocation.DepositSameDayPrenote}" value2="true" operator="equals"> 
																	checked 
																</ffi:cinclude> 
																<% 
																	} else { 
																%>
																	disabled
																<% 	} %>
																>
																 <input type="hidden" name="EditLocation.DepositSameDayPrenote">
															  </span><span><s:text name="jsp.user_572"/></span>
															</div>
														 </td>
														</tr>						
													</table>
												 </td>	
										</tr>
									</ffi:cinclude>
								</table>
								<!-- Deposit-specific information -->
								
								<!-- Disbursement-Request-Specific-Information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td><p class="transactionHeading"><s:text name="jsp.user_119"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<input type="hidden" name="EditLocation.DisbursementPrenote">
										<ffi:cinclude value1="${showDisbSameDayPrenote}" value2="true" operator="notEquals">
											<td>
												<input type="checkbox"  class="ui-widget-content ui-corner-all" disabled="disabled" name="DisbursementPrenote" border="0" <% if (disbursementEnabled) { %><ffi:cinclude value1="${EditLocation.DisbursementPrenote}" value2="true" operator="equals">checked</ffi:cinclude> <% } else { %> disabled <% } %>>
												<input type="hidden" name="EditLocation.DisbursementPrenote">
												&nbsp; <span class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_118"/></span>
											</td>
										</ffi:cinclude>
										<ffi:cinclude value1="${showDisbSameDayPrenote}" value2="true" operator="equals"> 
										
										<td id="disbursementSameDayPrenoteClass" colspan="3" class="displayNone">
											<table width="70%">
												<tr>
											    	<td class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
														<div id="disbursementPrenoteLabel" >
															<span class="<ffi:getPendingStyle fieldname="disbursementSameDayPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_118"/></span>
														</div>
													</td>
													
													<td>
														 <div id="disbursementPrenoteCheckbox" >
														 	<span>
															 	<input id="DisbursementPrenote" type="checkbox" class="ui-widget-content ui-corner-all" disabled="disabled" name="DisbursementPrenote" value="true" border="0" 
																    <% 
																		if (disbursementEnabled) { 
																	%>
																	<ffi:cinclude value1="${EditLocation.DisbursementPrenote}" value2="true" operator="equals">
																		<ffi:cinclude value1="${EditLocation.DisbursementSameDayPrenote}" value2="true" operator="notEquals">
																			checked 
																		</ffi:cinclude>
																    </ffi:cinclude>
																	<% 
																		} else { 
																	%> 
																		disabled 
																	<% } %>
																 >
															</span>	<span><s:text name="jsp.user_571"/></span>	
						 								 </div>	
													</td>
													
													
													<td>
														  <div id="disbursementSameDayPrenoteCheckbox" >
														  <span>
														  	<input id="DisbursementSameDayPrenote" type="checkbox" class="ui-widget-content ui-corner-all" disabled="disabled"     name="DisbursementSameDayPrenote" value="true" border="0" 
														  		<% 
																	if (disbursementEnabled) { 
																%>
																
																<ffi:cinclude value1="${EditLocation.DisbursementSameDayPrenote}" value2="true" operator="equals">
															  		checked
															  	</ffi:cinclude>
																<% 
																	} else { 
																%> 
																	disabled 
																<% 	} %>
															>
															<input type="hidden" name="EditLocation.DisbursementSameDayPrenote">
														  </span><span><s:text name="jsp.user_572"/></span>
													  </div>
													</td>
													
												</tr>

										    </table>
										  </td>
										</ffi:cinclude>
									</tr>
								</table>
								<!-- Disbursement-Request-Specific-Information -->
								
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td colspan="10" align="center">
											<sj:a button="true" title="Close" onclick="ns.common.closeDialog('viewLocationDialogID')"><s:text name="jsp.default_175" /></sj:a>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
								<!-- Ends here -->
								
								 <%-- <table border="1" cellspacing="0" cellpadding="3" width="710">
									<tr>
										<td colspan="4" class="adminBackground tbrd_b">
											<span class="sectionhead"><s:text name="jsp.user_188"/></span><br>
										</td>
										<td class="adminBackground">&nbsp;</td>
										<td class="adminBackground">&nbsp;</td>
										<td class="tbrd_b adminBackground" colspan="4">
											<span class="sectionhead"><s:text name="jsp.user_113"/></span>
										</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="4">
											<div align="left"><span class="<ffi:getPendingStyle fieldname="customLocalBank" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_61"/></span></div>
										</td>
										<td valign="top" class="tbrd_r adminBackground" rowspan="13">&nbsp;</td>
										<td valign="top" class="adminBackground" rowspan="13">&nbsp;</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="depositMinimum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_112"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="notEquals">
													<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMinimum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="equals">
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMinimum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="DepositMinimum"/>
												</span>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td>
											<div align="left">
												&nbsp;&nbsp;<input  class="ui-widget-content ui-corner-all" disabled="disabled" type="radio" name="EditLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="notEquals">checked</ffi:cinclude>>
											</div>
										</td>
										<td nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="localRoutingNumber" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
											  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
											:</span></div>
										</td>
										<td>
											<div align="left">
												<input class="ui-widget-content ui-corner-all" disabled="disabled" type="text" name="EditLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value); AddEditLocationForm['EditLocation.CustomLocalBank'][0].checked = true;">
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="LocalRoutingNumber"/>
												</span>
											</div>
										</td>
										<td>
												&nbsp;							
										</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="depositMaximum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_111"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="notEquals">
													<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMaximum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="equals">
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.DepositMaximum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="DepositMaximum"/>
												</span>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td>&nbsp;</td>
										<td nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="localBankName" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_63"/>:</span></div>
										</td>
										<td>
											<input class="ui-widget-content ui-corner-all" disabled="disabled" type="text" name="EditLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="EditLocation" property="LocalBankName"/>">
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="LocalBankName"/>
											</span>
										</td>
										<td></td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="anticDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_44"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="notEquals">
													<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.AnticDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="equals">
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.AnticDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
											</div>
											<span class="sectionhead_greyDA">
												<ffi:getProperty name="oldDAObject" property="AnticDepositString"/>
											</span>
										</td>
									</tr>
									<tr valign="middle">
										<td>
											&nbsp;&nbsp;<input type="radio"  class="ui-widget-content ui-corner-all" disabled="disabled" name="EditLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID(AddEditLocationForm['EditLocation.LocalBankID'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="selectBankIDView" disabled="disabled" class="txtbox" name="EditLocation.LocalBankID" onclick="updateFields(); updateAccountsListByID(AddEditLocationForm['EditLocation.LocalBankID'].value); AddEditLocationForm['EditLocation.CustomLocalBank'][1].checked = true;">
													<ffi:list collection="AffiliateBanks" items="Bank">
														<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${EditLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
															<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
														</option>
													</ffi:list>
												</select>
												<span class="sectionhead_greyDA">
													<ffi:list collection="AffiliateBanks" items="Bank">
														<ffi:cinclude value1="${Bank.AffiliateBankID}" value2="${oldDAObject.LocalBankID}" operator="equals">
															<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
														</ffi:cinclude>
													</ffi:list>
												</span>
											</div>
										</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="threshDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_337"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="notEquals">
													<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.ThreshDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="equals">
													<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.ThreshDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="" <% } else { %> disabled <% } %>>
												</ffi:cinclude>
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="ThreshDepositString"/>
												</span>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="4">
											<div align="left"> <span class="<ffi:getPendingStyle fieldname="customLocalAccount" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_15"/></span></div>
										</td>
										<td>
											<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="checkbox" name="ConsolidateDeposits" border="0" <% if (depositEnabled) { %> <ffi:cinclude value1="${EditLocation.ConsolidateDeposits}" value2="true" operator="equals">checked</ffi:cinclude> <% } else { %> disabled <% } %>>
											<input type="hidden" name="EditLocation.ConsolidateDeposits">
										</td>
										<td colspan="3"><span class="<ffi:getPendingStyle fieldname="consolidateDeposits" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_94"/></span></td>
									</tr>
									<tr valign="middle">
										<td>
											<div align="left">
												&nbsp;&nbsp;<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="radio" name="EditLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
											</div>
										</td>
										<td nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="localAccountNumber" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_19"/>:</span></div>
										</td>
										<td>
											<div align="left">
												<input disabled="disabled" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="EditLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['EditLocation.CustomLocalAccount'][0].checked = true;">
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="LocalAccountNumber"/>
												</span>
											</div>
										</td>
										<td>&nbsp; </td>
										<td>
											<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="checkbox" name="DepositPrenote" border="0" <% if (depositEnabled) { %><ffi:cinclude value1="${EditLocation.DepositPrenote}" value2="true" operator="equals">checked</ffi:cinclude> <% } else { %> disabled <% } %>>
											<input type="hidden" name="EditLocation.DepositPrenote">
										</td>
										<td colspan="3"><span class="<ffi:getPendingStyle fieldname="depositPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_110"/></span></td>
									</tr>
									<tr valign="middle">
										<td>&nbsp;</td>
										<td nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="localAccountType" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_20"/>:</span></div>
										</td>
										<td>
											<select id="selectAccountTypeIDView" disabled="disabled" class="txtbox" name="EditLocation.LocalAccountType">
												<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
												<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
											</select>
											<span class="sectionhead_greyDA">
													<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
														<s:text name="jsp.user_64"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
														<s:text name="jsp.user_281"/>
													</ffi:cinclude>
											</span>
										</td>
										<td></td>
										<td colspan="4">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td>
											&nbsp;&nbsp;<input disabled="disabled"  class="ui-widget-content ui-corner-all" type="radio" name="EditLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="selectAccountIDView" class="txtbox" disabled="disabled" name="EditLocation.LocalAccountID" onclick="AddEditLocationForm['EditLocation.CustomLocalAccount'][1].checked = true;">
													<option value=""><s:text name="jsp.default_296"/></option>
												</select>
												<span class="sectionhead_greyDA">
													<ffi:getProperty name="oldDAObject" property="LocalAccountID"/>
												</span>
											</div>
										</td>
										<td colspan="4" class="tbrd_b"><span class="sectionhead"><s:text name="jsp.user_119"/></span></td>
									</tr>
									<tr valign="middle">
										<td colspan="4">&nbsp;</td>
										<td>
											<input type="checkbox"  class="ui-widget-content ui-corner-all" disabled="disabled" name="DisbursementPrenote" border="0" <% if (disbursementEnabled) { %><ffi:cinclude value1="${EditLocation.DisbursementPrenote}" value2="true" operator="equals">checked</ffi:cinclude> <% } else { %> disabled <% } %>>
											<input type="hidden" name="EditLocation.DisbursementPrenote">
										</td>
										<td colspan="3" nowrap><span class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_118"/></span></td>
									</tr>
									<tr valign="middle">
										<td class="tbrd_b adminBackground" colspan="4"><span class="sectionhead"><s:text name="jsp.user_276"/></span></td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="cashConCompanyBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_55"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<select id="selectCashConCompanyIDView" disabled="disabled" class="txtbox" name="EditLocation.CashConCompanyBPWID" onchange="updateFields(); document.AddEditLocationForm.submit();" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
												    <option value=""><s:text name="jsp.default_296"/></option>
</ffi:cinclude>
Disabled CashConCompany will not be shown in collection, so display now as invalid.
	<ffi:cinclude value1="${CashConCompany.Active}" value2="false" operator="equals">
		    <option value=""><ffi:getProperty name="CashConCompany" property="CompanyName"/> (<s:text name="jsp.user_116"/>)</option>
	</ffi:cinclude>
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<option value="<ffi:getProperty name="tempCompany" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
														</option>
													</ffi:list>
												</select>
												<span class="sectionhead_greyDA">
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<ffi:cinclude value1="${oldDAObject.CashConCompanyBPWID}" value2="${tempCompany.BPWID}">
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
														</ffi:cinclude>
													</ffi:list>
												</span>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<s:if test="#session.oldDAObject.cashConCompanyBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' " >
											<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
											<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${oldDAObject.CashConCompanyBPWID}"/>
											<ffi:process name="SetCashConCompany"/>
										</s:if>
										<s:elseif test="#session.oldDAObject.disbAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
											<ffi:setProperty name="oldDAObject" property="CashConCompanyBPWID" value="CashConCompanies"/>
											<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
											<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
											<ffi:process name="SetCashConCompany"/>
										</s:elseif>
										<s:elseif test="#session.oldDAObject.concAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
											<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
											<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
											<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
											<ffi:process name="SetCashConCompany"/>
										</s:elseif>	
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="concAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_75"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
													<select id="selectConcAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.ConcAccountBPWID" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</select>
												</ffi:cinclude>
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="false" operator="equals">
													<select id="selectConcAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.ConcAccountBPWID" onchange="updateFields();" disabled>
															<option value="" selected><s:text name="jsp.default_296"/></option>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals">
													<select id="selectConcAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.ConcAccountBPWID" onchange="updateFields();">
															<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
																<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																	<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																	<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																		 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
				 													</ffi:cinclude>	
																</option>
															</ffi:list>
														</ffi:cinclude>
													</select>
												</ffi:cinclude>
												<span class="sectionhead_greyDA">
													<ffi:list collection="OldCashConCompany.ConcAccounts" items="ConcAccount">
														<ffi:cinclude value1="${oldDAObject.ConcAccountBPWID}" value2="${ConcAccount.BPWID}">
																<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																	<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																		 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
				 													</ffi:cinclude>
														</ffi:cinclude>
													</ffi:list>
												</span>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="<ffi:getPendingStyle fieldname="disbAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_117"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
													<select id="selectDisbAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.DisbAccountBPWID" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</select>
												</ffi:cinclude>
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
															<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="false" operator="equals">
													<select id="selectDisbAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.DisbAccountBPWID" onchange="updateFields();" disabled>
															<option value="" selected><s:text name="jsp.default_296"/></option>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="true" operator="equals">
													<select id="selectDisbAccountIDView" disabled="disabled" class="txtbox" name="EditLocation.DisbAccountBPWID" onchange="updateFields();">
															<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
																<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																	<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																	<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																		 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
															 		</ffi:cinclude>	
																</option>
															</ffi:list>
														</ffi:cinclude>
													</select>
												</ffi:cinclude>
												<span class="sectionhead_greyDA">
													<ffi:list collection="OldCashConCompany.DisbAccounts" items="DisbAccount">
														<ffi:cinclude value1="${oldDAObject.DisbAccountBPWID}" value2="${DisbAccount.BPWID}">
																<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
														 		</ffi:cinclude>
														</ffi:cinclude>
													</ffi:list>
												</span>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td width="25">&nbsp;</td>
										<td width="110">&nbsp;</td>
										<td width="100">&nbsp;</td>
										<td width="75">&nbsp;</td>
										<td valign="top" class="adminBackground">&nbsp;</td>
										<td valign="top" class="adminBackground">&nbsp;</td>
										<td width="25">&nbsp;</td>
										<td width="100">&nbsp;</td>
										<td width="100">&nbsp;</td>
										<td width="65">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="10" align="center">
											<sj:a button="true" title="Close" onclick="ns.common.closeDialog('viewLocationDialogID')"><s:text name="jsp.default_175" /></sj:a>
										</td>
									</tr>
								</table> 										 --%>
							</div>
						</form>
					</td>
				</tr>
			</table>
		</div>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminlocview.jsp?addlocation-reload=false UseLastRequest=TRUE" URLEncrypt="true"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="Location"/>
<script language="javascript">
	$(document).ready(function(){
	$("#selectBankIDView").selectmenu({width: 200});
	$("#selectAccountTypeIDView").selectmenu({width: 150});
	$("#selectAccountIDView").selectmenu({width: 200});
	$("#selectCashConCompanyIDView").selectmenu({width: 150});
	$("#selectConcAccountIDView").selectmenu({width: 150});
	$("#selectDisbAccountIDView").selectmenu({width: 150});
});
</script>