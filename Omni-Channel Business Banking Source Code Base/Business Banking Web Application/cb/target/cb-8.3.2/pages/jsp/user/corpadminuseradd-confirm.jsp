<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:help id="user_corpadminuseradd-confirm" className="moduleHelpClass"/>



<ffi:removeProperty name="NewPassword"/>
<ffi:removeProperty name="ConfirmPassword"/>
<ffi:removeProperty name="NewPassword2"/>
<ffi:removeProperty name="ConfirmPassword2"/>
<ffi:removeProperty name="NewBusinessEmployee"/>

<%-- BusinessEmployeeId is needed in case clone permissions is selected --%>
<ffi:setProperty name="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>

	<div class="adminBackground marginTop20" align="center">
						<div id="userSaveMessage" class="columndata" align="center">
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<%-- <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-confirm.jsp-1" parm0="${BusinessEmployee.UserName}"/> --%>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-confirm.msg1.jsp-1"/>
								<strong> <ffi:getProperty name="BusinessEmployee" property="UserName"/> </strong>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-confirm.msg1.jsp-2"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<%-- <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-confirm.jsp-2" parm0="${BusinessEmployee.UserName}"/> --%>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-confirm.msg2.jsp-1"/> 
								<strong> <ffi:getProperty name="BusinessEmployee" property="UserName"/> </strong> 
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-confirm.msg2.jsp-2"/>
							</ffi:cinclude>
						</div>
						<div class="btn-row">
							<sj:a button="true" onClickTopics="cancelUserForm,refreshUserGrid"><s:text name="jsp.default_175"/></sj:a>
							<s:url id="userAddClonePermissionsUrl" value="%{#session.PagesPath}user/clone-user-permissions-init.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								<s:param name="AddUser" value="%{'TRUE'}"/>
							</s:url>
							<s:if test="(usingEntProfile==false)">		
							<sj:a
								id="userAddClonePermissionsButton"
								href="%{userAddClonePermissionsUrl}"
								targets="inputDiv"
								button="true"
								title="%{getText('jsp.user_398')}"
								onClickTopics="beforeCloneUserAndAccountPermLoad"
								onCompleteTopics="completeLoad"
								onErrorTopics="errorLoad"
								>
								  &nbsp;<s:text name="jsp.user_398.1" /></sj:a>
							</s:if>
						</div>
					
</div>
<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminusers-pre.jsp"/>
