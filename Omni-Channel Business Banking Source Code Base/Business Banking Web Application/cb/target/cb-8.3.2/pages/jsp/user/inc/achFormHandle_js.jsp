<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script language="JavaScript" type="text/javascript"><!--

/*Check all uncheck all the checkBox start with a specific perfix
 * form - the form to operate on
 * controlBox - control checkBox name
 * prefix - prefix of the boxes to operate on
 */
function setACHCheckBoxes( form, controlBox, prefix ) {
	controlBoxStatus = form.elements[controlBox].checked;
	for( i = 0; i < form.length; i++ ) {
		if(  form.elements[i].name.indexOf(prefix) == 0 ) {
			form.elements[i].checked = controlBoxStatus;
		}
	}
}

/* Checks for negative values, and values that are made redundant by other ACHLimits
 * form - the form to operate on
 * prefix - prefix for the entitlement type check box
 */
function validateACHLimits( form, prefix ) {
	for( i = 0; i < form.length; i++ ) {
		name = form.elements[i].name;
		if( name.indexOf(prefix) == 0 && form.elements[i].checked ) {
			// all the limits of this entitlement type will have the same suffix
			suffix = name.substring(prefix.length, name.length);
			checkCreditLimits = false;
			// if the entitlement type has a limit, elements[i+1] would be a limit text box
			if( form.elements[i+1] != null && form.elements[i+1].name.indexOf(suffix) != -1) {
				nextName = form.elements[i+1].name;
				nextSuffix = nextName.substring( nextName.indexOf(suffix), nextName.length );
				if( suffix == nextSuffix ) {
					if( form.elements[i+1].name.indexOf("Credit") != -1) {
						if( !checkPeriodRelation( form, "Credit", suffix, i ) ) {
							return false;
						}
					}
					//this is for the case there is only debit limit type
					if( form.elements[i+1].name.indexOf("Debit") != -1) {
						if( !checkPeriodRelation( form, "Debit", suffix, i ) ) {
							return false;
						}
					}
				}
			}

			// Determine whether the "Exceed Limit with Approval" check box exists for the entitlement.
			// If it does, then the textbox is i+3 , if it's not then the textbox is i+2.
			var type = form.elements[i+2].type;
			var offset;
			if ( type ==  "checkbox" ) {
				offset = 3;
			} else {
				offset = 2;
			}

			// if the entitlement type has a debit limit, elements[i+offset] would be the limit text box
			if( form.elements[i+offset] != null && form.elements[i+offset].name.indexOf(suffix) != -1) {
				nextName = form.elements[i+offset].name;
				nextSuffix = nextName.substring( nextName.indexOf(suffix), nextName.length );
				if( suffix == nextSuffix ) {
					//this is for the case where there are both debit and credit limit type
					if( form.elements[i+offset].name.indexOf("Debit") != -1) {
						if( !checkPeriodRelation( form, "Debit", suffix, i ) ) {
							return false;
						}
					}
				}
			}
		}
	}
	return true;
}

/*
 * Check two limits of the same type(credit/debit) for an entitlement type
 */
function checkPeriodRelation( form, limitType, suffix, index ) {
	var periodName = new Array('perBatch', 'daily');
	var periodType = new Array('Per batch', 'Daily');
	prevMin = 0;
	prevMinPeriod = -1;
	for( j = 0; j < 2; j++ ) {
		amountString = form.elements[periodName[j] + limitType + suffix].value;

		if( !isNumerical( amountString ) ) {
			return false;
		} else if ( amountString.length > <%= com.ffusion.tasks.Task.MAX_PER_ACCOUNT_LIMIT_LENGTH %> ) {
			alert('<s:text name="jsp.user_256"/> <%= com.ffusion.tasks.Task.MAX_PER_ACCOUNT_LIMIT_LENGTH %> <s:text name="jsp.user_115"/>');
			return false;
		} else {
			if( amountString.charAt(0) == '+' || amountString.charAt(0) == '-' ) {
				alert( periodType[j] + " " + limitType.toLowerCase() + " limit for " + form.elements[index].value + " <s:text name="jsp.user_54"/>" );
				return false;
			}
			amount = parseFloat( amountString );
			if (amount < 0) {
				//this should never happen, but we check it just in case
				alert( periodType[j] + " " + limitType.toLowerCase() + " limit for " + form.elements[index].value + " <s:text name="jsp.user_180"/>" );
				return false;
			}
			if( prevMinPeriod == -1 ) {
				//this is first one with data
				prevMinPeriod = j;
				prevMin = amount;
			} else {
				if (amount < prevMin) {
					alert( periodType[prevMinPeriod] + ' ' + limitType.toLowerCase() + " limit for " + form.elements[index].value + " <s:text name="jsp.user_181"/> " + periodName[j] + ' ' + limitType.toLowerCase() + " <s:text name="jsp.user_187"/>" );
					return false;
				}
			}
		}

	}
	return true;
}

/*
 * Check if a given amount is numerical
 */
function isNumerical( amountString ) {
	var validChars = "0123456789";
	points = 0;
	startIndex = 0;
	if( amountString.length==1 ) {
		if( validChars.indexOf(amountString.charAt( 0 ) ) < 0 ) {
			alert( amountString + " <s:text name="jsp.user_178"/>" );
			return false;
		} else {
			return true;
		}
	} else if( amountString.length>1 ) {
		if( amountString.charAt(0) == '+' || amountString.charAt(0) == '-' ) {
			startIndex = 1;
		}
		for( n = startIndex; n < amountString.length; n++ ) {
			if( amountString.charAt( n )=='.' ) {
				points++;
				if( points>1 ) {
					alert( amountString + " <s:text name="jsp.user_178"/>" );
					return false;
				}
			} else if( validChars.indexOf( amountString.charAt( n ) ) < 0 ) {
				alert( amountString + " <s:text name="jsp.user_178"/>" );
				return false;
			}
		}
		return true;
	} else {
		//the amountString is empty
		return true;
	}
}

// --></script>
