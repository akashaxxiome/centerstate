<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_141')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_141"/></span>
<ffi:help id="user_corpadmindivedit" className="moduleHelpClass"/>
<%
	String EditDivision_GroupId = request.getParameter("EditDivision_GroupId");
	if(EditDivision_GroupId!= null){
		session.setAttribute("EditDivision_GroupId", EditDivision_GroupId);
	}
	
	String EditGroup_GroupId = request.getParameter("EditGroup_GroupId");
	if(EditGroup_GroupId!= null){
		session.setAttribute("EditGroup_GroupId", EditGroup_GroupId);
	}
	
	String EditDivision_ACHCompID = request.getParameter("EditDivision_ACHCompID");
	if(EditDivision_ACHCompID!= null){
		session.setAttribute("EditDivision_ACHCompID", EditDivision_ACHCompID);
	}
	
	String EntGroupItemACHCompanyName = request.getParameter("EntGroupItemACHCompanyName");
	if(EntGroupItemACHCompanyName!= null){
		session.setAttribute("EntGroupItemACHCompanyName", EntGroupItemACHCompanyName);
	}
		
	String Section = request.getParameter("Section");
	if(Section!= null){
		session.setAttribute("Section", Section);
	}
	
	String viewOnly = request.getParameter("viewOnly");
	if(viewOnly!= null){
		session.setAttribute("viewOnly", viewOnly);
	}
	
	
	//if flag is sent in request then give priority over session.
	String UseLastRequest = request.getParameter("UseLastRequest");	
	if(UseLastRequest != null && !"".equals(UseLastRequest)){
		session.setAttribute("UseLastRequest", UseLastRequest);
	}	
%>

<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
	<ffi:setL10NProperty name='PageHeading' value='Edit Division'/>
</ffi:cinclude>
<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="equals">
	<ffi:setL10NProperty name='PageHeading' value='View Division'/>
</ffi:cinclude>
<ffi:setProperty name='PageText' value=''/>

<%-- save the group name - if we are coming back from admin edit, we need the possibly modified group name, not the one from the DA object --%>
	<ffi:setProperty name="saveGroupName" value="${AddBusinessGroup.groupName}"/>	

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="DAEntitlementGroupObject" name="com.ffusion.csil.beans.entitlements.EntitlementGroup" />

<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
	<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="DAEntitlementGroupObject"/>
<ffi:process name="PopulateObjectWithDAValues"/>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
	<ffi:object id="AddBusinessGroup" name="com.ffusion.tasks.admin.AddBusinessGroup" scope="session"/>	
	<ffi:cinclude value1="${DAEntitlementGroupObject.GroupName}" value2="" operator="notEquals">
		<ffi:setProperty name="AddBusinessGroup" property="GroupName" value="${DAEntitlementGroupObject.GroupName}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${DAEntitlementGroupObject.ParentId}" value2="0" operator="notEquals">
  	    <ffi:setProperty name="AddBusinessGroup" property="ParentGroupId" value="${DAEntitlementGroupObject.ParentId}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${DAEntitlementGroupObject.EntGroupType}" value2="" operator="notEquals">
  	    <ffi:setProperty name="AddBusinessGroup" property="GroupType" value="${DAEntitlementGroupObject.EntGroupType}"/>
	</ffi:cinclude>
	<% session.setAttribute("FFIAddBusinessGroup", session.getAttribute("AddBusinessGroup")); %>
</ffi:cinclude>
	
	

<ffi:removeProperty name="DAEntitlementGroupObject"/>
	
<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="TempBusinessEmployee"/>

<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
	<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value=" "/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntToCheck" value="<%=EntitlementsDefines.DIVISION_MANAGEMENT%>"/>
	<ffi:process name="GenerateUserAndAdminLists"/>
	<ffi:removeProperty name="adminIds"/>
	<ffi:removeProperty name="userIds"/>
	<ffi:removeProperty name="GenerateUserAndAdminLists"/>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditDivision_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
	<ffi:process name="GenerateListsFromDAAdministrators"/>
	
	<ffi:object id="GenerateUserAdminList" name="com.ffusion.tasks.dualapproval.GenerateUserAdminList"/>
	<ffi:setProperty name="GenerateUserAdminList" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateUserAdminList" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateUserAdminList" property="sessionName" value="businessAdminEmployees"/>
	<ffi:process name="GenerateUserAdminList"/>
	<% session.setAttribute("tempAdminEmps",session.getAttribute("businessAdminEmployees")); %>
	
</ffi:cinclude>

<!-- Take the value set from DA object -->
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="equals">
	<ffi:setProperty name="AddBusinessGroup" property="groupName" value="${saveGroupName}"/>
</ffi:cinclude>
<ffi:removeProperty name="saveGroupName"/>


<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="NewAdminStringList"/>
<ffi:removeProperty name="OldAdminEmployees"/>
<ffi:removeProperty name="OldAdminGroups"/>
<ffi:removeProperty name="Admins"/>
<ffi:removeProperty name="BusinessEmployees"/>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>
<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<s:form id="editAddDivFormId" namespace="/pages/user" action="addBusinessDivision-verify" theme="simple" name="editDivisionAddFormName" method="post">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>												
							<input type="hidden" name="AddBusinessGroup.ParentGroupId" value="<ffi:getProperty name="Business" property="EntitlementGroupId"/>">
							<input type="hidden" name="AddBusinessGroup.GroupType" value="Division">
							<input type="hidden" name="UseLastRequest" value="TRUE">
							<input type="hidden" name="divAdd" value="true">
							<input type="hidden" name="ItemId" value="<ffi:getProperty name="EditDivision_GroupId"/>">
							<input type="hidden" name="AddBusinessGroup.CheckboxesAvailable" value="false">
							<input type="hidden" name="AddBusinessGroup.SuccessURL" value="<ffi:urlEncrypt url="${SecurePath}redirect.jsp?target=${SecurePath}user/corpadmindiv.jsp" />">
							
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td width="40">&nbsp;</td>
									<td width="100" class="sectionheadDA"><s:text name="jsp.user_121"/><span class="required">*</span></td>
									<td>
										 <ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
											<input class="ui-widget-content ui-corner-all" type="text" name="GroupName" style="width:120px;" size="24" maxlength="255" border="0" value="<ffi:getProperty name="AddBusinessGroup" property="GroupName"/>">
										</ffi:cinclude>
										<ffi:cinclude value1="${viewOnly}" value2="TRUE">
											<span class="columndata">
												<ffi:getProperty name="AddBusinessGroup" property="GroupName"/>
											</span>
										</ffi:cinclude>
										 (<ffi:getProperty name="Business" property="BusinessName"/>) &nbsp;&nbsp; <span id="GroupNameError"></span>
									</td>
								</tr>
								<tr>
									<ffi:setProperty name="AddBusinessGroup" property="AdminListName" value="adminMembers"/>
									<td width="40">&nbsp;</td>
									<td valign="middle"  class="sectionheadDA"><s:text name="jsp.user_38"/></td>
									<td valign="middle">
										<div style="display:inline-block; ">
											<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
												<ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="tempAdminEmps"/>
												<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
											<ffi:process name="FilterNotEntitledEmployees"/>
	 										<ffi:setProperty name="tempAdminEmps" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
											<%--display name of each employee in BusinessEmployees--%>
		 									
		 									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
											<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="tempAdminEmps"/>
											<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="AdministratorStr"/>
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
												<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
												<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
											</ffi:cinclude>
											<ffi:setProperty name="GenerateCommaSeperatedList" property="maxLength" value="120"/>
											<ffi:process name="GenerateCommaSeperatedList"/>
											<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									
 											<ffi:removeProperty name="AdminsSelected"/>	
	 										<ffi:getProperty name="AdministratorStr"/>
											<ffi:cinclude value1="AdministratorStr" value2="" operator="equals">
												<!--L10NStart-->None<!--L10NEnd-->
											</ffi:cinclude>	
											<%-- set modifiedAdmins to "TRUE", so when the edit icon is clicked the corpadminadminedit.jsp --%>
											<%-- will look for the appropriate object in session for displaying --%>
											<ffi:setProperty name="modifiedAdmins" value="TRUE"/>
											<ffi:removeProperty name="empName"/>
											<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
												<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminadminedit.jsp?UseLastRequest=TRUE&divAdd=true" URLEncrypt="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${viewOnly}" value2="TRUE">
												<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminadminview.jsp?UseLastRequest=TRUE&divAdd=true" URLEncrypt="true"/>
											</ffi:cinclude>
										</div>
									   &nbsp;&nbsp;
									   
									   <ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">									
													<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
											
														<sj:a
															button="true"
															targets="editAdminerDialogId"
															formIds="editAddDivFormId"
															postaction="/cb/pages/user/addBusinessDivision-editAddDivModifyAdminerChange.action"
															formid="editAddDivFormId"
															onClickTopics="postOnlyFormTopic"
															onCompleteTopics="openEditAddDivisionAdminerDialog"><s:text name="jsp.default_178" />
													</sj:a>
												 </ffi:cinclude>
												<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
													<sj:a
														button="true"
														targets="editAdminerDialogId"
														formIds="editAddDivFormId"												
														postaction="/cb/pages/user/addBusinessDivision-editAddDivModifyAdminerChange.action"
														formid="editAddDivFormId"
														onClickTopics="postOnlyFormTopic"
														onCompleteTopics="openEditAddDivisionAdminerDialog"><s:text name="jsp.default_178" />
														</sj:a>		
												
												</ffi:cinclude>
											</ffi:cinclude>
											<ffi:cinclude value1="${viewOnly}" value2="TRUE">																
												<s:url id="addDivisionAdministratorsViewURL" value="%{#session.PagesPath}user/corpadminadminview.jsp" escapeAmp="false">
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
													<s:param name="UseLastRequest" value="%{'TRUE'}"/>
													<s:param name="divAdd" value="%{'true'}"/>
												</s:url>
												<sj:a
													href="%{addDivisionAdministratorsViewURL}" 
													targets="viewAdminInViewDiv" 
													button="true" 
													title="View"
													onCompleteTopics="viewAdminInViewDivTopic">
													<s:text name="jsp.default_6" />
												</sj:a>												
											</ffi:cinclude>
																												
											<ffi:removeProperty name="tmp_url"/>
										
										</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="3">
										<div align="center">
												<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
												<span class="required">* <s:text name="jsp.default_240"/></span>
												<br>
												<span class="required">* <s:text name="user.division.minimum.two.users.required"/></span>
												<br>
												<br>
												
												<s:url id="editDADivisionResetURL" value="%{#session.PagesPath}user/da-new-division-edit.jsp" escapeAmp="false">
														<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
														<s:param name="UseLastRequest">FALSE</s:param>	
													</s:url>
													<sj:a 
														href="%{editDADivisionResetURL}" 
														targets="inputDiv"
														button="true" 
													><s:text name="jsp.default_358"/></sj:a>
												</ffi:cinclude>
												
												<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
													<sj:a
													   button="true"
													    summaryDivId="summary" buttonType="cancel"
													   onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
												</ffi:cinclude>
												<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
												<sj:a
													formIds="editAddDivFormId"
													targets="verifyDiv"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify2"
													onCompleteTopics="completeVerify2"
													onErrorTopics="errorVerify"
													onSuccessTopics="closeDialog"
													formid="editAddDivFormId"
													field1="needVerify_HiddenFieldId"
													value1="true"
													postaction="/cb/pages/user/addBusinessDivision-addNewEditedDivision.action"																								
													onClickTopics="postFormTopic"		 
													><s:text name="jsp.default_29"/></sj:a>
												</ffi:cinclude>
													
											</div>
									</td>
								</tr>
								<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="equals">
									<tr>
										<td colspan="3" align="center">
											<div align="center" class="ui-widget-header customDialogFooter">
												<sj:a button="true" title="Close" onClickTopics="closeDialog,cancelDivisionForm"><s:text name="jsp.default_175" /></sj:a>
											</div>
										</td>
									</tr>
								</ffi:cinclude>
							</table>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
		
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="businessAdminEmployees"/>
<ffi:removeProperty name="viewOnly"/>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-new-division-edit.jsp?UseLastRequest=TRUE&AdminsSelected=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-new-division-edit.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:setProperty name="onCancelGoto" value="da-new-division-edit.jsp"/>
<ffi:setProperty name="onDoneGoto" value="da-new-division-edit.jsp"/>
