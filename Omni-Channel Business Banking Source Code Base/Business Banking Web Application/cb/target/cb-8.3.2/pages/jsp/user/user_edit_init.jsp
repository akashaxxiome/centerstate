<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:removeProperty name="OriginalUser"/>
<ffi:removeProperty name="Password"/>
<ffi:removeProperty name="ConfirmPassword"/>
<ffi:removeProperty name="AddModeFlipped"/>
<ffi:removeProperty name="<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_CORE_ACCOUNT %>" startsWith="true"/>
<ffi:removeProperty name="<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_ACCOUNT %>" startsWith="true"/>
<ffi:removeProperty name="<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>" startsWith="true"/>
<ffi:removeProperty name="<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>" startsWith="true"/>
<ffi:removeProperty name="EnableInternalTransfers"/>
<ffi:removeProperty name="EnableExternalTransfers"/>
<ffi:removeProperty name="EnableBillPayments"/>
<ffi:removeProperty name="EnableManagePayees"/>
<ffi:removeProperty name="IsTransfersPermissionsInitialized"/>
<ffi:removeProperty name="IsExternalTransfersPermissionsInitialized"/>
<ffi:removeProperty name="IsBillPaymentPermissionsInitialized"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>' startsWith="true"/>

<ffi:setProperty name="PasswordModified" value=""/>

<%-- 
	Commenting this code to get the User status from DB not from session.
<ffi:setProperty name="FFISetSecondaryUsersActiveState" property="SuccessURL" value=""/>
<ffi:process name="FFISetSecondaryUsersActiveState"/>
 --%>
<ffi:object name="com.ffusion.tasks.multiuser.EditSecondaryUser" id="FFIEditSecondaryUser" scope="session"/>

<ffi:object name="com.ffusion.tasks.banking.SignOn" id="UserAddEditBankingSignOn" scope="session"/>
<ffi:setProperty name="UserAddEditBankingSignOn" property="ServiceName" value="UserAddEditBankingService"/>
<ffi:setProperty name="UserAddEditBankingSignOn" property="UserName" value="${TransID}"/>
<ffi:setProperty name="UserAddEditBankingSignOn" property="Password" value="${TransPassword}"/>
<ffi:process name="UserAddEditBankingSignOn"/>


<%-- Get account collections containing primary user account settings necessary for the account setup page. --%>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_AGGREGATION %>'>
    <%--
    Get a separate accounts collection for external accounts so as not to impact the BankingAccoungs collection which
    will be used for non-external account purposes.
    --%>
    <ffi:object name="com.ffusion.tasks.accounts.GetAccounts" id="GetExternalAccounts" scope="request"/>
    <ffi:setProperty name="GetExternalAccounts" property="AccountsName" value="ExternalAccounts"/>
    <ffi:setProperty name="GetExternalAccounts" property="Reload" value="true"/>
    <ffi:process name="GetExternalAccounts"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_AGGREGATION %>'>
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="ExternalAccounts" scope="session"/>
</ffi:cinclude>

<ffi:object id="SetRegisterAccountsData" name="com.ffusion.tasks.register.SetRegisterAccountsData" scope="request"/>
<ffi:setProperty name="SetRegisterAccountsData" property="CollectionSessionName" value="ExternalAccounts"/>
<ffi:process name="SetRegisterAccountsData"/>

<ffi:setProperty name="ExternalAccounts" property="Filter" value="All"/>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
    <%-- Create a temporary ExtTransferAccounts bean for the GetAccountsForExtTransfer task. --%>
    <ffi:object name="com.ffusion.beans.exttransfers.ExtTransferAccounts" id="ExternalTransferAccounts" scope="session"/>

    <ffi:object name="com.ffusion.tasks.exttransfers.GetAccountsForExtTransfer" id="GetPrimaryUserExtTransferAccounts" scope="request"/>
    <ffi:setProperty name="GetPrimaryUserExtTransferAccounts" property="IsConsumer" value="TRUE"/>
    <ffi:setProperty name="GetPrimaryUserExtTransferAccounts" property="CustId" value="${SecureUser.ProfileID}"/>
    <ffi:setProperty name="GetPrimaryUserExtTransferAccounts" property="AccountsSessionName" value="ExternalTransferAccounts"/>
    <ffi:process name="GetPrimaryUserExtTransferAccounts"/>

    <ffi:object name="com.ffusion.tasks.exttransfers.GetExtTransferAccounts" id="GetPrimaryUserExtTransferAccountsFromBPW" scope="request"/>
    <ffi:setProperty name="GetPrimaryUserExtTransferAccountsFromBPW" property="UserId" value="${SecureUser.ProfileID}"/>
    <ffi:setProperty name="GetPrimaryUserExtTransferAccountsFromBPW" property="AccountsSessionName" value="ExternalTransferAccountsFromBPW"/>
    <ffi:process name="GetPrimaryUserExtTransferAccountsFromBPW"/>

    <%-- Update the ExternalTransferAccounts collection with status information in ExternalTransferAccountsFromBPW. --%>
    <ffi:list collection="ExternalTransferAccountsFromBPW" items="TempExternalTransferAccountFromBPW">
        <ffi:list collection="ExternalTransferAccounts" items="TempExternalTransferAccount">
            <ffi:cinclude value1="${TempExternalTransferAccountFromBPW.RoutingNumber}" value2="${TempExternalTransferAccount.RoutingNum}" operator="equals">
                <ffi:cinclude value1="${TempExternalTransferAccountFromBPW.Number}" value2="${TempExternalTransferAccount.Number}" operator="equals">
                    <ffi:cinclude value1="${TempExternalTransferAccountFromBPW.TypeValue}" value2="${TempExternalTransferAccount.TypeValue}" operator="equals">
                        <ffi:setProperty name="TempExternalTransferAccount" property="BPWSTATUS" value="${TempExternalTransferAccountFromBPW.StatusValue}"/>
						<ffi:setProperty name="TempExternalTransferAccount" property="VERIFYSTATUS" value="${TempExternalTransferAccountFromBPW.VerifyStatusString}"/>
                    </ffi:cinclude>
                </ffi:cinclude>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="ExternalTransferAccounts" scope="session"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>">
    <ffi:object name="com.ffusion.tasks.istatements.GetStatementEnabledAccts" id="GetPrimaryUserStatementEnabledAccounts" scope="request"/>
    <ffi:setProperty name="GetPrimaryUserStatementEnabledAccounts" property="StatementAccountsInSessionName" value="PrimaryUserStatementEnabledAccounts"/>
    <ffi:setProperty name="GetPrimaryUserStatementEnabledAccounts" property="AccountsInSessionName" value="BankingAccounts"/>
    <ffi:process name="GetPrimaryUserStatementEnabledAccounts"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>">
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="PrimaryUserStatementEnabledAccounts" scope="session"/>
</ffi:cinclude>


<ffi:cinclude value1="${SecondaryUserID}" value2="" operator="notEquals">
    <%-- Set the existing user in the session so that it can be used to initialize the appropriate task(s). --%>
    <ffi:object name="com.ffusion.tasks.user.SetUserInSession" id="SetUserInSession" scope="request"/>
    <ffi:setProperty name="SetUserInSession" property="UsersSessionName" value="ModifiedSecondaryUsers"/>
    <ffi:setProperty name="SetUserInSession" property="UserSessionName" value="OriginalUser"/>
    
    <ffi:setProperty name="SetUserInSession" property="Id" value="${SecondaryUserID}"/>
    <ffi:process name="SetUserInSession"/>

    <ffi:removeProperty name="SecondaryUserID"/>
</ffi:cinclude>

<ffi:cinclude value1="${OriginalUser}" value2="" operator="equals">
    <ffi:setProperty name="AddEditMode" value="Add"/>
    <%-- Create the task used to hold all the new user's profile information. --%>
    <ffi:object name="com.ffusion.tasks.multiuser.AddSecondaryUser" id="FFIAddSecondaryUser" scope="session"/>
    <ffi:setProperty name="FFIAddSecondaryUser" property="FieldsToValidate" value="FIRST_NAME,LAST_NAME,USER_NAME,USER_PASSWORD,USER_CONFIRM_PASSWORD"/>
    <ffi:setProperty name="FFIAddSecondaryUser" property="FieldsToVerify" value="PHONE,EMAIL,ZIPCODE,PASSWORD"/>
    <ffi:setProperty name="FFIAddSecondaryUser" property="SecondaryUsersSessionName" value="NewSecondaryUsers"/>

    <%-- Create account collections necessary for the accounts setup page. --%>
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="SecondaryUserEntitledCoreAccounts" scope="session"/>
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="SecondaryUserEntitledExternalAccounts" scope="session"/>
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="SecondaryUserEntitledRegisterAccounts" scope="session"/>
    <ffi:object name="com.ffusion.beans.accounts.Accounts" id="SecondaryUserEntitledStatementAccounts" scope="session"/>

    <%--
    Initialize the GetMaxLimits task used to determine the maximum limit that can be set on each of the permissions pages.
    The primary user is used to initialize the task since the secondary user belongs to the same entitlement group.
    --%>
    <ffi:object name="com.ffusion.tasks.multiuser.GetMaxLimits" id="GetSecondaryUserMaxLimits" scope="session"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="GroupID" value="${SecureUser.EntitlementID}"/>
    <ffi:process name="GetSecondaryUserMaxLimits"/>

    <%-- Create the objects necessary for the Internal Transfers setup page. --%>
    <ffi:setProperty name="EnableInternalTransfers" value="FALSE"/>

    <ffi:object name="com.ffusion.tasks.multiuser.TransferAccessAndLimits" id="FFITransferAccessAndLimits" scope="session"/>

    <%-- Create the objects necessary for the External Transfers setup page. --%>
    <ffi:setProperty name="EnableExternalTransfers" value="FALSE"/>

    <ffi:object name="com.ffusion.tasks.multiuser.ExternalTransferAccessAndLimits" id="FFIExternalTransferAccessAndLimits" scope="session"/>

    <%-- Create the objects necessary for the Bill Payments setup page. --%>
    <ffi:setProperty name="EnableBillPayments" value="FALSE"/>
    <ffi:setProperty name="EnableManagePayees" value="FALSE"/>

    <ffi:object name="com.ffusion.tasks.multiuser.BillPaymentAccessAndLimits" id="FFIBillPaymentAccessAndLimits" scope="session"/>
</ffi:cinclude>


<ffi:cinclude value1="${OriginalUser}" value2="" operator="notEquals">
    <ffi:setProperty name="AddEditMode" value="Edit"/>

    <%-- Set the default action to take if the user being edited is deactivated and has pending transactions. --%>
    <ffi:setProperty name="TransferOrCancelTransactions" value="Transfer"/>


    <%-- Retrieve collections of pending transactions that may have been created by the user to be deleted. --%>
    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
		<%-- Initialize tasks and data structures necessary for determining if the user to be edited has any pending payments. --%>
		<%--
		<ffi:object name="com.ffusion.tasks.billpay.Initialize" id="BillPayInitService" scope="request"/>
		<ffi:setProperty name="BillPayInitService" property="ClassName" value="com.ffusion.services.billpay.BillPayServiceImpl"/>
		<ffi:setProperty name="BillPayInitService" property="InitURL" value="billpay.xml"/>
		<ffi:process name="BillPayInitService"/>
		--%>

		<ffi:object name="com.ffusion.tasks.billpay.SignOn" id="BillPaySignOn" scope="request"/>
		<ffi:setProperty name="BillPaySignOn" property="UserName" value="${TransID}"/>
		<ffi:setProperty name="BillPaySignOn" property="Password" value="${TransPassword}"/>
		<ffi:process name="BillPaySignOn"/>

		<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

		<ffi:object name="com.ffusion.tasks.billpay.GetAccounts" id="GetBillPayAccounts" scope="request"/>
		<ffi:setProperty name="GetBillPayAccounts" property="AccountReload" value="true"/>
		<ffi:setProperty name="GetBillPayAccounts" property="UseAccounts" value="BankingAccounts" />
		<ffi:process name="GetBillPayAccounts" />

		<ffi:object name="com.ffusion.tasks.billpay.GetExtPayees" id="GetPayees" scope="request"/>
		<ffi:setProperty name="GetPayees" property="Reload" value="true"/>
		<ffi:process name="GetPayees" />

		<ffi:object name="com.ffusion.tasks.billpay.GetPayments" id="GetPayments" scope="request"/>
		<ffi:setProperty name="GetPayments" property="Reload" value="true"/>
		<ffi:setProperty name="GetPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:process name="GetPayments" />

		<ffi:setProperty name="RecPayments" property="	Locale" value="${UserLocale.Locale}" />
		<ffi:setProperty name="Payments" property="Locale" value="${UserLocale.Locale}" />
    </ffi:cinclude>
    <ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
        <ffi:object name="com.ffusion.beans.billpay.RecPayments" id="RecPayments" scope="session"/>
        <ffi:object name="com.ffusion.beans.billpay.Payments" id="Payments" scope="session"/>
    </ffi:cinclude>

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
        <%-- Initialize tasks and data structures necessary for determining if the user to be deleted has any pending transfers. --%>
        <ffi:object name="com.ffusion.tasks.bptw.GetTransfers" id="GetRecTransfers" scope="request"/>
        <ffi:setProperty name="GetRecTransfers" property="Recurring" value="true"/>
		<ffi:setProperty name="GetRecTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
        <ffi:process name="GetRecTransfers"/>

        <ffi:object name="com.ffusion.tasks.bptw.GetTransfers" id="GetTransfers" scope="request"/>
        <ffi:setProperty name="GetTransfers" property="Recurring" value="false"/>
		<ffi:setProperty name="GetTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
        <ffi:process name="GetTransfers"/>

        <ffi:object name="com.ffusion.tasks.banking.UpdateTransferAccountInfo" id="UpdateRecTransfers" scope="request"/>
        <ffi:setProperty name="UpdateRecTransfers" property="AccountsSessionName" value="BankingAccounts"/>
        <ffi:setProperty name="UpdateRecTransfers" property="TransfersSessionName" value="RecTransfers"/>
        <ffi:process name="UpdateRecTransfers"/>

        <ffi:object name="com.ffusion.tasks.banking.UpdateTransferAccountInfo" id="UpdateTransfers" scope="request"/>
        <ffi:setProperty name="UpdateTransfers" property="AccountsSessionName" value="BankingAccounts"/>
        <ffi:setProperty name="UpdateTransfers" property="TransfersSessionName" value="Transfers"/>
        <ffi:process name="UpdateTransfers"/>
    </ffi:cinclude>
    <ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
        <ffi:object name="com.ffusion.beans.banking.RecTransfers" id="RecTransfers" scope="session"/>
        <ffi:object name="com.ffusion.beans.banking.Transfers" id="Transfers" scope="session"/>
    </ffi:cinclude>

    <%-- Create the task used to hold all the existing user's profile information. --%>
    <ffi:object name="com.ffusion.tasks.multiuser.EditSecondaryUser" id="FFIEditSecondaryUser" scope="session"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="SecondaryUserSessionName" value="OriginalUser"/>
    <ffi:setProperty name="SecondaryUser" property="FieldsToValidate" value="FIRST_NAME,LAST_NAME,USER_NAME,USER_CONFIRM_PASSWORD"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="FieldsToVerify" value="PHONE,EMAIL,ZIPCODE,PASSWORD"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="SecondaryUsersSessionName" value="NewSecondaryUsers"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="BankingServiceName" value="UserAddEditBankingService"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="RecPaymentsSessionName" value="RecPayments"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="PaymentsSessionName" value="Payments"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="RecTransfersSessionName" value="RecTransfers"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="TransfersSessionName" value="Transfers"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="PrimaryUserAccountsSessionName" value="BankingAccounts"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="Initialize" value="TRUE"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="Process" value="FALSE"/>
    <ffi:process name="FFIEditSecondaryUser"/>

    <ffi:setProperty name="FFIEditSecondaryUser" property="Initialize" value="FALSE"/>
    <ffi:setProperty name="FFIEditSecondaryUser" property="Process" value="TRUE"/>

    <%-- Get account collections containing existing account settings necessary for the account setup page. --%>
    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE!1,COREACCOUNT=1,AND"/>
    <ffi:object name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" id="GetSecondaryUserAccounts" scope="request"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="AccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="FilteredAccountsName" value="SecondaryUserEntitledCoreAccounts"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="ClearFilter" value="Dummy"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="EntitlementFilter" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="GroupId" value="${SecureUser.EntitlementID}"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ENT_MEMBER_TYPE_USER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberSubType" value='<%= "" + com.ffusion.beans.SecureUser.TYPE_CUSTOMER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberId" value="${FFIEditSecondaryUser.Id}"/>
    <ffi:process name="GetSecondaryUserAccounts"/>

    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE!1,COREACCOUNT=0,AND"/>
    <ffi:object name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" id="GetSecondaryUserAccounts" scope="request"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="AccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="FilteredAccountsName" value="SecondaryUserEntitledExternalAccounts"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="ClearFilter" value="Dummy"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="EntitlementFilter" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="GroupId" value="${SecureUser.EntitlementID}"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ENT_MEMBER_TYPE_USER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberSubType" value='<%= "" + com.ffusion.beans.SecureUser.TYPE_CUSTOMER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberId" value="${FFIEditSecondaryUser.Id}"/>
    <ffi:process name="GetSecondaryUserAccounts"/>

    <%--
    The ExternalAccounts collection is sufficient for the purposes of getting all the register enabled accounts
    since it contains both core and external accounts.
    --%>
    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE!1,REG_ENABLED==true,AND"/>
    <ffi:object name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" id="GetSecondaryUserAccounts" scope="request"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="AccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="FilteredAccountsName" value="SecondaryUserEntitledRegisterAccounts"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="ClearFilter" value="Dummy"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="EntitlementFilter" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="EntitlementFilter" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="GroupId" value="${SecureUser.EntitlementID}"/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ENT_MEMBER_TYPE_USER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberSubType" value='<%= "" + com.ffusion.beans.SecureUser.TYPE_CUSTOMER %>'/>
    <ffi:setProperty name="GetSecondaryUserAccounts" property="MemberId" value="${FFIEditSecondaryUser.Id}"/>
    <ffi:process name="GetSecondaryUserAccounts"/>

    <ffi:setProperty name="SecondaryUserEntitledCoreAccounts" property="Filter" value="COREACCOUNT=1,HIDE!1,AND"/>
    <ffi:setProperty name="SecondaryUserEntitledRegisterAccounts" property="Filter" value="HIDE!1,AND"/>

    <ffi:cinclude ifEntitled="OnlineStatement">
        <ffi:object name="com.ffusion.tasks.istatements.GetStatementEnabledAccts" id="GetSecondaryUserStatementEnabledAccounts" scope="request"/>
        <ffi:setProperty name="GetSecondaryUserStatementEnabledAccounts" property="StatementAccountsInSessionName" value="SecondaryUserEntitledStatementAccounts"/>
        <ffi:setProperty name="GetSecondaryUserStatementEnabledAccounts" property="AccountsInSessionName" value="SecondaryUserEntitledCoreAccounts"/>
        <ffi:setProperty name="GetSecondaryUserStatementEnabledAccounts" property="UserID" value="${FFIEditSecondaryUser.Id}"/>
        <ffi:process name="GetSecondaryUserStatementEnabledAccounts"/>
    </ffi:cinclude>
    <ffi:cinclude ifNotEntitled="OnlineStatement">
        <ffi:object name="com.ffusion.beans.accounts.Accounts" id="SecondaryUserEntitledStatementAccounts" scope="session"/>
    </ffi:cinclude>

    <%--
    Initialize the GetMaxLimits task used to determine the maximum limit that can be set on each of the permissions pages.
    The primary user is used to initialize the task since the secondary user belongs to the same entitlement group.
    --%>
    <ffi:object name="com.ffusion.tasks.multiuser.GetMaxLimits" id="GetSecondaryUserMaxLimits" scope="session"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="GroupID" value="${FFIEditSecondaryUser.EntitlementGroupId}"/>
    <ffi:process name="GetSecondaryUserMaxLimits"/>


    <ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" id="CheckSecondaryUserEntitlements" scope="request"/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="GroupId" value="${SecureUser.EntitlementID}"/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="MemberType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ENT_MEMBER_TYPE_USER %>'/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="MemberSubType" value='<%= "" + com.ffusion.beans.SecureUser.TYPE_CUSTOMER %>'/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="MemberId" value="${FFIEditSecondaryUser.Id}"/>

    <%-- Create the objects necessary for the Internal Transfers setup page. --%>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="OperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="AttributeName" value="EnableInternalTransfers"/>
    <ffi:process name="CheckSecondaryUserEntitlements"/>

    <ffi:object name="com.ffusion.tasks.multiuser.TransferAccessAndLimits" id="FFITransferAccessAndLimits" scope="session"/>

    <%-- Create the objects necessary for the External Transfers setup page. --%>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="OperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>'/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="AttributeName" value="EnableExternalTransfers"/>
    <ffi:process name="CheckSecondaryUserEntitlements"/>

    <ffi:object name="com.ffusion.tasks.multiuser.ExternalTransferAccessAndLimits" id="FFIExternalTransferAccessAndLimits" scope="session"/>

    <%-- Create the objects necessary for the Bill Payments setup page. --%>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="OperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="AttributeName" value="EnableBillPayments"/>
    <ffi:process name="CheckSecondaryUserEntitlements"/>

    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="OperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYEES %>'/>
    <ffi:setProperty name="CheckSecondaryUserEntitlements" property="AttributeName" value="EnableManagePayees"/>
    <ffi:process name="CheckSecondaryUserEntitlements"/>

    <ffi:object name="com.ffusion.tasks.multiuser.BillPaymentAccessAndLimits" id="FFIBillPaymentAccessAndLimits" scope="session"/>
</ffi:cinclude>

<ffi:object name="com.ffusion.tasks.multiuser.SetupAccounts" id="FFISetupAccounts" scope="session"/>
<ffi:object name="com.ffusion.tasks.multiuser.EditSecondaryUser" id="FFIUnlockedSecondaryUser" scope="session"/>
<ffi:object name="com.ffusion.tasks.multiuser.EditSecondaryUser" id="FFIResetPQASecondaryUser" scope="session"/>
 
<ffi:setProperty name="FFISetupAccounts" property="CandidateCoreAccountsSessionName" value="BankingAccounts"/>
<ffi:setProperty name="FFISetupAccounts" property="CandidateExternalAccountsSessionName" value="ExternalAccounts"/>
<ffi:setProperty name="FFISetupAccounts" property="CandidateStatementAccountsSessionName" value="PrimaryUserStatementEnabledAccounts"/>
<ffi:setProperty name="FFISetupAccounts" property="SecondaryUserCoreAccountsSessionName" value="SecondaryUserEntitledCoreAccounts"/>
<ffi:setProperty name="FFISetupAccounts" property="SecondaryUserStatementAccountsSessionName" value="SecondaryUserEntitledStatementAccounts"/>
<ffi:setProperty name="FFISetupAccounts" property="Initialize" value="TRUE"/>
<ffi:setProperty name="FFISetupAccounts" property="Process" value="FALSE"/>
<ffi:setProperty name="FFISetupAccounts" property="CoreAccountsSortFields" value="NICKNAME,ID" />
<ffi:setProperty name="FFISetupAccounts" property="ExternalAccountsSortFields" value="ConsumerDisplayText,ID" />
<ffi:process name="FFISetupAccounts"/>

<ffi:removeProperty name="BptwBanking"/>