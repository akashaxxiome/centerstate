<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:help id="user_corpadminlocadd-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_144')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request" />
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${AddLocation.CurrencyCode}"/>

<ffi:cinclude value1="${AffiliateBanks}" value2="" operator="equals">
	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
</ffi:cinclude>


<%-- get cashcon company list --%>
<ffi:cinclude value1="${CashConCompanies}" value2="" operator="equals">
	<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
	<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
	<ffi:process name="GetCashConCompanies"/>
	<ffi:removeProperty name="GetCashConCompanies"/>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="" operator="notEquals">
	<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${AddLocation.CashConCompanyBPWID}"/>
	<ffi:process name="SetCashConCompany"/>
</ffi:cinclude>

<%-- based on selected concentration account, set DepositEnabled --%>
<ffi:setProperty name="DepositEnabled" value="false"/>
<ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="" operator="notEquals">
	<ffi:setProperty name="DepositEnabled" value="true"/>
</ffi:cinclude>

<%-- based on selected disbursement account, set DisbursementEnabled --%>
<ffi:setProperty name="DisbursementEnabled" value="false"/>
<ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="" operator="notEquals">
	<ffi:setProperty name="DisbursementEnabled" value="true"/>
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>

<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
	 <ffi:cinclude value1="${CashConCompany.ConcSameDayCutOffDefined}" value2="true" operator="equals">
	 		<ffi:setProperty name="showConcSameDayPrenote" value="true"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.ConcSameDayCutOffDefined}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.DisbSameDayCutOffDefined}" value2="true" operator="equals">
			<ffi:setProperty name="showDisbSameDayPrenote" value="true"/>
	 </ffi:cinclude>
	 <ffi:cinclude value1="${CashConCompany.DisbSameDayCutOffDefined}" value2="true" operator="notEquals">
	 		<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>
	 </ffi:cinclude>
 </ffi:cinclude>
<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="notEquals">
	<ffi:setProperty name="showConcSameDayPrenote" value="false"/>
	<ffi:setProperty name="showDisbSameDayPrenote" value="false"/>
</ffi:cinclude>


		<div align="center">
			<s:form id="AddEditLocationVerifyFormID" namespace="/pages/user" action="addLocation-execute" theme="simple" name="AddEditLocationVerifyForm" method="post">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="AddLocation.ProcessFlag" value="true"/>
			<table width="676" border="0" cellspacing="0" cellpadding="0">
				<%-- <tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionhead"><ffi:getProperty name="EditGroup_DivisionName"/><br>
								<br>
							</span>
						</div>
					</td>
				</tr> --%>
				<tr>
					<td align="center" class="adminBackground">&nbsp;</td>
				</tr>
			</table>
			<table   cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10">
				<tr>
					<td width="20%" valign="top">
						<%-- <span class="ffivisible marginTop10">&nbsp;</span> --%>
						<div class="paneWrapper marginTop20">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="admin.division.summary"/>
							</div>
							<div class="paneContentWrapper">
								<table cellspacing="0" cellpadding="3" border="0" width="100%">
									<tr>
										<td><s:text name="jsp.user_121"/>:&nbsp; <ffi:getProperty name="EditGroup_DivisionName"/></td>
									</tr>
								</table>
							</div>
							</div>
						</div>
					</td>
					<td width="3%">&nbsp;</td>				
					<td align="left" class="adminBackground" width="77%" valign="top">
					<%--
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="equals">
									<div>
										<ffi:include page="${PathExt}user/group_member_tree.jsp" />
									</div>
							</ffi:cinclude>
					--%>
					
					<%-- kaijie: FFI80 SP2 Iteration 2, close DA feature and autoEntitlement feature in location module. set autoEntitleLocations to false  --%>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
							<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard" />" >
							<ffi:setProperty name="AddLocation" property="autoEntitleLocation" value="false" />
					</ffi:cinclude>
							
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="notEquals">	
					<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard" />" >
					<TABLE width="98%" border="0" cellspacing="0" cellpadding="3">
						<TR>
							<TD colspan="2"  nowrap><span class="columndata"><s:text name="jsp.user_130"/></span>
							
							<input type="radio" name="AddLocation.AutoEntitleLocation" value="true" checked="checked"/> <s:text name="jsp.default_467"/>
								&nbsp;&nbsp;
								<input type="radio" name="AddLocation.AutoEntitleLocation" value="false"/> <s:text name="jsp.default_295"/>
							
							</TD>
						</TR>
					 </TABLE>			
			   		 </ffi:cinclude>
					   		 
					<table width="100%" border="0" cellspacing="0" cellpadding="3" class="largeFormContent">
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="admin.location.summary" /></div>
									<div class="blockContent">
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_267"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:getProperty name="AddLocation" property="LocationName"/>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_189"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:getProperty name="AddLocation" property="LocationID"/>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead"><input type="checkbox" name="AddLocation.Active" border="0" value="true" <ffi:cinclude value1="${AddLocation.Active}" value2="true" operator="equals">checked</ffi:cinclude> disabled ></span>
												<span class="sectionsubhead valueCls">
													<s:text name="jsp.default_28"/>
												</span>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_188"/></div>
									<div class="blockContent">
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_61"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">
														<ffi:getProperty name="AddLocation" property="LocalBankName"/> : <ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">
														<ffi:list collection="AffiliateBanks" items="Bank">
															<ffi:cinclude value1="${AddLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">
																<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
															</ffi:cinclude>
														</ffi:list>
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_15"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="false" operator="equals">
														<ffi:getProperty name="AddLocation" property="LocalAccountNumber"/> :
														<ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
															<s:text name="jsp.user_64"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
															<s:text name="jsp.user_281"/>
														</ffi:cinclude>
													</ffi:cinclude>
													<ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="true" operator="equals">
														<ffi:list collection="BankingAccounts" items="Account">
															<ffi:cinclude value1="${Account.ID}" value2="${AddLocation.LocalAccountID}" operator="equals">
																<ffi:getProperty name="Account" property="DisplayText"/>
																<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="Account" property="NickName"/>
																 </ffi:cinclude>
															</ffi:cinclude>
														</ffi:list>
													</ffi:cinclude>
												</span>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_276"/></div>
									<div class="blockContent">
										<div class="blockRow label140">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_55"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
														</ffi:cinclude>
													</ffi:list>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_75"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
														<s:text name="jsp.default_296"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="equals">
															<s:text name="jsp.default_296"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="notEquals">
															<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
																<ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">
																	<ffi:getProperty name="ConcAccount" property="DisplayText"/> - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																</ffi:cinclude>
															</ffi:list>
														</ffi:cinclude>
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_117"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
														<s:text name="jsp.default_296"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="equals">
															<s:text name="jsp.default_296"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="notEquals">
															<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
																<ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">
																	<ffi:getProperty name="DisbAccount" property="DisplayText"/> - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																</ffi:cinclude>
															</ffi:list>
														</ffi:cinclude>
													</ffi:cinclude>	
												</span>
											</div>
										</div>
									</div>
								</div>		
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_113"/></div>
									<div class="blockContent label140">
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_112"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${AddLocation.DepositMinimum}" value2="" operator="notEquals">
													<ffi:setProperty name="CurrencyObject" property="Amount" value="${AddLocation.DepositMinimum}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${AddLocation.DepositMinimum}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_111"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${AddLocation.DepositMaximum}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${AddLocation.DepositMaximum}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${AddLocation.DepositMaximum}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_44"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${AddLocation.AnticDeposit}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${AddLocation.AnticDeposit}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${AddLocation.AnticDeposit}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
										</div>
										<div class="blockRow">
											<div class="inlineBlock" style="width:33%">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_337"/>: </span>
												<span class="sectionsubhead valueCls">
													<ffi:cinclude value1="${AddLocation.ThreshDeposit}" value2="" operator="notEquals">
														<ffi:setProperty name="CurrencyObject" property="Amount" value="${AddLocation.ThreshDeposit}"/>
														<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${AddLocation.ThreshDeposit}" value2="" operator="equals">
														&nbsp;
													</ffi:cinclude>
												</span>
											</div>
											<div class="inlineBlock" style="width:33%">
												<span><input type="checkbox" name="AddLocation.ConsolidateDeposits" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${AddLocation.ConsolidateDeposits}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled> </span>
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_94"/> </span>
											</div>
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="notEquals">
												<div class="inlineBlock" style="width:33%">
													<span><input type="checkbox" name="AddLocation.DepositPrenote" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${AddLocation.DepositPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled> </span>
													 <span class="sectionsubhead sectionLabel"><s:text name="jsp.user_110"/> </span>
												</div>
											</ffi:cinclude>	
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="equals">
												<div class="inlineBlock" style="width:33%"><span></span><span></span></div>
											</ffi:cinclude>
										</div>
											<ffi:cinclude value1="${showConcSameDayPrenote}" value2="true" operator="equals">
											<div class="blockRow">
												<div class="inlineBlock" style="width:33%">
													<span  class="sectionsubhead sectionLabel"><s:text name="jsp.user_110"/>: </span>
													<span class="sectionsubhead valueCls"></span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span><input type="checkbox" name="AddLocation.DepositPrenote" value="true" border="0" 
															<ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals">
																<ffi:cinclude value1="${AddLocation.DepositPrenote}" value2="true" operator="equals">
																	<ffi:cinclude value1="${AddLocation.DepositSameDayPrenote}" value2="true" operator="notEquals">
																		checked
																	</ffi:cinclude>
																</ffi:cinclude>
															</ffi:cinclude> disabled></span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_571"/></span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span><input type="checkbox" name="AddLocation.DepositSameDayPrenote" value="true" border="0" <ffi:cinclude value1="${DepositEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${AddLocation.DepositSameDayPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled></span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_572"/></span>
												</div>
											</div>
										</ffi:cinclude>
									</div>
								</div>		
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td >
								<div class="blockWrapper">
									<div class="blockHead"><s:text name="jsp.user_119"/></div>
									<div class="blockContent">
										<div class="blockRow">
										 <ffi:cinclude value1="${showDisbSameDayPrenote}" value2="true" operator="notEquals">
												<div class="inlineBlock" style="width:99%">
													<span class="sectionsubhead"><input type="checkbox" name="AddLocation.DisbursementPrenote" value="true" border="0" <ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${AddLocation.DisbursementPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled></span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_118"/> </span>
												</div>
										    </ffi:cinclude>
											 <ffi:cinclude value1="${showDisbSameDayPrenote}" value2="true" operator="equals"> 
												<div class="inlineBlock" style="width:33%">
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_118"/>: </span>
													<span> </span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span><input type="checkbox" name="AddLocation.DisbursementPrenote" value="true" border="0" 
															<ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals">
																<ffi:cinclude value1="${AddLocation.DisbursementPrenote}" value2="true" operator="equals">
																	<ffi:cinclude value1="${AddLocation.DisbursementSameDayPrenote}" value2="true" operator="notEquals">
																		checked
																	</ffi:cinclude>	
																</ffi:cinclude>
														</ffi:cinclude> disabled></span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_571"/></span>
												</div>
												<div class="inlineBlock" style="width:33%">
													<span><input type="checkbox" name="AddLocation.DisbursementSameDayPrenote" value="true" border="0" <ffi:cinclude value1="${DisbursementEnabled}" value2="true" operator="equals"><ffi:cinclude value1="${AddLocation.DisbursementSameDayPrenote}" value2="true" operator="equals">checked</ffi:cinclude></ffi:cinclude> disabled></span>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_572"/></span>
												</div>
											 </ffi:cinclude>
										</div>
									</div>
								</div>		
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center">
								<sj:a id="backBtn2"
									    button="true"
									    onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
								<sj:a id="cancelBtn2"
									    button="true"
									    summaryDivId="summary" buttonType="cancel"
									    onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
								<sj:a id="addSubmit"
										formIds="AddEditLocationVerifyFormID"
										targets="confirmDiv"
										button="true"
										validate="false"
										onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="completeSubmit,redefineLocation"><s:text name="jsp.user_2"/></sj:a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</s:form>
</div>
