<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
<ffi:process name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
	</ffi:cinclude>
	<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
<ffi:process name="SetBusinessEmployee"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER%>" />
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}" />
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>" />
<ffi:process name="GetDACategoryDetails" />

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />
	
<ffi:object id="LanguageResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
<ffi:setProperty name="LanguageResource" property="ResourceFilename" value="com.ffusion.utilresources.languages" />
<ffi:process name="LanguageResource" />	
	
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" id="GetAdminsForGroup" scope="request"/>
	
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="request"/>
<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="GroupAdmins" />


<script type="text/javascript">
function reject()
{
	document.frmDualApproval.action = "rejectchangesda.jsp";
	document.frmDualApproval.submit();
}
</script>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >
<div align="center">
	<ffi:setProperty name="subMenuSelected" value="users"/>
	<%-- include page header --%>
	<ffi:include page="${PathExt}user/inc/nav_header.jsp" />
	<table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><img src="/cb/web/multilang/grafx/user/corner_al.gif" alt="" height="7" width="11" border="0"></td>
			<td class="adminBackground" align="center"><img src="/cb/web/multilang/grafx/user/spacer.gif" height="7" width="728" border="0" alt=""></td>
			<td align="right"><img src="/cb/web/multilang/grafx/user/corner_ar.gif" alt="" height="7" width="11" border="0"></td>
		</tr>
		<tr>
			<td style="background-image:URL(/cb/web/multilang/grafx/user/corner_cellbg.gif); background-repeat:repeat-y;"><br><br></td>
			<td align="center" class="adminBackground">
				<table width="708" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="left" class="adminBackground sectionhead" width="750">
							&gt; 
							<ffi:cinclude value1="${rejectFlag}" value2="y"  operator="notEquals">
								<!--L10NStart-->Verify Pending Changes for Approval<!--L10NEnd-->
							</ffi:cinclude>
							<ffi:cinclude value1="${rejectFlag}" value2="y" >
								<!--L10NStart-->Verify Pending Changes for Rejection<!--L10NEnd-->
							</ffi:cinclude>
						</td>
					</tr>
					<tr>
						<td>	
								<table width="750" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td width="110">&nbsp;</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
											<!--L10NStart-->User<!--L10NEnd-->
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="108" >
											<!--L10NStart-->Submitted By<!--L10NEnd-->
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
											<!--L10NStart-->Submitted On<!--L10NEnd-->
										</td>
										<td width="110">&nbsp;</td>
										<td width="110">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="1">&nbsp;</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" colspan="1"  >
											<ffi:getProperty name="userName"/>
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17"  colspan="1">
											<ffi:getProperty name="SubmittedUserName" property="userName"/>
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" colspan="3" >
											<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
										</td>
									</tr>
									<tr>
		<td class="sectionsubhead">&gt;<!--L10NStart-->Profile<!--L10NEnd--></td>
	</tr>
	<tr>
		<td colspan=7>
		<hr noshade size="1" style="width: 90%">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Field Name<!--L10NEnd--></td>
		<td colspan="1" class="sectionsubhead adminBackground"><!--L10NStart-->Old Value<!--L10NEnd--></td>
		<td colspan="4" class="sectionsubhead adminBackground"><!--L10NStart-->New Value<!--L10NEnd--></td>
	</tr>
	<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
		<ffi:cinclude value1="${details.fieldName}" value2="country">
			<ffi:setProperty name="oldCountry" value="${details.oldValue}"/>
			<ffi:setProperty name="newCountry" value="${details.newValue}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId">
			<ffi:setProperty name="oldEntitlementGroupId" value="${details.oldValue}"/>
			<ffi:setProperty name="newEntitlementGroupId" value="${details.newValue}"/>
		</ffi:cinclude>
	</ffi:list>
	<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
		<ffi:cinclude value1="${details.fieldName}" value2="confirmPassword" operator="notEquals">
		<tr>
			<td>&nbsp;</td>
			<td class="columndata"><ffi:getL10NString rsrcFile="cb"
				msgKey="da.field.${details.fieldName}" /></td>
			<td colspan="1" class="columndata">
				<ffi:cinclude value1="${details.fieldName}" value2="accountStatus">
					<ffi:setProperty name="Compare" property="Value1"
						value="${details.oldValue}" />
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Active<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Inactive<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Temporarily Inactive<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Pending Approval<!--L10NEnd-->
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="primaryAdmin">
					 <ffi:cinclude value1="${oldEntitlementGroupId}" value2="" operator="notEquals">
					 	<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${oldEntitlementGroupId}"/>
					 </ffi:cinclude>
					 <ffi:cinclude value1="${oldEntitlementGroupId}" value2="">
							<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
					 </ffi:cinclude>
					 <ffi:process name="GetAdminsForGroup"/>
					 <ffi:process name="GetBusinessEmployeesByEntGroups"/>
					 <ffi:list collection="GroupAdmins" items="employee">
						<ffi:cinclude value1="${details.oldValue}" value2="${employee.Id}">
						 	<ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
						</ffi:cinclude>
					 </ffi:list>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId">
					<ffi:list collection="Entitlement_EntitlementGroups" items="Group">
						<ffi:cinclude value1="${Group.GroupId}" value2="${details.oldValue}" operator="equals">
							<ffi:getProperty name="Group" property="GroupName"/>
						</ffi:cinclude>
					</ffi:list> 
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="country">
 					<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${details.oldValue}" />
					<ffi:getProperty name='CountryResource' property='Resource'/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="state">
					<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
	 					<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${oldCountry}" />
	 					<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${details.oldValue}" />
					<ffi:process name="GetStateProvinceDefnForState"/>
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
						<ffi:getProperty name="details" property="oldValue"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
						<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="preferredLanguage">
					<ffi:setProperty name="LanguageResource" property="ResourceID" value="${details.oldValue}" />
					<ffi:getProperty name='LanguageResource' property='Resource'/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="password">
					<!--L10NStart-->******<!--L10NEnd-->
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="passwordReminder">
					<!--L10NStart-->******<!--L10NEnd-->
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="passwordReminder2">
					<!--L10NStart-->******<!--L10NEnd-->
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="passwordClue">
					<!--L10NStart-->******<!--L10NEnd-->
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="passwordClue2">
					<!--L10NStart-->******<!--L10NEnd-->
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="accountStatus" operator="notEquals">
					<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId" operator="notEquals">
						<ffi:cinclude value1="${details.fieldName}" value2="country" operator="notEquals">
							<ffi:cinclude value1="${details.fieldName}" value2="state" operator="notEquals">
								<ffi:cinclude value1="${details.fieldName}" value2="preferredLanguage" operator="notEquals">
									<ffi:cinclude value1="${details.fieldName}" value2="primaryAdmin" operator="notEquals">
										<ffi:cinclude value1="${details.fieldName}" value2="password" operator="notEquals">
											<ffi:cinclude value1="${details.fieldName}" value2="passwordReminder" operator="notEquals">
												<ffi:cinclude value1="${details.fieldName}" value2="passwordReminder2" operator="notEquals">
													<ffi:cinclude value1="${details.fieldName}" value2="passwordClue" operator="notEquals">
														<ffi:cinclude value1="${details.fieldName}" value2="passwordClue2" operator="notEquals">
															<ffi:getProperty name="details" property="oldValue" />
														</ffi:cinclude>
													</ffi:cinclude>
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:cinclude>
									</ffi:cinclude>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</td>
			<td colspan="4" class="columndata">
				<ffi:cinclude value1="${details.fieldName}" value2="accountStatus">
					<ffi:setProperty name="Compare" property="Value1"
						value="${details.newValue}" />
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Active<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Inactive<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Temporarily Inactive<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="value2"
						value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>" />
					<ffi:cinclude value1="true" value2="${Compare.Equals}">
						<!--L10NStart-->Pending Approval<!--L10NEnd-->
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="primaryAdmin">
					<ffi:cinclude value1="${newEntitlementGroupId}" value2="" operator="notEquals">
						<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${newEntitlementGroupId}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${newEntitlementGroupId}" value2="">
						<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
					</ffi:cinclude>
					<ffi:process name="GetAdminsForGroup"/>
					<ffi:process name="GetBusinessEmployeesByEntGroups"/>
					<ffi:list collection="GroupAdmins" items="employee">
						<ffi:cinclude value1="${details.newValue}" value2="${employee.Id}">
						 	<ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
						</ffi:cinclude>
					</ffi:list>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId">
					<ffi:list collection="Entitlement_EntitlementGroups" items="Group">
						<ffi:cinclude value1="${Group.GroupId}" value2="${details.newValue}" operator="equals">
							<ffi:getProperty name="Group" property="GroupName"/>
						</ffi:cinclude>
					</ffi:list> 
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="country">
 					<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${details.newValue}" />
					<ffi:getProperty name='CountryResource' property='Resource'/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="state">
					<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
	 					<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${oldCountry}" />
	 					<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${details.newValue}" />
					<ffi:process name="GetStateProvinceDefnForState"/>
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
						<ffi:getProperty name="details" property="newValue"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
						<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="preferredLanguage">
					<ffi:setProperty name="LanguageResource" property="ResourceID" value="${details.newValue}" />
					<ffi:getProperty name='LanguageResource' property='Resource'/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="password">
					<!--L10NStart-->******<!--L10NEnd-->
				</ffi:cinclude>
				<ffi:cinclude value1="${details.fieldName}" value2="accountStatus" operator="notEquals">
					<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId" operator="notEquals">
						<ffi:cinclude value1="${details.fieldName}" value2="country" operator="notEquals">
							<ffi:cinclude value1="${details.fieldName}" value2="state" operator="notEquals">
								<ffi:cinclude value1="${details.fieldName}" value2="preferredLanguage" operator="notEquals">
									<ffi:cinclude value1="${details.fieldName}" value2="primaryAdmin" operator="notEquals">
										<ffi:cinclude value1="${details.fieldName}" value2="password" operator="notEquals">
											<ffi:getProperty name="details" property="newValue" />
										</ffi:cinclude>
									</ffi:cinclude>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</td>
		</tr>
		</ffi:cinclude>
	</ffi:list>
								</table>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td colspan="7"><hr size="1" style="width: 90%"></td></tr>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<tr>
								<td width="100%" >
								<table width="100%" >
									<tr>
										<td width="15%" class="columndata" align="center">
											<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-user-verify.jsp-2" parm0="${userName}"/>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<tr>
							<td width="100%" >
							<form name="frmDualApproval"  method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<table width="100%" >
								<tr>
									<td width="15%" class="sectionsubhead adminBackground" ><!--L10NStart-->Reject Reason :<!--L10NEnd--></td><td><textarea name="REJECTREASON" rows="2" cols="60"></textarea></td>
								</tr>
							</table>
							</form>
							</td>
						</tr>
					</ffi:cinclude>
					<tr><td align="center">
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<input type="button" class="submitbutton" value="Approve" onclick="location.href='<ffi:urlEncrypt url="da-user-add-confirm.jsp?itemId=${itemId}" />'" />
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<input class="submitbutton" type="button" value="Reject" onclick="reject();"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<input type="button" class="submitbutton" value="Approve" onclick="location.href='<ffi:urlEncrypt url="da-user-delete-confirm.jsp?itemId=${itemId}" />'" />
						</ffi:cinclude>
					</ffi:cinclude>
					<input class="submitbutton" type="button" value="Cancel" onclick="location.href='<ffi:urlEncrypt url="corpadminusers.jsp?goback=false" />'"  />
					</td></tr>
				</table>
			</td>
			<td align="right" style="background-image:URL(/cb/web/multilang/grafx/user/corner_cellbg.gif); background-repeat:repeat-y;"><br><br></td>
		</tr>
		<tr>
			<td><img src="/cb/web/multilang/grafx/user/corner_bl.gif" alt="" height="7" width="11" border="0"></td>
			<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="1" border="0"></td>
			<td><img src="/cb/web/multilang/grafx/user/corner_br.gif" alt="" height="7" width="11" border="0"></td>
		</tr>
	</table>
	<br>
	<ffi:flush/>
</div>

<ffi:setProperty name="BackURL" value="${SecurePath}user/da-user-verify.jsp"/>
<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="DISABLE_APPROVE"/>
<ffi:removeProperty name="SubmittedUserName"/>
<ffi:removeProperty name="SetBusinessEmployee"/>
