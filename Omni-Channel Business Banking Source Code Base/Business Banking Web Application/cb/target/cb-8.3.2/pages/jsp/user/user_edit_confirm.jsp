<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name="PageTitle" value="Manage Users"/>
<ffi:setProperty name="topMenu" value="home"/>
<ffi:setProperty name="subMenu" value="preferences"/>
<ffi:setProperty name="sub2Menu" value="manageusers"/>

<%
String IsObjectEntitled = null;
String ObjectLimitValue = null;
%>

<%-- Create the task used to generate entitlement object identifiers for accounts. --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetAccountEntitlementObjectID" scope="request"/>

<ffi:object name="com.ffusion.tasks.util.Resource" id="CountryStateResources" scope="request"/>
<ffi:setProperty name="CountryStateResources" property="ResourceFilename" value="com.ffusion.utilresources.states"/>
<ffi:process name="CountryStateResources"/>

<ffi:object name="com.ffusion.tasks.util.Resource" id="LanguagesResources" scope="request"/>
<ffi:setProperty name="LanguagesResources" property="ResourceFilename" value="com.ffusion.utilresources.languages"/>
<ffi:process name="LanguagesResources"/>

<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResources" scope="request"/>
<ffi:setProperty name="UserResources" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResources"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><ffi:getProperty name="PageTitle"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="<ffi:getProperty name="ServletPath"/>FFretail.css" type="text/css">

    <script type="text/javascript">
	function submitBackButton(url) {
		document.UserConfirmForm.action=url;
		document.UserConfirmForm.submit();
	}
</script>
</head>
<body>
<div class="approvalDialogHt">
<ffi:help id="user_user_addedit_confirm" className="moduleHelpClass"/>
<p class="instructions"><s:text name="secondaryUser.addEditConfirmationDialog.instructions" /></p>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name='admin.login.information'/></div>
	<div class="blockContent label140">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name='jsp.user_Username'/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="UserName"/><br>
			</div>
			<div class="inlineBlock">
				<ffi:setProperty name="UserResources" property="ResourceID" value="ACCOUNTSTATUS.${FFIEditSecondaryUser.AccountStatus}"/>
               	<span class="sectionLabel"><s:text name='jsp.user_User_Status'/></span><span> <ffi:getProperty name="UserResources" property="Resource"/>
			</div>
		</div>
	</div>
</div>

<div class="blockWrapper">
	<div  class="blockHead"><s:text name='admin.personal.information'/></div>
	<div class="blockContent label140">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="user.title"/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="Title"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.user_162"/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="FirstName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.user_186"/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="LastName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name='jsp.user_Phone'/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="Phone"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name='jsp.user_148'/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="Email"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="user.address1"/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="user.address2"/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name='jsp.user_65'/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<ffi:setProperty name="CountryStateResources" property="ResourceID" value="Country${FFIEditSecondaryUser.Country}"/>
                <span class="sectionLabel"><s:text name='jsp.user_97'/></span><span> <ffi:getProperty name="CountryStateResources" property="Resource"/></span>
			</div>
			<div class="inlineBlock">
				<ffi:setProperty name="CountryStateResources" property="ResourceID" value="State_${FFIEditSecondaryUser.Country}_${FFIEditSecondaryUser.State}"/>
                <span class="sectionLabel"><s:text name='jsp.user_299'/></span><span> <ffi:getProperty name="CountryStateResources" property="Resource"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name='jsp.user_391'/></span><span> <ffi:getProperty name="FFIEditSecondaryUser" property="ZipCode"/></span>
			</div>
			<div class="inlineBlock">
				<ffi:setProperty name="LanguagesResources" property="ResourceID" value="${FFIEditSecondaryUser.PreferredLanguage}"/>
                <span class="sectionLabel"><s:text name='user.preferredLanguage'/></span><span> <ffi:getProperty name="LanguagesResources" property="Resource"/></span>
			</div>
		</div>
	</div>
</div>
<div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData tdTopBottomPadding5 tableAlerternateRowColor">
                    <tr class="header">
                        <td><div class=""><s:text name="user.enabledAccounts" /></div></td>
                        <td><div style="text-align: center;" class="">Active</div></td>
						<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'>
                        	<td><div style="text-align: center;" class=""><s:text name="user.registerEnabled" /></div></td>
						</ffi:cinclude>
						<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>'>
                        	<td><div style="text-align: center;" class=""><s:text name="user.statementsEnabled" /></div></td>
						</ffi:cinclude>
                    </tr>

<ffi:setProperty name="BankingAccounts" property="Filter" value="COREACCOUNT=1,HIDE!1,AND"/>
<ffi:setProperty name="BankingAccounts" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
<ffi:setProperty name="SecondaryUserEntitledCoreAccounts" property="Filter" value="All"/>
<%-- QTS 756842. If not entiled to ACCOUNT_SETUP form BC, dont display the accounts. --%>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_SETUP %>'>
	<ffi:list collection="BankingAccounts" items="CoreAccount">
	    <ffi:setProperty name="IsSecondaryUserCoreAccount" value=""/>
	    <ffi:setProperty name="IsSecondaryUserCoreAccountDisplayText" value="No"/>
	    <ffi:setProperty name="IsSecondaryUserRegisterAccountDisplayText" value="No"/>
	    <ffi:setProperty name="IsSecondaryUserStatementAccountDisplayText" value="No"/>
	
	    <%-- Determine if the secondary user is entitled to the current account. --%>
	    <ffi:list collection="SecondaryUserEntitledCoreAccounts" items="TempSecondaryUserCoreAccount">
	        <ffi:cinclude value1="${TempSecondaryUserCoreAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
	            <ffi:cinclude value1="${TempSecondaryUserCoreAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
	                <ffi:cinclude value1="${TempSecondaryUserCoreAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
	                    <ffi:setProperty name="IsSecondaryUserCoreAccount" value="true"/>
	                    <ffi:setProperty name="IsSecondaryUserCoreAccountDisplayText" value="Yes"/>
	                </ffi:cinclude>
	            </ffi:cinclude>
	        </ffi:cinclude>
	    </ffi:list>
	
	    <%-- Determine if the current account is enabled for account register for the secondary user. --%>
	    <ffi:list collection="SecondaryUserEntitledRegisterAccounts" items="TempSecondaryUserRegisterAccount">
	        <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
	            <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
	                <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
	                    <ffi:setProperty name="IsSecondaryUserRegisterAccountDisplayText" value="Yes"/>
	                </ffi:cinclude>
	            </ffi:cinclude>
	        </ffi:cinclude>
	    </ffi:list>
	
	    <%-- Determine if the current account is enabled for online statements for the secondary user. --%>
	    <ffi:list collection="SecondaryUserEntitledStatementAccounts" items="TempSecondaryUserStatementAccount">
	        <ffi:cinclude value1="${TempSecondaryUserStatementAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
	            <ffi:cinclude value1="${TempSecondaryUserStatementAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
	                <ffi:cinclude value1="${TempSecondaryUserStatementAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
	                    <ffi:setProperty name="IsSecondaryUserStatementAccountDisplayText" value="Yes"/>
	                </ffi:cinclude>
	            </ffi:cinclude>
	        </ffi:cinclude>
	    </ffi:list>
	
	    <%-- Reset various flags depending on whether or not the current account is accesible to the secondary user. --%>
	    <ffi:cinclude value1="${IsSecondaryUserCoreAccount}" value2="" operator="equals">
	        <ffi:setProperty name="IsSecondaryUserRegisterAccountDisplayText" value="No"/>
	        <ffi:setProperty name="IsSecondaryUserStatementAccountDisplayText" value="No"/>
	    </ffi:cinclude>
	
	                    <tr>
	                        <td><div class="txt_normal"><ffi:getProperty name="CoreAccount" property="ConsumerDisplayText"/></div></td>
	                        <td><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="IsSecondaryUserCoreAccountDisplayText"/></div></td>
	    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'>
	                        <td><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="IsSecondaryUserRegisterAccountDisplayText"/></div></td>
	    </ffi:cinclude>
	    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>'>
	                        <td><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="IsSecondaryUserStatementAccountDisplayText"/></div></td>
	    </ffi:cinclude>
	                    </tr>
	</ffi:list>
	
	<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_AGGREGATION %>'>
	    <ffi:setProperty name="ExternalAccounts" property="Filter" value="COREACCOUNT=0,HIDE!1,AND"/>
	    <ffi:setProperty name="ExternalAccounts" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
	
	    <ffi:list collection="ExternalAccounts" items="ExternalAccount">
	        <ffi:setProperty name="IsSecondaryUserExternalAccount" value=""/>
	        <ffi:setProperty name="IsSecondaryUserExternalAccountDisplayText" value="No"/>
	        <ffi:setProperty name="IsSecondaryUserRegisterAccountDisplayText" value="No"/>
	        <ffi:setProperty name="IsSecondaryUserStatementAccountDisplayText" value="No"/>
	
	        <%-- Determine if the secondary user is entitled to the current account. --%>
	        <ffi:list collection="SecondaryUserEntitledExternalAccounts" items="TempSecondaryUserExternalAccount">
	            <ffi:cinclude value1="${TempSecondaryUserExternalAccount.ID}" value2="${ExternalAccount.ID}" operator="equals">
	                <ffi:cinclude value1="${TempSecondaryUserExternalAccount.RoutingNumber}" value2="${ExternalAccount.RoutingNumber}" operator="equals">
	                    <ffi:cinclude value1="${TempSecondaryUserExternalAccount.BankId}" value2="${ExternalAccount.BankId}" operator="equals">
	                        <ffi:setProperty name="IsSecondaryUserExternalAccount" value="true"/>
	                        <ffi:setProperty name="IsSecondaryUserExternalAccountDisplayText" value="Yes"/>
	                    </ffi:cinclude>
	                </ffi:cinclude>
	            </ffi:cinclude>
	        </ffi:list>
	
	        <%-- Determine if the current account is enabled for account register for the secondary user. --%>
	        <ffi:list collection="SecondaryUserEntitledRegisterAccounts" items="TempSecondaryUserRegisterAccount">
	            <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.ID}" value2="${ExternalAccount.ID}" operator="equals">
	                <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.RoutingNum}" value2="${ExternalAccount.RoutingNum}" operator="equals">
	                    <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.BankID}" value2="${ExternalAccount.BankID}" operator="equals">
	                        <ffi:setProperty name="IsSecondaryUserRegisterAccountDisplayText" value="Yes"/>
	                    </ffi:cinclude>
	                </ffi:cinclude>
	            </ffi:cinclude>
	        </ffi:list>
	
	                    <tr>
	                        <td><div class="txt_normal"><ffi:getProperty name="ExternalAccount" property="ConsumerDisplayText"/></div></td>
	                        <td><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="IsSecondaryUserExternalAccountDisplayText"/></div></td>
	        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'>
	                        <td><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="IsSecondaryUserRegisterAccountDisplayText"/></div></td>
	        </ffi:cinclude>
	        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>'>
	                        <td><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="IsSecondaryUserStatementAccountDisplayText"/></div></td>
	        </ffi:cinclude>
	                    </tr>
	
	    </ffi:list>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_SETUP %>'>
<tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
	<td colspan="6"><div style="text-align: center"><!-- L10NStart-->(No accounts enabled.)<!-- L10NEnd--></div></td>
</tr>
</ffi:cinclude>
                </table></div></div>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
	<s:include value="/pages/jsp/user/inc/user_addedit_confirm_transfers.jsp" />
</ffi:cinclude>

<ffi:setProperty name="SkipExternalTransfersSection" value="TRUE"/>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_FROM %>' checkParent="true">
    <ffi:setProperty name="SkipExternalTransfersSection" value="FALSE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_TO %>' checkParent="true">
    <ffi:setProperty name="SkipExternalTransfersSection" value="FALSE"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>' checkParent="true">
    <ffi:setProperty name="SkipExternalTransfersSection" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>' checkParent="true">
    <ffi:setProperty name="SkipExternalTransfersSection" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude value1="${EnableInternalTransfers}" value2="FALSE" operator="equals">
    <ffi:setProperty name="SkipExternalTransfersSection" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude value1="${SkipExternalTransfersSection}" value2="TRUE" operator="notEquals">
	<s:include value="/pages/jsp/user/inc/user_addedit_confirm_exttransfers.jsp" />
</ffi:cinclude>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
	<s:include value="/pages/jsp/user/inc/user_addedit_confirm_payments.jsp" />
</ffi:cinclude>
<form name="UserConfirmForm" id="userConfirmForm" action="/cb/pages/jsp/user/saveSecondaryUser.action" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <br/> <br/>
    <div class="ui-widget-header customDialogFooter">    
		<sj:a id="secUserAddCancel" button="true" onClickTopics="secondaryUsers.closeAddEditConfirmationDialog"><s:text name="jsp.default_82"/></sj:a>        
		<sj:a id="secUserAddNext" button="true" formIds="userConfirmForm"  targets="targetDiv"	onSuccessTopics="secondaryUsers.saveUserSuccess"><s:text name="jsp.default_291"/></sj:a>        
	</div>
</form>
</div>

<ffi:removeProperty name="IsSecondaryUserCoreAccount"/>
<ffi:removeProperty name="IsSecondaryUserCoreAccountDisplayText"/>
<ffi:removeProperty name="IsSecondaryUserExternalAccount"/>
<ffi:removeProperty name="IsSecondaryUserExternalAccountDisplayText"/>
<ffi:removeProperty name="IsSecondaryUserRegisterAccountDisplayText"/>
<ffi:removeProperty name="IsSecondaryUserStatementAccountDisplayText"/>
<ffi:removeProperty name="HasAccount"/>
<ffi:removeProperty name="CancelButton"/>
<ffi:removeProperty name="BackButton"/>
<ffi:removeProperty name="SaveButton"/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/user_addedit_confirm.jsp" URLEncrypt="true"/>
