<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.util.DateFormatUtil" %>

<ffi:cinclude value1="${showPermission}" value2="y">
	<ffi:removeProperty name="showPermission" />
	<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request"/>
	<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>
	<tr>
		<td class="sectionsubhead">&gt;<!--L10NStart-->Permission<!--L10NEnd--></td>
	</tr>
	<tr>
		<td class="sectionsubhead adminBackground">&nbsp;<!--L10NStart-->Permission Type<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Activity<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Type<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Id<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Old Value<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->New Value<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Exceed Limit W/ Approval<!--L10NEnd--></td>
		<td class="sectionsubhead adminBackground"><!--L10NStart-->Limit Period<!--L10NEnd--></td>
	</tr>
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="" />
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>	
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>	
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>	
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>	
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>	
		</ffi:cinclude>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:object id="GetEntitlmentDisplayName" name="com.ffusion.tasks.dualapproval.GetEntitlmentDisplayName"/>

		<ffi:cinclude value1="${isAccountGroupAccess}" value2="true" operator="equals" >
			<ffi:object id="GetAccountGroupsForGroup" name="com.ffusion.tasks.admin.GetAccountGroupsForGroup" scope="session"/>
			<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupID" value="${BusinessEmployee.EntitlementGroupId}"/>
			<ffi:setProperty name="GetAccountGroupsForGroup" property="BusDirectoryId" value="${Business.Id}"/>
			<ffi:setProperty name="GetAccountGroupsForGroup" property="CheckAdminAccess" value="true"/>
			<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupAccountGroupsName" value="AccountGroupsAccessList"/>
			
			<ffi:process name="GetAccountGroupsForGroup"/>
			
		</ffi:cinclude>
		<ffi:cinclude value1="${isPerACHAccess}" value2="true" operator="equals">
			<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies" scope="session"/>
			<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
			<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
			<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="false"/>
			<ffi:process name="GetACHCompanies"/>
			<ffi:removeProperty name="GetACHCompanies"/>
		</ffi:cinclude>
		
		<ffi:cinclude value1="${isPerAccountGroup}" value2="true" operator="equals">
			<ffi:object id="GetAccountGroupsForUser" name="com.ffusion.tasks.admin.GetAccountGroupsForUser" scope="session"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="GroupID" value="${BusinessEmployee.EntitlementGroupId}"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="UserType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="MemberID" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="GroupAccountGroupsName" value="accountGroupsCollection"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="BusDirectoryId" value="${Business.Id}"/>
			<ffi:setProperty name="GetAccountGroupsForUser" property="CheckAdminAccess" value="true"/>
			<ffi:process name="GetAccountGroupsForUser"/>
		</ffi:cinclude>
		
		<ffi:cinclude value1="${isPerLocation}" value2="true" operator="equals">
			<ffi:object id="GetDescendantsByType" name="com.ffusion.efs.tasks.entitlements.GetDescendantsByType" scope="session"/>
							<ffi:setProperty name="GetDescendantsByType" property="GroupID" value="${Business.EntitlementGroupId}"/>
							<ffi:setProperty name="GetDescendantsByType" property="GroupType" value="Division"/>
							<ffi:process name="GetDescendantsByType"/>
							<ffi:removeProperty name="GetDescendantsByType" />
		</ffi:cinclude>
		
		<ffi:cinclude value1="${isWireTemplate}" value2="true" operator="equals">
			<ffi:object name="com.ffusion.tasks.wiretransfers.GetAllWireTemplates" id="GetAllWireTemplates" scope="session"/>
			<ffi:setProperty name="GetAllWireTemplates" property="businessID" value="${Business.EntitlementGroupId}"/>
			<ffi:setProperty name="GetAllWireTemplates" property="GroupID" value="${BusinessEmployee.EntitlementGroupId}"/>
			<ffi:setProperty name="GetAllWireTemplates" property="UserType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="GetAllWireTemplates" property="MemberID" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetAllWireTemplates" property="DateFormat" value="<%= DateFormatUtil.DATE_FORMAT_MMDDYYYY %>" />
			<ffi:setProperty name="GetAllWireTemplates" property="StartDate" value="01/01/2000" />
			<ffi:setProperty name="GetAllWireTemplates" property="EndDate" value="01/01/2050" />
			<ffi:process name="GetAllWireTemplates"/>
		</ffi:cinclude>
<ffi:list collection="categories" items="category">
	<ffi:include page="${PathExt}user/inc/da-common-limits.jsp"/>
</ffi:list>		
<ffi:cinclude value1="${showEntitlement}" value2="y">
<ffi:removeProperty name="showEntitlement" />		
<ffi:list collection="categories" items="category"> 
	<ffi:flush/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="${category.categoryType}"/>	
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${category.categorySubType}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${category.ObjectId}" />
		<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="${category.ObjectType}" />
		<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>">
		<ffi:process name="GetDACategoryDetails" />
		<ffi:include page="${PathExt}user/inc/da-user-entitlements.jsp"/>
		<ffi:list collection="CATEGORY_BEAN.daItems" items="entitlement">
		<ffi:flush/>
			<tr>
			<td class="columndata"><ffi:flush/><ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubType}" /></td>
				<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>">
 				 	<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 </ffi:cinclude>
 				 <ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="notEquals">
 				 	<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>">
 				 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 	</ffi:cinclude>
 				 	<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="notEquals">
 				 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="${entitlement.fieldName}"/>
 				 	</ffi:cinclude>
 				 </ffi:cinclude>
 				 
 				<ffi:cinclude value1="${entitlement.fieldName}" value2="" operator="notEquals">
 				 	<ffi:process name="GetEntitlmentDisplayName" />
 				 	<td class="columndata"> 
						<ffi:flush/><ffi:getProperty name="entitlementDisplayName" /> 
					</td>
 				 </ffi:cinclude>
 				 <ffi:cinclude value1="${entitlement.fieldName}" value2="" operator="equals">
 				 	<td class="columndata">
 				 		<ffi:flush/><ffi:getProperty name="entitlement" property="fieldName" />
 				 	</td>
 				 </ffi:cinclude>
				<td class="columndata">
					<ffi:cinclude value1="${CATEGORY_BEAN.objectType}" value2="">
						<ffi:flush/><!--L10NStart-->Cross Account<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectType}" value2="" operator="notEquals">
						<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectType" />				
					</ffi:cinclude>
				</td>
				<td class="columndata">
					<ffi:cinclude value1="${CATEGORY_BEAN.objectId}" value2="">
						<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>">
							<ffi:flush/><ffi:getProperty name="entitlement" property="fieldName" />	
						</ffi:cinclude>
						<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="notEquals">
							<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>">
								<ffi:flush/>
								<ffi:cinclude value1="${isAccountGroupAccess}" value2="true" operator="equals">
							<ffi:list collection="AccountGroupsAccessList" items="acctGroup">
								<ffi:cinclude value1="${acctGroup.id}" value2="${entitlement.fieldName}" operator="equals">
									<ffi:getProperty name="acctGroup" property="name"/>
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="notEquals">
								<ffi:flush/><!--L10NStart-->All Ids<!--L10NEnd-->
							</ffi:cinclude>
						</ffi:cinclude>
										
					</ffi:cinclude>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectId}" value2="" operator="notEquals">
						<ffi:cinclude value1="${isPerAccountGroup}" value2="true" operator="equals">
							<ffi:list collection="accountGroupsCollection" items="accountGroup">
								<ffi:cinclude value1="${accountGroup.id}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="accountGroup" property="name" /> - 
									<ffi:getProperty name="accountGroup" property="AcctGroupId" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						
						<ffi:cinclude value1="${isPerLocation}" value2="true" operator="equals">
							
							<ffi:list collection="Descendants" items="Descendant" >
							<ffi:removeProperty name="GetLocations"/>
							<ffi:removeProperty name="Locations"/>
							<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
							<ffi:setProperty name ="GetLocations" property="DivisionID" value="${Descendant.GroupId}"/>
							
							<ffi:process name="GetLocations"/>
							<ffi:list collection="Locations" items="LocationItem">
								<ffi:cinclude value1="${LocationItem.LocationBPWID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="LocationItem" property="LocationName" /> - 
									<ffi:getProperty name="LocationItem" property="LocationID" />
								</ffi:cinclude>
							</ffi:list>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isWireTemplate}" value2="true" operator="equals">
							<ffi:list collection="AllWireTemplates" items="WireTemplateItem">
								<ffi:cinclude value1="${WireTemplateItem.TemplateID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="WireTemplateItem" property="WireName" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerACHAccess}" value2="true" operator="equals">
							<ffi:list collection="ACHCOMPANIES" items="ACHCompanyListItem">
								<ffi:cinclude value1="${ACHCompanyListItem.CompanyID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="ACHCompanyListItem" property="CompanyName"/> - 
												<ffi:getProperty name="ACHCompanyListItem" property="CompanyID"/>
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerAccount}" value2="true" operator="equals">
							<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectId" />
						</ffi:cinclude>
					</ffi:cinclude>
				</td>
				<td class="columndata">
					<ffi:cinclude value1="${entitlement.oldValue}" value2="true">
						<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${entitlement.oldValue}" value2="false" >
						<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
					</ffi:cinclude>	</td>
				<td class="columndata">
					<ffi:cinclude value1="${entitlement.newValue}" value2="true">
						<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${entitlement.newValue}" value2="false" >
						<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
					</ffi:cinclude>
				</td>
				<td class="columndata"><ffi:flush/><ffi:getProperty name="entitlement"	property="userAction" /></td>

			</tr>
			<ffi:cinclude value1="${entitlement.error}" value2="" operator="notEquals">
				<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
				<tr>
					<td class="columndataDANote" colspan="7">
						<ffi:flush/><ffi:getProperty name="entitlement" property="error"/> 
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:flush/>
		</ffi:list>
		</ffi:cinclude>
</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${showRequireApproval}" value2="y">

<ffi:removeProperty name="showRequireApproval" />		
<ffi:list collection="categories" items="category"> 
	<ffi:flush/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="${category.categoryType}"/>	
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${category.categorySubType}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${category.ObjectId}" />
		<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="${category.ObjectType}" />
		<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>">
		<ffi:process name="GetDACategoryDetails" />
		
		<%-- Validate Requires Approval --%>
		<ffi:object id="ValidateRequiresApprovalDA" name="com.ffusion.tasks.dualapproval.ValidateRequiresApprovalDA" />
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="CategorySessionName" value="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.CATEGORY_BEAN %>"/>
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="ValidateRequiresApprovalDA" property="ModifyUserOnly" value="true"/>
			<ffi:setProperty name="ValidateRequiresApprovalDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="ValidateRequiresApprovalDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="ValidateRequiresApprovalDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		</s:if>
		<ffi:process name="ValidateRequiresApprovalDA" />
		<ffi:removeProperty name="ValidateRequiresApprovalDA" />
		<%-- End Validate Requires Approval --%>
		
		<ffi:list collection="CATEGORY_BEAN.daItems" items="entitlement">
		<ffi:flush/>
			<tr>
			<td class="columndata"><ffi:flush/><ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubType}" /></td>
				<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>">
 				 	<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 </ffi:cinclude>
 				 <ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="notEquals">
 				 	<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>">
 				 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 	</ffi:cinclude>
 				 	<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="notEquals">
 				 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="${entitlement.fieldName}"/>
 				 	</ffi:cinclude>
 				 </ffi:cinclude>
 				 
 				<ffi:cinclude value1="${entitlement.fieldName}" value2="" operator="notEquals">
 				 	<ffi:process name="GetEntitlmentDisplayName" />
 				 	<td class="columndata"> 
						<ffi:flush/><ffi:getProperty name="entitlementDisplayName" /> - <!--L10NStart-->Require Approval<!--L10NEnd--> 
					</td>
 				 </ffi:cinclude>
 				 <ffi:cinclude value1="${entitlement.fieldName}" value2="" operator="equals">
 				 	<td class="columndata">
 				 		<ffi:flush/><ffi:getProperty name="entitlement" property="fieldName" /> - <!--L10NStart-->Require Approval<!--L10NEnd--> 
 				 	</td>
 				 </ffi:cinclude>
				<td class="columndata">
					<ffi:cinclude value1="${CATEGORY_BEAN.objectType}" value2="">
						<ffi:flush/><!--L10NStart-->Cross Account<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectType}" value2="" operator="notEquals">
						<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectType" />				
					</ffi:cinclude>
				</td>
				<td class="columndata">
					<ffi:cinclude value1="${CATEGORY_BEAN.objectId}" value2="">
						<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>">
							<ffi:flush/><ffi:getProperty name="entitlement" property="fieldName" />	
						</ffi:cinclude>
						<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="notEquals">
							<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>">
								<ffi:flush/>
								<ffi:cinclude value1="${isAccountGroupAccess}" value2="true" operator="equals">
							<ffi:list collection="AccountGroupsAccessList" items="acctGroup">
								<ffi:cinclude value1="${acctGroup.id}" value2="${entitlement.fieldName}" operator="equals">
									<ffi:getProperty name="acctGroup" property="name"/>
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
									
							</ffi:cinclude>
							<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="notEquals">
								<ffi:flush/><!--L10NStart-->All Ids<!--L10NEnd-->
							</ffi:cinclude>
						</ffi:cinclude>
										
					</ffi:cinclude>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectId}" value2="" operator="notEquals">
						<ffi:cinclude value1="${isPerAccountGroup}" value2="true" operator="equals">
							<ffi:list collection="accountGroupsCollection" items="accountGroup">
								<ffi:cinclude value1="${accountGroup.id}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="accountGroup" property="name" /> - 
									<ffi:getProperty name="accountGroup" property="AcctGroupId" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerLocation}" value2="true" operator="equals">
							<ffi:list collection="Locations" items="LocationItem">
								<ffi:cinclude value1="${LocationItem.LocationBPWID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="LocationItem" property="LocationName" /> - 
									<ffi:getProperty name="LocationItem" property="LocationID" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isWireTemplate}" value2="true" operator="equals">
							<ffi:list collection="AllWireTemplates" items="WireTemplateItem">
								<ffi:cinclude value1="${WireTemplateItem.TemplateID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="WireTemplateItem" property="WireName" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerACHAccess}" value2="true" operator="equals">
							<ffi:list collection="ACHCOMPANIES" items="ACHCompanyListItem">
								<ffi:cinclude value1="${ACHCompanyListItem.CompanyID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="ACHCompanyListItem" property="CompanyName"/> - 
												<ffi:getProperty name="ACHCompanyListItem" property="CompanyID"/>
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerAccount}" value2="true" operator="equals">
							<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectId" />
						</ffi:cinclude>
					</ffi:cinclude>
				</td>
				<td class="columndata">
					<ffi:cinclude value1="${entitlement.oldValue}" value2="true">
						<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${entitlement.oldValue}" value2="false" >
						<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
					</ffi:cinclude>	</td>
				<td class="columndata">
					<ffi:cinclude value1="${entitlement.newValue}" value2="true">
						<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${entitlement.newValue}" value2="false" >
						<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
					</ffi:cinclude>
				</td>
				<td class="columndata"><ffi:flush/><ffi:getProperty name="entitlement"	property="userAction" /></td>

			</tr>
			<ffi:cinclude value1="${entitlement.error}" value2="" operator="notEquals">
				<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
				<tr>
					<td class="columndataDANote" colspan="7">
						<ffi:flush/><ffi:getProperty name="entitlement" property="error"/> 
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:flush/>
		</ffi:list>
		</ffi:cinclude>
</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${showLimit}" value2="y">
<ffi:removeProperty name="showLimit" />
<ffi:list collection="categories" items="category"> 
<ffi:flush/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${category.categorySubType}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${category.ObjectId}" />
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="${category.categoryType}"/>	
	<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="${category.ObjectType}" />
	<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_LIMIT %>">
			<ffi:process name="GetDACategoryDetails" />	
			<ffi:cinclude value1="${isAccountAccess}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-accountaccess-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isAccountGroupAccess}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-accountgroupaccess-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isCrossACHAccess}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-crossach-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isPerACHAccess}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-perach-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isCrossAccount}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-crossaccount-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isPerAccount}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-peraccount-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isPerAccountGroup}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-peraccountgroup-limits.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${isPerLocation}" value2="true">
				<ffi:include page="${PathExt}user/inc/da-verify-user-perlocation-limits.jsp"/>
			</ffi:cinclude>
			
		<ffi:list collection="CATEGORY_BEAN.daItems" items="limit">
			<ffi:flush/>
			<tr>
				<td class="columndata"><ffi:flush/><ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubType}" /></td>
				<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>">
 				 	<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 	<ffi:setProperty name="displayOperationName" value="${limit.operationName}"/>
 				 	<ffi:setProperty name="limit" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 </ffi:cinclude>
 				 
 				 <ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="notEquals">
 				 	<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>">
 				 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 		<ffi:setProperty name="displayOperationName" value="${limit.operationName}"/>
 				 		<ffi:setProperty name="limit" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
 				 	</ffi:cinclude>
 				 	<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="notEquals">
 				 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="${limit.operationName}"/>
 				 	</ffi:cinclude>
 				 </ffi:cinclude>
 				 <ffi:cinclude value1="${limit.operationName}" value2="" operator="notEquals">
 				 	<ffi:process name="GetEntitlmentDisplayName"/>
					<td class="columndata"><ffi:flush/><ffi:getProperty name="entitlementDisplayName" /></td>
 				 </ffi:cinclude>
 				 <ffi:cinclude value1="${limit.operationName}" value2="" operator="equals">
 				 	<td class="columndata"><ffi:flush/><ffi:getProperty name="limit" property="operationName" /></td>
 				 </ffi:cinclude> 
				
				<td class="columndata">
					<ffi:cinclude value1="${CATEGORY_BEAN.objectType}" value2="">
						<ffi:flush/><!--L10NStart-->Cross Account<!--L10NEnd-->				
					</ffi:cinclude>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectType}" value2="" operator="notEquals">
						<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectType" />				
					</ffi:cinclude>
				</td>
				<td class="columndata">
				<ffi:flush/>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectId}" value2="">
						<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>">
							<ffi:flush/><ffi:getProperty name="displayOperationName" />	
						</ffi:cinclude>
						<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="notEquals">
							<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>">
								<ffi:flush/>
								<ffi:cinclude value1="${isAccountGroupAccess}" value2="true" operator="equals">
									<ffi:list collection="AccountGroupsAccessList" items="acctGroup">
										<ffi:cinclude value1="${acctGroup.id}" value2="${displayOperationName}" operator="equals">
											<ffi:getProperty name="acctGroup" property="name" />
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="notEquals">
								<ffi:flush/><!--L10NStart-->All Ids<!--L10NEnd-->
							</ffi:cinclude>		
						</ffi:cinclude>		
					</ffi:cinclude>
					<ffi:cinclude value1="${CATEGORY_BEAN.objectId}" value2="" operator="notEquals">
						<ffi:cinclude value1="${isPerAccountGroup}" value2="true" operator="equals">
							<ffi:list collection="accountGroupsCollection" items="accountGroup">
								<ffi:cinclude value1="${accountGroup.id}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="accountGroup" property="name" /> - 
									<ffi:getProperty name="accountGroup" property="AcctGroupId" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerLocation}" value2="true" operator="equals">
							<ffi:list collection="Locations" items="LocationItem">
								<ffi:cinclude value1="${LocationItem.LocationBPWID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="LocationItem" property="LocationName" /> - 
									<ffi:getProperty name="LocationItem" property="LocationID" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isWireTemplate}" value2="true" operator="equals">
							<ffi:list collection="AllWireTemplates" items="WireTemplateItem">
								<ffi:cinclude value1="${WireTemplateItem.TemplateID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="WireTemplateItem" property="WireName" />
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerACHAccess}" value2="true" operator="equals">
							<ffi:list collection="ACHCOMPANIES" items="ACHCompanyListItem">
								<ffi:cinclude value1="${ACHCompanyListItem.CompanyID}" value2="${CATEGORY_BEAN.objectId}" >
									<ffi:getProperty name="ACHCompanyListItem" property="CompanyName"/> - 
									<ffi:getProperty name="ACHCompanyListItem" property="CompanyID"/>
								</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${isPerAccount}" value2="true" operator="equals">
							<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectId" />
						</ffi:cinclude>
					</ffi:cinclude>	
				</td>
				<td class="columndata">
				<ffi:cinclude value1="${limit.oldData}" value2="" operator="notEquals">
					<ffi:setProperty name="CurrencyObject" property="Amount" value="${limit.oldData}"/>
					<ffi:flush/><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
				</ffi:cinclude>
				</td>
				<td class="columndata">
				<ffi:cinclude value1="${limit.newData}" value2="" operator="notEquals">
					<ffi:setProperty name="CurrencyObject" property="Amount" value="${limit.newData}"/>
					<ffi:flush/><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
				</ffi:cinclude></td>
				<td class="columndata">
				<ffi:include page="${PathExt}user/inc/da-verify-user-checkrequireapproval.jsp"/>
					<ffi:cinclude value1="${IsRequireApprovalChecked}" value2="" operator="notEquals">
						<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y" operator="equals">
							<ffi:setProperty name="limit" property="error" value="${RequiresApprovalErrMsg}" />
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:removeProperty name="IsRequireApprovalChecked" />
					<ffi:removeProperty name="RequiresApprovalErrMsg" />
					<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y">
						<ffi:flush/><!--L10NStart-->Yes<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:cinclude value1="${limit.allowNewApproval}" value2="N">
						<ffi:flush/><!--L10NStart-->No<!--L10NEnd-->
					</ffi:cinclude></td>
				<td class="columndata">
					<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>">
						<ffi:cinclude value1="${limit.period}" value2="1">
							<ffi:flush/><!--L10NStart-->per batch<!--L10NEnd-->				
						</ffi:cinclude>
						<ffi:cinclude value1="${limit.period}" value2="2">
							<ffi:flush/><!--L10NStart-->daily<!--L10NEnd-->				
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>" operator="notEquals">
						<ffi:cinclude value1="${limit.period}" value2="1">
							<ffi:flush/><!--L10NStart-->per transaction<!--L10NEnd-->				
						</ffi:cinclude>
						<ffi:cinclude value1="${limit.period}" value2="2">
							<ffi:flush/><!--L10NStart-->per day<!--L10NEnd-->				
						</ffi:cinclude>
						<ffi:cinclude value1="${limit.period}" value2="3">
							<ffi:flush/><!--L10NStart-->per week<!--L10NEnd-->				
						</ffi:cinclude>
						<ffi:cinclude value1="${limit.period}" value2="4">
							<ffi:flush/><!--L10NStart-->per month<!--L10NEnd-->				
						</ffi:cinclude>
					</ffi:cinclude>
				</td>
			</tr>
			<ffi:cinclude value1="${limit.error}" value2="" operator="notEquals">
				<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
				<tr>
					<td class="columndataDANote" colspan="8"><ffi:flush/><ffi:getProperty name="limit" property="error" /></td>
				</tr>
			</ffi:cinclude>
			<ffi:flush/>
		</ffi:list>
		</ffi:cinclude> 
</ffi:list>
</ffi:cinclude>
<ffi:removeProperty name="CurrencyObject"/>
<ffi:removeProperty name="displayOperationName"/>
<ffi:removeProperty name="isPerAccount"/>
<ffi:removeProperty name="isPerLocation"/>
<ffi:removeProperty name="wireTemplate"/>
<ffi:removeProperty name="perachCompany"/>
<ffi:removeProperty name="accountGroupsCollection"/>
<ffi:removeProperty name="Locations"/>
<ffi:removeProperty name="AllWireTemplates"/>
<ffi:removeProperty name="ACHCOMPANIES"/>
<ffi:removeProperty name="AccountAccessList"/>
<ffi:removeProperty name="AccountGroupAccessList"/>
<ffi:removeProperty name="limitInfoList"/>
<ffi:removeProperty name="crossACHEntListsBeforeFilter"/>
<ffi:removeProperty name="ParentChildLists"/>
<ffi:removeProperty name="ChildParentLists"/>


</ffi:cinclude>
