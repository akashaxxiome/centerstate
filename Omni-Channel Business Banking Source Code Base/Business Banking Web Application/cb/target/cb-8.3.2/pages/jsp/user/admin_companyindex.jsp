<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>

<script src="<s:url value='/web/js/user/company%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/admin%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/admin_grid%{#session.minVersion}.js'/>" type="text/javascript"></script>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	
<s:include value="inc/index-pre.jsp"/>
<s:include value="inc/corpadmininfo-pre.jsp" />

<div id="desktop" align="center">
	 
	<div id="appdashboard">
		<s:include value="admin_company_dashboard.jsp" />
	</div> 
	
        <style type="text/css">
            .indent1 { padding-left: 2em; }
            .indent2 { padding-left: 4em; }
            .indent3 { padding-left: 6em; }
            .indent4 { padding-left: 8em; }
        </style>

	<div id="operationresult">
		<div id="resultmessage"></div>
	</div>
	<!-- result -->
	<div id="companyDetails" class="hidden">
		<s:include value="admin_compDetails.jsp"/>
	</div>
	
	<div id="details" class="hidden">
		<s:include value="admin_details.jsp" />
	</div>
	<div id="permissionsDiv" class="adminBackground sectionsubhead" />
	
	<% 
		String adminSummaryCssClass ="visible"; 
	%>
	<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="notEquals">
	<%
		adminSummaryCssClass = "hidden";
	%>
	</ffi:cinclude>
	
	<div id="summary" class="<%=adminSummaryCssClass%>">
		<s:include value="admin_company_summary.jsp" />
	</div>

</div>
<%-- auto entitlement dialog --%>
<s:url id="autoEntitlementUrl" value="%{#session.PagesPath}user/auto_entitlement.jsp" escapeAmp="false">
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<sj:dialog 
	id="autoEntitlementDialogID" 
	cssClass="adminDialog"
	title="%{getText('jsp.user_49')}" 
	href="%{autoEntitlementUrl}" 
	modal="true"
	resizable="false"
	autoOpen="false"
	closeOnEscape="true"
	showEffect="fold" 
	hideEffect="clip" 
	width="550"
	>
</sj:dialog>
<%-- transaction group del dialog --%>
<sj:dialog 
	id="deleteTransactionGroupDialog" 
	cssClass="adminDialog"
	title="%{getText('jsp.user_108')}" 
	modal="true" 
	resizable="false"
	autoOpen="false" 
	closeOnEscape="true" 
	showEffect="fold"
	hideEffect="clip" 
	width="450">
</sj:dialog>

<sj:dialog id="editAdminerDialogId" cssClass="adminDialog" title="%{getText('jsp.user_Edit_Adminer')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="850" height="650" cssStyle="overflow:hidden" onCloseTopics="onAdministratorsDialogCloseTopic"></sj:dialog>

<sj:dialog id="editAdminerAutoEntitleDialogId" cssClass="adminDialog" title="%{getText('jsp.user_Edit_Adminer')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650" onCloseTopics="onAdministratorsDialogClose"></sj:dialog>

<%-- dual approval submit company changes for approval dialog --%>
<sj:dialog 
	id="DACompanySubmitForApprovalDialogId" 
	cssClass="adminDialog"
	title="%{getText('jsp.da_company_submit_for_approval')}" 
	modal="true" 
	resizable="false"
	autoOpen="false" 
	closeOnEscape="true" 
	showEffect="fold"
	hideEffect="clip" 
	width="800">
</sj:dialog>

<%-- dual approval discard company changes dialog --%>
<sj:dialog 
	id="DACompanyDiscardChangesDialogId" 
	cssClass="adminDialog"
	title="%{getText('jsp.da_company_discard_changes')}" 
	modal="true" 
	resizable="false"
	autoOpen="false" 
	closeOnEscape="true" 
	showEffect="fold"
	hideEffect="clip" 
	width="auto">
</sj:dialog>

<%-- dual approval review pending changes dialog --%>
<sj:dialog 
	id="DACompanyReviewChangesDialogId" 
	cssClass="adminDialog"
	title="%{getText('jsp.da_company_verify_pending_changes')}" 
	modal="true" 
	resizable="false"
	autoOpen="false" 
	closeOnEscape="true" 
	showEffect="fold"
	hideEffect="clip" 
	width="800">
</sj:dialog>

<sj:dialog id="approvalWizardDialogID" cssClass="adminDialog" title="%{getText('jsp.user.ApprovalWizard')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto" position="['middle','center']">
</sj:dialog>

<sj:dialog id="DACompanyProfileViewDialogId" cssClass="adminDialog" onCloseTopics="onCompanyProfileViewDialogClose" title="%{getText('jsp.da_view_company_profile')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="DACompanyBaiViewDialogId" cssClass="adminDialog" onCloseTopics="onBAIViewDialogClose" title="View BAI Export Settings" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="DACompanyTGViewDialogId" cssClass="adminDialog" title="View Transaction Groups" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="DACompanyAccountConfigurationViewDialogId" cssClass="adminDialog" title="View Account Configuration" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>


<script>
	$(document).ready(function(){
		var $editAdminerDialogIdCloseXButton =$('#editAdminerDialogId').parent().find('.ui-dialog-titlebar-close');
		$editAdminerDialogIdCloseXButton.unbind('click');
		$editAdminerDialogIdCloseXButton.bind('click', function(){
			ns.admin.editAdminerCancelButtonOnClick();
		});
		
		var $editAdminerAutoEntitleDialogCloseXButton =$('#editAdminerAutoEntitleDialogId').parent().find('.ui-dialog-titlebar-close');
		$editAdminerAutoEntitleDialogCloseXButton.unbind('click');
		$editAdminerAutoEntitleDialogCloseXButton.bind('click', function(){
			ns.admin.editAdminerAutoEntitleCancelButtonOnClick();
		});
		
		ns.company.showCompanyDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>

	<ffi:setProperty name="Section" value="Company" />
</ffi:cinclude>