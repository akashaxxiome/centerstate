<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setProperty name="Compare" property="Value1" value="TRUE"/>
<% int limitIndex = ( ( Integer )session.getAttribute( "limitIndex" ) ).intValue(); %>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:removeProperty name="DARequest" />
	<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
	<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
	<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${LimitType.OperationName}"/>
	<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
</ffi:cinclude>

<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${LimitType.OperationName}"/>

	<ffi:setProperty name="EntitlementsDisplayedNames" property="Add" value="${LimitType.OperationName}"/>
	<ffi:setProperty name="DisplayedLimit" value="TRUE"/>

	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:setProperty name="GetMaxLimitForPeriod" property="ObjectID" value="${LimitType.OperationName}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:setProperty name="TempOpName" value="${LimitType.OperationName}"/>
		<% String entStr = (new com.ffusion.csil.beans.entitlements.Entitlement( (String)session.getAttribute("TempOpName"), com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE, (String)session.getAttribute("WireID") ) ).toString(); %>
		<ffi:removeProperty name="TempOpName"/>
		<ffi:setProperty name="GetMaxLimitForPeriod" property="ObjectID" value="<%= entStr %>"/>
	</ffi:cinclude>

	<tr>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
				<td class="tbrd_t" valign="middle">
					<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
					<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="equals">

						<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${LimitType.Value}"/>
						<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
							<ffi:setProperty name="CheckEntitlementByMember" property="ObjectType" value="${CheckEntitlementObjectType}"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
							<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${CheckEntitlementObjectId}"/>
						</ffi:cinclude>
						<ffi:process name="CheckEntitlementByMember"/>
						<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>

						<div align="center">
							<ffi:setProperty name="LastRequest" property="Name" value="admin${limitIndex}${TemplateIndex}"/>

							<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${LimitType.Value}"/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
							<ffi:removeProperty name="admin${limitIndex}${TemplateIndex}"/>
							44<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0" <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( permLimitFrm, this )"</ffi:cinclude>>
						</div>
					</ffi:cinclude>
					<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="notEquals">
						<div align="center">
							<ffi:setProperty name="LastRequest" property="Name" value="admin${limitIndex}${TemplateIndex}"/>

							<ffi:setProperty name="LastRequest" property="CheckboxValue" value=""/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
							<ffi:removeProperty name="admin${limitIndex}${TemplateIndex}"/>
							&nbsp;<input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
						</div>
					</ffi:cinclude>
				</td>
			</ffi:cinclude>
		</ffi:cinclude>

		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:cinclude value1="${AdminCheckBoxTypeDA}" value2="hidden" >
			<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
					<ffi:setProperty name="LastRequest" property="Name" value="admin${limitIndex}${TemplateIndex}"/>
					<ffi:setProperty name="LastRequest" property="CheckboxValue" value=""/>
					<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
					<ffi:removeProperty name="admin${limitIndex}${TemplateIndex}"/>
					<input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
			</ffi:cinclude>
		</ffi:cinclude>
		</ffi:cinclude>

		<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${LimitType.OperationName}"/>
		<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
			<ffi:setProperty name="CheckEntitlementByMember" property="ObjectType" value="${CheckEntitlementObjectType}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
			<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${CheckEntitlementObjectId}"/>
		</ffi:cinclude>
		<ffi:process name="CheckEntitlementByMember"/>

		<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>
		<td class="tbrd_t" valign="middle">
			<div align="center">
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:setProperty name="LastRequest" property="Name" value="entitlement${limitIndex}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:setProperty name="LastRequest" property="Name" value="entitlement${limitIndex}_${TemplateIndex}"/>
	</ffi:cinclude>
				<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${LimitType.OperationName}"/>
				<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:removeProperty name="entitlement${limitIndex}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:removeProperty name="entitlement${limitIndex}_${TemplateIndex}"/>
	</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<ffi:process name="GetEntitlementsFromDA" />
				</ffi:cinclude>
				<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0" <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( permLimitFrm, this )"</ffi:cinclude>>
			</div>
		</td>
				<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
                <%  String classParam = "tbrd_t columndata"; %>
				<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">

					<% String indentLevel; %>
					<ffi:getProperty name="LimitType" property="Value" assignTo="indentLevel"/>
					<%
						int indentLevelAsInt = Integer.parseInt( indentLevel );
						if (indentLevelAsInt > 0) {
                           classParam = "tbrd_t indent" + Integer.toString(indentLevelAsInt) + " columndata";
						}
					%>
				</ffi:cinclude>
		<td class='<ffi:getPendingStyle fieldname="${LastRequest.CheckboxValue}" defaultcss="<%=classParam %>"
						dacss='<%=classParam + " columndataDA" %>'
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'
						valign="middle" align="left">
				<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
				<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
				<ffi:process name="GetEntitlementPropertyValues"/>
				<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
					<ffi:getProperty name="LimitType" property="OperationName"/>
				</ffi:cinclude>
		</td>

		<ffi:setProperty name="GetGroupLimits" property="Period" value="1"/>
		<ffi:process name="GetGroupLimits"/>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="1"/>
			<ffi:process name="GetLimitsFromDA" />
		</ffi:cinclude>
		<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
		<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:removeProperty name="transaction_limit${limitIndex}"/>
</ffi:cinclude>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:removeProperty name="transaction_limit${limitIndex}_${TemplateIndex}"/>
</ffi:cinclude>
		<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
		</ffi:list>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:setProperty name="LastRequest" property="Name" value="transaction_limit${limitIndex}"/>
</ffi:cinclude>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:setProperty name="LastRequest" property="Name" value="transaction_limit${limitIndex}_${TemplateIndex}"/>
</ffi:cinclude>
				<%-- Requires Approval Processing --%>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
	<ffi:setProperty name="CheckRequiresApproval" property="OperationName" value="${LimitType.OperationName}" />
	<ffi:process name="CheckRequiresApproval" />
	<td class="tbrd_t" valign="middle">&nbsp;&nbsp;
		<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="true" operator="equals">
			<input name="req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />" type="checkbox"  disabled value="<ffi:getProperty name="CheckRequiresApproval" property="OperationName" />"
			 onclick="disableExceedLimitById(this.form, this, '<ffi:getProperty name="limitIndex" />')"
			 <ffi:getProperty name="CheckRequiresApproval" property="Enabled" />
			 <ffi:getProperty name="CheckRequiresApproval" property="Selected" />
			 />
			<ffi:setProperty name="EditRequiresApproval" property="ReqApprOpName" value="${LimitType.OperationName}" />
			<ffi:setProperty name="DisableExceed" value="${CheckRequiresApproval.Selected}" />
			<%-- Set display properties into javascript --%>
			<script type="text/javascript">
				var ra_indx = (<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" /> - 1);
				ra_disp_props[ra_indx] = new Array('req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />', '<ffi:getProperty name="CheckRequiresApproval" property="DisplayParent" />', '<ffi:getProperty name="CheckRequiresApproval" property="ControlParent" />');
				name_id_map['<ffi:getProperty name="CheckRequiresApproval" property="OperationName" />'] = 'req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />';
				ra_limits['req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />'] = '<ffi:getProperty name="limitIndex" />';
			</script>
		</ffi:cinclude>
		<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="true" operator="notEquals">
			&nbsp;
		</ffi:cinclude>
	</td>
 </ffi:cinclude>
</ffi:cinclude>
		<%-- END Requires Approval Processing --%>

		<td class="tbrd_t" valign="middle">
			<div align="center">
				<input class="ui-widget-content ui-corner-all" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
			</div>
		</td>
		<td class="tbrd_t" valign="middle" align="left" nowrap>

		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals" >
			<span class="columndata"><s:text name="jsp.user_244"/></span>
			</ffi:cinclude>
			<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="equals" >
			<span class="columndataDA"><s:text name="jsp.user_244"/></span>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<span class="columndata"><s:text name="jsp.user_244"/></span>
		</ffi:cinclude>

		</td>
		<td class="tbrd_t" valign="middle" align="center">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
				<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
				<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:setProperty name="LastRequest" property="Name" value="transaction_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:setProperty name="LastRequest" property="Name" value="transaction_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:removeProperty name="transaction_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:removeProperty name="transaction_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>

				<%-- Requires Approval Relation Processing --%>
				<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
					<ffi:cinclude value1="${DisableExceed}" value2="checked" operator="equals">
					disabled
					</ffi:cinclude>
				</ffi:cinclude>
				<%--END Requires Approval Relation Processing --%>
					/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</td>

		<ffi:setProperty name="GetGroupLimits" property="Period" value="2"/>
		<ffi:process name="GetGroupLimits"/>

		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="2"/>
			<ffi:process name="GetLimitsFromDA" />
		</ffi:cinclude>

		<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
		<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:removeProperty name="day_limit${limitIndex}"/>
</ffi:cinclude>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:removeProperty name="day_limit${limitIndex}_${TemplateIndex}"/>
</ffi:cinclude>
		<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
		</ffi:list>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:setProperty name="LastRequest" property="Name" value="day_limit${limitIndex}"/>
</ffi:cinclude>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:setProperty name="LastRequest" property="Name" value="day_limit${limitIndex}_${TemplateIndex}"/>
</ffi:cinclude>
		<td class="tbrd_t" valign="middle">
			<div align="center">
				<input class="ui-widget-content ui-corner-all" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
			</div>
		</td>
		<td class="tbrd_t" valign="middle" align="left">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals" >
				<span class="columndata"><s:text name="jsp.user_242"/></span>
			</ffi:cinclude>
			<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="equals" >
				<span class="columndataDA"><s:text name="jsp.user_242"/></span>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<span class="columndata"><s:text name="jsp.user_242"/></span>
		</ffi:cinclude>

		</td>
		<td class="tbrd_t" valign="middle" align="center">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
				<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
				<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:setProperty name="LastRequest" property="Name" value="day_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:setProperty name="LastRequest" property="Name" value="day_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:removeProperty name="day_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:removeProperty name="day_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>
					<%-- Requires Approval Relation Processing --%>
				<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
					<ffi:cinclude value1="${DisableExceed}" value2="checked" operator="equals">
					disabled
					</ffi:cinclude>
				</ffi:cinclude>
				<%--END Requires Approval Relation Processing --%>
					/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</td>
	</tr>
	<tr>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
				<td></td>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<td colspan="2"></td>
			<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
		<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
		<td valign="middle"></td>
                </ffi:cinclude>
</ffi:cinclude>
			<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerTransaction" /></span></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<td colspan="3"></td>
		</ffi:cinclude>
		<td colspan="2" align="left"><span class="columndata">(<ffi:getProperty name="GetMaxLimitForPeriod" property="TransactionLimitDisplay"/>)</span></td>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<td></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerDay" /></span></td>
		</ffi:cinclude>
		<td colspan="2" style="text-align: left;"><span class="columndata">(<ffi:getProperty name="GetMaxLimitForPeriod" property="DailyLimitDisplay"/>)</span></td>
	</tr>
	<tr>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
				<td></td>
			</ffi:cinclude>
		</ffi:cinclude>
		<td></td>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
		<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
		<td valign="middle"></td>
                </ffi:cinclude>
</ffi:cinclude>
		<td></td>

		<ffi:setProperty name="GetGroupLimits" property="Period" value="3"/>
		<ffi:process name="GetGroupLimits"/>

		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="3"/>
			<ffi:process name="GetLimitsFromDA" />
		</ffi:cinclude>

		<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
		<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:removeProperty name="week_limit${limitIndex}"/>
</ffi:cinclude>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:removeProperty name="week_limit${limitIndex}_${TemplateIndex}"/>
</ffi:cinclude>
		<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
		</ffi:list>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:setProperty name="LastRequest" property="Name" value="week_limit${limitIndex}"/>
</ffi:cinclude>
<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:setProperty name="LastRequest" property="Name" value="week_limit${limitIndex}_${TemplateIndex}"/>
</ffi:cinclude>
		<td valign="middle">
			<div align="center">
				<input class="ui-widget-content ui-corner-all" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
			</div>
		</td>
		<td valign="middle" align="left">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="notEquals" >
				<span class="columndata"><s:text name="jsp.user_245"/></span>
			</ffi:cinclude>
			<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="equals" >
				<span class="columndataDA"><s:text name="jsp.user_245"/></span>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<span class="columndata"><s:text name="jsp.user_245"/></span>
		</ffi:cinclude>

		</td>
		<td valign="middle" align="center">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
				<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
				<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:setProperty name="LastRequest" property="Name" value="week_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:setProperty name="LastRequest" property="Name" value="week_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:removeProperty name="week_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:removeProperty name="week_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>
					<%-- Requires Approval Relation Processing --%>
					<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
						<ffi:cinclude value1="${DisableExceed}" value2="checked" operator="equals">
						disabled
						</ffi:cinclude>
					</ffi:cinclude>
					<%--END Requires Approval Relation Processing --%>
					>
				</ffi:cinclude>
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</td>

		<ffi:setProperty name="GetGroupLimits" property="Period" value="4"/>
		<ffi:process name="GetGroupLimits"/>

		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="4"/>
			<ffi:process name="GetLimitsFromDA" />
		</ffi:cinclude>

		<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
		<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
		<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:removeProperty name="month_limit${limitIndex}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:removeProperty name="month_limit${limitIndex}_${TemplateIndex}"/>
	</ffi:cinclude>
		</ffi:list>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:setProperty name="LastRequest" property="Name" value="month_limit${limitIndex}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:setProperty name="LastRequest" property="Name" value="month_limit${limitIndex}_${TemplateIndex}"/>
	</ffi:cinclude>
		<td valign="middle">
			<div align="center">
				<input class="ui-widget-content ui-corner-all" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
			</div>
		</td>
		<td valign="middle" align="left">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="notEquals" >
				<span class="columndata"><s:text name="jsp.user_243"/></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="equals" >
				<span class="columndataDA"><s:text name="jsp.user_243"/></span>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<span class="columndata"><s:text name="jsp.user_243"/></span>
			</ffi:cinclude>

		</td>
		<td valign="middle" align="center">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
				<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
				<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:setProperty name="LastRequest" property="Name" value="month_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:setProperty name="LastRequest" property="Name" value="month_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:removeProperty name="month_exceed${limitIndex}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:removeProperty name="month_exceed${limitIndex}_${TemplateIndex}"/>
					</ffi:cinclude>
					<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>
					<%-- Requires Approval Relation Processing --%>
					<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
						<ffi:cinclude value1="${DisableExceed}" value2="checked" operator="equals">
						disabled
						</ffi:cinclude>
					</ffi:cinclude>
					<%--END Requires Approval Relation Processing --%>
					/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</td>
	</tr>
	<tr>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
				<td></td>
			</ffi:cinclude>
		</ffi:cinclude>
        <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
            <td colspan="2"></td>
            <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
        <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
        <td valign="middle"></td>
                </ffi:cinclude>
</ffi:cinclude>
            <td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerWeek" /></span></td>
        </ffi:cinclude>
        <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
    <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
                <td colspan="4"></td>
            </ffi:cinclude>
            <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="notEquals">
                <td colspan="2"></td>
            </ffi:cinclude>
    </ffi:cinclude>
		<td colspan="2" style="text-align: left;"><span class="columndata">(<ffi:getProperty name="GetMaxLimitForPeriod" property="WeeklyLimitDisplay"/>)</span></td>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerMonth" /></span></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<td></td>
		</ffi:cinclude>
		<td colspan="2" style="text-align: left;"><span class="columndata">(<ffi:getProperty name="GetMaxLimitForPeriod" property="MonthlyLimitDisplay"/>)</span></td>
	</tr>
	<ffi:cinclude value1="${DisplayErrors}" value2="TRUE" operator="equals">
		<tr>
			<td></td>
			<td colspan="7" valign="middle" align="left" class="errortext">
		<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
			<ffi:getProperty name="limit_error${limitIndex}" encode="false"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
			<ffi:getProperty name="limit_error${limitIndex}_${TemplateIndex}" encode="false"/>
		</ffi:cinclude>
			</td>
		</tr>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
		<ffi:removeProperty name="limit_error${limitIndex}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
		<ffi:removeProperty name="limit_error${limitIndex}_${TemplateIndex}"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:removeProperty name="DisableExceed" />
