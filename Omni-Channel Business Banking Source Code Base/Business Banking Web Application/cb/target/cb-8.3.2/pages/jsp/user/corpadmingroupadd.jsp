<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil"%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:setProperty name="CanAdministerAnyGroup" property="GroupType" value="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>"/>
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<ffi:help id="user_corpadmingroupadd" className="moduleHelpClass"/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_19"/></span>
<ffi:setProperty name='PageText' value=''/>

<script language="javascript">
	$(function(){
		$("#DivisionSelectId").selectmenu({width: 150});
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#addGroupFormId").setFocus();
		}
	});	
</script>

<%
	String CancelReload = request.getParameter("CancelReload");
	if(CancelReload!= null){
		session.setAttribute("CancelReload", CancelReload);
	}
	String AdminsSelected = request.getParameter("AdminsSelected");
	if(AdminsSelected!= null){
		session.setAttribute("AdminsSelected", AdminsSelected);
	}
	String AutoEntitleAdministrators = request.getParameter("AutoEntitleAdministrators");
	if(AutoEntitleAdministrators!= null){
		session.setAttribute("AutoEntitleAdministrators", AutoEntitleAdministrators);
	}
	String UseLastRequest = request.getParameter("UseLastRequest");
	if(UseLastRequest!= null){
		session.setAttribute("UseLastRequest", UseLastRequest);
	}
%>

<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
// The user has pressed cancel, so go back to the previous version of the collections
session.setAttribute( "NonAdminEmployees", session.getAttribute("NonAdminEmployeesCopy"));
session.setAttribute( "AdminEmployees", session.getAttribute("AdminEmployeesCopy"));
session.setAttribute( "NonAdminGroups", session.getAttribute("NonAdminGroupsCopy"));
session.setAttribute( "AdminGroups", session.getAttribute("AdminGroupsCopy"));
session.setAttribute( "adminMembers", session.getAttribute("adminMembersCopy"));
session.setAttribute( "userMembers", session.getAttribute("userMembersCopy"));
session.setAttribute( "groupIds", session.getAttribute("groupIdsCopy"));
session.setAttribute( "tempAdminEmps", session.getAttribute("tempAdminEmpsCopy") );
session.setAttribute( "GroupDivAdminEdited", session.getAttribute("GroupDivAdminEditedCopy"));
%>
</ffi:cinclude>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="CancelReload"/>

<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
<ffi:process name="GetGroupsAdministeredBy"/>
<ffi:removeProperty name="GetGroupsAdministeredBy" />

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
	<ffi:object id="AddBusinessGroup" name="com.ffusion.tasks.admin.AddBusinessGroup" scope="session"/>
	<% session.setAttribute("FFIAddBusinessGroup", session.getAttribute("AddBusinessGroup")); %>
</ffi:cinclude>
	
<ffi:removeProperty name="UseLastRequest"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>

<ffi:setProperty name="GetAncestorGroupByType" property="GroupType" value="Division"/>
<ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="DivisionEntGroup"/>
<ffi:process name="GetAncestorGroupByType"/>

<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<s:form id="addGroupFormId" namespace="/pages/user" action="addBusinessGroup-verify" theme="simple" name="groupAddFormName" method="post">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                            <input type="hidden" name="GroupType" value="Group">
              			    <input type="hidden" name="CheckboxesAvailable" value="false">
							<input type="hidden" name="UseLastRequest" value="TRUE">
							<input type="hidden" name="divAdd" value="true">
							
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td width="40">&nbsp;</td>
									<td width="100"><s:text name="jsp.default_226"/>:<span class="required">*</span></td>
									<td>
										<input class="ui-widget-content ui-corner-all" type="text" name="GroupName" style="width:140px;" size="24" maxlength="150" border="0" value="<ffi:cinclude value1="${tempGroupName}" value2="" operator="equals"><ffi:getProperty name="AddBusinessGroup" property="GroupName"/></ffi:cinclude><ffi:cinclude value1="${tempGroupName}" value2="" operator="notEquals"><ffi:getProperty name="tempGroupName"/></ffi:cinclude>">
										<input class="ui-widget-content ui-corner-all" type="text" name="nonDisplayGroupName" size="24" maxlength="150" border="0" value="" style="display:none">
										(<ffi:getProperty name="Business" property="BusinessName"/>) &nbsp;&nbsp; <span id="GroupNameError"></span>
									</td>
								</tr>
								<tr>
									<td width="40">&nbsp;</td>
									<td><s:text name="jsp.default_174"/>:<span class="required">*</span></td>
									<td valign="middle">
										<select id="DivisionSelectId" class="txtbox" name="ParentGroupId">
											<option value="-1"><s:text name="jsp.default_375"/></option>
				                                                <ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
										<ffi:cinclude value1="${EntGroupItem.EntGroupType}" value2="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>" operator="equals">
												<ffi:cinclude value1="${AddBusinessGroup.ParentGroupId}" value2="0" operator="equals">
													<ffi:cinclude value1="${FirstDivision}" value2="" operator="equals">
														<ffi:setProperty name="FirstDivision" value="${EntGroupItem.GroupId}"/>
													</ffi:cinclude>
												</ffi:cinclude>
                                       		<option value='<ffi:getProperty name="EntGroupItem" property="GroupId"/>' <ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${AddBusinessGroup.ParentGroupId}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="EntGroupItem" property="GroupName"/></option>
										</ffi:cinclude>
                                       	 </ffi:list>
										</select>
										<span id="ParentGroupIdError"></span>
									</td>
								</tr>
								<tr>
									<ffi:setProperty name="AddBusinessGroup" property="AdminListName" value="adminMembers"/>
									<td width="40">&nbsp;</td>
									<td><s:text name="jsp.user_38"/>:</td>
									<td valign="middle">
										<div style="display:inline-block;">
											<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
											<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
											   <ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="tempAdminEmps" />
											   <ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
											<ffi:process name="FilterNotEntitledEmployees"/>
		 									<ffi:setProperty name="tempAdminEmps" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
											<%--display name of each employee in BusinessEmployees--%>
	 										<%
	 									       		boolean isFirst= true;
	 											String temp = "";
		 								       	%>
	 										<ffi:list collection="tempAdminEmps" items="emp">
			 								<%--display separator if this is not the first one--%>
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
												<ffi:setProperty name="empName" value="${emp.Name}"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
												<ffi:setProperty name="empName" value="${emp.LastName}"/>
											</ffi:cinclude>
	 										<%
	 										if( isFirst ) {
	 											isFirst = false;
		 										temp = temp + session.getAttribute( "empName" );
	 										} else {
	 											temp = temp + ", " + session.getAttribute( "empName" );
	 										}
		 									%>
		 									</ffi:list>
	 										<%
		 									if( temp.length() > 120 ) {
	 											temp = temp.substring( 0, 120 );
	 											int index = temp.lastIndexOf( ',' );
												temp = temp.substring( 0, index ) + "...";
	 										}
	 										session.setAttribute( "AdministratorStr", temp );%>
	
		 									<ffi:getProperty name="AdministratorStr"/>
											<ffi:cinclude value1="AdministratorStr" value2="" operator="equals">
												<s:text name="jsp.default_296"/>
											</ffi:cinclude>
											<%-- set modifiedAdmins to "TRUE", so when the edit icon is clicked the corpadminadminedit.jsp --%>
											<%-- will look for the appropriate object in session for displaying --%>
											<ffi:setProperty name="modifiedAdmins" value="TRUE"/>
											<ffi:removeProperty name="empName"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
											<%-- By default, the administor for the new group is the currently logged in user --%>
											<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
											<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
											<ffi:process name="GetBusinessEmployee"/>
											<ffi:removeProperty name="GetBusinessEmployee"/>
											<ffi:getProperty name="BusinessEmployee" property="Name"/>
											<%
											   java.util.ArrayList adminMembers = new java.util.ArrayList();
											   BusinessEmployee tempEmp = (BusinessEmployee)session.getAttribute( com.ffusion.tasks.Task.BUSINESS_EMPLOYEE );
											   EntitlementGroupMember member = EntitlementsUtil.getEntitlementGroupMemberForBusinessEmployee(tempEmp);
									         	   adminMembers.add( member );
											   session.setAttribute( "adminMembers", adminMembers );
											   session.setAttribute( "AdministratorStr", tempEmp.getUserName() );
											%>
											<ffi:setProperty name="modifiedAdmins" value="FALSE"/>
										</ffi:cinclude>
										
											<s:hidden id="needVerify_HiddenFieldId" name="needVerify" value="true"/>
											<%-- Kaijies: add administrator icon button --%>
											<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
												<sj:a
													id="addGroupAdminButtonId"
													button="true"
													targets="editAdminerDialogId"
													formIds="addGroupFormId"
													formid="addGroupFormId"
													validate="true" 
													validateFunction="customValidation"
		                                            onBeforeTopics="beforeVerify2" 
													field1="needVerify_HiddenFieldId"
													value1="false"
													buttontype="editadmin"
													postaction="/cb/pages/user/addBusinessGroup-addGroupModifyAdminerBeforeChange.action"																								
													onClickTopics="postFormTopic"											
													onCompleteTopics="openGroupAdminerDialog"
												>
													<s:text name="jsp.default_178" />
												</sj:a>
											</ffi:cinclude>
											<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
												<sj:a
													id="addGroupAdminButtonId"
													button="true"
													targets="editAdminerDialogId"
													formIds="addGroupFormId"
													formid="addGroupFormId"
													validate="true" 
													validateFunction="customValidation"
		                                            onBeforeTopics="beforeVerify2" 
													field1="needVerify_HiddenFieldId"
													value1="false"
													postaction="/cb/pages/user/addBusinessGroup-addGroupModifyAdminerAfterChange.action"																								
													onClickTopics="postFormTopic"												
													onCompleteTopics="openGroupAdminerDialog"
												>
													<s:text name="jsp.default_178" />
												</sj:a>
											</ffi:cinclude>
											
											<ffi:removeProperty name="AdminsSelected"/>
										</div>
									   &nbsp;&nbsp;
									   
									   
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<div align="center">
												<span class="required">* <s:text name="jsp.default_240"/></span>
												<br><br>
												
												<%--
											<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
												<span class="required">* <s:text name="jsp.user_198"/></span>
												<br>
											</ffi:cinclude>
											--%>
											<br>
											
											<s:url id="resetAddGroupButtonUrl" value="/pages/jsp/user/corpadmingroupadd.jsp">  
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
											</s:url>	
											<sj:a id="resetAddGroupBtn"
                                                  href="%{resetAddGroupButtonUrl}"
											   targets="inputDiv"
											   button="true" ><s:text name="jsp.default_358"/></sj:a>
                                               <sj:a id="cancelBtn"
                                                  button="true" 
                                                  summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelGroupForm"><s:text name="jsp.default_82"/></sj:a>
                                               <sj:a
                                                formIds="addGroupFormId"
                                                targets="verifyDiv"
                                                button="true"
                                                validate="true" validateFunction="customValidation"
                                                onBeforeTopics="beforeVerify2" 
												onCompleteTopics="completeVerify2"
                                                onErrorTopics="errorVerify" 
												onSuccessTopics="successVerify"
												formid="addGroupFormId"
												field1="needVerify_HiddenFieldId"
												value1="true"
												postaction="/cb/pages/user/addBusinessGroup-verify.action"																								
												onClickTopics="postFormTopic"																							
                                               	><s:text name="jsp.default_29"/></sj:a>
													
											</div>
									</td>
								</tr>
							</table>
						</s:form>
					</td>
				</tr>
			</table>		
</div>

<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupadd.jsp?dontRefresh=false&AdminsSelected=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupadd.jsp?dontRefresh=false" URLEncrypt="true"/>
</ffi:cinclude>

<ffi:setProperty name="onCancelGoto" value="corpadmingroupadd.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmingroupadd.jsp"/>

<ffi:removeProperty name="DivisionEntGroup"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
