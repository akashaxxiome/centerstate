<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_baiexportsettingsedit" className="moduleHelpClass"/>
<div id='PageHeading' style="display:none;"><s:text name="jsp.user_138"/></div>

<s:include value="inc/baiexportsettingsedit-pre.jsp" />
<ffi:setProperty name="BAIEportSettingDAMode" value="${Business.dualApprovalMode}" />
<%
session.setAttribute("FFIBAIExportSettings", session.getAttribute("SetBAIExportSettings"));
%>
<style>
	#ultimateReceiverIDCustomID { margin-right: 40px \9; }
</style>
<script type="text/javascript"><!--
$(function(){
	 ns.admin.toggleTextBoxes();
	 
	 $("#resetBAISettingsForm").button({
		icons: {
		}
	});
});


ns.admin.resetBAISettingsForm = function ()
{
	removeValidationErrors();
	$("#editBAIExportSettingsForm").get(0).reset();
	ns.admin.toggleTextBoxes();
}

function callCancel()
{
	// return back to the previous page
	//window.location.href="corpadmininfo.jsp";
}
//--></script>

  <div align="center">

  <%-- include page header, PageHeading and PageText should be set --%>


  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="adminBackground" align="center"><img src="/cb/web/multilang/grafx/user/spacer.gif" height="7" width="100%" border="0" alt=""></td>
		</tr>
	<tr>
	  <td align="center" class="adminBackground">
	  
		<s:form id="editBAIExportSettingsForm" namespace="/pages/user" name="editBAIExportSettingsForm" action="setBAIExportSettings-verify" validate="false" method="post" theme="simple">
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<table width="80%" border="0" cellspacing="0" cellpadding="3">
		  <tr>
			<td colspan="2" class="<ffi:getPendingStyle fieldname="senderIDType" dacss="sectionheadDA" defaultcss="sectionhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" align="left" ><s:text name="jsp.user_295"/></td>
			<td> &nbsp; </td>
			<td colspan="2" class="<ffi:getPendingStyle fieldname="receiverIDType" dacss="sectionheadDA" defaultcss="sectionhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" align="left" ><s:text name="jsp.user_270"/></td>
		  </tr>
		  <tr>
			<td colspan="2">
				<table>
					<tr>
		    				<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="senderIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" <ffi:cinclude value1="${BAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals"> Checked </ffi:cinclude> onClick="ns.admin.toggleTextBoxes()"; >
							<s:text name="jsp.user_358"/>
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
							<input type="hidden" name="oldSenderIDType" value="<ffi:getProperty name="BAIExportSettings" property="SenderIDType"/>"/>
		    				</td>
		  			</tr>
		  			<tr>
						<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="senderIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" 
								<ffi:cinclude value1="${BAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals"> Checked </ffi:cinclude>
								 onClick="ns.admin.toggleTextBoxes();">
								<s:text name="jsp.user_361"/>
		    				</td>
		  			</tr>
					<tr>						
						<td colspan="2" style="padding-left:15px;">
							<!--
							<input class="txtbox" type="text" name="senderIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="SenderIDCustom"/>" size="36" maxlength="100" border="0">
							-->
							<s:textfield name="senderIDCustom" size="36" style="width:180px;float:left;" maxlength="100" value="%{#session.BAIExportSettings.SenderIDCustom}" cssClass="ui-widget-content ui-corner-all"/><br>
							
							<input type="hidden" name="oldSenderIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="SenderIDCustom"/>"/>
						</td>
					</tr>
					<tr>
						<td colspan="2"><span id="senderIDCustomError"></span></td>
					</tr>
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="" operator="notEquals">
						<tr>
							<td colspan="2" class="sectionhead_greyDA">&nbsp;&nbsp;
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
									<s:text name="jsp.user_358"/>
									<ffi:getProperty name="TempBankIdentifierDisplayText"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
									<ffi:getProperty name="OLD_BAIEXPORT_SETTINGS" property="SenderIDCustom"/>
								</ffi:cinclude>
							</td>
						</tr>						
					</ffi:cinclude>					
				</table>
			</td>
			<td> &nbsp; </td>
			<td colspan="2">
				<table>
					<tr>
		    				<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="receiverIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" 
								<ffi:cinclude value1="${BAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals"> Checked </ffi:cinclude> 
							onClick="ns.admin.toggleTextBoxes()"; >
							<s:text name="jsp.user_358"/>
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
							<input type="hidden" name="oldReceiverIDType" value="<ffi:getProperty name="BAIExportSettings" property="ReceiverIDType"/>"/>
		    				</td>
		  			</tr>
		  			<tr>
						<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="receiverIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" <ffi:cinclude value1="${BAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals"> Checked </ffi:cinclude> onClick="ns.admin.toggleTextBoxes();">
							<s:text name="jsp.user_360"/>
		    				</td>
					</tr>
					<tr>										
						<td colspan="2" style="padding-left:15px;">
							<!--
							<input class="txtbox" type="text" name="receiverIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="ReceiverIDCustom"/>" size="36" maxlength="100" border="0">
							-->
							<s:textfield name="receiverIDCustom" size="36" maxlength="100" style="width:180px;float:left;" value="%{#session.BAIExportSettings.ReceiverIDCustom}" cssClass="ui-widget-content ui-corner-all"/><br>
							
							<input type="hidden" name="oldReceiverIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="ReceiverIDCustom"/>"/>
						</td>
		  			</tr>
		  			<tr>
						<td colspan="2"><span id="receiverIDCustomError"></span></td>
					</tr>
		  			<ffi:cinclude value1="${AddBAIExportSettingsToDA.receiverIDType}" value2="" operator="notEquals">
						<tr>
							<td colspan="2" class="sectionhead_greyDA">&nbsp;&nbsp;
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.receiverIDType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
									<s:text name="jsp.user_358"/>
									<ffi:getProperty name="TempBankIdentifierDisplayText"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.receiverIDType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
									<ffi:getProperty name="OLD_BAIEXPORT_SETTINGS" property="ReceiverIDCustom"/>
								</ffi:cinclude>
							</td>
						</tr>						
					</ffi:cinclude>		  					  			
				</table>
			</td>
		  </tr>
		  <tr>
			<td colspan="2" class="<ffi:getPendingStyle fieldname="ultimateReceiverIDType" dacss="sectionheadDA" defaultcss="sectionhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" align="left" ><s:text name="jsp.user_348"/></td>
			<td> &nbsp; </td>
			<td colspan="2" class="<ffi:getPendingStyle fieldname="originatorIDType" dacss="sectionheadDA" defaultcss="sectionhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" align="left" ><s:text name="jsp.user_232"/></td>
		  </tr>
		  <tr>
			<td colspan="2">
				<table>
					<tr>
		    				<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="ultimateReceiverIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" <ffi:cinclude value1="${BAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals"> Checked </ffi:cinclude> onClick="ns.admin.toggleTextBoxes()"; >
							<s:text name="jsp.user_358"/>
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
							<input type="hidden" name="oldUltimateReceiverIDType" value="<ffi:getProperty name="BAIExportSettings" property="UltimateReceiverIDType"/>"/>
		    				</td>
		  			</tr>
		  			<tr>
						<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="ultimateReceiverIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" <ffi:cinclude value1="${BAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals"> Checked </ffi:cinclude> onClick="ns.admin.toggleTextBoxes();">
							<s:text name="jsp.user_362"/>
		    				</td>
					</tr>
					<tr>						
						<td colspan="2" style="padding-left:15px;">
							<!--
							<input class="txtbox" type="text" name="ultimateReceiverIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="UltimateReceiverIDCustom"/>" size="36"  maxlength="100" border="0">
							-->
							<s:textfield name="ultimateReceiverIDCustom" size="36" maxlength="100" style="width:180px;float:left;" id="ultimateReceiverIDCustomID" value="%{#session.BAIExportSettings.UltimateReceiverIDCustom}" cssClass="ui-widget-content ui-corner-all"/><br>
							
							<input type="hidden" name="oldUltimateReceiverIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="UltimateReceiverIDCustom"/>"/>
						</td>
		  			</tr>
		  			<tr>
						<td colspan="2"><span id="ultimateReceiverIDCustomError"></span></td>
					</tr>
		  			<ffi:cinclude value1="${AddBAIExportSettingsToDA.ultimateReceiverIDType}" value2="" operator="notEquals">
						<tr>
							<td colspan="2" class="sectionhead_greyDA">&nbsp;&nbsp;
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.ultimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
									<s:text name="jsp.user_358"/>
									<ffi:getProperty name="TempBankIdentifierDisplayText"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.ultimateReceiverIDType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
									<ffi:getProperty name="OLD_BAIEXPORT_SETTINGS" property="UltimateReceiverIDCustom"/>
								</ffi:cinclude>
							</td>
						</tr>						
					</ffi:cinclude>		
				</table>
			</td>
			<td> &nbsp; </td>
			<td colspan="2">
				<table>
					<tr>
		    				<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="originatorIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" <ffi:cinclude value1="${BAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals"> Checked </ffi:cinclude> onClick="ns.admin.toggleTextBoxes();" >
							<s:text name="jsp.user_355"/>
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
							<input type="hidden" name="oldOriginatorIDType" value="<ffi:getProperty name="BAIExportSettings" property="OriginatorIDType"/>"/>
		    				</td>
		  			</tr>
		  			<tr>
						<td colspan="2" class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="originatorIDType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" <ffi:cinclude value1="${BAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals"> Checked </ffi:cinclude> onClick="ns.admin.toggleTextBoxes();">
							<s:text name="jsp.user_359"/>
		    				</td>
					</tr>
					<tr>						
						<td colspan="2" style="padding-left:15px;">
							<!--
							<input class="txtbox" type="text" name="originatorIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="OriginatorIDCustom"/>" size="36"  maxlength="100" border="0">
							-->
							<s:textfield name="originatorIDCustom" size="36" maxlength="100" style="width:180px;float:left;" value="%{#session.BAIExportSettings.OriginatorIDCustom}" cssClass="ui-widget-content ui-corner-all"/><br>
							<input type="hidden" name="oldOriginatorIDCustomValue" value="<ffi:getProperty name="BAIExportSettings" property="OriginatorIDCustom"/>"/>
						</td>
		  			</tr>
		  			<tr>
						<td colspan="2"><span id="originatorIDCustomError"></span></td>
					</tr>
		  			<ffi:cinclude value1="${AddBAIExportSettingsToDA.originatorIDType}" value2="" operator="notEquals">
						<tr>
							<td colspan="2" class="sectionhead_greyDA">&nbsp;&nbsp;
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.originatorIDType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER%>" operator="equals">
									<s:text name="jsp.user_354"/> 
									<ffi:getProperty name="TempBankIdentifierDisplayText"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.originatorIDType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
									<ffi:getProperty name="OLD_BAIEXPORT_SETTINGS" property="OriginatorIDCustom"/>
								</ffi:cinclude>
							</td>
						</tr>						
					</ffi:cinclude>		
				</table>
			</td>
		  </tr>
		  <tr>
			<td colspan="2" class="<ffi:getPendingStyle fieldname="customerAccountNumberType" dacss="sectionheadDA" defaultcss="sectionhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" align="left" ><s:text name="jsp.user_100"/></td>
			<td> &nbsp; </td>
			<td colspan="2"> &nbsp; </td>
		  </tr>
		  <tr>
			<td colspan="2">
				<table>
					<tr>
		    				<td class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="customerAccountNumberType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" <ffi:cinclude value1="${BAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals"> Checked </ffi:cinclude> >
							<s:text name="jsp.user_356"/>
							<input type="hidden" name="oldCustomerAccountNumberType" value="<ffi:getProperty name="BAIExportSettings" property="CustomerAccountNumberType"/>"/>
		    				</td>
		    				<td>
							&nbsp;
		    				</td>
		  			</tr>
		  			<tr>
						<td class="columndata" align="left" valign="baseline">
							<INPUT	TYPE="radio" NAME="customerAccountNumberType" VALUE="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" <ffi:cinclude value1="${BAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals"> Checked </ffi:cinclude> >
							<s:text name="jsp.user_352"/> 
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
							 <s:text name="jsp.user_41"/> (eg. 567483920:<%= com.ffusion.util.settings.AccountSettings.buildAccountId("50000", "1") %>)
		    				</td>
						<td>
							&nbsp;
						</td>
		  			</tr>
		  			<ffi:cinclude value1="${AddBAIExportSettingsToDA.customerAccountNumberType}" value2="" operator="notEquals">
						<tr>
							<td colspan="2" class="sectionhead_greyDA">&nbsp;&nbsp;
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.customerAccountNumberType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
									<s:text name="jsp.user_356"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${OLD_BAIEXPORT_SETTINGS.customerAccountNumberType}" value2="<%=com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">										
									<s:text name="jsp.user_353"/>
									<ffi:getProperty name="TempBankIdentifierDisplayText"/>
							 		<s:text name="jsp.user_41"/> (eg. 567483920:<%= com.ffusion.util.settings.AccountSettings.buildAccountId("50000", "1") %>)
								</ffi:cinclude>
							</td>
						</tr>						
					</ffi:cinclude>	
				</table>
			</td>
			<td> &nbsp; </td>
			<td colspan="2"> &nbsp; </td>
		  </tr>
		  <tr>
		    <td colspan="5" class="columndata" align="center" valign="top">
			<%-- <a 	id="resetBAISettingsForm" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
				title='<s:text name="jsp.default_358.1" />' 
				href='#' 
				onClick="ns.admin.resetBAISettingsForm();">
				<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358" /></span>
			</a> --%>
			
			<a id="resetBAISettingsForm"	 
				title='<s:text name="jsp.default_358.1" />' 
				href='#' 
				onClick="ns.admin.resetBAISettingsForm();">
				<s:text name="jsp.default_358" />
			</a>
			
			<sj:a
				button="true" summaryDivId="companySummaryTabsContent" buttonType="cancel"
				onClickTopics="showSummary,cancelCompanyForm,cancelEditBAICompanyForm"
				><s:text name="jsp.default_82"/></sj:a>
			<sj:a 
				formIds="editBAIExportSettingsForm"
				targets="verifyDiv"
				button="true"
				validate="true" 
				validateFunction="customValidation"
				onBeforeTopics="beforeVerify"
				onCompleteTopics="completeVerify"
				onErrorTopics="errorVerify"
				><s:text name="jsp.default_366"/></sj:a>
		    </td>
		  </tr>
		</table>
	    </s:form>
	    <br>
	  </td>
		</tr>
		<tr>
			<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="1" border="0"></td>
		</tr>
    </table>
  </div>
<ffi:removeProperty name="BAIExportSettings"/>
<ffi:removeProperty name="SetBAIExportSettings"/>

