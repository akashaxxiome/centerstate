<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:removeProperty name="RecPayments"/>
<ffi:removeProperty name="Payments"/>
<ffi:removeProperty name="PendingTransfers"/>
<ffi:removeProperty name="RecTransfers"/>

<%-- Save in memory the state of the active checkboxes on the user list page. --%>
<ffi:setProperty name="SetSecondaryUsersActiveState" property="SuccessURL" value=""/>
<ffi:process name="SetSecondaryUsersActiveState"/>


<ffi:object name="com.ffusion.tasks.banking.SignOn" id="UserDeleteBankingSignOn" scope="session"/>
<ffi:setProperty name="UserDeleteBankingSignOn" property="ServiceName" value="UserDeleteBankingService"/>
<ffi:setProperty name="UserDeleteBankingSignOn" property="UserName" value="${TransID}"/>
<ffi:setProperty name="UserDeleteBankingSignOn" property="Password" value="${TransPassword}"/>
<ffi:process name="UserDeleteBankingSignOn"/>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />


<%-- Retrieve collections of pending transactions that may have been created by the user to be deleted. --%>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
	<%-- Initialize tasks and data structures necessary for determining if the user to be deleted has any pending payments. --%>
	<%-- 
	<ffi:object name="com.ffusion.tasks.billpay.Initialize" id="BillPayInitService" scope="request"/>
	<ffi:setProperty name="BillPayInitService" property="ClassName" value="com.ffusion.services.billpay.BillPayServiceImpl"/>
	<ffi:setProperty name="BillPayInitService" property="InitURL" value="billpay.xml"/>
	<ffi:process name="BillPayInitService"/>
	 --%>

	<ffi:object name="com.ffusion.tasks.billpay.SignOn" id="BillPaySignOn" scope="request"/>
	<ffi:setProperty name="BillPaySignOn" property="UserName" value="${TransID}"/>
	<ffi:setProperty name="BillPaySignOn" property="Password" value="${TransPassword}"/>
	<ffi:process name="BillPaySignOn"/>

	<ffi:object name="com.ffusion.tasks.billpay.GetAccounts" id="GetBillPayAccounts" scope="request"/>
	<ffi:setProperty name="GetBillPayAccounts" property="AccountReload" value="true"/>
	<ffi:setProperty name="GetBillPayAccounts" property="UseAccounts" value="BankingAccounts" />
	<ffi:process name="GetBillPayAccounts" />

	<ffi:object name="com.ffusion.tasks.billpay.GetExtPayees" id="GetPayees" scope="request"/>
	<ffi:setProperty name="GetPayees" property="Reload" value="true"/>
	<ffi:process name="GetPayees" />

	<ffi:object name="com.ffusion.tasks.billpay.GetPayments" id="GetPayments" scope="request"/>
	<ffi:setProperty name="GetPayments" property="Reload" value="true"/>
	<ffi:setProperty name="GetPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
	<ffi:process name="GetPayments" />

	<ffi:setProperty name="RecPayments" property="Locale" value="${UserLocale.Locale}" />
	<ffi:setProperty name="Payments" property="Locale" value="${UserLocale.Locale}" />
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
    <ffi:object name="com.ffusion.beans.billpay.RecPayments" id="RecPayments" scope="session"/>
    <ffi:object name="com.ffusion.beans.billpay.Payments" id="Payments" scope="session"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
    <%-- Initialize tasks and data structures necessary for determining if the user to be deleted has any pending transfers. --%>
    <ffi:object name="com.ffusion.tasks.bptw.GetTransfers" id="GetRecTransfers" scope="request"/>
    <ffi:setProperty name="GetRecTransfers" property="Recurring" value="true"/>
    <ffi:setProperty name="GetRecTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:process name="GetRecTransfers"/>

    <ffi:object name="com.ffusion.tasks.bptw.GetTransfers" id="GetTransfers" scope="request"/>
    <ffi:setProperty name="GetTransfers" property="Recurring" value="false"/>
    <ffi:setProperty name="GetTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:process name="GetTransfers"/>

    <ffi:object name="com.ffusion.tasks.banking.UpdateTransferAccountInfo" id="UpdateRecTransfers" scope="request"/>
    <ffi:setProperty name="UpdateRecTransfers" property="AccountsSessionName" value="BankingAccounts"/>
    <ffi:setProperty name="UpdateRecTransfers" property="TransfersSessionName" value="RecTransfers"/>
    <ffi:process name="UpdateRecTransfers"/>

    <ffi:object name="com.ffusion.tasks.banking.UpdateTransferAccountInfo" id="UpdateTransfers" scope="request"/>
    <ffi:setProperty name="UpdateTransfers" property="AccountsSessionName" value="BankingAccounts"/>
    <ffi:setProperty name="UpdateTransfers" property="TransfersSessionName" value="Transfers"/>
    <ffi:process name="UpdateTransfers"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
    <ffi:object name="com.ffusion.beans.banking.RecTransfers" id="RecTransfers" scope="session"/>
    <ffi:object name="com.ffusion.beans.banking.Transfers" id="Transfers" scope="session"/>
</ffi:cinclude>


<ffi:object name="com.ffusion.tasks.user.SetUserInSession" id="SetUserInSession" scope="request"/>
<ffi:setProperty name="SetUserInSession" property="UsersSessionName" value="ModifiedSecondaryUsers"/>
<ffi:setProperty name="SetUserInSession" property="UserSessionName" value="TempSecondaryUser"/>
<ffi:setProperty name="SetUserInSession" property="Id" value="${SecondaryUserID}"/>
<ffi:process name="SetUserInSession"/>

<ffi:object name="com.ffusion.tasks.multiuser.DeleteSecondaryUser" id="FFIDeleteSecondaryUser" scope="session"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="SecondaryUserSessionName" value="TempSecondaryUser"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="SecondaryUserCollectionNames" value="SecondaryUsers,ModifiedSecondaryUsers"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="BankingServiceName" value="UserDeleteBankingService"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="RecPaymentsSessionName" value="RecPayments"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="PaymentsSessionName" value="Payments"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="RecTransfersSessionName" value="RecTransfers"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="TransfersSessionName" value="Transfers"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="PrimaryUserAccountsSessionName" value="BankingAccounts"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="Initialize" value="TRUE"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="Process" value="FALSE"/>
<ffi:process name="FFIDeleteSecondaryUser"/>

<ffi:setProperty name="FFIDeleteSecondaryUser" property="Initialize" value="FALSE"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="Process" value="TRUE"/>
<ffi:setProperty name="FFIDeleteSecondaryUser" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/user_delete_confirm.jsp" URLEncrypt="true"/>

<ffi:setProperty name="AddEditMode" value="Delete"/>

<%-- Set the default action to take if the user being deleted has pending transactions. --%>
<ffi:setProperty name="TransferOrCancelTransactions" value="Transfer"/>

<ffi:removeProperty name="BptwBanking"/>

<ffi:removeProperty name="SecondaryUserID"/>
<ffi:removeProperty name="UserDeleteBankingInitialize"/>
<ffi:removeProperty name="UserDeleteBankingSignOn"/>
