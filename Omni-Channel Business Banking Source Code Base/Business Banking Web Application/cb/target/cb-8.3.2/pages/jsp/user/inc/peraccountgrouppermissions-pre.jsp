<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>


<ffi:cinclude value1="${PermissionsWizard}" value2="TRUE" operator="notEquals">
	<ffi:removeProperty name="ComponentNamesPer" />
</ffi:cinclude>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_REQUIRE_APPROVAL %>"/>
</ffi:cinclude>

<%-- Set the page heading --%>
<ffi:removeProperty name="PageHeading2"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
<%--
<ffi:include page="${PathExt}user/inc/SetPageText.jsp" />
--%>
<ffi:setProperty name="PermissionsWizardSubMenu" value="TRUE"/>

<%-- Initialize Request --%>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>
<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>

<%-- get the entitlementgroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
	<ffi:process name="GetCategories"/>
	
	<%--We need to check accountgroup access from DA for GetAccountGroupsForUser task --%>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="dependentDaEntitlements"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
	<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${ComponentName}"/>
	<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
					value="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>" />
	<ffi:process name="GetDACategorySubType"/>
	<ffi:removeProperty name="GetDACategorySubType"/>
	<%String catSubType = null; %>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${catSubType}" />
	<ffi:removeProperty name="catSubType"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.GetPendingObjectsByTypeSubType" id="getPendingObjects"/>
		<ffi:setProperty name="getPendingObjects" property="objectIdsSessionName" value="daObjectIds"/>
		<ffi:setProperty name="getPendingObjects" property="objectType" value="<%=EntitlementsDefines.ACCOUNT_GROUP%>"/>
		<ffi:setProperty name="getPendingObjects" property="categorySubType" value="${Category_Sub_Session_Name}"/>
	<ffi:process name="getPendingObjects"/>
</ffi:cinclude>

<%-- Get group Accounts --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object id="GetAccountGroupsForUser" name="com.ffusion.tasks.admin.GetAccountGroupsForUser" scope="session"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="GroupID" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="UserType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="MemberID" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="GroupAccountGroupsName" value="MyAccountGroups"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="BusDirectoryId" value="${Business.Id}"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="CheckAdminAccess" value="true"/>
	<ffi:setProperty name="GetAccountGroupsForUser" property="DaCategorySessionName" value="dependentDaEntitlements"/>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles">
		<ffi:setProperty name="GetAccountGroupsForUser" property="UsingEntProfiles" value="true"/>
	</s:if>
	<ffi:process name="GetAccountGroupsForUser"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:object id="GetAccountGroupsForGroup" name="com.ffusion.tasks.admin.GetAccountGroupsForGroup" scope="session"/>
	<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupAccountGroupsName" value="MyAccountGroups"/>
	<ffi:setProperty name="GetAccountGroupsForGroup" property="BusDirectoryId" value="${Business.Id}"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<%-- On the company page, since admin checkboxes are not shown, do not display account 
		groups for which the user has only "Access (admin)" --%>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="CheckAdminAccess" value="false"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:setProperty name="GetAccountGroupsForGroup" property="CheckAdminAccess" value="true"/>
	</ffi:cinclude>
	<ffi:process name="GetAccountGroupsForGroup"/>
</s:if>

<%-- Filter the account groups to only inlude those for which the logged in user has the 'Access (admin)' entitlement. --%>
<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="notEquals">
	<ffi:object id="CheckEntitlementByMemberCB" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMemberCB" scope="session"/>
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="AttributeName" value="sUserHasAdmin" />
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS_ADMIN %>" />
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>" />
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="GroupId" value="${SecureUser.EntitlementID}"/>
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="MemberId" value="${SecureUser.ProfileID}"/>
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="MemberType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_MEMBER_TYPE_USER %>" />
	<ffi:setProperty name="CheckEntitlementByMemberCB" property="MemberSubType" value="${SecureUser.UserType}" />

	<ffi:list collection="MyAccountGroups" items="acctGrp">
		<ffi:setProperty name="CheckEntitlementByMemberCB" property="ObjectId" value="${acctGrp.Id}"/>
		<ffi:process name="CheckEntitlementByMemberCB"/>

		<ffi:setProperty name="acctGrp" property="sUserHasAdmin" value="${sUserHasAdmin}"/>
	</ffi:list>

	<ffi:setProperty name="MyAccountGroups" property="Filter" value="sUserHasAdmin=TRUE" />
	
	<ffi:removeProperty name="CheckEntitlementByMemberCB"/>
	<ffi:removeProperty name="sUserHasAdmin"/>
</ffi:cinclude>

<ffi:object id="SetAccountGroupById" name="com.ffusion.tasks.accountgroups.SetAccountGroupById" scope="session"/>
<ffi:setProperty name="SetAccountGroupById" property="AccountGroupsName" value="MyAccountGroups"/>
<ffi:setProperty name="SetAccountGroupById" property="AccountGroup" value="AccountGrp"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<ffi:cinclude value1="${runSearch}" value2="">

	<ffi:getProperty name="Entitlement_Entitlements" property="Size"/>
		
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">	
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>
				<ffi:setProperty name="EditAccountGroupPermissions"  property="approveAction" value="false" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
			</ffi:cinclude>
	    </s:if>
	    
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
					<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>
					<ffi:setProperty name="EditAccountGroupPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
					<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
				</ffi:cinclude>
				<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountGroupPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountGroupPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountGroupPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditAccountGroupPermissions" property="approveAction" value="false"/>
				</ffi:cinclude>
				
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditAccountGroupPermissions" name="com.ffusion.tasks.admin.EditAccountGroupPermissions" scope="session"/>
			</ffi:cinclude>
			<ffi:setProperty name="EditAccountGroupPermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="EditAccountGroupPermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="EditAccountGroupPermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		</s:if>

		<ffi:setProperty name="EditAccountGroupPermissions" property="AccountGroupId" value="${EditGroup_AccountGroupID}"/>
		<ffi:setProperty name="EditAccountGroupPermissions" property="GroupId" value="${EditGroup_GroupId}"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditAccountGroupPermissions" property="ProfileId" value="${Business.Id}"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditAccountGroupPermissions" property="EntitlementTypesWithLimits" value="AccountEntitlementsWithLimits"/>
		<ffi:setProperty name="EditAccountGroupPermissions" property="EntitlementTypesWithoutLimits" value="AccountEntitlementsWithoutLimits"/>
		<ffi:setProperty name="EditAccountGroupPermissions" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
		<ffi:setProperty name="EditAccountGroupPermissions" property="EntitlementTypePropertyLists" value="LimitsList"/>
		<ffi:setProperty name="EditAccountGroupPermissions" property="SaveLastRequest" value="false"/>

	<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session" />
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		</s:if>
		<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:process name="GetRestrictedEntitlements"/>
	<ffi:removeProperty name="GetRestrictedEntitlements"/>

	<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="ComponentValue" value="${ComponentName}"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
		</ffi:cinclude> 
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
		</s:if>
		<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="AccountMerged" value="AccountEntitlementsMergedBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
	<ffi:process name="GetTypesForEditingLimits"/>
	<ffi:removeProperty name="GetTypesForEditingLimits"/>

</ffi:cinclude>

<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
	<ffi:cinclude value1="${perAccountGroupInit}" value2="true" operator="equals">
		<ffi:list collection="MyAccountGroups" items="AccountGroupItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="EditGroup_AccountGroupID" value="${AccountGroupItem.Id}"/>
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:removeProperty name="perAccountGroupInit"/>
<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<ffi:list collection="MyAccountGroups" items="AccountGroupItem" startIndex="1" endIndex="1">
		<ffi:setProperty name="EditGroup_AccountGroupID" value="${AccountGroupItem.Id}"/>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="notEquals">
	<ffi:cinclude value1="${EditGroup_AccountGroupID}" value2="" operator="notEquals">
		<ffi:setProperty name="SetAccountGroupById" property="Id" value="${EditGroup_AccountGroupID}"/>
		<ffi:process name="SetAccountGroupById"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="CheckOperationEntitlement" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>
<%-- set variables used to interchange row colors --%>

<ffi:setProperty name="numAccountGroups" value="${MyAccountGroups.Size}"/>
<%
	int accountGroupDisplaySize;
	int numAccountGroups = Integer.parseInt( (String)session.getAttribute("numAccountGroups") );
	
	accountGroupDisplaySize = numAccountGroups;
	session.setAttribute( "accountGroupDisplaySize", String.valueOf( accountGroupDisplaySize) );
%>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="notEquals">
			<ffi:setProperty name="SetAccountGroupById" property="Id" value="${EditGroup_AccountGroupID}"/>
			<ffi:process name="SetAccountGroupById"/>

			<%-- filter the entitlements so that those entitlements restricted by the BusinessAdmin entitlement group 
				(as well as its parents) would not be displayed, along with the restricted entitlements' control children --%>
			<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsMergedBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsMerged"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${AccountGrp.Id}"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithoutLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsWithLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsWithoutLimits"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:removeProperty name="FilterEntitlementsForBusiness"/>
			
			<ffi:object id="HandleParentChildDisplay" name="com.ffusion.tasks.admin.HandleParentChildDisplay"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="EntitlementTypePropertyLists" value="AccountEntitlementsMerged"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="PrefixName" value="entitlement"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="AdminPrefixName" value="admin"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="ParentChildName" value="ParentChildLists"/>
			<ffi:setProperty name="HandleParentChildDisplay" property="ChildParentName" value="ChildParentLists"/>
			<ffi:process name="HandleParentChildDisplay"/>

			<ffi:object name="com.ffusion.tasks.admin.GetMaxPerAccountGroupLimits" id="GetMaxPerAccountGroupLimits" scope="session"/>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="EntitlementTypePropertyListsName" value="AccountEntitlementsWithLimits"/>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="AccountGroupId" value="${AccountGrp.Id}"/>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			    <ffi:setProperty name="GetMaxPerAccountGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			    <ffi:setProperty name="GetMaxPerAccountGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			    <ffi:setProperty name="GetMaxPerAccountGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			</s:if>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="PerDayMapName" value="PerDayLimits"/>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="PerWeekMapName" value="PerWeekLimits"/>
			<ffi:setProperty name="GetMaxPerAccountGroupLimits" property="PerMonthMapName" value="PerMonthLimits"/>
			<ffi:process name="GetMaxPerAccountGroupLimits"/>
			<ffi:removeProperty name="GetMaxPerAccountGroupLimits"/>

			<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
			<ffi:process name="GetMaxLimitForPeriod"/>
			<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

			<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
			<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="AccountEntitlementsWithLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="MergedListName" value="AccountEntitlementsMerged"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/peraccountgrouppermissions-verify-init.jsp??PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/peraccountgrouppermissions.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
			<ffi:setProperty name="CheckForRedundantLimits" property="ObjectId" value="${AccountGrp.Id}"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			    <ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
				<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			</s:if>

			<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
				<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
				<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			</s:if>
			<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<ffi:setProperty name="GetGroupLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
			<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${AccountGrp.Id}"/>
			
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${AccountGrp.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="<%=  com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
				<ffi:process name="GetDACategoryDetails" />
				
				<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
				<ffi:process name="GetDACategoryDetails" />
			</ffi:cinclude>
</ffi:cinclude>
			

<script type="text/javascript"><!--

	(function($){
		initializeParentChild();
	})(jQuery);
	
	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].checked = value;
			}
		}
	}
	
	function initializeParentChild() {
		parentChildArray = new Array();
		childParentArray = new Array();
		
		prefixes = new Array( "<ffi:getProperty name="HandleParentChildDisplay" property="PrefixName"/>", "<ffi:getProperty name="HandleParentChildDisplay" property="AdminPrefixName"/>" );
		
<ffi:object id="counterMath" name="com.ffusion.beans.util.IntegerMath"/>		
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:setProperty name="counterMath" property="Value2" value="1"/>

		// Populate the parentChildArray
<ffi:list collection="ParentChildLists" items="childList">
		parentChildArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="childList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="childList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />	
</ffi:list>	

		// Populate the childParentArray
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:list collection="ChildParentLists" items="parentList">
		childParentArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="parentList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="parentList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />	
</ffi:list>	
	}
	
	ns.admin.nextPermissionForPerAcctGrp = function ()
	{
		<ffi:cinclude value1="${ComponentIndex}" value2="${pgcount}" operator="notEquals">
			ns.admin.nextPermission('#<ffi:getProperty name="nextPermission"/>','#perAcctGrpTabs')
		</ffi:cinclude>
		<ffi:cinclude value1="${ComponentIndex}" value2="${pgcount}" operator="equals">
			ns.admin.nextPermission('#<ffi:getProperty name="nextPermission"/>','#permTabs')
		</ffi:cinclude>
		
	}
	
	ns.admin.resetFormForPerAcctGrp = function ()
	{
		$("#companyPermissionsLink").click();
	}
// --></script>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
		 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="/pages/jsp/user/inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- Requires Approval Settings --%>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:removeProperty name="CheckRequiresApproval" />
	<ffi:removeProperty name="EditRequiresApproval"/>	
</ffi:cinclude>
<ffi:removeProperty name="REQUIRES_APPROVAL" />

<%-- Requires Approval Processing for Per Account Permissions --%>
<ffi:removeProperty name="CheckRequiresApprovalPerAcct" />
<ffi:object id="CheckRequiresApprovalPerAcct" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="OpCounter" value="0" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="DispPropListName" value="NonAccountEntitlementsMerged" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="PerAcctRACheck" value="TRUE" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="ModifyUserOnly" value="true" />
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="notEquals">
	<ffi:setProperty name="EditAccountGroupPermissions" property="AccountGroupId" value="${EditGroup_AccountGroupID}"/>
	<ffi:setProperty name="EditAccountGroupPermissions" property="InitOnly" value="false"/>
</ffi:cinclude>

