<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>

<ffi:help id="user_clone-user-permissions" className="moduleHelpClass"/>
<%
	session.setAttribute("FFIClonePermissions", session.getAttribute("ClonePermissions"));
	if(request.getParameter("sortUsers") != null)
		session.setAttribute("sortUsers", request.getParameter("sortUsers"));
	if(request.getParameter("usersSortedBy") != null)
		session.setAttribute("usersSortedBy", request.getParameter("usersSortedBy"));
	if(request.getParameter("profilesSortedBy") != null)
		session.setAttribute("profilesSortedBy", request.getParameter("profilesSortedBy"));
%>

<ffi:cinclude value1="${sortUsers}" value2="true" operator="equals">
	<ffi:setProperty name="BusinessEmployeesForClone" property="sortedBy" value="${usersSortedBy}"/>
	<ffi:setProperty name="EntitlementProfilesForClone" property="sortedBy" value="${profilesSortedBy}"/>
</ffi:cinclude>
<ffi:removeProperty name="sortUsers,usersSortedBy,profilesSortedBy"/>

<ffi:cinclude value1="${SourceBusinessEmployeeId}" value2="" operator="equals">
	<ffi:list collection="BusinessEmployeesForClone" items="employee1" startIndex="1" endIndex="2">
		<ffi:cinclude value1="${SourceBusinessEmployeeId}" value2="" operator="equals">
			<ffi:cinclude value1="${employee1.Id}" value2="${TargetBusinessEmployeeId}" operator="notEquals">
				<ffi:setProperty name="SourceBusinessEmployeeId" value="${employee1.Id}"/>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:list>
	<ffi:cinclude value1="${SourceBusinessEmployeeId}" value2="" operator="equals">
		<ffi:list collection="EntitlementProfilesForClone" items="profile1" startIndex="1" endIndex="1">
			<ffi:setProperty name="SourceBusinessEmployeeId" value="${profile1.Id}"/>
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>

		<div align="center" class="marginTop10">
			<ffi:setProperty name="subMenuSelected" value="users"/>

			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr colspan="5">
					<td>
						<div align="center">
							<span class="columndata"><s:text name="jsp.user_70"/> 
							<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${TargetBusinessEmployee.FullName}"/>
							(<s:text name="jsp.default_576"/>)
							</span>
						</div>
					</td>
				</tr>
			</table>
		<%-- Set full name of target to Context, it would be used on confirm page. --%>
		<ffi:setProperty name="Context" value="${TargetBusinessEmployee.FullName}"/>
		
		<s:form id="SourceUserSelect" namespace="/pages/user" action="clonePermissions-verify" theme="simple" validate="false" method="post" name="SourceUserSelect">
            <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<ffi:setProperty name="reverse${BusinessEmployeesForClone.SortedBy}" value=",REVERSE"/>
					<td colspan="4">
					<div class="paneWrapper">
				  	<div class="paneInnerWrapper">
				  		<div class="header">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" width="33%">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><span class="sectionsubhead"><s:text name="jsp.user_69"/></span></td>
											<td>
											    <ffi:setProperty name="tmpURL" value="/cb/pages/jsp/user/clone-user-permissions.jsp?sortUsers=true&usersSortedBy=NAME${reverseNAME}" URLEncrypt="true"/>
											    <a title='sort' href='#' onClick="ns.admin.sort('<ffi:getProperty name="tmpURL"/>', '#inputDiv')">
													<ffi:cinclude value1="${BusinessEmployeesForClone.SortedBy}" value2="NAME" operator="notEquals" >
														<ffi:cinclude value1="${BusinessEmployeesForClone.SortedBy}" value2="NAME,REVERSE" operator="notEquals" >
															<img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
														</ffi:cinclude>
													</ffi:cinclude>
													<ffi:cinclude value1="${BusinessEmployeesForClone.SortedBy}" value2="NAME" operator="equals" >
														<img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
													</ffi:cinclude>
													<ffi:cinclude value1="${BusinessEmployeesForClone.SortedBy}" value2="NAME,REVERSE" operator="equals" >
														<img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
													</ffi:cinclude>
												</a>
											</td>
										</tr>
									</table>
								</td>
			
								<td align="left" width="33%">
									<table  border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><span class="sectionsubhead"><s:text name="jsp.default_225"/></span></td>								
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>					
								<td align="left" width="33%">
									<span class="sectionsubhead"><s:text name="jsp.user_32"/></span>
								</td>
								<td align="left">&nbsp;</td>
							</tr>
						</table>
						</div>
						<table border="0" width="100%">
							<tr>
								<td height="1" colspan="4"><img src="/cb/web/multilang/grafx/payments/spacer.gif" height="1" width="225" border="0"></td>
							</tr>
			
							<% 
							int band = 1;
							%>
							<ffi:setProperty name="Compare" property="Value1" value="${SourceBusinessEmployeeId}"/>
							
			                <ffi:list collection="BusinessEmployeesForClone" items="employee1">
							<ffi:setProperty name="Compare" property="Value2" value="${employee1.Id}"/>
			                <ffi:cinclude value1="${employee1.Id}" value2="${TargetBusinessEmployeeId}" operator="notEquals">
							<tr <%= band % 2 == 0 ? "class=\"columndata dkrow\"" : "class=\"columndata ltrow3\"" %>" >
								<td align="left" width="33%">
									<input type="radio" name="SourceBusinessEmployeeId" value="<ffi:getProperty name="employee1" property="Id"/>" <ffi:getProperty name="Compare" property="CheckedIfEquals"/> />
									  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${employee1.FullName}"/>
										&nbsp;(<ffi:getProperty name="employee1" property="UserName"/>)
								</td>
								<td align="left" width="33%">
									<ffi:getProperty name="employee1" property="Group"/>
			                    </td>                    
			                    <td align="left" width="33%">
									<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
			                        <ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${employee1.EntitlementGroupMember.Id}"/>
			                        <ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${employee1.EntitlementGroupMember.MemberType}"/>
			                        <ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${employee1.EntitlementGroupMember.MemberSubType}"/>
			                        <ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${employee1.EntitlementGroupId}"/>
			
									<ffi:process name="CanAdministerAnyGroup"/>
									<%
										Object obj = session.getAttribute(com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_CAN_ADMIN_ANY_GROUP);
										String canAdministerAsString = "No";
										
										if (obj != null && obj instanceof String) {
											canAdministerAsString = (String)obj;
											canAdministerAsString = (canAdministerAsString.equalsIgnoreCase("TRUE") ? "Yes" : "No" );
										}
										out.println (canAdministerAsString);
			
			                        %>
								</td>
								
								<td>&nbsp;</td>
								
							</tr>
							<% band++; %>
							</ffi:cinclude>
							</ffi:list>
							<%if(band == 1){%>
							<tr >
								<td colspan="4" align="center">
									<s:text name="jsp.user_209"/>
								</td>
							</tr>
							<%}%>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
						</table>
					</td>
					<ffi:setProperty name="reverse${BusinessEmployeesForClone.SortedBy}" value=""/>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>                                
					<ffi:setProperty name="reverse${EntitlementProfilesForClone.SortedBy}" value=",REVERSE"/>
					<td colspan="4">
					<div class="paneWrapper">
				  	<div class="paneInnerWrapper">
				  		<div class="header">
				  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<td align="left" width="33%">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><span class="sectionsubhead"><s:text name="jsp.user_68"/></span></td>
										<td>
										    <ffi:setProperty name="tmpURL" value="/cb/pages/jsp/user/clone-user-permissions.jsp?sortUsers=true&profilesSortedBy=USERNAME${reverseUSERNAME}"/>
										    <a title='sort' href='#' onClick="ns.admin.sort('<ffi:getProperty name="tmpURL"/>', '#inputDiv')">
												<ffi:cinclude value1="${EntitlementProfilesForClone.SortedBy}" value2="USERNAME" operator="notEquals" >
													<ffi:cinclude value1="${EntitlementProfilesForClone.SortedBy}" value2="USERNAME,REVERSE" operator="notEquals" >
														<img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
													</ffi:cinclude>
												</ffi:cinclude>
												<ffi:cinclude value1="${EntitlementProfilesForClone.SortedBy}" value2="USERNAME" operator="equals" >
													<img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
												</ffi:cinclude>
												<ffi:cinclude value1="${EntitlementProfilesForClone.SortedBy}" value2="USERNAME,REVERSE" operator="equals" >
													<img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
												</ffi:cinclude>
											</a>
										</td>
									</tr>
								</table>
							</td>
							<td align="left" width="33%">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><span class="sectionsubhead"><s:text name="jsp.default_225"/></span></td>								
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>					
							<td align="left" width="33%" >&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					</table>
					</div>
						<ffi:setProperty name="reverse${EntitlementProfilesForClone.SortedBy}" value=""/>
						
						<table border="0" width="100%">
							<tr>
								<td height="1" colspan="4">&nbsp;</td>
							</tr>
			
							<% 
							int band2 = 1;
							%>
							<ffi:setProperty name="Compare" property="Value1" value="${SourceBusinessEmployeeId}"/>
							
			                <ffi:list collection="EntitlementProfilesForClone" items="profile1">
							<ffi:setProperty name="Compare" property="Value2" value="${profile1.Id}"/>
							<tr <%= band2 % 2 == 0 ? "class=\"columndata dkrow\"" : "class=\"columndata ltrow3\"" %>" >
								<td align="left" width="33%">
									<input type="radio" name="SourceBusinessEmployeeId" value="<ffi:getProperty name="profile1" property="Id"/>" <ffi:getProperty name="Compare" property="CheckedIfEquals"/> />
									<ffi:getProperty name="profile1" property="UserName"/>
								</td>
								<td align="left" width="33%">
									<ffi:getProperty name="profile1" property="Group"/>
			                    </td>                    
								
								<td width="33%">&nbsp;</td>
								<td>&nbsp;</td>
								
							</tr>
							<% band2++; %>
							</ffi:list>
							<%if(band2 == 1){%>
							<tr>
								<td colspan="4" align="center">
									<s:text name="jsp.user_210"/>
								</td>
							</tr>
							<%}%>
							<tr >
								<td colspan="4" >
								&nbsp;
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
				<div align="center">
					<ffi:cinclude value1="${AddUser}" value2="TRUE" operator="equals">
						<sj:a
							button="true"
							summaryDivId="summary" 
							buttonType="cancel"
							onClickTopics="showSummary,cancelAddUserCloneForm,refreshUserGrid"
							><s:text name="jsp.default_82"/></sj:a>
					</ffi:cinclude>
					<ffi:cinclude value1="${AddUser}" value2="TRUE" operator="notEquals">
						<sj:a
						    id="cloneUserPermissionCancel"
							button="true"
							onClickTopics="completeCloneUserPermLoad"
							><s:text name="jsp.default_82"/></sj:a>
					</ffi:cinclude>
					<%if((band > 1)||(band2 > 1)){%>
					<sj:a
					        id="cloneUserPermissionClone"
						formIds="SourceUserSelect"
						targets="verifyDiv"
						button="true"
						validate="true"
						validateFunction="customValidation"
						onBeforeTopics="beforeVerify"
						onCompleteTopics="completeVerify"
						><s:text name="jsp.user_66"/></sj:a>
					<%}%>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>		
</s:form>
</div>
<ffi:removeProperty name="tmpURL"/>

