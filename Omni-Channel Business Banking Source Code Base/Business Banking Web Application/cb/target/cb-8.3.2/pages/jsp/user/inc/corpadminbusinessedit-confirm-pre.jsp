<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>


<%-- The resource filenames and names are set in corpadminbusinessedit.jsp --%>
<ffi:setProperty name="ModifyBusiness" property="StateValidationResourceFile" value="${State_ResourceFilename}"/>
<ffi:setProperty name="ModifyBusiness" property="StateValidationResourceName" value="${State_ResourceID}"/>
<ffi:setProperty name="ModifyBusiness" property="CountryValidationResourceFile" value="${Country_ResourceFilename}"/>
<ffi:setProperty name="ModifyBusiness" property="CountryValidationResourceName" value="${Country_ResourceID}"/>


<ffi:object id="IsRegisteredCountry" name="com.ffusion.tasks.util.IsRegisteredCountry" scope="session" />
    <ffi:setProperty name="IsRegisteredCountry" property="CountryCode" value="${ModifyBusiness.Country}" />
<ffi:process name="IsRegisteredCountry" />			

<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
	<ffi:setProperty name="ModifyBusiness" property="Validate" value="STREET,CITY,STATE,ZIPCODE,COUNTRY,PHONE" />
	<ffi:setProperty name="ModifyBusiness" property="VerifyFormat" value="STREET,CITY,STATE,ZIPCODE,COUNTRY,PHONE,FAXPHONE,EMAIL" />
</ffi:cinclude>
<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">  
	<ffi:cinclude value1="${IsRegisteredCountry.IsCountryRegistered}" value2="true" operator="equals">
		<ffi:setProperty name="ModifyBusiness" property="Validate" value="STREET,CITY,ZIPCODE,COUNTRY,PHONE" />
		<ffi:setProperty name="ModifyBusiness" property="VerifyFormat" value="STREET,CITY,ZIPCODE,COUNTRY,PHONE,FAXPHONE,EMAIL" />
	</ffi:cinclude>
	<ffi:cinclude value1="${IsRegisteredCountry.IsCountryRegistered}" value2="true" operator="notEquals">
		<ffi:setProperty name="ModifyBusiness" property="Validate" value="STREET,CITY,COUNTRY,PHONE" />
		<ffi:setProperty name="ModifyBusiness" property="VerifyFormat" value="STREET,CITY,ZIPCODE,COUNTRY,PHONE,FAXPHONE,EMAIL" />
	</ffi:cinclude>
</ffi:cinclude>

<!-- Dual Approval -->

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="ModifyBusiness" property="Process" value="false" />
	<ffi:process name="ModifyBusiness"/>
	<ffi:object id="AddBusinessToDA" name="com.ffusion.tasks.dualapproval.AddBusinessToDA" />
		<ffi:setProperty name="AddBusinessToDA" property="Category" value="Profile"/>
	<ffi:process name="AddBusinessToDA" />
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals" >
	<ffi:setProperty name="ModifyBusiness" property="Process" value="true" />
	<ffi:process name="ModifyBusiness"/>
</ffi:cinclude>



<!-- dual approval ends -->

<ffi:removeProperty name="IsRegisteredCountry"/>
<ffi:removeProperty name="State_ResourceFilename"/>
<ffi:removeProperty name="State_ResourceID"/>
<ffi:removeProperty name="Country_List"/>
<ffi:removeProperty name="Country_ResourceFilename"/>
<ffi:removeProperty name="Country_ResourceID"/>
<ffi:removeProperty name="ModifyBusiness"/>
<ffi:removeProperty name="EditBusiness"/>

<ffi:setProperty name="Business" property="Street" value="${ModifyBusiness.Street}"/>
<ffi:setProperty name="Business" property="Street2" value="${ModifyBusiness.Street2}"/>
<ffi:setProperty name="Business" property="City" value="${ModifyBusiness.City}"/>
<ffi:setProperty name="Business" property="State" value="${ModifyBusiness.State}"/>
<ffi:setProperty name="Business" property="ZipCode" value="${ModifyBusiness.ZipCode}"/>
<ffi:setProperty name="Business" property="Country" value="${ModifyBusiness.Country}"/>
<ffi:setProperty name="Business" property="Phone" value="${ModifyBusiness.Phone}"/>
<ffi:setProperty name="Business" property="FaxPhone" value="${ModifyBusiness.FaxPhone}"/>
<ffi:setProperty name="Business" property="Email" value="${ModifyBusiness.Email}"/>