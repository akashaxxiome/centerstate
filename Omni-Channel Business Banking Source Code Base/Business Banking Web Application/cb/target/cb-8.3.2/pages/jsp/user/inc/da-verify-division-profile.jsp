<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:cinclude value1="${isProfileChanged}" value2="Y">

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>" />
	<ffi:process name="GetDACategoryDetails" />
		
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.da_approvals_pending_user_FieldName_column" /></td>
							<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.da_approvals_pending_user_OldValue_column" /></td>
							<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.da_approvals_pending_user_NewValue_column" /></td>
						</tr>
					</table>
				</div>
				<div class="paneContentWrapper">
					<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tableData">
						<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
							<ffi:cinclude value1="${details.fieldName}" value2="groupName">
								<tr>
									<td width="33%" class="columndata"><ffi:getL10NString rsrcFile="cb"
										msgKey="da.field.division.${details.fieldName}" /></td>
									<td width="33%" class="columndata">
										<ffi:getProperty name="details" property="oldValue" />
									</td>
									<td width="33%" class="columndata">
										<span class="sectionheadDA"><ffi:getProperty name="details" property="newValue" /></span>
									</td>
								</tr>
							</ffi:cinclude>
						</ffi:list>
					</table>
				</div>
			</div>
		</div>
</ffi:cinclude>	
	
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
