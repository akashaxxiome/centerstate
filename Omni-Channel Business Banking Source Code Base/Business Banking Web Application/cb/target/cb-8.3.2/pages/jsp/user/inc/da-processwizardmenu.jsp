<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="SetEntitlementTypeOrder" name="com.ffusion.tasks.admin.SetEntitlementTypeOrder" scope="session"/>
<ffi:removeProperty name="entitlementTypeOrder"/>
<ffi:cinclude value1="${isProfileChanged}" value2="Y" operator="equals">
	<s:if test="%{#session.Section == 'Users'}">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-user-profile.jsp?Section=${Section}" URLEncrypt="true" />
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-profile.jsp?Section=${Section}" URLEncrypt="true" />
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-business-profile.jsp?Section=${Section}" URLEncrypt="true" />
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Division">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-division-profile.jsp?Section=${Section}" URLEncrypt="true" />
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Group">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-group-profile.jsp?Section=${Section}" URLEncrypt="true" />
	</ffi:cinclude>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="profile"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Profile"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
	<ffi:process name="SetEntitlementTypeOrder"/>
</ffi:cinclude>

<ffi:cinclude value1="${isPermissionChanged}" value2="Y" operator="equals">
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-accountaccess.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="accountaccess"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Account Access"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isAccountAccessChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-accountgroupaccess.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="accountgroupaccess"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Account Group Access"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isAccountGroupAccessChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-crossachcompanyaccess.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="crossachcompanyaccess"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Cross ACH Company Access"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isCrossACHAccessChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-perachcompanyaccess.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="perachcompanyaccess"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Per ACH Company Access"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isPerACHAccessChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
	
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-crossaccount.jsp?Section=${Section}"  URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="crossaccount"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Cross Account Access and Limits"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isCrossAccountChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
	
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-peraccount.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="peraccount"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Per Account Access and Limits"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isPerAccountChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	

	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-peraccountgroup.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="peraccountgroup"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Per Account Group Access and Limits"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isPerAccountGroupChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
	
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-perlocation.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="perlocation"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Per Location Access and Limits"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isPerLocationChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
	
		
	<ffi:setProperty name="SetEntitlementTypeOrder" property="MenuUrl" value="da-summary-wiretemplatesaccess.jsp?Section=${Section}" URLEncrypt="true" />
	<ffi:setProperty name="SetEntitlementTypeOrder" property="PageKey" value="wiretemplateaccess"/>
	<ffi:setL10NProperty name="SetEntitlementTypeOrder" property="menuDisplayName" value="Wire Template Access and Limits"/>
	<ffi:setProperty name="SetEntitlementTypeOrder" property="ParentMenu" value="top"/>
	<ffi:cinclude value1="${isWireTemplateChanged}" value2="Y" operator="equals">
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableMenu" value="true"/>
		<ffi:setProperty name="SetEntitlementTypeOrder" property="EnableLink" value="false"/>
		<ffi:process name="SetEntitlementTypeOrder"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:list collection="entitlementTypeOrder" items="PermissionTypeAdmin">
	<ffi:cinclude value1="${firstMenuShowed}" value2="" >
		<ffi:cinclude value1="${PermissionTypeAdmin.EnableMenu}" value2="true" >
		 <ffi:setProperty name="firstMenuShowed" value="true" />
			<script>

				$.ajax({
						url: '/cb/pages/jsp/user/<ffi:urlEncrypt url="${PermissionTypeAdmin.MenuUrl}&ParentMenu=${PermissionTypeAdmin.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${PermissionTypeAdmin.PageKey}&SortKey=${PermissionTypeAdmin.NextSortKey}"/>',
						success: function(data)
						{
							$('#approvalWizardDialogID').html(data).dialog('open');
						}
					});

			</script>
		</ffi:cinclude>	
	</ffi:cinclude>
</ffi:list>
<ffi:removeProperty name="firstMenuShowed"/>
