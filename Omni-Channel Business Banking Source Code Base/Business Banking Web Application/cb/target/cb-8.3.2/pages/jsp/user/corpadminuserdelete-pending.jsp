<%@ page import="com.ffusion.beans.ach.ACHBatch,
				 com.ffusion.beans.ach.ACHBatchTemplate,
                 com.ffusion.beans.wiretransfers.WireHistory,
                 com.ffusion.beans.wiretransfers.WireTransfer,
                 com.ffusion.beans.banking.Transfer,
                 com.ffusion.beans.banking.TransferDefines,
                 com.ffusion.beans.billpay.Payment,
                 com.ffusion.beans.ach.ACHBatches"%>
<%@ page import="com.ffusion.beans.ach.ACHStatus" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="com.ffusion.beans.cashcon.CashCon" %>
<%@ page import="com.ffusion.beans.cashcon.CashConStatus" %>
<%@ page import="com.ffusion.beans.ach.ACHDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminuserdelete-pending" className="moduleHelpClass"/>
<div id="scrollableDiv" class="approvalDialogHt" >
<s:set var="tmpI18nStr" value="%{getText('jsp.user_371')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<%-- <TR>
			<TD class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuserdelete-verify.jsp-1" parm0="${BusinessEmployee.FullName}"/></TD>
			<td></td>
		</TR> --%>
		<TR>
			<TD class="sectionsubhead">
			<div align="center">
				<s:text name="jsp.user_236"/>
			</div>
			</TD>
		</TR>
		<TR>
			<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
			<td></td>
		</TR>
        <TR>
		<TD class="columndata">
           <div class="paneWrapper">
			<div class="paneInnerWrapper">
			 <div class="header">
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sectionsubhead">
                      <td width="35%"><s:text name="jsp.default_436"/></td>
                      <td width="15%"><s:text name="jsp.default_137"/></td>
                      <td width="15%"><s:text name="jsp.default_43"/></td>
                      <td width="25%"><s:text name="jsp.default_435"/></td>
                      <td width="10%"><s:text name="jsp.default_388"/></td>
                  </tr>
                  </table>
               </div>
               <div class="paneContentWrapper">
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <% boolean canDelete = true; %>
                    <ffi:list collection="PendingTransactions" items="trans">
                    <% String dispClass = "columndata"; %>
                                <tr>
                        <% if (request.getAttribute("trans") instanceof ACHBatch) {
                                ACHBatch batch = (ACHBatch)request.getAttribute("trans");
                                String achType = batch.getACHType();
                                String name = batch.getName();
                                boolean recurring = false;
                                boolean isTemplate = false;
                                if ("ACH_BATCH_TEMPLATE".equals(batch.getCategory()) ||
                                	"ACH_MULTIBATCH_TEMPLATE".equals(batch.getCategory()) )
                                {
                                    isTemplate = true;
                                }
                                if ("RECACH".equals(batch.getTransactionType()))
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                    recurring = true;
                                }
                                String achTypeString =(!recurring)?com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_23", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()):com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_345", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                if (ACHDefines.ACH_TYPE_TAX.equals(achType))
                                    achTypeString =(!recurring)?com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_406", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()):com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_273", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                if (ACHDefines.ACH_TYPE_CHILD.equals(achType))
                                    achTypeString =(!recurring)?com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_97", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()):com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_271", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                
                                else
                                if (isTemplate)
                                {
                                	name = batch.getTemplateName();
                                    achTypeString =(!recurring)?com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.1", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()):com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.2", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                    if (ACHDefines.ACH_TYPE_TAX.equals(achType))
                                        achTypeString =(!recurring)?com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.3", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()):com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.4", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                    if (ACHDefines.ACH_TYPE_CHILD.equals(achType))
                                        achTypeString =(!recurring)?com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.5", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()):com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.6", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                    if ("ACH_MULTIBATCH_TEMPLATE".equals(batch.getCategory()))
                                    {
                                        achTypeString = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user.corpadminuserdelete.7", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
                                    }                                            
                                }
                                if (batch.getStatusValue() == ACHStatus.ACH_APPROVAL_PENDING)
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                }
                                %>
                                <td width="35%" class="<%=dispClass%>"><%=achTypeString%><br>
                                <%= name %></td>
                                <td width="15%" class="<%=dispClass%>"><%=batch.getDate()%></td>
                                <td width="15%" class="<%=dispClass%>"><%=batch.getAmount()%></td>
                                <td width="25%" class="<%=dispClass%>"><%=batch.getTrackingID()%></td>
                                <td width="10%" class="<%=dispClass%>"><%=batch.getStatus()%></td>
                        <% } %>
                        
                        <% if (request.getAttribute("trans") instanceof CashCon) {
                                CashCon batch = (CashCon)request.getAttribute("trans");
                                if (batch.getStatus() == CashConStatus.CASH_CON_STATUS_PENDING_APPROVAL)
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                } %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.default_85"/><br>
                                        <%=batch.getDivisionName()+"/"+batch.getLocationName()+"/"+batch.getTypeString()%>
                                    </td>
                                <td width="15%" class="<%=dispClass%>"><%=batch.getSubmitDate()%></td>
                                <td width="15%" class="<%=dispClass%>"><%=batch.getAmount()%></td>
                                <td width="25%" class="<%=dispClass%>"><%=batch.getTrackingID()%></td>
                                <td width="10%" class="<%=dispClass%>"><%=batch.getStatusName()%></td>
                        <% } %>

                        <% if (request.getAttribute("trans") instanceof WireHistory) { %>
                                <% WireHistory wire = (WireHistory)request.getAttribute("trans");
                                if (wire.getStatus() == WireDefines.WIRE_STATUS_PENDING_APPROVAL)
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                }
                                if ("RECWIRE".equals(wire.getTransType())) {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.user_274"/>
                                    </td>
                                <% } else { %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.default_465"/>
                                    </td>
                                <% } %>
                                <td width="15%" class="<%=dispClass%>"><%=wire.getDate()%></td>
                                <td width="15%" class="<%=dispClass%>"><%=wire.getAmount()%></td>
                                <td width="25%" class="<%=dispClass%>"><%=wire.getTrackingID()%></td>
                                <td width="10%" class="<%=dispClass%>"><%=wire.getStatusName()%></td>
                        <% } %>
                         <% if (request.getAttribute("trans") instanceof WireTransfer) { %>
                                <% WireTransfer wire = (WireTransfer)request.getAttribute("trans");
                                if (wire.getStatus() == WireDefines.WIRE_STATUS_PENDING_APPROVAL)
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                }
                               
                                %>
                               <td width="35%" class="<%=dispClass%>"><s:text name="jsp.user_414"/>
                               </td>
                                
                                <td width="15%" class="<%=dispClass%>"><%=wire.getDateToPost()%></td>
                                <td width="15%" class="<%=dispClass%>"><%=wire.getAmount()%></td>
                                <td width="25%" class="<%=dispClass%>"><%=wire.getTrackingID()%></td>
                                <td width="10%" class="<%=dispClass%>"><%=wire.getStatusName()%></td>
                        <% } %>
                        <% if (request.getAttribute("trans") instanceof Transfer) { %>
                                <% Transfer transfer = (Transfer)request.getAttribute("trans");
                                if (transfer.getStatus() == Transfer.TRS_PENDING_APPROVAL)
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                }
                                if ("Recmodel".equals(transfer.getTransferType())) {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.default_346"/>
                                    </td>
                                <% } else if(TransferDefines.TRANSFER_TYPE_TEMPLATE.equals(transfer.getTransferType()) || TransferDefines.TRANSFER_TYPE_RECTEMPLATE.equals(transfer.getTransferType())) { 
                                %>    
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.transfers_84"/>
                                    </td>
                                <% } else { %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.default_441"/>
                                    </td>
                                <% } %>
                                <td width="15%" class="<%=dispClass%>"><%=transfer.getDate()%></td>
                                <td width="15%" class="<%=dispClass%>"><%=transfer.getAmount()%></td>
                                <td width="25%" class="<%=dispClass%>"><%=transfer.getTrackingID()%></td>
                                <td width="10%" class="<%=dispClass%>"><%=transfer.getStatusName()%></td>
                        <% } %>
                        <% if (request.getAttribute("trans") instanceof Payment) { %>
                                <% Payment payment = (Payment)request.getAttribute("trans");
                                if (payment.getStatus() == Payment.PMS_PENDING_APPROVAL)
                                {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                }
                                if ("RECPMT".equals(payment.getTransactionType())) {
                                    canDelete = false;
                                    dispClass = "columndata_bold columndata_error";
                                %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.user_272"/>
                                    <%= payment.getPayeeName()%></td>
                                <% } else { %>
                                    <td width="35%" class="<%=dispClass%>"><s:text name="jsp.default_317"/>
                                    <%= payment.getPayeeName()%></td>
                                <% } %>

                                <td width="15%" class="<%=dispClass%>"><%=payment.getPayDate()%></td>
                                <td width="15%" class="<%=dispClass%>"><%=payment.getAmount()%></td>
                                <td width="25%" class="<%=dispClass%>"><%=payment.getTrackingID()%></td>
                                <td width="10%" class="<%=dispClass%>"><%=payment.getStatusName()%></td>
                        <% } %>
                                </tr>
                    </ffi:list>
		       	  </table>
		         </div>
		       </div>
		     </div>  
		   </TD>
	    </TR>
		<TR>
			<TD class="columndata" height="30">
				<s:text name="jsp.user_171"/>
			</TD>
		</TR>
		<TR>
			<TD class="columndata" height="30">
               <s:text name="jsp.user_170"/>
			</TD>
		</TR>
     <% if (!canDelete) { %>
         <TR>
             <TD class="columndata" height="30">
            	 <span class="errorcolor"><s:text name="jsp.user_330"/></span>
             </TD>
         </TR>
     <% } %>
		<TR>
			<TD height="50">&nbsp;</TD>
		</TR>
		<TR>
			<TD>
			<div align="center" class="ui-widget-header customDialogFooter">
				<ffi:cinclude value1="${approveDelete}" value2="true" operator="notEquals">
				<sj:a id="cancelDeleteUserWithPendingLink" button="true" onClickTopics="closeDeleteUserDialog"
					  title="%{getText('jsp.default_83')}"
					  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
				</ffi:cinclude>
				<ffi:cinclude value1="${approveDelete}" value2="true">
				<sj:a id="cancelDeleteUserWithPendingLink1" button="true" onClickTopics="closeApprovalWizardDialog"
					  title="%{getText('jsp.default_83')}" 
					  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
				</ffi:cinclude>
				 <% if (canDelete) {%>
				<ffi:cinclude value1="${approveDelete}" value2="true" operator="notEquals">
				<s:url id="deleteUserUrl" value="/pages/user/deleteBusinessUser_execute.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="REALLY_DELETE" value="true"></s:param>
				</s:url>
				<sj:a id="deleteUserLink" href="%{deleteUserUrl}"
					  targets="wholeworld"
					  button="true"
					  title="%{getText('jsp.user_400')}"
					  effectDuration="1500"
					  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.user_399" /></sj:a>
				</ffi:cinclude>	 
				<ffi:cinclude value1="${approveDelete}" value2="true">
				<s:url id="deleteUserUrl1" value="/pages/jsp/user/corpadminuserdelete-confirm.jsp?approveDelete=true&itemId=${itemId}">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="REALLY_DELETE" value="true"></s:param>
				</s:url>
				<sj:a id="deleteUserLink1" href="%{deleteUserUrl1}"
					  targets="approvalWizardDialogID"
					  button="true"
					  title="%{getText('jsp.user_400')}"
					  effectDuration="1500"
					  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.user_399" /></sj:a>
				</ffi:cinclude>	  
				
				<% } %>
				<ffi:removeProperty name="approveDelete"/>
			</div>
			</TD>
		</TR>
	</TABLE>
</div>		
