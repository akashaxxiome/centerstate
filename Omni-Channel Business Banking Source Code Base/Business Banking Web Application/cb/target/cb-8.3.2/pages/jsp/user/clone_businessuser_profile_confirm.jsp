<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminprofileedit-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_84')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	<div align="center" class="marginTop20">
			<div class="marginBottom10">
				Profile has been successfully cloned.
			</div>
		    <div class="btn-row">
		    	<sj:a button="true" onClickTopics="cancelProfileForm,refreshProfileGrid"><s:text name="jsp.default_175"/></sj:a>
			</div>
	</div>
<ffi:removeProperty name="TempBusEmployee"/>
<ffi:removeProperty name="NewBusinessEmployee"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="GroupAdminEmployees"/>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminprofiles-pre.jsp"/>
