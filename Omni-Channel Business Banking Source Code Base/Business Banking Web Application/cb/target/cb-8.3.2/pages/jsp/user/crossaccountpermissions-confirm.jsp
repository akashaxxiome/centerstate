<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_crossaccountpermissions-confirm" className="moduleHelpClass"/>
<ffi:removeProperty name="doAutoEntitle"/>
<ffi:removeProperty name="GetCumulativeSettings"/>

<%-- DA mode processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="equals">
	<%-- Process tree if flag is set --%>
	<ffi:cinclude value1="${PROCESS_TREE_FLAG}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditCrossAccountPermissions" property="AutoEntitle" value="true"/>
		<ffi:setProperty name="EditCrossAccountPermissions" property="InitOnly" value="false"/>
		<ffi:process name="EditCrossAccountPermissions"/>
		<ffi:process name="ProcessEntitlementTree" />
		<ffi:removeProperty name="ProcessEntitlementTree" />
	</ffi:cinclude>
	<%-- Entitlement tree is not displayed. --%>
	<ffi:cinclude value1="${PROCESS_TREE_FLAG}" value2="TRUE" operator="notEquals">
		<ffi:setProperty name="EditCrossAccountPermissions" property="AutoEntitle" value="false"/>
		<ffi:setProperty name="EditCrossAccountPermissions" property="InitOnly" value="false"/>
		<ffi:process name="EditCrossAccountPermissions"/>
	</ffi:cinclude>	
	<ffi:removeProperty name="DA_GRANT_ENTITLEMENTS" />
	<ffi:removeProperty name="DA_REVOKE_ENTITLEMENTS" />	
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:removeProperty name="ENTITLEMENT_TREE_GROUPS" />
	<ffi:removeProperty name="ENTITLEMENT_TREE_MEMBERS_MAP" />
	<ffi:removeProperty name="RootGroupId" />
</ffi:cinclude>

<%-- Requires Approval Processing --%>
<ffi:setProperty name="EditRequiresApproval" property="ExecutePhase1" value="false" />
<ffi:setProperty name="EditRequiresApproval" property="ExecutePhase2" value="true" />
<ffi:process name="EditRequiresApproval" />
<ffi:removeProperty name="EditRequiresApproval" />
<ffi:removeProperty name="CheckRequiresApproval" />
<ffi:removeProperty name="EditCrossAccountPermissions"/>
<%-- END Requires Approval Processing --%>

<ffi:setProperty name="PermissionsWizardSubMenu" value="TRUE"/>
<%--
<ffi:include page="${PathExt}user/inc/permissionswizard-pageorder.jsp"/> 
--%>

		<div align="center">
				<TABLE class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
					<TR>
						<TD>
							<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
								<TR>
									<TD class="columndata" align="center"><s:text name="jsp.user_58"/></TD>
								</TR>
								<TR>
									<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="5"></TD>
								</TR>
								<TR>
									<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
										<TD align="center">
										<sj:a button="true" buttonIcon="ui-icon-check" onClickTopics="cancelPermForm"><s:text name="jsp.default_175"/></sj:a>
										</TD>
									</ffi:cinclude>
									<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
										<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
											<TD align="center">
											<sj:a button="true" buttonIcon="ui-icon-check" onClickTopics="cancelPermForm"><s:text name="jsp.default_175"/></sj:a>
											</TD>
										</ffi:cinclude>										
										<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="notEquals">
											<ffi:setProperty name="BackURL" value="${SecurePath}user/crossaccountpermissions.jsp?UseLastRequest=TRUE&PermissionsWizard=TRUE" URLEncrypt="true" />
											<TD align="center"><ffi:include page="${PathExt}user/inc/permissionswizard-continue.jsp"/></TD>	 
										</ffi:cinclude>
										
									</ffi:cinclude>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			<p></p>
		</div>
	</BODY>
</HTML>
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckOperationEntitlement"/>
<ffi:removeProperty name="AccountEntitlementsWithLimits"/>
<ffi:removeProperty name="AccountEntitlementsWithoutLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsWithLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsWithoutLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsMerged"/>
<ffi:removeProperty name="AccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:removeProperty name="AccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:removeProperty name="NonAccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:removeProperty name="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:removeProperty name="NonAccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/crossaccountpermissions-verify-init.jsp?UseLastRequest=TRUE&PermissionsWizard=${PermissionsWizard}&ParentMenu=${ParentMenu}" URLEncrypt="true" />
<ffi:setProperty name="confirmation_done" value="true"/> 
<s:include value="/pages/jsp/inc/checkbox-limit-cleanup.jsp"/>
<ffi:removeProperty name="PermissionsWizardSubMenu"/>

<ffi:removeProperty name="action"/>
