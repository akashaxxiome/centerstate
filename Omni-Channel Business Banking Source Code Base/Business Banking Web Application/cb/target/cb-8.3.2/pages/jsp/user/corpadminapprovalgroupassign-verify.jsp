<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminapprovalgroupassign" className="moduleHelpClass"/>

<%
	com.ffusion.beans.approvals.ApprovalsGroup apprGroup;
	com.ffusion.beans.approvals.ApprovalsGroups apprGroups = (com.ffusion.beans.approvals.ApprovalsGroups) session.getAttribute(com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_GROUPS);
	if (apprGroups != null)
		apprGroups.setSortedBy("APPROVALGROUPNAME");
%>

<span id="PageHeading" style="display:none;"><s:text name="jsp.user_47"/></span>

<div align="center" class="marginTop20">
	<div align="center">
		<span class="sectionsubhead">
			<s:text name="jsp.user_appr_assign_verify">
				<s:param><%= com.ffusion.util.HTMLUtil.encode((String)session.getAttribute("Context"))%></s:param>
			</s:text>
			<br><br>
		</span>
	</div>
	<div><ul id="formerrors"></ul></div>
				<s:form id="assignApprovalGroupsVerifyID" namespace="/pages/user" action="assignApprovalGroups-execute" theme="simple" name="FormName" method="post">
					<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<div align="left" class="marginTop10">
							<% String checkBoxName;
								Integer tempGroupID;
								for (int a = 0; a < apprGroups.size(); a++) {
									apprGroup = (com.ffusion.beans.approvals.ApprovalsGroup) apprGroups.get(a);
									checkBoxName = "apprGroup" + apprGroup.getApprovalsGroupID();
									tempGroupID = new Integer(apprGroup.getApprovalsGroupID()); %>
									<%-- <tr>
										<td class="adminBackground">
											<img src="/cb/web/multilang/grafx/user/spacer.gif" border="0"></td>
										<td class="adminBackground" valign="top">
											<ffi:setProperty name="ApprovalsGroupName" value="<%=apprGroup.getGroupName()%>"/>
											<input disabled type="checkbox" <% if ( request.getParameter(checkBoxName) != null) { %> checked <% } %> border="0"><span class="sectionsubhead">&nbsp;<ffi:getProperty name="ApprovalsGroupName"/></span>
											<% if ( request.getParameter(checkBoxName) != null) {
											%>
												<input type="hidden" name="<%=checkBoxName%>" value="on"/>
											<% } %>
										</td>
										<td class="adminBackground" align="right" width="150"></td>
									</tr> --%>
									<div class="approvalCheckDiv">
										<ffi:setProperty name="ApprovalsGroupName" value="<%=apprGroup.getGroupName()%>"/>
										<input disabled type="checkbox" <% if ( request.getParameter(checkBoxName) != null) { %> checked <% } %> border="0"><span class="sectionsubhead">&nbsp;<ffi:getProperty name="ApprovalsGroupName"/></span>
										<% if ( request.getParameter(checkBoxName) != null) {
										%>
											<input type="hidden" name="<%=checkBoxName%>" value="on"/>
										<% } %>
									</div>
										
								<% } %>
							<div class="btn-row">
								<sj:a id="cancelBtnV"
									  button="true" 
									  onClickTopics="cancelUserForm"><s:text name="jsp.default_82"/></sj:a>
								<sj:a id="backBtnV"
										button="true"
										onClickTopics="backToInput"
								><s:text name="jsp.default_57"/></sj:a>
								<sj:a id="addSubmitV"
									  formIds="assignApprovalGroupsVerifyID"
									  targets="confirmDiv"
									  button="true"
									  validate="true"
									  validateFunction="customValidation"
									  onBeforeTopics="beforeSubmit"
									  onCompleteTopics="completeSubmit"
									  onErrorTopics="errorSubmit"
										><s:text name="jsp.user_2"/></sj:a>
							</div>
					</div>
				</s:form>
</div>
