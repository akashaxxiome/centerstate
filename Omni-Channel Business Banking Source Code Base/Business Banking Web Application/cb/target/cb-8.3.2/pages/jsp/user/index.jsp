<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<ffi:help id="user_index" className="moduleHelpClass"/>
<s:include value="inc/index-pre.jsp"/>


<script type="text/javascript" src="<s:url value='/web/js/user/admin%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/user/admin_grid%{#session.minVersion}.js'/>"></script>

<div id="desktop">

    <div id="operationresult">
        <div id="resultmessage"><s:text name="jsp.default_498"/></div>
    </div>

	<div id="appdashboard">
		<s:include value="admin_dashboard.jsp"/>
	</div>

	<div id="details">
		<s:include value="admin_details.jsp"/>
	</div>
	<div id="summary">
		<s:include value="admin_summary.jsp"/>
	</div>
</div>
