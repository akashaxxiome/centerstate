<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request"/>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>
<%     int indentLevelOffset = 0;
%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
		<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
		<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
		<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetGroupLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
		<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${ACHCompanyID}"/>
	</s:if>
</ffi:cinclude>
<ffi:list collection="${collectionName}" items="EntType">
		<% String hideInitBox; %>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_HIDE_BOX %>"/>
		<ffi:getProperty name="EntType" property="Value" assignTo="hideInitBox"/>
		
        <% String indentLevel = "";
            boolean isSectionHeading = false;
        %>
        <ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
        <ffi:getProperty name="EntType" property="Value" assignTo="indentLevel"/>

        <ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
        <ffi:cinclude value1="${EntType.Value}" value2="0" operator="notEquals">
            <ffi:setProperty name="isSectionHeading" value="false"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.Value}" value2="0" operator="equals">
            <%-- indent level of 0 indicate the start of a new section --%>
            <%-- which requires the name of the Entitlement type to be bolded and a line to be draw across the from --%>
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.OperationName}" value2="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACH_BATCH %>" operator="equals">
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.OperationName}" value2="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.CCD_TXP %>" operator="equals">
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${EntType.OperationName}" value2="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.CCD_DED %>" operator="equals">
            <ffi:setProperty name="isSectionHeading" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${isSectionHeading}" value2="true" operator="equals">
            <%isSectionHeading = true;
            indentLevelOffset = 0; %>
        </ffi:cinclude>

		<ffi:setProperty name="hideEnt" value="false" />
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:cinclude value1="${hideInitBox}" value2="" operator="notEquals">
				<ffi:setProperty name="hideEnt" value="true" />
			</ffi:cinclude> 
		</ffi:cinclude>
	<ffi:cinclude value1="${hideEnt}" value2="false">
	<TR>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
		<ffi:getProperty name="EntType" property="Value" assignTo="indentLevel"/>
		<% int indentLevelAsInt = 0;
           String classParam = "columndata";
		   String space = "";
		   if( indentLevel != null && !indentLevel.equals( "" ) ) {
				indentLevelAsInt = Integer.parseInt( indentLevel );
               if (isSectionHeading)
                   indentLevelOffset = indentLevelAsInt;
	        	if (indentLevelAsInt > 0) {
	           		classParam = "indent" + Integer.toString(indentLevelAsInt - indentLevelOffset) + " columndata";
	        	}
		   }
		%>
		<ffi:cinclude value1="${isSectionHeading}" value2="true" operator="equals">
				<TD colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top"><hr noshade size="1"></TD>
			</TR>
			<TR>
		</ffi:cinclude>
		
		<ffi:setProperty name="EntType" property="CurrentProperty" value="form element name"/>
	   	<ffi:setProperty name="EntTypeLabel" value="${EntType.Value}"/>

		<% String checkBoxValue = "";
		   String limitValue = ""; 
		   String approvalBoxValue = ""; %>

		<%-- check if the admin checkbox for the entitlement type should be checked --%>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		<TD class="adminBackground" align="center" valign="middle">
		</ffi:cinclude>
		<% String hideAdminBox; %>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_HIDE_ADMIN_BOX %>"/>
		<ffi:getProperty name="EntType" property="Value" assignTo="hideAdminBox"/>

		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="equals">
			<%-- if no admin partner property is set --%>
			&nbsp;
		</ffi:cinclude>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">			
			<%-- display box if it should be visible, else put hidden input in the form --%>
			<% if( hideAdminBox == null || hideAdminBox.equals( "" ) ) { %>
				<ffi:getProperty name="admin${EntTypeLabel}" assignTo="checkBoxValue"/>
        	        	<INPUT type="<ffi:getProperty name="AdminCheckBoxType"/>" disabled <ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.Value}" operator="equals"> checked </ffi:cinclude> border="0">
			<% } else { %>
			 	&nbsp;
			<% } %>
		</ffi:cinclude>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		</TD>
		</ffi:cinclude>

		<%-- check if the init checkbox for the entitlement type should be checked --%>
		<TD class="adminBackground" align="center" valign="middle">

		<%-- display box if it should be visible, else put hidden input in the form --%>
		<% if( hideInitBox == null || hideInitBox.equals( "" ) ) { %>
			<ffi:getProperty name="init${EntTypeLabel}" assignTo="checkBoxValue"/>
                	<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals"> checked </ffi:cinclude> border="0">
		<% } else { %>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		 	&nbsp;
		 </ffi:cinclude>
		<% } %>
		</TD>
		
	<%-- we only need to display the limit if the init box is visible --%>
	<% if( hideInitBox == null || hideInitBox.equals( "" ) ) { %>
		<%-- set up the variables for display limits --%>
		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.DEBIT_NAME %>"/>
		<ffi:setProperty name="hasDebit" value="false"/>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
			<ffi:setProperty name="hasDebit" value="true"/>
		</ffi:cinclude>

		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CREDIT_NAME %>"/>
		<ffi:setProperty name="hasCredit" value="false"/>
		<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
			<ffi:setProperty name="hasCredit" value="true"/>
		</ffi:cinclude>

		<%-- place the limit txtboxes, labels and approval boxes in the correct order --%>
		<ffi:cinclude value1="${hasCredit}" value2="true" operator="equals">
			<TD class='<ffi:getPendingStyle fieldname="${EntType.OperationName}" 
				defaultcss='<%=( isSectionHeading ? "sectionsubhead" : classParam ) %>' 
				dacss='<%=classParam + " columndataDA" %>' 
				sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'  
				valign="middle" align="left" nowrap>
         		<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
				<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
					<ffi:getProperty name="EntType" property="Value"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="notEquals">
					<ffi:getProperty name="EntType" property="OperationName"/>
				</ffi:cinclude>
			</TD>
			<ffi:cinclude value1="${hasDebit}" value2="true" operator="equals">
			 	<%-- put per batch credit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_218"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="perBatchCredit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_218"/></TD>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 							<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
							<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_238"/> 
						</TD>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approvePerBatchCredit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>
				
			 	<%-- put per batch debit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_219"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="perBatchDebit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_219"/></TD>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 							<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
							<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_239"/> 
						</TD>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approvePerBatchDebit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>
				
			<%-- some Entitlement Type has 4 limits, therefore requires two rows for display --%>
			</TR>
			<TR>
			
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
				<TD class="adminBackground" colspan="3"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
				</ffi:cinclude>
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
				<TD class="adminBackground" colspan="2"><img src="/cb/web/multilang/grafx/user/spacer.gif"></TD>
				</ffi:cinclude>
				
			 	<%-- put daily credit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_216"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="dailyCredit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_216"/></TD>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 							<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
							<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_101"/> 
						</TD>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approveDailyCredit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>

			 	<%-- put daily debit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_217"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="dailyDebit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_217"/></TD>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 							<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
							<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_102"/> 
						</TD>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approveDailyDebit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>
			</ffi:cinclude>
			<ffi:cinclude value1="${hasDebit}" value2="false" operator="equals">
				<%-- Requires Approval Processing --%>
				<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">	
				 <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
					<ffi:setProperty name="EditRequiresApproval" property="OperationName" value="${EntType.OperationName}" />
					<ffi:setProperty name="EditRequiresApproval" property="ExecOpSelVerify" value="true" />	
					<ffi:process name="EditRequiresApproval" /> 	
					<TD valign="middle" align="center">&nbsp;&nbsp;
						<ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="equals">
							<input type="checkbox"  disabled <ffi:getProperty name="EditRequiresApproval" property="OpSelected" />	/>		
						</ffi:cinclude>
						<ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="notEquals">
							&nbsp;
						</ffi:cinclude>
					</TD>
				 </ffi:cinclude>
				</ffi:cinclude>
				<%--  END Requires Approval Processing --%>
			 	<%-- put per batch credit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_218"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="perBatchCredit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<ffi:removeProperty name="Entitlement_Limits"/>
							<ffi:removeProperty name="DARequest" />
							<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetGroupLimits" property="Period" value="1"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="1"/>
							<ffi:process name="GetLimitsFromDA" />
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<TD class="columndataDA" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_218"/>
								<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perTransaction}"/>
								<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></span>
								</TD>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_218"/></TD>							
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
						<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_218"/></TD>
						</ffi:cinclude>						
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<ffi:cinclude value1="${DARequest.perTransaction}" value2="" operator="notEquals" >
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
										<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_238"/></span> 
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perTransaction}"/>
										<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> </span>
									</TD>
								</ffi:cinclude>
								<ffi:cinclude value1="${DARequest.perTransaction}" value2="" operator="equals" >
								<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
									<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_238"/></span>
									</TD> 
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 									<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_238"/>
								</TD>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 								<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
								<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_238"/>
							</TD>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>

				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approvePerBatchCredit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>
	
			 	<%-- put daily credit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_216"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="dailyCredit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<ffi:removeProperty name="Entitlement_Limits"/>
							<ffi:removeProperty name="DARequest" />
							<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetGroupLimits" property="Period" value="2"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="2"/>
							<ffi:process name="GetLimitsFromDA" />
						</ffi:cinclude>
					</ffi:cinclude>
					
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<TD class="columndataDA" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_216"/>
								<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perDay}"/>
								<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></span>
								</TD>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_216"/></TD>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_216"/></TD>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<ffi:cinclude value1="${DARequest.perDay}" value2="" operator="notEquals" >
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
										<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_240"/></span> 
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perDay}"/>
										<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> </span>
									</TD>
								</ffi:cinclude>
								<ffi:cinclude value1="${DARequest.perDay}" value2="" operator="equals" >
								<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
									<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_240"/></span>
									</TD> 
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 									<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_240"/>
								</TD>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 								<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
								<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_240"/>
							</TD>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approveDailyCredit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${hasCredit}" value2="false" operator="equals">
			<ffi:cinclude value1="${hasDebit}" value2="true" operator="equals">
				<TD class='<ffi:getPendingStyle fieldname="${EntType.OperationName}" 
					defaultcss='<%=( isSectionHeading ? "sectionsubhead" : classParam ) %>' 
					dacss='<%=classParam + " columndataDA" %>' 
					sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'   
					valign="middle" align="left" nowrap>
				    <ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:getProperty name="EntType" property="Value"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="notEquals">
						<ffi:getProperty name="EntType" property="OperationName"/>
					</ffi:cinclude>
				</TD>
				<%-- Requires Approval Processing --%>
				<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
				<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
					<ffi:setProperty name="EditRequiresApproval" property="OperationName" value="${EntType.OperationName}" />
					<ffi:setProperty name="EditRequiresApproval" property="ExecOpSelVerify" value="true" />	
					<ffi:process name="EditRequiresApproval" /> 	
					<TD valign="middle" align="center">&nbsp;&nbsp;
						<ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="equals">
							<input type="checkbox"  disabled <ffi:getProperty name="EditRequiresApproval" property="OpSelected" />	/>		
						</ffi:cinclude>
						<ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="notEquals">
							&nbsp;
						</ffi:cinclude>
					</TD>
				 </ffi:cinclude>
				</ffi:cinclude>
				<%--  END Requires Approval Processing --%>
			 	<%-- put per batch debit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_219"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<ffi:removeProperty name="Entitlement_Limits"/>
							<ffi:removeProperty name="DARequest" />
							<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetGroupLimits" property="Period" value="1"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="1"/>
							<ffi:process name="GetLimitsFromDA" />
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:getProperty name="perBatchDebit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<TD class="columndataDA" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_219"/>
								<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perTransaction}"/>
								<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></span>
								</TD>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_219"/></TD>							
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_219"/></TD>						
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<ffi:cinclude value1="${DARequest.perTransaction}" value2="" operator="notEquals" >
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
										<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_239"/></span> 
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perTransaction}"/>
										<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> </span>
									</TD>
								</ffi:cinclude>
								<ffi:cinclude value1="${DARequest.perTransaction}" value2="" operator="equals" >
									<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
										<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
											<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
											<span class="columndataDA"><s:text name="jsp.user_238"/></span>
										</TD> 
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perTransactionChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 									<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_239"/>
								</TD>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 								<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
								<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_239"/>
							</TD>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approvePerBatchDebit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>

			 	<%-- put daily debit value and approval checkbox if there is one --%>
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="notEquals">
					<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_217"/></TD>
				</ffi:cinclude>
				
				<ffi:cinclude value1="<%= checkBoxValue %>"  value2="${EntType.OperationName}" operator="equals">
					<ffi:getProperty name="dailyDebit${EntTypeLabel}" assignTo="limitValue"/>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<ffi:removeProperty name="Entitlement_Limits"/>
							<ffi:removeProperty name="DARequest" />
							<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetGroupLimits" property="Period" value="2"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${EntType.OperationName}"/>
							<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="2"/>
							<ffi:process name="GetLimitsFromDA" />
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<TD class="columndataDA" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_217"/>
								<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perDay}"/>
								<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></span>
								</TD>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_217"/></TD>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap><s:text name="jsp.user_217"/></TD>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" >
								<ffi:cinclude value1="${DARequest.perDay}" value2="" operator="notEquals" >
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
										<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_241"/></span> 
										<ffi:setProperty name="CurrencyObject" property="Amount" value="${DARequest.perDay}"/>
										<span class="sectionhead_greyDA"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> </span>
									</TD>
								</ffi:cinclude>
								<ffi:cinclude value1="${DARequest.perDay}" value2="" operator="equals" >
									<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
										<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
										<span class="columndataDA"><s:text name="jsp.user_241"/></span>
									</TD>
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${DARequest.perDayChange}" value2="<%=IDualApprovalConstants.YES %>" operator="notEquals">
								<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 									<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
									<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_241"/>
								</TD>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TD class="columndata" valign="middle" align="left" colspan="2" nowrap>
 								<ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
								<ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> <s:text name="jsp.user_241"/>
							</TD>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
				
				<TD valign="middle" align="center">
					<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
					<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
					<ffi:cinclude value1="${EntType.Value}" value2="" operator="notEquals">
						<ffi:setProperty name="allowApproval" value="${EntType.Value}"/>
					</ffi:cinclude>
					<% space = "&nbsp;"; %>
					<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
							<ffi:getProperty name="approveDailyDebit${EntTypeLabel}" assignTo="approvalBoxValue"/>
							<INPUT type="checkbox" disabled <ffi:cinclude value1="<%= approvalBoxValue %>"  value2="true" operator="equals"> checked </ffi:cinclude> border="0">
							<% pageContext.setAttribute("approvalBoxValue", ""); %>
							<% space = ""; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<%=space%>
				</TD>
			</ffi:cinclude>
			<ffi:cinclude value1="${hasDebit}" value2="false" operator="equals">
				<TD class='<ffi:getPendingStyle fieldname="${EntType.OperationName}" 
					defaultcss='<%=( isSectionHeading ? "sectionsubhead" : classParam ) %>' 
					dacss='<%=classParam + " columndataDA" %>' 
					sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'   
					valign="middle" style="text-align: left;" colspan="7" nowrap>
                    <ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:getProperty name="EntType" property="Value"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="notEquals">
						<ffi:getProperty name="EntType" property="OperationName"/>
					</ffi:cinclude>
				</TD>
			</ffi:cinclude>
		</ffi:cinclude>
	<%-- closing the if statement for the displayInitBox check --%>
	<% } else { %>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		<TD class='<%=isSectionHeading?"sectionsubhead":classParam %>'   valign="middle" style="text-align: left;" colspan="7" nowrap>
			<ffi:setProperty name="EntType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
			<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
				<ffi:getProperty name="EntType" property="Value"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="notEquals">
				<ffi:getProperty name="EntType" property="OperationName"/>
			</ffi:cinclude>
		</TD>
	</ffi:cinclude>
	<% } %>
	</TR>
	</ffi:cinclude>
</ffi:list>

<ffi:removeProperty name="CurrencyObject"/>
<ffi:removeProperty name="isSectionHeading"/>
<ffi:removeProperty name="EntTypeLabel"/>
<ffi:removeProperty name="hasCredit"/>
<ffi:removeProperty name="hasDebit"/>
<ffi:removeProperty name="allowApproval"/>
<ffi:removeProperty name="GetEntitlementsFromDA"/>
<ffi:removeProperty name="GetLimitsFromDA"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>
