<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<ffi:object  id="CheckRequiresApprovalDAId"  name="com.ffusion.tasks.dualapproval.CheckRequiresApprovalDA" scope="session"/>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="CheckRequiresApprovalDA" property="ModifyUserOnly" value="false" />
	<ffi:setProperty name="CheckRequiresApprovalDA" property="GroupId" value="${EditGroup_GroupId}"/>
</s:if>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckRequiresApprovalDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
    <ffi:setProperty name="CheckRequiresApprovalDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="CheckRequiresApprovalDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="CheckRequiresApprovalDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<ffi:setProperty name="CheckRequiresApprovalDA" property="ObjectType" value="${category.objectType}"/>
<ffi:setProperty name="CheckRequiresApprovalDA" property="ObjectId" value="${category.objectId}"/>
<ffi:setProperty name="CheckRequiresApprovalDA" property="OperationName" value="${limit.operationName}"/>
<ffi:process name="CheckRequiresApprovalDA"/>
