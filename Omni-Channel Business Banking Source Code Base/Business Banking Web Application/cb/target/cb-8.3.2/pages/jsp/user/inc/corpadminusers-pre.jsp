<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name="corpadminusers_init_touched" value="true"/>

<%-- check if this secure user is entitled to manage user --%>

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="DivisionId"/>
<ffi:removeProperty name="DivisionName"/>

<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
<ffi:process name="GetBusinessEmployeesByEntGroups"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
<% int AdminCount = 0; %>
<ffi:list collection="BusinessEmployees" items="admin">
<% AdminCount++; %>
</ffi:list>
<% if( AdminCount == 1 ) { %>
	<ffi:setProperty name="OneAdmin" value="TRUE"/>
<% } else { %>
	<ffi:setProperty name="OneAdmin" value="FALSE"/>
<% }  %>

<ffi:object id="Entitlement_EntitlementGroup" name="com.ffusion.csil.beans.entitlements.EntitlementGroup" scope="session"/>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>

<ffi:object name="com.ffusion.tasks.util.GetCSILGlobalSetting" id="GetCSILGlobalSetting" scope="request" />
<ffi:setProperty name="GetCSILGlobalSetting" property="TagName" value="BusinessUserMaxResultSize"/>
<ffi:setProperty name="GetCSILGlobalSetting" property="SessionName" value="BusinessUserMaxResultSizeSession"/>
<ffi:process name="GetCSILGlobalSetting"/>




<ffi:setProperty name="BusinessEmployees" property="Filter" value="CUSTOMER_TYPE=1"/>

<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>

<s:action namespace="/pages/user" name="getPendingApprovalBusinessEmployees_getPendingRecCount"/>      

<% if (request.getAttribute("pendiangDAUserCount") != null &&  request.getAttribute("pendiangDAUserCount").equals("0")) {
	%>
	 <script>
	  $('#admin_usersPendingApprovalTab').hide();</script>
<%} else {%>
 <script>
 	 $('#admin_usersPendingApprovalTab').show();
	</script>
<%}%>

