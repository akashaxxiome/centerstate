<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.ffusion.struts.profile.DeleteBusinessUserProfileAction"%>

<%-- This page is only called when using channel based entitlements. --%>
<ffi:help id="user_corpadminprofiledelete-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_567')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<div id="wholeworld">
		<div align="center">

		<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center">
						<ffi:cinclude value1="${ProfAssignCount}" value2="1">
							<ffi:getProperty name="ProfAssignCount"/>&nbsp;<s:text name="jsp.user_565"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ProfAssignCount}" value2="1" operator="notEquals">
							<ffi:getProperty name="ProfAssignCount"/>&nbsp;<s:text name="jsp.user_566"/>
						</ffi:cinclude>
						</TD>
							<td></td>
						</TR>
						<TR>
							<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
							<td></td>
						</TR>
						<TR>
							<TD>
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a id="cancelDeleteUserProfileLink" button="true" onClickTopics="closeDeleteProfileDialog"
									  title="%{getText('jsp.default_83')}" 
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
								<s:url id="deleteUserProfileUrl" value="/pages/jsp/businessuserprofile/deleteBusinessUserProfile_execute.action">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
			<p></p>
		</div>
</div>
<ffi:removeProperty name="<% DeleteBusinessUserProfileAction.SESSION_NAME %>"/>
