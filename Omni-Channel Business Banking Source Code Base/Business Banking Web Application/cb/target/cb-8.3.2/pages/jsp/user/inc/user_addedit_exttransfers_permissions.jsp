<%--
| The purpose of this page is to generate the permissions table for the secondary user external transfers pages.
| Inputs that this page expects are:
|
|       - the title to be displayed in the header of the table set in the session variable called "PermissionsTitle"
|       - the operation display name to be included in the "Enable ... From This Account" and
|         "Enable ... To This Account" column headers in the session variable called "EnableOperationDisplayValue"
|       - the session variable called "IncludeFromSection" set to "TRUE" if and only if the "From ..." section should
|         be rendered
|       - the session variable called "IncludeToSection" set to "TRUE" if and only if the "To ..." section should be
|         rendered
|       - the session variable called "AccountsCollectionNames" set to a StringList of session names of Accounts
|         collections to which the corresponding collections of accounts the user can be granted access to for the
|         purposes of initiating transactions of the given operation type(s) is bound
|       - the session variable called "FromOperationName" set to the entitlement operation to include in the names of
|         the form input elements in the "From ..." section
|       - the session variable called "ToOperationName" set to the entitlement operation to include in the names of the
|         form input elements in the "To ..." section
|       - the session variable called "AccountsFromFilters" set to a table of filters to be applied against each
|         individual Accounts bean. Each account in a particular collection must satisfy the corresponding filter in
|         order to be included in the list of candidate accounts in the "From ..." section of the page. The key of each
|         pair in the table is the session name of the collection to which the value, i.e., the filter, will be applied.
|       - the session variable called "AccountsToFilters" set to a table of filters to be applied against each
|         individual Accounts bean. Each account in a particular collection must satisfy the corresponding filter in
|         order to be included in the list of candidate accounts in the "To ..." section of the page. The key of each
|         pair in the table is the session name of the collection to which the value, i.e., the filter, will be applied.
|       - the session variable called "FeatureAccessAndLimitsTaskName" set to the session name of the
|         FeatureAccessAndLimits task used to process and persist the permissions settings
|       - the session variable called "TransferAccessAndLimitsTaskName" set to the session name of the
|         TransferAccessAndLimits task storing the current set of transfer permissions and limits.
--%>


<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>' startsWith="true"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_MAX_LIMIT_PREFIX %>' startsWith="true"/>

<%
String IsObjectEntitled = null;
String ObjectLimitValue = null;
String ErrorMessage = null;
String ErrorsDetected = null;
%>

<%-- The utility used to help compare integer values. --%>
<ffi:object name="com.ffusion.beans.util.IntegerMath" id="Comparator" scope="request"/>

<%-- Set variables to display records in alternating colors. --%>
<ffi:setProperty name="pband1" value="class=\"tabledata_white\""/>
<ffi:setProperty name="pband2" value="class=\"tabledata_gray\""/>

<%-- Create the task used to generate entitlement object identifiers for accounts. --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetAccountEntitlementObjectID" scope="request"/>

<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="Reset" value=""/>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddLimitPeriodToList" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddLimitPeriodToList" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>

<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup" id="CheckParentEntitlements" scope="request"/>
<ffi:setProperty name="CheckParentEntitlements" property="AttributeName" value="IsParentEntitled"/>

<ffi:removeProperty name="Errors Detected"/>
<ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="HasDetectedRedundantLimits" assignTo="ErrorsDetected"/>
<ffi:cinclude value1="${ErrorsDetected}" value2="true" operator="equals">
<div class="marginTop10"></div>
  <div style="padding-left: 10px; padding-right: 10px;" class="limitCheckError">
      Error: One or more of the limits you are attempting to create is less restrictive than existing limits and/or other limits that you have specified. The invalid limits in question are indicated below and must be reduced or removed.
  </div>
</ffi:cinclude>
<div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="paneContentWrapper">
   			<div class="nameSubTitle"><ffi:getProperty name="PermissionsTitle"/></div>
	                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData tdTopBottomPadding5 marginTop10">
	<ffi:cinclude value1="${IncludeFromSection}" value2="TRUE" operator="equals">
		<s:include value="/pages/jsp/user/inc/user_addedit_exttransfers_permissions_from.jsp" />
	</ffi:cinclude>
	<ffi:cinclude value1="${IncludeFromSection}" value2="TRUE" operator="equals">
	    <ffi:cinclude value1="${IncludeToSection}" value2="TRUE" operator="equals">
	                        <tr>
	                            <td colspan="6" height="20"></td>
	                        </tr>
	    </ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${IncludeToSection}" value2="TRUE" operator="equals">
		<s:include value="/pages/jsp/user/inc/user_addedit_exttransfers_permissions_to.jsp" />	
	</ffi:cinclude>
	                    </table>
	</div>
</div></div>
<ffi:removeProperty name="SessionPrefix"/>
<ffi:removeProperty name="FAALMLSessionPrefix"/>
<ffi:removeProperty name="AccountEntitlementObject"/>
<ffi:removeProperty name="FAALBaseName"/>
<ffi:removeProperty name="FAALMLBaseName"/>
<ffi:removeProperty name="IsObjectEntitled"/>
<ffi:removeProperty name="ObjectLimitValue"/>
<ffi:removeProperty name="ErrorMessage"/>
<ffi:removeProperty name="Errors Detected"/>
<ffi:removeProperty name="TempPeriod"/>
<ffi:removeProperty name="HasAccount"/>
<ffi:removeProperty name="MaxLimit"/>
<ffi:removeProperty name="TempIsObjectEntitled"/>
<ffi:removeProperty name="IsParentEntitled"/>
