<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<ffi:help id="user_corpadminprofiles_da_pendingapproval" className="moduleHelpClass"/>

<div id="hidden"  style="display:none;">
    <s:url id="editDAProfileUrl" value="/pages/jsp/user/businessuserprofileedit.jsp" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
    </s:url>
    <sj:a id="editDAProfileLink" href="%{editDAProfileUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_146')}"
        onClickTopics="beforeLoad" onCompleteTopics="completeProfileLoad" onErrorTopics="errorLoad">
      <span class="ui-icon ui-icon-wrench"></span>
    </sj:a>	
</div>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="ApproveURL" url="/cb/pages/jsp/user/da-profile-verify.jsp?Section=Profiles&itemId={0}&rejectFlag=n&userName={1}&userAction={2}" parm0="Id" parm1="UserName" parm2="Action"/>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Profiles&BusinessEmployeeId={0}&OneAdmin={1}" parm0="Id" parm1="OneAdmin"/>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="DiscardURL" url="/cb/pages/jsp/user/da-verify-discard-profile.jsp?itemId={0}&userAction={1}&successUrl=/SecurePath/user/businessuserprofiles.jsp&itemType=EntitlementProfile&category=Profile" parm0="Id" parm1="Action"/>
				
<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="SubmitForApprovalURL" url="/cb/pages/jsp/user/da-verify-submit-profile.jsp?itemId={0}&successUrl=/SecurePath/user/businessuserprofiles.jsp&itemType=EntitlementProfile&category=Profile" parm0="Id"/>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="ModifyChangeURL" url="/cb/pages/dualapproval/dualApprovalAction-revokingApproval.action?itemId={0}&successUrl=/SecurePath/user/businessuserprofiles.jsp&module=EntitlementProfile&category=Profile" parm0="Id"/>
			
<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="RejectURL" url="/cb/pages/jsp/user/da-profile-verify.jsp?Section=Profiles&itemId={0}&successUrl=/SecurePath/user/businessuserprofiles.jsp&itemType=EntitlementProfile&category=Profile&rejectFlag=y&userName={1}&userAction={2}" parm0="Id" parm1="UserName" parm2="Action"/>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="ViewProfilePermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Profiles&BusinessEmployeeId={0}&OneAdmin={1}&ViewUserPermissions=TRUE&UsingEntProfiles={2}&ChannelId={3}" parm0="Id" parm1="OneAdmin" parm2="UsingEntProfiles" parm3="ChannelId"/>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="EditURL" url="/cb/pages/jsp/businessuserprofile/modifyBusinessUserProfile_init.action?employeeID={0}" parm0="Id"/>

<ffi:setGridURL grid="GRID_adminProfilesPendingApprovalChanges" name="ViewURL" url="/cb/pages/jsp/user/corpadminprofileview.jsp?itemId={0}" parm0="Id"/>
		
<ffi:setProperty name="profilesWithPendingChangesGridTempURL" value="getPendingApprovalBusinessUserProfiles.action?GridURLs=GRID_adminProfilesPendingApprovalChanges&CollectionName=PendingApprovalEmployees&dualApprovalMode=1" URLEncrypt="true"/>
<s:url namespace="/pages/jsp/businessuserprofile" id="getEntitlementProfilesUrl" action="%{#session.profilesWithPendingChangesGridTempURL}" escapeAmp="false"/>
<ffi:setProperty name="profilesWithPendingChangesGridTempChildURL" value="getPendingApprovalBusinessUserProfilesChild.action?GridURLs=GRID_adminProfilesPendingApprovalChanges&CollectionName=PendingApprovalEmployees&dualApprovalMode=1" URLEncrypt="true"/>
<s:url namespace="/pages/jsp/businessuserprofile" id="getEntitlementProfilesChildUrl" action="%{#session.profilesWithPendingChangesGridTempChildURL}" escapeAmp="false"/>

     <sjg:grid
       id="profilesWithPendingChangesGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{getEntitlementProfilesUrl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       altRows="true"
       sortname="USERNAME"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       navigatorSearch="false"
       onGridCompleteTopics="addGridControlsEvents,removeSubGridItems,pendingProfileDAChangesGridCompleteEvents,tabifyNotes"
     >
	    <sjg:grid
	       id="profilesWithPendingChangesChildGrid"
	       caption=""
		   sortable="true"  
	       dataType="json"
	       href="%{getEntitlementProfilesChildUrl}"
	       gridModel="gridModel"
		   rowList="4,8,10"
		   rowNum="10" 
	       rownumbers="false"
	       altRows="true"
	       shrinkToFit="true"
	       onGridCompleteTopics="addGridControlsEvents"
	     >
	     <sjg:gridColumn name="userName" width="300" index="USERNAME" title="%{getText('jsp.user_267')}" sortable="false" />
	     <sjg:gridColumn name="map.Group" width="150" index="Group" title="%{getText('jsp.default_225')}" sortable="false" />
	     <sjg:gridColumn name="ID" index="operation" title="%{getText('jsp.default_27')}" sortable="false" width="90" search="false" formatter="formatDAPendingChangesProfileActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	  	 <sjg:gridColumn name="map.ChannelType" width="150" index="ChannelType" title="Channel" sortable="true" />
	 </sjg:grid>
     <sjg:gridColumn name="userName" width="300" index="USERNAME" title="%{getText('jsp.user_267')}" sortable="true" />
     <sjg:gridColumn name="map.Group" width="150" index="Group" title="%{getText('jsp.default_225')}" sortable="true" />
     <sjg:gridColumn name="action" index="action" title="%{getText('jsp.default_388')}" sortable="false" hidedlg="false" width="90" search="false"/>
     <sjg:gridColumn name="ID" index="operation" title="%{getText('jsp.default_27')}" sortable="false" width="90" search="false" formatter="formatDAPendingChangesProfileActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
  	 <sjg:gridColumn name="map.ChannelType" width="150" index="ChannelType" title="Channel" sortable="true" />
	 <sjg:gridColumn name="map.IsAnAdminOf" width="0" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.OneAdmin" width="0" index="OneAdmin" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.Self" width="0" index="Self" title="%{getText('jsp.user_294')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="rejectReason" width="0" index="rejectReason" title="rejectReason" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="rejectedBy" index="rejectedBy" title="rejectedBy" sortable="false" width="0" hidden="true" hidedlg="true"/>
	 <sjg:gridColumn name="status" index="status" title="status" sortable="false" width="0" hidden="true" hidedlg="true"/>
	 <sjg:gridColumn name="channelId" width="0" index="ChannelId" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
 </sjg:grid>

<script type="text/javascript">
//Code to remove the expand subgrid icon from the grid which is not applicable.
//Only cross channel profiles have child profiles so removing the icon from the rest of the per channel profiles.
	$.subscribe('removeSubGridItems', function(event,data) {	
		var gridID = data.id;
		if (!gridID.startsWith('#'))
			gridID = '#' + gridID;
			var data = $(gridID).getDataIDs();
			for(a=0;a<data.length;a++){
				row=jQuery(gridID).getRowData(data[a]);
				
				var group = row['map.Group'];
				var userName = row['userName'];
				var id = data[a];
				var channelId = row['channelId'];
				if(channelId != "X") {
					 $('tr#'+id, $(gridID)).children("td.sgcollapsed")
	                 .html("")
	                 .removeClass('ui-sgcollapsed sgcollapsed');
			}
		}
	});
	var fnCustomPendingProfilesRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#profilesWithPendingChangesGrid").data('fnCustomRefresh', fnCustomPendingProfilesRefresh);
	
	$("#profilesWithPendingChangesGrid").data("supportSearch",true);

	//disable sortable rows
	if(ns.common.isInitialized($("#profilesWithPendingChangesGrid tbody"),"jqGrid")){
		$("#profilesWithPendingChangesGrid tbody").sortable("destroy");
	};
	$("#profilesWithPendingChangesGrid").jqGrid('setColProp','ID',{title:false});	
</script>
