<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:url id="pageurl" value="/pages/jsp/user/themePickerDialog.jsp" escapeAmp="false">

</s:url>
<sj:dialog
	id="themePickerDialogId"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.home_244')}"
	height="500"
	width="565"
	href="%{pageurl}"
	showEffect="fold"
	hideEffect="clip"
	onCloseTopics="ns.home.saveThemeTopic"
	resizable="false">
</sj:dialog>