<%--
| The purpose of this page is to generate the tables listing any outstanding bill payments and/or transfers that a secondary user may
| have when s/he is being marked inactive or deleted. This page requires the following inputs:
|
|       - the profile ID of the secondary user being modified set in the session variable SecondaryUserToModifyID
|       - the name of the radio button used by the user to specify the desired action set in the session variable TransferOrCancelTransactionsInputName
|       - the mode in which this page is being used set in the session variable AddEditMode; possible values are: "Edit" if editing a user,
|         "Delete" if deleting a user, or "EditMultiple" if changing the state of multiple users simultaneously
|       - the collection of recurring payments set in the session variable RecPayments
|       - the collection of single payments set in the session variable Payments
|       - the collection of recurring transfers set in the session variable RecTransfers
|       - the collection of single transfers set in the session variable Transfers
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>


<%-- Set variables to display records in alternating colors. --%>
<ffi:setProperty name="pband1" value="class=\"tabledata_white\""/>
<ffi:setProperty name="pband2" value="class=\"tabledata_gray\""/>

<%--
Filter all the pending transactions collections so that only the transactions created by the secondary user will
be displayed on the page.
--%>
<ffi:setProperty name="PaymentPendingStatus" value='<%= "" + com.ffusion.beans.billpay.PaymentStatus.PMS_SCHEDULED %>'/>
<ffi:setProperty name="PaymentPendingApprovalStatus" value='<%= "" + com.ffusion.beans.billpay.PaymentStatus.PMS_PENDING_APPROVAL %>'/>

<ffi:setProperty name="RecPayments" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="RecPayments" property="FilterOnFilter" value="STATUS==${PaymentPendingStatus},STATUS==${PaymentPendingApprovalStatus}"/>

<ffi:object name="com.ffusion.tasks.billpay.FindRecPayments" id="FindRecPayments" scope="request"/>
<ffi:setProperty name="FindRecPayments" property="RemovePending" value="true"/>
<ffi:process name="FindRecPayments"/>

<ffi:setProperty name="Payments" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="Payments" property="FilterOnFilter" value="STATUS==${PaymentPendingStatus},STATUS==${PaymentPendingApprovalStatus}"/>

<ffi:setProperty name="TransferPendingStatus" value='<%= "" + com.ffusion.beans.banking.TransferStatus.TRS_SCHEDULED %>'/>
<ffi:setProperty name="TransferPendingApprovalStatus" value='<%= "" + com.ffusion.beans.banking.TransferStatus.TRS_PENDING_APPROVAL %>'/>

<ffi:setProperty name="RecTransfers" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="RecTransfers" property="FilterOnFilter" value="STATUS==${TransferPendingStatus},STATUS==${TransferPendingApprovalStatus}"/>

<ffi:object name="com.ffusion.tasks.banking.FindRecTransfers" id="FindRecTransfers" scope="request"/>
<ffi:setProperty name="FindRecTransfers" property="RemovePending" value="true"/>
<ffi:process name="FindRecTransfers"/>

<ffi:setProperty name="Transfers" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="Transfers" property="FilterOnFilter" value="STATUS==${TransferPendingStatus},STATUS==${TransferPendingApprovalStatus}"/>

<ffi:setProperty name="IncludePendingTXNotice" value=""/>

<ffi:cinclude value1="${RecPayments.Size}" value2="0" operator="notEquals">
    <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${Payments.Size}" value2="0" operator="notEquals">
    <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${RecTransfers.Size}" value2="0" operator="notEquals">
    <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${Transfers.Size}" value2="0" operator="notEquals">
    <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${IncludePendingTXNotice}" value2="true" operator="equals">
    <ffi:setProperty name="HasExpiredTransaction" value=""/>
    <ffi:setProperty name="RecPaymentsCollectionName" value="RecPayments"/>
    <ffi:setProperty name="PaymentsCollectionName" value="Payments"/>
    <ffi:setProperty name="RecTransfersCollectionName" value="RecTransfers"/>
    <ffi:setProperty name="TransfersCollectionName" value="Transfers"/>

    <%-- Check for any outstanding transactions that may have expired. --%>
    <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredRecPayments" scope="request"/>
    <ffi:setProperty name="GetExpiredRecPayments" property="SourceCollectionSessionName" value="RecPayments"/>
    <ffi:setProperty name="GetExpiredRecPayments" property="DestinationCollectionSessionName" value="ExpiredRecPayments"/>
    <ffi:setProperty name="GetExpiredRecPayments" property="Initialize" value="TRUE"/>
    <ffi:setProperty name="GetExpiredRecPayments" property="Process" value="TRUE"/>
    <ffi:process name="GetExpiredRecPayments"/>

    <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredPayments" scope="request"/>
    <ffi:setProperty name="GetExpiredPayments" property="SourceCollectionSessionName" value="Payments"/>
    <ffi:setProperty name="GetExpiredPayments" property="DestinationCollectionSessionName" value="ExpiredPayments"/>
    <ffi:setProperty name="GetExpiredPayments" property="Initialize" value="TRUE"/>
    <ffi:setProperty name="GetExpiredPayments" property="Process" value="TRUE"/>
    <ffi:process name="GetExpiredPayments"/>

    <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredRecTransfers" scope="request"/>
    <ffi:setProperty name="GetExpiredRecTransfers" property="SourceCollectionSessionName" value="RecTransfers"/>
    <ffi:setProperty name="GetExpiredRecTransfers" property="DestinationCollectionSessionName" value="ExpiredRecTransfers"/>
    <ffi:setProperty name="GetExpiredRecTransfers" property="Initialize" value="TRUE"/>
    <ffi:setProperty name="GetExpiredRecTransfers" property="Process" value="TRUE"/>
    <ffi:process name="GetExpiredRecTransfers"/>

    <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredTransfers" scope="request"/>
    <ffi:setProperty name="GetExpiredTransfers" property="SourceCollectionSessionName" value="Transfers"/>
    <ffi:setProperty name="GetExpiredTransfers" property="DestinationCollectionSessionName" value="ExpiredTransfers"/>
    <ffi:setProperty name="GetExpiredTransfers" property="Initialize" value="TRUE"/>
    <ffi:setProperty name="GetExpiredTransfers" property="Process" value="TRUE"/>
    <ffi:process name="GetExpiredTransfers"/>

    <ffi:cinclude value1="${ExpiredRecPayments.Size}" value2="0" operator="notEquals">
        <ffi:setProperty name="HasExpiredTransaction" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${ExpiredPayments.Size}" value2="0" operator="notEquals">
        <ffi:setProperty name="HasExpiredTransaction" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${ExpiredRecTransfers.Size}" value2="0" operator="notEquals">
        <ffi:setProperty name="HasExpiredTransaction" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${ExpiredTransfers.Size}" value2="0" operator="notEquals">
        <ffi:setProperty name="HasExpiredTransaction" value="true"/>
    </ffi:cinclude>

    <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="equals">
        <ffi:setProperty name="RecPaymentsCollectionName" value="ExpiredRecPayments"/>
        <ffi:setProperty name="PaymentsCollectionName" value="ExpiredPayments"/>
        <ffi:setProperty name="RecTransfersCollectionName" value="ExpiredRecTransfers"/>
        <ffi:setProperty name="TransfersCollectionName" value="ExpiredTransfers"/>
    </ffi:cinclude>

    <ffi:cinclude value1="${FoundUserWithExpiredTransaction}" value2="true" operator="equals">
        <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="notEquals">
            <ffi:setProperty name="IncludePendingTXNotice" value=""/>
        </ffi:cinclude>
    </ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${IncludePendingTXNotice}" value2="true" operator="equals">

    <%
    String InputValue = null;
    String CollectionSize1 = null;
    String CollectionSize2 = null;
    boolean notEmpty = false;
    %>

    <ffi:removeProperty name="InputValue"/>
    <ffi:getProperty name="${TransferOrCancelTransactionsInputName}" assignTo="InputValue"/>
    <ffi:cinclude value1="${AddEditMode}" value2="Edit" operator="equals">
<div class="instructions">
        <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="equals">
<ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_pending_transactions.jsp-3" parm0="${SecondaryUser.FullNameWithLoginId}"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="notEquals">
<ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_pending_transactions.jsp-2" parm0="${SecondaryUser.FullNameWithLoginId}"/>
        </ffi:cinclude>
</div>
    </ffi:cinclude>
    <ffi:cinclude value1="${AddEditMode}" value2="Delete" operator="equals">
<div class="instructions">
        <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="equals">
<ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_pending_transactions.jsp-4" parm0="${TempSecondaryUser.FullNameWithLoginId}"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="notEquals">
<ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_pending_transactions.jsp-1" parm0="${TempSecondaryUser.FullNameWithLoginId}"/>
        </ffi:cinclude>
</div>
    </ffi:cinclude>
    <ffi:cinclude value1="${AddEditMode}" value2="EditMultiple" operator="equals">
<div class="instructions">
    <ffi:getProperty name="TempSecondaryUser" property="FullNameWithLoginId"/>
</div>
    </ffi:cinclude>
    <ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="notEquals">
<div class="instructions">
    <input name="<ffi:getProperty name='TransferOrCancelTransactionsInputName'/>" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_ACTION_TRANSFER %>' <ffi:cinclude value1="${InputValue}" value2='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_ACTION_TRANSFER %>' operator="equals">checked</ffi:cinclude>> <span class=""><!-- L10NStart -->Transfer ownership of these pending transactions to me.<!-- L10NEnd --></span>
    <input name="<ffi:getProperty name='TransferOrCancelTransactionsInputName'/>" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_ACTION_CANCEL %>' <ffi:cinclude value1="${InputValue}" value2='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_ACTION_CANCEL %>' operator="equals">checked</ffi:cinclude>> <span class=""><!-- L10NStart -->Cancel these pending transactions.<!-- L10NEnd --></span>
</div>
    </ffi:cinclude>
    
    
    <ffi:removeProperty name="CollectionEmpty"/>
    <ffi:removeProperty name="CollectionSize1"/>
    <ffi:removeProperty name="CollectionSize2"/>
    <ffi:setProperty name="${RecPaymentsCollectionName}" property="FilterOnFilter" value="STATUS==${PaymentPendingApprovalStatus}"/>
    <ffi:setProperty name="${PaymentsCollectionName}" property="FilterOnFilter" value="STATUS==${PaymentPendingApprovalStatus}"/>
    <ffi:getProperty name="${RecPaymentsCollectionName}" property="Size" assignTo="CollectionSize1"/>
    <ffi:getProperty name="${PaymentsCollectionName}" property="Size" assignTo="CollectionSize2"/>
    <ffi:cinclude value1="${CollectionSize1}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <ffi:cinclude value1="${CollectionSize2}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <%
        if( notEmpty ) {
            session.setAttribute( "CollectionEmpty", "false" );   
        }
    %>
    
    <ffi:cinclude value1="${CollectionEmpty}" value2="false" operator="equals"> <div class="marginTop10"></div>
   <div class="paneWrapper">
  	<div class="paneInnerWrapper"> 
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData">
                <tr class="header">
                    <td style="padding-left: 5px;"><!-- L10NStart -->Date<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->From Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->To Payee/Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Frequency<!-- L10NEnd --></td>
                    <td style="padding-right: 0px; text-align: right;"><!-- L10NStart -->#<!-- L10NEnd --></td>
                    <td style="padding-right: 5px; text-align: right;"><!-- L10NStart -->Amount<!-- L10NEnd --></td>
                </tr>
        <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
        <ffi:setProperty name="Calculator" property="Value1" value="3"/>
        <ffi:setProperty name="Calculator" property="Value2" value="1"/>

        <ffi:setProperty name="${RecPaymentsCollectionName}" property="FilterSortedBy" value="NEXTPAYDATE,TRANSACTIONID"/>
        <ffi:setProperty name="${PaymentsCollectionName}" property="FilterSortedBy" value="PAYDATE,TRANSACTIONID"/>
        
        <ffi:list collection="${RecPaymentsCollectionName}" items="TempRecPayment">
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempRecPayment" property="NextPayDate"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecPayment" property="ConsumerAccountDisplayText"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecPayment" property="PayeeNickName"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecPayment" property="Frequency"/></td>
                    <td style="padding-right: 0px; text-align: right;"><ffi:getProperty name="TempRecPayment" property="RemainingPayments"/></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempRecPayment" property="AmountValue.CurrencyString"/></td>
                </tr>
                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
        <ffi:list collection="${PaymentsCollectionName}" items="TempPayment">
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempPayment" property="PayDate"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempPayment" property="ConsumerAccountDisplayText"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempPayment" property="PayeeNickName"/></td>
                    <td><!--L10NStart-->One Time<!--L10NEnd--></td>
                    <td></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempPayment" property="AmountValue.CurrencyString"/></td>
                </tr>
                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
            </table>
     </div></div>
    </ffi:cinclude>
    

    <ffi:removeProperty name="CollectionEmpty"/>
    <ffi:removeProperty name="CollectionSize1"/>
    <ffi:removeProperty name="CollectionSize2"/>
    <ffi:setProperty name="${RecPaymentsCollectionName}" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
    <ffi:setProperty name="${PaymentsCollectionName}" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
    <ffi:setProperty name="${RecPaymentsCollectionName}" property="FilterOnFilter" value="STATUS==${PaymentPendingStatus}"/>
    <ffi:setProperty name="${PaymentsCollectionName}" property="FilterOnFilter" value="STATUS==${PaymentPendingStatus}"/>
    <ffi:getProperty name="${RecPaymentsCollectionName}" property="Size" assignTo="CollectionSize1"/>
    <ffi:getProperty name="${PaymentsCollectionName}" property="Size" assignTo="CollectionSize2"/>
    <ffi:cinclude value1="${CollectionSize1}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <ffi:cinclude value1="${CollectionSize2}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <%
        if( notEmpty ) {
            session.setAttribute( "CollectionEmpty", "false" );   
        }
    %>
    
    <ffi:cinclude value1="${CollectionEmpty}" value2="false" operator="equals"> <div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="paneContentWrapper"><div class="blockHead marginBottom10">Pending Payments<!-- L10NEnd --></div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData">
                <tr class="header">
                    <td style="padding-left: 5px;"><!-- L10NStart -->Date<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->From Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->To Payee/Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Frequency<!-- L10NEnd --></td>
                    <td style="padding-right: 0px; text-align: right;"><!-- L10NStart -->#<!-- L10NEnd --></td>
                    <td style="padding-right: 5px; text-align: right;"><!-- L10NStart -->Amount<!-- L10NEnd --></td>
                </tr>
        <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
        <ffi:setProperty name="Calculator" property="Value1" value="3"/>
        <ffi:setProperty name="Calculator" property="Value2" value="1"/>

        <ffi:setProperty name="${RecPaymentsCollectionName}" property="FilterSortedBy" value="NEXTPAYDATE,TRANSACTIONID"/>
        <ffi:setProperty name="${PaymentsCollectionName}" property="FilterSortedBy" value="PAYDATE,TRANSACTIONID"/>
        
        <ffi:list collection="${RecPaymentsCollectionName}" items="TempRecPayment">
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempRecPayment" property="NextPayDate"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecPayment" property="ConsumerAccountDisplayText"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecPayment" property="PayeeNickName"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecPayment" property="Frequency"/></td>
                    <td style="padding-right: 0px; text-align: right;"><ffi:getProperty name="TempRecPayment" property="RemainingPayments"/></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempRecPayment" property="AmountValue.CurrencyString"/></td>
                </tr>
            <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
        <ffi:list collection="${PaymentsCollectionName}" items="TempPayment">
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempPayment" property="PayDate"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempPayment" property="ConsumerAccountDisplayText"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempPayment" property="PayeeNickName"/></td>
                    <td><!--L10NStart-->One Time<!--L10NEnd--></td>
                    <td></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempPayment" property="AmountValue.CurrencyString"/></td>
                </tr>
            <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
            </table>
      </div></div></div>
    </ffi:cinclude>
    
    
    <ffi:removeProperty name="CollectionEmpty"/>
    <ffi:removeProperty name="CollectionSize1"/>
    <ffi:removeProperty name="CollectionSize2"/>
    <ffi:setProperty name="${RecTransfersCollectionName}" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
    <ffi:setProperty name="${TransfersCollectionName}" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
    <ffi:setProperty name="${RecTransfersCollectionName}" property="FilterOnFilter" value="STATUS==${TransferPendingApprovalStatus}"/>
    <ffi:setProperty name="${TransfersCollectionName}" property="FilterOnFilter" value="STATUS==${TransferPendingApprovalStatus}"/>
    <ffi:getProperty name="${RecTransfersCollectionName}" property="Size" assignTo="CollectionSize1"/>
    <ffi:getProperty name="${TransfersCollectionName}" property="Size" assignTo="CollectionSize2"/>
    <% notEmpty = false; %>
    <ffi:cinclude value1="${CollectionSize1}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <ffi:cinclude value1="${CollectionSize2}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <%
        if( notEmpty ) {
            session.setAttribute( "CollectionEmpty", "false" );   
        }
    %>
    
    <ffi:cinclude value1="${CollectionEmpty}" value2="false" operator="equals"> <div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="paneContentWrapper"><div class="blockHead marginBottom10">Transfers Pending Approval<!-- L10NEnd --></div>
   		
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData">
                <tr class="header">
                    <td style="padding-left: 5px;"><!-- L10NStart -->Date<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->From Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->To Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Type<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Status<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Frequency<!-- L10NEnd --></td>
                    <td style="padding-right: 0px; text-align: right;"><!-- L10NStart -->#<!-- L10NEnd --></td>
                    <td style="padding-right: 5px; text-align: right;"><!-- L10NStart -->Amount<!-- L10NEnd --></td>
                </tr>
        <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
        <ffi:setProperty name="Calculator" property="Value1" value="3"/>
        <ffi:setProperty name="Calculator" property="Value2" value="1"/>

        <ffi:setProperty name="${RecTransfersCollectionName}" property="FilterSortedBy" value="NEXTDATE,TRANSACTIONID"/>
        <ffi:setProperty name="${TransfersCollectionName}" property="FilterSortedBy" value="DATE,TRANSACTIONID"/>
        
        <ffi:list collection="${RecTransfersCollectionName}" items="TempRecTransfer">
                <ffi:setL10NProperty name="TransferType" value="Internal"/>
                <ffi:setL10NProperty name="FromAccountType" value=""/>
                <ffi:setL10NProperty name="ToAccountType" value=""/>

                <ffi:cinclude value1="${TempRecTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="ToAccountType" value="(E)"/>
                </ffi:cinclude>
                <ffi:cinclude value1="${TempRecTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="FromAccountType" value="(E)"/>
                </ffi:cinclude>
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempRecTransfer" property="NextDate"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="FromAccount.ConsumerDisplayText"/> <ffi:getProperty name="FromAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="ToAccount.ConsumerDisplayText"/> <ffi:getProperty name="ToAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TransferType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="StatusName"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="Frequency"/></td>
                    <td style="padding-right: 0px; text-align: right;"><ffi:getProperty name="TempRecTransfer" property="RemainingTransfers"/></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempRecTransfer" property="AmountValue.CurrencyString"/></td>
                </tr>
                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
        <ffi:list collection="${TransfersCollectionName}" items="TempTransfer">
                <ffi:setL10NProperty name="TransferType" value="Internal"/>
                <ffi:setL10NProperty name="FromAccountType" value=""/>
                <ffi:setL10NProperty name="ToAccountType" value=""/>

                <ffi:cinclude value1="${TempTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="ToAccountType" value="(E)"/>
                </ffi:cinclude>
                <ffi:cinclude value1="${TempTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="FromAccountType" value="(E)"/>
                </ffi:cinclude>
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempTransfer" property="Date"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempTransfer" property="FromAccount.ConsumerDisplayText"/> <ffi:getProperty name="FromAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempTransfer" property="ToAccount.ConsumerDisplayText"/> <ffi:getProperty name="ToAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TransferType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempTransfer" property="StatusName"/></td>
                    <td><!--L10NStart-->One Time<!--L10NEnd--></td>
                    <td></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempTransfer" property="AmountValue.CurrencyString"/></td>
                </tr>
                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
            </table>
      </div></div></div>
    </ffi:cinclude>
    
    
    <ffi:removeProperty name="CollectionEmpty"/>
    <ffi:removeProperty name="CollectionSize1"/>
    <ffi:removeProperty name="CollectionSize2"/>
    <ffi:setProperty name="${RecTransfersCollectionName}" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
    <ffi:setProperty name="${TransfersCollectionName}" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
    <ffi:setProperty name="${RecTransfersCollectionName}" property="FilterOnFilter" value="STATUS==${TransferPendingStatus}"/>
    <ffi:setProperty name="${TransfersCollectionName}" property="FilterOnFilter" value="STATUS==${TransferPendingStatus}"/>
    <ffi:getProperty name="${RecTransfersCollectionName}" property="Size" assignTo="CollectionSize1"/>
    <ffi:getProperty name="${TransfersCollectionName}" property="Size" assignTo="CollectionSize2"/>
    <% notEmpty = false; %>
    <ffi:cinclude value1="${CollectionSize1}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <ffi:cinclude value1="${CollectionSize2}" value2="0" operator="notEquals">
        <% notEmpty = true; %>
    </ffi:cinclude>
    <%
        if( notEmpty ) {
            session.setAttribute( "CollectionEmpty", "false" );   
        }
    %>
   
    <ffi:cinclude value1="${CollectionEmpty}" value2="false" operator="equals"> <div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="paneContentWrapper"><div class="blockHead marginBottom10"><!-- L10NStart -->Pending Transfers<!-- L10NEnd --></div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData">
                <tr class="header">
                    <td style="padding-left: 5px;"><!-- L10NStart -->Date<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->From Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->To Account<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Type<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Status<!-- L10NEnd --></td>
                    <td><!-- L10NStart -->Frequency<!-- L10NEnd --></td>
                    <td style="padding-right: 0px; text-align: right;"><!-- L10NStart -->#<!-- L10NEnd --></td>
                    <td style="padding-right: 5px; text-align: right;"><!-- L10NStart -->Amount<!-- L10NEnd --></td>
                </tr>
        <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
        <ffi:setProperty name="Calculator" property="Value1" value="3"/>
        <ffi:setProperty name="Calculator" property="Value2" value="1"/>

        <ffi:setProperty name="${RecTransfersCollectionName}" property="FilterSortedBy" value="NEXTDATE,TRANSACTIONID"/>
        <ffi:setProperty name="${TransfersCollectionName}" property="FilterSortedBy" value="DATE,TRANSACTIONID"/>
        
        <ffi:list collection="${RecTransfersCollectionName}" items="TempRecTransfer">
                <ffi:setL10NProperty name="TransferType" value="Internal"/>
                <ffi:setL10NProperty name="FromAccountType" value=""/>
                <ffi:setL10NProperty name="ToAccountType" value=""/>

                <ffi:cinclude value1="${TempRecTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="ToAccountType" value="(E)"/>
                </ffi:cinclude>
                <ffi:cinclude value1="${TempRecTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="FromAccountType" value="(E)"/>
                </ffi:cinclude>
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempRecTransfer" property="NextDate"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="FromAccount.ConsumerDisplayText"/> <ffi:getProperty name="FromAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="ToAccount.ConsumerDisplayText"/> <ffi:getProperty name="ToAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TransferType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="StatusName"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempRecTransfer" property="Frequency"/></td>
                    <td style="padding-right: 0px; text-align: right;"><ffi:getProperty name="TempRecTransfer" property="RemainingTransfers"/></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempRecTransfer" property="AmountValue.CurrencyString"/></td>
                </tr>
                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
        <ffi:list collection="${TransfersCollectionName}" items="TempTransfer">
                <ffi:setL10NProperty name="TransferType" value="Internal"/>
                <ffi:setL10NProperty name="FromAccountType" value=""/>
                <ffi:setL10NProperty name="ToAccountType" value=""/>

                <ffi:cinclude value1="${TempTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="ToAccountType" value="(E)"/>
                </ffi:cinclude>
                <ffi:cinclude value1="${TempTransfer.TransferDestination}" value2='<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>' operator="equals">
                    <ffi:setL10NProperty name="TransferType" value="External"/>
                    <ffi:setL10NProperty name="FromAccountType" value="(E)"/>
                </ffi:cinclude>
                <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                    <td style="padding-left: 5px;"><ffi:getProperty name="TempTransfer" property="Date"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempTransfer" property="FromAccount.ConsumerDisplayText"/> <ffi:getProperty name="FromAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempTransfer" property="ToAccount.ConsumerDisplayText"/> <ffi:getProperty name="ToAccountType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TransferType"/></td>
                    <td style="padding-left: 0px;"><ffi:getProperty name="TempTransfer" property="StatusName"/></td>
                    <td><!--L10NStart-->One Time<!--L10NEnd--></td>
                    <td></td>
                    <td style="padding-right: 5px; text-align: right;"><ffi:getProperty name="TempTransfer" property="AmountValue.CurrencyString"/></td>
                </tr>
                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
        </ffi:list>
            </table>
</div></div></div>
    </ffi:cinclude>

    

    <ffi:removeProperty name="InputValue"/>
</ffi:cinclude>

<ffi:setProperty name="RecPayments" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="RecPayments" property="FilterOnFilter" value="STATUS==${PaymentPendingStatus},STATUS==${PaymentPendingApprovalStatus}"/>
<ffi:setProperty name="Payments" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="Payments" property="FilterOnFilter" value="STATUS==${PaymentPendingStatus},STATUS==${PaymentPendingApprovalStatus}"/>

<ffi:setProperty name="RecTransfers" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="RecTransfers" property="FilterOnFilter" value="STATUS==${TransferPendingStatus},STATUS==${TransferPendingApprovalStatus}"/>
<ffi:setProperty name="Transfers" property="Filter" value="SUBMITTED_BY==${SecondaryUserToModifyID}"/>
<ffi:setProperty name="Transfers" property="FilterOnFilter" value="STATUS==${TransferPendingStatus},STATUS==${TransferPendingApprovalStatus}"/>

<ffi:removeProperty name="PaymentPendingStatus"/>
<ffi:removeProperty name="IncludePendingTXNotice"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_TRANSACTIONS_TRANSFER_OR_CANCEL %>'/>
<ffi:removeProperty name="ExpiredRecPayments"/>
<ffi:removeProperty name="ExpiredPayments"/>
<ffi:removeProperty name="ExpiredRecTransfers"/>
<ffi:removeProperty name="ExpiredTransfers"/>
<ffi:removeProperty name="RecPaymentsCollectionName"/>
<ffi:removeProperty name="PaymentsCollectionName"/>
<ffi:removeProperty name="RecTransfersCollectionName"/>
<ffi:removeProperty name="TransfersCollectionName"/>
<ffi:removeProperty name="CollectionEmpty"/>
<ffi:removeProperty name="CollectionSize1"/>
<ffi:removeProperty name="CollectionSize2"/>
<ffi:removeProperty name="TransferType"/>

