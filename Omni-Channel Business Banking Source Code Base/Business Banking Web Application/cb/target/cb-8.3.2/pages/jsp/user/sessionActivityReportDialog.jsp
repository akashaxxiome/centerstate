<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script>
	$.subscribe('quickSessionOtherPreferences', function(event,data) {	
	if(event.originalEvent.request.status =='500'){		
		ns.common.showError(event.originalEvent.request.responseJSON.response.data);		
		ns.common.closeDialog("sessionActivityDialogId");
	}else{
		ns.common.showStatus($("#saveSessionActivityResultmessage.sessionActivitySuccess").html().trim());
		ns.common.closeDialog("sessionActivityDialogId");
		setTimeout(function(){
			window.location.hash = ns.home.currentMenuId;
			window.location.reload();
		},1000);
	}
	});
</script>

<ffi:help id="userprefs_misc"/>

<s:action namespace="/pages/jsp/user" name="QuickEditPreferenceAction_initSessionActivity" executeResult="true">
 </s:action>

<span id="saveSessionActivityResultmessage" style="display:none" class="sessionActivitySuccess">
	<s:text name="jsp.user_390"/>
</span> 

<div style="width:100%; margin:0 auto; text-align:center;" id="sessionActivityHolderDiv">
</div>
