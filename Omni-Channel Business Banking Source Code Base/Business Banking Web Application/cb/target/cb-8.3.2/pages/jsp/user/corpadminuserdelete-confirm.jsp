<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadminuserdelete-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_81')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<% if( request.getParameter("approveDelete") != null ){ session.setAttribute("approveDelete", request.getParameter("approveDelete")); }%>
<% if( request.getParameter("itemId") != null ){ session.setAttribute("itemId", request.getParameter("itemId")); }%>

<ffi:setProperty name="RemoveBusinessUser" property="PendingTransactionsURL" value=""/>

<%-- If dual approval mode is on, Deleting a user should call AddUserToDA task --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
<ffi:cinclude value1="${approveDelete}" value2="true" >
	<ffi:setProperty name="RemoveBusinessUser" property="DAApprove" value="true"/>
	<ffi:process name="RemoveBusinessUser"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${itemId}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="true" />
	<ffi:process name="ApprovePendingChangesTask"/>
</ffi:cinclude>	

</ffi:cinclude>

		<div align="center">

		<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center">
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"  operator="notEquals">
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuserdelete-confirm.jsp-1" parm0="${BusinessEmployee.FullName}"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<ffi:cinclude value1="${deleteApproved}" value2="true">
										<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuserdelete-confirm.jsp-3" parm0="${BusinessEmployee.FullName}"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${deleteApproved}" value2="true" operator="notEquals">
										<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuserdelete-confirm.jsp-2" parm0="${BusinessEmployee.FullName}"/>
									</ffi:cinclude>
								</ffi:cinclude>
							</TD>
						</TR>
						<TR>
							<TD align="center"  class="ui-widget-header customDialogFooter">
							<ffi:cinclude value1="${approveDelete}" value2="true" operator="notEquals">
								<sj:a id="closeDeleteUserConfirm" button="true"
									  onClickTopics="closeDeleteUserDialog" title="%{getText('Close_Window')}"
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_175" /></sj:a>
							</ffi:cinclude>		  
							<ffi:cinclude value1="${approveDelete}" value2="true">								
								<sj:a id="closeDeleteUserConfirm1" button="true"
									  onClickTopics="closeApprovalWizardDialog,refreshDAPendingUsersGrid" title="%{getText('Close_Window')}"
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_175" /></sj:a>
							</ffi:cinclude>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
			<p></p>
		</div>
<ffi:removeProperty name="approveDelete"/>
<ffi:removeProperty name="itemId"/>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminusers-pre.jsp"/>
