<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setL10NProperty name='PageHeading' value='Company Profile'/>

<%-- initializing if necessary --%>
<ffi:removeProperty name="GroupDivAdminEdited"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="modifiedAdmins"/>
<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>


<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>

<ffi:setProperty name='PageText' value=''/>

<%--get company profile info from the business bean--%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<%--get the company's BAI export preferences--%>
<ffi:object id="GetBAIExportSettings" name="com.ffusion.tasks.user.GetBAIExportSettings" scope="request"/>
<ffi:setProperty name="GetBAIExportSettings" property="BusinessSessionName" value="Business"/>
<ffi:process name="GetBAIExportSettings"/>

<%--get service package info from GetEntitlementGroup, which stores info in session under Entitlement_EntitlementGroup--%>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Business.ServicesPackageId}" />
<ffi:process name="GetEntitlementGroup"/>

<%--get contact using GetPrimaryBusinessEmployees, which stores info for primary contact in session under PrimaryBusinessEmployee--%>
<ffi:object id="GetPrimaryBusinessEmployees" name="com.ffusion.tasks.user.GetPrimaryBusinessEmployees" scope="session"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BankId" value="${Business.BankId}"/>
<ffi:process name="GetPrimaryBusinessEmployees"/>
<ffi:removeProperty name="GetPrimaryBusinessEmployees" />

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
<ffi:process name="GetBusinessEmployeesByEntGroups"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
 								
<%--get accounts info from GetAccountsByBusinessEmployee, stores results in session as BusEmpAccounts--%>
<ffi:object id="GetAccountsByBusinessEmployee" name="com.ffusion.tasks.accounts.GetAccountsByBusinessEmployee" scope="session"/>
	<ffi:setProperty name="GetAccountsByBusinessEmployee" property="AccountsName" value="BusEmpAccounts"/>
<ffi:process name="GetAccountsByBusinessEmployee"/>
<ffi:removeProperty name="GetAccountsByBusinessEmployee"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />

<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>


<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<!-- Dual Approval -->
<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="Business"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
<ffi:process name="GetDACategoryDetails" />

<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
	<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="Business"/>
<ffi:process name="PopulateObjectWithDAValues"/>
<ffi:setProperty name="editURL" value="${SecurePath}user/corpadminbusinessedit.jsp?goback=false" URLEncrypt="true"/>
<ffi:removeProperty name="editURL"/>
<div class="blockWrapper">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead adminBackground sectionLabel"><s:text name="jsp.user_201"/></span>
				<span class="sectionsubhead adminBackground"><ffi:getProperty name="Business" property="BusinessName"/></span>
			</div>
			<ffi:cinclude value1="${Business.ACH_IAT_UseTaxID}" value2="Y" operator="equals">
				<div class="inlineBlock">
					<span class="sectionsubhead adminBackground sectionLabel"><s:text name="jsp.user_303"/></span>
					<span class="sectionsubhead adminBackground"><ffi:getProperty name="Business" property="TaxId"/></span>
				</div>
            </ffi:cinclude>
		</div>
		<div class="blockRow">
			<ffi:list collection="CATEGORY_BEAN.DaItems" items="daItemRow">
				<ffi:cinclude value1="${daItemRow.FieldName}" value2="street" operator="equals">
					<ffi:setProperty name="highlightAddress" value="y" />
				</ffi:cinclude>
				<ffi:cinclude value1="${daItemRow.FieldName}" value2="street2" operator="equals">
					<ffi:setProperty name="highlightAddress" value="y" />
				</ffi:cinclude>
				<ffi:cinclude value1="${daItemRow.FieldName}" value2="city" operator="equals">
					<ffi:setProperty name="highlightAddress" value="y" />
				</ffi:cinclude>
				<ffi:cinclude value1="${daItemRow.FieldName}" value2="state" operator="equals">
					<ffi:setProperty name="highlightAddress" value="y" />
				</ffi:cinclude>
				<ffi:cinclude value1="${daItemRow.FieldName}" value2="zipCode" operator="equals">
					<ffi:setProperty name="highlightAddress" value="y" />
				</ffi:cinclude>
				<ffi:cinclude value1="${daItemRow.FieldName}" value2="country" operator="equals">
					<ffi:setProperty name="highlightAddress" value="y" />
				</ffi:cinclude>
			</ffi:list>
			<ffi:cinclude value1="${highlightAddress}" value2="y" operator="equals">
			<span class="sectionheadDA sectionLabel"><s:text name="jsp.default_35"/>:</span>
			</ffi:cinclude>
			<ffi:cinclude value1="${highlightAddress}" value2="y" operator="notEquals">
			<span class="sectionhead sectionLabel"><s:text name="jsp.default_35"/>:</span>
			</ffi:cinclude>
			<ffi:removeProperty name="highlightAddress" />
			<span class="sectionsubhead"><ffi:getProperty name="Business" property="Street"/>,
				<ffi:cinclude value1="${Business.Street2}" value2="" operator="notEquals">
					<ffi:getProperty name="Business" property="Street2"/>,
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.City}" value2="" operator="notEquals">
					<ffi:getProperty name="Business" property="City"/>,
				</ffi:cinclude>
						<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Business.Country}" />
						<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Business.State}" />
				<ffi:process name="GetStateProvinceDefnForState"/>
				<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
					<ffi:getProperty name="Business" property="State"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
					<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.State}" value2="" operator="notEquals">
				  <ffi:cinclude value1="${Business.ZipCode}" value2="" operator="notEquals">
				  ,
				  </ffi:cinclude>
				</ffi:cinclude>
				<ffi:getProperty name="Business" property="ZipCode"/>
				<ffi:cinclude value1="${Business.State}${Business.ZipCode}" value2="" operator="notEquals">
				</ffi:cinclude>
				<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Business.Country}" />
				<ffi:getProperty name='CountryResource' property='Resource'/>
					<span class="sectionhead_greyDA">
						<ffi:cinclude value1="${oldDAObject.Street}" value2="" operator="notEquals">
						<ffi:getProperty name="oldDAObject" property="Street"/>,
						</ffi:cinclude>
						<ffi:cinclude value1="${oldDAObject.Street2}" value2="" operator="notEquals">
							<ffi:getProperty name="oldDAObject" property="Street2"/>,
						</ffi:cinclude>
						<ffi:cinclude value1="${oldDAObject.City}" value2="" operator="notEquals">
							<ffi:getProperty name="oldDAObject" property="City"/>,
						</ffi:cinclude>
  						<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${oldDAObject.Country}" />
  						<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${oldDAObject.State}" />
						<ffi:process name="GetStateProvinceDefnForState"/>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
							<ffi:getProperty name="oldDAObject" property="State"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
							<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${oldDAObject.State}" value2="" operator="notEquals">
						  <ffi:cinclude value1="${oldDAObject.ZipCode}" value2="" operator="notEquals">
						  ,
						  </ffi:cinclude>
						</ffi:cinclude>
						<ffi:getProperty name="oldDAObject" property="ZipCode"/>
						<ffi:cinclude value1="${oldDAObject.State}${oldDAObject.ZipCode}" value2="" operator="notEquals">
						  
						</ffi:cinclude>
						<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${oldDAObject.Country}" />
						<ffi:getProperty name='CountryResource' property='Resource'/>
					</span>
			</span>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class='<ffi:getPendingStyle fieldname="phone" defaultcss="sectionhead"  dacss="sectionheadDA"/> sectionLabel' ><s:text name="jsp.user_252"/></span>
				<span class="sectionsubhead"><ffi:getProperty name="Business" property="Phone"/>
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Phone"/></span>
				</span>
			</div>
			<div class="inlineBlock">
				<span class='<ffi:getPendingStyle fieldname="faxPhone" defaultcss="sectionhead"  dacss="sectionheadDA"/> sectionLabel'><s:text name="jsp.user_Fax"/></span>
				<span class="sectionsubhead"><ffi:getProperty name="Business" property="FaxPhone"/><span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="FaxPhone"/></span></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class='<ffi:getPendingStyle fieldname="email" defaultcss="sectionhead"  dacss="sectionheadDA"/> sectionLabel'><s:text name="jsp.user_Email"/></span>
				<span class="sectionsubhead"><ffi:getProperty name="Business" property="Email" /><span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Email"/></span></span>
			</div>
			<div class="inlineBlock">
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="adminBackground sectionsubhead sectionLabel"><s:text name="jsp.user_296"/>:</span>
				<span class="sectionsubhead"><ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="adminBackground sectionsubhead sectionLabel"><s:text name="jsp.user_297"/>:</span>
				<ffi:setProperty name="Business" property="DateFormat" value="${UserLocale.DateFormat}" />  
				<span class="sectionsubhead"><ffi:getProperty name="Business" property="ActivationDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="adminBackground sectionsubhead sectionLabel"><s:text name="jsp.user_249"/>:</span>
				<span class="sectionsubhead" >
					<ffi:cinclude value1="${Business.PersonalBanker}" value2="0" operator="notEquals">
                 <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmininfo.jsp-1" parm0="${AccountRep.FullName}" />
						<ffi:cinclude value1="${PersonalBanker.Phone}" value2="" operator="notEquals">, <s:text name="jsp.user_304"/>: <ffi:getProperty name="PersonalBanker" property="Phone"/></ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${Business.PersonalBanker}" value2="0" operator="equals">
			         		<s:text name="jsp.default_296"/>
					</ffi:cinclude>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="adminBackground sectionsubhead sectionLabel">
					<s:text name="jsp.user_8"/>:
				</span>
				<span class="sectionsubhead" >
					<ffi:cinclude value1="${Business.AccountRep}" value2="0" operator="notEquals">
						<ffi:cinclude value1="${AccountRep.Phone}" value2="" operator="notEquals">, <s:text name="jsp.user_304"/>: <ffi:getProperty name="AccountRep" property="Phone"/></ffi:cinclude>											
						<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmininfo.jsp-1" parm0="${AccountRep.FullName}" />
						<ffi:cinclude value1="${AccountRep.Phone}" value2="" operator="notEquals">, <s:text name="jsp.user_304"/>: <ffi:getProperty name="PersonalBanker" property="Phone"/></ffi:cinclude>											
					</ffi:cinclude>
					<ffi:cinclude value1="${Business.AccountRep}" value2="0" operator="equals">
			         		<s:text name="jsp.default_296"/>
					</ffi:cinclude>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
<sj:a button="true" title="Close" onClickTopics="closeDialog,closeViewProfileDialog"><s:text name="jsp.default_175" /></sj:a>
</div>
	<ffi:flush/>
	<!-- Restore Business Employee in session to the original values -->
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="Business"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="restoredObject" value="y"/>
 	<ffi:process name="PopulateObjectWithDAValues"/>

<ffi:setProperty name="onCancelGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp"/>
<ffi:removeProperty name="BusEmpAccounts" />
<ffi:removeProperty name="BusinessEmployees" />
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<ffi:removeProperty name="DAITEM"/>