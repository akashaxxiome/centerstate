<%--
| The purpose of this page is to initialize the FeatureAccessAndLimits task with the secondary user's current
| permissions for the internal transfers, external transfers, and bill payment administration pages. Inputs that this
| page expects are:
|
|       - the session variable called "IncludeFromSection" set to "TRUE" if and only if the "From ..." permissions
|         should be included
|       - the session variable called "IncludeToSection" set to "TRUE" if and only if the "To ..." permissions should
|         be included
|       - the session variable called "AccountsCollectionNames" set to a StringList of session names of Accounts
|         collections to which the corresponding collections of accounts the user can be granted access to for the
|         purposes of initiating transactions of the given operation type(s) is bound
|       - the session variable called "FromOperationName" set to the entitlement operation to include in the names of
|         the form input elements in the "From ..." section
|       - the session variable called "ToOperationName" set to the entitlement operation to include in the names of the
|         form input elements in the "To ..." section
|       - the session variable called "AccountsFromFilters" set to a table of filters to be applied against each
|         individual Accounts bean. Each account in a particular collection must satisfy the corresponding filter in
|         order to be included in the list of candidate accounts in the "From ..." section of the page. The key of each
|         pair in the table is the session name of the collection to which the value, i.e., the filter, will be applied.
|       - the session variable called "AccountsToFilters" set to a table of filters to be applied against each
|         individual Accounts bean. Each account in a particular collection must satisfy the corresponding filter in
|         order to be included in the list of candidate accounts in the "To ..." section of the page. The key of each
|         pair in the table is the session name of the collection to which the value, i.e., the filter, will be applied.
|       - the session variable called "FeatureAccessAndLimitsTaskName" set to the session name of the
|         FeatureAccessAndLimits task used to process and persist the permissions settings
--%>


<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>' startsWith="true"/>

<%-- Create the task used to generate entitlement object identifiers for accounts. --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetAccountEntitlementObjectID" scope="request"/>


<%-- Get the secondary user's immediate entitlement restrictions. --%>
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" id="GetRestrictedSecondaryUserEntitlements" scope="request"/>
<ffi:setProperty name="GetRestrictedSecondaryUserEntitlements" property="GroupId" value="${SecondaryUser.EntitlementGroupId}"/>
<ffi:setProperty name="GetRestrictedSecondaryUserEntitlements" property="MemberType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ENT_MEMBER_TYPE_USER %>'/>
<ffi:setProperty name="GetRestrictedSecondaryUserEntitlements" property="MemberSubType" value='<%= "" + com.ffusion.beans.SecureUser.TYPE_CUSTOMER %>'/>
<ffi:setProperty name="GetRestrictedSecondaryUserEntitlements" property="MemberId" value="${SecondaryUser.Id}"/>
<ffi:process name="GetRestrictedSecondaryUserEntitlements"/>


<%-- Initialize the task used to get the secondary user's immediate limits. --%>
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetGroupLimits" id="GetSecondaryUserLimits" scope="request"/>
<ffi:setProperty name="GetSecondaryUserLimits" property="GroupId" value="${SecondaryUser.EntitlementGroupId}"/>
<ffi:setProperty name="GetSecondaryUserLimits" property="MemberType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ENT_MEMBER_TYPE_USER %>'/>
<ffi:setProperty name="GetSecondaryUserLimits" property="MemberSubType" value='<%= "" + com.ffusion.beans.SecureUser.TYPE_CUSTOMER %>'/>
<ffi:setProperty name="GetSecondaryUserLimits" property="MemberId" value="${SecondaryUser.Id}"/>
<ffi:setProperty name="GetSecondaryUserLimits" property="AllowApproval" value="FALSE"/>
<ffi:setProperty name="GetSecondaryUserLimits" property="ObjectType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>


<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="Reset" value=""/>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddLimitPeriodToList" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddLimitPeriodToList" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>

<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup" id="CheckParentEntitlements" scope="request"/>
<ffi:setProperty name="CheckParentEntitlements" property="AttributeName" value="IsParentEntitled"/>

<ffi:cinclude value1="${IncludeFromSection}" value2="TRUE" operator="equals">
    <ffi:setProperty name="GetSecondaryUserLimits" property="OperationName" value="${FromOperationName}"/>
    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
        <ffi:setProperty name="AccountsFromFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsFromFilters.Value}"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="CheckParentEntitlements" property="GroupId" value="${SecureUser.EntitlementID}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="CheckParentEntitlements" property="OperationName" value="${FromOperationName}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:process name="CheckParentEntitlements"/>

            <ffi:cinclude value1="${IsParentEntitled}" value2="TRUE" operator="equals">
                <ffi:setProperty name="IsRestrictedObject" value=""/>

                <ffi:list collection="Entitlement_Restricted_list" items="RestrictedEntitlement">
                    <ffi:cinclude value1="${RestrictedEntitlement.OperationName}" value2="${FromOperationName}" operator="equals">
                        <ffi:cinclude value1="${RestrictedEntitlement.ObjectType}" value2='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>' operator="equals">
                            <ffi:cinclude value1="${RestrictedEntitlement.ObjectId}" value2="${GetAccountEntitlementObjectID.ObjectID}" operator="equals">
                                <ffi:setProperty name="IsRestrictedObject" value="true"/>
                            </ffi:cinclude>
                        </ffi:cinclude>
                    </ffi:cinclude>
                </ffi:list>

                <ffi:cinclude value1="${IsRestrictedObject}" value2="true" operator="notEquals">
                    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddObjectIDToList" value='${GetAccountEntitlementObjectID.ObjectID}'/>
                    <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
                    <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
                    <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${FromOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>

                    <ffi:setProperty name="${FAALBaseName}" value="true"/>

                    <ffi:setProperty name="GetSecondaryUserLimits" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>

                    <ffi:setProperty name="GetSecondaryUserLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                    <ffi:process name="GetSecondaryUserLimits"/>

                    <ffi:setProperty name="FAALLimitName" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                    <ffi:setProperty name="FAALLimitName" value='${FAALBaseName}!${FAALLimitName}'/>

                    <%-- There should only be one matching limit so this loop should only ever iterate once. --%>
                    <ffi:list collection="Entitlement_Limits" items="TempLimit">
                        <ffi:setProperty name="${FAALLimitName}" value="${TempLimit.Data}"/>
                    </ffi:list>

                    <ffi:setProperty name="GetSecondaryUserLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                    <ffi:process name="GetSecondaryUserLimits"/>

                    <ffi:setProperty name="FAALLimitName" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                    <ffi:setProperty name="FAALLimitName" value='${FAALBaseName}!${FAALLimitName}'/>

                    <%-- There should only be one matching limit so this loop should only ever iterate once. --%>
                    <ffi:list collection="Entitlement_Limits" items="TempLimit">
                        <ffi:setProperty name="${FAALLimitName}" value="${TempLimit.Data}"/>
                    </ffi:list>
                </ffi:cinclude>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
</ffi:cinclude>


<ffi:cinclude value1="${IncludeToSection}" value2="TRUE" operator="equals">
    <ffi:setProperty name="GetSecondaryUserLimits" property="OperationName" value="${ToOperationName}"/>
    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
        <ffi:setProperty name="AccountsToFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsToFilters.Value}"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="CheckParentEntitlements" property="GroupId" value="${SecureUser.EntitlementID}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="CheckParentEntitlements" property="OperationName" value="${ToOperationName}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:process name="CheckParentEntitlements"/>

            <ffi:cinclude value1="${IsParentEntitled}" value2="TRUE" operator="equals">
                <ffi:setProperty name="IsRestrictedObject" value=""/>

                <ffi:list collection="Entitlement_Restricted_list" items="RestrictedEntitlement">
                    <ffi:cinclude value1="${RestrictedEntitlement.OperationName}" value2="${ToOperationName}" operator="equals">
                        <ffi:cinclude value1="${RestrictedEntitlement.ObjectType}" value2='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>' operator="equals">
                            <ffi:cinclude value1="${RestrictedEntitlement.ObjectId}" value2="${GetAccountEntitlementObjectID.ObjectID}" operator="equals">
                                <ffi:setProperty name="IsRestrictedObject" value="true"/>
                            </ffi:cinclude>
                        </ffi:cinclude>
                    </ffi:cinclude>
                </ffi:list>

                <ffi:cinclude value1="${IsRestrictedObject}" value2="true" operator="notEquals">
                    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddObjectIDToList" value='${GetAccountEntitlementObjectID.ObjectID}'/>
                    <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
                    <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
                    <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${ToOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>

                    <ffi:setProperty name="${FAALBaseName}" value="true"/>

                    <ffi:setProperty name="GetSecondaryUserLimits" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>

                    <ffi:setProperty name="GetSecondaryUserLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                    <ffi:process name="GetSecondaryUserLimits"/>

                    <ffi:setProperty name="FAALLimitName" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                    <ffi:setProperty name="FAALLimitName" value='${FAALBaseName}!${FAALLimitName}'/>

                    <%-- There should only be one matching limit so this loop should only ever iterate once. --%>
                    <ffi:list collection="Entitlement_Limits" items="TempLimit">
                        <ffi:setProperty name="${FAALLimitName}" value="${TempLimit.Data}"/>
                    </ffi:list>

                    <ffi:setProperty name="GetSecondaryUserLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                    <ffi:process name="GetSecondaryUserLimits"/>

                    <ffi:setProperty name="FAALLimitName" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                    <ffi:setProperty name="FAALLimitName" value='${FAALBaseName}!${FAALLimitName}'/>

                    <%-- There should only be one matching limit so this loop should only ever iterate once. --%>
                    <ffi:list collection="Entitlement_Limits" items="TempLimit">
                        <ffi:setProperty name="${FAALLimitName}" value="${TempLimit.Data}"/>
                    </ffi:list>
                </ffi:cinclude>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
</ffi:cinclude>


<%-- Initialize the FeatureAccessAndLimits task used to process the permissions settings. --%>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="Process" value="FALSE"/>
<ffi:process name="${FeatureAccessAndLimitsTaskName}"/>

<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>' startsWith="true"/>
<ffi:removeProperty name="SessionPrefix"/>
<ffi:removeProperty name="AccountEntitlementObject"/>
<ffi:removeProperty name="FAALBaseName"/>
<ffi:removeProperty name="FAALLimitName"/>
<ffi:removeProperty name="IsParentEntitled"/>
<ffi:removeProperty name="IsRestrictedObject"/>
