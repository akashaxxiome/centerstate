<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="java.util.Locale"%>
<%@page import="com.ffusion.util.UserLocaleConsts"%>
<%@page import="com.ffusion.beans.user.UserLocale"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>


<%-- <style>
	#requireTextID { margin-left: 190px; }
	#userprofileButtonID { margin-left: 190px; }
</style> --%>

<style>
input[type="text"],input[type="password"] {
 width:265px;
 }
</style>

<ffi:help id="userprefspersonal" className="moduleHelpClass"/>
<script type="text/javascript" src="<s:url value='/web/js/userPreferences/userPreferences%{#session.minVersion}.js'/>"></script>

<script>
function showStateForProfile(obj) {
	var urlString = '/cb/pages/jsp/user/ModifyUserProfileAction_getStatesForSelectedCountry.action';
	$.ajax({
		   type: 'POST',
		   url: urlString,
		   data: "countryCode=" + obj.value + "&CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>',
		   success: function(data){
		    if(data.indexOf("option") != -1) {
		    	$("#stateNameIdForProfile").html('<span class="<ffi:getPendingStyle fieldname="state" defaultcss="greenClayBackground" dacss="sectionheadDA"/>"><s:text name="jsp.default_386"/>:</span>');
		    } else {
		    	$("#stateNameIdForProfile").html('<span></span>');
		    }
		   	$("#userprofileStateSelectId").html(data);
		   	$("#stateForUserSelectID-menu").css("width", '23em');
		   	$("#StateError").html('');
	   }
	});
}
</script>

<s:include value="/pages/jsp/user/password-indicator-js.jsp" />
<s:include value="/pages/jsp/user/corpadmin_js.jsp" />

<ffi:setProperty name="profileSubMenu" value="true"/>

<ffi:setProperty name="disableEdit" value="" />
<ffi:cinclude value1="${isBaSBackendEnable}" value2="true" operator="equals">	
	<ffi:setProperty name="disableEdit" value="disabled" />
</ffi:cinclude>

<ffi:cinclude value1="${DontRefresh}" value2="TRUE" operator="notEquals">	
	<s:set var="UserInfoForEdit" value="%{#session.User}"/>
</ffi:cinclude>

<ffi:cinclude value1="${DontRefresh}" value2="TRUE" operator="equals">
	<s:set var="UserInfoForEdit" value="%{#session.ModifyUser}"/>
</ffi:cinclude>

<ffi:removeProperty name="StateReloadOnly"/>

<%-------------------- Always load default blank passfields ---%>
<ffi:setProperty name="CurrentPassword" value=""/>
<ffi:setProperty name="NewPassword" value=""/>
<ffi:setProperty name="ConfirmPassword" value=""/>
<ffi:setProperty name="ConfirmPasswordReminder" value=""/>
<ffi:setProperty name="ConfirmPasswordReminder2" value=""/>
<%-----------------------------------------------------------------------------------------------------------%>

<%-------------------- Get the appropriate resources files containing the PasswordQuestions -----------------%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResource" scope="session"/>
    <ffi:setProperty name="UserResource" property="ResourceFilename" value="com.ffusion.beansresources.user.resources" />
<ffi:process name="UserResource"/>


<ffi:object name="com.ffusion.tasks.util.ResourceList" id="UserResourceList" scope="session"/>
    <ffi:setProperty name="UserResourceList" property="ResourceFilename" value="com.ffusion.beansresources.user.resources" />
<ffi:process name="UserResourceList" />

<%
	//This code is needed to switch locale to get english value for chinese text of password questions
	Locale englishLocale = new Locale("en", "US");
	com.ffusion.tasks.util.Resource userResourceTemp = (com.ffusion.tasks.util.Resource)session.getAttribute("UserResource");
	UserLocale userLocale = (UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE);
	Locale originalLocale = userLocale.getLocale();
%>
<%---------------------------------------------------------------------------------------------------------%>

  <div id= "userprofileDesktop" align="center" class="dialogHtDvc">
	<%-- <div id="operationresult">
		<div id="resultmessage"><s:text name="jsp.default_498"/></div>
	</div> --%><!-- result -->
  
  <ffi:setProperty name="subMenuSelected" value="user profile"/>

  <s:form id="UserProfileForm" name="UserProfileForm" action="ModifyUserProfileAction_verify" method="post"  namespace="/pages/jsp/user" theme="simple" autocomplete="OFF">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input value="<s:property value="%{isQuickSearch}"/>" name="isQuickSearch" type="hidden">
		<table align="center"  width="100%" border="0" cellspacing="0" cellpadding="0" style="font-weight:normal" class="tableData">
		<tr>
			<td colspan="3" align="center">
				<ul id="formerrors"></ul>
			</td>
		</tr>			
		<tr>
			<td align="left" width="49%">
				<table align="left" width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:20px;" class="marginLeft-0 updateProfTablLeft">
					 <tr>
						<td class="greenClayBackground" align="left" valign="baseline" ><s:text name="jsp.default_577"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left" >
							<!-- In case of consumer, Username is editable. In Corporate, it is shown as read only field. -->
							<s:if test="%{#request.IsConsumerUser}"> 
								<input class="ui-widget-content ui-corner-all" type="text" name="UserName" value="<ffi:getProperty name="UserName"/>" size="32" maxlength="20" border="4">
							</s:if>
							<s:else>
								<ffi:getProperty name="UserName"/>
							</s:else>							
						</td>	    
						<!-- <td colspan class="greenClayBackground" valign="baseline" align="left">
						</td> -->
					  </tr>
					  <s:if test="%{#request.IsConsumerUser}"> 
						  <tr>
							<td class="greenClayBackground" align="left" valign="baseline"></td>
							<td><span id="userNameError"></span></td>
						  </tr>
					  </s:if>
					<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_162"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="FirstName" value="<ffi:getProperty name="FirstName"/>" size="32" maxlength="35" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="firstNameError"></span></td>
					  </tr>
					</ffi:cinclude>
					<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
					<tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_186"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="LastName" value="<ffi:getProperty name="LastName"/>" size="32" maxlength="35" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="lastNameError"></span></td>
					  </tr>
					</ffi:cinclude>
					<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
					<tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_201"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="LastName" value="<ffi:getProperty name="FullName"/>" size="32" maxlength="35" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="lastNameError"></span></td>
					  </tr>
					</ffi:cinclude>
					 <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_31"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="Street" value="<ffi:getProperty name="Street"/>" size="32" maxlength="40" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="streetError"></span></td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_30"/></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="Street2" value="<ffi:getProperty name="Street2"/>" size="32" maxlength="40" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="street2Error"></span></td>
					  </tr>
					<tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_65"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="City" value="<ffi:getProperty name="City"/>" size="32" maxlength="20" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					</tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="cityError"></span></td>
					  </tr>
					<%
						String userCountry = null;
						String userState = null;
					%>
							  
					<!-- default selected country and state -->
					<ffi:getProperty name="Country" assignTo="userCountry"/>
					<% if (userCountry == null) { userCountry = ""; } %>
					<ffi:getProperty name="State" assignTo="userState"/>
					<% if (userState == null) { userState = ""; } %>					
							  
					<ffi:cinclude value1="${StatesExists}" value2="true" operator="equals">  
					  <tr>
						<td class="greenClayBackground">
								<div align="left" id="stateNameIdForProfile"><span class='<ffi:getPendingStyle fieldname="state" defaultcss="sectionhead"  dacss="sectionheadDA"/>'><s:text name="jsp.default_386"/>:</span></div>
						</td>
						<td class="columndata" valign="baseline" align="left">
								<%-- The resource filename and ID used for the abbreviated state names which will be the values stored.
								These properties are also used to verify the state values to be stored;
								the values are needed in userprefspersonal-confirm.jsp for the EditUser task (with id "ModifyUser")
								to enable the verification. --%>
							<ffi:setProperty name="StateValidationResourceFile" value="com.ffusion.utilresources.states"/>
							<ffi:setProperty name="StateValidationResourceName" value="StateList"/>
							<div id="userprofileStateSelectId">
								<select id="stateForUserSelectID" class="txtbox" name="State" size="1" <ffi:getProperty name="disableEdit"/> >
									<option<ffi:cinclude value1="<%= userState %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
									<ffi:list collection="StateList" items="item">
										<option <ffi:cinclude value1="<%= userState %>" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
									</ffi:list>
								</select>
								<input type="hidden" name="stateHidden" value="false"/>
							</div>
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="stateError"></span></td>
					  </tr>
					</ffi:cinclude>
					<ffi:cinclude value1="${StatesExists}" value2="true" operator="notEquals">
						<!-- <tr height="18">
							<td class="columndata" align="left" valign="baseline"><br/></td>
							<td class="columndata" valign="baseline" align="left"><br/></td>
						</tr> -->
						<ffi:setProperty name="State" value=""/>
					</ffi:cinclude>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_391"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="ZipCode" value="<ffi:getProperty name="ZipCode"/>" size="32" maxlength="11" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="zipCode.zipCodeError"></span></td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_97"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
								<%-- The resource filename and ID used for the abbreviated country names which will be the values stored.
								These properties are also used to verify the country values to be stored;
								the values are needed in userprefspersonal-confirm.jsp for the EditUser task (with id "ModifyUser")
								to enable the verification.--%>							
							<ffi:setProperty name="CountryValidationResourceFile" value="com.ffusion.utilresources.states"/>
							<ffi:setProperty name="CountryValidationResourceName" value="CountryList"/>

							<ffi:setProperty name="tmpURL" value="${SecurePath}userprefspersonal.jsp?StateReloadOnly=true&pageRefresh=true" URLEncrypt="true"/>
							<select id="countrySelectID" class="txtbox" name="Country" size="1" onchange="showStateForProfile(this);" <ffi:getProperty name="disableEdit"/> >
								<option<ffi:cinclude value1="<%= userCountry %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.user_283"/></option>
								<ffi:list collection="CountryList" items="item">
									<option <ffi:cinclude value1="<%= userCountry %>" value2="${item.CountryCode}">selected</ffi:cinclude> 
									value="<ffi:getProperty name="item" property="CountryCode"/>"><ffi:getProperty name='item' property='Name'/></option>
								</ffi:list>
							</select>
							<ffi:removeProperty name="tmpURL"/>
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="countryError"></span></td>
					  </tr>
				
					<ffi:cinclude value1="${LanguagesList.size}" value2="1" operator="notEquals">
						<tr>
							<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_262"/></td>
							<td class="columndata" valign="baseline" align="left">
								<select id="languageSelectID" name="PreferredLanguage" class="txtbox" >
									<ffi:list collection="LanguagesList" items="language">
										<option value="<ffi:getProperty name="language" property="Language" />" <ffi:cinclude value1="${language.Language}" value2="${preferredLanguage}" operator="equals" >selected</ffi:cinclude> ><ffi:getProperty name="language" property="DisplayName" /></option>
									</ffi:list>
								</select>
							</td>
						</tr>
						<tr>
							<td class="greenClayBackground" align="left" valign="baseline"></td>
							<td><span id="preferredLanguageError"></span></td>
						</tr>
					</ffi:cinclude>
					<tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_252"/><span class="required">*</span></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="Phone" value="<ffi:getProperty name="Phone"/>" size="32" maxlength="14" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="phone.phoneError"></span><span id="phoneError"></span></td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_157"/></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="FaxPhone" value="<ffi:getProperty name="FaxPhone"/>" size="32" maxlength="14" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="faxPhoneError"></span></td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.default_558"/></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="DataPhone" value="<ffi:getProperty name="DataPhone"/>" size="32" maxlength="14" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="dataPhoneError"></span></td>
					  </tr>
					  <tr>
						<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_133"/></td>
						<td class="columndata" valign="baseline" align="left">
						<input class="ui-widget-content ui-corner-all" type="text" name="Email" value="<ffi:getProperty name="Email"/>" size="32" maxlength="40" border="4" <ffi:getProperty name="disableEdit"/> >
						</td>
					</tr>
					<tr>
						<td class="greenClayBackground" align="left" valign="baseline"></td>
						<td><span id="email.emailError"></span></td>
					</tr>
				</table>
			</td>			
			
			<td align="left" valign="top" width="49%">
				 <table align="left" width="100%" border="0" cellspacing="0" cellpadding="0" style="font-weight:normal;" class="updateProfTablLeft">					 
					
				  <tr>
					<td>&nbsp;</td>
					<td class="columndata" valign="middle" align="left">
						<ffi:setProperty name="useBCsettings" value="false"/>
						<ffi:setProperty name="changeOwnPassword" value="true"/>
						<%-- <s:include value="/pages/jsp-ns/common/password-strength-text.jsp" /> --%>
						<ffi:removeProperty name="changeOwnPassword"/>
						<ffi:removeProperty name="useBCsettings"/>
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_99"/></td>
					<td class="columndata" valign="baseline" align="left">
					<div style="width:242px;">
					<input class="ui-widget-content ui-corner-all" type="password" name="CurrentPassword" size="32" maxlength="15" border="4">
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="currentPasswordError"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_202"/></td>
					<td class="columndata" valign="baseline" align="left">
					<div style="width:270px;">
					<input id="newPasswordBox" class="ui-widget-content ui-corner-all inputPassword" type="password" name="NewPassword" size="32" maxlength="<%= String.valueOf(com.ffusion.csil.handlers.util.SignonSettings.getMaxPasswordLength())%>" border="4" onchange="evalPswd(this.value);" onkeyup="evalPswd(this.value);">
					</div>
					</td>
				  </tr>
				  <tr>
				  	<td></td>
				  	<td>
				  		<span style="width:23em; display:inline-block"><s:include value="/pages/jsp-ns/common/password-strength-text.jsp" /></span>
				  	</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="passwordError"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline" nowrap><s:text name="jsp.user_91"/></td>
					<td class="columndata" valign="baseline" align="left">
					<input class="ui-widget-content ui-corner-all" type="password" name="NewConfirmPassword" size="32" maxlength="<%= String.valueOf(com.ffusion.csil.handlers.util.SignonSettings.getMaxPasswordLength())%>" border="4">
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="confirmPasswordError"></span></td>
				  </tr>
				  <tr>
				  <td class="greenClayBackground" align="left" valign="baseline" nowrap></td>
				  <td><span id="missPasswordError"></span></td>
				  </tr>
				   <%-- <tr>
					<td class="greenClayBackground" align="left" valign="baseline" nowrap><s:text name="jsp.user_234"/></td>
					<td class="columndata" valign="baseline"><s:include value="/pages/jsp/user/password-strength-indicator-bar.jsp"/></td>
				  </tr> --%>
				  <tr>
					<td>&nbsp;</td>
					<td class="columndata" valign="middle" align="left">
					<s:text name="jsp.user_175"/>
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.default_567"/><span class="required">*</span></td>
					<td class="columndata" valign="baseline" align="left" nowrap>
						 <select id="passwordClueSelectID" class="txtbox" name="PasswordClue" > 
								<option value=""><s:text name="jsp.default_571"/></option>
								<ffi:setProperty name="UserResourceList" property="ResourceID" value="PWD_QUESTION_LIST" />
								<ffi:getProperty name="UserResourceList" property="Resource" />
								<ffi:list collection="UserResourceList" items="item" >
									<ffi:setProperty name="UserResource" property="ResourceID" value="PWD_QUESTION.${item}" />
									<%
										userResourceTemp.setLocale(englishLocale); //switch locale to get english value
									%>
									<ffi:cinclude value1="${UserResource.Resource}" value2="${PasswordClue}" operator="equals">
										<option value="<ffi:getProperty name="UserResource" property="Resource"/>" selected ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
									</ffi:cinclude>
									<ffi:cinclude value1="${UserResource.Resource}" value2="${PasswordClue}" operator="notEquals">
										<option value="<ffi:getProperty name="UserResource" property="Resource"/>" ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
									</ffi:cinclude>
								</ffi:list>
						</select>
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="passwordClueError"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.default_565"/></td>
					<td class="columndata" valign="baseline" align="left">
					<input class="ui-widget-content ui-corner-all" type="password" name="NewPasswordReminder" size="32" maxlength="40" border="4">
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="passwordReminderError"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.default_556"/></td>
					<td class="columndata" valign="baseline" align="left">
					<input class="ui-widget-content ui-corner-all" type="password" name="ConfirmNewPasswordReminder" size="32" maxlength="40" border="4">
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="confirmNewPasswordReminderError"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.default_568"/><span class="required">*</span></td>
					<td class="columndata" valign="baseline" align="left" nowrap>
						 <select id="passwordClue2SelectID" class="txtbox" name="PasswordClue2" > 
								<option value=""><s:text name="jsp.default_571"/></option>
								<ffi:setProperty name="UserResourceList" property="ResourceID" value="PWD_QUESTION_LIST" />
								<ffi:getProperty name="UserResourceList" property="Resource" />
								<ffi:list collection="UserResourceList" items="item" >
									<ffi:setProperty name="UserResource" property="ResourceID" value="PWD_QUESTION.${item}" />
									<%
										userResourceTemp.setLocale(englishLocale); //switch locale to get english value
									%>
									<ffi:cinclude value1="${UserResource.Resource}" value2="${PasswordClue2}" operator="equals">
									<option value="<ffi:getProperty name="UserResource" property="Resource"/>" selected ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
									</ffi:cinclude>
									<ffi:cinclude value1="${UserResource.Resource}" value2="${PasswordClue2}" operator="notEquals">
									<option value="<ffi:getProperty name="UserResource" property="Resource"/>" ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
									</ffi:cinclude>
					  			</ffi:list>
							</select>
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="passwordClue2Error"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline" nowrap><s:text name="jsp.default_566"/></td>
					<td class="columndata" valign="baseline" align="left">
					<input class="ui-widget-content ui-corner-all" type="password" name="NewPasswordReminder2" size="32" maxlength="40" border="4">
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="passwordReminder2Error"></span></td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline" nowrap><s:text name="jsp.default_557"/></td>
					<td class="columndata" valign="baseline" align="left">
					<input class="ui-widget-content ui-corner-all" type="password" name="ConfirmNewPasswordReminder2" size="32" maxlength="40" border="4">
					</td>
				  </tr>
				  <tr>
					<td class="greenClayBackground" align="left" valign="baseline"></td>
					<td><span id="confirmNewPasswordReminder2Error"></span></td>
				  </tr>
				  <tr> 
					<td colspan="2">&nbsp;</td>
				  </tr>				
				</table> 
			</td>
		</tr>
		<tr>
			<td colspan="3">
		
		<table align="left" width="100%" border="0" cellspacing="0" cellpadding="0" style="font-weight:normal">
		  <tr>
				<td align="center" class="greenClayBackground">
				<span id="requireTextID" class="required">* <s:text name="jsp.default_240"/></span>
				</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
		  </tr>
		  <tr>		  
			<td class="columndata" align="center" valign="top">
			<div id="userprofileButtonID" align="center" class="ui-widget-header customDialogFooter">
				<sj:a id="cancelUserprofileButtonId"
					  button="true"
					  onclick="ns.common.closeDialog('updateProfileDialogId')"
					><s:text name="jsp.default_82"/></sj:a>
				<sj:a 
					  formIds ="UserProfileForm"
					  targets="Miscellaneous"
					  button="true" 
					  validate="true" 
					  validateFunction="customValidation"						  
					  onBeforeTopics="beforeVerifyUserProfile"
					  onCompleteTopics="quickafterSuccessfulSave,quickafterSuccessfulSave"
					><s:text name="jsp.default_366" /></sj:a>
					
				
			</div>
			</td>
		  </tr>
		  <!-- <tr>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
		  </tr> -->
		</table>
		</td>
		</tr>
		</table>

		<ffi:cinclude value1="${LanguagesList.size}" value2="1" operator="equals">
			<ffi:list collection="LanguagesList" items="language">
				<ffi:cinclude value1="${language.IsDefault}" value2="true" operator="equals">
					<input type="hidden" name="PreferredLanguage" value="<ffi:getProperty name="language" property="Language" />">
				</ffi:cinclude>
			</ffi:list>
		</ffi:cinclude>		
		
	    </s:form>
	</div>

	<script>
		$(function(){
			$("#stateForUserSelectID").selectmenu({width:'19.4em'});
			$("#countrySelectID").selectmenu({width:'19.4em'});
			$("#languageSelectID").selectmenu({width:'19.4em'});
			$("#passwordClueSelectID").selectmenu({width:'19.4em'});
			$("#passwordClue2SelectID").selectmenu({width:'19.4em'});
		});
	</script>

<ffi:setProperty name="BackURL" value="${SecurePath}userprefspersonal.jsp?DontRefresh=TRUE" URLEncrypt="true" />
<ffi:removeProperty name="pageRefresh"/>
<ffi:removeProperty name="DontRefresh"/>
<ffi:removeProperty name="UserInfoForEdit"/>
<ffi:removeProperty name="Country_Resource"/>
<ffi:removeProperty name="GetStatesForCountry"/>
<ffi:removeProperty name="UserResource"/>
<ffi:removeProperty name="UserResourceList"/>	
<ffi:removeProperty name="backLinkURL"/>	 

<ffi:object id="ModifyUser" name="com.ffusion.tasks.user.EditUser" scope="session"/>
<ffi:setProperty name="ModifyUser" property="Init" value="true" />
<ffi:process name="ModifyUser"/>
<%
	 session.setAttribute("FFIModifyUser", session.getAttribute("ModifyUser"));
%>

<script type="text/javascript">
<!--
var clearUserPrefVerifiedFlagForSubmenu = function(){
	var menuId = $(this).attr("menuId");
	if(menuId != 'home_prefs'){
		ns.home.sessionCleanup('home_prefs');
	}
	
	//Unbind click event.
	$("#subList li").unbind("click",clearUserPrefVerifiedFlagForSubmenu);
	$("#tabbuttons li a").unbind("click",clearUserPrefVerifiedFlagForMainmenu);
};

var clearUserPrefVerifiedFlagForMainmenu = function(){
	var menuId = $(this).parent().attr('menuId');
	if(menuId != 'home_prefs'){
		ns.home.sessionCleanup('home_prefs');
	}
	
	//Unbind click event.
	$("#tabbuttons li a").unbind("click",clearUserPrefVerifiedFlagForMainmenu);
	$("#subList li").unbind("click",clearUserPrefVerifiedFlagForSubmenu);	
};

$("#subList li").bind("click", clearUserPrefVerifiedFlagForSubmenu);
$("#tabbuttons li a").bind("click", clearUserPrefVerifiedFlagForMainmenu);
//-->

	/* $(document).ready(function(){
		$('#userprofileDesktop').portlet({
			generateDOM: true,
			title: js_user_profile});
	}); */

	$("#newPasswordBox").strength({
	});

</script>
 <div id="operationresult">
<span id="quickUpdateProfileResultmessage" style="display:none">
	<s:text name="jsp.user_390"/>
</span>
</div>
<ffi:removeProperty name="disableEdit" />