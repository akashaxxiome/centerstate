<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<s:include value="inc/admin-init.jsp"/>
<ffi:removeProperty name="DisplayLocation"/>
<script src="<s:url value='/web/js/user/admin%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/admin_grid%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/division%{#session.minVersion}.js'/>" type="text/javascript"></script>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>">
<div id="desktop" align="center">

    <style type="text/css">
        .indent1 { padding-left: 2em; }
        .indent2 { padding-left: 4em; }
        .indent3 { padding-left: 6em; }
        .indent4 { padding-left: 8em; }
    </style>

	<div id="appdashboard">
		<s:include value="division_dashboard.jsp"/> 
	</div>

	<div id="operationresult">
		<div id="resultmessage"></div>
	</div>
	<div id="details">
		<s:include value="admin_details.jsp"/>
	</div>
	<div id="permissionsDiv" class="adminBackground sectionsubhead"></div>
	
	<div id="summary">
		<s:if test="%{#parameters.dashboardElementId[0] == 'addDivisionLink'}">
		<%-- do nothing 2 this is a negative check for else condition --%>
		</s:if>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'showdbLocationSummary'}">
			<%-- do nothing 2 this is a negative check for else condition --%>
		</s:elseif>
		<s:else>
			<s:include value="admin_divisions_summary.jsp"/>
		</s:else>
	</div>
	<div id="tempDivIDForMethodNsAdminReloadDivisions" style="display: none;"></div>
			 
</div>

<sj:dialog id="deleteLocationDialogID" cssClass="adminDialog" title="%{getText('jsp.user_106')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500"  onCloseTopics="closeDeleteLocationDialog">
</sj:dialog>



<sj:dialog id="approvalWizardDialogID" cssClass="adminDialog" title="%{getText('jsp.default_Pending_Approval_Changes')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="700" height="500" >
</sj:dialog>

<sj:dialog id="viewDivisionPendingDialogID" cssClass="adminDialog" title="%{getText('jsp.da_view_division_profile')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto">
</sj:dialog>

<sj:dialog id="viewAdminInViewDiv" cssClass="adminDialog" title="%{getText('jsp.da_view_division_administrator')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto">
</sj:dialog>

<sj:dialog id="viewLocationDialogID" cssClass="adminDialog" title="%{getText('jsp.da_view_division_location')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="850" height="750">
</sj:dialog>

<sj:dialog id="DADivisionSubmitForApprovalDialogId" cssClass="adminDialog" title="%{getText('jsp.da_verify_division_apprival')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>

<sj:dialog id="verifyDiscardPendingDialogID" cssClass="adminDialog" title="%{getText('jsp.da_verify_division_discard')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500">
</sj:dialog>

<sj:dialog id="locationSearchBankDialogID" cssClass="adminDialog" title="%{getText('jsp.default_62')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="850">
</sj:dialog>

<sj:dialog id="editAdminerDialogId" cssClass="adminDialog" title="%{getText('jsp.user_Edit_Adminer')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="850" cssStyle="overflow:hidden;"></sj:dialog>

<sj:dialog id="editDADivisionDialogId" cssClass="adminDialog" title="%{getText('jsp.da_add_division')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="850"></sj:dialog>

<sj:dialog id="editAdminerAutoEntitleDialogId" cssClass="adminDialog" title="%{getText('jsp.user_Edit_Adminer')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650"></sj:dialog>

<sj:dialog id="deleteDivisionDialogID" cssClass="adminDialog" title="%{getText('jsp.user_104')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500" onCloseTopics="closeDeleteDivisionDialog">
</sj:dialog>

<script>
	$(document).ready(function(){
		var $editAdminerDialogIdCloseXButton =$('#editAdminerDialogId').parent().find('.ui-dialog-titlebar-close');
		$editAdminerDialogIdCloseXButton.unbind('click');
		$editAdminerDialogIdCloseXButton.bind('click', function(){
			ns.admin.editAdminerCancelButtonOnClick();
		});
		
		var $editAdminerAutoEntitleDialogCloseXButton =$('#editAdminerAutoEntitleDialogId').parent().find('.ui-dialog-titlebar-close');
		$editAdminerAutoEntitleDialogCloseXButton.unbind('click');
		$editAdminerAutoEntitleDialogCloseXButton.bind('click', function(){
			ns.admin.editAdminerAutoEntitleCancelButtonOnClick();
		});
		
		var availableClickTypes = "addDivisionLink,showdbLocationSummary";
		var elementClicked = "<s:property value='#parameters.dashboardElementId' />";
		if($.trim(elementClicked)!='' && 
				availableClickTypes.indexOf(elementClicked)!=-1){
			ns.admin.showDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
		}
	
	});
	</script>
</ffi:cinclude>
