<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
<ffi:object id="EditCrossAccountPermissions" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
<ffi:object id="ApprovePendingChanges" name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" />
<ffi:object id="approveCategory" name="com.ffusion.beans.dualapproval.DAItemCategory" />

<ffi:setProperty name="Context" value="${BusinessEmployee.FullName} (User)"/>
	<ffi:cinclude value1="3" value2="${BusinessEmployee.CustomerType}" operator="equals">
		<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
	</ffi:cinclude>
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminusers.jsp"/>

<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>" />
	<ffi:setProperty name="itemId" value="${Business.id}" />
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Division" operator="equals">
	<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>" />
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Group" operator="equals">
	<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
	<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE%>" />
</ffi:cinclude> 
<s:if test="%{#session.Section == 'Users'}">
	<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>" />
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
	<ffi:setProperty name="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>" />
</ffi:cinclude>

<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetCategories" property="itemType" value="${itemType}"/>
<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>

<ffi:setProperty name="ApprovePendingChanges" property="businessId" value="${SecureUser.businessID}" />
<ffi:setProperty name="ApprovePendingChanges" property="itemType" value="${itemType}" />
<ffi:setProperty name="ApprovePendingChanges" property="itemId" value="${itemId}" />
<ffi:setProperty name="ApprovePendingChanges" property="categoryBeanName" value="approveCategory" />

<ffi:cinclude value1="${isAccountAccessChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-account-access-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isAccountGroupAccessChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-account-group-access-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isCrossACHAccessChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-cross-ach-company-access-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isPerACHAccessChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-per-ach-company-access-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isCrossAccountChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-cross-account-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isPerAccountChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-per-account-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isPerAccountGroupChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-per-account-group-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isPerLocationChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-per-location-approve-permission.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${isWireTemplateChanged}" value2="Y" >
	<s:include value="/pages/jsp/user/inc/da-approve-wire-templates-access-permission.jsp" />
</ffi:cinclude>
<ffi:object name="com.ffusion.tasks.dualapproval.ProcessDACategories" id="ProcessDACategories" />
<ffi:setProperty name="ProcessDACategories" property="ProcessFlag" value="false" />
<ffi:process name="ProcessDACategories" />
<ffi:removeProperty name="ProcessDACategories" />
<ffi:removeProperty name="<%=SessionNames.PER_ACCOUNT_CATEGORY_DATA_1 %>"/>
<ffi:removeProperty name="<%=SessionNames.PER_ACCOUNT_CATEGORY_DATA_2 %>"/>
<ffi:removeProperty name="<%=SessionNames.PER_ACCOUNT_CATEGORY_DATA_3 %>"/>