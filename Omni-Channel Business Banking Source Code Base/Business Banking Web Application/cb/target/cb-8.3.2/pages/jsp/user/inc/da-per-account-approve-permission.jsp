<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<ffi:object id="EditAccountPermissionsDA" name="com.ffusion.tasks.dualapproval.EditAccountPermissionsDA" scope="session"/>

<ffi:setProperty name="EditAccountPermissionsDA" property="approveAction" value="true" />
<ffi:setProperty name="EditAccountPermissionsDA" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="EditAccountPermissionsDA" property="EntitlementTypePropertyLists" value="LimitsList"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditAccountPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditAccountPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditAccountPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditAccountPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditAccountPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>
<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="AccountMerged" value="AccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>

<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${EntitlementGroupParentId}"/>

<%-- approve all the per account changes --%>
<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePerAccountPermissionsDA" id="ApprovePerAccountPermissionsDA"/>
<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="EntitlementTypePropertyLists" value="LimitsList"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>

<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="itemType" value="${ApprovePendingChanges.itemType}" />
<ffi:setProperty name="ApprovePerAccountPermissionsDA" property="itemId" value="${ApprovePendingChanges.itemId}" />
<ffi:process name="ApprovePerAccountPermissionsDA"/>

<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" />
<ffi:removeProperty name="catSubType" />
<ffi:removeProperty name="EditAccountPermissionsDA" />
<ffi:removeProperty name="ApprovePerAccountPermissionsDA"/>
<ffi:removeProperty name="dacategories" />