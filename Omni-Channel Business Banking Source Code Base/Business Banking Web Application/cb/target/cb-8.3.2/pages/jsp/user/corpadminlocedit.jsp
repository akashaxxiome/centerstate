<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
<ffi:process name="GetBPWProperty"/>

<ffi:help id="user_corpadminlocedit" className="moduleHelpClass"/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_144"/></span>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name='PageHeadingForLocation' value='Edit Location'/>
<script>
	ns.admin.PageHeadingForLocation="<ffi:getProperty name='PageHeadingForLocation' />";
</script>
<style>
.displayNone{display:none;}
.displayVisible{display:table-cell;}
</style>
<%
	String EditLocation_LocationBPWID = request.getParameter("EditLocation_LocationBPWID");
	if(EditLocation_LocationBPWID!= null){
		session.setAttribute("EditLocation_LocationBPWID", EditLocation_LocationBPWID);
	}
	
	String EditDivision_GroupId = request.getParameter("EditDivision_GroupId");
	if(EditDivision_GroupId != null) {
		session.setAttribute("EditDivision_GroupId", EditDivision_GroupId);
	}
	
	String editlocationreload = request.getParameter("editlocationreload");
	if(editlocationreload!= null){
		session.setAttribute("editlocationreload", editlocationreload);
	}
	
	String childSequence = request.getParameter("childSequence");
	if(childSequence!= null){
		session.setAttribute("childSequence", childSequence);
	}
	
	String fromDivEdit = request.getParameter("fromDivEdit");
	if(fromDivEdit!= null){
		session.setAttribute("fromDivEdit", fromDivEdit);
	}
	
%>


<script language="javascript">
$(function(){
	$("#selectBankID").selectmenu({width: 200});
	$("#selectAccountTypeID").selectmenu({width: 150});
	$("#selectAccountID").selectmenu({width: 200});
	$("#selectCashConCompanyID").selectmenu({width: 150});
	$("#selectConcAccountID").selectmenu({width: 250});
	$("#selectDisbAccountID").selectmenu({width: 250});
	
	$("#selectCashConCompanyID").change(function() {
    	showHideSameDayPrenoteFields($("option:selected", this));
    });
});
</script>

<style>
/* --- Fix for the dropdown arrow - Issue: when the value of textbox is more than the specified width, the arrow shifts down --- */
#selectConcAccountID-menu span.ui-selectmenu-icon, #selectDisbAccountID-menu span.ui-selectmenu-icon{
	position: absolute !important;
	right:0 !important;
}
</style>

<s:include value="inc/corpadminlocations-pre.jsp"/>

<ffi:setProperty name="locationEditDualApprovalMode" value="${Business.dualApprovalMode}"/>
<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request" />


<ffi:cinclude value1="${editlocationreload}" value2="true" operator="equals">
	<%-- Get the affiliate bank list --%>
	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
	<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />

	<%-- Get cashcon company list --%>
	<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
	<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
	<ffi:process name="GetCashConCompanies"/>
	<ffi:removeProperty name="GetCashConCompanies"/>

	<%-- Get the location to be edited --%>
	<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation" scope="session" />
	<ffi:setProperty name="SetLocation" property="BPWID" value="${EditLocation_LocationBPWID}"/>
	<ffi:process name="SetLocation"/>
	
	<%-- Fill in the location values --%>
	<ffi:object id="EditLocation" name="com.ffusion.tasks.cashcon.EditLocation" scope="session" />
	<ffi:setProperty name="EditLocation" property="SessionKey" value="Location"/>
	<ffi:setProperty name="EditLocation" property="DivisionID" value="${Location.DivisionID}"/>
	<ffi:setProperty name="EditLocation" property="AccountsName" value="BankingAccounts"/>
	<ffi:setProperty name="EditLocation" property="LocationBPWID" value="${Location.LocationBPWID}"/>
	<ffi:setProperty name="EditLocation" property="LocationName" value="${Location.LocationName}"/>
	<ffi:setProperty name="EditLocation" property="LocationID" value="${Location.LocationID}"/>
	<ffi:setProperty name="EditLocation" property="LogId" value="${Location.LogId}"/>
	<ffi:setProperty name="EditLocation" property="SubmittedBy" value="${Location.SubmittedBy}"/>
	<ffi:setProperty name="EditLocation" property="Active" value="${Location.Active}"/>
	<ffi:setProperty name="EditLocation" property="DepPrenoteStatus" value="${Location.DepPrenoteStatus}"/>
	<ffi:setProperty name="EditLocation" property="DisPrenoteStatus" value="${Location.DisPrenoteStatus}"/>

	<%-- See if the routing number is in the affiliate bank list --%>
	<% boolean customLocalBank = false;
	   boolean customLocalAccount = false; %>
	<ffi:list collection="AffiliateBanks" items="Bank">
		<ffi:cinclude value1="${Bank.AffiliateRoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
			<% customLocalBank = true; %>
			<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
		</ffi:cinclude>
	</ffi:list>
	<% if (!customLocalBank) { %>
		<ffi:setProperty name="EditLocation" property="LocalRoutingNumber" value="${Location.LocalRoutingNumber}"/>
		<ffi:setProperty name="EditLocation" property="LocalBankName" value="${Location.LocalBankName}"/>
	<% } %>
	<ffi:setProperty name="EditLocation" property="CustomLocalBank" value="<%= String.valueOf(customLocalBank)%>"/>

	<%-- See if the account is in the accounts list --%>
	<ffi:list collection="BankingAccounts" items="Account">
		<ffi:cinclude value1="${Account.Number}" value2="${Location.LocalAccountNumber}" operator="equals">
			<% customLocalAccount = true; %>
			<ffi:setProperty name="EditLocation" property="LocalAccountID" value="${Account.ID}"/>
		</ffi:cinclude>
	</ffi:list>
	<% if (!customLocalAccount) { %>
	    <ffi:setProperty name="EditLocation" property="LocalAccountNumber" value="${Location.LocalAccountNumber}"/>
	    <ffi:setProperty name="EditLocation" property="LocalAccountType" value="${Location.LocalAccountType}"/>
	<% } %>
	<ffi:setProperty name="EditLocation" property="CustomLocalAccount" value="<%= String.valueOf(customLocalAccount)%>"/>

	<ffi:setProperty name="EditLocation" property="CashConCompanyBPWID" value="${Location.CashConCompanyBPWID}"/>
	<ffi:setProperty name="EditLocation" property="DisbAccountBPWID" value="${Location.DisbAccountBPWID}"/>
	<ffi:setProperty name="EditLocation" property="ConcAccountBPWID" value="${Location.ConcAccountBPWID}"/>
	<ffi:cinclude value1="${Location.DepositMinimum}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="DepositMinimum" value="${Location.DepositMinimum}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Location.DepositMaximum}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="DepositMaximum" value="${Location.DepositMaximum}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Location.AnticDeposit}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="AnticDeposit" value="${Location.AnticDeposit}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Location.ThreshDeposit}" value2="" operator="notEquals">
	    <ffi:setProperty name="EditLocation" property="ThreshDeposit" value="${Location.ThreshDeposit}"/>
	</ffi:cinclude>
	<ffi:setProperty name="EditLocation" property="ConsolidateDeposits" value="${Location.ConsolidateDeposits}"/>
	<ffi:setProperty name="EditLocation" property="DepositPrenote" value="${Location.DepositPrenote}"/>
	<ffi:setProperty name="EditLocation" property="DisbursementPrenote" value="${Location.DisbursementPrenote}"/>
	<ffi:setProperty name="EditLocation" property="DepositSameDayPrenote" value="${Location.DepositSameDayPrenote}"/>
	<ffi:setProperty name="EditLocation" property="DisbursementSameDayPrenote" value="${Location.DisbursementSameDayPrenote}"/>

</ffi:cinclude>
<% session.setAttribute("FFIEditLocation", session.getAttribute("EditLocation")); %>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

	<ffi:cinclude value1="${editlocationreload}" value2="true" operator="notEquals">

		<%-- Get cashcon company list --%>

		<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
		<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${EditDivision_GroupId}"/>
		<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
		<ffi:process name="GetCashConCompanies"/>
		<ffi:removeProperty name="GetCashConCompanies"/>
	</ffi:cinclude>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditDivision_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="objectId" value="${EditLocation_LocationBPWID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="objectType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
	<ffi:process name="GetDACategoryDetails"/>

	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
		<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
			<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="EditLocation"/>
		<ffi:process name="PopulateObjectWithDAValues"/>
	</ffi:cinclude>

	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="PopulateObjectWithDAValues"/>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="0" operator="equals">
	<ffi:list collection="AffiliateBanks" items="Bank" startIndex="1" endIndex="1">
		<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="0" operator="notEquals">
	<ffi:object id="SetAffiliateBank" name="com.ffusion.tasks.affiliatebank.SetAffiliateBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="CollectionSessionName" value="AffiliateBanks"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankSessionName" value="CurrentBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankID" value="${EditLocation.LocalBankID}"/>
	<ffi:process name="SetAffiliateBank"/>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="" operator="equals">
	<ffi:list collection="CashConCompanies" items="tempCompany" startIndex="1" endIndex="1">
		<ffi:setProperty name="EditLocation" property="CashConCompanyBPWID" value="${tempCompany.BPWID}"/>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="" operator="notEquals">
	<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
	<ffi:process name="SetCashConCompany"/>
</ffi:cinclude>

<%-- AFTER we have the edit location's company, then set the filter because this location could be using an inactive company QTS 406048 --%>
	<ffi:setProperty name="CashConCompanies" property="Filter" value="ACTIVE=true"/>
	<ffi:setProperty name="CashConCompanies" property="FilterOnFilter" value="CONC_ENABLED=true,DISB_ENABLED=true"/>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<ffi:setProperty name="CCLocationPrenoteStatus" value="<%= com.ffusion.beans.cashcon.CashConDefines.CC_LOCATION_PRENOTE_PENDING %>"/>

<SCRIPT language=JavaScript><!--
<ffi:setProperty name="AffiliateBanks" property="Filter" value=""/>
/* Account lists for each of the "pre-defined" banks. */
var bankAccounts = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
<% int bankAccountsCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
    <ffi:setProperty name="BankingAccounts" property="Filter" value="ROUTINGNUM=${affiliateBank.AffiliateRoutingNum}"/>
bankAccounts[<%= Integer.toString(bankAccountsCounter) %>] = new Array(
<ffi:setProperty name="spaceOrComma" value=" "/>
    <ffi:list collection="BankingAccounts" items="bankAccount">
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='DisplayText'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='DisplayText'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
    </ffi:list>
	);
<% bankAccountsCounter++; %>
</ffi:list>

/* Mappings of banks to indices into the account lists.*/
var bankIndexMapping = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
var bankIndexMappingByID = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);

<% int bankIndexMappingCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
bankIndexMapping[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateRoutingNum'/>";
bankIndexMappingByID[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateBankID'/>";
<% bankIndexMappingCounter++; %>
</ffi:list>
<ffi:setProperty name="BankingAccounts" property="Filter" value=""/>

/* concentration and disbursement Account lists for each of the CC companies. */
var ccAccounts = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var disbAccounts = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var ccEnabled = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var disbEnabled = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var ccSize = <ffi:getProperty name="CashConCompanies" property="size"/>;
var ccBPWIDs = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);

<% int ccCounter = 0; %>
<ffi:list collection="CashConCompanies" items="ccCompany">
	ccEnabled[<%= Integer.toString(ccCounter) %>] = <ffi:getProperty name="ccCompany" property="ConcEnabledString"/>;
	ccBPWIDs[<%= Integer.toString(ccCounter) %>] = "<ffi:getProperty name='ccCompany' property='BPWID'/>";
	ccAccounts[<%= Integer.toString(ccCounter) %>] = new Array(
		<ffi:setProperty name="spaceOrComma" value=" "/>
    	<ffi:list collection="ccCompany.ConcAccounts" items="ccAccount">
			<ffi:getProperty name="spaceOrComma"/>
			new Array("<ffi:getProperty name="ccAccount" property="DisplayText"/><ffi:cinclude value1="${ccAccount.Nickname}" value2="" operator="notEquals"> - <ffi:getProperty name="ccAccount" property="Nickname"/></ffi:cinclude>", "<ffi:getProperty name='ccAccount' property='BPWID'/>")
			<ffi:setProperty name="spaceOrComma" value=","/>
		</ffi:list>
	);
	disbEnabled[<%= Integer.toString(ccCounter) %>] = <ffi:getProperty name="ccCompany" property="DisbEnabledString"/>;
	disbAccounts[<%= Integer.toString(ccCounter) %>] = new Array(
		<ffi:setProperty name="spaceOrComma" value=" "/>
		<ffi:list collection="ccCompany.DisbAccounts" items="disbAccount">
			<ffi:getProperty name="spaceOrComma"/>
			new Array("<ffi:getProperty name="disbAccount" property="DisplayText"/><ffi:cinclude value1="${disbAccount.Nickname}" value2="" operator="notEquals"> - <ffi:getProperty name="disbAccount" property="Nickname"/></ffi:cinclude>", "<ffi:getProperty name='disbAccount' property='BPWID'/>")
			<ffi:setProperty name="spaceOrComma" value=","/>
		</ffi:list>
	);
<% ccCounter++; %>
</ffi:list>

<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals" >
	<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals" >
		var cashConcAccountsCurrencyCode = new Array ();
		<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount" >
			cashConcAccountsCurrencyCode[ "<ffi:getProperty name="ConcAccount" property="BPWID" />" ] = "<ffi:getProperty name="ConcAccount" property="Currency" />";
			<ffi:cinclude value1="${ConcAccount.BPWID}" value2="${EditLocation.ConcAccountBPWID}" operator="equals" >
				<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${ConcAccount.Currency}"/>
			</ffi:cinclude>
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>

function updateAccountsList(routingNum)
{
	var updated = false;
	var accountIDSel = document.getElementById("selectAccountID");
	var selectedValue = accountIDSel.options[accountIDSel.selectedIndex].value;
	var selectedAccountID = "<ffi:getProperty name="EditLocation" property="LocalAccountID" />";

	for (var i = 0; i < bankIndexMapping.length; i++) {
		if (bankIndexMapping[i] == routingNum) {

			if (bankAccounts[i].length == 0) {
				accountIDSel.options.length = 1;
				accountIDSel.options[0] = new Option(js_transfer_freq_NONE, "");
			} else {
				accountIDSel.options.length = bankAccounts[i].length;

				for (var j = 0; j < bankAccounts[i].length; j++) {
					var defaultSelected = (selectedValue == bankAccounts[i][j][1] || selectedAccountID == bankAccounts[i][j][1]) ? true : false;
					accountIDSel.options[j] = new Option(bankAccounts[i][j][0],
							bankAccounts[i][j][1], defaultSelected, defaultSelected);
				}
			}

			updated = true;
			break;
		}
	}

	if (!updated) {
		accountIDSel.options.length = 1;
		accountIDSel.options[0] = new Option(js_transfer_freq_NONE, "");
	}
	$("#selectAccountID").selectmenu('destroy');
	$('#selectAccountID').selectmenu({width: 200});
}

function updateAccountsListByID(id) {
    for (var i = 0; i < bankIndexMappingByID.length; i++) {
	if (bankIndexMappingByID[i] == id) {
	    updateAccountsList(bankIndexMapping[i]);
	    break;
	}
    }
}

var bConc = true;
var bDisb = true;

function updateCCAccountsList(ccBPWID)
{
	var concIDSel = document.getElementById("selectConcAccountID");
	var disbIDSel = document.getElementById("selectDisbAccountID");
	var selectedValue = "";
	if (concIDSel.selectedIndex != -1) {
		selectedValue = concIDSel.options[concIDSel.selectedIndex].value;
	}
	
	bConc = true;
	bDisb = true;
	
	var ccIndex = 0;
	for (var i = 0; i < ccBPWIDs.length; i++) {
		if (ccBPWIDs[i] == ccBPWID) {
			ccIndex = i;
			break;
		}
	}
	if (ccSize == 0 || ccEnabled[ccIndex] == false || ccAccounts[ccIndex].length == 0) {
		concIDSel.options.length = 1;
		concIDSel.options[0] = new Option("None", "");
		bConc = false;
	} else {
		concIDSel.options.length = ccAccounts[ccIndex].length;
		for (var j = 0; j < ccAccounts[ccIndex].length; j++) {
			var defaultSelected = (selectedValue == ccAccounts[ccIndex][j][1]) ? true : false;
			concIDSel.options[j] = new Option(ccAccounts[ccIndex][j][0],
					ccAccounts[ccIndex][j][1], defaultSelected, defaultSelected);
		}
	}

	selectedValue = disbIDSel.options[disbIDSel.selectedIndex].value;
	if (ccSize == 0 || disbEnabled[ccIndex] == false || disbAccounts[ccIndex].length == 0) {
		disbIDSel.options.length = 1;
		disbIDSel.options[0] = new Option("None", "");
		bDisb = false;
	} else {
		disbIDSel.options.length = disbAccounts[ccIndex].length;
		for (var j = 0; j < disbAccounts[ccIndex].length; j++) {
			var defaultSelected = (selectedValue == disbAccounts[ccIndex][j][1]) ? true : false;
			disbIDSel.options[j] = new Option(disbAccounts[ccIndex][j][0],
					disbAccounts[ccIndex][j][1], defaultSelected, defaultSelected);
		}
	}
	$("#selectConcAccountID").selectmenu('destroy');
	$('#selectConcAccountID').selectmenu({width: 250});
	$("#selectDisbAccountID").selectmenu('destroy');
	$('#selectDisbAccountID').selectmenu({width: 250});
	disableFields(bConc, bDisb);
}

function showHideSameDayPrenoteFields(element) {
	
    var sameDayCashConEnabled = element.attr("SameDayCashConEnabled");
    var concSameDayCutOffDefined = element.attr("ConcSameDayCutOffDefined");
    var concCutOffsPassed = element.attr("ConcCutOffsPassed");
    var depositSameDayPrenote = element.attr("DepositSameDayPrenote");
    var disbSameDayCutOffDefined = element.attr("DisbSameDayCutOffDefined");
    var disbCutOffsPassed = element.attr("DisbCutOffsPassed");
    var disbursementSameDayPrenote = element.attr("DisbursementSameDayPrenote");
    var depPrenoteStatus = element.attr("DepPrenoteStatus");
    var disPrenoteStatus = element.attr("DisPrenoteStatus");
    var locationPrenoteStatus = element.attr("CCLocationPrenoteStatus");
    var depositPrenote = element.attr("DepositPrenote");
    var disbursementPrenote = element.attr("DisbursementPrenote");
    
    if (sameDayCashConEnabled == "true") {
		if (bConc) {			
			if (concSameDayCutOffDefined == "true") {
				 $('#regularDepositPrenoteLabelClass').removeClass("displayVisible");
				 $('#regularDepositPrenoteCheckboxClass').removeClass("displayVisible");
				 $('#depositSameDayPrenoteClass').removeClass("displayNone");
				 $('#regularDepositPrenoteLabelClass').addClass("displayNone");
				 $('#regularDepositPrenoteCheckboxClass').addClass("displayNone");
				 $('#depositSameDayPrenoteClass').addClass("displayVisible");
				 $('#regularDepositPrenote').removeAttr("name");
				 $('#__checkbox_regularDepositPrenote').removeAttr("name");
				 $('#DepositPrenote').attr("name", "EditLocation.DepositPrenote" );
				 $('#__checkbox_DepositPrenote').attr("name", "__checkbox_EditLocation.DepositPrenote" );
				if (concCutOffsPassed == "true") {
					$('#showConcNote').show();
					$('#DepositSameDayPrenote').attr("disabled", true);
				} else {
					$('#showConcNote').hide();
					$('#DepositSameDayPrenote').removeAttr("disabled");
				}
				$('#DepositPrenote').removeAttr("disabled");
				
				if (depositSameDayPrenote == "true") {
					$('#DepositSameDayPrenote').val(true);
					$('#DepositSameDayPrenote').attr('checked', true);
					$('#hiddenDepositSameDayPrenote').attr("name", "EditLocation.DepositSameDayPrenote" );
					
				}
			} else {
				disableDepositSameDayFields();
			}
		}
		if (bDisb) {
			if (disbSameDayCutOffDefined == "true") {
				
				 $('#regularDisbursementPrenoteCheckboxClass').removeClass("displayVisible");
				 $('#regularDisbursementPrenoteLabelClass').removeClass("displayVisible");
				 $('#regularDisbursementPrenoteCheckboxClass').addClass("displayNone");
				 $('#regularDisbursementPrenoteLabelClass').addClass("displayNone");
				 $('#disbursementSameDayPrenoteClass').addClass("displayVisible");
				 $('#disbursementSameDayPrenoteClass').removeClass("displayNone");
				 $('#regularDisbursementPrenote').removeAttr("name");
				 $('#__checkbox_regularDisbursementPrenote').removeAttr("name");
				 $('#DisbursementPrenote').attr("name", "EditLocation.DisbursementPrenote" );
				 $('#__checkbox_DisbursementPrenote').attr("name", "__checkbox_EditLocation.DisbursementPrenote" );
				if (disbCutOffsPassed == "true") {
					$('#showDisbNote').show();
					$('#DisbursementSameDayPrenote').attr("disabled", true);
				} else {
					$('#showDisbNote').hide();
					$('#DisbursementSameDayPrenote').removeAttr("disabled");
				}
				$('#DisbursementPrenote').removeAttr("disabled");
				if (disbursementSameDayPrenote == "true") {
					$('#DisbursementSameDayPrenote').val(true);
					$('#DisbursementSameDayPrenote').attr('checked', true);
					 $('#hiddenDisbursementSameDayPrenote').attr("name", "EditLocation.DisbursementSameDayPrenote" );
				}
			} else {
				disableDisbursementSameDayFields();
			}
		}
    } else {
    	disableDepositSameDayFields();
    	disableDisbursementSameDayFields();
    }
    
    disableCommonFields(depositSameDayPrenote, disbursementSameDayPrenote, depPrenoteStatus, 
    		disPrenoteStatus, locationPrenoteStatus, depositPrenote, disbursementPrenote);
}

function disableCommonFields(depositSameDayPrenote, disbursementSameDayPrenote, depPrenoteStatus, 
		disPrenoteStatus, locationPrenoteStatus, depositPrenote, disbursementPrenote) {
	
	if (depositSameDayPrenote == "true") {
		$('#DepositPrenote').attr('checked', false);
		$('#DepositPrenote').removeAttr('checked');
		$('#regularDepositPrenote').attr('checked', false);
		$('#regularDepositPrenote').removeAttr('checked');
	}
	
	if (disbursementSameDayPrenote == "true") {
		$('#DisbursementPrenote').attr('checked', false);
		$('#DisbursementPrenote').removeAttr('checked');
		$('#regularDisbursementPrenote').attr('checked', false);
		$('#regularDisbursementPrenote').removeAttr('checked');
	}
	
	if (depPrenoteStatus == locationPrenoteStatus) {
		$('#DepositSameDayPrenote').attr("disabled", true);
		$('#DepositPrenote').attr("disabled", true);
		$('#regularDepositPrenote').attr("disabled", true);
		$('#showConcNote').hide();
		 $('#hiddenDepositPrenote').removeAttr("name");
		 $('#hiddenDepositPrenote').attr("name", "EditLocation.DepositPrenote" );
	}
	
	if (disPrenoteStatus == locationPrenoteStatus)  {
		$('#DisbursementSameDayPrenote').attr("disabled", true);
		$('#DisbursementPrenote').attr("disabled", true);
		$('#regularDisbursementPrenote').attr("disabled", true);
		$('#showDisbNote').hide();
		 $('#hiddenDisbursementPrenote').removeAttr("name");
		 $('#hiddenDisbursementPrenote').attr("name", "EditLocation.DisbursementPrenote" );
	}
	 
	if (depositSameDayPrenote == "true") {
		 document.getElementById('hiddenDepositPrenote').value = false;
		 document.getElementById('hiddenDepositSameDayPrenote').value = true;
	} else if (depositPrenote == "true") {	
		document.getElementById('hiddenDepositPrenote').value = true;
		document.getElementById('hiddenDepositSameDayPrenote').value = false;
	}
	
	if (disbursementSameDayPrenote == "true") {
		
		document.getElementById('hiddenDisbursementPrenote').value = false;
		document.getElementById('hiddenDisbursementSameDayPrenote').value = true;
		
	} else if (disbursementPrenote == "true") {
		
		document.getElementById('hiddenDisbursementPrenote').value = true;
		document.getElementById('hiddenDisbursementSameDayPrenote').value = false;
	}
}

function disableDepositSameDayFields () {
	 $('#hiddenDepositPrenote').removeAttr("name");
	 $('#hiddenDepositSameDayPrenote').removeAttr("name");
	 document.getElementById('DepositSameDayPrenote').value = false;
	 $('#DepositSameDayPrenote').attr('checked', false);
	 $('#DepositSameDayPrenote').removeAttr('checked');
	 $('#regularDepositPrenoteLabelClass').removeClass("displayNone");
	 $('#regularDepositPrenoteCheckboxClass').removeClass("displayNone");
	 $('#depositSameDayPrenoteClass').removeClass("displayVisible");
	 $('#regularDepositPrenoteLabelClass').addClass("displayVisible");
	 $('#regularDepositPrenoteCheckboxClass').addClass("displayVisible");
	 $('#depositSameDayPrenoteClass').addClass("displayNone");
	 $('#regularDepositPrenote').removeAttr("disabled");
	 $('#DepositPrenote').removeAttr("name");
	 $('#__checkbox_DepositPrenote').removeAttr("name");
	 $('#regularDepositPrenote').attr("name", "EditLocation.DepositPrenote" );
	 $('#__checkbox_regularDepositPrenote').attr("name", "__checkbox_EditLocation.DepositPrenote" );
	 $('#showConcNote').hide();
}

function disableDisbursementSameDayFields () {
	 $('#hiddenDisbursementPrenote').removeAttr("name");
	 $('#hiddenDisbursementSameDayPrenote').removeAttr("name");
	 document.getElementById('DisbursementSameDayPrenote').value = false;
	 $('#DisbursementSameDayPrenote').attr('checked', false);
	 $('#DisbursementSameDayPrenote').removeAttr('checked');
	 $('#regularDisbursementPrenoteCheckboxClass').removeClass("displayNone");
	 $('#regularDisbursementPrenoteLabelClass').removeClass("displayNone");
	 $('#disbursementSameDayPrenoteClass').removeClass("displayVisible");
	 $('#regularDisbursementPrenoteCheckboxClass').addClass("displayVisible");
	 $('#regularDisbursementPrenoteLabelClass').addClass("displayVisible");
	 $('#disbursementSameDayPrenoteClass').addClass("displayNone");
	 $('#regularDisbursementPrenote').removeAttr("disabled");
	 $('#DisbursementPrenote').removeAttr("name");
	 $('#__checkbox_DisbursementPrenote').removeAttr("name");
	 $('#regularDisbursementPrenote').attr("name", "EditLocation.DisbursementPrenote" );
	 $('#__checkbox_regularDisbursementPrenote').attr("name", "__checkbox_EditLocation.DisbursementPrenote" );
	 $('#showDisbNote').hide();
}

function disableFields(depositEnabled, disbursementEnabled)
{
	if (depositEnabled) {
		$('#ConsolidateDeposits').removeAttr("disabled");
		
		$('#DepositMinimum').removeAttr("disabled");
		$('#DepositMaximum').removeAttr("disabled");
		$('#AnticDeposit').removeAttr("disabled");
		$('#ThreshDeposit').removeAttr("disabled");
	} else {
		$('#ConsolidateDeposits').attr("disabled", true);
		$('#DepositPrenote').attr("disabled", true);
		$('#regularDepositPrenote').attr("disabled", true);
		$('#DepositSameDayPrenote').attr("disabled", true);
		$('#DepositMinimum').attr("disabled", true);
		$('#DepositMaximum').attr("disabled", true);
		$('#AnticDeposit').attr("disabled", true);
		$('#ThreshDeposit').attr("disabled", true);
		$('#ConsolidateDeposits').attr('checked', false);
		$('#DepositPrenote').attr('checked', false);
		$('#regularDepositPrenote').attr('checked', false);
		$('#DepositSameDayPrenote').attr('checked', false);
		$('#DepositMinimum').val('');
		$('#DepositMaximum').val('');
		$('#AnticDeposit').val('');
		$('#ThreshDeposit').val('');

	}
	if (!disbursementEnabled) {
		$('#regularDisbursementPrenote').attr("disabled", true);
		$('#regularDisbursementPrenote').checked = false;
		$('#DisbursementPrenote').attr("disabled", true);
		$('#DisbursementPrenote').checked = false;
		$('#DisbursementSameDayPrenote').attr("disabled", true);
		$('#DisbursementSameDayPrenote').checked = false;
	}
}

$(document).ready(function(){
	updateCCAccountsList('<ffi:getProperty name="EditLocation" property="CashConCompanyBPWID"/>');
	showHideSameDayPrenoteFields($("#selectCashConCompanyID option:selected"));

	<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="false" operator="equals">
		updateAccountsList('<ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>');
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">
		updateAccountsList('<ffi:getProperty name="CurrentBank" property="AffiliateRoutingNum"/>');
	</ffi:cinclude>
});


function removeDepositSameDayPrenoteChecked(prenoteId) {
	if (prenoteId.checked) {
		document.getElementById('DepositSameDayPrenote').checked = false;
		document.getElementById('DepositPrenote').value = true;
		document.getElementById('DepositSameDayPrenote').value = false;
		document.getElementById('hiddenDepositPrenote').value = true;
		document.getElementById('hiddenDepositSameDayPrenote').value = false;
	} else {
		document.getElementById('DepositPrenote').value = false;
		document.getElementById('hiddenDepositPrenote').value = false;
	}
}

function removeDepositNormalPrenoteChecked(sameDayPrenoteId) {
    if(sameDayPrenoteId.checked) {
    	document.getElementById('DepositPrenote').checked = false;
    	document.getElementById('DepositPrenote').value = false;
    	document.getElementById('DepositSameDayPrenote').value = true;
    	document.getElementById('hiddenDepositSameDayPrenote').value = true;
    	document.getElementById('hiddenDepositPrenote').value = false;
    } else {
    	document.getElementById('DepositSameDayPrenote').value = false;
    	document.getElementById('hiddenDepositSameDayPrenote').value = false;
    }
    
}

function removeDisbursementSameDayPrenoteChecked(prenoteId) {
	if (prenoteId.checked) {
		document.getElementById('DisbursementSameDayPrenote').checked = false;
		document.getElementById('DisbursementSameDayPrenote').value = false;
		document.getElementById('DisbursementPrenote').value = true;
		document.getElementById('hiddenDisbursementPrenote').value = true;
		document.getElementById('hiddenDisbursementSameDayPrenote').value = false;
	} else {
		document.getElementById('DisbursementPrenote').value = false;
		document.getElementById('hiddenDisbursementPrenote').value = false;
	}
}

function removeDisbursementNormalPrenoteChecked(sameDayPrenoteId) {
    if(sameDayPrenoteId.checked) {
    	document.getElementById('DisbursementPrenote').checked = false;
    	document.getElementById('DisbursementPrenote').value = false;
    	document.getElementById('DisbursementSameDayPrenote').value = true;
    	document.getElementById('hiddenDisbursementSameDayPrenote').value = true;
    	document.getElementById('hiddenDisbursementPrenote').value = false;
    } else {
    	document.getElementById('DisbursementSameDayPrenote').value = false;
    	document.getElementById('hiddenDisbursementSameDayPrenote').value = false;
    }
}


//--></SCRIPT>
	<ffi:setProperty name="AddLoc" value="FALSE" />
		<div align="center">
			<%-- <table width="676" border="1" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionhead"><ffi:getProperty name="EditGroup_DivisionName"/><br>
								<br>
							</span>
						</div>
					</td>
				</tr>
			</table> --%>
			
			<table  cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10">
				<tr>
					<td width="22%" valign="top"> 
						<span class="ffivisible marginTop10">&nbsp;</span>
						<div class="paneWrapper marginTop20">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="admin.division.summary"/>
							</div>
							<div class="paneContentWrapper">
								<table cellspacing="0" cellpadding="3" border="0" width="100%">
									<tr>
										<td><s:text name="jsp.user_121"/>:&nbsp; <ffi:getProperty name="EditGroup_DivisionName"/></td>
									</tr>
								</table>
							</div>
							</div>
						</div>
					</td>
					<td width="1%">&nbsp;</td>
					<td align="left" class="adminBackground" width="77%" valign="top">
						<s:form id="AddEditLocationFormID" namespace="/pages/user" action="verifyeditLocation" theme="simple" name="AddEditLocationForm" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="EditLocation.DivisionID" value="<ffi:getProperty name="EditLocation" property="DivisionID"/>"/>
						<input type="hidden" name="AddingFreeFormAccount" value="true"/>
						<input type="hidden" id="hiddenDepositSameDayPrenote" name="EditLocation.DepositSameDayPrenote" value="${EditLocation.DepositSameDayPrenote}"/>
						<input type="hidden" id="hiddenDepositPrenote" name="DepositPrenote" value="${EditLocation.DepositPrenote}"/>
						<input type="hidden" id="hiddenDisbursementSameDayPrenote" name="EditLocation.DisbursementSameDayPrenote" value="${EditLocation.DisbursementSameDayPrenote}"/>
						<input type="hidden" id="hiddenDisbursementPrenote" name="DisbursementPrenote" value="${EditLocation.DisbursementPrenote}"/>
	
						<div id="editLocationPanel" class="rightPaneWrapper" style="width:100%">
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr  class="columndataauto" >
								<td><p class="transactionHeading"><s:text name="admin.location.summary"/></p></td>
							</tr>
						</table>
								
						<table border="0" cellspacing="0" cellpadding="3" align="left">	
							<tr class="columndataauto">
								<td class="sectionsubhead ltrow2_color" align="left">
									<span class="<ffi:getPendingStyle fieldname="locationName" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_267"/>:</span>
								</td>
								<td width="140">&nbsp;</td>
								<td class="sectionsubhead ltrow2_color" align="left">
									<span class="<ffi:getPendingStyle fieldname="locationID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_189"/>:</span>
								</td>
								<td width="80">&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<input class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocationName" size="24" maxlength="16" border="0" value="<ffi:getProperty name="EditLocation" property="LocationName"/>">
									<br><span id="EditLocation.locationNameError"></span>
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="LocationName"/>
									</span>
								</td>
								<td>&nbsp;</td>
								<td>
									<input class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocationID" size="24" maxlength="15" border="0" value="<ffi:getProperty name="EditLocation" property="LocationID"/>">
									<br><span id="EditLocation.locationIDError"></span>
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="LocationID"/>
									</span>
								</td>
								<td>&nbsp;</td>
								<td>
									<s:checkbox name="EditLocation.Active" value="%{#session.EditLocation.Active}" fieldValue="true"/>&nbsp;
									<span class="<ffi:getPendingStyle fieldname="active" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/> adminBackground" width="120"><s:text name="jsp.default_28"/></span>
								</td>
							</tr>
						</table>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="3"><!-- Bank -->
							<tr  class="columndataauto" >
								<td colspan="4"><p class="transactionHeading"><s:text name="jsp.user_188"/></p></td>
							</tr>
							<tr class="columndataauto">
								<td class="sectionsubhead ltrow2_color" align="left" height="50" width="180">
									<input type="radio" name="EditLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="notEquals">checked</ffi:cinclude>>
									<s:text name="jsp.default_61"/>&nbsp;<ffi:getProperty name="TempBankIdentifierDisplayText"/><span class="required">*</span>:</span>
								</td>
								<td class="sectionsubhead ltrow2_color" width="230" height="50" valign="middle">
									<input id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value); AddEditLocationForm['EditLocation.CustomLocalBank'][0].checked = true;">
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="LocalRoutingNumber"/>
									</span>
								</td>
								<td class="sectionsubhead ltrow2_color" width="270">
									<span class="ffivisible marginRight10"><s:text name="jsp.default_306" /><span>   &nbsp; &nbsp; &nbsp; &nbsp;
									<input id="BANKNAME" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="AddLocation" property="LocalBankName"/>">
								</td>
								<td class="sectionsubhead ltrow2_color">
									<sj:a button="true" title="%{getText('jsp.default_373')}" onClickTopics="locationEdit_selectBankTopics"><s:text name="jsp.default_373" /></sj:a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;<span id="EditLocation.localRoutingNumberError"></span></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4" height="50">
									<input type="radio" name="EditLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID($('#selectBankID').val());" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
									<select id="selectBankID" class="txtbox" name="EditLocation.LocalBankID" onchange="updateAccountsListByID($('#selectBankID').val()); AddEditLocationForm['EditLocation.CustomLocalBank'][1].checked = true;">
										<ffi:list collection="AffiliateBanks" items="Bank">
											<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${EditLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
												<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
											</option>
										</ffi:list>
									</select>
									<span class="sectionhead_greyDA">
										<ffi:list collection="AffiliateBanks" items="Bank">
											<ffi:cinclude value1="${Bank.AffiliateBankID}" value2="${oldDAObject.LocalBankID}" operator="equals">
												<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
											</ffi:cinclude>
										</ffi:list>
									</span>
								</td>
							</tr>
							<!-- Bank  -->
							<tr>
								<td>&nbsp;</td>
							</tr>
							<!-- Account Information  -->
							<tr class="columndataauto">
								<td colspan="4">
								<table  width="80%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="sectionsubhead ltrow2_color" align="left"  height="50" width="160">
										<input type="radio" name="EditLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
										&nbsp;<span class="<ffi:getPendingStyle fieldname="localAccountNumber" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_19"/>:</span>
									</td>
									<td class="sectionsubhead ltrow2_color" width="250">
										<input class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="EditLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['EditLocation.CustomLocalAccount'][0].checked = true;">
										<span class="sectionhead_greyDA">
											<ffi:getProperty name="oldDAObject" property="LocalAccountNumber"/>
										</span>
									</td>
									<td class="sectionsubhead ltrow2_color"  width="100">
										<span class="<ffi:getPendingStyle fieldname="localAccountType" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_20"/>:</span>
									</td>
									<td class="sectionsubhead ltrow2_color">
										<select id="selectAccountTypeID" class="txtbox" name="EditLocation.LocalAccountType">
											<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
											<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
										</select>
										<span class="sectionhead_greyDA">
											<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
												<s:text name="jsp.user_64"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
												<s:text name="jsp.user_281"/>
											</ffi:cinclude>
										</span>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;<span id="EditLocation.localAccountNumberError"></span></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td colspan="4" height="50">
										<input type="radio" name="EditLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
										&nbsp;&nbsp;
										<select id="selectAccountID" class="txtbox" name="EditLocation.LocalAccountID" onchange="AddEditLocationForm['EditLocation.CustomLocalAccount'][1].checked = true;">
											<option value=""><s:text name="jsp.default_296"/></option>
										</select><span id="EditLocation.LocalAccountIDError"></span>
										<span class="sectionhead_greyDA">
											<ffi:getProperty name="oldDAObject" property="LocalAccountID"/>
										</span>
									</td>
								</tr>
								</table>
								</td>
							</tr>
							<!-- Account Information  -->
						</table>
						
						<!-- Remote Bank information -->
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr  class="columndataauto" >
								<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_276"/></p></td>
							</tr>
							<tr class="columndataauto" >
								<td><span class="<ffi:getPendingStyle fieldname="cashConCompanyBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_55"/>:</span></td>
								<td><span class="<ffi:getPendingStyle fieldname="concAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_75"/>:</span></td>
								<td><span class="<ffi:getPendingStyle fieldname="disbAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_117"/>:</span></td>
							</tr>
							<tr class="columndataauto" >
								<td>
										<select id="selectCashConCompanyID" class="txtbox" name="EditLocation.CashConCompanyBPWID" 
											onchange="updateCCAccountsList(this.options[this.selectedIndex].value)"
											<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>
											>
											
										<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
											<option value=""><s:text name="jsp.default_296"/></option>
										</ffi:cinclude>
										<%-- Disabled CashConCompany will not be shown in collection, so display now as invalid. --%>
										<ffi:cinclude value1="${CashConCompany.Active}" value2="false" operator="equals">
										<option value=""><ffi:getProperty name="CashConCompany" property="CompanyName"/> (<s:text name="jsp.user_116"/>)</option>
										</ffi:cinclude>
												<ffi:list collection="CashConCompanies" items="tempCompany">
													<option 
													value="<ffi:getProperty name="tempCompany" property="BPWID"/>" 
													SameDayCashConEnabled="<ffi:getProperty name="SameDayCashConEnabled"/>"  
													ConcSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="ConcSameDayCutOffDefined"/>"
													ConcCutOffsPassed="<ffi:getProperty name="tempCompany" property="ConcCutOffsPassed"/>"
													DepositSameDayPrenote="<ffi:getProperty name="EditLocation" property="DepositSameDayPrenote"/>"
													DisbSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="DisbSameDayCutOffDefined"/>"
													DisbCutOffsPassed="<ffi:getProperty name="tempCompany" property="DisbCutOffsPassed"/>"
													DisbursementSameDayPrenote="<ffi:getProperty name="EditLocation" property="DisbursementSameDayPrenote"/>"
													DepPrenoteStatus="<ffi:getProperty name="EditLocation" property="DepPrenoteStatus"/>"
													DisPrenoteStatus="<ffi:getProperty name="EditLocation" property="DisPrenoteStatus"/>"
													CCLocationPrenoteStatus="<ffi:getProperty name="CCLocationPrenoteStatus"/>"
													DepositPrenote="<ffi:getProperty name="EditLocation" property="DepositPrenote"/>"
													DisbursementPrenote="<ffi:getProperty name="EditLocation" property="DisbursementPrenote"/>"
													<ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="tempCompany" property="CompanyName"/>
													</option>
												</ffi:list>
										</select>
										<span class="sectionhead_greyDA">
											<ffi:list collection="CashConCompanies" items="tempCompany">
												<ffi:cinclude value1="${oldDAObject.CashConCompanyBPWID}" value2="${tempCompany.BPWID}">
													<ffi:getProperty name="tempCompany" property="CompanyName"/>
												</ffi:cinclude>
											</ffi:list>
										</span>
								</td>
								<td>
									<s:if test="#session.oldDAObject.cashConCompanyBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' " >
										<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
										<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${oldDAObject.CashConCompanyBPWID}"/>
										<ffi:process name="SetCashConCompany"/>
									</s:if>
									<s:elseif test="#session.oldDAObject.disbAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
										<ffi:setProperty name="oldDAObject" property="CashConCompanyBPWID" value="CashConCompanies"/>
										<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
										<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
										<ffi:process name="SetCashConCompany"/>
									</s:elseif>
									<s:elseif test="#session.oldDAObject.concAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
										<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
										<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
										<ffi:process name="SetCashConCompany"/>
									</s:elseif>
									
									<select id="selectConcAccountID" class="txtbox" name="EditLocation.ConcAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
										<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
											<option value="" selected><s:text name="jsp.default_296"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
											<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
												<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
													<ffi:getProperty name="ConcAccount" property="DisplayText"/>
													<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
														 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
													 </ffi:cinclude>
												</option>
											</ffi:list>
										</ffi:cinclude>
									</select>
									<span class="sectionhead_greyDA">
										<ffi:list collection="OldCashConCompany.ConcAccounts" items="ConcAccount">
											<ffi:cinclude value1="${oldDAObject.ConcAccountBPWID}" value2="${ConcAccount.BPWID}">
													<ffi:getProperty name="ConcAccount" property="DisplayText"/>
														<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
															 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
														</ffi:cinclude>
											</ffi:cinclude>
										</ffi:list>
									</span>				
								</td>
								<td>
									<select id="selectDisbAccountID" class="txtbox" name="EditLocation.DisbAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
										<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
											<option value="" selected><s:text name="jsp.default_296"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
											<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
												<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
													<ffi:getProperty name="DisbAccount" property="DisplayText"/>
													<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
														 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
													 </ffi:cinclude>
												</option>
											</ffi:list>
										</ffi:cinclude>
									</select>
									<span class="sectionhead_greyDA">
										<ffi:list collection="OldCashConCompany.DisbAccounts" items="DisbAccount">
											<ffi:cinclude value1="${oldDAObject.DisbAccountBPWID}" value2="${DisbAccount.BPWID}">
													<ffi:getProperty name="DisbAccount" property="DisplayText"/>
													<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
														 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
													</ffi:cinclude>
											</ffi:cinclude>
										</ffi:list>
									</span>
								</td>
							</tr>
						</table>
						<!-- Remote Bank information -->	
						
						<!-- Deposit-specific information -->
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr  class="columndataauto" >
								<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_113"/></p></td>
							</tr>
							<tr class="columndataauto" >
								<td><span class="<ffi:getPendingStyle fieldname="depositMinimum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_112"/>:</span></td>
								<td><span class="<ffi:getPendingStyle fieldname="depositMaximum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_111"/>:</span></td>
								<td><span class="<ffi:getPendingStyle fieldname="anticDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_44"/>:</span></td>
							</tr>
							<tr class="columndataauto" >
								<td>
									<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
									<input class="ui-widget-content ui-corner-all" id="DepositMinimum" type="text" name="EditLocation.DepositMinimum" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="DepositMinimum"/>
									</span>
									<br><span id="depositMinimumError"></span>
								</td>
								<td>
									<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
									<input class="ui-widget-content ui-corner-all" id="DepositMaximum" type="text" name="EditLocation.DepositMaximum" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="DepositMaximum"/>
									</span>
									<br><span id="depositMaximumError"></span>
								</td>
								<td>
									<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
									<input class="ui-widget-content ui-corner-all" id="AnticDeposit" type="text" name="EditLocation.AnticDeposit" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="AnticDepositString"/>
									</span>
									<br><span id="anticDepositError"></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">&nbsp;</td>
							</tr>
							<tr class="columndataauto" >
								<td><span class="<ffi:getPendingStyle fieldname="threshDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_337"/>:</span></td>
								<td></td>
								<td></td>
							</tr>
							<tr class="columndataauto" >
								<td>
									<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
									<input class="ui-widget-content ui-corner-all" id="ThreshDeposit" type="text" name="EditLocation.ThreshDeposit" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
									<span class="sectionhead_greyDA">
										<ffi:getProperty name="oldDAObject" property="ThreshDepositString"/>
									</span>
									<br><span id="threshDepositError"></span>
								</td>
								<td>
									<s:checkbox id="ConsolidateDeposits" name="EditLocation.ConsolidateDeposits" value="%{#session.EditLocation.ConsolidateDeposits}" fieldValue="true"/>
									&nbsp;<span class="<ffi:getPendingStyle fieldname="consolidateDeposits" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_94"/></span>
								</td>
								<td id="regularDepositPrenoteCheckboxClass" class="displayNone">
									 <div id="regularDepositPrenoteCheckbox">
										 <s:checkbox id="regularDepositPrenote" name="EditLocation.DepositPrenote" value="%{#session.EditLocation.DepositPrenote}" fieldValue="true" /> 
										 <span class="<ffi:getPendingStyle fieldname="depositPrenote" 
										 defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
										 <s:text name="jsp.user_110"/></span>
									 </div>
								 </td>
							</tr>
							<tr class="columndataauto" >
								<td id="depositSameDayPrenoteClass" colspan="3" class="displayNone">
											<table width="84%">
											<tr>
												<td class="<ffi:getPendingStyle fieldname="depositPrenote" 
												 defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
													<div id="depositPrenoteLabel" >
															<span class="<ffi:getPendingStyle fieldname="depositSameDayPrenote" 
															defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
															<s:text name="jsp.user_110"/>
														</span>
													</div>	
												</td>
												 <td>
													 <div id="depositPrenoteCheckbox" >
													 	<span>
														 	<s:checkbox id="DepositPrenote" name="EditLocation.DepositPrenote" 
														 	value="%{#session.EditLocation.DepositPrenote}" fieldValue="true" 
														 	onchange="removeDepositSameDayPrenoteChecked(this);"/>
													 	</span><span><s:text name="jsp.user_571"/></span>
													</div>		
												 </td>
											
												 <td>
													  <div id="depositSameDayPrenoteCheckbox" >
													  <span>
													 	<s:checkbox id="DepositSameDayPrenote" name="EditLocation.DepositSameDayPrenote" 
													 	value="%{#session.EditLocation.DepositSameDayPrenote}"
													 	fieldValue="true" onchange="removeDepositNormalPrenoteChecked(this);"/>
													 </span><span><s:text name="jsp.user_572"/></span>	
												 	 </div>
												 </td>
												
											</tr>
											<tr class="columndataauto" >
												<td colspan="3">
													<table width="100%">
														<tr>
															<td>
																<div id="showConcNote" >
																	<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>

										</table>
										</td>
									</tr>
									<tr class="columndataauto" >
										<td colspan="3">
											<table width="100%">
												<tr>
													<td>
														<ffi:cinclude value1="${EditLocation.DepPrenoteStatus}" value2="" operator="notEquals">
															<div>
																<span>
																	<s:text name="jsp.user_574"/>:
																</span>
																<span class="sectionhead_greyDA"><ffi:getProperty name="EditLocation" property="DepPrenoteStatus"/></span>
															</div>
														</ffi:cinclude>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									 
							</tr>
						</table>
						<!-- Deposit-specific information -->	
						
						<!-- Disbursement-Request-Specific-Information -->
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr  class="columndataauto" >
								<td><p class="transactionHeading"><s:text name="jsp.user_119"/></p></td>
							</tr>
							<tr class="columndataauto" >
								<td id="regularDisbursementPrenoteCheckboxClass" class="displayNone">
									 <div id="regularDisbursementPrenoteCheckbox" >
										<span> <s:checkbox id="regularDisbursementPrenote" name="EditLocation.DisbursementPrenote" value="%{#session.EditLocation.DisbursementPrenote}" fieldValue="true" /> </span>
										<span class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_118"/></span>
									 </div>
								</td>
							</tr>
							<tr class="columndataauto" >
							<td id="disbursementSameDayPrenoteClass" colspan="3" class="displayNone">
									    <table width="70%">
									    	<tr>
										    	<td class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
													<div id="disbursementPrenoteLabel" >
														<span class="<ffi:getPendingStyle fieldname="disbursementSameDayPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_118"/></span>
													</div>
												</td>
												<td>
													 <div id="disbursementPrenoteCheckbox" >
													 	<span><s:checkbox id="DisbursementPrenote" name="EditLocation.DisbursementPrenote" 
																value="%{#session.EditLocation.DisbursementPrenote}" fieldValue="true" 
																onchange="removeDisbursementSameDayPrenoteChecked(this);"/>
														</span><span><s:text name="jsp.user_571"/></span>		
					 								 </div>	
												</td>
												
												<td>
													  <div id="disbursementSameDayPrenoteCheckbox" >
													  <span>
													  	<s:checkbox id="DisbursementSameDayPrenote" name="EditLocation.DisbursementSameDayPrenote" 
														value="%{#session.EditLocation.DisbursementSameDayPrenote}" fieldValue="true" 
														onchange="removeDisbursementNormalPrenoteChecked(this);"/>
													  </span><span><s:text name="jsp.user_572"/></span>
												  </div>
												</td>
												
											</tr>
											<tr class="columndataauto" >
												<td colspan="3">
													<table width="100%">
														<tr>
															<td>
																<div id="showDisbNote" >
																<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span></div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											
									    	</table>
									    </td>
									</tr>
									<tr class="columndataauto" >
										<td colspan="3">
											<table width="100%">
												<tr>
													<td>
														<div>
															<ffi:cinclude value1="${EditLocation.DisPrenoteStatus}" value2="" operator="notEquals">
															<span class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
																<s:text name="jsp.user_575"/>:
															</span>
															<span class="sectionhead_greyDA"><ffi:getProperty name="EditLocation" property="DisPrenoteStatus"/></span>
															</ffi:cinclude>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
						</table>
						<!-- Disbursement-Request-Specific-Information -->
								
						<table width="750" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" class="adminBackground">
									
										<div align="center">
											<%-- <table width="711" border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td class="adminBackground" nowrap width="120">
														<div align="right"><span class="<ffi:getPendingStyle fieldname="locationName" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_267"/>:</span></div>
													</td>
													<td class="adminBackground" width="120">
														<input class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocationName" size="24" maxlength="16" border="0" value="<ffi:getProperty name="EditLocation" property="LocationName"/>">
														<span id="EditLocation.locationNameError"></span>
														<span class="sectionhead_greyDA">
															<ffi:getProperty name="oldDAObject" property="LocationName"/>
														</span>
													</td>
													<td class="adminBackground" nowrap width="120">
														<div align="right"> <span class="<ffi:getPendingStyle fieldname="locationID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_189"/>:</span></div>
													</td>
													<td class="adminBackground" width="120">
														<input class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocationID" size="24" maxlength="15" border="0" value="<ffi:getProperty name="EditLocation" property="LocationID"/>">
														<span id="EditLocation.locationIDError"></span>
														<span class="sectionhead_greyDA">
															<ffi:getProperty name="oldDAObject" property="LocationID"/>
														</span>
													</td>
													<td class="adminBackground" width="120">
														<div align="right">
															<s:checkbox name="EditLocation.Active" value="%{#session.EditLocation.Active}" fieldValue="true"/>
														</div>
													</td>
													<td class="<ffi:getPendingStyle fieldname="active" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/> adminBackground" width="120"><s:text name="jsp.default_28"/></td>
												</tr>
											</table> --%>
											
											<%-- <table border="0" cellspacing="0" cellpadding="3" width="710">
												<tr>
													<td colspan="4" class="adminBackground tbrd_b">
														<span class="sectionhead"><s:text name="jsp.user_188"/></span><br>
													</td>
													<td class="adminBackground">&nbsp;</td>
													<td class="adminBackground">&nbsp;</td>
													<td class="tbrd_b adminBackground" colspan="4">
														<span class="sectionhead"><s:text name="jsp.user_113"/></span>
													</td>
												</tr>
												<tr valign="middle">
													<td class="adminBackground" colspan="4">
														<div align="left"><span class="<ffi:getPendingStyle fieldname="customLocalBank" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_61"/></span></div>
													</td>
													<td valign="top" class="tbrd_r adminBackground" rowspan="13">&nbsp;</td>
													<td valign="top" class="adminBackground" rowspan="13">&nbsp;</td>
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="depositMinimum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_112"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<div align="left">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
															<input class="ui-widget-content ui-corner-all" id="DepositMinimum" type="text" name="EditLocation.DepositMinimum" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
															<span id="depositMinimumError"></span>
															<span class="sectionhead_greyDA">
																<ffi:getProperty name="oldDAObject" property="DepositMinimum"/>
															</span>
														</div>
													</td>
												</tr>
												<tr valign="middle">
													<td>
														<div align="left">
															&nbsp;&nbsp;<input type="radio" name="EditLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="notEquals">checked</ffi:cinclude>>
														</div>
													</td>
													<td nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="localRoutingNumber" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>">
														  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
														:</span></div>
													</td>
													<td>
														<div align="left">
															<input id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="EditLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['EditLocation.LocalRoutingNumber'].value); AddEditLocationForm['EditLocation.CustomLocalBank'][0].checked = true;">
															<span id="EditLocation.localRoutingNumberError"></span>
															<span class="sectionhead_greyDA">
																<ffi:getProperty name="oldDAObject" property="LocalRoutingNumber"/>
															</span>
														</div>
													</td>
													<td>
														<sj:a button="true" title="%{getText('jsp.default_373')}" onClickTopics="locationEdit_selectBankTopics"><s:text name="jsp.default_373" /></sj:a>
													</td>
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="depositMaximum" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_111"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<div align="left">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
															<input class="ui-widget-content ui-corner-all" id="DepositMaximum" type="text" name="EditLocation.DepositMaximum" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
															<span id="depositMaximumError"></span>
															<span class="sectionhead_greyDA">
																<ffi:getProperty name="oldDAObject" property="DepositMaximum"/>
															</span>
														</div>
													</td>
												</tr>
												<tr valign="middle">
													<td>&nbsp;</td>
													<td nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="localBankName" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_63"/>:</span></div>
													</td>
													<td>
														<input id="BANKNAME" class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="EditLocation" property="LocalBankName"/>">
														<span id="EditLocation.localBankNameError"></span>
														<span class="sectionhead_greyDA">
															<ffi:getProperty name="oldDAObject" property="LocalBankName"/>
														</span>
													</td>
													<td></td>
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="anticDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_44"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<div align="left">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
															<input class="ui-widget-content ui-corner-all" id="AnticDeposit" type="text" name="EditLocation.AnticDeposit" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
														</div>
														<span id="anticDepositError"></span>
														<span class="sectionhead_greyDA">
															<ffi:getProperty name="oldDAObject" property="AnticDepositString"/>
														</span>
													</td>
												</tr>
												<tr valign="middle">
													<td>
														&nbsp;&nbsp;<input type="radio" name="EditLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID($('#selectBankID').val());" <ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
													</td>
													<td colspan="3">
														<select id="selectBankID" class="txtbox" name="EditLocation.LocalBankID" onchange="updateAccountsListByID($('#selectBankID').val()); AddEditLocationForm['EditLocation.CustomLocalBank'][1].checked = true;">
															<ffi:list collection="AffiliateBanks" items="Bank">
																<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${EditLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
																	<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
																</option>
															</ffi:list>
														</select>
														<span class="sectionhead_greyDA">
															<ffi:list collection="AffiliateBanks" items="Bank">
																<ffi:cinclude value1="${Bank.AffiliateBankID}" value2="${oldDAObject.LocalBankID}" operator="equals">
																	<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
																</ffi:cinclude>
															</ffi:list>
														</span>
													</td>
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="threshDeposit" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_337"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<div align="left">
															<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
															<input class="ui-widget-content ui-corner-all" id="ThreshDeposit" type="text" name="EditLocation.ThreshDeposit" size="18" maxlength="11" border="0" value="<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="" operator="notEquals"><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/></ffi:cinclude>">
															<span id="threshDepositError"></span>
															<span class="sectionhead_greyDA">
																<ffi:getProperty name="oldDAObject" property="ThreshDepositString"/>
															</span>
														</div>
													</td>
												</tr>
												<tr valign="middle">
													<td class="adminBackground" colspan="4">
														<div align="left"> <span class="<ffi:getPendingStyle fieldname="customLocalAccount" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_15"/></span></div>
													</td>
													<td>
														<s:checkbox id="ConsolidateDeposits" name="EditLocation.ConsolidateDeposits" value="%{#session.EditLocation.ConsolidateDeposits}" fieldValue="true"/>
													</td>
													<td colspan="3" align="left"><span class="<ffi:getPendingStyle fieldname="consolidateDeposits" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_94"/></span></td>
												</tr>
												<tr valign="middle">
													<td>
														<div align="left">
															&nbsp;&nbsp;<input type="radio" name="EditLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
														</div>
													</td>
													<td nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="localAccountNumber" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_19"/>:</span></div>
													</td>
													<td>
														<div align="left">
															<input class="ui-widget-content ui-corner-all" type="text" name="EditLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="EditLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['EditLocation.CustomLocalAccount'][0].checked = true;">
															<span id="EditLocation.localAccountNumberError"></span>
															<span class="sectionhead_greyDA">
																<ffi:getProperty name="oldDAObject" property="LocalAccountNumber"/>
															</span>
														</div>
													</td>
													<td>&nbsp; </td>
													<td>
														<s:checkbox id="DepositPrenote" name="EditLocation.DepositPrenote" value="%{#session.EditLocation.DepositPrenote}" fieldValue="true"/>
													</td>
													<td colspan="3" align="left"><span class="<ffi:getPendingStyle fieldname="depositPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_110"/></span></td>
												</tr>
												<tr valign="middle">
													<td>&nbsp;</td>
													<td nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="localAccountType" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.default_20"/>:</span></div>
													</td>
													<td>
														<select id="selectAccountTypeID" class="txtbox" name="EditLocation.LocalAccountType">
															<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
															<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${EditLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
														</select>
														<span class="sectionhead_greyDA">
																<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
																	<s:text name="jsp.user_64"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${oldDAObject.LocalAccountTypeString}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
																	<s:text name="jsp.user_281"/>
																</ffi:cinclude>
														</span>
													</td>
													<td></td>
													<td colspan="4">&nbsp;</td>
												</tr>
												<tr valign="middle">
													<td>
														&nbsp;&nbsp;<input type="radio" name="EditLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
													</td>
													<td colspan="3">
														<select id="selectAccountID" class="txtbox" name="EditLocation.LocalAccountID" onchange="AddEditLocationForm['EditLocation.CustomLocalAccount'][1].checked = true;">
															<option value=""><s:text name="jsp.default_296"/></option>
														</select><span id="EditLocation.LocalAccountIDError"></span>
														<span class="sectionhead_greyDA">
															<ffi:getProperty name="oldDAObject" property="LocalAccountID"/>
														</span>
													</td>
													<td colspan="4" class="tbrd_b"><span class="sectionhead"><s:text name="jsp.user_119"/></span></td>
												</tr>
												<tr valign="middle">
													<td colspan="4">&nbsp;</td>
													<td>
														<s:checkbox id="DisbursementPrenote" name="EditLocation.DisbursementPrenote" value="%{#session.EditLocation.DisbursementPrenote}" fieldValue="true"/>
													</td>
													<td colspan="3" align="left" nowrap><span class="<ffi:getPendingStyle fieldname="disbursementPrenote" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_118"/></span></td>
												</tr>
												<tr valign="middle">
													<td class="tbrd_b adminBackground" colspan="4"><span class="sectionhead"><s:text name="jsp.user_276"/></span></td>
													<td>&nbsp;</td>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr valign="middle">
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="cashConCompanyBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_55"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<select id="selectCashConCompanyID" class="txtbox" name="EditLocation.CashConCompanyBPWID" onchange="updateCCAccountsList(this.options[this.selectedIndex].value)" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
														<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
														    <option value=""><s:text name="jsp.default_296"/></option>
														</ffi:cinclude>
			Disabled CashConCompany will not be shown in collection, so display now as invalid.
			<ffi:cinclude value1="${CashConCompany.Active}" value2="false" operator="equals">
				    <option value=""><ffi:getProperty name="CashConCompany" property="CompanyName"/> (<s:text name="jsp.user_116"/>)</option>
			</ffi:cinclude>
															<ffi:list collection="CashConCompanies" items="tempCompany">
																<option value="<ffi:getProperty name="tempCompany" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
																	<ffi:getProperty name="tempCompany" property="CompanyName"/>
																</option>
															</ffi:list>
														</select>
														<span class="sectionhead_greyDA">
															<ffi:list collection="CashConCompanies" items="tempCompany">
																<ffi:cinclude value1="${oldDAObject.CashConCompanyBPWID}" value2="${tempCompany.BPWID}">
																	<ffi:getProperty name="tempCompany" property="CompanyName"/>
																</ffi:cinclude>
															</ffi:list>
														</span>
													</td>
													<td>&nbsp;</td>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr valign="middle">
													<s:if test="#session.oldDAObject.cashConCompanyBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' " >
														<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
														<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
														<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
														<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${oldDAObject.CashConCompanyBPWID}"/>
														<ffi:process name="SetCashConCompany"/>
													</s:if>
													<s:elseif test="#session.oldDAObject.disbAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
														<ffi:setProperty name="oldDAObject" property="CashConCompanyBPWID" value="CashConCompanies"/>
														<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
														<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
														<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
														<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
														<ffi:process name="SetCashConCompany"/>
													</s:elseif>
													<s:elseif test="#session.oldDAObject.concAccountBPWID != null && #session.oldDAObject.cashConCompanyBPWID != '' ">
														<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
														<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
														<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>
														<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${EditLocation.CashConCompanyBPWID}"/>
														<ffi:process name="SetCashConCompany"/>
													</s:elseif>										
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="concAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_75"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<select id="selectConcAccountID" class="txtbox" name="EditLocation.ConcAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
															<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
																<option value="" selected><s:text name="jsp.default_296"/></option>
															</ffi:cinclude>
															<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
																<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
																	<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																		<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																		<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																			 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																		 </ffi:cinclude>
																	</option>
																</ffi:list>
															</ffi:cinclude>
														</select>
														<span class="sectionhead_greyDA">
															<ffi:list collection="OldCashConCompany.ConcAccounts" items="ConcAccount">
																<ffi:cinclude value1="${oldDAObject.ConcAccountBPWID}" value2="${ConcAccount.BPWID}">
																		<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																			<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																				 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
						 													</ffi:cinclude>
																</ffi:cinclude>
															</ffi:list>
														</span>
													</td>
													<td>&nbsp;</td>
													<td colspan="3">&nbsp;</td>
												</tr>
			
												<tr valign="middle">
													<td class="adminBackground" colspan="2" nowrap>
														<div align="right"><span class="<ffi:getPendingStyle fieldname="disbAccountBPWID" defaultcss="sectionsubhead"  dacss="sectionsubheadDA"/>"><s:text name="jsp.user_117"/>:</span></div>
													</td>
													<td class="adminBackground" colspan="2">
														<select id="selectDisbAccountID" class="txtbox" name="EditLocation.DisbAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
															<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
																<option value="" selected><s:text name="jsp.default_296"/></option>
															</ffi:cinclude>
															<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
																<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
																	<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${EditLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																		<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																		<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																			 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																		 </ffi:cinclude>
																	</option>
																</ffi:list>
															</ffi:cinclude>
														</select>
														<span class="sectionhead_greyDA">
															<ffi:list collection="OldCashConCompany.DisbAccounts" items="DisbAccount">
																<ffi:cinclude value1="${oldDAObject.DisbAccountBPWID}" value2="${DisbAccount.BPWID}">
																		<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																		<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																			 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																 		</ffi:cinclude>
																</ffi:cinclude>
															</ffi:list>
														</span>
													</td>
													<td>&nbsp;</td>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr valign="middle">
													<td width="25">&nbsp;</td>
													<td width="110">&nbsp;</td>
													<td width="100">&nbsp;</td>
													<td width="75">&nbsp;</td>
													<td valign="top" class="adminBackground">&nbsp;</td>
													<td valign="top" class="adminBackground">&nbsp;</td>
													<td width="25">&nbsp;</td>
													<td width="100">&nbsp;</td>
													<td width="100">&nbsp;</td>
													<td width="65">&nbsp;</td>
												</tr>
											</table> --%>
											<div align="center">
											<ffi:cinclude value1="${fromDivEdit}" value2="true" operator="equals">
												<s:url id="resetEditLocButtonUrl" value="/pages/jsp/user/corpadminlocedit.jsp"> 
														<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>	
														<s:param name="editlocation-reload" value="false"></s:param>											
												</s:url>	
												<sj:a id="resetEditLocDialogBtn"
					                                href="%{resetEditLocButtonUrl}"
													targets="viewLocationDialogID"
													button="true"><s:text name="jsp.default_358"/></sj:a>
												<sj:a
												   button="true"
												   onClickTopics="closeDialog"><s:text name="jsp.default_82"/></sj:a>
												<sj:a id="verifyEditSubmit"
													formIds="AddEditLocationFormID"
													targets="viewLocationDialogID"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify"
													onCompleteTopics="completeVerify"
													onErrorTopics="errorVerify"
													onSuccessTopics="successVerify"
													><s:text name="jsp.user_2"/></sj:a>
											</ffi:cinclude>
											<ffi:cinclude value1="${fromDivEdit}" value2="true" operator="notEquals">
												<s:url id="resetEditLocButtonUrl" value="/pages/jsp/user/corpadminlocedit.jsp"> 
														<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>	
														<s:param name="editlocation-reload" value="false"></s:param>											
												</s:url>	
												<sj:a id="resetEditLocBtn"
					                                href="%{resetEditLocButtonUrl}"
													targets="inputDiv"
													button="true"><s:text name="jsp.default_358"/></sj:a>
												<sj:a
												   button="true"
												   summaryDivId="summary" buttonType="cancel"
												   onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
												<sj:a id="verifyEditSubmit"
													formIds="AddEditLocationFormID"
													targets="verifyDiv"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify"
													onCompleteTopics="completeVerify"
													onErrorTopics="errorVerify"
													onSuccessTopics="successVerify"
													><s:text name="jsp.user_2"/></sj:a>
											</ffi:cinclude>
											</div>
										</div>
									</td>
								</tr>
							</table>
					     </div>
					  </s:form>
					</td>
				</tr>
			</table>			
		</div>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="OldCashConCompany"/>
<ffi:removeProperty name="Location"/>
