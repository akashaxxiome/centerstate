<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-manageUsers"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.user.preferences.manageUsersTabName" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/homeSubmenuDropdown.jsp" />
    	</div>
    	 <!-- dashboard items listing for messages -->
        <div id="dbUsersSummary" class="dashboardSummaryHolderDiv">
        
        <s:url id="goBackSummaryUrl" value="/pages/jsp/user/secondary-users-grid.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
        
        	<sj:a id="goBackSummaryLink" href="%{goBackSummaryUrl}"
				button="true" cssClass="summaryLabelCls" targets="summary"
				title="%{getText('jsp.billpay_go_back_summary')}"
				onClickTopics="openUsersSummary" >
				  	<s:text name="jsp.billpay_summary" />
			</sj:a>
			
        	<sj:a
				id="addSecondaryUserBtn"
				button="true"><s:text name="jsp.user_28"/></sj:a>
        </div>
</div>
	<%-- <span class="ui-helper-clearfix">&nbsp;</span> --%>	
<script>
$("#goBackSummaryLink").find("span").addClass("dashboardSelectedItem");
</script>