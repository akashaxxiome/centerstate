<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:removeProperty name="RecPayments"/>
<ffi:removeProperty name="Payments"/>
<ffi:removeProperty name="PendingTransfers"/>
<ffi:removeProperty name="RecTransfers"/>

<%-- Save in memory the state of the active checkboxes on the user list page. --%>
<ffi:setProperty name="FFISetSecondaryUsersActiveState" property="SuccessURL" value=""/>
<ffi:process name="FFISetSecondaryUsersActiveState"/>



<ffi:object name="com.ffusion.tasks.banking.SignOn" id="UserStatusChangeBankingSignOn" scope="session"/>
<ffi:setProperty name="UserStatusChangeBankingSignOn" property="ServiceName" value="UserStatusChangeBankingService"/>
<ffi:setProperty name="UserStatusChangeBankingSignOn" property="UserName" value="${TransID}"/>
<ffi:setProperty name="UserStatusChangeBankingSignOn" property="Password" value="${TransPassword}"/>
<ffi:process name="UserStatusChangeBankingSignOn"/>


<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<%-- Retrieve collections of pending transactions that may have been created by the user to be deleted. --%>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
    <%-- Initialize tasks and data structures necessary for determining if the user to be deleted has any pending payments. --%>
    <%-- 
	    <ffi:object name="com.ffusion.tasks.billpay.Initialize" id="BillPayInitService" scope="request"/>
	    <ffi:setProperty name="BillPayInitService" property="ClassName" value="com.ffusion.services.billpay.BillPayServiceImpl"/>
	    <ffi:setProperty name="BillPayInitService" property="InitURL" value="billpay.xml"/>
	    <ffi:process name="BillPayInitService"/>
    --%>

    <ffi:object name="com.ffusion.tasks.billpay.SignOn" id="BillPaySignOn" scope="request"/>
    <ffi:setProperty name="BillPaySignOn" property="UserName" value="${TransID}"/>
    <ffi:setProperty name="BillPaySignOn" property="Password" value="${TransPassword}"/>
    <ffi:process name="BillPaySignOn"/>

    <ffi:object name="com.ffusion.tasks.billpay.GetAccounts" id="GetBillPayAccounts" scope="request"/>
    <ffi:setProperty name="GetBillPayAccounts" property="AccountReload" value="true"/>
    <ffi:setProperty name="GetBillPayAccounts" property="UseAccounts" value="BankingAccounts" />
    <ffi:process name="GetBillPayAccounts" />

    <ffi:object name="com.ffusion.tasks.billpay.GetExtPayees" id="" scope="request"/>
    <ffi:setProperty name="" property="Reload" value="true"/>
    <ffi:process name="" />

    <ffi:object name="com.ffusion.tasks.billpay.GetPayments" id="GetPayments" scope="request"/>
    <ffi:setProperty name="GetPayments" property="Reload" value="true"/>
    <ffi:setProperty name="GetPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
    <ffi:process name="GetPayments" />

    <ffi:setProperty name="RecPayments" property="Locale" value="${UserLocale.Locale}" />
    <ffi:setProperty name="Payments" property="Locale" value="${UserLocale.Locale}" />
</ffi:cinclude>
<ffi:cinclude value1="${RecPayments}" value2="" operator="equals">
    <ffi:object name="com.ffusion.beans.billpay.RecPayments" id="RecPayments" scope="session"/>
</ffi:cinclude>
<ffi:cinclude value1="${Payments}" value2="" operator="equals">
    <ffi:object name="com.ffusion.beans.billpay.Payments" id="Payments" scope="session"/>
</ffi:cinclude>


<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
    <%-- Initialize tasks and data structures necessary for determining if the user to be deleted has any pending transfers. --%>
    <ffi:object name="com.ffusion.tasks.bptw.GetTransfers" id="GetRecTransfers" scope="request"/>
    <ffi:setProperty name="GetRecTransfers" property="Recurring" value="true"/>
    <ffi:setProperty name="GetRecTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:process name="GetRecTransfers"/>

    <ffi:object name="com.ffusion.tasks.bptw.GetTransfers" id="GetTransfers" scope="request"/>
    <ffi:setProperty name="GetTransfers" property="Recurring" value="false"/>
    <ffi:setProperty name="GetTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:process name="GetTransfers"/>

    <ffi:object name="com.ffusion.tasks.banking.UpdateTransferAccountInfo" id="UpdateRecTransfers" scope="request"/>
    <ffi:setProperty name="UpdateRecTransfers" property="AccountsSessionName" value="BankingAccounts"/>
    <ffi:setProperty name="UpdateRecTransfers" property="TransfersSessionName" value="RecTransfers"/>
    <ffi:process name="UpdateRecTransfers"/>

    <ffi:object name="com.ffusion.tasks.banking.UpdateTransferAccountInfo" id="UpdateTransfers" scope="request"/>
    <ffi:setProperty name="UpdateTransfers" property="AccountsSessionName" value="BankingAccounts"/>
    <ffi:setProperty name="UpdateTransfers" property="TransfersSessionName" value="Transfers"/>
    <ffi:process name="UpdateTransfers"/>
</ffi:cinclude>

<ffi:cinclude value1="${RecTransfers}" value2="" operator="equals">
    <ffi:object name="com.ffusion.beans.banking.RecTransfers" id="RecTransfers" scope="session"/>
</ffi:cinclude>

<ffi:cinclude value1="${Transfers}" value2="" operator="equals">
    <ffi:object name="com.ffusion.beans.banking.RecTransfers" id="Transfers" scope="session"/>
</ffi:cinclude>

<ffi:setProperty name="AddEditMode" value="EditMultiple"/>

<ffi:removeProperty name="BptwBanking"/>
<ffi:removeProperty name="TempURL"/>

<s:include value="/pages/jsp/user/user_statuschange_transactions.jsp" />

