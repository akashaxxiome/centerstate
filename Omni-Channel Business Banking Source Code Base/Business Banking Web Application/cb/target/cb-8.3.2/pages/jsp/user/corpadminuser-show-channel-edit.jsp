<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

  <s:iterator value="SelectedBizProfile.childProfiles"  var="profile">
  	
  	<s:if test="#profile.id != '-1'">
  <li>	<b>
  	  	<s:property value="#profile.channelId"/>
  	</b>
  	<span class="channelId-padding">&#40; <s:property value="#profile.description"/> &#41;</span>
  	  </li>
  	  </s:if>
	  <input type="hidden" name="<s:property value='#profile.channelId'/>" value="<s:property value='#profile.profileEntGroupId'/>"/>
  	
  </s:iterator>
  <s:if test="%{SelectedBizProfile.status == 'INACTIVE'}">
  <span class="required">Selected Profile is currently inactive. User will not be able to login till it's made active.</span>
  </s:if>
<input type="hidden" name="BusinessEmployee.EntitlementGroupId" value="<s:property value='SelectedBizProfile.ProfileEntGroupId' />"/></td>
 
