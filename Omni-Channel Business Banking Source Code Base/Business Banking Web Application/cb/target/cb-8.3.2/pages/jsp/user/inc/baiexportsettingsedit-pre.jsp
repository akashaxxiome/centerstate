<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name='PageText' value=''/>

<ffi:object id="BAIExportSettings" name="com.ffusion.tasks.user.GetBAIExportSettings" scope="session"/>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BAIExportSettings" property="SenderIDType" value="${SetBAIExportSettings.SenderIDType}"/>
	<ffi:setProperty name="BAIExportSettings" property="SenderIDCustom" value="${SetBAIExportSettings.SenderIDCustom}"/>
	<ffi:setProperty name="BAIExportSettings" property="ReceiverIDType" value="${SetBAIExportSettings.ReceiverIDType}"/>
	<ffi:setProperty name="BAIExportSettings" property="ReceiverIDCustom" value="${SetBAIExportSettings.ReceiverIDCustom}"/>
	<ffi:setProperty name="BAIExportSettings" property="UltimateReceiverIDType" value="${SetBAIExportSettings.UltimateReceiverIDType}"/>
	<ffi:setProperty name="BAIExportSettings" property="UltimateReceiverIDCustom" value="${SetBAIExportSettings.UltimateReceiverIDCustom}"/>
	<ffi:setProperty name="BAIExportSettings" property="OriginatorIDType" value="${SetBAIExportSettings.OriginatorIDType}"/>
	<ffi:setProperty name="BAIExportSettings" property="OriginatorIDCustom" value="${SetBAIExportSettings.OriginatorIDCustom}"/>
	<ffi:setProperty name="BAIExportSettings" property="CustomerAccountNumberType" value="${SetBAIExportSettings.CustomerAccountNumberType}"/>
</ffi:cinclude>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
	<%--get the company's BAI export preferences--%>
	<ffi:setProperty name="BAIExportSettings" property="BusinessSessionName" value="Business"/>
	<ffi:process name="BAIExportSettings"/>
</ffi:cinclude>

<%-- Dual Approval Processing --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object name="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA" id="AddBAIExportSettingsToDA" scope="session" />
	<ffi:setProperty name="AddBAIExportSettingsToDA" property="SwapInstances" value="true" />	 
	<ffi:process name="AddBAIExportSettingsToDA" />	
</ffi:cinclude>

<%-- End Dual Approval Processing --%>

<ffi:removeProperty name="UseLastRequest"/>