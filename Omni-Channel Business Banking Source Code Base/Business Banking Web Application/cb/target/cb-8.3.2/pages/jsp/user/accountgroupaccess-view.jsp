<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div id="accountgroupaccess-viewDiv" class="remoteAcctGroupAccess">

<ffi:help id="user_accountgroupaccess-view" className="moduleHelpClass"/>
<ffi:setProperty name="confirmation_done" value="false"/>

<s:include value="%{#session.PagesPath}user/inc/SetPageText.jsp"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<s:include value="%{#session.PagesPath}user/inc/accountgroupaccess-init.jsp"/>

<%    
	/*--------------------------------------------------------------------------
     * Various placeholder and state variables.
     */
	String noAcctGrpsStr = "";
%>
<script type="text/javascript"><!--
	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].checked = value;
			}
		}
	}
	
	/* a popop window to view accounts that belong to account group 
	 */
	function popup(mylink, windowname)
	{
		if (! window.focus)return true;
		var href;
		if ( typeof(mylink) == 'string')
	   		href=mylink;
		else
   			href=mylink.href;
		window.open(href, windowname, 'width=800,height=400,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,directories=no,location=no,status=no');
		return false;
	}
	//-->
// --></script>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:removeProperty name="DARequest" />
	<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
	<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
	<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
</ffi:cinclude>



<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>



<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>	 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<div align="center">
    <ffi:cinclude value1="${DisplayErrors}" value2="TRUE" operator="equals">
		<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
				    <td class="errortext">
					<ffi:getProperty name="limit_error" encode="false"/><ffi:removeProperty name="limit_error"/>
				    </td>
		    </tr>
		</table>
    </ffi:cinclude>
<form action="<ffi:getProperty name='SecureServletPath'/>CheckForRedundantLimits" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div><ffi:getProperty name="Context"/></div>
<ffi:setProperty name="accountGroupDisplay" value="false"/>
<ffi:list collection="ParentsAccountGroups" items="acctGroup">
	<ffi:cinclude value1="${acctGroup.DisplayRow}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
		<ffi:setProperty name="accountGroupDisplay" value="true"/>
	</ffi:cinclude>
</ffi:list>


<ffi:cinclude value1="${accountGroupDisplay}" value2="false" operator="equals">
    <%
	    noAcctGrpsStr = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_325", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
    %>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
		<td colspan="<ffi:getProperty name="NumTotalColumns"/>" width="100%"><div style="text-align: center; padding-top: 6px;"><span class="sectionsubhead"><%= noAcctGrpsStr %></span></div></td>
	    </tr>
	    <tr>
	    <td colspan="<ffi:getProperty name="NumTotalColumns"/>" width="100%">
			<div align="center" style="padding-top: 16px; padding-bottom: 6px;">
			<span class="sectionsubhead">
				<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
			</span>
			</div>
		</td>
	    </tr>
	</table>
</ffi:cinclude>
<ffi:cinclude value1="${accountGroupDisplay}" value2="true" operator="equals">

<div class="paneWrapper">
  	<div class="paneInnerWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
				<tr class="header">
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
				<td class="sectionsubhead" align="center">
					<s:text name="jsp.user_32"/><br>
					<!-- <input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'admin');"> -->
				</td>
				</ffi:cinclude>
				<td class="sectionsubhead" align="center"><s:text name="jsp.user_177"/><br>
					<!-- <input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'account_group');"> -->
				</td>
				<td class="sectionsubhead" align="left"><s:text name="jsp.default_17"/></td>
				<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
				<ffi:process name="GetLimitBaseCurrency" />
				<td class="sectionsubhead" align="center" colspan="2"><s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)</td>
				<td class="sectionsubhead" align="center">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<s:text name="jsp.user_154"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
				<td class="sectionsubhead" align="center" colspan="2"><s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)</td>
				<td class="sectionsubhead" align="center">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<s:text name="jsp.user_154"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
           <%	int checkboxCount = 0; %>
           
			<%--Filter Results a row for each accountgroup--%>
	   <ffi:list collection="ParentsAccountGroups" items="acctGroup">
				<ffi:cinclude value1="${acctGroup.DisplayRow}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
					<ffi:setProperty name="hideEnt" value="false" />
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<%--Following line added to prevent admin changes going to pending island --%>
					<ffi:setProperty name="acctGroup" property="CanAdminRow" value="false"/>
					<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="FALSE">
						<ffi:setProperty name="hideEnt" value="true" />
					</ffi:cinclude> 
				</ffi:cinclude>
		<ffi:cinclude value1="${hideEnt}" value2="false">
			<ffi:removeProperty name="hideEnt" />
					<tr class"adminBackground">
					 <% pageContext.setAttribute( "checkboxCount", new Integer( checkboxCount ) ); %>
					
						<ffi:setProperty name="Access" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
						<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
							<ffi:object id="CheckEntByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
							<ffi:setProperty name="CheckEntByMember" property="GroupId" value="${BusinessEmployee.EntitlementGroupMember.EntitlementGroupId}"/>
							<ffi:setProperty name="CheckEntByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
							<ffi:setProperty name="CheckEntByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
							<ffi:setProperty name="CheckEntByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
							<ffi:setProperty name="CheckEntByMember" property="AttributeName" value="HasAdmin"/>
							<ffi:setProperty name="CheckEntByMember" property="OperationName" value="${Access.Value}"/>
							<ffi:setProperty name="CheckEntByMember" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
							<ffi:setProperty name="CheckEntByMember" property="ObjectId" value="${acctGroup.Id}"/>
							<ffi:process name="CheckEntByMember"/>
							<ffi:removeProperty name="CheckEntByMember"/>
						</s:if>
						<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
							<ffi:object id="CheckEntByGroup" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup"/>
							<ffi:setProperty name="CheckEntByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
							<ffi:setProperty name="CheckEntByGroup" property="AttributeName" value="HasAdmin"/>
							<ffi:setProperty name="CheckEntByGroup" property="OperationName" value="${Access.Value}"/>
							<ffi:setProperty name="CheckEntByGroup" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
							<ffi:setProperty name="CheckEntByGroup" property="ObjectId" value="${acctGroup.Id}"/>
							<ffi:process name="CheckEntByGroup"/>
							<ffi:removeProperty name="CheckEntByGroup"/>
						</s:if>
							<ffi:removeProperty name='admin${checkboxCount}'/>
							<ffi:setProperty name="LastRequest" property="Name" value="admin${checkboxCount}"/>
							<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${acctGroup.Id}"/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${HasAdmin}"/>
						
						<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
							<td class="columndata " valign="center" align="center">
						</ffi:cinclude>
							<ffi:cinclude value1="${acctGroup.CanAdminRow}" value2="TRUE" operator="equals">
								
								<input type="<ffi:getProperty name="AdminCheckBoxType"/>" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
							</ffi:cinclude>
							<ffi:cinclude value1="${acctGroup.CanAdminRow}" value2="TRUE" operator="notEquals">
								
								&nbsp;
							</ffi:cinclude>
						<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
							</td>
						</ffi:cinclude>
					

						<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
							<ffi:object id="CheckEntByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
							<ffi:setProperty name="CheckEntByMember" property="GroupId" value="${BusinessEmployee.EntitlementGroupMember.EntitlementGroupId}"/>
							<ffi:setProperty name="CheckEntByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
							<ffi:setProperty name="CheckEntByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
							<ffi:setProperty name="CheckEntByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
							<ffi:setProperty name="CheckEntByMember" property="AttributeName" value="HasInit"/>
							<ffi:setProperty name="CheckEntByMember" property="OperationName" value="${Access.OperationName}"/>
							<ffi:setProperty name="CheckEntByMember" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
							<ffi:setProperty name="CheckEntByMember" property="ObjectId" value="${acctGroup.Id}"/>
							<ffi:process name="CheckEntByMember"/>
							
							<ffi:removeProperty name="CheckEntByMember"/>
						</s:if>
						<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
							<ffi:object id="CheckEntByGroup" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup"/>
							<ffi:setProperty name="CheckEntByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
							<ffi:setProperty name="CheckEntByGroup" property="AttributeName" value="HasInit"/>
							<ffi:setProperty name="CheckEntByGroup" property="OperationName" value="${Access.OperationName}"/>
							<ffi:setProperty name="CheckEntByGroup" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
							<ffi:setProperty name="CheckEntByGroup" property="ObjectId" value="${acctGroup.Id}"/>
							<ffi:process name="CheckEntByGroup"/>
							
							<ffi:removeProperty name="CheckEntByGroup"/>
						</s:if>
						
						<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${acctGroup.Id}"/>
						<ffi:setProperty name="GetMaxLimitForPeriod" property="ObjectID" value="${acctGroup.Id}"/>

						<ffi:removeProperty name='account_group${checkboxCount}'/>
						<ffi:setProperty name="LastRequest" property="Name" value="account_group${checkboxCount}"/>
						<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${acctGroup.Id}"/>
						<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${HasInit}"/>
						<td class="columndata " valign="center" align="center">
							
							<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<ffi:process name="GetEntitlementsFromDA" />
								</ffi:cinclude>
								
								<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
							</ffi:cinclude>
							<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="notEquals">
								&nbsp;
							</ffi:cinclude>
						</td>
						<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/accountsForAccountGroup.jsp?Init=true&grpid=${acctGroup.Id}" URLEncrypt="true"/>
						<td width="150px;" style="max-width:180px;word-wrap:break-word;" class="columndata " align="left" ><a class="anchorText" style="color:<ffi:getPendingStyle fieldname="${LastRequest.CheckboxValue}" defaultcss="blue" 
						dacss="red" sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />" href="#" onClick="ns.admin.viewAdminPermAcctGrpList('<ffi:getProperty name="tempURL"/>')" ><ffi:getProperty name="acctGroup" property="Name"/> - <ffi:getProperty name="acctGroup" property="AcctGroupId"/></a></td>
						
						<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
							<ffi:setProperty name="GetGroupLimits" property="Period" value="1"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:removeProperty name="DARequest" />
								<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${acctGroup.Id}"/>
								<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="1"/>
								<ffi:process name="GetLimitsFromDA" />
							</ffi:cinclude>
							<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
							<ffi:removeProperty name="transaction_limit${checkboxCount}"/>
							<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1" >
								<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
								<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
							</ffi:list>
							<ffi:setProperty name="LastRequest" property="Name" value="transaction_limit${checkboxCount}"/>
							<td class="" valign="middle">
								<input class="ui-widget-content ui-corner-all" class="txtbox" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
							</td>
			 				<td class="" valign="middle" align="left">
			 					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals" >
										<span class="columndata"><s:text name="jsp.user_244"/></span>
										
									</ffi:cinclude>
									<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="equals" >
										<span class="columndataDA"><s:text name="jsp.user_244"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span class="columndata"><s:text name="jsp.user_244"/></span>
								</ffi:cinclude>
							</td>
							<td class="" valign="middle" align="center">
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
									<ffi:setProperty name="LastRequest" property="Name" value="transaction_exceed${checkboxCount}"/>
									<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
									<ffi:removeProperty name="transaction_exceed${checkboxCount}"/>
									<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>>
								</ffi:cinclude>
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									&nbsp;
								</ffi:cinclude>
							</td>
			
							<ffi:setProperty name="GetGroupLimits" property="Period" value="2"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="2"/>
								<ffi:process name="GetLimitsFromDA" />
							</ffi:cinclude>
							<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
							<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1" >
								<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
								<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
							</ffi:list>
							<ffi:setProperty name="LastRequest" property="Name" value="day_limit${checkboxCount}"/>
							<ffi:removeProperty name="day_limit${checkboxCount}"/>
							<td class="" valign="middle">
								<input class="ui-widget-content ui-corner-all" class="txtbox" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
							</td>
			 				<td class="" valign="middle" align="left">
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals" >
										<span class="columndata"><s:text name="jsp.user_242"/></span>
									</ffi:cinclude>
									<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="equals" >
										<span class="columndataDA"><s:text name="jsp.user_242"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span class="columndata"><s:text name="jsp.user_242"/></span>
								</ffi:cinclude>
							</td>
							<td class="" valign="middle" align="center">
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
									<ffi:setProperty name="LastRequest" property="Name" value="day_exceed${checkboxCount}"/>
									<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
									<ffi:removeProperty name="day_exceed${checkboxCount}"/>
									<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>>
								</ffi:cinclude>
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									&nbsp;
								</ffi:cinclude>
							</td>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="notEquals">
							<td class="" colspan="6" valign="middle" align="center">
								&nbsp;
							</td>
						</ffi:cinclude>
						</tr>
						<tr>
							<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
								<td></td>
							</ffi:cinclude>
							<td></td>
			 				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<td></td>
								<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerTransaction" /></span></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<td colspan="2"></td>
							</ffi:cinclude>
			 				<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
				 				<td class="columndata" colspan="2" align="left">(<ffi:getProperty name="GetMaxLimitForPeriod" property="TransactionLimitDisplay"/>)</td>
				 				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<td></td>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerDay" /></span></td>
								</ffi:cinclude>
				 				<td class="columndata"colspan="2" align="left">(<ffi:getProperty name="GetMaxLimitForPeriod" property="DailyLimitDisplay"/>)</td>
			 				</ffi:cinclude>
							<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="notEquals">
								<td colspan="5"></td>
							</ffi:cinclude>
			 			</tr>
						<tr>
							<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
								<td></td>
							</ffi:cinclude>
							<td colspan="2"></td>
						<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
							<ffi:setProperty name="GetGroupLimits" property="Period" value="3"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="3"/>
								<ffi:process name="GetLimitsFromDA" />
							</ffi:cinclude>
							<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
							<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1" >
								<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
								<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
							</ffi:list>
							<ffi:setProperty name="LastRequest" property="Name" value="week_limit${checkboxCount}"/>
							<ffi:removeProperty name="week_limit${checkboxCount}"/>
							<td valign="middle">
								<input class="ui-widget-content ui-corner-all" class="txtbox" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
							</td>
			 				<td valign="middle" align="left">
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="notEquals" >
										<span class="columndata"><s:text name="jsp.user_245"/></span>
									</ffi:cinclude>
									<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="equals" >
										<span class="columndataDA"><s:text name="jsp.user_245"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span class="columndata"><s:text name="jsp.user_245"/></span>
								</ffi:cinclude>
							</td>
							<td valign="middle" align="center">
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
									<ffi:setProperty name="LastRequest" property="Name" value="week_exceed${checkboxCount}"/>
									<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
									<ffi:removeProperty name="week_exceed${checkboxCount}"/>
									<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>>
								</ffi:cinclude>
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									&nbsp;
								</ffi:cinclude>
							</td>
			
							<ffi:setProperty name="GetGroupLimits" property="Period" value="4"/>
							<ffi:process name="GetGroupLimits"/>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="4"/>
								<ffi:process name="GetLimitsFromDA" />
							</ffi:cinclude>
							<ffi:setProperty name="LastRequest" property="TextDefaultValue" value=""/>
							<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="false"/>
							<ffi:list collection="Entitlement_Limits" items="LimitItem" startIndex="1" endIndex="1" >
								<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${LimitItem.Data}"/>
								<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${LimitItem.AllowApproval}"/>
							</ffi:list>
							<ffi:setProperty name="LastRequest" property="Name" value="month_limit${checkboxCount}"/>
							<ffi:removeProperty name="month_limit${checkboxCount}"/>
							<td valign="middle">
								<input class="ui-widget-content ui-corner-all" class="txtbox" disabled type="text" size="12" maxlength="17" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="TextValue"/>">
							</td>
			 				<td valign="middle" align="left">
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="notEquals" >
										<span class="columndata"><s:text name="jsp.user_243"/></span>
									</ffi:cinclude>
									<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="equals" >
										<span class="columndataDA"><s:text name="jsp.user_243"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span class="columndata"><s:text name="jsp.user_243"/></span>
								</ffi:cinclude>
							</td>
							<td valign="middle" align="center">
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
									<ffi:setProperty name="LastRequest" property="Name" value="month_exceed${checkboxCount}"/>
									<ffi:setProperty name="LastRequest" property="CheckboxValue" value="true"/>
									<ffi:removeProperty name="month_exceed${checkboxCount}"/>
									<input type="checkbox" disabled <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/>>
								</ffi:cinclude>
								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									&nbsp;
								</ffi:cinclude>
							</td>
						</ffi:cinclude>
						</tr>
						<tr>
							<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
								<td></td>
							</ffi:cinclude>
			 				<td></td>
			 				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<td colspan="2"></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<td></td>
								<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerWeek" /></span></td>
							</ffi:cinclude>
			 				<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="equals">
				 				<td class="columndata" colspan="2" align="left">(<ffi:getProperty name="GetMaxLimitForPeriod" property="WeeklyLimitDisplay"/>)</td>
				 				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<td><span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerMonth" /></span></td>
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<td></td>
								</ffi:cinclude>
				 				<td class="columndata" colspan="2" align="left">(<ffi:getProperty name="GetMaxLimitForPeriod" property="MonthlyLimitDisplay"/>)</td>
			 				</ffi:cinclude>
							<ffi:cinclude value1="${acctGroup.CanInitRow}" value2="TRUE" operator="notEquals">
								<td colspan="5"></td>
							</ffi:cinclude>
			 			</tr>
					
					<ffi:cinclude value1="${DisplayErrors}" value2="TRUE" operator="equals">
						<tr>
							<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
								<td></td>
							</ffi:cinclude>
							<td></td>
							<td colspan="7" valign="middle" align="left" class="errortext"><ffi:getProperty name="limit_error${checkboxCount}" encode="false"/></td>
						</tr>
						<ffi:removeProperty name="limit_error${checkboxCount}"/>
					</ffi:cinclude>
		
					<ffi:setProperty name="Math" property="Value2" value="${Math.Subtract}" />
				</ffi:cinclude>
				</ffi:cinclude>
			<% checkboxCount++; %>
			</ffi:list>
			<ffi:removeProperty name="checkboxCount"/>
			
					</table></div></div>
			<div class="btn-row">
				<sj:a
					button="true" 
					onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
					><s:text name="jsp.default_102"/>
				</sj:a>
			</div>	
			<%--
			<td class="adminBackground" align="center">
				<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
					<input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_57"/>" border="0" onclick="location.replace('<ffi:getProperty name="SecurePath"/>user/permissions.jsp')">
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
					<input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_57"/>" border="0" onclick="location.replace('<ffi:getProperty name="SecurePath"/>user/corpadmininfo.jsp')">
				</ffi:cinclude>
			</td>
			--%>
		</form>
</ffi:cinclude>
		</div>
</div>
<ffi:removeProperty name="GetMaxLimitForPeriod"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="GetEntitlementTypePropertyList" />
<ffi:removeProperty name="Access" />
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
<ffi:setProperty name="FromBack" value=""/>
<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />