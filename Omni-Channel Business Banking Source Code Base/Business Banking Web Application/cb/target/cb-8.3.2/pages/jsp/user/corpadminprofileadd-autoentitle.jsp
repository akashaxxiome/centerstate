<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminprofileadd-autoentitle" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_54')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>


	<div align="center" class="marginTop20 marginBottom10">
				<div class="leftPaneWrapper" id="">
					<div class="leftPaneInnerWrapper">				
						<div class="header">
							<s:text name="user.profiles.summary"/>
						</div>
						<div style="padding: 15px 10px;" class="summaryBlock">
							<div style="width:100%" class="floatleft">
								<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_267"/>:</span>
								<span id="" class="inlineSection floatleft labelValue"><ffi:getProperty name="BusinessEmployee" property="UserName"/></span>
							</div>
						<div style="width:100%" class="marginTop10 clearBoth floatleft">
									<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_225"/>:</span>
									<span id="" class="inlineSection floatleft labelValue">
												<ffi:list collection="GroupSummaries" items="Group">
												<ffi:list collection="Group" items="spaces, groupName, groupId, numUsers" >
													<ffi:cinclude value1="${groupId}" value2="${BusinessEmployee.EntitlementGroupId}" operator="equals">
														<ffi:getProperty name="groupName" />
													</ffi:cinclude>
												</ffi:list>
												</ffi:list>
								    </span>
					    </div>
						</div>
						</div></div>
				<div class="rightPane">
			    		<s:form id="addBusinessProfileAutoEntitleFormId" namespace="/pages/user" action="addUserProfile_execute" validate="false" theme="simple" method="post" name="autoEntConfirm">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<s:if test="%{doAutoEntitle}">
								<div><span class="columndata"><s:text name="jsp.user_126"/></span>
									&nbsp;&nbsp;
										<input type="radio" name="BusinessEmployee.AutoEntitle" value="true" checked="checked"/>&nbsp;<s:text name="jsp.default_467"/>
										<input type="radio" name="BusinessEmployee.AutoEntitle" value="false"/>&nbsp;<s:text name="jsp.default_295"/>
								</div>
							</s:if>	
								
								<div class="sectionhead">
									<s:text name="jsp.user_380"/>
								</div>
								<div class="btn-row">
									<sj:a
										  button="true"
										  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
									<sj:a
										  button="true"
										  summaryDivId="profileSummary" 
										  buttonType="cancel"
										  onClickTopics="showSummary,cancelProfileForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a
											formIds="addBusinessProfileAutoEntitleFormId"
											targets="confirmDiv"
											button="true"
											validate="false"
											onBeforeTopics="beforeSubmit"
											onErrorTopics="errorSubmit"
											onSuccessTopics="completeSubmit"
											><s:text name="jsp.default_366"/></sj:a>
								</div>
			   		 </s:form>
				</div>
		</div>
