<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:url id="pageurl" value="/pages/jsp/user/bankLookupDialog.jsp" escapeAmp="false">

</s:url>
<sj:dialog
	id="bankLookupDialogId"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.default_554')}"
	height="400"
	width="750"
	href="%{pageurl}"
	showEffect="fold"
	hideEffect="clip"
	resizable="false"
	cssStyle="overflow:hidden">
</sj:dialog>