<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%-- Generate group member tree --%>
<ffi:object id="GenerateGroupMemberTree" name="com.ffusion.tasks.dualapproval.GenerateGroupMemberTree" />
<ffi:setProperty name="GenerateGroupMemberTree" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:setProperty name="GenerateGroupMemberTree" property="GroupType" value="${Section}"  />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="GenerateGroupMemberTree" property="ModifyUserOnly" value="true" />	
</s:if>
<ffi:process name="GenerateGroupMemberTree" />

<%-- Generate entitlement tree process task  --%>
<ffi:object id="ProcessEntitlementTree"  name="com.ffusion.tasks.dualapproval.ProcessEntitlementTree" scope="session" />
<ffi:setProperty name="ProcessEntitlementTree" property="GroupId" value="${GenerateGroupMemberTree.GroupId}" />
<ffi:setProperty name="ProcessEntitlementTree" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
<ffi:setProperty name="ProcessEntitlementTree" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
<ffi:setProperty name="ProcessEntitlementTree" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
<ffi:setProperty name="ProcessEntitlementTree" property="Category" value="${Category}" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
<ffi:setProperty name="ProcessEntitlementTree" property="ModifyUserOnly" value="true" />
</s:if>
<% session.setAttribute("FFIProcessEntitlementTree", session.getAttribute("ProcessEntitlementTree")); %>

<%-- Display entitlement group member tree. --%>
<ffi:cinclude value1="${GenerateGroupMemberTree.ModifyUserOnly}" value2="true" operator="notEquals">
	<script language="JavaScript" type="text/javascript">
		//create parent child array
		var child_groups = new Array();
		var child_members = new Array();
	<ffi:list items="treeElement" collection="ENTITLEMENT_TREE_GROUPS">		
		child_groups["group_<ffi:getProperty name="treeElement" property="groupId" />"] = new Array(<ffi:list items="child" collection="treeElement.children" startIndex="1" endIndex="1">"group_<ffi:getProperty name="child" property="groupId" />"</ffi:list><ffi:list items="child" collection="treeElement.children" startIndex="2">,"group_<ffi:getProperty name="child" property="groupId" />"</ffi:list>);
					
		child_members["group_<ffi:getProperty name="treeElement" property="groupId" />"] = new Array(<ffi:map key="groupIdKey" value="members" map="ENTITLEMENT_TREE_MEMBERS_MAP"><ffi:cinclude value1="${groupIdKey}" value2="${treeElement.groupId}" operator="equals" ><ffi:list items="memberElement" collection="members" startIndex="1" endIndex="1">"member_<ffi:getProperty name="memberElement" property="memberId" />"</ffi:list><ffi:list items="memberElement" collection="members" startIndex="2">,"member_<ffi:getProperty name="memberElement" property="memberId" />"</ffi:list></ffi:cinclude></ffi:map>);
	</ffi:list>
		
		//**********************************
		//	Tree element selection handler.
		//**********************************
		function tree_element_clicked(tree_element) {		
			var ele_name = tree_element.name;
			
			if(tree_element.checked) { //process if this element is selected			
				select_element(ele_name);	
			} else { // process if this element is deselected
				uncheckChildElements(ele_name);
			}
		}
		
		//*************************************
		//select the children of given element	
		//*************************************
		function select_element(ele_name) {		
			for (var i=0; i<child_groups[ele_name].length; i++) {				
				if(autoEntConfirm[child_groups[ele_name][i]]) {								
				    autoEntConfirm[child_groups[ele_name][i]].disabled = false;				
			    }
			}
			for (var i=0; i<child_members[ele_name].length; i++) {				
				if(autoEntConfirm[child_members[ele_name][i]]) {				
				    autoEntConfirm[child_members[ele_name][i]].disabled = false;
			    }
		    }
		}
		
		//**************************************************
		//deselect and disable the children of given element
		//**************************************************
		function uncheckChildElements(ele_name) {
			for (var i=0; i<child_groups[ele_name].length; i++) {
		    	if(autoEntConfirm[child_groups[ele_name][i]]) {
					autoEntConfirm[child_groups[ele_name][i]].checked = false;
					autoEntConfirm[child_groups[ele_name][i]].disabled = true;
					uncheckChildElements(autoEntConfirm[child_groups[ele_name][i]].name);
			    }
			}
			for (var i=0; i<child_members[ele_name].length; i++) {				
			    if(autoEntConfirm[child_members[ele_name][i]]) {				
					autoEntConfirm[child_members[ele_name][i]].checked = false;
					autoEntConfirm[child_members[ele_name][i]].disabled = true;
			    }
		    }
		}
		
		function selectRootElement() {
			uncheckChildElements("group_<ffi:getProperty name="GenerateGroupMemberTree" property="groupId" />");
			select_element("group_<ffi:getProperty name="GenerateGroupMemberTree" property="groupId" />"); 
		}
	</script>
	
	<%-- <FORM action="<ffi:urlEncrypt url="${autoEntitleFormAction}" />" method="post" name="autoEntConfirm" > --%>
	<s:form id="autoEntConfirmID" name="autoEntConfirm" namespace="/pages/user" action="editAccountAccessPermissions-execute" method="post" validate="false" theme="simple">
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard" />" />							
		<table>
		<tr><td class="columndata">
		Please select the items below that should be granted the new permission(s).
		</td></tr>
		
		<tr><td class="columndata">
		<ffi:list items="treeElement" collection="ENTITLEMENT_TREE_GROUPS">
			<div class="columndata">
				<ffi:getProperty name="treeElement" property="indentLevel" encode="false" />		
				<ffi:cinclude value1="${GenerateGroupMemberTree.groupId}" value2="${treeElement.groupId}" operator="equals">
					<input type="checkbox" name="group_<ffi:getProperty name="treeElement" property="groupId" />" value="<ffi:getProperty name="treeElement" property="groupId" />" disabled checked /><ffi:getProperty name="treeElement" property="displayName" />
					<input type="hidden" name="RootGroupId" value="<ffi:getProperty name="treeElement" property="groupId" />" />				
				</ffi:cinclude>
				<ffi:cinclude value1="${GenerateGroupMemberTree.groupId}" value2="${treeElement.groupId}" operator="notEquals">
					<input type="checkbox" name="group_<ffi:getProperty name="treeElement" property="groupId" />" value="<ffi:getProperty name="treeElement" property="groupId" />" onclick="tree_element_clicked(this)" /><ffi:getProperty name="treeElement" property="displayName" />
				</ffi:cinclude>
			</div>
			<ffi:map key="groupIdKey" value="members" map="ENTITLEMENT_TREE_MEMBERS_MAP">
				<ffi:cinclude value1="${groupIdKey}" value2="${treeElement.groupId}" operator="equals" >
					<ffi:list items="memberElement" collection="members">
					    <ffi:cinclude value1="${memberElement.InActive}" value2="false" operator="equals">
						<div class="columndata">
							<ffi:getProperty name="memberElement" property="indentLevel" encode="false" />
							<input type="checkbox" name="member_<ffi:getProperty name="memberElement" property="memberId" />" value="<ffi:getProperty name="memberElement" property="memberId" />" /><ffi:getProperty name="memberElement" property="displayName" />
						</div>
						</ffi:cinclude>
					</ffi:list>
				</ffi:cinclude>
			</ffi:map>
		</ffi:list>
		</td></tr>
		</table>
		<div class="btn-row">
			<%-- <input class="submitbutton" type="button" value="BACK" onclick="document.location='<ffi:getProperty name="autoEntitleBackURL"/>';"> --%>
			<%-- Auto entitle cancel URL spelling keeps changing. --%>
			<ffi:setProperty name="entitleTreeCancelURL" value="${autoEntitleCancel}" />
			<ffi:cinclude value1="${autoEntitleCancle}" value2="" operator="notEquals">
				<ffi:setProperty name="entitleTreeCancelURL" value="${autoEntitleCancle}" />
			</ffi:cinclude>
			<%-- <input class="submitbutton" type="button" value="CANCEL" onclick="document.location='<ffi:getProperty name="entitleTreeCancelURL"/>';"> --%>
			<s:url id="inputUrl" value="%{#session.autoEntitleBackURL}" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				href="%{inputUrl}"
				targets="%{#session.perm_target}"
				button="true"
				><s:text name="jsp.default_57"/></sj:a>
			<sj:a
				button="true"
				onClickTopics="cancelPermForm"
				><s:text name="jsp.default_102"/></sj:a>
			<%-- <input class="submitbutton" type="submit" value="SAVE" > --%>
			<sj:a
				formIds="autoEntConfirmID"
				targets="resultmessage"
				button="true"
				validate="false"
				validateFunction="customValidation"
				onBeforeTopics="beforeSubmit" 
				onErrorTopics="errorSubmit"
				onCompleteTopics="%{#session.completedTopics}"
				>Save
			</sj:a>
		</div>
	<%-- </FORM> --%>
	</s:form>
	<script language="JavaScript" type="text/javascript">
		selectRootElement();
	</script>
</ffi:cinclude>
<%-- User only processing mode. --%>
<ffi:cinclude value1="${GenerateGroupMemberTree.ModifyUserOnly}" value2="true" operator="equals">
<FORM action="<ffi:urlEncrypt url="${autoEntitleFormAction}" />" method="post" name="autoEntConfirm" >
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard" />" />
	<div>
		<ffi:getProperty name="autoEntitleConfirmMsg"/>
	</div>
	<div class="btn-row">
			<input class="submitbutton" type="button" value="Back" onclick="document.location='<ffi:getProperty name="autoEntitleBackURL"/>';">
			<input class="submitbutton" type="button" value="Cancel" onclick="document.location='<ffi:getProperty name="autoEntitleCancel"/>';">
			<%-- <input class="submitbutton" type="submit" value="SAVE" > --%>
			<sj:a
				formIds="${autoEntitleFormAction}"
				targets="resultmessage"
				button="true"
				validate="true"
				validateFunction="customValidation"
				onBeforeTopics="beforeVerify"
				><s:text name="jsp.default_366"/>
			</sj:a>
	</div>
</FORM>
</ffi:cinclude>
<%-- to be remove once account access permission type will implemented for da --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="PROCESS_TREE_FLAG" value="false" />	
</s:if>
<%-- end --%>
<ffi:removeProperty name="GenerateGroupMemberTree" /> 
<ffi:removeProperty name="PROCESS_USER_FLAG" />

<%-- remove action and topic name unnecessary --%>
<ffi:removeProperty name="action" />
<ffi:removeProperty name="action_execute" />
<ffi:removeProperty name="completedTopics" />
<ffi:removeProperty name="perm_target" />
