<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminprofileedit-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_376')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminprofileedit.jsp"/>

		<div align="center" class="marginBottom10 marginTop20">
			<div class="leftPaneWrapper" id="">
					<div class="leftPaneInnerWrapper">		
						<div class="header">
							<s:text name="admin.profile.new.summary"/>
						</div>
						<div style="padding: 15px 10px;" class="summaryBlock">
								<div style="width:100%" class="floatleft">
									<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_267"/>:</span>
									<span id="" class="inlineSection floatleft labelValue"><ffi:getProperty name="BusinessEmployee" property="UserName"/></span>
								</div>
								<div style="width:100%" class="marginTop10 clearBoth floatleft">
									<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_225"/>:</span>
									<span id="" class="inlineSection floatleft labelValue">
										
										<ffi:list collection="GroupSummaries" items="Group">
										
										<ffi:list collection="Group" items="spaces, groupName, groupId, numUsers" >
											<ffi:cinclude value1="${groupId}" value2="${BusinessEmployee.EntitlementGroupId}" operator="equals">
												<ffi:getProperty name="groupName" />														
											</ffi:cinclude>
										</ffi:list>
									</ffi:list>
									</span>
								</div>
						</div>
						</div>
					</div>
				<div class="rightPane">
						<s:form id="editBusinessProfileAutoEntitleFormId" namespace="/pages/user" action="modifyBusinessEntitlementProfile_execute" validate="false" theme="simple" name="EditBusinessUserForm" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<input type="hidden" name="BusinessEmployee.UserName" value="<ffi:getProperty name='BusinessEmployee' property='UserName'/>"/>
							<div align="center">
									<input type="hidden" name="BusinessEmployee.AutoEntitle" value="true"/>

									 <div><s:text name="jsp.user_380"/></div>
									<div class="btn-row">
											<sj:a
												  button="true" 
												  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
											<sj:a
												  button="true"
												  summaryDivId="profileSummary" 
										  		  buttonType="cancel"
										  		  onClickTopics="showSummary,cancelProfileForm"><s:text name="jsp.default_82"/></sj:a>
											<sj:a	
													formIds="editBusinessProfileAutoEntitleFormId"
													targets="confirmDiv"
													button="true" 
													validate="false"
													onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="completeSubmit"
												><s:text name="jsp.default_366"/></sj:a>
								</div>
							</div>
						</s:form>
		</div>
</div>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>