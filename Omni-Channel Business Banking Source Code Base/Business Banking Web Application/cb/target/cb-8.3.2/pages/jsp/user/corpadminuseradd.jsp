<%@ page import="com.ffusion.beans.user.User" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<ffi:help id="user_corpadminuseradd" className="moduleHelpClass"/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_29"/></span>
<style type="text/css">
/* #AddUserFormID input[type="text"], #AddUserFormID input[type="password"]{width: 200px;} */
#AddUserFormID .selectBoxHolder input{width: 216px;}
#AddUserFormID .errorLabel{display: block;}
</style>
<ffi:setProperty name="channelListLink"
                 value="/pages/user/addBusinessUser_getChannelsForProfile.action"
                 URLEncrypt="false"/>
<s:url id="getChannelsForProfileUrl" value="%{#session.channelListLink}" escapeAmp="false">  
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>				
</s:url>
<script type="text/javascript"
	src="<s:url value='/web'/>/js/user/profiles.js"></script>
<script language="javascript">
var channelUrl = "<ffi:getProperty name='getChannelsForProfileUrl'/>";
jQuery( document ).ready(function( $ ) {
	var selectdGroup =  $("#selectUserGroupID").val();
	loadChannelsForGroup();
	/*
	//Addding the click handlers to the radio buttons.
	$("#perChannelRadioId").click(function(){
		//Populating the per channel profiles in all the dropdown.Passing the profile id as -1.
		populatePerChannelProfiles(crossSel.getEntitledProfileById("-1"));
		$(".markerChannels").each(function() {
			$(this).css('display', 'block');
		});
		$("#crossProfilesId").hide();
	});
	
	$("#crossXRadioId").click(function(){
		//When we select cross radio button clearing the per channels dropdown since it will now hold
		// cross channel related profiles.
		clearChannelProfiles(crossSel.getEntitledProfileById("-1"));
		$(".markerChannels").each(function() {
			$(this).css('display', 'none');
		});
		$("#crossProfilesId").show();
	});*/
});
//TODO: Move this in the jquery onReady method.
$(function(){
	$("#selectUserCountryID").combobox({width: 250});
	$("#selectUserStateID").combobox({width: 240,searchFromFirst:true});
	$("#selectUserLanguageID").selectmenu({width: 240});
	$("#selectUserStatusID").selectmenu({width: 240});
	$("#selectUserGroupID").selectmenu({width: 240});
	$("#selectUserAdminID").selectmenu({escapeHtml:true, width: 240});
	ns.users.entitlementProfile={};
	$("select[data-sap-banking-channelType]").selectmenu({width: 150});

});

function showState(obj) {
	//TODO: Replace /cb with thew context path.
	var urlString = '/cb/pages/jsp/user/inc/country-state.jsp';
	$.ajax({
		   type: 'POST',
		   url: urlString,
		   data: "countryCode=" + obj.value + "&CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>',
		   success: function(data){
		    if(data.indexOf("option") != -1) {
		    	$("#stateNameId").html('<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_386"/></span>');
		    } else {
		    	$("#stateNameId").html('<span></span>');
		    }
		   	$("#stateSelectId").html(data);
	   }
	});
}

// submit the form to update the entitlement group and other items.
function updateEntGroup(obj) {
	if($("#selectUserGroupID").val() !== "0"){
		$("#crossChannelChildId").empty();
		$.ajax({
			type: "POST",
			data: $("#AddUserFormID").serialize(),
			url: "/cb/pages/user/addBusinessUser_updateEntGroup.action",
			success: function(data){
				$("#inputDiv").html(data);
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#selectUserGroupID-menu").focus();
				}
				loadChannelsForGroup();
		   }
		});
	}
}

function loadChannelsForGroup(){
	var usingEntitlementProfile = '<ffi:getProperty name="BusinessEmployee" property="usingEntProfiles"/>';
	$("#crossProfilesId").show();
	var entitlementGroupId = $("#selectUserGroupID").val();
	var isMigration = '<s:property value="migration" />';
	
	if(usingEntitlementProfile && usingEntitlementProfile == 'true' || isMigration == 'true') {
		$.ajax({
			type: "POST",
			data: $("#AddUserFormID").serialize(),
			url: "/cb/pages/user/addBusinessUser_getUserProfilesList.action?entitlementGroupId="+entitlementGroupId
				 + "&CSRF_TOKEN=" + ns.home.getSessionTokenVarForGlobalMessage,
			success: function(response){
			$("#crossProfilesId").html(response);
			crossSel = new selectWizard();
			crossSel.init("selectEntitlementGroup_X", {width:200});
			crossSel.addChangeEvent(crossProfileChange);
			//$("#crossXRadioId").click();
		   }
		});
	}
}
</script>
<%
	if (request.getParameter("UpdateEntGroup") != null) {
		session.setAttribute("UpdateEntGroup", request.getParameter("UpdateEntGroup"));
	}
	if (request.getParameter("Init") != null) {
		session.setAttribute("Init", request.getParameter("Init"));
	}
%>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<ffi:setProperty name="checkedfalse" value=""/>
<ffi:setProperty name="checkedtrue" value="checked"/>


<s:include value="/pages/jsp/user/password-indicator-js.jsp" />
<s:include value="/pages/jsp/user/corpadmin_js.jsp" />

<div align="center" class="marginTop20">
<%-- <table width="70%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2">
			<div align="center">
				<span class="sectionsubhead"><br><ffi:getProperty name="BusinessName"/> <ffi:cinclude value1="DivisionName" value2="" operator="equals"> /
					<ffi:getProperty name="DivisionName"/></ffi:cinclude>&nbsp;<br>
					<br>
				</span>
			</div>
		</td>
	</tr>
</table> --%>
<s:form namespace="/pages/user" action="addBusinessUser_verify" theme="simple" name="AddUserForm" id="AddUserFormID" method="post">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td colspan="4"><h3 class="transactionHeading"><s:text name="admin.login.information" /></h3></td>
	</tr>
	<tr>
		<td width="25%" id="userNameLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.default_455"/><span class="required">*</span>
		</td>
		<td width="25%" id="userPasswordLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_233"/><span class="required">*</span>
		</td>
		<td width="25%" id="userConfirmPasswordLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_92"/><span class="required">*</span>
		</td>
		<td width="25%" id="userGroupLabel" align="left" class="sectionsubhead adminBackground">
			<ffi:cinclude value1="${GroupSummaries.Size}" value2="0" operator="notEquals">
				<s:text name="jsp.default_225"/><span class="required">*</span>
			</ffi:cinclude>
		</td>
	</tr>
	<tr class="adminUserTr">
		<td class="adminBackground" width="200" valign="top">
			<input id="userName" style="width:237px;" tabindex="14" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.UserName" size="24" maxlength="20" border="0" value="<ffi:getProperty name="BusinessEmployee" property="UserName"/>">
			<span id="BusinessEmployee.userNameError"></span>
			<span id="userNameError"></span>
		</td>
		<td class="adminBackground" width="200" valign="top">
			<div style="width:242px;" id="userPasswordDvc">
			<input id="userPassword" tabindex="15" width:237px;" class="ui-widget-content ui-corner-all inputPassword" type="password" name="BusinessEmployee.Password" size="24" maxlength="50" border="0" value="<ffi:getProperty name="BusinessEmployee" property="password"/>">
			</div>
			<span id="BusinessEmployee.passwordError"></span>
		</td>
		<td class="adminBackground" width="200" valign="top">
			<input id="userConfirmPassword" tabindex="15" class="ui-widget-content ui-corner-all"  style="width:237px;" type="password" name="BusinessEmployee.ConfirmPassword" size="24" maxlength="50" border="0"
				   value="<ffi:getProperty name="BusinessEmployee" property="confirmPassword"/>">
			<span id="BusinessEmployee.confirmPasswordError"></span>
		</td>
		<td class="adminBackground" width="200" valign="top">
			<ffi:cinclude value1="${GroupSummaries.Size}" value2="0" operator="notEquals">
				<select id="selectUserGroupID" class="txtbox" tabindex="20" name="BusinessEmployee.EntitlementGroupId" onchange="updateEntGroup(this);">
					<ffi:cinclude value1="${UpdateEntGroup}" value2="TRUE" operator="notEquals">
						<option value="0"><s:text name="jsp.default_375"/></option>
					</ffi:cinclude>
					<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.EntitlementGroupId}"/>
					<ffi:list collection="GroupSummaries" items="group">
						<ffi:list collection="group" items="spaces, groupName, groupId, numUsers">
							<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals">
								<ffi:setProperty name="Compare" property="value2" value="${groupId}"/>
								<option value="<ffi:getProperty name="groupId" />" <ffi:getProperty	name="selected${Compare.Equals}"/> >
									<ffi:getProperty name="spaces" encode="false"/><ffi:getProperty name="groupName"/>
								</option>
							</ffi:cinclude>
						</ffi:list>
					</ffi:list>
				</select>
				<span id="BusinessEmployee.entitlementGroupIdError"></span>
			</ffi:cinclude>
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<tr>
		<td id="userStatusLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_364"/>
		</td>
		<td id="userPrimaryAdminLabel" align="left" class="sectionsubhead adminBackground">
		<ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
			<div align="left"><span class="sectionsubhead"><s:text name="jsp.user_264"/></span>
			</div>
		</ffi:cinclude>
		</td>
		<td colspan="2">
			<span class="sectionsubhead"><s:text name="jsp.user_53"/></span>
		</td>	
	</tr>
	<tr class="adminUserTr">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<td class="columndata" width="200" align="left">
				<ffi:setProperty name="BusinessEmployee" property="AccountStatus" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>"/>
				<s:text name="jsp.user_235"/>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<td class="adminBackground" width="200" align="left">
				<select id="selectUserStatusID" class="txtbox" tabindex="19" name="BusinessEmployee.AccountStatus">
					<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.AccountStatus}"/>
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
					<option value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.default_28"/></option>
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
					<option value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.default_238"/></option>
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
					<option value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.user_306"/></option>
				</select>
			</td>
		</ffi:cinclude>
		</td>
		<td class="adminBackground" width="300" valign="top">
			<ffi:cinclude value1="${GroupAdmins}" value2="" operator="notEquals">
				<ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
					<select id="selectUserAdminID" class="txtbox" name="BusinessEmployee.PrimaryAdmin">
						<option value="0"><s:text name="jsp.default_375"/></option>
						<ffi:setProperty name="GroupAdmins" property="SortedBy" value="NAME"/>
						<ffi:list collection="GroupAdmins" items="employee">
							<ffi:cinclude value1="${employee.AccountStatus}" value2="<%= User.STATUS_ACTIVE %>">
								<option value="<ffi:getProperty name='employee' property='Id'/>"
									<ffi:cinclude value1="${BusinessEmployee.PrimaryAdmin}" value2="${employee.Id}">selected</ffi:cinclude>>
									<ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
								</option>
							</ffi:cinclude>
						</ffi:list>
					</select>
				</ffi:cinclude>
			</ffi:cinclude>
		</td>
		<td class="columndata adminBackground textWrapInTd" colspan="2">
		 	<ffi:getProperty name="adminStrLine"/>
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<s:if test="%{#session.Business.usingEntProfiles == true || migration == true}">
		<tr>
			<td class="adminBackground" valign="baseline" colspan="4">
				<div align="left"><span class="sectionsubhead"><s:text name="admin.profile.assignment"/></span><span class="required">*</span></div>
			</td>
		</tr>
		<tr>
			<!-- <td id="userPrimaryAdminLabel" align="left" class="sectionsubhead adminBackground">
			<td id="userPrimaryAdminLabel" align="left" class="sectionsubhead adminBackground"> -->
			
			<td width="250" id="crossProfilesId" class="adminBackground" valign="top" colspan="4" >
			</td>
		</tr>
		
	</s:if>
	<tr>
		<td colspan="4">
			<span class="required">
				*<ffi:setProperty name="useBCsettings" value="false"/>
				<ffi:setProperty name="changeOwnPassword" value="false"/>
				<s:include value="/pages/jsp-ns/common/password-strength-text.jsp"/>
				<ffi:removeProperty name="changeOwnPassword"/>
				<ffi:removeProperty name="useBCsettings"/> 
			</span>
		</td>
	</tr>
	<tr>
		<td colspan="4"><h3 class="transactionHeading"><s:text name="admin.personal.information" /></h3></td>
	</tr>
	<tr>
		<td id="userFirstNameLabel" align="left" class="sectionsubhead adminBackground">
			<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
				<s:text name="jsp.user_161"/><span class="required">*</span>
			</ffi:cinclude>
			<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">&nbsp;
			</ffi:cinclude>
		</td>
		<td id="userLastNameLabel" align="left" class="sectionsubhead adminBackground">
			<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
				<s:text name="jsp.default_283"/><span class="required">*</span>
			</ffi:cinclude>
			<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
				<s:text name="jsp.user_185"/><span class="required">*</span>
			</ffi:cinclude>
		</td>
		<td id="userPhoneLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_250"/>
		</td>
		<td id="userEmailLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_131"/>
		</td>
	</tr>
	<tr>
		<td class="adminBackground" width="350"  valign="top">
			<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">&nbsp;
			</ffi:cinclude>
			<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
				<input id="userFirstName" tabindex="1" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.firstName" size="24" maxlength="35" border="0" value="<ffi:getProperty name="BusinessEmployee" property="FirstName"/>">
				<span id="BusinessEmployee.firstNameError"></span>
			</ffi:cinclude>
		</td>
		<td class="adminBackground"  valign="top">
			<input id="userLastName" tabindex="2" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.lastName" size="24" maxlength="35" border="0" value="<ffi:getProperty name="BusinessEmployee" property="LastName"/>">
			<span id="BusinessEmployee.lastNameError"></span>
		</td>
		<td class="adminBackground"  valign="top">
			<input id="userPhone" tabindex="10" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Phone" size="24" maxlength="14" border="0" value="<ffi:getProperty name="BusinessEmployee" property="Phone"/>">
			<span id="BusinessEmployee.phoneError"></span>
		</td>
		<td class="adminBackground"  valign="top">
			<input id="userEmail" tabindex="13" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Email" size="24" maxlength="40" border="0" value="<ffi:getProperty name="BusinessEmployee" property="Email"/>">
			<span id="BusinessEmployee.emailError"></span>
		</td>
	</tr>	
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<tr>
		<td id="userDataPhoneLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_103"/>
		</td>
		<td id="userFaxLabel" align="left" class="sectionsubhead adminBackground">
			<s:text name="jsp.user_156"/>
		</td>
		<td id="userAddress1Label" class="adminBackground">
			<div align="left"><span
					class="sectionsubhead"><s:text name="jsp.default_35"/></span></div>
		</td>
		<td id="userAddress2Label" class="adminBackground">
			<div align="left"><span class="sectionsubhead"><s:text name="jsp.default_37"/></span></div>
		</td>
	</tr>
	<tr>
		<td class="adminBackground"  valign="top">
			<input id="userDataPhone" tabindex="12" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.DataPhone" size="24" maxlength="14" border="0" value="<ffi:getProperty name="BusinessEmployee" property="DataPhone"/>">
			<span id="BusinessEmployee.dataPhoneError"></span>
		</td>
		<td class="adminBackground" width="350" valign="top">
			<input id="userFax" tabindex="11" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.FaxPhone" size="24" maxlength="14" border="0" value="<ffi:getProperty name="BusinessEmployee" property="FaxPhone"/>">
			<span id="BusinessEmployee.faxPhoneError"></span>
		</td>
		<td class="adminBackground"  valign="top">
			<input id="userAddress1" tabindex="3" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Street" value="<ffi:getProperty name="BusinessEmployee" property="Street"/>" size="24" maxlength="40" border="0">
		</td>
		<td class="adminBackground"  valign="top">
			<input id="userAddress2" tabindex="4" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Street2" value="<ffi:getProperty name="BusinessEmployee" property="Street2"/>" size="24" maxlength="40" border="0">
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<tr>
		<td id="userCityLabel" class="adminBackground">
			<div align="left"><span class="sectionsubhead"><s:text name="jsp.default_101"/></span>
			</div>
		</td>
		<ffi:cinclude value1="${StatesExists}" value2="true" operator="equals">
			<td id="userStateLabel" class="adminBackground">
				<div align="left" id="stateNameId"><span class="sectionsubhead"><s:text name="jsp.default_386"/></span></div>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${StatesExists}" value2="true" operator="notEquals">
			<ffi:setProperty name="BusinessEmployee" property="State" value=""/>
			<td class="adminBackground">&nbsp;</td>
		</ffi:cinclude>	
		<td id="userCountryLabel" class="adminBackground" valign="middle">
			<div align="left"><span class="sectionsubhead"><s:text name="jsp.default_115"/></span><span class="required">*</span></div>
		</td>
		<td id="userZipCodeLabel" class="adminBackground" valign="top">
			<div align="left"><span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
		</td>
	</tr>
	<tr class="adminUserTr2">
		<td class="adminBackground"  valign="top">
			<input id="userCity" tabindex="5" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.City" value="<ffi:getProperty name="BusinessEmployee" property="City"/>" size="24" maxlength="20" border="0">
		</td>
		<ffi:cinclude value1="${StatesExists}" value2="true" operator="equals">
			<td class="adminBackground"  valign="top">
				<div id="stateSelectId">
					<select class="txtbox" tabindex="6" id="selectUserStateID" name="BusinessEmployee.State" size="1">
						<option<ffi:cinclude value1="${BusinessEmployee.State}" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
						<ffi:list collection="StateList" items="item">
							<option <ffi:cinclude value1="${BusinessEmployee.State}" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
						</ffi:list>
					</select>
				</div>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${StatesExists}" value2="true" operator="notEquals">
			<ffi:setProperty name="BusinessEmployee" property="State" value=""/>
			<td class="adminBackground"  valign="top">&nbsp;</td>
		</ffi:cinclude>	
		
		<td class="adminBackground"  valign="middle">
			<ffi:setProperty name="SelectedUserCountry" value="${BusinessEmployee.Country}"/>
			<div class="selectBoxHolder">
				<select class="txtbox" tabindex="8" id="selectUserCountryID" name="BusinessEmployee.Country" size="1" onchange="showState(this);">
					<ffi:cinclude value1="${SelectedUserCountry}" value2="">
						<ffi:setProperty name="SelectedUserCountry" value="${SecureUser.Locale.ISO3Country}"/>
					</ffi:cinclude>
					
					<ffi:list collection="CountryList" items="item">
						<option <ffi:cinclude value1="${SelectedUserCountry}" value2="${item.CountryCode}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="CountryCode"/>"><ffi:getProperty name='item' property='Name'/></option>
					</ffi:list>
				</select>
			</div>
		</td>
		<td class="adminBackground"  valign="top">
			<input id="userZipCode" tabindex="7" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.ZipCode" value="<ffi:getProperty name="BusinessEmployee" property="ZipCode"/>" size="24" maxlength="11" border="0">
			<span id="BusinessEmployee.zipCodeError"></span>
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	
	<ffi:cinclude value1="${LanguagesList.size}" value2="1" operator="notEquals">
	<tr>
		<td id="userLanguageLabel" align="left" class="sectionsubhead adminBackground"><s:text name="jsp.user_261"/></td>
		<td colspan="3">&nbsp;</td>
	</tr>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${LanguagesList.size}" value2="1" operator="notEquals">
	<tr class="adminUserTr2">
		<td class="adminBackground"  valign="top">
			<ffi:list collection="LanguagesList" items="language">
				<ffi:cinclude value1="${language.IsDefault}" value2="true" operator="equals">
					<ffi:setProperty name="Compare" property="Value1" value="${language.Language}"/>
				</ffi:cinclude>
			</ffi:list>
			<select id="selectUserLanguageID" class="txtbox" tabindex="9" name="BusinessEmployee.PreferredLanguage">
				<ffi:list collection="LanguagesList" items="language">
					<ffi:setProperty name="Compare" property="Value2" value="${language.Language}"/>
					<option value="<ffi:getProperty name="language" property="Language" />" <ffi:getProperty name="selected${Compare.equals}"/> ><ffi:getProperty name="language" property="DisplayName"/></option>
				</ffi:list>
			</select>
		</td>
		<td colspan="3">&nbsp;</td>
	</tr>
	</ffi:cinclude>
	<tr>
		<td colspan="4" align="center">
			<div align="center">
				<br>
				<s:url id="resetAddUserButtonUrl" value="/pages/user/addBusinessUser_init.action" escapeAmp="false">  
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>				
				</s:url>	
				<sj:a id="resetAddUserBtn"
				   href="%{resetAddUserButtonUrl}"
				   targets="inputDiv"
				   button="true" ><s:text name="jsp.default_358"/></sj:a>
				<sj:a id="cancelBtn"
					  button="true"
					  summaryDivId="summary" 
					  buttonType="cancel"
					  onClickTopics="showSummary,cancelUserForm"><s:text name="jsp.default_82"/></sj:a>
				<sj:a id="verifyAddSubmit"
					  	formIds="AddUserFormID"
						targets="verifyDiv"
						button="true"
						validate="true"
						validateFunction="customValidation"
						onBeforeTopics="beforeVerify"
						onCompleteTopics="completeVerify"
						onErrorTopics="errorVerify"
						onSuccessTopics="successVerify"
						><s:text name="jsp.user_1"/></sj:a>
			</div>
		</td>
	</tr>
</table>

</s:form>
</div>

<script type="text/javascript">
$("#userPassword").strength({
});
</script>
<ffi:removeProperty name="UpdateEntGroup"/>
<ffi:removeProperty name="Init"/>
