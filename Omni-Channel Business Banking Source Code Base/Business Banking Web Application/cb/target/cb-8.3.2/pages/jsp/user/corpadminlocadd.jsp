<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadminlocadd" className="moduleHelpClass"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
<ffi:process name="GetBPWProperty"/>

<ffi:help id="user_corpadminlocadd" className="moduleHelpClass"/>


<span id="PageHeading" style="display:none;"><s:text name="jsp.user_21"/></span>

<script language="javascript">
$(function(){
	$("#selectBankID").selectmenu({width: 200});
	$("#selectAccountTypeID").selectmenu({width: 150});
	$("#selectAccountID").selectmenu({width: 200});
	$("#selectCashConCompanyID").selectmenu({width: 150});
	$("#selectConcAccountID").selectmenu({width: 250});
	$("#selectDisbAccountID").selectmenu({width: 250});
	
	  $("#selectCashConCompanyID").change(function() {
	    	showHideSameDayPrenoteFields($("option:selected", this));
	  });

});

</script>

<style>
.displayNone{display:none;}
.displayVisible{display:table-cell;}

/* --- Fix for the dropdown arrow - Issue: when the value of textbox is more than the specified width, the arrow shifts down --- */
#selectConcAccountID-menu span.ui-selectmenu-icon, #selectDisbAccountID-menu span.ui-selectmenu-icon{
	position: absolute !important;
	right:0 !important;
}
</style>

<ffi:removeProperty name="AddLocation"/>
<ffi:removeProperty name="editlocation-reload"/>
<ffi:removeProperty name="EditLocation_LocationBPWID"/>
<ffi:removeProperty name="locationBpwdId"/>
<ffi:removeProperty name="verifyEditAddLocation"/>

<ffi:setProperty name="locationAddDualApprovalMode" value="${Business.dualApprovalMode}"/>
<ffi:setProperty name='PageText' value=''/>

<ffi:setProperty name="childSequence" value="0"/>

<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditDivision_GroupId}"/>
<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="divisionEntitlementGroup" />
<ffi:process name="GetEntitlementGroup"/>

<ffi:object id="GetCumulativeSettings" name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" scope="session" />
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="divisionEntitlementGroup" />
<ffi:process name="GetCumulativeSettings" />
<ffi:setProperty name="autoEntitleLocations" value="${GetCumulativeSettings.EnableLocations}" />
<ffi:removeProperty name="GetCumulativeSettings" />

<ffi:object id="AddLocation" name="com.ffusion.tasks.cashcon.AddLocation" scope="session" />
<ffi:setProperty name="AddLocation" property="DivisionID" value="${EditDivision_GroupId}"/>
<ffi:cinclude value1="${autoEntitleLocations}" value2="true" operator="equals">
	<ffi:setProperty name="AddLocation" property="AutoEntitleLocation" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${autoEntitleLocations}" value2="true" operator="notEquals">
	<ffi:setProperty name="AddLocation" property="AutoEntitleLocation" value="false" />
</ffi:cinclude>

<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
<ffi:process name="GetAffiliateBanks"/>
<ffi:removeProperty name="GetAffiliateBanks"/>
<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />

<%-- get cashcon company list --%>
<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${EditDivision_GroupId}"/>
<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
<ffi:process name="GetCashConCompanies"/>
<ffi:removeProperty name="GetCashConCompanies"/>

<ffi:setProperty name="CashConCompanies" property="Filter" value="ACTIVE=true"/>
<ffi:setProperty name="CashConCompanies" property="FilterOnFilter" value="CONC_ENABLED=true,DISB_ENABLED=true"/>

<% session.setAttribute("FFIAddLocation", session.getAttribute("AddLocation")); %>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
		<ffi:setProperty name="autoEntitleLocations" value="false" />
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.LocalBankID}" value2="0" operator="equals">
	<ffi:list collection="AffiliateBanks" items="Bank" startIndex="1" endIndex="1">
		<ffi:setProperty name="AddLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.LocalBankID}" value2="0" operator="notEquals">
	<ffi:object id="SetAffiliateBank" name="com.ffusion.tasks.affiliatebank.SetAffiliateBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="CollectionSessionName" value="AffiliateBanks"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankSessionName" value="CurrentBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankID" value="${AddLocation.LocalBankID}"/>
	<ffi:process name="SetAffiliateBank"/>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="" operator="equals">
	<ffi:list collection="CashConCompanies" items="tempCompany" startIndex="1" endIndex="1">
		<ffi:setProperty name="AddLocation" property="CashConCompanyBPWID" value="${tempCompany.BPWID}"/>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="" operator="notEquals">
	<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${AddLocation.CashConCompanyBPWID}"/>
	<ffi:process name="SetCashConCompany"/>
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>





<SCRIPT language=JavaScript><!--

<ffi:setProperty name="AffiliateBanks" property="Filter" value=""/>
/* Account lists for each of the "pre-defined" banks. */
var bankAccounts = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
<% int bankAccountsCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
    <ffi:setProperty name="BankingAccounts" property="Filter" value="ROUTINGNUM=${affiliateBank.AffiliateRoutingNum}"/>
bankAccounts[<%= Integer.toString(bankAccountsCounter) %>] = new Array(
<ffi:setProperty name="spaceOrComma" value=" "/>
    <ffi:list collection="BankingAccounts" items="bankAccount">
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='ID'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='ID'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
    </ffi:list>
	);
<% bankAccountsCounter++; %>
</ffi:list>

/* Mappings of banks to indices into the account lists.*/
var bankIndexMapping = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
var bankIndexMappingByID = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
<% int bankIndexMappingCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
bankIndexMapping[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateRoutingNum'/>";
bankIndexMappingByID[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateBankID'/>";
<% bankIndexMappingCounter++; %>
</ffi:list>

<ffi:setProperty name="BankingAccounts" property="Filter" value=""/>

/* concentration and disbursement Account lists for each of the CC companies. */
var ccAccounts = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var disbAccounts = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var ccEnabled = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var disbEnabled = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var ccSize = <ffi:getProperty name="CashConCompanies" property="size"/>;

<% int ccCounter = 0; %>
<ffi:list collection="CashConCompanies" items="ccCompany">
	ccEnabled[<%= Integer.toString(ccCounter) %>] = <ffi:getProperty name="ccCompany" property="ConcEnabledString"/>;
	ccAccounts[<%= Integer.toString(ccCounter) %>] = new Array(
		<ffi:setProperty name="spaceOrComma" value=" "/>
    	<ffi:list collection="ccCompany.ConcAccounts" items="ccAccount">
			<ffi:getProperty name="spaceOrComma"/>
			new Array("<ffi:getProperty name="ccAccount" property="DisplayText"/><ffi:cinclude value1="${ccAccount.Nickname}" value2="" operator="notEquals"> - <ffi:getProperty name="ccAccount" property="Nickname"/></ffi:cinclude>", "<ffi:getProperty name='ccAccount' property='BPWID'/>")
			<ffi:setProperty name="spaceOrComma" value=","/>
		</ffi:list>
	);
	disbEnabled[<%= Integer.toString(ccCounter) %>] = <ffi:getProperty name="ccCompany" property="DisbEnabledString"/>;
	disbAccounts[<%= Integer.toString(ccCounter) %>] = new Array(
		<ffi:setProperty name="spaceOrComma" value=" "/>
		<ffi:list collection="ccCompany.DisbAccounts" items="disbAccount">
			<ffi:getProperty name="spaceOrComma"/>
			new Array("<ffi:getProperty name="disbAccount" property="DisplayText"/><ffi:cinclude value1="${disbAccount.Nickname}" value2="" operator="notEquals"> - <ffi:getProperty name="disbAccount" property="Nickname"/></ffi:cinclude>", "<ffi:getProperty name='disbAccount' property='BPWID'/>")
			<ffi:setProperty name="spaceOrComma" value=","/>
		</ffi:list>
	);
<% ccCounter++; %>
</ffi:list>

<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals" >
	<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals" >
		var cashConcAccountsCurrencyCode = new Array ();
		<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount" >
			cashConcAccountsCurrencyCode[ "<ffi:getProperty name="ConcAccount" property="BPWID" />" ] = "<ffi:getProperty name="ConcAccount" property="Currency" />";
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>

function updateAccountsList(routingNum)
{
	var updated = false;
	var accountIDSel = document.getElementById("selectAccountID");
	var selectedValue = accountIDSel.options[accountIDSel.selectedIndex].value;

	for (var i = 0; i < bankIndexMapping.length; i++) {
		if (bankIndexMapping[i] == routingNum) {

			if (bankAccounts[i].length == 0) {
				accountIDSel.options.length = 1;
				accountIDSel.options[0] = new Option("None", "");
			} else {
				accountIDSel.options.length = bankAccounts[i].length;

				for (var j = 0; j < bankAccounts[i].length; j++) {
					var defaultSelected = (selectedValue == bankAccounts[i][j][1]) ? true : false;
					accountIDSel.options[j] = new Option(bankAccounts[i][j][0],
							bankAccounts[i][j][1], defaultSelected, defaultSelected);
				}
			}

			updated = true;
			break;
		}
	}

	if (!updated) {
		accountIDSel.options.length = 1;
		accountIDSel.options[0] = new Option("None", "");
	}
	$("#selectAccountID").selectmenu('destroy');
	$('#selectAccountID').selectmenu({width: 200});
}

function updateAccountsListByID(id)
{
	for (var i = 0; i < bankIndexMappingByID.length; i++) {
		if (bankIndexMappingByID[i] == id) {
			updateAccountsList(bankIndexMapping[i]);
			break;
		}
	}
}

var bConc = true;
var bDisb = true;

function updateCCAccountsList(cccIndex)
{
	var concIDSel = document.getElementById("selectConcAccountID");
	var disbIDSel = document.getElementById("selectDisbAccountID");
	var selectedValue = concIDSel.options[concIDSel.selectedIndex].value;

	bConc = true;
	bDisb = true;
	
	if (ccSize == 0 || ccEnabled[cccIndex] == false || ccAccounts[cccIndex].length == 0) {
		concIDSel.options.length = 1;
		concIDSel.options[0] = new Option("None", "");
		bConc = false;
	} else {
		concIDSel.options.length = ccAccounts[cccIndex].length;
		for (var j = 0; j < ccAccounts[cccIndex].length; j++) {
			var defaultSelected = (selectedValue == ccAccounts[cccIndex][j][1]) ? true : false;
			concIDSel.options[j] = new Option(ccAccounts[cccIndex][j][0],
					ccAccounts[cccIndex][j][1], defaultSelected, defaultSelected);
		}
	}
if (disbIDSel.length == 0) {
				disbIDSel.options.length = 1;
				disbIDSel.options[0] = new Option("None", "");
}
if (disbIDSel.options[disbIDSel.selectedIndex]!= undefined){
	selectedValue = disbIDSel.options[disbIDSel.selectedIndex].value;
	if (ccSize == 0 || disbEnabled[cccIndex] == false || disbAccounts[cccIndex].length == 0) {
		disbIDSel.options.length = 1;
		disbIDSel.options[0] = new Option("None", "");
		bDisb = false;
	} else {
		disbIDSel.options.length = disbAccounts[cccIndex].length;
		for (var j = 0; j < disbAccounts[cccIndex].length; j++) {
			var defaultSelected = (selectedValue == disbAccounts[cccIndex][j][1]) ? true : false;
			disbIDSel.options[j] = new Option(disbAccounts[cccIndex][j][0],
					disbAccounts[cccIndex][j][1], defaultSelected, defaultSelected);
		}
	}
 }
	$("#selectConcAccountID").selectmenu('destroy');
	$('#selectConcAccountID').selectmenu({width: 250});
	$("#selectDisbAccountID").selectmenu('destroy');
	$('#selectDisbAccountID').selectmenu({width: 250});
	
	
	disableFields(bConc, bDisb);
}

function showHideSameDayPrenoteFields(element) {
	
    var sameDayCashConEnabled = element.attr("SameDayCashConEnabled");
    
    var concSameDayCutOffDefined = element.attr("ConcSameDayCutOffDefined");
    
    var concCutOffsPassed = element.attr("ConcCutOffsPassed");
   
    var depositSameDayPrenote = element.attr("DepositSameDayPrenote");
    var disbSameDayCutOffDefined = element.attr("DisbSameDayCutOffDefined");
    var disbCutOffsPassed = element.attr("DisbCutOffsPassed");
    var disbursementSameDayPrenote = element.attr("DisbursementSameDayPrenote");
   
    if (sameDayCashConEnabled == "true") {
    	
		if (bConc) {
				
			if (concSameDayCutOffDefined == "true") {
				 
				 $('#regularDepositPrenoteLabelClass').removeClass("displayVisible");
				 $('#regularDepositPrenoteCheckboxClass').removeClass("displayVisible");
				 $('#depositSameDayPrenoteClass').removeClass("displayNone");
				 $('#regularDepositPrenoteLabelClass').addClass("displayNone");
				 $('#regularDepositPrenoteCheckboxClass').addClass("displayNone");
				 $('#depositSameDayPrenoteClass').addClass("displayVisible");
				 $('#regularDepositPrenote').removeAttr("name");
				 $('#__checkbox_regularDepositPrenote').removeAttr("name");
				 $('#DepositPrenote').attr("name", "AddLocation.DepositPrenote" );
				 $('#__checkbox_DepositPrenote').attr("name", "__checkbox_AddLocation.DepositPrenote" );
				 
				if (concCutOffsPassed == "true") {
					$('#showConcNote').show();
					
					$('#DepositSameDayPrenote').attr("checked", false);
					$('#DepositSameDayPrenote').removeAttr("checked");
					$('#DepositSameDayPrenote').attr("disabled", true);
				} else {
					$('#showConcNote').hide();
					$('#DepositSameDayPrenote').removeAttr("disabled");
				}
			} else {
				disableDipositSameDayFields();
			}
		
			if (depositSameDayPrenote == "true") {
				$('#DepositPrenote').attr('checked', false);
				$('#DepositPrenote').removeAttr('checked');
				$('#regularDepositPrenote').attr('checked', false);
				$('#regularDepositPrenote').removeAttr('checked');
			}
		}
		if (bDisb) {
			if (disbSameDayCutOffDefined == "true") {
				
				 $('#regularDisbursementPrenoteCheckboxClass').removeClass("displayVisible");
				 $('#regularDisbursementPrenoteLabelClass').removeClass("displayVisible");
				 $('#regularDisbursementPrenoteCheckboxClass').addClass("displayNone");
				 $('#regularDisbursementPrenoteLabelClass').addClass("displayNone");
				 $('#disbursementSameDayPrenoteClass').addClass("displayVisible");
				 $('#disbursementSameDayPrenoteClass').removeClass("displayNone");
				 $('#regularDisbursementPrenote').removeAttr("name");
				 $('#__checkbox_regularDisbursementPrenote').removeAttr("name");
				 $('#DisbursementPrenote').attr("name", "AddLocation.DisbursementPrenote" );
				 $('#__checkbox_DisbursementPrenote').attr("name", "__checkbox_AddLocation.DisbursementPrenote" );
				if (disbCutOffsPassed == "true") {
					$('#showDisbNote').show();
					$('#DisbursementSameDayPrenote').attr("checked", false);
					$('#DisbursementSameDayPrenote').removeAttr("checked");
					$('#DisbursementSameDayPrenote').attr("disabled", true);
				} else {
					$('#showDisbNote').hide();
					$('#DisbursementSameDayPrenote').removeAttr("disabled");
				}
			} else {
				disableDisbursementSameDayFields();
			}
			
			if (disbursementSameDayPrenote == "true") {
				$('#DisbursementPrenote').attr('checked', false);
				$('#DisbursementPrenote').removeAttr('checked');
				$('#regularDisbursementPrenote').attr('checked', false);
				$('#regularDisbursementPrenote').removeAttr('checked');
			}
		}
    } else {
    	disableDipositSameDayFields();
    	disableDisbursementSameDayFields();
    }
}

function disableDipositSameDayFields()
{ 
	 document.getElementById('DepositSameDayPrenote').value = false;
	 $('#regularDepositPrenoteLabelClass').removeClass("displayNone");
	 $('#regularDepositPrenoteCheckboxClass').removeClass("displayNone");
	 $('#depositSameDayPrenoteClass').removeClass("displayVisible");
	 $('#regularDepositPrenoteLabelClass').addClass("displayVisible");
	 $('#regularDepositPrenoteCheckboxClass').addClass("displayVisible");
	 $('#depositSameDayPrenoteClass').addClass("displayNone");
	 $('#DepositPrenote').removeAttr("name");
	 $('#__checkbox_DepositPrenote').removeAttr("name");
	 $('#regularDepositPrenote').attr("name", "AddLocation.DepositPrenote" );
	 $('#__checkbox_regularDepositPrenote').attr("name", "__checkbox_AddLocation.DepositPrenote" );
	 $('#showConcNote').hide();
}

function disableDisbursementSameDayFields()
{ 
	 document.getElementById('DisbursementSameDayPrenote').value = false;
	 $('#regularDisbursementPrenoteCheckboxClass').removeClass("displayNone");
	 $('#regularDisbursementPrenoteLabelClass').removeClass("displayNone");
	 $('#disbursementSameDayPrenoteClass').removeClass("displayVisible");
	 $('#regularDisbursementPrenoteCheckboxClass').addClass("displayVisible");
	 $('#regularDisbursementPrenoteLabelClass').addClass("displayVisible");
	 $('#disbursementSameDayPrenoteClass').addClass("displayNone");
	 $('#DisbursementPrenote').removeAttr("name");
	 $('#__checkbox_DisbursementPrenote').removeAttr("name");
	 $('#regularDisbursementPrenote').attr("name", "AddLocation.DisbursementPrenote" );
	 $('#__checkbox_regularDisbursementPrenote').attr("name", "__checkbox_AddLocation.DisbursementPrenote" );
	 $('#showDisbNote').hide();
}

function disableFields(depositEnabled, disbursementEnabled)
{
	
	if (depositEnabled) {
		$('#ConsolidateDeposits').removeAttr("disabled");
		$('#DepositPrenote').removeAttr("disabled");
		$('#regularDepositPrenote').removeAttr("disabled");
		$('#DepositSameDayPrenote').removeAttr("disabled");
		$('#DepositMinimum').removeAttr("disabled");
		$('#DepositMaximum').removeAttr("disabled");
		$('#AnticDeposit').removeAttr("disabled");
		$('#ThreshDeposit').removeAttr("disabled");
	} else {
		$('#ConsolidateDeposits').attr("disabled", true);
		$('#DepositPrenote').attr("disabled", true);
		$('#regularDepositPrenote').attr("disabled", true);
		$('#DepositSameDayPrenote').attr("disabled", true);
		$('#DepositMinimum').attr("disabled", true);
		$('#DepositMaximum').attr("disabled", true);
		$('#AnticDeposit').attr("disabled", true);
		$('#ThreshDeposit').attr("disabled", true);
		$('#ConsolidateDeposits').attr('checked', false);
		$('#DepositPrenote').attr('checked', false);
		$('#regularDepositPrenote').attr('checked', false);
		$('#DepositMinimum').val('');
		$('#DepositMaximum').val('');
		$('#AnticDeposit').val('');
		$('#ThreshDeposit').val('');

	}
	if (disbursementEnabled) {
		$('#regularDisbursementPrenote').removeAttr("disabled");
		$('#DisbursementPrenote').removeAttr("disabled");
		$('#DisbursementSameDayPrenote').removeAttr("disabled");
	} else {
		$('#regularDisbursementPrenote').attr("disabled", true);
		$('#regularDisbursementPrenote').checked = false;
		$('#DisbursementPrenote').attr("disabled", true);
		$('#DisbursementPrenote').checked = false;
		$('#DisbursementSameDayPrenote').attr("disabled", true);
		$('#DisbursementSameDayPrenote').checked = false;
	}
}


function checkSameDayFields() {
	<ffi:cinclude value1="${AddLocation.DepositSameDayPrenote}" value2="true" operator="equals">;
		$('#DepositSameDayPrenote').attr("checked", true);
		$('#DepositPrenote').attr('checked', false);
	</ffi:cinclude>;

	<ffi:cinclude value1="${AddLocation.DisbursementSameDayPrenote}" value2="true" operator="equals">;
		$('#DisbursementSameDayPrenote').attr("checked", true);
		$('#DisbursementPrenote').attr('checked', false);
	</ffi:cinclude>;
}

$(document).ready(function(){
	updateCCAccountsList(0);
	
	checkSameDayFields();
	
	showHideSameDayPrenoteFields($("#selectCashConCompanyID option:selected"));

	<ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">
			updateAccountsList('<ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>');
	</ffi:cinclude>
	<ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">
			updateAccountsList('<ffi:getProperty name="CurrentBank" property="AffiliateRoutingNum"/>');
	</ffi:cinclude>
});

function removeDepositSameDayPrenoteChecked(prenoteId) {
	if (prenoteId.checked) {
		document.getElementById('DepositSameDayPrenote').checked = false;
		document.getElementById('DepositPrenote').value = true;
	} else {
		document.getElementById('DepositPrenote').value = false;
	} 
}

function removeDepositNormalPrenoteChecked(sameDayPrenoteId) {
    if(sameDayPrenoteId.checked) {
    	document.getElementById('DepositPrenote').checked = false;
    	document.getElementById('DepositSameDayPrenote').value = true;
    } else {
    	document.getElementById('DepositSameDayPrenote').value = false;
    }
}

function removeDisbursementSameDayPrenoteChecked(prenoteId) {
	if (prenoteId.checked) {
		document.getElementById('DisbursementSameDayPrenote').checked = false;
		document.getElementById('DisbursementPrenote').value = true;
	} else {
		document.getElementById('DisbursementPrenote').value = false;
	} 
}

function removeDisbursementNormalPrenoteChecked(sameDayPrenoteId) {
    if(sameDayPrenoteId.checked) {
    	document.getElementById('DisbursementPrenote').checked = false;
    	document.getElementById('DisbursementSameDayPrenote').value = true;
    } else {
    	document.getElementById('DisbursementSameDayPrenote').value = false;
    }
}

//--></SCRIPT>
	<ffi:setProperty name="AddLoc" value="TRUE" />
		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr> 
			</table>
			<table  cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10">
				<tr>
					<td width="22%" valign="top"> 
						<span class="ffivisible marginTop10">&nbsp;</span>
						<div class="paneWrapper marginTop20">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="admin.division.summary"/>
							</div>
							<div class="paneContentWrapper">
								<table cellspacing="0" cellpadding="3" border="0" width="100%">
									<tr>
										<td><s:text name="jsp.user_121"/>:&nbsp; 
										<%-- <select id="divisionId" class="txtbox" name="division" size="1">
					<ffi:cinclude value1="" value2="<ffi:getProperty name='EditGroup_DivisionName'/>" operator="notEquals">
						<option selected><ffi:getProperty name='EditGroup_DivisionName'/>
						</option>
					</ffi:cinclude> --%>
				</select>
				<ffi:getProperty name="EditGroup_DivisionName"/></td>
									</tr>
								</table>
							</div>
							</div>
						</div>
					</td>
					<td width="1%">&nbsp;</td>
					<td align="left" class="adminBackground" width="77%" valign="top">
						<s:form id="AddEditLocationFormID" namespace="/pages/user" action="verifyaddLocation" theme="simple" name="AddEditLocationForm" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="AddLocation.DivisionID" value="<ffi:getProperty name="EditDivision_GroupId"/>"/>
						<input type="hidden" name="AddingFreeFormAccount" value="true"/>

							<div id="addLocationPanel" class="rightPaneWrapper"  style="width:100%">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td><p class="transactionHeading"><s:text name="admin.location.summary"/></p></td>
									</tr>
								</table>
								<table border="0" cellspacing="0" cellpadding="3" align="left">	
									<tr class="columndataauto">
										<td class="sectionsubhead ltrow2_color" align="left">
											<s:text name="jsp.default_267"/><span class="required">*</span>
										</td>
										<td width="140">&nbsp;</td>
										<td class="sectionsubhead ltrow2_color" align="left">
											<s:text name="jsp.user_189"/><span class="required">*</span>
										</td>
										<td width="80">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocationName" size="24" maxlength="16" border="0" value="<ffi:getProperty name="AddLocation" property="LocationName"/>">
											<br><span id="AddLocation.locationNameError"></span>
										</td>
										<td>&nbsp;</td>
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocationID" size="24" maxlength="15" border="0" value="<ffi:getProperty name="AddLocation" property="LocationID"/>">
											<br><span id="AddLocation.locationIDError"></span>
										</td>
										<td>&nbsp;</td>
										<td>
											<s:checkbox name="AddLocation.Active" value="%{#session.AddLocation.Active}" fieldValue="true"/>&nbsp;<s:text name="jsp.default_28"/> 
										</td>
									</tr>
								</table>
								
								
								<table width="100%" border="0" cellspacing="0" cellpadding="3"><!-- Bank -->
									
									<tr  class="columndataauto" >
										<td colspan="4"><p class="transactionHeading"><s:text name="jsp.user_188"/></p></td>
									</tr>
									<tr class="columndataauto">
										<td class="sectionsubhead ltrow2_color" align="left" height="50" width="200">
											<input type="radio" name="AddLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">checked</ffi:cinclude>>
											&nbsp;<s:text name="jsp.default_61"/>&nbsp;<ffi:getProperty name="TempBankIdentifierDisplayText"/><span class="required">*</span>
											:</span>
										</td>
										<td class="sectionsubhead ltrow2_color" width="230" height="50" valign="middle">
											<input id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value); AddEditLocationForm['AddLocation.CustomLocalBank'][0].checked = true;">
										</td>
										<td class="sectionsubhead ltrow2_color" width="270">
											<span class="ffivisible marginRight10"><s:text name="jsp.default_306" /><span>   &nbsp; &nbsp; &nbsp; &nbsp;
											<input id="BANKNAME" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="AddLocation" property="LocalBankName"/>">
										</td>
										<td class="sectionsubhead ltrow2_color">
											<sj:a button="true" title="%{getText('jsp.default_373')}" onClickTopics="location_selectBankTopics"><s:text name="jsp.default_373" /></sj:a>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;<span id="AddLocation.localRoutingNumberError"></span></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="4" height="50">
											<input type="radio" name="AddLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID($('#selectBankID').val());" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
											<select id="selectBankID" class="txtbox" name="AddLocation.LocalBankID" onchange="updateAccountsListByID($('#selectBankID').val()); AddEditLocationForm['AddLocation.CustomLocalBank'][1].checked = true;">
												<ffi:list collection="AffiliateBanks" items="Bank">
													<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${AddLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
													</option>
												</ffi:list>
											</select>
										</td>
									</tr>
									<!-- Bank  -->
									<tr>
										<td>&nbsp;</td>
									</tr>
									<!-- Account Information  -->
									<tr class="columndataauto">
										<td colspan="4">
										<table  width="90%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="sectionsubhead ltrow2_color" align="left"  height="50" width="180">
												<input type="radio" id="customLocalAccountID" name="AddLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
												&nbsp;<span class="sectionsubhead"><s:text name="jsp.default_19"/><span class="required">*</span> :</span>
											</td>
											<td class="sectionsubhead ltrow2_color" width="250">
												<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="AddLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['AddLocation.CustomLocalAccount'][0].checked = true;">
											</td>
											<td class="sectionsubhead ltrow2_color"  width="130">
												<span class="sectionsubhead"><s:text name="jsp.default_20"/>:</span>
											</td>
											<td class="sectionsubhead ltrow2_color">
												<select id="selectAccountTypeID" class="txtbox" name="AddLocation.LocalAccountType">
													<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
													<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
												</select>
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;<span id="AddLocation.localAccountNumberError"></span></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="4" height="50">
												<input type="radio" name="AddLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
												&nbsp;&nbsp;
												<select id="selectAccountID" class="txtbox" name="AddLocation.LocalAccountID" onchange="AddEditLocationForm['AddLocation.CustomLocalAccount'][1].checked = true;">
												    <option value=""><s:text name="jsp.default_296"/></option>
												</select><span id="AddLocation.LocalAccountIDError"></span>
											</td>
										</tr>
										</table>
										</td>
									</tr>
									<!-- Account Information  -->
								</table>
								
								<!-- Remote Bank information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_276"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="sectionsubhead"><s:text name="jsp.user_55"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_75"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_117"/><span class="required">*</span>:</span></td>
									</tr>
									<tr class="columndataauto" >
										<td>
										<select id="selectCashConCompanyID" class="txtbox" name="AddLocation.CashConCompanyBPWID" onchange="updateCCAccountsList(this.selectedIndex)" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
										    <option value=""><s:text name="jsp.default_296"/></option>
</ffi:cinclude>
											<ffi:list collection="CashConCompanies" items="tempCompany">
												<option 													
												value="<ffi:getProperty name="tempCompany" property="BPWID"/>" 
												SameDayCashConEnabled="<ffi:getProperty name="SameDayCashConEnabled"/>"  
												ConcSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="ConcSameDayCutOffDefined"/>"
												ConcCutOffsPassed="<ffi:getProperty name="tempCompany" property="ConcCutOffsPassed"/>"
												DepositSameDayPrenote="<ffi:getProperty name="AddLocation" property="DepositSameDayPrenote"/>"
												DisbSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="DisbSameDayCutOffDefined"/>"
												DisbCutOffsPassed="<ffi:getProperty name="tempCompany" property="DisbCutOffsPassed"/>"
												DisbursementSameDayPrenote="<ffi:getProperty name="AddLocation" property="DisbursementSameDayPrenote"/>"
											
												<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
													<ffi:getProperty name="tempCompany" property="CompanyName"/>
													
												</option>
											</ffi:list>
										</select>
										</td>
										<td>
											<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
												<option value="" selected><s:text name="jsp.default_296"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
												<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
													<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="ConcAccount" property="DisplayText"/>
														<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
															 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
														 </ffi:cinclude>
													</option>
												</ffi:list>
											</ffi:cinclude>
										</select>
										</td>
										<td>
											<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
												<option value="" selected><s:text name="jsp.default_296"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
												<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
													<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="DisbAccount" property="DisplayText"/>
														<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
															 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
														 </ffi:cinclude>
													</option>
												</ffi:list>
											</ffi:cinclude>
										</select>
										<br><span id="AddLocation.DisbAccountBPWIDError"></span>
										</td>
									</tr>
								</table>
								<!-- Remote Bank information -->
								
								<!-- Deposit-specific information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_113"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="sectionsubhead"><s:text name="jsp.user_112"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_111"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_44"/>:</span></td>
									</tr>
									<tr class="columndataauto" >
										<td>
											<input class="ui-widget-content ui-corner-all" id="DepositMinimum" type="text" name="AddLocation.DepositMinimum" size="18" maxlength="11" border="0">
											<br><span id="depositMinimumError"></span>
										</td>
										<td>
											<input class="ui-widget-content ui-corner-all" id="DepositMaximum" type="text" name="AddLocation.DepositMaximum" size="18" maxlength="11" border="0">
											<br><span id="depositMaximumError"></span>
										</td>
										<td>
											<input class="ui-widget-content ui-corner-all" id="AnticDeposit" type="text" name="AddLocation.AnticDeposit" size="18" maxlength="11" border="0">
											<br><span id="anticDepositError"></span>
										</td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="sectionsubhead"><s:text name="jsp.user_337"/>:</span></td>
										<td></td>
										<td></td>
									</tr>
									
										<tr class="columndataauto" >
											<td>
												<input class="ui-widget-content ui-corner-all" id="ThreshDeposit" type="text" name="AddLocation.ThreshDeposit" size="18" maxlength="11" border="0">
												<br><span id="threshDepositError"></span></td>
											<td>
												<s:checkbox id="ConsolidateDeposits" name="AddLocation.ConsolidateDeposits" value="%{#session.AddLocation.ConsolidateDeposits}" fieldValue="true"/>
												<span class="sectionsubhead"><s:text name="jsp.user_94"/></span>
											</td>
											<td id="regularDepositPrenoteCheckboxClass" class="displayNone">
												 <div id="regularDepositPrenoteCheckbox">
													 <span><s:checkbox id="regularDepositPrenote" name="AddLocation.DepositPrenote" value="%{#session.AddLocation.DepositPrenote}" fieldValue="true"/></span>
													 <span class="sectionsubhead"><s:text name="jsp.user_110"/></span>
												 </div>
											</td>
											</tr>
											<tr class="columndataauto" >
											<td id="depositSameDayPrenoteClass" colspan="3" class="displayNone">
													<table width="84%">
													<tr>
														<td>
															<div id="depositPrenoteLabel" >
																<span class="sectionsubhead"><s:text name="jsp.user_110"/></span>
															</div>	
														</td>
														 <td>
															 <div id="depositPrenoteCheckbox" >
																<span>
																	<s:checkbox id="DepositPrenote" name="AddLocation.DepositPrenote" 
																	value="%{#session.AddLocation.DepositPrenote}" fieldValue="true" onchange="removeDepositSameDayPrenoteChecked(this);"/>
																</span><span class="sectionsubhead"><s:text name="jsp.user_571"/></span>
															</div>		
														 </td>
														 <td>
															  <div id="depositSameDayPrenoteCheckbox" >
																  <span>
																	<s:checkbox id="DepositSameDayPrenote" name="AddLocation.DepositSameDayPrenote" 
																	value="%{#session.AddLocation.DepositSameDayPrenote}" fieldValue="true" onchange="removeDepositNormalPrenoteChecked(this);" />
																 </span> <span class="sectionsubhead"><s:text name="jsp.user_572"/>	
															 </div>
														 </td>
														
													</tr>
													<tr class="columndataauto" >
														<td colspan="3">
															<table width="100%">
																<tr>
																	<td>
																		<div id="showConcNote" >
																			<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span>
																		</div>
																	</td>
																	
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
								</table>
								<!-- Deposit-specific information -->
								
								
								<!-- Disbursement-Request-Specific-Information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_119"/></p></td>
									</tr>
									
									
									<tr class="columndataauto" >
										<td id="regularDisbursementPrenoteCheckboxClass" class="displayNone">
											 <div id="regularDisbursementPrenoteCheckbox" >
										     	<span> <s:checkbox id="regularDisbursementPrenote" name="AddLocation.DisbursementPrenote" value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true"/> </span>
												<span class="sectionsubhead"><s:text name="jsp.user_118"/></span>
											 </div>
									    </td>
									</tr>
									<tr class="columndataauto" >
									    <td id="disbursementSameDayPrenoteClass" colspan="3" class="displayNone">
									    	<table width="70%">
									    	<tr>
										    	<td>
													<div id="disbursementPrenoteLabel" >
														<span class="sectionsubhead"><s:text name="jsp.user_118"/></span>
													</div>
												</td>
												<td>
													 <div id="disbursementPrenoteCheckbox" >
													 	<span><s:checkbox id="DisbursementPrenote" name="AddLocation.DisbursementPrenote" 
															  value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true" onchange="removeDisbursementSameDayPrenoteChecked(this);"/>
														</span><span class="sectionsubhead"><s:text name="jsp.user_571"/></span>		
					 								 </div>	
												</td>
												<td>
													  <div id="disbursementSameDayPrenoteCheckbox" >
													  <span>
														<s:checkbox id="DisbursementSameDayPrenote" name="AddLocation.DisbursementSameDayPrenote" 
														value="%{#session.AddLocation.DisbursementSameDayPrenote}" fieldValue="true" onchange="removeDisbursementNormalPrenoteChecked(this);" />
													  </span><span class="sectionsubhead"><s:text name="jsp.user_572"/></span>
												  </div>
												</td>
												
											</tr>
											<tr class="columndataauto" >
												<td colspan="3">
													<table width="100%">
														<tr>
															<td>
																<div id="showDisbNote" >
																<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span></div>															
															</td>
														</tr>
													</table>
												</td>
											</tr>
									    	</table>
									    </td>
									</tr>									
								</table>
								<!-- Disbursement-Request-Specific-Information -->
								
								<%-- <table border="1" cellspacing="0" cellpadding="3" width="710">
									<tr align="left">
										<td colspan="4" class="adminBackground tbrd_b">
											<span class="sectionhead"><s:text name="jsp.user_188"/></span><br>
										</td>
										<td class="adminBackground">&nbsp;</td>
										<td class="adminBackground">&nbsp;</td>
										<td class="tbrd_b adminBackground" colspan="4">
											<span class="sectionhead"><s:text name="jsp.user_113"/></span>
										</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="4">
											<div align="left"><span class="sectionsubhead"><s:text name="jsp.default_61"/></span></div>
										</td>
										<td valign="top" class="tbrd_r adminBackground" rowspan="13">&nbsp;</td>
										<td valign="top" class="adminBackground" rowspan="13">&nbsp;</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_112"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" id="DepositMinimum" type="text" name="AddLocation.DepositMinimum" size="18" maxlength="11" border="0">
											</div>
											<span id="depositMinimumError"></span>
										</td>
									</tr>
									<tr valign="middle">
										<td>
											<div align="left">
												&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">checked</ffi:cinclude>>
											</div>
										</td>
										<td nowrap>
											<div align="right"><span class="sectionsubhead">
											brno  <ffi:getProperty name="TempBankIdentifierDisplayText"/><span class="required">*</span>
											:</span></div>
										</td>
										<td>
											<div align="left">
												<input id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value); AddEditLocationForm['AddLocation.CustomLocalBank'][0].checked = true;">
											</div>
											<span id="AddLocation.localRoutingNumberError"></span>
										</td>
										<td>
											serc<sj:a button="true" title="%{getText('jsp.default_373')}" onClickTopics="location_selectBankTopics"><s:text name="jsp.default_373" /></sj:a>
										</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_111"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" id="DepositMaximum" type="text" name="AddLocation.DepositMaximum" size="18" maxlength="11" border="0">
											</div>
											<span id="depositMaximumError"></span>
										</td>
									</tr>
									<tr valign="middle">
										<td>&nbsp;</td>
										<td nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.default_63"/>:</span></div>
										</td>
										<td>
										<div align="left">
											<input id="BANKNAME" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="AddLocation" property="LocalBankName"/>">
										</div>
											<span id="AddLocation.localBankNameError"></span>
										</td>
										<td></td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_44"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" id="AnticDeposit" type="text" name="AddLocation.AnticDeposit" size="18" maxlength="11" border="0">
											</div>
											<span id="anticDepositError"></span>
										</td>
									</tr>
									<tr valign="middle">
										<td>
											&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID($('#selectBankID').val());" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="selectBankID" class="txtbox" name="AddLocation.LocalBankID" onchange="updateAccountsListByID($('#selectBankID').val()); AddEditLocationForm['AddLocation.CustomLocalBank'][1].checked = true;">
													<ffi:list collection="AffiliateBanks" items="Bank">
														<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${AddLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
															<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
														</option>
													</ffi:list>
												</select>
											</div>
										</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_337"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" id="ThreshDeposit" type="text" name="AddLocation.ThreshDeposit" size="18" maxlength="11" border="0">
											</div>
											<span id="threshDepositError"></span>
										</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="4">
											<div align="left"> <span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
										</td>
										<td  align="right">
											<s:checkbox id="ConsolidateDeposits" name="AddLocation.ConsolidateDeposits" value="%{#session.AddLocation.ConsolidateDeposits}" fieldValue="true"/>
										</td>
										<td colspan="3" align="left"><span class="sectionsubhead"><s:text name="jsp.user_94"/></span></td>
									</tr>
									<tr valign="middle">
										<td>
											<div align="left">
												&nbsp;&nbsp;<input type="radio" id="customLocalAccountID" name="AddLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
											</div>
										</td>
										<td nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.default_19"/><span class="required">*</span>:</span></div>
										</td>
										<td>
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="AddLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['AddLocation.CustomLocalAccount'][0].checked = true;">
											</div>
											<span id="AddLocation.localAccountNumberError"></span>
										</td>
										<td>&nbsp; </td>
										<td id="regularDepositPrenoteCheckboxClass" class="displayNone"  align="right">
											 <div id="regularDepositPrenoteCheckbox">
												 <span><s:checkbox id="regularDepositPrenote" name="AddLocation.DepositPrenote" value="%{#session.AddLocation.DepositPrenote}" fieldValue="true"/></span>
											 </div>
										 </td>
										 <td id="regularDepositPrenoteLabelClass" colspan="3" class="displayNone">
											  <div id="regularDepositPrenoteLabel" >
												<span class="sectionsubhead"><s:text name="jsp.user_110"/></span>
											  </div>
										</td>

										<td id="depositSameDayPrenoteClass" colspan="4" class="displayNone">
											<table width="100%">
											<tr>
												<td>
													<div id="depositPrenoteLabel" >
														<span class="sectionsubhead"><s:text name="jsp.user_110"/></span>
													</div>	
												</td>
												 <td>
													 <div id="depositPrenoteCheckbox" >
													 	<span>
														 	<s:checkbox id="DepositPrenote" name="AddLocation.DepositPrenote" 
															value="%{#session.AddLocation.DepositPrenote}" fieldValue="true" onchange="removeDepositSameDayPrenoteChecked(this);"/>
													 	</span>
													</div>		
												 </td>
												  
												 <td align="left">
													 <div id="depositPrenoteText" >
													 	<span class="sectionsubhead"><s:text name="jsp.user_571"/></span>
													 </div>
												 </td>
												 
												 <td>
													  <div id="depositSameDayPrenoteCheckbox" >
														  <span>
														 	<s:checkbox id="DepositSameDayPrenote" name="AddLocation.DepositSameDayPrenote" 
															value="%{#session.AddLocation.DepositSameDayPrenote}" fieldValue="true" onchange="removeDepositNormalPrenoteChecked(this);" />
														 </span>	
												 	 </div>
												 </td>
												 <td align="left">
												 	<div id="depositSameDayPrenoteText" >
													 <span class="sectionsubhead"><s:text name="jsp.user_572"/>
													 </div>
												 </td>
											</tr>
											<tr>
												<td colspan="5">
													<table align="left">
														<tr>
															<td>
																<div id="showConcNote" >
																	<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									</tr>
									<tr valign="middle">
										<td>&nbsp;</td>
										<td nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.default_20"/>:</span></div>
										</td>
										<td>
											<select id="selectAccountTypeID" class="txtbox" name="AddLocation.LocalAccountType">
												<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
												<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
											</select>
										</td>
										<td></td>
										<td colspan="4">&nbsp;</td>
									</tr>
									<tr valign="middle" align="left">
										<td>
											&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="selectAccountID" class="txtbox" name="AddLocation.LocalAccountID" onchange="AddEditLocationForm['AddLocation.CustomLocalAccount'][1].checked = true;">
												    <option value=""><s:text name="jsp.default_296"/></option>
												</select><span id="AddLocation.LocalAccountIDError"></span>
											</div>
										</td>
										<td colspan="4" class="tbrd_b"><span class="sectionhead"><s:text name="jsp.user_119"/></span></td>
									</tr>
									<tr valign="middle">
										<td colspan="4">&nbsp;</td>
										<td id="regularDisbursementPrenoteCheckboxClass" class="displayNone"  align="right">
											 <div id="regularDisbursementPrenoteCheckbox" >
										     	<span> <s:checkbox id="regularDisbursementPrenote" name="AddLocation.DisbursementPrenote" value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true"/> </span>
											 </div>
									    </td>
									    <td id="regularDisbursementPrenoteLabelClass" colspan="3" class="displayNone" nowrap>
									    	<div id="regularDisbursementPrenoteLabel">
											 	<span class="sectionsubhead"><s:text name="jsp.user_118"/></span>
											</div>
									    </td>
									    <td id="disbursementSameDayPrenoteClass" colspan="4" class="displayNone">
									    	<table width="100%">
									    	<tr>
										    	<td>
													<div id="disbursementPrenoteLabel" >
														<span class="sectionsubhead"><s:text name="jsp.user_118"/></span>
													</div>
												</td>
												<td>
													 <div id="disbursementPrenoteCheckbox" >
													 	<span><s:checkbox id="DisbursementPrenote" name="AddLocation.DisbursementPrenote" 
															  value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true" onchange="removeDisbursementSameDayPrenoteChecked(this);"/>
														</span>		
					 								 </div>	
												</td>
												
												<td align="left">
													 <div id="disbursementPrenoteText" >
													 	<span class="sectionsubhead"><s:text name="jsp.user_571"/></span>
													 </div>
												</td>
												<td>
													  <div id="disbursementSameDayPrenoteCheckbox" >
													  <span>
														<s:checkbox id="DisbursementSameDayPrenote" name="AddLocation.DisbursementSameDayPrenote" 
														value="%{#session.AddLocation.DisbursementSameDayPrenote}" fieldValue="true" onchange="removeDisbursementNormalPrenoteChecked(this);" />
													  </span>
												  </div>
												</td>
												<td align="left" nowrap>
													 <div id="disbursementSameDayPrenoteText" >
												   		<span class="sectionsubhead"><s:text name="jsp.user_572"/></span>
												     </div>
												</td>
											</tr>
											<tr>
												<td colspan="5">
													<table align="left">
														<tr>
															<td>
																<div id="showDisbNote" >
																<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span></div>															</td>
															</td>
														</tr>
													</table>
												</td>
											</tr>
									    	</table>
									    </td>

									</tr>
									<tr valign="middle" align="left">
										<td class="tbrd_b adminBackground" colspan="4"><span class="sectionhead"><s:text name="jsp.user_276"/></span></td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_55"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<select id="selectCashConCompanyID" class="txtbox" name="AddLocation.CashConCompanyBPWID" onchange="updateCCAccountsList(this.selectedIndex)" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
												    <option value=""><s:text name="jsp.default_296"/></option>
</ffi:cinclude>
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<option 													
														value="<ffi:getProperty name="tempCompany" property="BPWID"/>" 
														SameDayCashConEnabled="<ffi:getProperty name="SameDayCashConEnabled"/>"  
														ConcSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="ConcSameDayCutOffDefined"/>"
														ConcCutOffsPassed="<ffi:getProperty name="tempCompany" property="ConcCutOffsPassed"/>"
														DepositSameDayPrenote="<ffi:getProperty name="AddLocation" property="DepositSameDayPrenote"/>"
														DisbSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="DisbSameDayCutOffDefined"/>"
														DisbCutOffsPassed="<ffi:getProperty name="tempCompany" property="DisbCutOffsPassed"/>"
														DisbursementSameDayPrenote="<ffi:getProperty name="AddLocation" property="DisbursementSameDayPrenote"/>"
													
														<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
															
														</option>
													</ffi:list>
												</select>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_75"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
															<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
																 </ffi:cinclude>
															</option>
														</ffi:list>
													</ffi:cinclude>
												</select>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubhead"><s:text name="jsp.user_117"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" <ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">disabled</ffi:cinclude>>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
															<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
																 </ffi:cinclude>
															</option>
														</ffi:list>
													</ffi:cinclude>
												</select>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td width="">&nbsp;</td>
										<td width="">&nbsp;</td>
										<td width="">&nbsp;</td>
										<td width="">&nbsp;</td>
										<td valign="" class="adminBackground">&nbsp;</td>
										<td valign="" class="adminBackground">&nbsp;</td>
										<td width="">&nbsp;</td>
										<td width="">&nbsp;</td>
										<td width="">&nbsp;</td>
										<td width="">&nbsp;</td>
									</tr>
								</table> --%>
								<div align="center">
								<span class="required">* <s:text name="jsp.default_240"/></span>
								<br><br>
								<s:url id="resetEditAddButtonUrl" value="/pages/jsp/user/corpadminlocadd.jsp"> 
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>	
										<s:param name="addlocation-reload" value="false"></s:param>											
								</s:url>	
								<sj:a id="resetAddLocBtn"
		                            href="%{resetEditAddButtonUrl}"
									targets="inputDiv"
									button="true"><s:text name="jsp.default_358"/></sj:a>
								<sj:a
								   button="true"
								   summaryDivId="summary" buttonType="cancel"
								   onClickTopics="cancelLocationForm"><s:text name="jsp.default_82"/></sj:a>
								<sj:a id="verifyEditSubmit"
									formIds="AddEditLocationFormID"
									targets="verifyDiv"
									button="true"
									validate="true"
									validateFunction="customValidation"
									onBeforeTopics="beforeVerify"
									onCompleteTopics="completeVerify"
									onErrorTopics="errorVerify"
									onSuccessTopics="successVerify"
									><s:text name="jsp.user_1"/></sj:a>
								</div>	
							</div>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
<ffi:removeProperty name="AddingFreeFormAccount"/>
<ffi:removeProperty name="autoEntitleLocations" />

