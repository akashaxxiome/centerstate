<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>

<ffi:help id="user_lookupuser" className="moduleHelpClass"/>
<script type="text/javascript"><!--

function FillInAgentIDAndName() {
	var idPre = $("#input_id_prefix").html();
	var textBoxForID = "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_AGENT %>";
	var textBoxForName = "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_AGENT_NAME %>";
	$("#" + idPre + textBoxForID).val( $("#lookupuser_list").val() );
	$("#" + idPre + textBoxForName).val( $.trim($("#lookupuser_list :selected").text()) );
	if ( $("#lookupuser_list").val() != null ) { ns.common.closeDialog(); }
	return false;
}

--></script>
		<div align="center">
		<span id="input_id_prefix" style="display:none"></span>
		<form method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<table width="281" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<%--<%
					SecureUser su = ( SecureUser )session.getAttribute( "SecureUser" );
					BusinessEmployee be = new BusinessEmployee();
					be.set( ProfileDefines.BANK_ID, su.getBankID() );
					session.setAttribute( "BusinessEmployee", be );
				%>--%>
			<td>
                         	<ffi:object id="BankEmployees" name="com.ffusion.tasks.bankemployee.GetBankEmployeeList" scope="session"/>
                         	<ffi:process name="BankEmployees"/>

                         	<ffi:setProperty name="BankEmployees" property="SortedBy" value="LAST"/>

				<select id="lookupuser_list" name="selectName" size="10" width="281" class="ui-widget-content ui-corner-all" style="width: 281">
					<ffi:list collection="BankEmployees" items="bankEmployee">
						<option value="<ffi:getProperty name='bankEmployee' property='Id'/>">
						<ffi:getProperty name="bankEmployee" property="SortableFullNameWithLoginId"/>,
						</option>
					</ffi:list>
				</select>
			</td>
			</tr>

			<tr class="reportsBackground" align="center">
				<td><p>
				  <sj:a 
						 button="true" 
				         onClick="FillInAgentIDAndName();"
					     title="%{getText('jsp.user_401')}"
				         ><s:text name="jsp.user_401" /></sj:a>
				</p>
				</td>
			</tr>
		</table>
		</form>
		<br><br>
		</div>
