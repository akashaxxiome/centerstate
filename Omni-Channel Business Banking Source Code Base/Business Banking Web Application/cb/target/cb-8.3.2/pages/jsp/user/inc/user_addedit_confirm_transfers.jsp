<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
 
<%
String IsObjectEntitled = null;
String ObjectLimitValue = null;
%>

                <div class="blockHead marginTop10 marginBottom10">
    <ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="equals">
                <span class="">Transfers Enabled: Yes</span> 
    </ffi:cinclude>
    <ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="notEquals">
                <span class="">Transfers Enabled: No</span> 
    </ffi:cinclude></div>
    <ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="equals">
     <div class="paneWrapper">
  	   <div class="paneInnerWrapper"> 
  	      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData tdTopBottomPadding5 tableAlerternateRowColor">
                    <tr class="header">
                        <td><div class="">Transfer From Accounts</div></td>
                        <td><div class="">Limit Per Transaction</div></td>
                        <td><div class="">Limit Per Day</div></td>
                    </tr>

    <ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
    <ffi:setProperty name="AccountsCollectionNames" property="Add" value="SecondaryUserEntitledCoreAccounts"/>
    <ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_FROM %>'/>

    <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
    <ffi:setProperty name="AccountsFromFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
    <ffi:setProperty name="AccountsFromFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.TRANSFER_FROM_FILTER %>'/>

    <ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFITransferAccessAndLimits"/>
    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementOperationName" value="${FromOperationName}"/>
    <ffi:setProperty name="HasAccount" value=""/>
    <ffi:setProperty name="NoAccountsMessage" value="(No accounts enabled for transfers.)"/>

    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
	
        <ffi:setProperty name="AccountsFromFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsFromFilters.Value}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
            <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${FromOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>

            <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:removeProperty name="IsObjectEntitled"/>
            <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="IsEntitledObject" assignTo="IsObjectEntitled"/>
            <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="equals">
                <ffi:setProperty name="HasAccount" value="true"/>
                    <tr style="padding: 0;">
                        <td><div class="txt_normal"><ffi:getProperty name="TempAccount" property="ConsumerDisplayText"/></div></td>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="equals">
                        <td><div class="txt_normal">None</div></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <td><div class="txt_normal"><ffi:getProperty name="ObjectLimitValue"/></div></td>
                </ffi:cinclude>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="equals">
                        <td><div class="txt_normal">None</div></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <td><div class="txt_normal"><ffi:getProperty name="ObjectLimitValue"/></div></td>
                </ffi:cinclude>
                    </tr>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
    <ffi:cinclude value1="${HasAccount}" value2="" operator="equals">
                    <tr>
                        <td colspan="3"><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="NoAccountsMessage"/></div></td>
                    </tr>
    </ffi:cinclude>
                </table></div></div>
		<div class="paneWrapper">
  	      <div class="paneInnerWrapper">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData tableAlerternateRowColor marginTop10 tdTopBottomPadding5">
                    <tr class="header">
                        <td><div class="">Transfer To Accounts</div></td>
                        <td><div class="">Limit Per Transaction</div></td>
                        <td><div class="">Limit Per Day</div></td>
                    </tr>

    <ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
    <ffi:setProperty name="AccountsCollectionNames" property="Add" value="SecondaryUserEntitledCoreAccounts"/>
    <ffi:setProperty name="ToOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_TO %>'/>

    <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsToFilters" scope="session"/>
    <ffi:setProperty name="AccountsToFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
    <ffi:setProperty name="AccountsToFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.TRANSFER_TO_FILTER %>'/>

    <ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFITransferAccessAndLimits"/>
    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementOperationName" value="${ToOperationName}"/>
    <ffi:setProperty name="HasAccount" value=""/>
    <ffi:setProperty name="NoAccountsMessage" value="(No accounts enabled for transfers.)"/>

    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
        <ffi:setProperty name="AccountsToFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsToFilters.Value}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
            <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${ToOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>

            <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:removeProperty name="IsObjectEntitled"/>
            <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="IsEntitledObject" assignTo="IsObjectEntitled"/>
            <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="equals">
                <ffi:setProperty name="HasAccount" value="true"/>
                    <tr style="padding: 0;">
                        <td><div class="txt_normal"><ffi:getProperty name="TempAccount" property="ConsumerDisplayText"/></div></td>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="equals">
                        <td><div class="txt_normal">None</div></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <td><div class="txt_normal"><ffi:getProperty name="ObjectLimitValue"/></div></td>
                </ffi:cinclude>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="equals">
                        <td><div class="txt_normal">None</div></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${ObjectLimitValue}" value2="" operator="notEquals">
                        <td><div class="txt_normal"><ffi:getProperty name="ObjectLimitValue"/></div></td>
                </ffi:cinclude>
                    </tr>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
    <ffi:cinclude value1="${HasAccount}" value2="" operator="equals">
                    <tr>
                        <td colspan="3"><div style="text-align: center;" class="txt_normal"><ffi:getProperty name="NoAccountsMessage"/></div></td>
                    </tr>
    </ffi:cinclude>
                </table></div></div>
    </ffi:cinclude>
<%--
<ffi:removeProperty name="FromOperationName"/>
<ffi:removeProperty name="NoAccountsMessage"/>
<ffi:removeProperty name="SessionPrefix"/>
<ffi:removeProperty name="AccountEntitlementObject"/>
<ffi:removeProperty name="FeatureAccessAndLimitsTaskName"/>
<ffi:removeProperty name="ToOperationName"/>
--%>