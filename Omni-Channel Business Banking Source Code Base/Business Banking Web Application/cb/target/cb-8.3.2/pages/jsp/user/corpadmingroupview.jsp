<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<ffi:setL10NProperty name='PageHeading' value='View Group'/>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%	
	String EditGroup_GroupName = request.getParameter("EditGroup_GroupName");
	if(EditGroup_GroupName!= null){
		session.setAttribute("EditGroup_GroupName", EditGroup_GroupName);
	}
	
	String EditGroup_GroupId = request.getParameter("EditGroup_GroupId");
	if(EditGroup_GroupId!= null){
		session.setAttribute("EditGroup_GroupId", EditGroup_GroupId);
	}
	
	String EditGroup_Supervisor = request.getParameter("EditGroup_Supervisor");
	if(EditGroup_Supervisor!= null){
		session.setAttribute("EditGroup_Supervisor", EditGroup_Supervisor);
	}
	
	String EditGroup_DivisionName = request.getParameter("EditGroup_DivisionName");
	if(EditGroup_DivisionName!= null){
		session.setAttribute("EditGroup_DivisionName", EditGroup_DivisionName);
	}
%>

<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
// The user has pressed cancel, so go back to the previous version of the collections
session.setAttribute( "NonAdminEmployees", session.getAttribute("NonAdminEmployeesCopy"));
session.setAttribute( "AdminEmployees", session.getAttribute("AdminEmployeesCopy"));
session.setAttribute( "NonAdminGroups", session.getAttribute("NonAdminGroupsCopy"));
session.setAttribute( "AdminGroups", session.getAttribute("AdminGroupsCopy"));
session.setAttribute( "adminMembers", session.getAttribute("adminMembersCopy"));
session.setAttribute( "userMembers", session.getAttribute("userMembersCopy"));
session.setAttribute( "groupIds", session.getAttribute("groupIdsCopy"));
session.setAttribute( "tempAdminEmps", session.getAttribute("tempAdminEmpsCopy") );
session.setAttribute( "GroupDivAdminEdited", session.getAttribute("GroupDivAdminEditedCopy")); 
%>
</ffi:cinclude>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="CancelReload"/>

<%-- update Entitlement_EntitlementGroups in session --%>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:object id="GetChildrenByGroupType" name="com.ffusion.efs.tasks.entitlements.GetChildrenByGroupType" scope="session"/>
    <ffi:setProperty name="GetChildrenByGroupType" property="GroupType" value="Division"/>
<ffi:setProperty name="GetChildrenByGroupType" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:process name="GetChildrenByGroupType"/>
<ffi:removeProperty name="GetChildrenByGroupType" />

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
	<ffi:cinclude value1="${ErrorOccured}" value2="TRUE" operator="notEquals">
		<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
		<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
		<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
		<ffi:process name="GetEntitlementGroup"/>
		<ffi:removeProperty name="GetEntitlementGroup" />
		
		<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
		   <ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${EditGroup_GroupName}"/>
		   <ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value="${EditGroup_Supervisor}"/>
		   <ffi:setProperty name="EditBusinessGroup" property="ParentGroupName" value="${EditGroup_DivisionName}"/>
		   <ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:removeProperty name="ErrorOccured"/>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- if division size is not 0 and this secure user is entitled to adminstrator this group
     enable the addgroup button --%>
<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadmingroupadd.jsp"><img src="/cb/cb/${ImgExt}grafx/user/i_addgroup.gif" alt="" width="67" height="16" border="0"></a>'/>
</ffi:cinclude>
</ffi:cinclude>

<%-- if division size is not 0 or this secure user is entitled to adminstrator this group
     disable the addgroup button --%>
<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
<ffi:setProperty name='PageText' value=''/>
</ffi:cinclude>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
<ffi:setProperty name='PageText' value=''/>
</ffi:cinclude>

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>


<!-- Dual Approval for profile starts-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="DAEntitlementGroupObject" name="com.ffusion.csil.beans.entitlements.EntitlementGroup" />
	
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="DAEntitlementGroupObject"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
	
	<ffi:cinclude value1="${DAEntitlementGroupObject.GroupName}" value2="" operator="notEquals">
		<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${DAEntitlementGroupObject.GroupName}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${DAEntitlementGroupObject.ParentId}" value2="0" operator="notEquals">
   	    <ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${DAEntitlementGroupObject.ParentId}"/>
    </ffi:cinclude>
	
	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="DAEntitlementGroupObject"/>
</ffi:cinclude>
<!-- Dual Approval ends -->

<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupview.jsp?ErrorOccured=TRUE" URLEncrypt="true"/>

		<div align="center">
			<%-- <table width="676" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" class="sectionhead" align="center">
 			                   	<ffi:getProperty name="Business" property="BusinessName"/><br><br>
					</td>
				</tr>
			</table> --%>
			<table width="" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="adminBackground">
						<form action='<ffi:getProperty name="SecurePath"/>user/corpadmingroupedit-verify.jsp' name="FormName" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
                    			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<input type="hidden" name="DeleteBusinessGroup.SuccessURL" value="<ffi:urlEncrypt url="${SecurePath}redirect.jsp?target=${SecurePath}user/corpadmingroups.jsp" />">
								<input type="hidden" name="SecurePath" value='<ffi:getProperty name="SecurePath"/>'>
								<input type="hidden" name="EditBusinessGroup.CheckboxesAvailable" value="false">
								<input type="hidden" name="EditBusinessGroup.SessionGroupName" value="Entitlement_EntitlementGroup"/>
								<input type="hidden" name="EditBusinessGroup.SuccessURL" value='<ffi:urlEncrypt url="${SecurePath}redirect.jsp?target=${SecurePath}user/corpadmingroups.jsp" />'>							
								<tr>
									<td colspan="3" height="10">&nbsp;</td>
								</tr>
								<tr>
									<td class="<ffi:getPendingStyle fieldname="groupName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>" align="right" width="150">
										<%-- <!--L10NStart-->Group Name<!--L10NEnd--> --%>
										<s:text name="admin.group.name" />
										:<span class="required">*</span>
									</td>
									<td class="adminBackground columndata" valign="top" width="441">
										<ffi:setProperty name="LastRequest" property="Name" value="EditBusinessGroup.GroupName"/>
										<ffi:setProperty name="LastRequest" property="TextDefaultValue" value="${EditBusinessGroup.GroupName}"/>
										<ffi:getProperty name="EditBusinessGroup" property="GroupName"/>
										<span class="sectionhead_greyDA">
											<ffi:getProperty name="oldDAObject" property="GroupName"/> 
										</span>&nbsp; (<ffi:getProperty name="Business" property="BusinessName"/>)
									</td>
								</tr>
								<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
									<input type="hidden" name="EditBusinessGroup.ParentGroupId" value="<ffi:getProperty name="Entitlement_EntitlementGroup" property="ParentId"/>"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
								<tr>
									<td class="<ffi:getPendingStyle fieldname="parentId" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>" align="right" width="150">
										<s:text name="jsp.user_122" />:<span class="required">*</span> 
									</td>
									<td class="adminBackground columndata" valign="top">
										<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
											<ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${EditBusinessGroup.ParentGroupId}" operator="equals">
														<ffi:getProperty name="EntGroupItem" property="GroupName" />
											</ffi:cinclude>
										</ffi:list>
										<span class="sectionhead_greyDA">
											<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
												<ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${oldDAObject.ParentId}" operator="equals">
															<ffi:getProperty name="EntGroupItem" property="GroupName" />
												</ffi:cinclude>
											</ffi:list>
										</span>
									</td>
								</tr>
								</ffi:cinclude>								
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								
									<ffi:removeProperty name="CATEGORY_BEAN"/>
									<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>
								
									<%-- based on the business employee object, get the business employees for the business --%>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
									<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
									<ffi:process name="GetBusinessEmployees"/>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:removeProperty name="TempBusinessEmployee"/>
									
									<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
										<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
									<ffi:process name="GetDACategoryDetails"/>
									
									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">		
										<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
										<ffi:process name="GenerateUserAndAdminLists"/>
										<ffi:removeProperty name="adminIds"/>
										<ffi:removeProperty name="userIds"/>
										<ffi:removeProperty name="GenerateUserAndAdminLists"/>
										
										<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
										<ffi:process name="GenerateListsFromDAAdministrators"/>
									</ffi:cinclude>
									
									<ffi:object id="GenerateUserAdminList" name="com.ffusion.tasks.dualapproval.GenerateUserAdminList"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminEmployees" value="AdminEmployees"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminGroups" value="AdminGroups"/>
									<ffi:setProperty name="GenerateUserAdminList" property="sessionName" value="NewAdminBusinessEmployees"/>
									<ffi:process name="GenerateUserAdminList"/>
									
									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="NewAdminBusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="NewAdminUserStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>
									
									<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="OldAdminEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="OldAdminGroups"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="tempUserName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="tempGroupName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
									<ffi:process name="GenerateUserAndAdminLists"/>
									<ffi:removeProperty name="adminIds"/>
									<ffi:removeProperty name="tempUserName"/>
									<ffi:removeProperty name="tempGroupName"/>
									<ffi:removeProperty name="userIds"/>
									<ffi:removeProperty name="GenerateUserAndAdminLists"/>
									
									<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
									<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
									  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
									<ffi:process name="GetBusinessEmployeesByEntGroups"/>
									<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
									
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="OldAdminStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>
									
									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GetDACategoryDetails"/>
									<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
									<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
									<ffi:removeProperty name="NewAdminStringList"/>
									<ffi:removeProperty name="BusinessEmployees"/>
									<ffi:removeProperty name="NewAdminBusinessEmployees"/>
								</ffi:cinclude>
								<!-- Dual Approval for administrator ends -->	
								
								<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
								<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
								  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
								<ffi:process name="GetBusinessEmployeesByEntGroups"/>
								<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
							
								<tr>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
										<td class="adminBackground sectionsubhead" align="right" valign="baseline" width="120">
												<s:text name="jsp.user_38" />:&nbsp; 
										</td>
									</ffi:cinclude>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<td class="adminBackground sectionheadDA" align="right" valign="baseline" width="120">
												<s:text name="jsp.user_38" />:&nbsp; 
										</td>
									</ffi:cinclude>
									<td class="columndata adminBackground" valign="baseline" colspan="2">
									
									<%--display name of each employee in BusinessEmployees--%>
		 							<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="AdministratorStr"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="maxLength" value="120"/>
									<ffi:process name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									
 									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
		 								<ffi:getProperty name="AdministratorStr"/>
										<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
											<!--L10NStart-->None<!--L10NEnd-->
										</ffi:cinclude>
									</ffi:cinclude>
	 								<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	 									<ffi:getProperty name="NewAdminUserStringList"/>
	 								</ffi:cinclude>
									<ffi:removeProperty name="empName"/>
									<%--
									<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminadminview.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
									<input type="image" src="/cb/cb/multilang/grafx/user/i_view.gif" alt="View" border="0" onclick="document.FormName.submit();document.FormName.action='<ffi:getProperty name="tmp_url"/>';">
									--%>
										<s:url id="GroupAdministratorsViewURL" value="%{#session.PagesPath}user/corpadminadminview.jsp" escapeAmp="false">
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
											<s:param name="UseLastRequest" value="%{'TRUE'}"/>
										</s:url>
										<sj:a  
											href="%{GroupAdministratorsViewURL}" 
											targets="viewAdminInViewDiv" 
											button="true" 
											title="View"
											onCompleteTopics="viewAdminInViewDivTopic">
											<s:text name="jsp.default_6" />
										</sj:a>
									<ffi:removeProperty name="tmp_url"/>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<span class="sectionhead_greyDA">
												<br>
												<ffi:getProperty name="OldAdminStringList"/>
										</span>	
									</ffi:cinclude>	
									</td>
								</tr>	
								<tr>
									<td colspan="3" height="10">&nbsp;</td>
								</tr>							
							</table>
						</form>
					</td>
				</tr>
			</table>
			<div>
				<table width="" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a button="true" title="Close" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

<ffi:setProperty name="onCancelGoto" value="corpadmingroupview.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmingroupview.jsp"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupview.jsp?UseLastRequest=TRUE&AdminsSelected=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupview.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
</ffi:cinclude>


<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="NewAdminUserStringList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="AdministratorStr"/>
