<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<ffi:help id="userprefs_misc"/>

<s:form id="otherPrefs" name="otherPrefs" method="post" action="EditPreferenceAction_updateOtherSettings.action" namespace="/pages/jsp/user" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" id="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<div id="PreferenceDesk" style="width:100%;">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align: top;">
		<tr>
			<td width="100%">
				<div id="sessionActivityPortID" width="99%">
					<br>
					<table border="0" cellpadding="0" cellspacing="0" align="center">
						<tr>
							<td colspan="2">
								<s:if test="%{#session.SecureUser.AppType == @com.ffusion.csil.core.common.EntitlementsDefines@ENT_GROUP_BUSINESS}">
									<span ><s:text name="jsp.user_163"/> &nbsp;</span>
								</s:if>
								<s:else>
									<span ><s:text name="jsp.user_generate_session_receipt"/> &nbsp;</span>
								</s:else>
								<input id="displaySessionSummary" type="radio" name="displaySessionSummary" value="true" <s:if test="%{displaySessionSummary == true}">checked</s:if>>
								<s:text name="jsp.default_467"/>&nbsp;
								<input id="displaySessionSummary" type="radio" name="displaySessionSummary" value="false" <s:if test="%{displaySessionSummary == false}">checked</s:if>>
								<s:text name="jsp.default_295"/>&nbsp;
							</td>
						</tr>
					</table>
				</div>
				<br>

				<div id="bankLookupPortID" width="99%">
					<br>
					<table border="0" cellpadding="0" cellspacing="0" align="center">
						<tr>
							<td colspan="2">
								<span ><s:text name="jsp.user_196"/>&nbsp;</span>
								<select id="bankLookupID" class="txtbox" name="bankLookupMatches" size="1">
									<ffi:list collection="<%=com.ffusion.struts.user.EditPreferenceAction.BANK_LOOKUP_MAX_LIST%>" items="blMax">
										<option value="<ffi:getProperty name="blMax"/>"<s:if test="%{bankLookupMatches == #attr.blMax}">selected</s:if>><ffi:getProperty name="blMax"/></option>
									</ffi:list>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<br>

				<div id="timeoutPortID" width="99%">
					<br>
					<table border="0" cellpadding="0" cellspacing="0" align="center">
						<tr>
							<TD colspan="2">
								<span ><s:text name="jsp.user_301"/>&nbsp;</span>
								<select id="timeOutID" class="txtbox" name="timeout" size="1">
									<ffi:list collection="<%=com.ffusion.struts.user.EditPreferenceAction.TIMEOUT_LIST%>" items="Item">
										<option value="<ffi:getProperty name="Item" property="Key"/>"<s:if test="%{timeout == #attr.Item.Key}">selected</s:if>><ffi:getProperty name="Item" property="Value"/></option>

									</ffi:list>
								</select><span >&nbsp; <s:text name="jsp.user_199"/></span>
						</tr>
					</table>
				</div>
				<br>
			</td>
		</tr>
	</table>
	<br>
	<br>

	<div id="otherPreferenceBtn" align="center">
		<sj:a id="resetOtherpreferenceButtonId" onclick="ns.userPreference.resetPreferences('MISC_TAB');" button="true">
			<s:text name="jsp.default_358"/>
		</sj:a>
		<sj:a id="cancelOtherpreferenceButtonId" button="true" onclick="ns.shortcut.goToFavorites('home');">
			<s:text name="jsp.default_82"/>
		</sj:a>
		<sj:a id="saveOtherPrefButton" formIds="otherPrefs" button="true" validate="false" targets="Miscellaneous" onCompleteTopics="saveOtherPreferences">
			<s:text name="jsp.default_366"/>
		</sj:a>
	</div>
</s:form>
<script>
	$('#timeoutPortID').portlet({
		generateDOM: true,
		title :js_timeout
	});
	$('#sessionActivityPortID').portlet({
		generateDOM: true,
		title :js_session_report
	});
	$('#bankLookupPortID').portlet({
		generateDOM: true,
		title :js_bank_lookup
	});

	$(function()
	{
		$("#bankLookupID").selectmenu({width:'5em'});
		$("#timeOutID").selectmenu({width:'5em'});
	});
</script>
</div>