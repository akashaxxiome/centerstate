<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.tasks.dualapproval.IDACategoryConstants" %>

<script type="text/javascript">

// Handle text area maxlength
$(document).ready(function(){
	ns.common.handleTextAreaMaxlength("#RejectReason");
});

</script>

<ffi:setProperty name="entitlementTypeOrder" property="Filter" value="EnableMenu=true" />
<ffi:object id="DAWizardUtil" name="com.ffusion.tasks.dualapproval.DAWizardUtil" />
<ffi:setProperty name="DAWizardUtil" property="currentPage" value="${currentPage}" />
<ffi:process name="DAWizardUtil" />
<ffi:removeProperty name="DAWizardUtil"/>
<div>
		<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
			<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-user-verify.jsp-2" parm0="${userName}"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${rejectFlag}" value2="y" >
			<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
				<ffi:object id="RejectPendingChange" name="com.ffusion.tasks.dualapproval.RejectPendingChange"  />
				<ffi:setProperty name="RejectPendingChange" property="validate" value="REJECTREASON" />
				<% 
					session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("RejectPendingChange")); 
				%>
			
						<s:form id="rejectDualApprovalFormID" namespace="/pages/dualapproval" validate="false" action="rejectDAChanges" method="post" name="RejectDualApprovalForm" theme="simple">
	           		       	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	           		       	<input type="hidden" name="CommonDualApprovalTask.ItemId" value="<ffi:getProperty name='itemId'/>"/>
							<input type="hidden" name="CommonDualApprovalTask.ItemType" value="<ffi:getProperty name='itemType'/>"/>
							<input type="hidden" name="module" value="User"/>
							<input type="hidden" name="daAction" value="Rejected"/>
								<div>
									<span  class="sectionsubhead adminBackground" ><!--L10NStart-->Reject Reason :<!--L10NEnd--><span class="required">*</span></span> <span  align="left"><textarea id="RejectReason" name="rejectReason" rows="3" cols="70" maxlength="120"></textarea>
									<br/><span id="rejectReasonError"></span>
								</span></div>
								<div>
									<span><span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span></span>
								</div>
						</s:form>
			</ffi:cinclude>
		</ffi:cinclude>
</div>	

		<div class="ffivisible marginTop30">&nbsp;</div>	
		<div class="btn-row ui-widget-header customDialogFooter">
				<sj:a 
					id="cancelWizardID"
					button="true" 
					onclick="ns.admin.DACancelWizard();"
				><s:text name="jsp.da_company_approval_cancel_button_text"/></sj:a>
			<ffi:cinclude value1="${previouspage}" value2="" operator="notEquals">
									<sj:a 
										id="previousPageID"
										button="true" 
										onclick="ns.admin.DAWPreviousPage();"
									><s:text name="jsp.default_57" /></sj:a>
			</ffi:cinclude>
			<ffi:cinclude value1="${nextpage}" value2="" operator="notEquals">
				<ffi:cinclude value1="${DISABLE_APPROVE_ERROR}" value2="true" operator="notEquals" >
									<sj:a
										id="nextPageID"
										button="true" 
										onclick="ns.admin.DAWNextPage();"
									><s:text name="jsp.default_111" /></sj:a>
				</ffi:cinclude>
				<ffi:cinclude value1="${DISABLE_APPROVE_ERROR}" value2="true">
					<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals">
						<sj:submit
							id="nextPageIDDisabled"
							button="true"
							disabled="true"
							value="%{getText('jsp.default_111')}" />
					</ffi:cinclude>
					<%-- If rejecting allow user to go ahead and reject even if there are errors. --%>
					<ffi:cinclude value1="${rejectFlag}" value2="y" operator="equals">
						<sj:a
							id="nextPageID"
							button="true" 
							onclick="ns.admin.DAWNextPage();">
							<s:text name="jsp.default_111" /></sj:a>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
				<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
					<s:if test="%{#session.Section == 'Users'}">
						<s:url id="approveURL" value="/pages/jsp/user/da-user-add-confirm.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="itemId" value="%{#session.itemId}"></s:param> 
						</s:url>
					</s:if>
					<ffi:cinclude value1="${Section}" value2="Profiles">
						<s:url id="approveURL" value="/pages/jsp/user/da-profile-add-confirm.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="itemId" value="%{#session.itemId}"></s:param> 
						</s:url>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="Company">
						<s:url id="approveUrl" value="/pages/jsp/user/da-business-confirm.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
						</s:url>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="Division">
						<s:url id="approveUrl" value="/pages/jsp/user/da-division-modify-confirm.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
							<s:param name="itemId" value="%{#session.itemId}"></s:param>  										 
						</s:url>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="Group">
						<s:url id="approveUrl" value="/pages/jsp/user/da-group-modify-confirm.jsp">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
								<s:param name="itemId" value="%{#session.itemId}"></s:param>  
						</s:url>
					</ffi:cinclude>
					<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
						<ffi:cinclude value1="${DISABLE_APPROVE_ERROR}" value2="true" operator="notEquals" >
							<s:if test="%{#session.Section == 'Users'}">
								<sj:a									
									id="ApproveID"
									button="true"
									targets="approvalWizardDialogID"
									href="%{approveURL}" 
									onSuccessTopics="addModifyUserVerifyCompleteTopics"
									onCompleteTopics=""
									onErrorTopics=""
								><s:text name="jsp.da_company_approval_button_text" /></sj:a>
							</s:if>
							<ffi:cinclude value1="${Section}" value2="Profiles">
								<sj:a									
									id="ApproveID"
									button="true"
									targets="approvalWizardDialogID"
									href="%{approveURL}" 
									onCompleteTopics="addModifyProfileVerifyCompleteTopics"
								><s:text name="jsp.da_company_approval_button_text" /></sj:a>
							</ffi:cinclude>
							<ffi:cinclude value1="${Section}" value2="Company">
									<sj:a id="ApproveID"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics=""
										onSuccessTopics="DACompanyApprovalReviewCompleteTopics"
										onCompleteTopics=""
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
							</ffi:cinclude>
							<ffi:cinclude value1="${Section}" value2="Division">
									<sj:a id="ApproveID"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics=""
										onSuccessTopics="DADivisionApprovalReviewCompleteTopics"
										onCompleteTopics=""
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
							</ffi:cinclude>
							<ffi:cinclude value1="${Section}" value2="Group">
									<sj:a id="ApproveID"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics=""
										onSuccessTopics="DAGroupApprovalReviewCompleteTopics"
										onCompleteTopics=""
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${DISABLE_APPROVE_ERROR}" value2="true">
							<sj:submit
								id="ApproveIDDisabled"
								button="true"
								disabled="true"
								value="%{getText('jsp.da_company_approval_button_text')}"
							/>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
				<ffi:cinclude value1="${rejectFlag}" value2="y" >
					<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<sj:a 
										id="rejectID"
										formIds="rejectDualApprovalFormID"
										targets="resultmessage"
										button="true" 
										validate="true"
										validateFunction="customValidation"
										onclick="removeValidationErrors();"
										onSuccessTopics="rejectDAChangesCompleteTopics"
									><s:text name="jsp.da_company_reject_button_text"/></sj:a>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
					<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
						<ffi:setProperty name="approveURL" value="da-user-delete-confirm.jsp?itemId=${itemId}"  />
							<sj:a 
							id="approveDeleteUser"
							button="true" 
							onclick="ns.admin.DAApprove();"
							><s:text name="jsp.da_company_approval_button_text"/></sj:a>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
				<s:if test="%{#session.Section == 'Users'}">
					<ffi:setProperty name="cancelURL" value="corpadminusers.jsp?goback=false" />
				</s:if>
				<ffi:cinclude value1="${Section}" value2="Profiles">
					<ffi:setProperty name="cancelURL" value="corpadminprofiles.jsp?goback=false" />
				</ffi:cinclude>			
				<ffi:cinclude value1="${Section}" value2="Company">
					<ffi:setProperty name="cancelURL" value="corpadmininfo.jsp?goback=false" />
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Division">
					<ffi:setProperty name="cancelURL" value="corpadmindiv.jsp?goback=false" />
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Group">
					<ffi:setProperty name="cancelURL" value="corpadmingroups.jsp?goback=false" />
				</ffi:cinclude>
			</div>
<ffi:flush/>

<s:if test="%{#session.Section == 'Users'}">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-user-verify.jsp"/>
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-profile-verify.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Company">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-business-verify.jsp"/>
</ffi:cinclude>

<script>
ns.admin.DAWNextPage = function(){
	$.ajax({
		url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${nextpage.MenuUrl}&ParentMenu=${nextpage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${nextpage.PageKey}&SortKey=${nextpage.NextSortKey}"/>',
		success: function(data)
		{
			$('#approvalWizardDialogID').html(data);
			ns.admin.checkResizeApprovalDialog();
		}
	});
}

ns.admin.DAWPreviousPage = function(){
	$.ajax({
		url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${previouspage.MenuUrl}&ParentMenu=${previouspage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${previouspage.PageKey}&SortKey=${previouspage.NextSortKey}"/>',
		success: function(data)
		{
			$('#approvalWizardDialogID').html(data);
			ns.admin.checkResizeApprovalDialog();
		}
	});
}

ns.admin.DACancelWizard = function(){
	$('#approvalWizardDialogID').dialog('close');
}

ns.admin.DAApprove = function(){
	$.ajax({
		url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${approveURL}"/>',
		success: function(data)
		{
			ns.admin.reloadPendingAfterApprove();
			ns.common.closeDialog("#approvalWizardDialogID");
		}
	});

}
</script>
<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="SubmittedUserName"/>
<ffi:removeProperty name="SetBusinessEmployee"/>
<ffi:removeProperty name="CurrencyObject"/>
<ffi:removeProperty name="displayOperationName"/>
<ffi:removeProperty name="GetCategories"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="GetEntitlmentDisplayName"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="cancelURL"/>
<ffi:removeProperty name="approveURL"/>
<ffi:removeProperty name="currentPage"/>
<ffi:removeProperty name="DISABLE_APPROVE_ERROR"/>
