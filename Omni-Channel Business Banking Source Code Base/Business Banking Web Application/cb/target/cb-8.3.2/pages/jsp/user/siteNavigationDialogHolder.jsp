<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:url id="pageurl" value="/pages/jsp/user/siteNavigationDialog.jsp" escapeAmp="false">

</s:url>
<sj:dialog
	id="siteNavigationDialogId"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.default_Site_Navigation')}"
	height="450"
	width="720"
	href="%{pageurl}"
	showEffect="fold"
	hideEffect="clip"
	resizable="false"
    cssStyle="overflow:hidden">
</sj:dialog>
