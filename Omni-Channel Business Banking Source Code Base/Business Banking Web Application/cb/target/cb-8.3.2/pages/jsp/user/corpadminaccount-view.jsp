<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setL10NProperty name='PageHeading' value='View Account Configuration'/>
<ffi:setProperty name='PageText' value=''/>

<ffi:setProperty name="disableEdit" value=""/>
<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>

<% 
	session.setAttribute("aid", request.getParameter("aid")); 
	session.setAttribute("did", request.getParameter("did")); 
	session.setAttribute("rnum", request.getParameter("rnum")); 
%>

<%
    String accountID = null;
    String routingNum = null;
    String accountState = null;
    String accountCountry = null;
%>

<ffi:getProperty name="aid" assignTo="accountID"/>
<ffi:getProperty name="rnum" assignTo="routingNum"/>
<ffi:removeProperty name="aid"/>
<ffi:removeProperty name="rnum"/>


<ffi:cinclude value1="<%= accountID %>" value2="" operator="notEquals">
    <ffi:removeProperty name="ModifyAccount"/>
    <ffi:removeProperty name="AccountContact"/>
    <ffi:removeProperty name="oldNickName"/>
	<ffi:removeProperty name="oldDAObject"/>
	<ffi:removeProperty name="oldState"/>
	<ffi:removeProperty name="oldCountry"/>

    <ffi:object name="com.ffusion.tasks.accounts.ModifyAccountById" id="ModifyAccount" scope="session" />
    <ffi:setProperty name="ModifyAccount" property="Init" value="true"/>
    <ffi:setProperty name="ModifyAccount" property="ID" value="<%= accountID %>"/>
    <ffi:setProperty name="ModifyAccount" property="RoutingNum" value="<%= routingNum%>"/>
    <ffi:setProperty name="ModifyAccount" property="AccountCollection" value="FilteredAccounts"/>
    <ffi:setProperty name="ModifyAccount" property="DirectoryId" value="${Business.Id}"/>
    
   
	<%-- validation performed in corpadminaccountedit.jsp --%>
    <ffi:process name="ModifyAccount"/>
	<ffi:object name="com.ffusion.tasks.util.GetCountryList" id="GetCountryList" scope="session"/>
	<ffi:setProperty name="GetCountryList" property="CollectionSessionName" value="Country_List"/>
	<ffi:process name="GetCountryList"/>
	<ffi:removeProperty name="GetCountryList"/> 
	 <%
	 	
        
    	com.ffusion.beans.accounts.Account account = (com.ffusion.beans.accounts.Account)session.getAttribute( "ModifyAccount" );
        com.ffusion.beans.Contact contact = account.getContact();
        
        if( contact == null ) {
            contact = new com.ffusion.beans.Contact();
			session.setAttribute( "AccountContact", contact );
			%>
			<ffi:setProperty name="AccountContact" property="PreferredLanguage" value="${Business.PreferredLanguage}" />
			<%
        } else {
			session.setAttribute( "AccountContact", contact );
        }
    %>
    
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="Business"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ACCOUNT_CONFIGURATION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="objectId" value="${ModifyAccount.ID}-${ModifyAccount.RoutingNum}"/>
	<ffi:process name="GetDACategoryDetails"/>

	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="ModifyAccount"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
	<ffi:setProperty name="oldNickName" value="${oldDAObject.NickName}"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="AccountContact"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
	
</ffi:cinclude> 

</ffi:cinclude>


<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
	<ffi:setProperty name="RegAvailableFlag" value="true"/>
	<ffi:cinclude value1="${ModifyAccount.TypeValue}" value2="1" operator="notEquals">
		<ffi:cinclude value1="${ModifyAccount.TypeValue}" value2="2" operator="notEquals">
			<ffi:cinclude value1="${ModifyAccount.TypeValue}" value2="12" operator="notEquals">
				<ffi:setProperty name="RegAvailableFlag" value="false"/>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminaccounts-view.jsp" URLEncrypt="true"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%--
<ffi:include page="${PathExt}common/zipcode_js.jsp"/>
--%>
<script type="text/javascript"><!--
function validateData( formName )
{
    var form = document.forms[formName];
<ffi:cinclude value1="${RegAvailableFlag}" value2="true">
    if (document.frmEditAccount.registerEnabled.checked) {
        document.frmEditAccount["ModifyAccount.REG_ENABLED"].value = "true";
    } else {
        document.frmEditAccount["ModifyAccount.REG_ENABLED"].value = "false";
    }
</ffi:cinclude>
    
    form.submit();
}

//--></script>
<form action="" method="post" name="frmEditAccount">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="ModifyAccount.REG_ENABLED" value="false">
<div class="blockWrapper">
	<div  class="blockHead"></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionhead sectionLabel"><s:text name="jsp.user_52"/></span>
                <span class="sectionhead"><ffi:getProperty name="ModifyAccount" property="BankName" /></span>
                                       
			</div>
			<div class="inlineBlock">
				 <span class="sectionhead sectionLabel"><s:text name="jsp.user_98"/></span>
                 <span class="sectionhead"><ffi:getProperty name="ModifyAccount" property="CurrencyCode" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionhead sectionLabel"><s:text name="jsp.user_10"/></span>
                <span class="sectionhead"><ffi:getProperty name="ModifyAccount" property="DisplayText" /></span>
                                       
			</div>
			<div class="inlineBlock">
				 <span class="sectionhead sectionLabel"><s:text name="jsp.user_11"/></span>
                 <span class="sectionhead"><ffi:getProperty name="ModifyAccount" property="Type" /></span>
			</div>
		</div>
		
		<div class="blockRow" >
			<div class="inlineBlock" style="width: 50%">
				<span class='<ffi:getPendingStyle fieldname="nickName" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_204"/></span>
                 <span   class="columndata">
                     <ffi:getProperty name="ModifyAccount" property="NickName" />
                 	<span class="sectionhead_greyDA"><ffi:getProperty name="oldNickName" /></span>
                 </span>
                <ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
						<ffi:cinclude value1="${RegAvailableFlag}" value2="true">
                               <% String regEnabled = ""; %>
                               <ffi:getProperty name="ModifyAccount" property="REG_ENABLED" assignTo="regEnabled" />
                               <% if( regEnabled == null ) { regEnabled = ""; } %>
                               <span  class="sectionhead" <ffi:getProperty name="disableEdit"/>><input type="checkbox" name="registerEnabled" value="checkboxValue" border="0" <ffi:getProperty name="disableEdit"/> <%= ( regEnabled.equals("T") || regEnabled.equals("true") )  ? "checked" : "" %>><s:text name="jsp.user_275"/></span>
						</ffi:cinclude>
						<ffi:cinclude value1="${RegAvailableFlag}" value2="true" operator="notEquals">
						</ffi:cinclude>
				</ffi:cinclude>

                <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
      			</ffi:cinclude>
			</div>
			<div class="inlineBlock">
				<span class='<ffi:getPendingStyle fieldname="street" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.default_35"/>:<span class="required">*</span></span>
                <span   class="columndata">
                    <ffi:getProperty name="AccountContact" property="Street" />
                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street" /></span>
                </span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class='<ffi:getPendingStyle fieldname="street2" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_30"/></span>
                 <span   class="columndata">
                     <ffi:getProperty name="AccountContact" property="Street2" />
                     <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street2" /></span>
                 </span>
			</div>
			<div class="inlineBlock">
				<span class='<ffi:getPendingStyle fieldname="city" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_65"/><span class="required">*</span></span>
	             <span   class="columndata">
	                 <ffi:getProperty name="AccountContact" property="City" />'
	                 <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="City" /></span>
	             </span>
			</div>
		</div>
		<ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
		<!-- default selected country and state -->
        <ffi:getProperty name="AccountContact" property="Country" assignTo="accountCountry"/>
        <% if (accountCountry == null) { accountCountry = ""; } %>
        <ffi:getProperty name="AccountContact" property="State" assignTo="accountState"/>
        <% if (accountState == null) { accountState = ""; } %>

        <ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
            <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value="<%= accountCountry %>" />
        <ffi:process name="GetStatesForCountry" />
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
                           <span class='<ffi:getPendingStyle fieldname="state" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_299"/></span>
                           <span class="columndata">
                                   <ffi:list collection="GetStatesForCountry.StateList" items="item">
                                       <ffi:cinclude value1="<%= accountState %>" value2="${item.StateKey}" operator="equals"><ffi:getProperty name='item' property='Name'/></ffi:cinclude>
                                    </ffi:list>
                               <ffi:cinclude value1="${oldDAObject.state}" value2="" operator="notEquals">
                                   <ffi:list collection="GetStatesForCountry.StateList" items="item">
                                       <ffi:cinclude value1="${oldDAObject.state}" value2="${item.StateKey}" operator="equals">
                                           <ffi:setProperty name="oldState" value="${item.Name}"/>
                                       </ffi:cinclude>
                                   </ffi:list>
                               </ffi:cinclude>

                          <span class="sectionhead_greyDA"><ffi:getProperty name="oldState"/></span>
                           </span>
               </ffi:cinclude>
               <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
                   <ffi:setProperty name="AccountContact" property="State" value=""/>
               </ffi:cinclude>
			</div>
			<div class="inlineBlock">
				<span class='<ffi:getPendingStyle fieldname="zipCode" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_391"/><span class="required">*</span></span>
                <span   class="columndata">
                    <ffi:getProperty name="AccountContact" property="ZipCode" />
                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="ZipCode" /></span>
                </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class='<ffi:getPendingStyle fieldname="country" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_97"/></span>
                <span class="columndata">
	                    <ffi:list collection="Country_List" items="item">
	                        <ffi:cinclude value1="<%= accountCountry %>" value2="${item.CountryCode}" operator="equals"><ffi:getProperty name='item' property='Name'/></ffi:cinclude>
	                    </ffi:list>
	                </select>
	                <ffi:cinclude value1="${oldDAObject.Country}" value2="" operator="notEquals">
	                    <ffi:list collection="Country_List" items="item">
	                        <ffi:cinclude value1="${oldDAObject.Country}" value2="${item.CountryCode}" operator="equals">
	                            <ffi:setProperty name="oldCountry" value="${item.Name}"/>
	                        </ffi:cinclude>
	                    </ffi:list>
	                </ffi:cinclude>
	
	                <span class="sectionhead_greyDA"><ffi:getProperty name="oldCountry"/></span>
                </span>
			</div>
			<div class="inlineBlock">
				<span class='<ffi:getPendingStyle fieldname="phone" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_Phone"/></span>
                <span   class="columndata">
                    <ffi:getProperty name="AccountContact" property="Phone" />
                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Phone" /></span>
                </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class='<ffi:getPendingStyle fieldname="faxPhone" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_157"/></span>
                <span   class="columndata">
                    <ffi:getProperty name="AccountContact" property="FaxPhone" />
                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="FaxPhone" /></span>
                </span>
			</div>
			<div class="inlineBlock">
				 <span class='<ffi:getPendingStyle fieldname="email" dacss="sectionheadDA" defaultcss="sectionhead" /> sectionLabel'><s:text name="jsp.user_148"/></span>
                 <span   class="columndata">
                     <ffi:getProperty name="AccountContact" property="Email" />
                     <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Email" /></span>
                 </span>
			</div>
		</div>
		</ffi:cinclude>
	</div>
</div>
<div align="center" class="marginTop10"><span class="required">* <s:text name="jsp.default_240"/></span></div>
<div align="center" class="marginTop30">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a button="true" title="Close" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a>
</div>
</form>
<ffi:removeProperty name="GetStatesForCountry"/>
<ffi:removeProperty name="oldNickName"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="oldState"/>
<ffi:removeProperty name="oldCountry"/>