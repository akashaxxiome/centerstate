<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="PageHeading" value="Security Check"/>
<ffi:setProperty name="PageText" value="" />

<SCRIPT LANGUAGE="JavaScript">
function setFocus()
{
	frm = document.frmCredentialList;

	for( i=0; i < frm.length; i++ ) {
		curr_type=frm.elements[i].type;
		if (curr_type.indexOf("password") >= 0) {
			frm.elements[i].focus();
			break;
		}
	}
}
setFocus();
</SCRIPT>


<ffi:setProperty name="subMenuSelected" value="${securitySubMenu}"/>

<%-- Create a Resource tasks to access the authentication resource bundle for names of credentials --%>
<%-- Currently supporting scratch card, token and challenge questions --%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResource" scope="session"/>
    <ffi:setProperty name="UserResource" property="ResourceFilename" value="com.ffusion.beansresources.authentication.resources" />
<ffi:process name="UserResource"/>
<% String type_token = "" + com.ffusion.beans.authentication.AuthConsts.AUTH_TYPE_TOKEN;
   String type_scratch = "" + com.ffusion.beans.authentication.AuthConsts.AUTH_TYPE_SCRATCH;
   String type_challenge = "" + com.ffusion.beans.authentication.AuthConsts.AUTH_TYPE_CHALLENGE;
   String credential_scratch= "CREDENTIAL_FIELD_NAME_" + type_scratch;
   String credential_token = "CREDENTIAL_FIELD_NAME_" + type_token;
   
%>   

		<div align="center">

			<%-- include page header --%>
			<ffi:include page="${PathExt}inc/nav_header.jsp" />
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="/cb/web/multilang/grafx/payments/corner_al.gif" alt="" height="7" width="11" border="0"></td>
					<td class="ltrow6"><img src="/cb/web/multilang/grafx/payments/spacer.gif" height="7" width="728" border="0"></td>
					<td align="right" class="ltrow1_color"><img src="/cb/web/multilang/grafx/payments/corner_ar.gif" alt="" height="7" width="11" border="0"></td>
				</tr>
				<tr>
					<td></td>
					<td class="ltrow6">
						<div align="center">

<form method="post" name="frmCredentialList" action="<ffi:urlEncrypt url="${SecurePath}security-check-confirm.jsp"/>">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%">
	<tr>
		<td align="center" class="home_instructions">
		<ffi:getL10NString rsrcFile="cb" msgKey="${securityMessageKey}"/><br>&nbsp;
		</td>
	</tr>
</table>

<%--- List the Credentials for validation ----%>
<% int curNum = 0; %>
<table width="100%">
	<ffi:list collection="Credentials" items="item">
	<tr>
		<td width="35%">&nbsp;</td>
		<ffi:cinclude value1="${item.Type}" value2="<%=type_token%>" operator="equals">
			<ffi:setProperty name="UserResource" property="ResourceID" value="<%=credential_token%>" />
			<td width="65%" class="sectionhead" align="left">
				<ffi:getProperty name="UserResource" property="Resource"/><span class="required">*</span><br>
				<input type="password" class="txtbox" value="" name="CredentialResponse_<%=curNum%>" size="40" maxlength="40" AUTOCOMPLETE="off" /><br>&nbsp;
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${item.Type}" value2="<%=type_scratch%>" operator="equals">
			<ffi:setProperty name="UserResource" property="ResourceID" value="<%=credential_scratch%>" />
			<td width="65%" class="sectionhead" align="left">
				<ffi:getProperty name="UserResource" property="Resource"/><span class="required">*</span><br>
				<span class="txt_normal_bold"><ffi:getProperty name="item" property="LocalizedRequest"/></span><br>
				<input type="password" class="txtbox" value="" name="CredentialResponse_<%=curNum%>" size="40" maxlength="40" AUTOCOMPLETE="off" /><br>&nbsp;
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${item.Type}" value2="<%=type_challenge%>" operator="equals">
			<td width="65%" class="sectionhead" align="left">
			<s:text name="jsp.default_90"/><span class="required">*</span><br>
				<span class="txt_normal_bold"><ffi:getProperty name="item" property="LocalizedRequest"/></span><br>				
				<input type="password" class="txtbox" value="" name="CredentialResponse_<%=curNum%>" size="40" maxlength="40" /><br>&nbsp;
			</td>
		</ffi:cinclude>
	</tr>
	<% curNum ++; %>
	</ffi:list>
</table>
<table width="100%">
    <tr>
        <td width="100%" class="sectionhead" align="center">
            <span class="required">* <s:text name="jsp.default_240"/></span>
        </td>
    </tr>
</table>

<%-------------------------------------------------------------%>
<table width="100%">
	<tr> 
		<td class="ltrow6" width="100%">&nbsp;</td>
	</tr>
</table>
<center>
<input type="button" name="BACK" value="<s:text name="jsp.default_57"/>" class="submitbutton" onClick="window.location='<ffi:urlEncrypt url="${BackURL}"/>';">
<input type="Submit" name="Continue" value="<s:text name="jsp.default_111"/>" class="submitbutton">
</center>
</form>

						</div>
					</td>
					<td</td>
				</tr>
				<tr>
					<td><img src="/cb/web/multilang/grafx/payments/corner_bl.gif" alt="" height="7" width="11" border="0"></td>
					<td class="ltrow6"><img src="/cb/web/multilang/grafx/payments/spacer.gif" height="7" width="728" border="0"></td>
					<td><img src="/cb/web/multilang/grafx/payments/corner_br.gif" alt="" height="7" width="11" border="0"></td>
				</tr>
			</table>
</div>

<ffi:removeProperty name="UserResource"/>
