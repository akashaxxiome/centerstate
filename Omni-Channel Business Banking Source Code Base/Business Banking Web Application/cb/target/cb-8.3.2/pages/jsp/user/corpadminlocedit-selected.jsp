<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String locationBPWID = request.getParameter("EditLocation_LocationBPWID");
%>
<ffi:setProperty name="EditLocation.LocationBPWID" value="<%=locationBPWID%>"/>

<%-- Get the affiliate bank list --%>
<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
<ffi:process name="GetAffiliateBanks"/>
<ffi:removeProperty name="GetAffiliateBanks"/>
<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />

<%-- Get cashcon company list --%>
<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${EditDivision_GroupId}"/>
<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
<ffi:process name="GetCashConCompanies"/>
<ffi:removeProperty name="GetCashConCompanies"/>

<%-- Get the location to be edited --%>
<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation" scope="session" />
<ffi:setProperty name="SetLocation" property="BPWID" value="<%=locationBPWID%>"/>
<ffi:process name="SetLocation"/>

<%-- Fill in the location values --%>
<ffi:object id="EditLocation" name="com.ffusion.tasks.cashcon.EditLocation" scope="session" />
<ffi:setProperty name="EditLocation" property="SessionKey" value="Location"/>
<ffi:setProperty name="EditLocation" property="DivisionID" value="${Location.DivisionID}"/>
<ffi:setProperty name="EditLocation" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="EditLocation" property="LocationBPWID" value="${Location.LocationBPWID}"/>
<ffi:setProperty name="EditLocation" property="LocationName" value="${Location.LocationName}"/>
<ffi:setProperty name="EditLocation" property="LocationID" value="${Location.LocationID}"/>
<ffi:setProperty name="EditLocation" property="LogId" value="${Location.LogId}"/>
<ffi:setProperty name="EditLocation" property="SubmittedBy" value="${Location.SubmittedBy}"/>
<ffi:setProperty name="EditLocation" property="Active" value="${Location.Active}"/>
<%-- See if the routing number is in the affiliate bank list --%>
<% boolean customLocalBank = false;
   boolean customLocalAccount = false; %>
<ffi:list collection="AffiliateBanks" items="Bank">
	<ffi:cinclude value1="${Bank.AffiliateRoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
		<% customLocalBank = true; %>
		<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
	</ffi:cinclude>
</ffi:list>
<% if (!customLocalBank) { %>
	<ffi:setProperty name="EditLocation" property="LocalRoutingNumber" value="${Location.LocalRoutingNumber}"/>
	<ffi:setProperty name="EditLocation" property="LocalBankName" value="${Location.LocalBankName}"/>
<% } %>
<ffi:setProperty name="EditLocation" property="CustomLocalBank" value="<%= String.valueOf(customLocalBank)%>"/>

<%-- See if the account is in the accounts list --%>
<ffi:list collection="BankingAccounts" items="Account">
    <ffi:cinclude value1="${Account.RoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
        <ffi:cinclude value1="${Account.Number}" value2="${Location.LocalAccountNumber}" operator="equals">
            <ffi:cinclude value1="${Account.TypeValue}" value2="${Location.LocalAccountType}" operator="equals">
                <% boolean rightType = false; %>
                <ffi:cinclude value1="${Account.TypeValue}"
                    value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING ) %>"
                    operator="equals">
                    <% rightType = true; %>
                </ffi:cinclude>
                <ffi:cinclude value1="${Account.TypeValue}"
                    value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS ) %>"
                    operator="equals">
                    <% rightType = true; %>
                </ffi:cinclude>
                <% if (rightType) {
                     customLocalAccount = true; %>
                     <ffi:setProperty name="EditLocation" property="LocalAccountID" value="${Account.ID}"/>
                <% } %>
            </ffi:cinclude>
        </ffi:cinclude>
    </ffi:cinclude>
</ffi:list>
<% if (!customLocalAccount) { %>
	<ffi:setProperty name="EditLocation" property="LocalAccountNumber" value="${Location.LocalAccountNumber}"/>
	<ffi:setProperty name="EditLocation" property="LocalAccountType" value="${Location.LocalAccountType}"/>
<% } %>
<ffi:setProperty name="EditLocation" property="CustomLocalAccount" value="<%= String.valueOf(customLocalAccount)%>"/>

<ffi:setProperty name="EditLocation" property="CashConCompanyBPWID" value="${Location.CashConCompanyBPWID}"/>
<ffi:setProperty name="EditLocation" property="DisbAccountBPWID" value="${Location.DisbAccountBPWID}"/>
<ffi:setProperty name="EditLocation" property="ConcAccountBPWID" value="${Location.ConcAccountBPWID}"/>
<ffi:cinclude value1="${Location.DepositMinimum}" value2="" operator="notEquals">
	<ffi:setProperty name="EditLocation" property="DepositMinimum" value="${Location.DepositMinimum}"/>
</ffi:cinclude>
<ffi:cinclude value1="${Location.DepositMaximum}" value2="" operator="notEquals">
	<ffi:setProperty name="EditLocation" property="DepositMaximum" value="${Location.DepositMaximum}"/>
</ffi:cinclude>
<ffi:cinclude value1="${Location.AnticDeposit}" value2="" operator="notEquals">
	<ffi:setProperty name="EditLocation" property="AnticDeposit" value="${Location.AnticDeposit}"/>
</ffi:cinclude>
<ffi:cinclude value1="${Location.ThreshDeposit}" value2="" operator="notEquals">
	<ffi:setProperty name="EditLocation" property="ThreshDeposit" value="${Location.ThreshDeposit}"/>
</ffi:cinclude>
<ffi:setProperty name="EditLocation" property="ConsolidateDeposits" value="${Location.ConsolidateDeposits}"/>
<ffi:setProperty name="EditLocation" property="DepositPrenote" value="${Location.DepositPrenote}"/>
<ffi:setProperty name="EditLocation" property="DisbursementPrenote" value="${Location.DisbursementPrenote}"/>
<ffi:setProperty name="EditLocation" property="DisPrenoteStatus" value="${Location.DisPrenoteStatus}" />
<ffi:setProperty name="EditLocation" property="DepPrenoteStatus" value="${Location.DepPrenoteStatus}" />

