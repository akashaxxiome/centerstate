<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<% boolean showCASHCON = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>" >
	<% showCASHCON = true; %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>" >
	<% showCASHCON = true; %>
</ffi:cinclude>
<ffi:setProperty name="displayCashConcentation" value="false"/>
<% if (showCASHCON) { %>
	<ffi:setProperty name="displayCashConcentation" value="true"/>
<% } %>

<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>

<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
<ffi:setProperty name ="GetLocations" property="DivisionID" value="${EditDivision_GroupId}"/>
<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
<ffi:process name="GetLocations"/>

<ffi:setProperty name="EditDivision_ShowLocSearch" value="TRUE"/>
<ffi:cinclude value1="${GetLocations.LocationName},${GetLocations.LocationID}" value2="," operator="equals">
	<ffi:cinclude value1="${DisplayCount}" value2="${Locations.size}" operator="notEquals">
		<ffi:setProperty name="EditDivision_ShowLocSearch" value="FALSE"/>
	</ffi:cinclude>
</ffi:cinclude>
