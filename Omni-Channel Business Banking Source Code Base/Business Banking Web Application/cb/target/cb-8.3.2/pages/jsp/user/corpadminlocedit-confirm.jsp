<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:help id="user_corpadminlocedit-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_90')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

	<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request" />
	<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${EditLocation.CurrencyCode}"/>

	<%-- Get the affiliate bank list --%>

	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
	<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />

	<ffi:setProperty name="EditLocation" property="ProcessFlag" value="false" />

	<%-- <ffi:process name="EditLocation" /> --%>

	<%-- Get the location to be edited --%>

	<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation" scope="session" />
	<ffi:setProperty name="SetLocation" property="BPWID" value="${EditLocation.LocationBPWID}"/>
	<ffi:process name="SetLocation"/>

	<ffi:object id="OldEditLocation" name="com.ffusion.tasks.cashcon.EditLocation" scope="session"/>
	<ffi:setProperty name="OldEditLocation" property="locationName" value="${Location.LocationName}" />
	<ffi:setProperty name="OldEditLocation" property="locationID" value="${Location.LocationID}" />
	<ffi:setProperty name="OldEditLocation" property="active" value="${Location.Active}" />
	<ffi:setProperty name="OldEditLocation" property="depositMinimum" value="${Location.DepositMinimum.AmountValue}" />
	<ffi:setProperty name="OldEditLocation" property="customLocalBank" value="false" />

	<ffi:list collection="AffiliateBanks" items="Bank">
		<ffi:cinclude value1="${Bank.AffiliateRoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
			<ffi:setProperty name="OldEditLocation" property="customLocalBank" value="true" />
		</ffi:cinclude>
	</ffi:list>
	<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true" operator="notEquals">
		<ffi:setProperty name="OldEditLocation" property="localRoutingNumber" value="${Location.LocalRoutingNumber}" />
		<ffi:setProperty name="OldEditLocation" property="localBankName" value="${Location.LocalBankName}" />
		<ffi:setProperty name="OldEditLocation" property="LocalBankID" value="${EditLocation.LocalBankID}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="true">
		<ffi:setProperty name="OldEditLocation" property="localRoutingNumber" value="${EditLocation.LocalRoutingNumber}" />
		<ffi:setProperty name="OldEditLocation" property="localBankName" value="${EditLocation.LocalBankName}" />
		<ffi:list collection="AffiliateBanks" items="Bank">
			<ffi:cinclude value1="${Bank.AffiliateRoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
				<ffi:setProperty name="OldEditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${OldEditLocation.LocalBankID}" value2="0" operator="equals">
			<% boolean tempStart = true; %>
			<ffi:list collection="AffiliateBanks" items="Bank">
				<% if (tempStart) {
					tempStart = false; %>
					<ffi:setProperty name="OldEditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
				<% } %>
			</ffi:list>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:setProperty name="OldEditLocation" property="depositMaximum" value="${Location.DepositMaximum.AmountValue}" />
	<ffi:setProperty name="OldEditLocation" property="anticDeposit" value="${Location.AnticDeposit.AmountValue}" />
	<ffi:setProperty name="OldEditLocation" property="threshDeposit" value="${Location.ThreshDeposit.AmountValue}" />
	<ffi:setProperty name="OldEditLocation" property="consolidateDeposits" value="${Location.ConsolidateDeposits}" />
	<ffi:setProperty name="OldEditLocation" property="depositPrenote" value="${Location.DepositPrenote}" />
	<ffi:setProperty name="OldEditLocation" property="depositSameDayPrenote" value="${Location.DepositSameDayPrenote}" />
	<ffi:setProperty name="OldEditLocation" property="customLocalAccount" value="false" />

	<ffi:list collection="BankingAccounts" items="Account">
		<ffi:cinclude value1="${Account.Number}" value2="${Location.LocalAccountNumber}" operator="equals">
			<ffi:setProperty name="OldEditLocation" property="customLocalAccount" value="true" />
		</ffi:cinclude>
	</ffi:list>

	<ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true" operator="notEquals">
		<ffi:setProperty name="OldEditLocation" property="localAccountNumber" value="${Location.LocalAccountNumber}" />
		<ffi:setProperty name="OldEditLocation" property="localAccountType" value="${Location.LocalAccountType}" />
		<ffi:setProperty name="OldEditLocation" property="localAccountID" value="${EditLocation.LocalAccountID}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.CustomLocalAccount}" value2="true">
		<ffi:setProperty name="OldEditLocation" property="localAccountNumber" value="${EditLocation.LocalAccountNumber}" />
		<ffi:setProperty name="OldEditLocation" property="localAccountType" value="${EditLocation.LocalAccountType}" />
		<ffi:list collection="BankingAccounts" items="Account">
			<ffi:cinclude value1="${Account.Number}" value2="${Location.LocalAccountNumber}" operator="equals">
				<ffi:setProperty name="OldEditLocation" property="LocalAccountID" value="${Account.ID}"/>
			</ffi:cinclude>
		</ffi:list>
	</ffi:cinclude>
	<ffi:setProperty name="OldEditLocation" property="disbursementPrenote" value="${Location.DisbursementPrenote}" />
	<ffi:setProperty name="OldEditLocation" property="disbursementSameDayPrenote" value="${Location.DisbursementSameDayPrenote}" />
	<ffi:setProperty name="OldEditLocation" property="cashConCompanyBPWID" value="${Location.CashConCompanyBPWID}" />
	<ffi:setProperty name="OldEditLocation" property="concAccountBPWID" value="${Location.ConcAccountBPWID}" />
	<ffi:setProperty name="OldEditLocation" property="disbAccountBPWID" value="${Location.DisbAccountBPWID}" />

	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetDAItem" />

	<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
		<ffi:setProperty name="fromPendingTable" value="true"/>
	</ffi:cinclude>

	<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMinimum}"/>
	<ffi:setProperty name="EditLocation" property="depositMinimum" value="${CurrencyObject.AmountValue}" />
	<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.DepositMaximum}"/>
	<ffi:setProperty name="EditLocation" property="depositMaximum" value="${CurrencyObject.AmountValue}" />
	<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.AnticDeposit}"/>
	<ffi:setProperty name="EditLocation" property="anticDeposit" value="${CurrencyObject.AmountValue}" />
	<ffi:setProperty name="CurrencyObject" property="Amount" value="${EditLocation.ThreshDeposit}"/>
	<ffi:setProperty name="EditLocation" property="threshDeposit" value="${CurrencyObject.AmountValue}" />
	<ffi:cinclude value1="${EditLocation.DepositMaximum}" value2="0" operator="equals">
		<ffi:setProperty name="EditLocation" property="depositMaximum" value="" />
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.AnticDeposit}" value2="0" operator="equals">
		<ffi:setProperty name="EditLocation" property="anticDeposit" value="" />
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.ThreshDeposit}" value2="0" operator="equals">
		<ffi:setProperty name="EditLocation" property="threshDeposit" value="" />
	</ffi:cinclude>
	<ffi:cinclude value1="${EditLocation.DepositMinimum}" value2="0" operator="equals">
		<ffi:setProperty name="EditLocation" property="depositMinimum" value="" />
	</ffi:cinclude>

	<ffi:object id="AddLocationToDA" name="com.ffusion.tasks.dualapproval.AddLocationToDA" scope="session"/>
	<ffi:setProperty name="AddLocationToDA" property="CategoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>
	<ffi:setProperty name="AddLocationToDA" property="UserAction" value="<%= String.valueOf( com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.USER_ACTION_MODIFIED ) %>"/>
	<ffi:setProperty name="AddLocationToDA" property="ItemId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="AddLocationToDA" property="OldLocationSessionName" value="OldEditLocation"/>
	<ffi:setProperty name="AddLocationToDA" property="newLocationSessionName" value="EditLocation"/>
	<ffi:setProperty name="AddLocationToDA" property="childSequence" value="${childSequence}"/>
	<ffi:setProperty name="AddLocationToDA" property="objectId" value="${EditLocation.LocationBPWID}"/>
	<ffi:setProperty name="AddLocationToDA" property="objectType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>


	<%-- sendForApproval property will decide message to be shown on this page --%>

	<ffi:setProperty name="sendForApproval" value="true"/>
	<ffi:process name="AddLocationToDA"/>
	
	<input value="true" id="isDA" type="hidden"/>
	<ffi:removeProperty name="AddLocationToDA"/>
	<ffi:removeProperty name="GetDAItem"/>
	<ffi:removeProperty name="SetLocation"/>
	<ffi:removeProperty name="GetCashConCompanies"/>
	<ffi:removeProperty name="OldEditLocation"/>
	<ffi:removeProperty name="DAItem"/>
	<ffi:removeProperty name="childSequence"/>
	<ffi:removeProperty name="AffiliateBanks"/>

</ffi:cinclude>


		<div align="center">

		<TABLE class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center">
								<ffi:cinclude value1="${sendForApproval}" value2="true" operator="notEquals">
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminglocedit-confirm.jsp-1" parm0="${Location.LocationName}"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${sendForApproval}" value2="true">
									<ffi:cinclude value1="${fromPendingTable}" value2="true">
										<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocedit-confirm.jsp-3" parm0="${Location.LocationName}"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${fromPendingTable}" value2="true" operator="notEquals">
										<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocedit-confirm.jsp-2" parm0="${Location.LocationName}"/>
									</ffi:cinclude>
								</ffi:cinclude>
							</TD>
						</TR>
						<TR>
							<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
						</TR>
						<TR>
							<TD align="center">
								<sj:a button="true"  onClickTopics="cancelLocationForm"><s:text name="jsp.default_175"/></sj:a>
							</TD>

						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
			<p></p>
		</div>
<ffi:removeProperty name="EditLocation"/>
<ffi:removeProperty name="sendForApproval"/>
<ffi:removeProperty name="fromPendingTable"/>
<ffi:removeProperty name="Location"/>
<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminlocations-pre.jsp"/>
