<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:help id="user_crossaccountpermissions-verify" className="moduleHelpClass"/>
<ffi:setProperty name="PermissionsWizardSubMenu" value="TRUE"/>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>

<%-- we need to do auto entitlement checking for company, division and group being modified --%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

<%-- get the auto entitle settings for the business --%>
<%-- assuming the correct Entitlement_EntitlementGroup is in session --%>
<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="Entitlement_EntitlementGroup"/>
<ffi:process name="GetCumulativeSettings"/>

</s:if>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>	 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>

<%
   session.setAttribute( "limitIndex", new Integer( 0 ) );
   session.setAttribute( "LimitsList", session.getAttribute("NonAccountEntitlementsMerged") );
    
	if (request.getParameter("perm_target") != null)
        session.setAttribute("perm_target", request.getParameter("perm_target"));
		   
   
   if (request.getParameter("completedTopics") != null)
         session.setAttribute("completedTopics", request.getParameter("completedTopics"));

%>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">
		<%-- Initialize -- make available number of granted entitlements --%>
		<%-- (init only mode set for task) --%>
		<ffi:setProperty name="EditCrossAccountPermissions" property="InitOnly" value="true" />
		<ffi:process name="EditCrossAccountPermissions" />
	</ffi:cinclude>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>


<ffi:setProperty name="BackURL" value="${SecurePath}user/crossaccountpermissions.jsp?FromBack=TRUE&UseLastRequest=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="true" />
<ffi:setProperty name="SuccessURL" value="${SecurePath}user/crossaccountpermissions-confirm.jsp?EditGroup_AccountID=${Account.ID}&EditGroup_GroupId= ${EditGroup_GroupId}&PermissionsWizard=${PermissionsWizard}" URLEncrypt="true" />

<%-- Requires Approval Processing --%>
<ffi:setProperty name="EditRequiresApproval" property="ExecutePhase1" value="true" />
<ffi:process name="EditRequiresApproval" />
<%-- set action start --%>
<% String action = "editCrossAccountPermissions-execute"; %>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">
		<ffi:cinclude value1="${EditCrossAccountPermissions.NumGrantedEntitlements}" value2="0" operator="notEquals">
			<% action = "editCrossAccountPermissions-autoentitle"; %>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<% action = "editCrossAccountPermissions-execute"; %>
	</ffi:cinclude>
</s:if> 
<% 
	request.setAttribute("action", action); 
	session.setAttribute("action_execute", "editCrossAccountPermissions-execute");
%>
<%-- set action end --%>	
<div align="center" class="marginBottom20 marginTop10"><ffi:getProperty name="Context"/></div>
<s:form id="permLimitFrmVerify" name="permLimitFrmVerify" namespace="/pages/user" action="%{#request.action}" validate="false" theme="simple" method="post">	
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="EditGroup_AccountID" value='<ffi:getProperty name="Account" property="ID"/>'>
<input type="hidden" name="EditGroup_GroupId" value='<ffi:getProperty name="EditGroup_GroupId"/>'>
<input type="hidden" name="PermissionsWizard" value='<ffi:getProperty name="SavePermissionsWizard"/>'>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<input type="hidden" name="doAutoEntitle" value='true' />
	</ffi:cinclude>
</s:if> 
<ffi:flush/>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminBackground tableData permissionTblWithBorder">
			                                    <tr class="header">
			                                    	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			                                        <td valign="middle" align="center" nowrap class="sectionsubhead">
				                                            <s:text name="jsp.user_32"/>
			                                        </td>
			                                        </ffi:cinclude>
			                                        <td valign="middle" align="center" nowrap class="sectionsubhead">
			                                            <s:text name="jsp.user_177"/>
			                                        </td>
			                                        <td valign="middle" nowrap class="sectionsubhead">
			                                            <s:text name="jsp.user_346"/>
			                                        </td>
			                                     		<%-- Requires Approval Column --%>
		                                        <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
			                                        <td valign="middle" nowrap class="sectionsubhead">
														<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
															<s:text name="jsp.user_278"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
															&nbsp;
														</ffi:cinclude>
													</td>
		                                        </ffi:cinclude>
			                                        <td valign="middle" align="left" nowrap class="sectionsubhead">
								<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
								<ffi:process name="GetLimitBaseCurrency" />
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td valign="middle" align="center" nowrap class="sectionsubhead">
			                                        </td>
			                                        <td valign="middle" align="center" nowrap class="sectionsubhead">
																								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                            <s:text name="jsp.user_153"/>
																								</ffi:cinclude>
																								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																									&nbsp;
																								</ffi:cinclude>
			                                        </td>
			                                        <td valign="middle" align="left" nowrap class="sectionsubhead">
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td valign="middle" align="center" nowrap class="sectionsubhead">
			                                        </td>
			                                        <td valign="middle" align="center" nowrap class="sectionsubhead">
																								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                           <s:text name="jsp.user_153"/>
																								</ffi:cinclude>
																								<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																									&nbsp;
																								</ffi:cinclude>
			                                        </td>
			                                    </tr>

<ffi:setProperty name="DisplayedLimit" value="FALSE"/>
<ffi:setProperty name="CheckEntitlementObjectType" value=""/>
<ffi:setProperty name="CheckEntitlementObjectId" value=""/>
<ffi:flush/>

<%-- Include page to display Entitlements with limits --%>

<%-- Flag to determine if the admin column should be drawn --%>
<ffi:setProperty name="HasAdmin" value="true"/>
<ffi:setProperty name="CheckRequiresApproval" property="OpCounter" value="0" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<s:include value="inc/corpadminuserlimit_merged-verify.jsp"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<s:include value="inc/corpadmingrouplimit_merged-verify.jsp"/>
</s:if>

<ffi:cinclude value1="${DisplayedLimit}" value2="FALSE" operator="equals">
                                                <tr>
                                                    <td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="middle" align="center">
                                                        <span class="columndata"><s:text name="jsp.user_211"/></span>
                                                    </td>
                                                </tr>
</ffi:cinclude>
			                                    <tr>
			                                        <td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top" class="sectionsubhead" align="center">
													<s:url id="inputUrl" value="/pages/jsp/user/crossaccountpermissions.jsp" escapeAmp="false">
														<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
														<s:param name="ComponentIndex" value="%{#session.ComponentIndex}"></s:param>
														<s:param name="perm_target" value="%{#session.perm_target}"></s:param>
														<s:param name="FromBack" value="%{'TRUE'}"></s:param>
														<s:param name="UseLastRequest" value="%{'TRUE'}"></s:param>
														<s:param name="PermissionsWizard" value="%{#session.PermissionsWizard}"></s:param>
														<s:param name="completedTopics" value="%{#session.completedTopics}"></s:param>
													</s:url>
													<sj:a 
														href="%{inputUrl}"
														targets="%{#session.perm_target}"
														button="true"
														onCompleteTopics="adminSaveOnCompleteTopic"
														><s:text name="jsp.default_57"/></sj:a>
													<sj:a
														button="true"
														onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
														><s:text name="jsp.default_102"/></sj:a>
												<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
													<sj:a id="SAVE"
														formIds="permLimitFrmVerify"
														targets="resultmessage"
														button="true" 
														validate="false"
														onBeforeTopics="beforeSubmit" 
														onErrorTopics="errorSubmit"
														onSuccessTopics="%{#session.completedTopics}"
														onCompleteTopics="adminSaveOnCompleteTopic"														
														><s:text name="jsp.default_366"/></sj:a>
												</s:if>
												<s:if test="%{#session.Section == 'UserProfile'}">
													<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="equals">
															<sj:a id="SAVE"
																formIds="permLimitFrmVerify"
																targets="resultmessage"
																button="true" 
																validate="false"
																onBeforeTopics="beforeSubmit" 
																onErrorTopics="errorSubmit"
																onSuccessTopics="%{#session.completedTopics},reloadCompanyApprovalGrid"
																><s:text name="jsp.default_366"/></sj:a>
													</ffi:cinclude>
													<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="notEquals">
														<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">
														
															<ffi:cinclude value1="${EditCrossAccountPermissions.NumGrantedEntitlements}" value2="0" operator="notEquals">
																<ffi:setProperty name="perm_target" value="${perm_target}"/>
																<ffi:setProperty name="completedTopics" value="${completedTopics}"/>
																<sj:a
																	formIds="permLimitFrmVerify"
																	targets="%{#session.perm_target}"
																	button="true" 
																	validate="false"
																	onErrorTopics="errorSubmit"
																	onBeforeTopics="beforeVerify" 
																	><s:text name="jsp.default_111"/></sj:a>
																<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg17')}" scope="request" />
																		<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
																		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg18')}" scope="request" />
																		<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
																		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg19')}" scope="request" />
																		<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
																		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg26')}" scope="request" />
																		<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
																</ffi:cinclude>
																<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/crossaccountpermissions-verify.jsp?perm_target=${perm_target}&completedTopics=${completedTopics}"/>
																<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																	<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
																</ffi:cinclude>
																<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
																	<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
																</ffi:cinclude>
																<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/crossaccountpermissions-confirm.jsp"/>
															</ffi:cinclude>
	
															<ffi:cinclude value1="${EditCrossAccountPermissions.NumGrantedEntitlements}" value2="0" operator="equals">
																<sj:a id="SAVE"
																	formIds="permLimitFrmVerify"
																	targets="resultmessage"
																	button="true" 
																	validate="false"
																	onBeforeTopics="beforeSubmit" 
																	onErrorTopics="errorSubmit"
																	onSuccessTopics="%{#session.completedTopics},reloadCompanyApprovalGrid"
																	><s:text name="jsp.default_366"/></sj:a>
															</ffi:cinclude>
														
														</ffi:cinclude>
														
														<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="false" operator="equals">
															<sj:a id="SAVE"
																formIds="permLimitFrmVerify"
																targets="resultmessage"
																button="true" 
																validate="false"
																onBeforeTopics="beforeSubmit" 
																onErrorTopics="errorSubmit"
																onSuccessTopics="%{#session.completedTopics},reloadCompanyApprovalGrid"
																><s:text name="jsp.default_366"/></sj:a>
														</ffi:cinclude>
													
													</ffi:cinclude>
												</s:if> 
												<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles' && #session.Section != 'UserProfile'}">

													<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="true" operator="equals">
													
														<ffi:cinclude value1="${EditCrossAccountPermissions.NumGrantedEntitlements}" value2="0" operator="notEquals">
															<ffi:setProperty name="perm_target" value="${perm_target}"/>
															<ffi:setProperty name="completedTopics" value="${completedTopics}"/>
															<sj:a
																formIds="permLimitFrmVerify"
																targets="%{#session.perm_target}"
																button="true" 
																validate="false"
																onErrorTopics="errorSubmit"
																onBeforeTopics="beforeVerify" 
																><s:text name="jsp.default_111"/></sj:a>
															<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																	<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg17')}" scope="request" />
																	<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
																	<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg18')}" scope="request" />
																	<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
																	<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg19')}" scope="request" />
																	<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
																	<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg26')}" scope="request" />
																	<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/crossaccountpermissions-verify.jsp?perm_target=${perm_target}&completedTopics=${completedTopics}"/>
															<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
																<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
															</ffi:cinclude>
															<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/crossaccountpermissions-confirm.jsp"/>
														</ffi:cinclude>

														<ffi:cinclude value1="${EditCrossAccountPermissions.NumGrantedEntitlements}" value2="0" operator="equals">
															<sj:a id="SAVE"
																formIds="permLimitFrmVerify"
																targets="resultmessage"
																button="true" 
																validate="false"
																onBeforeTopics="beforeSubmit" 
																onErrorTopics="errorSubmit"
																onSuccessTopics="%{#session.completedTopics},reloadCompanyApprovalGrid"
																><s:text name="jsp.default_366"/></sj:a>
														</ffi:cinclude>
													
													</ffi:cinclude>
													
													<ffi:cinclude value1="${GetCumulativeSettings.EnablePermissions}" value2="false" operator="equals">
														<sj:a id="SAVE"
															formIds="permLimitFrmVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="beforeSubmit" 
															onErrorTopics="errorSubmit"
															onSuccessTopics="%{#session.completedTopics},reloadCompanyApprovalGrid"
															><s:text name="jsp.default_366"/></sj:a>
													</ffi:cinclude>
													
												</s:if>
			                                            </td>
			                                    </tr>
												<tr>
													<td height="25">&nbsp;</td>
												</tr>
								</table></div></div>
</s:form>

<script type="text/javascript">
	
	$.subscribe('<ffi:getProperty name="completedTopics" />', function(event, data)
	{	
        ns.common.showStatus($("#resultmessage").html().trim());
		
		ns.admin.goBackInput();
	});
	
	ns.admin.goBackInput = function ()
	{
		$.post('/cb/pages/jsp/user/crossaccountpermissions.jsp', {ComponentIndex: '<ffi:getProperty name="ComponentIndex" />', perm_target: '<ffi:getProperty name="perm_target" />', completedTopics:'<ffi:getProperty name="completedTopics" />', CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'}, 
		function(data) {
			$('#<ffi:getProperty name="perm_target" />').html(data);
			$.publish("common.topics.tabifyNotes");
			if(accessibility){
				$('#<ffi:getProperty name="perm_target" />').setFocus();	
			}			
		});
	
	}
</script>

<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<ffi:removeProperty name="HasAdmin"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="CheckForRedundantLimits"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="PermissionsWizardSubMenu"/>
<ffi:removeProperty name="SavePermissionsWizard"/>
