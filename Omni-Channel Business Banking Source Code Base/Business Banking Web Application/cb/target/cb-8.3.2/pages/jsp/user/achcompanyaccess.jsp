<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<div id="achcompanyaccessDiv" class="remoteACHCompany" >
<ffi:help id="user_achcompanyaccess" className="moduleHelpClass"/>
<%
	request.setAttribute("ACHCompanyID", request.getParameter("ACHCompanyID"));
	request.setAttribute("FromBack", request.getParameter("FromBack"));
	session.setAttribute("UseLastRequest", request.getParameter("UseLastRequest"));
%>
<%-- reset the CheckEntitlementByGroup caching --%>
<ffi:setProperty name="CheckEntitlementByGroup" property="resetCache" value="true"/>

<s:set var="tmpI18nStr" value="%{getText('jsp.user_35')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%-- only one thread should be allowed to execute this page at a time; otherwise, display errors
     could potentially cause the user to submit corrupted data --%>
<ffi:synchronized>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_REQUIRE_APPROVAL %>"/>
</ffi:cinclude>

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<%-- Get the BusinessEmployee, whose permission is currently being edited --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>

<%-- put an ACHCompanies object ACHCOMPANIES into the session --%>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies" scope="session"/>
<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="false"/>
<ffi:setProperty name="GetACHCompanies" property="fetchForAdminUser" value="true"/>
<ffi:process name="GetACHCompanies"/>
<ffi:removeProperty name="GetACHCompanies"/>

<%-- setACHCompany put the ACH company that will have its entitlements and limits displayed in session --%>
<ffi:object id="SetACHCompany" name="com.ffusion.tasks.ach.SetACHCompany" scope="session"/>
<ffi:setProperty name="SetACHCompany" property="CompaniesInSessionName" value="ACHCOMPANIES"/>
<ffi:setProperty name="SetACHCompany" property="CompanyInSessionName" value="ACHCompany"/>
<ffi:cinclude value1="${ACHCompanyID}" value2="" operator="equals">
	<ffi:cinclude value1="${ACHCOMPANIES.size}" value2="0" operator="notEquals">
		<ffi:list collection="ACHCOMPANIES" items="CompanyItem" startIndex="1" endIndex="1">
			<ffi:setProperty name="ACHCompanyID" value="${CompanyItem.CompanyID}"/>
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${ACHCompanyID}" value2="" operator="notEquals">
	<ffi:setProperty name="SetACHCompany" property="CompanyID" value="${ACHCompanyID}"/>
	<ffi:process name="SetACHCompany"/>
</ffi:cinclude>
<ffi:removeProperty name="SetACHCompany"/>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
    <ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
        <ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
            <ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}" />
            <ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
            <ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
            <ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
            <ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
	<ffi:process name="GetCategories"/>
	<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.GetPendingObjectsByTypeSubType" id="getPendingObjects"/>
		<ffi:setProperty name="getPendingObjects" property="objectIdsSessionName" value="daObjectIds"/>
		<ffi:setProperty name="getPendingObjects" property="objectType" value="<%=EntitlementsDefines.ACHCOMPANY%>"/>
		<ffi:setProperty name="getPendingObjects" property="categorySubType" value="${Category_Sub_Session_Name}"/>
	<ffi:process name="getPendingObjects"/>

</ffi:cinclude>
<%-- if there is no entitled ACH company we don't have to call a lot of tasks --%>
<ffi:cinclude value1="${ACHCOMPANIES.size}" value2="0" operator="notEquals">
	<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">

		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session" />
				<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session" />
			</ffi:cinclude>
		</s:if>
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
					<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
					<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
				</ffi:cinclude>
				<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
					<s:if test="#session.BusinessEmployee.UsingEntProfiles">
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
					</s:if>
					<s:else>
						<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
					</s:else>
					<ffi:setProperty name="EditACHCompanyAccess" property="approveAction" value="false"/>
				</ffi:cinclude>
				
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<ffi:object id="EditACHCompanyAccess" name="com.ffusion.tasks.admin.EditACHCompanyAccess" scope="session"/>
			</ffi:cinclude>
			<ffi:setProperty name="EditACHCompanyAccess" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="EditACHCompanyAccess" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="EditACHCompanyAccess" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		</s:if>

		<ffi:setProperty name="EditACHCompanyAccess" property="GroupId" value="${EditGroup_GroupId}"/>
   	 	<ffi:setProperty name="EditACHCompanyAccess" property="PrefixName" value="init"/>
		<ffi:setProperty name="EditACHCompanyAccess" property="EntsListSessionName" value="perACHEntLists"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditACHCompanyAccess" property="ProfileId" value="${Business.Id}"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditACHCompanyAccess" property="ACHCompanyID" value="${ACHCompanyID}"/>

		<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
		<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
	</ffi:cinclude>

	<%-- call the GetACHCompanyAccess task to put object in session for the correct display of the page --%>
	<ffi:object id="GetACHCompanyAccess" name="com.ffusion.tasks.admin.GetACHCompanyAccess" scope="session"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
		<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
		<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
		<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
	</ffi:cinclude>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
		<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="GetACHCompanyAccess" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetACHCompanyAccess" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetACHCompanyAccess" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	</s:if>
	<ffi:setProperty name="GetACHCompanyAccess" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="EntsListSessionName" value="perACHEntListsBeforeFilter"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentChildName" value="ParentChildLists"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="ChildParentName" value="ChildParentLists"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="ACHCompanyID" value="${ACHCompanyID}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="EntsLimitsSessionName" value="limitInfoList"/>
	<ffi:process name="GetACHCompanyAccess"/>
	<ffi:removeProperty name="GetACHCompanyAccess"/>

	<%-- filter the entitlements so that those entitlements restricted by the BusinessAdmin entitlement group
		(as well as its parents) would not be displayed, along with the restricted entitlements' control children --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
				<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
				<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>" />
				<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="EntitlementBean" />
			<ffi:process name="GetDACategoryDetails"/>

			<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.dualapproval.FilterEntitlementsForBusinessDA"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="daCategorySessionName" value="EntitlementBean"/>
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
				<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
				<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>" />
				<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="EntitlementBean" />
			<ffi:process name="GetDACategoryDetails"/>

			<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.dualapproval.FilterEntitlementsForBusinessDA"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="daCategorySessionName" value="EntitlementBean"/>
		</ffi:cinclude>
		<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
		</s:if>
	</ffi:cinclude>

	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
	</ffi:cinclude>
	<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="perACHEntListsBeforeFilter"/>
	<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="perACHEntLists"/>
	<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
	<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
	<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${ACHCompanyID}"/>
	<ffi:process name="FilterEntitlementsForBusiness"/>
	<ffi:removeProperty name="FilterEntitlementsForBusiness"/>
	<ffi:removeProperty name="EntitlementBean"/>

	<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	</s:if>
	<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="GetGroupLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
	<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${ACHCompanyID}"/>

	<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
	<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
	<ffi:process name="ApprovalAdminByBusiness"/>

	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:object id="CheckEntitlementByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
		<ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	</s:if>

	<ffi:object id="GetMaxACHCompanyLimits" name="com.ffusion.tasks.admin.GetMaxACHCompanyLimits" scope="session" />
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="LimitInfoListSessionName" value="limitInfoList"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="PerTransactionMapName" value="perBatchMax"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="PerDayMapName" value="dailyMax"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	</s:if>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="ACHCompanyID" value="${ACHCompanyID}"/>
	<ffi:process name="GetMaxACHCompanyLimits"/>
	<ffi:removeProperty name="GetMaxACHCompanyLimits"/>

	<ffi:object id="GetMaxACHLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxACHLimitForPeriod" scope="session" />
	<ffi:setProperty name="GetMaxACHLimitForPeriod" property="PerTransactionMapName" value="perBatchMax"/>
	<ffi:setProperty name="GetMaxACHLimitForPeriod" property="PerDayMapName" value="dailyMax"/>
	<ffi:process name="GetMaxACHLimitForPeriod"/>
	<ffi:setProperty name="GetMaxACHLimitForPeriod" property="NoLimitString" value="no"/>

	<%-- this task need the perBatchMax and the dailyMax put into the session by the GetMaxACHCompanyLimits task --%>
	<ffi:object id="CheckForRedundantACHLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantACHLimits" scope="session" />
	<ffi:setProperty name="CheckForRedundantACHLimits" property="PerTransactionMapName" value="perBatchMax"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="PerDayMapName" value="dailyMax"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="LimitInfoListSessionName" value="limitInfoList"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="GroupId" value="${EditGroup_GroupId}"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="CheckForRedundantACHLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="CheckForRedundantACHLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="CheckForRedundantACHLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	</s:if>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="ACHCompanyID" value="${ACHCompanyID}"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/achcompanyaccess-verify-init.jsp&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
	<ffi:setProperty name="CheckForRedundantACHLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/achcompanyaccess.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>

	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>" />
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${ACHCompanyID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
			<s:if test="%{#session.Section == 'Users'}">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			</s:if>
			<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Business.Id}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			</ffi:cinclude>
		<ffi:process name="GetDACategoryDetails" />

		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
		<ffi:process name="GetDACategoryDetails" />
	</ffi:cinclude>

	<% session.setAttribute("FFIEditACHCompanyAccess", session.getAttribute("EditACHCompanyAccess")); %>
	
	<script language="JavaScript" type="text/javascript"><!--

	$(document).ready(function(){
		initializeParentChild();
		
		$("#perAchCompanyResetButtonId").button({
			icons: {
			}			
		});
		
		$("#ACHSelectCompanyID").selectmenu({width: 300});
	});
	
	function ACHSelectCompanyIDChange(){
		$("#achCompanySearchSubmit").trigger('click');	
	}
	function initializeParentChild() {
		parentChildArray = new Array();
		childParentArray = new Array();

		// in this page, there are only two prefixes
		prefixes = new Array( "init", "admin" );

	<ffi:object id="counterMath" name="com.ffusion.beans.util.IntegerMath"/>
	<ffi:setProperty name="counterMath" property="Value1" value="0"/>
	<ffi:setProperty name="counterMath" property="Value2" value="1"/>

	<ffi:object id="prefixCounter" name="com.ffusion.beans.util.IntegerMath"/>
	<ffi:setProperty name="prefixCounter" property="Value1" value="0"/>

		// Populate the parentChildArray
	<ffi:list collection="ParentChildLists" items="childList">
			parentChildArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array(
		<ffi:list collection="childList" items="entry" startIndex="1" endIndex="1">
				prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
		</ffi:list>
		<ffi:list collection="childList" items="entry" startIndex="2">
				, prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
		</ffi:list>
			);

		<%-- Make the index of the prefixes array alternate between 0 and 1 --%>
		<ffi:setProperty name="prefixCounter" property="Value2" value="1"/>
		<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Add}"/>
		<ffi:setProperty name="prefixCounter" property="Value2" value="2"/>
		<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Modulo}"/>

		<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />
	</ffi:list>

		// Populate the childParentArray
	<ffi:setProperty name="counterMath" property="Value1" value="0"/>
	<ffi:setProperty name="prefixCounter" property="Value1" value="0"/>

	<ffi:list collection="ChildParentLists" items="parentList">
			childParentArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array(
		<ffi:list collection="parentList" items="entry" startIndex="1" endIndex="1">
				prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
		</ffi:list>
		<ffi:list collection="parentList" items="entry" startIndex="2">
				, prefixes[<ffi:getProperty name="prefixCounter" property="Value1"/>] + "<ffi:getProperty name="entry"/>"
		</ffi:list>
			);

		<%-- Make the index of the prefixes array alternate between 0 and 1 --%>
		<ffi:setProperty name="prefixCounter" property="Value2" value="1"/>
		<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Add}"/>
		<ffi:setProperty name="prefixCounter" property="Value2" value="2"/>
		<ffi:setProperty name="prefixCounter" property="Value1" value="${prefixCounter.Modulo}"/>

		<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />
	</ffi:list>

	<ffi:removeProperty name="counterMath"/>
	<ffi:removeProperty name="prefixCounter"/>
	<ffi:removeProperty name="ParentChildLists"/>
	<ffi:removeProperty name="ChildParentLists"/>
	}

	ns.admin.resetACHCompanyAccessForm = function()
	{
		$("#aCHCompanyTab").click();
	}
	// --></script>

</ffi:cinclude>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>

	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>

	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	</s:else>
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- Requires Approval Settings --%>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:removeProperty name="EditRequiresApproval" />
	<ffi:removeProperty name="CheckRequiresApproval" />
</ffi:cinclude>
<%-- END Requires Approval Settings --%>

<%-- Requires Approval Processing for Per Account Permissions --%>
<ffi:removeProperty name="CheckRequiresApprovalPerAcct" />
<ffi:object id="CheckRequiresApprovalPerAcct" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="OpCounter" value="0" />
<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="DispPropListName" value="NonAccountEntitlementsMerged" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="ModifyUserOnly" value="true" />
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CheckRequiresApprovalPerAcct" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>

<s:include value="inc/parentchild_js.jsp"/>
<s:include value="inc/achFormHandle_js.jsp"/>
<%-- <span><s:text name="jsp.user_409"/></span> --%>
<ffi:cinclude value1="${ACHCOMPANIES.size}" value2="0" operator="notEquals">
<div align="center">
<ul id="formerrors"></ul>
<div class="marginBottom20"><ffi:getProperty name="Context"/></div>
<s:form id="ACHCompanySelect" name="ACHCompanySelect" namespace="/pages/user" action="achcompanyaccess" theme="simple" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
<table width="100%" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td class="columndata" align="center" valign="top"><s:text name="jsp.user_287"/></td>
		</tr>
		<tr>
			<td align="center" class="sectionsubhead">
				<select name="ACHCompanyID" size="5" style="width:300px"  class="txtbox" id="ACHSelectCompanyID" onchange="ACHSelectCompanyIDChange()">
					<ffi:list collection="ACHCOMPANIES" items="ACHCompanyListItem">
						<option <ffi:cinclude value1="${ACHCompany.CompanyID}" value2="${ACHCompanyListItem.CompanyID}" operator="equals"> selected</ffi:cinclude> value="<ffi:getProperty name="ACHCompanyListItem" property="CompanyID"/>"
							<ffi:list collection="daObjectIds" items="daCompanyId">
								<ffi:cinclude value1="${daCompanyId}" value2="${ACHCompanyListItem.CompanyID}" operator="equals"> class="columndataDA"</ffi:cinclude>
							</ffi:list>
							>
							<ffi:getProperty name="ACHCompanyListItem" property="CompanyName"/> -
							<ffi:getProperty name="ACHCompanyListItem" property="CompanyID"/>
						</option>
					</ffi:list>
				</select>
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead" align="center" valign="top">
				<sj:a
					id="achCompanySearchSubmit"
					formIds="ACHCompanySelect"
					targets="achcompanyaccessDiv"
					button="true"
					validate="false"
					cssStyle="display:none"
					onCompleteTopics="onCompleteCompanySearchTopic"
					><s:text name="jsp.default_459"/></sj:a>
			</td>
		</tr>
</table>
</s:form>
<s:form namespace="/pages/user" action="editACHCompanyAccess-verify" validate="false" theme="simple" method="post" name="EditACHCompanyForm" id="EditACHCompanyForm">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<%-- <div id="perAchCompanyLabel" class="sectionhead marginTop10 marginBottom20" align="center">
	<ffi:getProperty name="ACHCompany" property="CompanyName"/> -
	<ffi:getProperty name="ACHCompany" property="CompanyID"/>
</div> --%>
<div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">			
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData permissionsTableSection">
					<%-- display the entitlements and limits if there is any --%>
					<ffi:cinclude value1="${perACHEntLists.size}" value2="0" operator="notEquals">
							
							<tr class="header">
    							        <ffi:setProperty name="Compare" property="Value1" value="TRUE"/>
						                <ffi:setProperty name="Compare" property="Value2" value=""/>
							 	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
							 	<td class="sectionsubhead" align="center" valign="middle">
			                                           
							  	    <ffi:setProperty name="LastRequest" property="Name" value="adminAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxValue" value="adminAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
								    <ffi:removeProperty name="adminAll"/>
							            <INPUT type="checkbox" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> onClick="setACHCheckBoxes(EditACHCompanyForm, 'adminAll', 'admin');">
							             <s:text name="jsp.user_32"/>
						        </td>
							        </ffi:cinclude>
			                                        <td class="sectionsubhead" align="center" valign="middle">
							 	    <ffi:setProperty name="LastRequest" property="Name" value="initAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxValue" value="initAll"/>
								    <ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
								    <ffi:removeProperty name="initAll"/>
							            <INPUT type="checkbox" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> onClick="setACHCheckBoxes(EditACHCompanyForm, 'initAll', 'init');">
							             <s:text name="jsp.user_177"/>
			                   </td>
								<td class="sectionsubhead" valign="middle">
			                                            <s:text name="jsp.user_346"/>
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
								<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
								<ffi:process name="GetLimitBaseCurrency" />
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle" colspan="2">
			                                            <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
			                                        </td>
			                                        <td class="sectionsubhead" align="center" valign="middle">
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			                                        	    <s:text name="jsp.user_153"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
									    &nbsp;
									</ffi:cinclude>
			                                        </td>
		                                    	</tr>

							<%-- set up the correct collectionName for use by the achcompanyaccess_rowmaker.jsp --%>
							<ffi:setProperty name="collectionName" value="perACHEntLists"/>
							<ffi:setProperty name="ACHFormName" value="EditACHCompanyForm"/>
                        <%-- Flag so that the included pages that actually display the limits and entitlements
                             will include the javascript function that makes parent/child work --%>
                            <ffi:setProperty name="ParentChild" value="true"/>
							<s:include value="inc/achcompanyaccess_rowmaker.jsp"/>
							<ffi:removeProperty name="collectionName"/>
							<ffi:removeProperty name="ACHFormName"/>
                            <ffi:removeProperty name="ParentChild"/>

							<%-- <tr>
								<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
									<td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="40" height="1"></td>
								</ffi:cinclude>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="40" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="155" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="75" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="60" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="95" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="75" height="1"></td>
				                                <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="60" height="1"></td>
					                            <td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" width="95" height="1"></td>
							</tr> --%>
							<%-- <tr>
								<td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="top"><hr noshade size="1"></td>
							</tr> --%>
							<tr>
								<td class="sectionsubhead" colspan="<ffi:getProperty name="NumTotalColumns"/>" align="center" valign="top">
									<br/><br/>
									<%-- <a  id="perAchCompanyResetButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
										title='<s:text name="jsp.default_358.1" />' 
										href='#' 
										onClick="ns.admin.resetACHCompanyAccessForm(); removeValidationErrors();">
										<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358"/></span>
									</a> --%>
									
									<a  id="perAchCompanyResetButtonId" 
										title='<s:text name="jsp.default_358.1" />' 
										href='#' 
										onClick="ns.admin.resetACHCompanyAccessForm(); removeValidationErrors();">
										<s:text name="jsp.default_358"/>
									</a>
									
									<sj:a
										href='#'  
										id="ACHCompanyCancel"
										button="true"
										onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
										><s:text name="jsp.default_102"/></sj:a>
									<sj:a
										href='#' 
										id="ACHCompanySubmit"
										formIds="EditACHCompanyForm"
										targets="achcompanyaccessDiv"
										button="true"
										validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeVerify"
										onCompleteTopics="onCompletePermissionSaveTopic"
										><s:text name="jsp.default_366"/></sj:a>
									<sj:a
										href='#' 
										id="permACHNext"
										button="true" 
										onclick="return ns.admin.nextPermission('#permACHNext', '#permTabs');"
										><s:text name="jsp.user_203"/></sj:a>
									<br/><br/>
								</td>
							</tr>
						</ffi:cinclude>
						<ffi:cinclude value1="${perACHEntLists.size}" value2="0" operator="equals">
							<tr>
								<td class="sectionhead" align="center" valign="top">
									<ffi:getProperty name="ACHCompany" property="CompanyName"/> -
									<ffi:getProperty name="ACHCompany" property="CompanyID"/>
								</td>
							</tr>
							<tr>
								<td class="columndata" align="center" valign="top">
									<s:text name="jsp.user_333"/>
									<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
										<ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/>
									</s:if>
    								<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
								    	<ffi:getProperty name="BusinessEmployee" property="Name"/>
								    </s:if>
								</td>
							</tr>
							<tr>
								<td class="sectionsubhead" align="center" valign="top">
									<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
								</td>
							</tr>
						</ffi:cinclude>
						<tr>
								<td height="10">&nbsp;</td>
							</tr>
					</table></div></div>
					</s:form>
	</div>
   </body>
   <ffi:removeProperty name="GetMaxACHLimitForPeriod"/>
   <ffi:removeProperty name="AdminCheckBoxType"/>
   <ffi:removeProperty name="NumTotalColumns"/>
   <ffi:removeProperty name="UseLastRequest"/>
   <s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:removeProperty name="CheckEntitlementByMember"/>
   </s:if>
   <ffi:removeProperty name="DisplayErrors"/>
   <ffi:removeProperty name="GetGroupLimits"/>
   <ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals">
	   <ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
	   </ffi:cinclude>
	   <ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/permissions.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
	   </ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="notEquals">
		<ffi:setProperty name="BackURL" value="${SecurePath}user/achcompanyaccess.jsp?UseLastRequest=TRUE&PermissionsWizard=TRUE" URLEncrypt="TRUE"/>
	</ffi:cinclude>
</ffi:cinclude>



<%-- if there is no entitled ACH company --%>
<ffi:cinclude value1="${ACHCOMPANIES.size}" value2="0" operator="equals">
	<DIV align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="sectionhead" align="center">
				    <ffi:getProperty name="Context"/><br><br>
				</td>
			</tr>
		</table>
		<table class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" class="adminBackground">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td class="columndata" align="center" valign="top"><s:text name="jsp.user_327"/></td>
						</tr>
						<tr>
							<td class="sectionsubhead" align="center" valign="top">
								<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
								
								<sj:a
									href='#' 
									id="permACHNext"
									button="true" 
									onclick="return ns.admin.nextPermission('#permACHNext', '#permTabs');"
									><s:text name="jsp.user_203"/></sj:a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</DIV>
</div>
   <ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp"/>
   </ffi:cinclude>
   <ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/permissions.jsp"/>
   </ffi:cinclude>
</ffi:cinclude>

</ffi:synchronized>
<ffi:removeProperty name="CheckRequiresApprovalPerAcct" />
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="SavePermissionsWizard"/>
<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />