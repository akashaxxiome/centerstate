<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:help id="user_corpadminadminedit" className="moduleHelpClass"/>
<ffi:cinclude value1="${SecureUser.UsingEntProfiles}" value2="true" operator="equals">
	<ffi:setProperty name="AutoEntitleAdministrators" value="true"/>
</ffi:cinclude>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="CanAdministerAnyGroup" />

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">

	<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
		<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
			<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
			<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
			<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
			<ffi:setProperty name="SetAdministrators" property="Commit" value="true"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
			<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
			<ffi:process name="SetAdministrators"/>
	</ffi:cinclude>

</ffi:cinclude>
<%-- If dual approval is enabled,then add changed administrator list to DA tables. --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
	<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
	<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
	<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
	<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
	<ffi:process name="SetAdministrators"/>
	<ffi:object id="SetAdministratorsInDA" name="com.ffusion.tasks.dualapproval.SetAdministratorsInDA"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="AddedMemberIds" value="addedMemberIds"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="RemovedMemberIds" value="removedMemberIds"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="ItemId" value="${Business.Id}"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="UserAction" value="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>"/>
	<ffi:process name="SetAdministratorsInDA"/>
</ffi:cinclude>


<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="SetAdministrators"/>
<ffi:removeProperty name="SetAdministratorsInDA"/>
<ffi:removeProperty name="removedMemberIds"/>
<ffi:removeProperty name="addedMemberIds"/>


<div align="center">
	<div style="margin:20px 0;">
		<s:text name="jsp.user_61"/>
	</div>
	<div>
		<sj:a button="true"  onclick="$('#editAdminerAutoEntitleDialogId').dialog('close');">
			<s:text name="jsp.default_175"/>
		</sj:a>
	</div>
</div>
<%--onClickTopics="closeEditAdminerAutoEntitleDialog"--%>
<ffi:cinclude value1="${AdminEditType}" value2="Group" operator="equals">
	<s:include value="inc/corpadmingroups-pre.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${AdminEditType}" value2="Division" operator="equals">
	<s:include value="inc/corpadmindiv-pre.jsp"/>
</ffi:cinclude>