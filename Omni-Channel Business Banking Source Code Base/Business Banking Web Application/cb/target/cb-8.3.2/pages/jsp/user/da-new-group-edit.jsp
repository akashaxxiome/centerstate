<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setL10NProperty name='PageHeading' value='Add Group'/>
<ffi:setProperty name='PageText' value=''/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_143"/></span>
<script language="javascript">
$(function(){
	$("#parentGroupIdForAdd").selectmenu({width: 150});
});
</script>
<%	
	String EditGroup_GroupId = request.getParameter("EditGroup_GroupId");
	if(EditGroup_GroupId!= null){
		session.setAttribute("EditGroup_GroupId", EditGroup_GroupId);
	}
	
	String Section = request.getParameter("Section");
	if(Section!= null){
		session.setAttribute("Section", Section);
	}
	
	String viewOnly = request.getParameter("viewOnly");
	if(viewOnly!= null){
		session.setAttribute("viewOnly", viewOnly);
	}
%>

<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
	// The user has pressed cancel, so go back to the previous version of the collections
		session.setAttribute("NonAdminEmployees", session
				.getAttribute("NonAdminEmployeesCopy"));
		session.setAttribute("AdminEmployees", session
				.getAttribute("AdminEmployeesCopy"));
		session.setAttribute("NonAdminGroups", session
				.getAttribute("NonAdminGroupsCopy"));
		session.setAttribute("AdminGroups", session
				.getAttribute("AdminGroupsCopy"));
		session.setAttribute("adminMembers", session
				.getAttribute("adminMembersCopy"));
		session.setAttribute("userMembers", session
				.getAttribute("userMembersCopy"));
		session.setAttribute("groupIds", session
				.getAttribute("groupIdsCopy"));
		session.setAttribute("tempAdminEmps", session
				.getAttribute("tempAdminEmpsCopy"));
		session.setAttribute("GroupDivAdminEdited", session
				.getAttribute("GroupDivAdminEditedCopy"));
%>
</ffi:cinclude>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="CancelReload"/>

<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
<ffi:process name="GetGroupsAdministeredBy"/>
<ffi:removeProperty name="GetGroupsAdministeredBy" />

<%-- save the group name - if we are coming back from admin edit, we need the possibly modified group name, not the one from the DA object --%>
	<ffi:setProperty name="saveGroupName" value="${AddBusinessGroup2.groupName}"/>	
	<ffi:setProperty name="saveParentGroupId" value="${AddBusinessGroup2.ParentGroupId}"/>	
	
<ffi:cinclude value1="${dontRefresh}" value2="true" operator="equals">
	<ffi:object id="AddBusinessGroup2" name="com.ffusion.tasks.admin.AddBusinessGroup" scope="session"/>
</ffi:cinclude>
<ffi:cinclude value1="${dontRefresh}" value2="" operator="equals">
	<ffi:object id="AddBusinessGroup2" name="com.ffusion.tasks.admin.AddBusinessGroup" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="dontRefresh"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>

<ffi:setProperty name="GetAncestorGroupByType" property="GroupType" value="Division"/>
<ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="DivisionEntGroup"/>
<ffi:process name="GetAncestorGroupByType"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="DAEntitlementGroupObject" name="com.ffusion.csil.beans.entitlements.EntitlementGroup" />

<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
	<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="DAEntitlementGroupObject"/>
<ffi:process name="PopulateObjectWithDAValues"/>

<ffi:cinclude value1="${DAEntitlementGroupObject.GroupName}" value2="" operator="notEquals">
	<ffi:setProperty name="AddBusinessGroup2" property="GroupName" value="${DAEntitlementGroupObject.GroupName}"/>
</ffi:cinclude>
<ffi:cinclude value1="${DAEntitlementGroupObject.ParentId}" value2="0" operator="notEquals">
  	    <ffi:setProperty name="AddBusinessGroup2" property="ParentGroupId" value="${DAEntitlementGroupObject.ParentId}"/>
</ffi:cinclude>
<ffi:setProperty name="AddBusinessGroup2" property="GroupType" value="Group"/>
<% session.setAttribute("FFIAddBusinessGroup", session.getAttribute("AddBusinessGroup2")); %>
<ffi:removeProperty name="DAEntitlementGroupObject"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
	
<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="TempBusinessEmployee"/>

<%-- Update group Name and parent group Id with stored in session --%>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="equals">
	<ffi:setProperty name="AddBusinessGroup2" property="groupName" value="${saveGroupName}"/>
	<ffi:setProperty name="AddBusinessGroup2" property="ParentGroupId" value="${saveParentGroupId}"/>
	
</ffi:cinclude>
<ffi:removeProperty name="saveGroupName"/>
<ffi:removeProperty name="saveParentGroupId"/>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
	<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value=""/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntToCheck" value="<%=EntitlementsDefines.GROUP_MANAGEMENT%>"/>
	<ffi:process name="GenerateUserAndAdminLists"/>
	<ffi:removeProperty name="adminIds"/>
	<ffi:removeProperty name="userIds"/>
	<ffi:removeProperty name="GenerateUserAndAdminLists"/>

	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
	<ffi:process name="GenerateListsFromDAAdministrators"/>
	
	<ffi:object id="GenerateUserAdminList" name="com.ffusion.tasks.dualapproval.GenerateUserAdminList"/>
	<ffi:setProperty name="GenerateUserAdminList" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateUserAdminList" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateUserAdminList" property="sessionName" value="businessAdminEmployees"/>
	<ffi:process name="GenerateUserAdminList"/>
	<%
		session.setAttribute("tempAdminEmps", session
					.getAttribute("businessAdminEmployees"));
	%>
	
</ffi:cinclude>


<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="NewAdminStringList"/>
<ffi:removeProperty name="OldAdminEmployees"/>
<ffi:removeProperty name="OldAdminGroups"/>
<ffi:removeProperty name="Admins"/>
<ffi:removeProperty name="BusinessEmployees"/>

		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="adminBackground">
					<s:form id="editAddGroupFormId" namespace="/pages/user" action="addBusinessGroup-verify" theme="simple" name="editGroupAddFormName" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                            <input type="hidden" name="AddBusinessGroup2.GroupType" value="Group">
                     		<input type="hidden" name="AddBusinessGroup2.CheckboxesAvailable" value="false">
							<input type="hidden" name="divAdd" value="true">
							<input type="hidden" name="UseLastRequest" value="TRUE">
							<input type="hidden" name="ItemId" value="<ffi:getProperty name="EditGroup_GroupId"/>">
							<div align="left">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td class="adminBackground sectionheadDA" align="right" width="150">
											<s:text name="jsp.approvals_7" /><span class="required">*</span>
										</td>
										<td class="adminBackground" valign="top">
											<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
												<input class="ui-widget-content ui-corner-all" type="text" name="GroupName" style="width:120px;" size="24" maxlength="255" border="0" value="<ffi:cinclude value1="${tempGroupName}" value2="" operator="equals"><ffi:getProperty name="AddBusinessGroup2" property="GroupName"/></ffi:cinclude><ffi:cinclude value1="${tempGroupName}" value2="" operator="notEquals"><ffi:getProperty name="tempGroupName"/></ffi:cinclude>">
												
											</ffi:cinclude>
                          					<ffi:cinclude value1="${viewOnly}" value2="TRUE">
												<span class="columndata">
													<ffi:getProperty name="AddBusinessGroup2" property="GroupName"/>
												</span>
											</ffi:cinclude>
											(<ffi:getProperty name="Business" property="BusinessName"/>)&nbsp;&nbsp;<span id="GroupNameError"></span>
                     					</td>
									</tr>
									<tr>
										<td class="adminBackground sectionheadDA" align="right" width="150">
											<s:text name="jsp.user_122" /><span class="required">*</span>
										</td>
										<td class="adminBackground" valign="baseline">
											<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
												<select class="txtbox" id="parentGroupIdForAdd" name="ParentGroupId">
													<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
														<ffi:cinclude value1="${EntGroupItem.EntGroupType}" value2="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>"
															operator="equals">
															<ffi:cinclude value1="${AddBusinessGroup2.ParentGroupId}" value2="0" operator="equals">
																<ffi:cinclude value1="${FirstDivision}" value2="" operator="equals">
																	<ffi:setProperty name="FirstDivision" value="${EntGroupItem.GroupId}" />
																</ffi:cinclude>
															</ffi:cinclude>
															<option
																value='<ffi:getProperty name="EntGroupItem" property="GroupId"/>'
																<ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${AddBusinessGroup2.ParentGroupId}" operator="equals">selected</ffi:cinclude>><ffi:getProperty
																name="EntGroupItem" property="GroupName" /></option>
														</ffi:cinclude>
													</ffi:list>
												</select>
											</ffi:cinclude>
											<ffi:cinclude value1="${viewOnly}" value2="TRUE">
												<span class="columndata">
													<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
													<ffi:cinclude value1="${EntGroupItem.EntGroupType}" value2="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>" operator="equals">
														<ffi:cinclude value1="${AddBusinessGroup2.ParentGroupId}" value2="0" operator="equals">
															<ffi:cinclude value1="${FirstDivision}" value2="" operator="equals">
																<ffi:setProperty name="FirstDivision" value="${EntGroupItem.GroupId}" />
															</ffi:cinclude>
														</ffi:cinclude>
														<ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${AddBusinessGroup2.ParentGroupId}" operator="equals">
															<ffi:getProperty name="EntGroupItem" property="GroupName" />
														</ffi:cinclude>
													</ffi:cinclude>
												</ffi:list>
												</span>
											</ffi:cinclude>
					                    </td>
									</tr>
									<tr>
									<ffi:setProperty name="AddBusinessGroup2" property="AdminListName" value="adminMembers"/>
									<td class="adminBackground sectionheadDA" align="right" valign="baseline" width="120">
										<s:text name="jsp.user_38" />:&nbsp; 
									</td>
									<td class="columndata adminBackground" valign="baseline" colspan="2">
										<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
											<ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="tempAdminEmps"/>
											<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
										<ffi:process name="FilterNotEntitledEmployees"/>
	 									<ffi:setProperty name="tempAdminEmps" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
	 									
										<%--display name of each employee in BusinessEmployees--%>
 										<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
										<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="tempAdminEmps"/>
										<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="AdministratorStr"/>
										<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
											<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
											<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
										</ffi:cinclude>
										<ffi:setProperty name="GenerateCommaSeperatedList" property="maxLength" value="120"/>
										<ffi:process name="GenerateCommaSeperatedList"/>
										<ffi:removeProperty name="GenerateCommaSeperatedList"/>
										
										<ffi:removeProperty name="AdminsSelected"/>	
	 									<ffi:getProperty name="AdministratorStr"/>
										<ffi:cinclude value1="AdministratorStr" value2="" operator="equals">
											<!--L10NStart-->None<!--L10NEnd-->
										</ffi:cinclude>
										<%-- set modifiedAdmins to "TRUE", so when the edit icon is clicked the corpadminadminedit.jsp --%>
										<%-- will look for the appropriate object in session for displaying --%>
										<ffi:setProperty name="modifiedAdmins" value="TRUE"/>
										<ffi:removeProperty name="empName"/>
										
										<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
											<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminadminedit.jsp?dontRefresh=false&divAdd=true" URLEncrypt="true"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${viewOnly}" value2="TRUE">
											<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminadminview.jsp?dontRefresh=false&divAdd=true" URLEncrypt="true"/>	
										</ffi:cinclude>
										<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
												<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
														<sj:a
															button="true"
															targets="editAdminerDialogId"
															formIds="editAddGroupFormId"												
															postaction="/cb/pages/user/addBusinessGroup-editAddGroupModifyAdminerChange.action"
															formid="editAddGroupFormId"
															onClickTopics="postOnlyFormTopic"
															onCompleteTopics="openEditAddDivisionAdminerDialog"
														>
														<s:text name="jsp.default_178" />
													</sj:a>
												 </ffi:cinclude>
												<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
													<sj:a
														button="true"
														targets="editAdminerDialogId"
														formIds="editAddGroupFormId"												
														postaction="/cb/pages/user/addBusinessGroup-editAddGroupModifyAdminerChange.action"
														formid="editAddGroupFormId"
														onClickTopics="postOnlyFormTopic"
														onCompleteTopics="openEditAddDivisionAdminerDialog"		 			
													>
														<s:text name="jsp.default_178" />
														</sj:a>														
												</ffi:cinclude>
										</ffi:cinclude>
										<ffi:cinclude value1="${viewOnly}" value2="TRUE">
												<s:url id="addDivisionAdministratorsViewURL" value="%{#session.PagesPath}user/corpadminadminview.jsp" escapeAmp="false">
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
													<s:param name="dontRefresh" value="%{'false'}"/>
													<s:param name="divAdd" value="%{'true'}"/>
												</s:url>
												<sj:a
													href="%{addDivisionAdministratorsViewURL}" 
													targets="viewAdminInViewDiv" 
													button="true" 
													title="View"
													onCompleteTopics="viewAdminInViewDivTopic"
												>
													<s:text name="jsp.default_6" />
												</sj:a>	
										</ffi:cinclude>
										<ffi:removeProperty name="tmp_url"/>
										</td>
									</tr>	
									<tr>
										<td colspan="2" valign="top">
											<div align="center">
												<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
													<span class="required"><s:text name="jsp.da_approvals_pending_user_require_field" /></span>
													<br>
													<span class="required">*<s:text name="user.division.minimum.two.users.required" /></span>
													<br>
													<br>
													<s:url id="editDAGroupResetURL" value="%{#session.PagesPath}user/da-new-group-edit.jsp" escapeAmp="false">
														<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
													</s:url>
													<sj:a 
														href="%{editDAGroupResetURL}" 
														targets="inputDiv" 
														button="true" ><s:text name="jsp.default_358"/></sj:a>
												</ffi:cinclude>
												<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
													<sj:a
													   button="true" 
													   onClickTopics="cancelGroupForm"><s:text name="jsp.default_82"/></sj:a>
												</ffi:cinclude>
												<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="notEquals">
													<sj:a
														formIds="editAddGroupFormId"
														targets="verifyDiv"
														button="true" 
														validate="true"
														onBeforeTopics="beforeVerify2"
														validateFunction="customValidation"
														onCompleteTopics="completeVerify2"
														formid="editAddGroupFormId"
														field1="needVerify_HiddenFieldId"
														value1="true"
														postaction="/cb/pages/user/addBusinessGroup-addNewEditedGroup.action"																								
														onClickTopics="postFormTopic"		 													
														><s:text name="jsp.default_29"/></sj:a>
												</ffi:cinclude>
											</div>
										</td>
									</tr>
									<ffi:cinclude value1="${viewOnly}" value2="TRUE" operator="equals">
										<tr>
											<td height="40">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="2" align="center" class="ui-widget-header customDialogFooter">
												<sj:a button="true" title="Close" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a>
											</td>
										</tr>
									</ffi:cinclude>
								</table>
							</div>
						</s:form>
					</td>
				</tr>
			</table>
		</div>


<ffi:removeProperty name="businessAdminEmployees"/>
<ffi:removeProperty name="viewOnly"/>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-new-group-edit.jsp?dontRefresh=false&AdminsSelected=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/da-new-group-edit.jsp?dontRefresh=false" URLEncrypt="true"/>
</ffi:cinclude>

<ffi:setProperty name="onCancelGoto" value="da-new-group-edit.jsp"/>
<ffi:setProperty name="onDoneGoto" value="da-new-group-edit.jsp"/>

<ffi:removeProperty name="DivisionEntGroup"/>

