<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<div id="baiexportsettingsDiv">
<ffi:help id="user_baiexportsettings" className="moduleHelpClass"/>
<s:include value="inc/corpadmininfo-pre.jsp" />
<span id="pageHeading" style="display:none"><s:text name="jsp.user_51"/> </span>
<!-- 	<tr> -->
		<%--<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="10%" border="0"></td>
		 <td align="left" colspan="2" class="adminBackground sectionhead">&gt; <s:text name="jsp.user_51"/> --%>
			<%-- <ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="AddBAIExportSettingsToDA" value2="" operator="notEquals">
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">


						<s:url id="BAIEditURL" value="/pages/jsp/user/baiexportsettingsedit.jsp">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a id="BAIEditLink" href="%{BAIEditURL}" targets="inputDiv" button="true" buttonIcon="ui-icon-pencil"
							onClickTopics="beforeLoad"
							onCompleteTopics="completeCompanyLoad"><s:text name="jsp.default_178"/></sj:a>

					</ffi:cinclude>
					
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
					
						<a href="<ffi:getProperty name="SecurePath" />user/baiexportsettingsview.jsp"><img src="/cb/web/multilang/grafx/user/i_view.gif" alt="<s:text name="jsp.default_460"/>" border="0" hspace="3"></a>
					
						<s:url id="CompanyBaiViewURL" value="%{#session.PagesPath}user/baiexportsettingsview.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							id= "editCompanyBAI"
							href="%{CompanyBaiViewURL}"
							targets="DACompanyBaiViewDialogId"
							button="true" 
							buttonIcon="ui-icon-info"
							onClickTopics="beforeLoad"
							onSuccessTopics="openDABaiViewDialog"
							><s:text name="jsp.default_459"/></sj:a>
					</ffi:cinclude>
					
				</ffi:cinclude>
				<ffi:cinclude value1="AddBAIExportSettingsToDA" value2="" operator="equals">
					<a href="<ffi:getProperty name="SecurePath" />user/baiexportsettingsedit.jsp"><img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="<s:text name="jsp.default_179"/>" border="0" hspace="3"></a>
				</ffi:cinclude>
			</ffi:cinclude> --%>
	<!-- 	</td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" alt="" height="1" width="10%" border="0"></td>
	</tr> -->
				<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData formTablePadding10">
					<tr>
						<td width="50%" align="">
							<span width="50%" class="sectLabel <ffi:getPendingStyle fieldname="senderIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
								<s:text name="jsp.user_295"/>:
							</span>
							<span width="50%" class="sectValue columndata <ffi:getPendingStyle fieldname="SenderIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline">
								<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="" operator="notEquals">
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_358"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="AddBAIExportSettingsToDA" property="SenderIDCustom"/>
									</ffi:cinclude>
									<br />
								</ffi:cinclude>
								<span class="<ffi:getPendingStyle fieldname="senderIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
									<ffi:cinclude value1="${GetBAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_358"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetBAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="GetBAIExportSettings" property="SenderIDCustom"/>
									</ffi:cinclude>
								</span>
							</span>
						</td>
						<td width="50%" align="">
							<span class="sectLabel <ffi:getPendingStyle fieldname="receiverIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
								<s:text name="jsp.user_270"/>:
							</span>
							<span class="sectValue columndata" valign="baseline">
								<ffi:cinclude value1="${AddBAIExportSettingsToDA.ReceiverIDType}" value2="" operator="notEquals">
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_358"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="AddBAIExportSettingsToDA" property="ReceiverIDCustom"/>
									</ffi:cinclude>
									<br />
								</ffi:cinclude>
								<span class="<ffi:getPendingStyle fieldname="receiverIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
									<ffi:cinclude value1="${GetBAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_358"/>
											<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetBAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="GetBAIExportSettings" property="ReceiverIDCustom"/>
									</ffi:cinclude>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<td width="50%" align="">
							<span class="sectLabel <ffi:getPendingStyle fieldname="ultimateReceiverIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
								<s:text name="jsp.user_348"/>:
							</span>
							<span class="sectValue columndata" valign="baseline">
								<ffi:cinclude value1="${AddBAIExportSettingsToDA.UltimateReceiverIDType}" value2="" operator="notEquals">
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_357"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="AddBAIExportSettingsToDA" property="UltimateReceiverIDCustom"/>
									</ffi:cinclude>
									<br />
								</ffi:cinclude>
								<span class="<ffi:getPendingStyle fieldname="ultimateReceiverIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
									<ffi:cinclude value1="${GetBAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_358"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetBAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="GetBAIExportSettings" property="UltimateReceiverIDCustom"/>
									</ffi:cinclude>
								</span>
							</span>
						<td width="50%" align="">
							<span class="sectLabel <ffi:getPendingStyle fieldname="originatorIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
								<s:text name="jsp.user_232"/>:
							</span>
							<span class="sectValue columndata" valign="baseline">
								<ffi:cinclude value1="${AddBAIExportSettingsToDA.OriginatorIDType}" value2="" operator="notEquals">
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_355"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="AddBAIExportSettingsToDA" property="OriginatorIDCustom"/>
									</ffi:cinclude>
									<br/>
								</ffi:cinclude>
								<span class="<ffi:getPendingStyle fieldname="originatorIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
									<ffi:cinclude value1="${GetBAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_355"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetBAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
										<ffi:getProperty name="GetBAIExportSettings" property="OriginatorIDCustom"/>
									</ffi:cinclude>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<span class="sectLabel <ffi:getPendingStyle fieldname="customerAccountNumberType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
								<s:text name="jsp.user_100"/>:
							</span>
							<span class="sectValue columndata" valign="baseline">
								<ffi:cinclude value1="${AddBAIExportSettingsToDA.CustomerAccountNumberType}" value2="" operator="notEquals">
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
										<s:text name="jsp.user_356"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${AddBAIExportSettingsToDA.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_352"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
											<s:text name="jsp.user_43"/>
									</ffi:cinclude>
									<br />
								</ffi:cinclude>
								<span class="<ffi:getPendingStyle fieldname="customerAccountNumberType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
									<ffi:cinclude value1="${GetBAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
										<s:text name="jsp.user_356"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetBAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">
										<s:text name="jsp.user_353"/>
										<ffi:getProperty name="TempBankIdentifierDisplayText"/>
										<s:text name="jsp.user_42"/>
									</ffi:cinclude>
								</span>
							</span>
						</td>
					</tr>
				</table>
</div>