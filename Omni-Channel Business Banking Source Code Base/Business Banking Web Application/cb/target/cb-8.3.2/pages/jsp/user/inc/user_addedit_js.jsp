<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

    <script type="text/javascript">
    <!--
    <%--
    /*-
     * This function toggles the display of the Permissions section of the page. The Permissions section should only
     * be visible when if the "control" input is set to "Yes".
     *
     * @param form the form associated with the "control" input
     * @param inputName the name of the "control" input
     */
    --%>
    function togglePermissionsBlock(form, inputName) {
        for (i = 0; i < form.elements[inputName].length; i++) {
            if (form.elements[inputName][i].checked) {
                if (form.elements[inputName][i].value == "TRUE") {
                    document.getElementById("PermissionsBlock").style.display="block";
                } else {
                    document.getElementById("PermissionsBlock").style.display="none";
                }
            }
        }
    }

    <%--
    /*-
     * This function clears the limit values and disables the limit fields for a given row in the "Permissions" table if
     * the leading checkbox is being unchecked. Conversely, if the leading checkbox is being checked then the limit
     * fields are enabled. The row is identified by the name of the leading checkbox which is specified via the
     * baseName parameter.
     *
     * @param form the form associated with the leading checkbox input
     * @param baseName the name of the leading checkbox input
     */
    --%>
    function clearPermissionsRow(form, baseName) {
        if (form.elements[baseName].checked) {
            for (i = 0; i < form.elements.length; i++) {
                if ((form.elements[i].name.indexOf(baseName) == 0) && (form.elements[i].type.toLowerCase() == "text")) {
                    form.elements[i].value = "";
                    form.elements[i].disabled = false;
                }
            }
        } else {
            for (i = 0; i < form.elements.length; i++) {
                if ((form.elements[i].name.indexOf(baseName) == 0) && (form.elements[i].type.toLowerCase() == "text")) {
                    form.elements[i].value = "";
                    form.elements[i].disabled = true;
                }
            }
        }
    }

    function clearPermissionsRow2(form, baseName, limitBaseName) {
        if (form.elements[baseName].checked) {
            for (i = 0; i < form.elements.length; i++) {
                if ((form.elements[i].name.indexOf(limitBaseName) == 0) && (form.elements[i].type.toLowerCase() == "text")) {
                    form.elements[i].value = "";
                    form.elements[i].disabled = false;
                }
            }
        } else {
            for (i = 0; i < form.elements.length; i++) {
                if ((form.elements[i].name.indexOf(limitBaseName) == 0) && (form.elements[i].type.toLowerCase() == "text")) {
                    form.elements[i].value = "";
                    form.elements[i].disabled = true;
                }
            }
        }
    }
    //-->
    </script>
