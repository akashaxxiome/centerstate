<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadmingroupedit" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_143')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_143"/></span>

<script language="javascript">
$(function(){
	$("#divisionSelectId").selectmenu({width: 150});
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#editGroupFormId").setFocus();
	}
});
</script>

<%
	String CancelReload = request.getParameter("CancelReload");
	if(CancelReload!= null){
		session.setAttribute("CancelReload", CancelReload);
	}
	
	String AdminsSelected = request.getParameter("AdminsSelected");
	if(AdminsSelected!= null){
		session.setAttribute("AdminsSelected", AdminsSelected);
	}
	
	String AutoEntitleAdministrators = request.getParameter("AutoEntitleAdministrators");
	if(AutoEntitleAdministrators!= null){
		session.setAttribute("AutoEntitleAdministrators", AutoEntitleAdministrators);
	}
	
	String EditGroup_GroupName = request.getParameter("EditGroup_GroupName");
	if(EditGroup_GroupName!= null){
		session.setAttribute("EditGroup_GroupName", EditGroup_GroupName);
	}
	
	String EditGroup_GroupId = request.getParameter("EditGroup_GroupId");
	if(EditGroup_GroupId!= null){
		session.setAttribute("EditGroup_GroupId", EditGroup_GroupId);
	}
	
	String EditGroup_Supervisor = request.getParameter("EditGroup_Supervisor");
	if(EditGroup_Supervisor!= null){
		session.setAttribute("EditGroup_Supervisor", EditGroup_Supervisor);
	}
	
	String EditGroup_DivisionName = request.getParameter("EditGroup_DivisionName");
	if(EditGroup_DivisionName!= null){
		session.setAttribute("EditGroup_DivisionName", EditGroup_DivisionName);
	}
%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
// The user has pressed cancel, so go back to the previous version of the collections
session.setAttribute( "NonAdminEmployees", session.getAttribute("NonAdminEmployeesCopy"));
session.setAttribute( "AdminEmployees", session.getAttribute("AdminEmployeesCopy"));
session.setAttribute( "NonAdminGroups", session.getAttribute("NonAdminGroupsCopy"));
session.setAttribute( "AdminGroups", session.getAttribute("AdminGroupsCopy"));
session.setAttribute( "adminMembers", session.getAttribute("adminMembersCopy"));
session.setAttribute( "userMembers", session.getAttribute("userMembersCopy"));
session.setAttribute( "groupIds", session.getAttribute("groupIdsCopy"));
session.setAttribute( "tempAdminEmps", session.getAttribute("tempAdminEmpsCopy") );
session.setAttribute( "GroupDivAdminEdited", session.getAttribute("GroupDivAdminEditedCopy"));
%>
</ffi:cinclude>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="CancelReload"/>


<%-- update Entitlement_EntitlementGroups in session --%>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<%-- put Entitlement_EntitlementGroups into the session --%>
<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="BusinessEmployee"/>
<ffi:process name="GetGroupsAdministeredBy"/>
<ffi:removeProperty name="GetGroupsAdministeredBy" />

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
	<ffi:cinclude value1="${ErrorOccured}" value2="TRUE" operator="notEquals">
		<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
		<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
		<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
		<ffi:process name="GetEntitlementGroup"/>
		<ffi:removeProperty name="GetEntitlementGroup" />
		
		<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
		   <ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${EditGroup_GroupName}"/>
		   <ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value="${EditGroup_Supervisor}"/>
		   <ffi:setProperty name="EditBusinessGroup" property="ParentGroupName" value="${EditGroup_DivisionName}"/>
		   <ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>

		<ffi:removeProperty name="GroupDivAdminEdited"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:removeProperty name="ErrorOccured"/>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- if division size is not 0 and this secure user is entitled to adminstrator this group
     enable the addgroup button --%>
<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
<ffi:setProperty name='PageText' value='<a href="${SecurePath}user/corpadmingroupadd.jsp"><img src="/cb/cb/${ImgExt}grafx/user/i_addgroup.gif" alt="" width="67" height="16" border="0"></a>'/>
</ffi:cinclude>
</ffi:cinclude>

<%-- if division size is not 0 or this secure user is entitled to adminstrator this group
     disable the addgroup button --%>
<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
<ffi:setProperty name='PageText' value=''/>
</ffi:cinclude>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
<ffi:setProperty name='PageText' value=''/>
</ffi:cinclude>

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<%-- save the group name - if we are coming back from admin edit, we need the possibly modified group name, not the one from the DA object --%>
<ffi:setProperty name="saveGroupName" value="${EditBusinessGroup.groupName}"/>

<!-- Dual Approval for profile starts-->
<%-- kaijies: FFI80 SP2 Iteration2 close DA feature --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<%-- CR 627854 - Work around for group name caching issue starts--%>
	<ffi:setProperty name="Entitlement_EntitlementGroup" property="GroupName" value="${EditGroup_GroupName}"/>
	
	<%-- CR 627854 - Work around for group name caching issue ends--%>
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>

	<ffi:object id="DAEntitlementGroupObject" name="com.ffusion.csil.beans.entitlements.EntitlementGroup" />

	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="DAEntitlementGroupObject"/>
	<ffi:process name="PopulateObjectWithDAValues"/>

	<ffi:cinclude value1="${DAEntitlementGroupObject.GroupName}" value2="" operator="notEquals">
		<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${DAEntitlementGroupObject.GroupName}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${DAEntitlementGroupObject.ParentId}" value2="0" operator="notEquals">
   	    <ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${DAEntitlementGroupObject.ParentId}"/>
    </ffi:cinclude>

	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="DAEntitlementGroupObject"/>
</ffi:cinclude>
<!-- Dual Approval ends -->
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="equals">
	<ffi:setProperty name="EditBusinessGroup" property="groupName" value="${saveGroupName}"/>
</ffi:cinclude>
<ffi:removeProperty name="saveGroupName"/>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:setProperty name="CanDeleteEntitlementGroup" property="SuccessURL" value="${SecurePath}user/corpadmingroupdelete-verify.jsp"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupedit.jsp?ErrorOccured=TRUE" URLEncrypt="true"/>

<%-- put BusinessEmployee in session, which is the current user --%>
<ffi:cinclude value1="${BusinessEmployee}" value2="" operator="equals">
	<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
	<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
	<ffi:process name="GetBusinessEmployee"/>
	<ffi:removeProperty name="GetBusinessEmployee"/>
</ffi:cinclude>

<%-- see if there are any divisions this user can admin --%>
<ffi:setProperty name="ShowDivisionSelect" value="false"/>
<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
	<ffi:cinclude value1="${EntGroupItem.EntGroupType}" value2="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>" operator="equals">
		<ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${EditBusinessGroup.ParentGroupId}" operator="equals">
			<ffi:setProperty name="ShowDivisionSelect" value="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:list>

<% session.setAttribute("FFIEditBusinessGroup", session.getAttribute("EditBusinessGroup")); %>


		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<s:form id="editGroupFormId" namespace="/pages/user" action="editBusinessGroup-verify" theme="simple" name="groupEditForm" method="post">
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<input type="hidden" name="EditBusinessGroup.CheckboxesAvailable" value="false">
								<input type="hidden" name="EditBusinessGroup.SessionGroupName" value="Entitlement_EntitlementGroup"/>
							
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td width="40">&nbsp;</td>
									<td width="100"><s:text name="jsp.default_226"/>:<span class="required">*</span></td>
									<td>
										<input class="ui-widget-content ui-corner-all" type="text" name="GroupName" style="width:142px;" value="<ffi:getProperty name="EditBusinessGroup" property="GroupName"/>" size="24" maxlength="150" border="0">
										<input class="ui-widget-content ui-corner-all" type="text" name="nonDisplayGroupName" size="24" maxlength="255" border="0" value="" style="display:none">
										<span class="sectionhead_greyDA">
											<ffi:getProperty name="oldDAObject" property="GroupName"/>
										</span>
										 (<ffi:getProperty name="Business" property="BusinessName"/>) &nbsp;&nbsp; <span id="GroupNameError"></span>
									</td>
								</tr>
								<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
									<input type="hidden" name="ParentGroupId" value="<ffi:getProperty name="Entitlement_EntitlementGroup" property="ParentId"/>"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
								<tr>
									<td width="40">&nbsp;</td>
									<td><s:text name="jsp.default_174"/>:<span class="required">*</span></td>
									<td>
										<ffi:cinclude value1="${ShowDivisionSelect}" value2="true" operator="equals">
											<select id="divisionSelectId" class="txtbox" name="ParentGroupId">
												<option value="-1"><s:text name="jsp.default_375"/></option>

												<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
													<ffi:cinclude value1="${EntGroupItem.EntGroupType}" value2="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>" operator="equals">
													<ffi:setProperty name="LastRequest" property="SelectValue" value="${EntGroupItem.GroupId}"/>
													<option value='<ffi:getProperty name="EntGroupItem" property="GroupId"/>' <ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${EditBusinessGroup.ParentGroupId}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="EntGroupItem" property="GroupName"/></option>
													</ffi:cinclude>
												</ffi:list>
											</select>
											<span id="ParentGroupIdError"></span>
										</ffi:cinclude>
										<ffi:cinclude value1="${ShowDivisionSelect}" value2="true" operator="notEquals">
											<span class="columndata"><ffi:getProperty name="EditBusinessGroup" property="ParentGroupName"/></span>
										</ffi:cinclude>
										<span class="sectionhead_greyDA">
											<br>
											<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
												<ffi:cinclude value1="${EntGroupItem.GroupId}" value2="${oldDAObject.ParentId}">
													<ffi:getProperty name="EntGroupItem" property="GroupName"/>
												</ffi:cinclude>
											</ffi:list>
										</span>
									</td>
								</tr>
								</ffi:cinclude>
								<%-- kaijies: FFI80 SP2 Iteration2 close DA feature --%>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"> 


									<ffi:removeProperty name="CATEGORY_BEAN"/>
									<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

									<%-- based on the business employee object, get the business employees for the business --%>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
									<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
									<ffi:process name="GetBusinessEmployees"/>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:removeProperty name="TempBusinessEmployee"/>

									<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>"/>
										<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
									<ffi:process name="GetDACategoryDetails"/>

									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
										<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
										<ffi:process name="GenerateUserAndAdminLists"/>
										<ffi:removeProperty name="adminIds"/>
										<ffi:removeProperty name="userIds"/>
										<ffi:removeProperty name="GenerateUserAndAdminLists"/>

										<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
										<ffi:process name="GenerateListsFromDAAdministrators"/>
									</ffi:cinclude>

									<ffi:object id="GenerateUserAdminList" name="com.ffusion.tasks.dualapproval.GenerateUserAdminList"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminEmployees" value="AdminEmployees"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminGroups" value="AdminGroups"/>
									<ffi:setProperty name="GenerateUserAdminList" property="sessionName" value="NewAdminBusinessEmployees"/>
									<ffi:process name="GenerateUserAdminList"/>

									<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
									   <ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="NewAdminBusinessEmployees" />
									   <ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
									<ffi:process name="FilterNotEntitledEmployees"/>

									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="NewAdminBusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="NewAdminUserStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>

									<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="OldAdminEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="OldAdminGroups"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="tempUserName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="tempGroupName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
									<ffi:process name="GenerateUserAndAdminLists"/>
									<ffi:removeProperty name="adminIds"/>
									<ffi:removeProperty name="tempUserName"/>
									<ffi:removeProperty name="tempGroupName"/>
									<ffi:removeProperty name="userIds"/>
									<ffi:removeProperty name="GenerateUserAndAdminLists"/>

									<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
									<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
									  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
									<ffi:process name="GetBusinessEmployeesByEntGroups"/>
									<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
									<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
										<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
									<ffi:process name="FilterNotEntitledEmployees"/>

									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="OldAdminStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>

									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GetDACategoryDetails"/>
									<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
									<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
									<ffi:removeProperty name="NewAdminStringList"/>
									<ffi:removeProperty name="BusinessEmployees"/>
									<ffi:removeProperty name="NewAdminBusinessEmployees"/>
								</ffi:cinclude>
								<!-- Dual Approval for administrator ends -->

								<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
								<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
								  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
								<ffi:process name="GetBusinessEmployeesByEntGroups"/>
								<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
								<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
									<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
								<ffi:process name="FilterNotEntitledEmployees"/>
								<tr>
									<td width="40">&nbsp;</td>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
										<td class="adminBackground sectionsubhead" width="120">
												<s:text name="jsp.user_38"/>:&nbsp;
										</td>
									</ffi:cinclude>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<td class="adminBackground sectionheadDA" align="right" valign="baseline">
												<s:text name="jsp.user_38"/>:&nbsp;
										</td>
									</ffi:cinclude>
									<td  valign="top" align="left">
										<div style="display:inline-block; " class="alignItemsDivision">
											<%--display name of each employee in BusinessEmployees--%>
		 							<%
	 							       		boolean isFirst= true;
 										String temp = "";
	 							       	%>
 									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
										 <ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
											<ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="tempAdminEmps" />
											<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.GROUP_MANAGEMENT%>" />
										 <ffi:process name="FilterNotEntitledEmployees"/>

	 									<ffi:setProperty name="tempAdminEmps" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
										<ffi:list collection="tempAdminEmps" items="emp">
			 							<%--display separator if this is not the first one--%>
										<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
											<ffi:setProperty name="empName" value="${emp.Name}"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
											<ffi:setProperty name="empName" value="${emp.LastName}"/>
										</ffi:cinclude>
 										<%
 										if( isFirst ) {
	 									isFirst = false;
		 								temp = temp + session.getAttribute( "empName" );
			 							} else {
 										temp = temp + ", " + session.getAttribute( "empName" );
 										}
	 									%>
 										</ffi:list>
										<ffi:setProperty name="modifiedAdmins" value="TRUE"/>
	 								</ffi:cinclude>
									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
	 									<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
										<ffi:list collection="BusinessEmployees" items="emp">
	 									<%--display separator if this is not the first one--%>
										<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
											<ffi:setProperty name="empName" value="${emp.Name}"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
											<ffi:setProperty name="empName" value="${emp.LastName}"/>
										</ffi:cinclude>
 										<%
 										if( isFirst ) {
 											isFirst = false;
	 										temp = temp + session.getAttribute( "empName" );
 										} else {
 											temp = temp + ", " + session.getAttribute( "empName" );
	 									}
		 								%>
 										</ffi:list>
										<ffi:setProperty name="modifiedAdmins" value="FALSE"/>
									</ffi:cinclude>
									<%-- <ffi:removeProperty name="AdminsSelected"/> --%>
									<%
	 								if( temp.length() > 120 ) {
 										temp = temp.substring( 0, 120 );
 										int index = temp.lastIndexOf( ',' );
										temp = temp.substring( 0, index ) + "...";
 									}
 									session.setAttribute( "AdministratorStr", temp );%>
 									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
		 								<ffi:getProperty name="AdministratorStr"/>
										<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
											<s:text name="jsp.default_296"/>
										</ffi:cinclude>
									</ffi:cinclude>
	 								<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	 									<ffi:getProperty name="NewAdminUserStringList"/>
	 								</ffi:cinclude>
									<ffi:removeProperty name="empName"/>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<span class="sectionhead_greyDA">
												<br>
												<ffi:getProperty name="OldAdminStringList"/>
										</span>
									</ffi:cinclude>
								</div>
									&nbsp;&nbsp;
									<s:hidden id="needVerify_HiddenFieldId" name="needVerify" value="true"/>
									<%-- Kaijies: add administrator icon button --%>
									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
											<sj:a
												id="editGroupAdminButtonId"
												button="true"
												targets="editAdminerDialogId"
												formIds="editGroupFormId"
												formid="editGroupFormId"
												validate="true"
												validateFunction="customValidation"
												onBeforeTopics="beforeVerify2"
												field1="needVerify_HiddenFieldId"
												value1="false"
												postaction="/cb/pages/user/editBusinessGroup-editGroupModifyAdminerBeforeChange.action"
												onClickTopics="postFormTopic"
												onCompleteTopics="openGroupAdminerDialog"
											>
												<s:text name="jsp.default_178" />
											</sj:a>
									</ffi:cinclude>
									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
											<sj:a
												id="editGroupAdminButtonId"
												button="true"
												targets="editAdminerDialogId"
												formIds="editGroupFormId"												
												formid="editGroupFormId"
												validate="true"
												validateFunction="customValidation"
												onBeforeTopics="beforeVerify2"
												field1="needVerify_HiddenFieldId"
												value1="false"
												postaction="/cb/pages/user/editBusinessGroup-editGroupModifyAdminerAfterChange.action"																								
												onClickTopics="postFormTopic"
												onCompleteTopics="openGroupAdminerDialog"
											>
												<s:text name="jsp.default_178" />
											</sj:a>
									</ffi:cinclude>
									
									<ffi:removeProperty name="AdminsSelected"/>
										
								</td>
								</tr>
								<tr>
									<td colspan="3">
										<div align="center">
												<span class="required">* <s:text name="jsp.default_240"/></span>
												<br><br>
												
												<s:url id="resetEditGroupButtonUrl" value="/pages/jsp/user/corpadmingroupedit.jsp">  
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
												</s:url>	
												<sj:a id="resetEditGroupBtn"
	                                               href="%{resetEditGroupButtonUrl}"
												   targets="inputDiv"
												   button="true"><s:text name="jsp.default_358"/></sj:a>
												<sj:a
												   button="true"
												   summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelGroupForm"><s:text name="jsp.default_82"/></sj:a>
												<sj:a
													formIds="editGroupFormId"
													targets="verifyDiv"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify2"
													onCompleteTopics="completeVerify2"
													onErrorTopics="errorVerify"
													onSuccessTopics="successVerify"
													formid="editGroupFormId"
													field1="needVerify_HiddenFieldId"
													value1="true"												
													postaction="/cb/pages/user/editBusinessGroup-verify.action"
													onClickTopics="postFormTopic"
													><s:text name="jsp.default_366"/></sj:a>
													
											</div>
									</td>
								</tr>
							</table>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
<ffi:setProperty name="onCancelGoto" value="corpadmingroupedit.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmingroupedit.jsp"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupedit.jsp?UseLastRequest=TRUE&AdminsSelected=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroupedit.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
</ffi:cinclude>

<ffi:removeProperty name="OrigBusinessEmployee"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="NewAdminUserStringList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="ShowDivisionSelect"/>
