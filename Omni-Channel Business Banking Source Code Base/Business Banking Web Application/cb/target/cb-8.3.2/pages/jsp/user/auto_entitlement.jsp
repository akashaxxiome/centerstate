<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:help id="user_auto_entitlement" className="moduleHelpClass"/>	
				<TABLE align="center" width="100%">
					<TR>
						<TD align="center">
								<s:form id="autoEntitlementForm" namespace="/pages/user" name="autoEntitlementForm" action="setAdministrators-execute" validate="false" method="post" theme="simple">
		                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<TABLE width="98%" border="0" cellspacing="0" cellpadding="3">									
									<TR>
										<TD colspan="2" style="text-align:center" nowrap><span class="columndata"><ffi:getProperty name="autoEntitleConfirmMsg"/></span></TD>
									</TR>
									<TR>
										<TD colspan="2" style="text-align:center" nowrap><span class="columndata"><ffi:getProperty name="autoEntitleConfirmMsg2"/></span></TD>
									</TR>

									<TR>
										<TD style="text-align:right"><input type="radio" name="AutoEntitleAdministrators" value="true" checked="checked"/></TD>
										<TD class="columndata"><s:text name="jsp.default_467"/></TD>
									</TR>
									<TR>
										<TD style="text-align:right"><input type="radio" name="AutoEntitleAdministrators" value="false"/></TD>
										<TD class="columndata"><s:text name="jsp.default_295"/></TD>
									</TR>

							    		<TR>
										<TD colspan="2" align="center">
											<!--<input class="submitbutton" type="button" value="<s:text name="jsp.default_57"/>" onclick="document.location='<ffi:getProperty name="autoEntitleBackURL"/>';">-->
											<!--<input class="submitbutton" type="button" value="<s:text name="jsp.default_82"/>" onclick="document.location='<ffi:getProperty name="autoEntitleCancel"/>';">-->
											
											<sj:a   button="true" 
												onClickTopics="closeDialog"
												><s:text name="jsp.default_82"/></sj:a>
											<!--<input class="submitbutton" type="submit" value="<s:text name="jsp.default_366"/>">-->
											<sj:a id="autoEntitlementContinueButton"
												formIds="autoEntitlementForm"
												targets="confirmDiv" 
												button="true" 
												validateFunction="customValidation"
												onBeforeTopics="beforeVerify"
												onCompleteTopics="closeDialog"
												onErrorTopics="errorVerify"
												onSuccessTopics="completeSubmit"
												><s:text name="jsp.default_111"/></sj:a>
										</TD>
									</TR>
								 </TABLE>
					   		 </s:form>		    
						</TD>
					</TR>			
				</TABLE>