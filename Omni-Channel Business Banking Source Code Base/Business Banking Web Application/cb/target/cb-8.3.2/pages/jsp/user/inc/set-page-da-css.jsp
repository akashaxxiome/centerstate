<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script>
<%-- Set Top level CSS for DA mode, highlight those categories which have pending approval changes --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:list collection="categories" items="category">
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="equals">
				<ffi:setProperty name="hasAccountAccess" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="equals">
				<ffi:setProperty name="hasAccountGroupAccess" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>" operator="equals">
				<ffi:setProperty name="hasCrossAchCompanyAccess" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>">
				<ffi:setProperty name="hasPerAchCompanyAccess" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_PER_LOCATION %>">
				<ffi:setProperty name="hasPerLocationAccess" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_WIRE_TEMPLATE_ACCESS_LIMITS %>">
				<ffi:setProperty name="hasWireTemplateAccess" value="true"/>
			</ffi:cinclude>
		</ffi:list>
		
		<ffi:cinclude value1="${hasAccountAccess}" value2="true" operator="equals">
			$("#acctAccessTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasAccountAccess}" value2="true" operator="notEquals">
			$("#acctAccessTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasAccountAccess"/>
		
		<ffi:cinclude value1="${hasAccountGroupAccess}" value2="true" operator="equals">
			$("#acctGroupAccessTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasAccountGroupAccess}" value2="true" operator="notEquals">
			$("#acctGroupAccessTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasAccountGroupAccess"/>
		
		<ffi:cinclude value1="${hasCrossAchCompanyAccess}" value2="true" operator="equals">
			$("#xACHCompanyTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasCrossAchCompanyAccess}" value2="true" operator="notEquals">
			$("#xACHCompanyTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasCrossAchCompanyAccess"/>
		
		<ffi:cinclude value1="${hasPerAchCompanyAccess}" value2="true" operator="equals">
			$("#aCHCompanyTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasPerAchCompanyAccess}" value2="true" operator="notEquals">
			$("#aCHCompanyTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasPerAchCompanyAccess"/>
		
		<ffi:cinclude value1="${hasPerLocationAccess}" value2="true" operator="equals">
			$("#perLocationTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasPerLocationAccess}" value2="true" operator="notEquals">
			$("#perLocationTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasPerLocationAccess"/>
		
		<ffi:cinclude value1="${hasWireTemplateAccess}" value2="true" operator="equals">
			$("#wireTemplateTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasWireTemplateAccess}" value2="true" operator="notEquals">
			$("#wireTemplateTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasWireTemplateAccess"/>
				
		<%--Per Account Group DA Highlight start: --%>
		<%String  catSubType = null; session.setAttribute("CATEGORY_SUB_SESSION_NAME_BACKUP", session.getAttribute( IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME ));%>
		<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountgroupcomponents.jsp" />
			<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
			<ffi:list collection="LocalizedComponentNamesPer" items="currComponentName">
			
					<ffi:list collection="categories" items="perAccountGrpCategory">
							<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
							<ffi:setProperty name="GetDACategorySubType" property="DaItemCategoryName" value="perAccountGrpCategory"/>
							<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${currComponentName}"/>
							<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
											value="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>" />
							<ffi:process name="GetDACategorySubType"/>
							<ffi:removeProperty name="GetDACategorySubType"/>
							<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
							<ffi:cinclude value1="${catSubType}" value2="" operator="notEquals">
								<ffi:setProperty name="highLight" value="true" />
								<% if(catSubType.indexOf("Cash Management") != -1) {%>
									<ffi:setProperty name="hasCashManagement" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Payments & Transfers") != -1) {%>
									<ffi:setProperty name="hasPaymentsTransfers" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Reporting") != -1) {%>
									<ffi:setProperty name="hasReporting" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Other") != -1) {%>
									<ffi:setProperty name="hasOther" value="true"/>
								<%} %>
							</ffi:cinclude>
							<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
							<ffi:removeProperty name="catSubType"/>
					</ffi:list>
		</ffi:list>
		<ffi:cinclude value1="${highLight}" value2="true" operator="equals">
			$("#perAccountGroupTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
			$("#perAccountGroupTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="highLight" />
		
		<ffi:cinclude value1="${hasCashManagement}" value2="true" operator="equals">
			$("#pagtab0 a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasCashManagement}" value2="true" operator="notEquals">
			$("#pagtab0 a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasCashManagement"/>
<%
	int index = 0;
	if (((java.util.ArrayList)session.getAttribute("ComponentNamesPer")).indexOf("Cash Management") != -1) {
		index++;
	}
%>
		<ffi:cinclude value1="${hasPaymentsTransfers}" value2="true" operator="equals">
			$("#pagtab<%=index%> a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasPaymentsTransfers}" value2="true" operator="notEquals">
			$("#pagtab<%=index%> a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasPaymentsTransfers"/>
<%
	if (((java.util.ArrayList)session.getAttribute("ComponentNamesPer")).indexOf("Payments & Transfers") != -1) {
		index++;
	}
%>
		<ffi:cinclude value1="${hasReporting}" value2="true" operator="equals">
			$("#pagtab<%=index%> a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasReporting}" value2="true" operator="notEquals">
			$("#pagtab<%=index%> a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasReporting"/>
<%
	if (((java.util.ArrayList)session.getAttribute("ComponentNamesPer")).indexOf("Reporting") != -1) {
		index++;
	}
%>
		<ffi:cinclude value1="${hasOther}" value2="true" operator="equals">
			$("#pagtab<%=index%> a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasOther}" value2="true" operator="notEquals">
			$("#pagtab<%=index%> a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasOther"/>
		<%session.setAttribute(IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME, session.getAttribute("CATEGORY_SUB_SESSION_NAME_BACKUP"));%>
		<%--Per Account Group DA Highlight end: --%>
		
		<%--Per Account DA Highlight start: --%>
		<ffi:removeProperty name="currComponentName"/>
		<% catSubType = null; session.setAttribute("CATEGORY_SUB_SESSION_NAME_BACKUP", session.getAttribute( IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME ));%>
		<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountcomponents.jsp" />
			<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
			<ffi:list collection="LocalizedComponentNamesPer" items="currComponentName">
			
					<ffi:list collection="categories" items="perAccountCategory">
							<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
							<ffi:setProperty name="GetDACategorySubType" property="DaItemCategoryName" value="perAccountCategory"/>
							<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${currComponentName}"/>
							<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 	 
							     value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_ACCOUNT %>" />
							<ffi:process name="GetDACategorySubType"/>
							<ffi:removeProperty name="GetDACategorySubType"/>
				
							<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
							<ffi:cinclude value1="${catSubType}" value2="" operator="notEquals">
								<ffi:setProperty name="highLight" value="true" />
								<% if(catSubType.indexOf("Cash Management") != -1) {%>
									<ffi:setProperty name="hasCashManagement" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Payments & Transfers") != -1) {%>
									<ffi:setProperty name="hasPaymentsTransfers" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Reporting") != -1) {%>
									<ffi:setProperty name="hasReporting" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Other") != -1) {%>
									<ffi:setProperty name="hasOther" value="true"/>
								<%} %>
							</ffi:cinclude>
							<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
							<ffi:removeProperty name="catSubType"/>
					</ffi:list>
		</ffi:list>
		<ffi:cinclude value1="${highLight}" value2="true" operator="equals">
			$("#perAccountTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
			$("#perAccountTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="highLight" />
		
		<ffi:cinclude value1="${hasCashManagement}" value2="true" operator="equals">
			$("#patab0 a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasCashManagement}" value2="true" operator="notEquals">
			$("#patab0 a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasCashManagement"/>
<%
	index = 0;
	if (((java.util.ArrayList)session.getAttribute("ComponentNamesPer")).indexOf("Cash Management") != -1) {
		index++;
	}
%>
		<ffi:cinclude value1="${hasPaymentsTransfers}" value2="true" operator="equals">
			$("#patab<%=index%> a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasPaymentsTransfers}" value2="true" operator="notEquals">
			$("#patab<%=index%> a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasPaymentsTransfers"/>
<%
	if (((java.util.ArrayList)session.getAttribute("ComponentNamesPer")).indexOf("Payments & Transfers") != -1) {
		index++;
	}
%>
		<ffi:cinclude value1="${hasReporting}" value2="true" operator="equals">
			$("#patab<%=index%> a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasReporting}" value2="true" operator="notEquals">
			$("#patab<%=index%> a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasReporting"/>
<%
	if (((java.util.ArrayList)session.getAttribute("ComponentNamesPer")).indexOf("Reporting") != -1) {
		index++;
	}
%>
		<ffi:cinclude value1="${hasOther}" value2="true" operator="equals">
			$("#patab<%=index%> a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasOther}" value2="true" operator="notEquals">
			$("#patab<%=index%> a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasOther"/>
		<%session.setAttribute(IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME, session.getAttribute("CATEGORY_SUB_SESSION_NAME_BACKUP"));%>
		<%--Per Account DA Highlight end: --%>
		
		<%--Cross Account DA Highlight start: --%>
		<ffi:removeProperty name="currComponentName"/>
		<% catSubType = null; session.setAttribute("CATEGORY_SUB_SESSION_NAME_BACKUP", session.getAttribute( IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME ));%>
		<s:include value="%{#session.PagesPath}user/inc/checkentitled-crossaccountcomponents.jsp" />
			<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
			<ffi:list collection="LocalizedComponentNamesCross" items="currComponentName">
			
					<ffi:list collection="categories" items="crossAccCategory">
							<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
						<ffi:setProperty name="GetDACategorySubType" property="DaItemCategoryName" value="crossAccCategory"/>
						<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${currComponentName}"/>
						<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
						value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>" />
						<ffi:process name="GetDACategorySubType"/>
						<ffi:removeProperty name="GetDACategorySubType"/>
				
							<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
							<ffi:cinclude value1="${catSubType}" value2="" operator="notEquals">
								<ffi:setProperty name="highLight" value="true" />
								<% if(catSubType.indexOf("Account Management") != -1) {%>
									<ffi:setProperty name="hasAccManagement" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Administration") != -1) {%>
									<ffi:setProperty name="hasAdministration" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Cash Management") != -1) {%>
									<ffi:setProperty name="hasCashManagement" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Payments & Transfers") != -1) {%>
									<ffi:setProperty name="hasPaymentsTransfers" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Reporting") != -1) {%>
									<ffi:setProperty name="hasReporting" value="true"/>
								<%} %>
								<% if(catSubType.indexOf("Other") != -1) {%>
									<ffi:setProperty name="hasOther" value="true"/>
								<%} %>
							</ffi:cinclude>
							<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
							<ffi:removeProperty name="catSubType"/>
					</ffi:list>
		</ffi:list>
		<ffi:cinclude value1="${highLight}" value2="true" operator="equals">
			$("#crossAccountTab").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
			$("#crossAccountTab").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="highLight" />
		
		<ffi:cinclude value1="${hasAccManagement}" value2="true" operator="equals">
			$("#accountManagement a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasAccManagement}" value2="true" operator="notEquals">
			$("#accountManagement a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasAccManagement"/>
		
		<ffi:cinclude value1="${hasAdministration}" value2="true" operator="equals">
			$("#administration a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasAdministration}" value2="true" operator="notEquals">
			$("#administration a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasAdministration"/>
		
		<ffi:cinclude value1="${hasCashManagement}" value2="true" operator="equals">
			$("#cashManagement a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasCashManagement}" value2="true" operator="notEquals">
			$("#cashManagement a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasCashManagement"/>
		
		<ffi:cinclude value1="${hasPaymentsTransfers}" value2="true" operator="equals">
			$("#paymentsAndTransfers a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasPaymentsTransfers}" value2="true" operator="notEquals">
			$("#paymentsAndTransfers a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasPaymentsTransfers"/>
		
		<ffi:cinclude value1="${hasReporting}" value2="true" operator="equals">
			$("#reporting a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasReporting}" value2="true" operator="notEquals">
			$("#reporting a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasReporting"/>
		
		<ffi:cinclude value1="${hasOther}" value2="true" operator="equals">
			$("#other a span").addClass("columndataDA");
		</ffi:cinclude>
		<ffi:cinclude value1="${hasOther}" value2="true" operator="notEquals">
			$("#other a span").removeClass("columndataDA");
		</ffi:cinclude>
		<ffi:removeProperty name="hasOther"/>
		<%session.setAttribute(IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME, session.getAttribute("CATEGORY_SUB_SESSION_NAME_BACKUP"));%>
		<%--Cross Account DA Highlight end: --%>
		
	</ffi:cinclude>
</script>