<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<s:url id="remoteProfile" value="/pages/jsp/user/corpadmininfo.jsp"/>
<s:url id="remoteBaiSettings" value="/pages/jsp/user/baiexportsettings.jsp"/>
<s:url id="remoteAdmin" value="/pages/jsp/user/administrators.jsp"/>
<s:url id="remoteAcctConfig" value="/pages/jsp/user/accountconfiguration.jsp"/>
<s:url id="remoteAcctGrps" value="/pages/jsp/user/corpadminaccountgroups_grid.jsp"/>
<s:url id="remoteTranGrps" value="/pages/jsp/user/corpadmintransactiongroups.jsp"/>
<s:url id="remotePerms" value="/pages/jsp/user/corpadmincompanypermissions.jsp"/>


<%-- The following div tempDivIDForMethodNsAdminReloadCompany is used to hold temparory variables of company grids.--%>
<div id="tempDivIDForMethodNsAdminReloadCompany" style="display: none;"></div>

<%-- Pending Approval Company island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<div id="company_PendingApprovalTab"  class="portlet hidden" style="float: left; width: 100%; margin-bottom:10px;">
	    <div class="portlet-header">
			<span class="portlet-title" id="adminCompanyPendingApprovalLink"><s:text name="jsp.default.label.pending.approval.changes"/></span>
		</div>
		<div class="portlet-content">
				<s:include value="/pages/jsp/user/inc/da-business-island.jsp"/>
		</div>
		<div id="transferSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('company_PendingApprovalTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div>
	</div>
	<input value="true" id="isDA" type="hidden">
</ffi:cinclude>
<%-- Pending Approval Company island end --%>
<!-- <div class="clearBoth" style="padding: 10px 0"></div> -->
<div id="companySummaryTabs" class="portlet" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title">
			<ffi:cinclude value1="${summaryToLoad}" value2="BAI"	operator="notEquals">
				<ffi:cinclude value1="${summaryToLoad}" value2="Administrators"	operator="notEquals">
					<ffi:cinclude value1="${summaryToLoad}" value2="AccountConfiguration"	operator="notEquals">
						<ffi:cinclude value1="${summaryToLoad}" value2="AccountGroups"	operator="notEquals">
							<ffi:cinclude value1="${summaryToLoad}" value2="TransactionGroups"	operator="notEquals">
								<ffi:cinclude value1="${summaryToLoad}" value2="CompanyPermissions"	operator="notEquals">
									<s:text name="jsp.default_106"/> <s:text name="jsp.user_404"/>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="BAI"	operator="equals">
				<s:text name="jsp.user_51"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="Administrators"	operator="equals">
				<s:text name="jsp.user_36"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="AccountConfiguration"	operator="equals">
				Account Configuration
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="AccountGroups"	operator="equals">
				Account Group
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="TransactionGroups"	operator="equals">
				<s:text name="jsp.default_575"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="CompanyPermissions"	operator="equals">
				<ffi:setProperty name="EditGroup_GroupId" value="${Business.EntitlementGroupId}"/>
				<ffi:setProperty name="Section" value="Company"/>
				<s:set var="tmpI18nStr" value="%{getText('jsp.reports_526')}" scope="request" />
				<ffi:setProperty name="Context" value="${Business.BusinessName} (${tmpI18nStr})"/>
				<ffi:removeProperty name="BusinessEmployeeId"/>
				<s:text name="jsp.default_179"/> <ffi:getProperty name="Context"/> <s:text name="jsp.default_569"/>
			</ffi:cinclude>
		</span>
	</div>
	<div class="portlet-content" id="companySummaryTabsContent">
		<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
			<ffi:cinclude value1="${summaryToLoad}" value2="BAI"	operator="notEquals">
				<ffi:cinclude value1="${summaryToLoad}" value2="Administrators"	operator="notEquals">
					<ffi:cinclude value1="${summaryToLoad}" value2="AccountConfiguration"	operator="notEquals">
						<ffi:cinclude value1="${summaryToLoad}" value2="AccountGroups"	operator="notEquals">
							<ffi:cinclude value1="${summaryToLoad}" value2="TransactionGroups"	operator="notEquals">
								<ffi:cinclude value1="${summaryToLoad}" value2="CompanyPermissions"	operator="notEquals">
									<s:include value="/pages/jsp/user/corpadmininfo.jsp" />
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="BAI" operator="equals">
				<s:include value="/pages/jsp/user/baiexportsettings.jsp" />
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="Administrators"	operator="equals">
				<s:include value="/pages/jsp/user/administrators.jsp" />
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="AccountConfiguration"	operator="equals">
				<s:include value="/pages/jsp/user/accountconfiguration.jsp" />
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="AccountGroups"	operator="equals">
				<s:include value="/pages/jsp/user/corpadminaccountgroups_grid.jsp" />
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="TransactionGroups"	operator="equals">
				<s:include value="/pages/jsp/user/corpadmintransactiongroups.jsp" />
			</ffi:cinclude>
			<ffi:cinclude value1="${summaryToLoad}" value2="CompanyPermissions"	operator="equals">
				<s:include value="/pages/jsp/user/corpadmincompanypermissions.jsp" />
			</ffi:cinclude>
		</ffi:cinclude>
	</div>
	<div id="companySummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('companySummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">
//Initialize portlet with settings & calendar icon

$(document).ready(function(){
	ns.common.initializePortlet("companySummaryTabs");
	ns.common.initializePortlet("company_PendingApprovalTab");
});		
	
	if(accessibility){
		$("#companyTabs" ).tabs({
			show: function(event,ui){
				$.publish("common.topics.tabifyNotes");
				$("#companyTabs").setFocusOnTab();
			}		
		});
	}
</script>
