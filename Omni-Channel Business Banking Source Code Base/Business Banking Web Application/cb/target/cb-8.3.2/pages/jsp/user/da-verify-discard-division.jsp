<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setL10NProperty name='PageHeading' value='Verify Discard Division'/>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<% session.setAttribute("itemId",request.getParameter("itemId"));%>
<% session.setAttribute("itemType",request.getParameter("itemType"));%>
<% session.setAttribute("rejectFlag",request.getParameter("rejectFlag"));%>

<ffi:object id="DiscardPendingChangeTask" name="com.ffusion.tasks.dualapproval.DiscardPendingChange"  />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemType" value="${itemType}" />
	<ffi:setProperty name="DiscardPendingChangeTask" property="itemId" value="${itemId}" />
<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("DiscardPendingChangeTask") ); %>

<div align="center">
		<table class="adminBackground" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="columndata" align="center">
								<br>
								<s:text name="da.verify.discard.division">
									<s:param>
										<ffi:getProperty name="GroupName"/>
									</s:param>
								</s:text>
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="center">
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>								
								
								<s:url id="divisionDiscardUrl" escapeAmp="false" value="/pages/dualapproval/dualApprovalAction-discardChanges.action?itemId=${itemId}&itemType=${itemType}&module=Division&daAction=Discarded">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
								</s:url>

								<sj:a id="divisionDiscardLink"
									href="%{divisionDiscardUrl}"
									targets="resultmessage"
									button="true"
									title="%{getText('jsp.default_Discard')}"
									onClickTopics=""
									onSuccessTopics="DADivisionDiscardChangesCompleteTopics"
									onCompleteTopics=""
									onErrorTopics="">
									<s:text name="jsp.default_Discard" />
								</sj:a>
							</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
			<p></p>
		</div>

<ffi:removeProperty name="GroupName"/>