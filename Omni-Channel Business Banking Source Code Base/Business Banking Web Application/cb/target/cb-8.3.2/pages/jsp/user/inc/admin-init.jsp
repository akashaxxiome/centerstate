<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>


<%-- PUTTING TASKS IN SESSION NEEDED BY OTHER PAGES - previously done by user/index.jsp --%>
	<ffi:setProperty name="admin_init_touched" value="true"/>
	<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
	<ffi:object id="GetAccountsForUser" name="com.ffusion.tasks.admin.GetAccountsForUser" scope="session"/>
	<ffi:object id="GetCumulativeLimits" name="com.ffusion.efs.tasks.entitlements.GetCumulativeLimits" scope="session"/>
	<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
	<ffi:object id="CanDeleteEntitlementGroup" name="com.ffusion.tasks.admin.CanDeleteEntitlementGroup" scope="session"/>
		<ffi:setProperty name="CanDeleteEntitlementGroup" property="SuccessURL" value="${SecureServletPath}DeleteEntitlementGroup"/>
	<ffi:object id="DeleteBusinessGroup" name="com.ffusion.tasks.admin.DeleteBusinessGroup" scope="session"/>
	<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
	<ffi:object id="CheckEntitlementByGroup" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup" scope="session"/>
	<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
	<ffi:object name="com.ffusion.tasks.admin.GetEntitlementObjectID" id="GetEntitlementObjectID" scope="session"/>

<%-- used by js to determine if we should call functions to display DA grids --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<script>ns.admin.dualApprovalMode = "TRUE";</script>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<script>ns.admin.dualApprovalMode = "FALSE";</script>
</ffi:cinclude>

