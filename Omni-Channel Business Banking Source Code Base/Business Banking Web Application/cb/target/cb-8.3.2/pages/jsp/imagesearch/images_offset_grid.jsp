<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:setGridURL grid="GRID_accountOffsetImages" name="ViewURL" url="/cb/pages/jsp/account/GetImagesAction_viewImage.action?module=DEPOSIT&imageID={0}" parm0="ImageHandle"/>
	<ffi:setGridURL grid="GRID_accountOffsetImages" name="ViewOffsetURL" url="/cb/pages/jsp/account/GetImagesAction_viewImage.action?module=DEPOSIT&imageID={0}" parm0="ImageHandle"/>
		
	<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetOffsetImagesAction.action?GridURLs=GRID_accountOffsetImages" URLEncrypt="true"/>
										
	<s:url id="OffsetImagesURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<div id="accountOffsetImagesPortlet" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<ffi:help id="account_imageSearch_offsetImages" />	
	     <div class="portlet-header ui-widget-header ui-corner-all">
        	<span class="portlet-title"><s:text name="jsp.imageSearch.accountOffsetImages.header"/></span>
		</div>
		<div class="portlet-content">
		
	<sjg:grid  
		id="accountOffsetSummaryID"  
		sortable="true" 
		dataType="json"  
		href="%{OffsetImagesURL}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		navigator="true"
	    navigatorAdd="false"
	    navigatorDelete="false"
	    navigatorEdit="false"
	    navigatorRefresh="false"
	    navigatorSearch="false"
	    navigatorView="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		onGridCompleteTopics="addGridControlsEvents,offsetImagesGridOnComplete"
		multiselect = "true"
		multiboxonly = "true"
		viewrecords="true"
		> 
		
		<!--  TODO: implementation of i18n -->
		<sjg:gridColumn name="imageHandle" index="imageHandle" title="%{getText('jsp.imageSearch.gridColumnHeader.imageHandle')}" sortable="false" width="70" hidden="true" hidedlg="true"/> 
		<sjg:gridColumn name="postingDate" index="posting_date" title="%{getText('jsp.imageSearch.gridColumnHeader.postingDate')}" sortable="true" width="65"/>
		<sjg:gridColumn name="drCr" index="dr_cr" title="%{getText('jsp.imageSearch.gridColumnHeader.DR_CR')}" sortable="true" width="65"/>		
		<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="80"/>
		<sjg:gridColumn name="maskedAccountID" index="account_id" title="%{getText('jsp.default_15')}" sortable="true" width="80"/>
		<sjg:gridColumn name="checkNumber" index="check_number" title="%{getText('jsp.default_92')}" sortable="true" width="80"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_27')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn"   formatter="ns.imagesearch.offsetImageGridLinkFormatter" width="50"/>
	</sjg:grid>
	
	<div id="offsetDetailsMenu" class="portletHeaderMenuContainer" style="display:none;">
		<ul class="portletHeaderMenu">
			<li><a href='#' onclick="ns.common.showDetailsHelp('accountOffsetImagesPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
		</ul>
	</div> 

</div>
 <div class="marginTop10" id="goBackToImgSrcBtnDiv" align="center" >
	 <sj:a id="goBackToImgSrcBtn" button="true" onBeforeTopics="beforeImageSearch" onClickTopics="backToArchiveImagesGrid">
		<s:text name="jsp.default_57"/>
	</sj:a>
	<span class="errorLabel imgSrcErrorMsg"></span>
</div>
</div>
	<script>
		ns.common.initializePortlet('accountOffsetImagesPortlet');
	
		/* $('#accountOffsetImagesPortlet').portlet({
		close: false,
		bookmark: false,
		helpCallback: function(){
			var helpFile = $('#accountOffsetImagesPortlet').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		closeCallback: function(){
			$('#details').slideUp();
			$('#billpayGridTabs').portlet('expand');
			$('#billpayTempGridTabs').portlet('expand');
			$('#billpayPayeeGridTabs').portlet('expand');
		},
		bookMarkCallback: function(){
			var settingsArray = ns.shortcut.getShortcutSettingWithPortlet( '#accountOffsetImagesPortlet' );
			var path = settingsArray[0];
			var ent  = settingsArray[1];
			ns.shortcut.openShortcutWindow( path, ent );
		}
		}); */
	</script>
