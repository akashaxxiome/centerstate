<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- <div id="appdashboard-left" class="formLayout">
	<div id="imageSearchControlBox">
	   	<sj:a 	id="imageSearchButton"	button="true" buttonIcon="ui-icon-search"	>&nbsp;<s:text name="jsp.default_4"/></sj:a>
		<sj:a 	id="viewCheckedLink"	button="true" buttonIcon="ui-icon-info"	>&nbsp;<s:text name="jsp.imageSearch.button.viewChecked"/></sj:a>
	</div>
	
	<div id="pendingImagesControlBox">
		<sj:a 	id="viewPendingImages"	button="true" buttonIcon="ui-icon-info"	>&nbsp;<s:text name="jsp.imageSearch.button.viewChecked"/></sj:a>	
		<sj:a 	id="deletePendingImages"	button="true" buttonIcon="ui-icon-trash"	>&nbsp;<s:text name="jsp.imageSearch.button.deleteChecked"/></sj:a>
	</div>
</div>

<div id="appdashboard-right" >
	<div style="float:right;">	
   		<sj:a 	id="pendingImagesButton"	button="true" buttonIcon="ui-icon-info">&nbsp;<s:text name="jsp.imageSearch.button.reqImages"/></sj:a>		
	</div>	
</div>
<span class="ui-helper-clearfix">&nbsp;</span>	
<div id="imageSeachBox">
	<s:include value="/pages/jsp/imagesearch/image_search_form.jsp" />
</div> --%>


	<div class="moduleSubmenuItemCls">
		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-imageSearch"></span>
		<span class="moduleSubmenuLbl">
			<s:text name="jsp.home_254"/>
		</span>
		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
		<!-- dropdown menu include -->    		
		<s:include value="/pages/jsp/home/inc/accountSubmenuDropdown.jsp" />
	</div>
	
	<!-- dashboard items listing for transfers -->
	<div id="dbAccountSummary" class="dashboardSummaryHolderDiv">
		 <s:url id="goBackSummaryUrl" value="/pages/jsp/imagesearch/imageSearchSummary.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a id="imageSearchSummaryLink" cssClass="summaryLabelCls" href="%{goBackSummaryUrl}" targets="imageSearchSummary" button="true" onclick="ns.imagesearch.showImageSearchSummary()"><s:text name="jsp.home_197"/></sj:a>
		<sj:a id="viewCheckedLink" button="true">&nbsp;<s:text name="jsp.imageSearch.button.viewChecked"/></sj:a>
	</div>
	
	<!-- dashboard items listing for requested images, hidden by default -->
	<div id="dbPendingImagesSummary" class="dashboardSummaryHolderDiv ">
		<sj:a id="pendingImgSummary" cssClass="summaryLabelCls" button="true" onclick="ns.previouslyrequested.loadPendingImagesGrid()"><s:text name="jsp.imageSearch.button.requestedImage.summary"/></sj:a>
		<sj:a id="viewPendingImages" button="true">&nbsp;<s:text name="jsp.imageSearch.button.viewChecked"/></sj:a>	
		<sj:a id="deletePendingImages" button="true" cssStyle="display:none">&nbsp;<s:text name="jsp.imageSearch.button.deleteChecked"/></sj:a>
	</div>
	
	
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbAccountSummary" onclick="ns.common.refreshDashboard ('showdbAccountSummary',true)"><s:text name="jsp.home_254"/></a></li>
			<li class="dashboardSubmenuItem"><a id="showdbPendingImagesSummary" onclick="ns.common.refreshDashboard ('showdbPendingImagesSummary',true)"><s:text name="jsp.imageSearch.button.reqImages"/></a></li>
		</ul>
	</div>


<script type="text/javascript">
 
 
 	//$('.portlet-title').html('Consolidated Balance - Data Table');
 	//ns.common.initializePortlet('imageSearchHolder');
 	
</script> 	
