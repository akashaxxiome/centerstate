<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:removeProperty name="IMAGE_RESULTS" />
<ffi:object name="com.ffusion.tasks.checkimaging.DeleteImageResults" id="FFIDeleteImageResults" scope="session"/>
<ffi:object name="com.ffusion.tasks.checkimaging.GetPendingImageStatus" id="FFIGetPendingImageStatus" scope="session"/>
<ffi:object name="com.ffusion.tasks.checkimaging.SetPendingImages" id="FFISetPendingImages" scope="session"/>
<ffi:object name="com.ffusion.tasks.checkimaging.DeleteImageRequests" id="FFIDeleteImageRequests" scope="session"/>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="FFISearchImage" scope="session"/>
<ffi:object name="com.ffusion.tasks.checkimaging.GetCheckedImages" id="FFIGetCheckedImages" scope="session"/>
<ffi:object name="com.ffusion.tasks.checkimaging.GetImages" id="FFIGetImages" scope="session"/>

	<ffi:setGridURL grid="GRID_accountImagesPending" name="ViewURL" url="/cb/pages/jsp/account/GetImagesAction_viewImage.action?module=DEPOSIT&imageID={0}" parm0="Id"/>
	<ffi:setGridURL grid="GRID_accountImagesPending" name="ViewOffsetURL" url="/cb/pages/jsp/account/GetImagesAction_offsetImages.action?module=OFFSET&imageID={0}" parm0="Id"/>
																		
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetPendingImagesAction.action?GridURLs=GRID_accountImagesPending" URLEncrypt="true"/>
	
	<s:url id="pendingImagesURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<div id="accountPendingImagesPortlet" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	     <div class="portlet-header ui-widget-header ui-corner-all">
        	<span class="portlet-title"><s:text name="jsp.imageSearch.previouslyRequestPortlet.header"/></span>
		</div>
		<div class="portlet-content">
			<ffi:help id="account_imageSearch_pendingImages" />
	<sjg:grid  
		id="accountPendingImagesID"  
		sortable="true" 
		dataType="json"  
		href="%{pendingImagesURL}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		navigator="true"
	    navigatorAdd="false"
	    navigatorDelete="false"
	    navigatorEdit="false"
	    navigatorRefresh="false"
	    navigatorSearch="false"
	    navigatorView="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		onGridCompleteTopics="addGridControlsEvents,pendingImagesGridOnComplete,deleteAllImageCheckboxes"
		multiselect = "true"	
		viewrecords="true"	
		> 
              
		
		<!--  TODO: implementation of i18n -->
		<sjg:gridColumn 
			title="" 
			name="Delete"
			sortable="false" 
			width="35" 
			hidedlg="true" />
		<sjg:gridColumn name="imageHandle" index="imageHandle" title="%{getText('jsp.imageSearch.gridColumnHeader.imageHandle')}" sortable="false" width="70" hidden="true" hidedlg="true"/>
		 <sjg:gridColumn name="status" index="status" title="%{getText('jsp.default_388')}" sortable="true" width="70" hidden="false"  formatter="ns.previouslyrequested.availabilityFormatter"/>
		<sjg:gridColumn name="postingDate" index="posting_date" title="%{getText('jsp.imageSearch.gridColumnHeader.postingDate')}" sortable="true" width="65"/>
		<sjg:gridColumn name="drCr" index="dr_cr" title="%{getText('jsp.imageSearch.gridColumnHeader.DR_CR')}" sortable="true" width="65"/>		
		<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="80"/>
		<sjg:gridColumn name="maskedAccountID" index="account_id" title="%{getText('jsp.default_15')}" sortable="true" width="80"/>
		<sjg:gridColumn name="checkNumber" index="check_number" title="%{getText('jsp.default_92')}" sortable="true" width="80"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_27')}" search="false" sortable="false"  hidden="true" hidedlg="true" cssClass="__gridActionColumn"   formatter="ns.previouslyrequested.pendingImageActionFormatter" width="50"/>				
	</sjg:grid>
	</div>
	
	<div id="accountPendingImagesPortlet-menu" class="portletHeaderMenuContainer" style="display:none;">
       		<ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('accountPendingImagesPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      		</ul>
		</div>
</div>
	<script>
		ns.common.initializePortlet('accountPendingImagesPortlet');
		 $(document).ready(function () {	    	
			$.subscribe('deleteAllImageCheckboxes', function(event,data) {
				$("#jqgh_"+data.id+"_Delete").removeClass( "ui-jqgrid-sortable" ).addClass( "sapUiIconCls icon-delete" );
				$("#jqgh_"+data.id+"_Delete").css({"cursor" : "pointer"});
				
				$("#jqgh_"+data.id+"_Delete").on('click',function(){
					$('#deletePendingImages').trigger('click');
				 });
			});

	    });
	</script>
	 
