<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="cash_cashppaybuild" />
<span class="shortcutPathClass" style="display:none;">cashMgmt_ppay,ppaybuildLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Positive Pay</span>

<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='Temp1' value='${SecurePath}cash/cashppaybuild.jsp?buildFromBlank=true' URLEncrypt="true"/>
<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}cash/cashppupload.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_uploadrecord.gif" alt="" width="150" height="16" border="0" hspace="6"></a><a href="${Temp1}"><img src="/cb/pages/${ImgExt}grafx/cash/button_buildmanualfile_dim.gif" alt="" width="120" height="16" border="0"></a>'/>

<ffi:object id="Counter" name="com.ffusion.beans.util.IntegerMath" scope="session"/>

<BODY onload="preloadImages();CSCalculateSum();">
        <div class="marginTop10" align="center">


            <s:form namespace="/pages/jsp/positivepay" action="buildPositivePayManualFileAction_verify" method="post" name="BuildForm" id="BuildFormID" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
               <div class="paneWrapper">
							<div class="paneInnerWrapper">
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="ltrow2_color">
                                    <tr valign="middle" class="header" height="25">                                        
                                        <td nowrap class="sectionsubhead paddingLeft10"><s:text name="jsp.default_15"/>*</td>
										<td nowrap class="sectionsubhead"><s:text name="jsp.default_137"/>*</td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.default_92"/>*</td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.default_43"/>*</td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.cash_14"/></td>
                                        <td align="center" nowrap class="sectionsubhead"><s:text name="jsp.cash_122"/></td>
                                    </tr>
                                    
                                    <s:iterator value="%{positivePayCheckRecords}" status="listStatus" var="ppayCheckRecord">
                                        <tr valign="middle">
 					    					<%--For the drop down, list each account. If no account previously selected, select Choose Account. --%>

                                            <%--Otherwise, select the chosen account, and check to ensure that account isn't listed twice in the list --%>
                                            <td nowrap class="ltrow2_color">
												<select 
													id="accountString<s:property value="%{#listStatus.index}"/>"
													name="positivePayCheckRecords[<s:property value="%{#listStatus.index}"/>].accountString"  
													class="txtbox">
												</select>
												
												<s:hidden id="account%{#listStatus.index}" 
													name="positivePayCheckRecords[%{#listStatus.index}].accountID" 
													value="" />
													
												<s:hidden id="bank%{#listStatus.index}" 
													name="positivePayCheckRecords[%{#listStatus.index}].bankID" 
													value="" />
													
												<s:hidden id="currencyType%{#listStatus.index}" 
													name="positivePayCheckRecords[%{#listStatus.index}].currencyType" 
													value="" />
													
												<s:hidden id="routingNumber%{#listStatus.index}" 
													name="positivePayCheckRecords[%{#listStatus.index}].routingNumber" 
													value="" />
                                            </td>
                                            
											<td nowrap>
                                               	<%--For the date, either output the date if non-empty, or output the default date format set at top of page --%>
                                               	<sj:datepicker 
                                               		value="%{#ppayCheckRecord.CheckDate}" 
                                                	name="positivePayCheckRecords[%{#listStatus.index}].checkDateString" 
                                                	id="checkDateID%{#listStatus.index}" 
                                                	label="%{getText('jsp.default_137')}" 
                                                	size="6"
                                                	maxlength="10"
                                                	buttonImageOnly="true" 
                                                	cssClass="ui-widget-content ui-corner-all"/>
          
                                                	<script>
														var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=3&calDisplay=select&calForm=FormName&calTarget="/>';
														ns.common.enableAjaxDatepicker("checkDateID"+<s:property value="%{#listStatus.index}"/>, tmpUrl);
													</script>
					    					</td>
											
                                            <td nowrap class="ltrow2_color">
                                            	<input type="text" name="positivePayCheckRecords[<s:property value="%{#listStatus.index}"/>].checkNumber" 
	                                            	id="checkNumber<s:property value="%{#listStatus.index}"/>" 
	                                            	size="20" maxlength="20" class="ui-widget-content ui-corner-all" 
	                                            	value="<ffi:getProperty name="CheckRecord1" property="CheckNumber"/>">
                                            </td>
                                            
                                            <td nowrap class="ltrow2_color">
                                            	<input type="text" style="text-align:right" 
	                                            	name="positivePayCheckRecords[<s:property value="%{#listStatus.index}"/>].AmountValue"
	                                            	id="amount<s:property value="%{#listStatus.index}"/>" 
	                                            	size="20" maxlength="31" class="ui-widget-content ui-corner-all" 
	                                            	value="<ffi:getProperty name="CheckRecord1" property="Amount.CurrencyStringNoSymbolNoComma"/>" 
	                                            	onblur='CSValidateAmount(this)'>
                                            </td>
                                            
                                            <td nowrap class="ltrow2_color">
                                            	<input type="text" name="positivePayCheckRecords[<s:property value="%{#listStatus.index}"/>].additionalData" 
                                            		id="additionalData<s:property value="%{#listStatus.index}"/>" 
                                            		size="30" maxlength="32" class="ui-widget-content ui-corner-all" 
                                            		value="<ffi:getProperty name="CheckRecord1" property="AdditionalData"/>">
                                            </td>
                                            
                                              <td align="center" nowrap >
                                            	<input type="checkbox" name="positivePayCheckRecords[<s:property value="%{#listStatus.index}"/>].voidCheckValue"
                                            		id="voidCheckValue<s:property value="%{#listStatus.index}"/>"  
                                            		value="V" <ffi:cinclude value1="${CheckRecord1.VoidCheck}" value2="true" operator="equals">checked</ffi:cinclude>
                                            	>
                                            </td>
                                        </tr>


                                       <tr>											
											<td><span id="checkRecords_<s:property value="%{#listStatus.index}"/>_accountIDError"></span></td>
											<td><span id="checkRecords_<s:property value="%{#listStatus.index}"/>_checkDateError"></span></td>
											<td><span id="checkRecords_<s:property value="%{#listStatus.index}"/>_checkNumberError"></span></td>
											<td><span id="checkRecords_<s:property value="%{#listStatus.index}"/>_amountError"></span></td>
											<td><span id="checkRecords_<s:property value="%{#listStatus.index}"/>_additionalDataError"></span></td>
											<td><span id="checkRecords_<s:property value="%{#listStatus.index}"/>_voidCheckValueError"></span></td>
										</tr>

									</s:iterator>

                                    <tr valign="middle">
										<td nowrap class="sectionsubhead">&nbsp;</td>
										<td nowrap class="sectionsubhead">&nbsp;</td>
										<td nowrap class="sectionsubhead">&nbsp;</td>
										<td nowrap class="sectionsubhead" size="30">
										<s:text name="jsp.default_432"/>
										<span style="text-align:right" id="amountTotal">&nbsp;</span>
										</td>
										<td nowrap class="sectionsubhead">&nbsp;</td>
										<td></td>
									</tr>
								</table>	
				    <div align="center" class="marginTop10"><span class="required">* <s:text name="jsp.default_240"/></span></div>
                    <div class="btn-row">
						<ffi:setProperty name="ppayURL" value="/pages/jsp/positivepay/initPositivePayAction.action" URLEncrypt="true"/>
																		
						<s:url id="buildCancelUrl" value="%{#session.ppayURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="positivepayReload" value="true"/>
						</s:url>
						<sj:a
							id="cancelBuildFormID"
							href="%{buildCancelUrl}"
							targets="notes"
							indicator="indicator"
							button="true"
							><s:text name="jsp.default_82"/></sj:a>

                                       <sj:a
							id="buildManualFileID"
							formIds="BuildFormID"
							targets="secondDiv"
							button="true"
							validate="true"
							validateFunction="customValidation"
							onBeforeTopics="buildPPayFormBefore,enableManualFileDiv"
							onSuccessTopics="buildPPayFormSuccess"
							onErrorTopics="buildPPayFormError"
                                           onClickTopics="onClickTopicsbuildPPay"
							onCompleteTopics="buildPPayFormComplete"
							><s:text name="jsp.default_175"/></sj:a>
                                           
                         </div>
                   </div>
                 </div>  
            </s:form>
        </div>
    </BODY>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashppaybuild.jsp?buildFromBlank=false" URLEncrypt="true"/>

<%-- TODO - use FxImpl API here --%>
<ffi:object id="GetCurrencyNumDecimals" name="com.ffusion.tasks.fx.GetCurrencyNumDecimals" scope="session"/>
<ffi:process name="GetCurrencyNumDecimals"/>

<script>
$(document).ready(function(){
    $.subscribe('onClickTopicsbuildPPay', function(event,data) {
// select each account in case the AccountString is -1
        for(var i = 0; i < 15; i++){
            selectAccount(document.getElementById("accountString" + i));
           }
    });
    ns.common.updatePortletTitle("ppay_workflowID",js_ppay_build_portlet_title,false);

	for(var i = 0; i < 15; i++){
		//$("#accountString" + i).selectmenu({width: 390});
		
		$("#accountString" + i).lookupbox({
			"source":"/cb/pages/jsp/positivepay/positivePayAccountsLookupBoxAction.action",
			"controlType":"server",
			"collectionKey":"1",
			"size":"30",
			"module":"ppayAccounts",
		});
   	}
	
});

function selectAccount(accountStringObject){
	var recordID = accountStringObject.id;
	var recordIndex = recordID.substring("accountString".length);
	var accountStringVal = $(accountStringObject).val();
    if (accountStringVal == null) accountStringVal = "";
	var accountSeperator = "|";

	var accountDataArray = accountStringVal.split(accountSeperator);
	if(accountDataArray.length!=4){
        if (accountStringVal.length != 0)
		    console.log("Selected account is not valid.");
		$("#account"+recordIndex).val("");
		$("#bank"+recordIndex).val("");
		$("#currencyType"+recordIndex).val("");
		$("#routingNumber"+recordIndex).val("");
	}else{
		accountID = accountDataArray[0];
		bankID = accountDataArray[1];
		currencyType = accountDataArray[2];
		routingNumber = accountDataArray[3];
		
		$("#account"+recordIndex).val(accountID);
		$("#bank"+recordIndex).val(bankID);
		$("#currencyType"+recordIndex).val(currencyType);
		$("#routingNumber"+recordIndex).val(routingNumber);
		
	}
}

function CSValidateAmount(object){
	var CSDecimalPlace = '<ffi:getProperty name="GetCurrencyNumDecimals" property="NumDecimals" />' ;// This variable shoud be later populated with the help of the user session locale
	
	var CSDecimalDummy = Math.pow(10,CSDecimalPlace); //This variable is used for javascript rounding

	if(!(object.value =="" ||object.value ==null)){
			object.value = Math.round(object.value*CSDecimalDummy)/CSDecimalDummy ;
			object.value = CSFormatDecimalPlaces(object.value ,CSDecimalPlace);
	}

	CSCalculateSum();
}

function CSCalculateSum(){
	var CSRowNumber = 15;
	var CSDecimalPlace = '<ffi:getProperty name="GetCurrencyNumDecimals" property="NumDecimals" />' ;// This variable shoud be later populated with the help of the user session locale
	var CSDecimalDummy = Math.pow(10,CSDecimalPlace); //This variable is used for javascript rounding
	
	var amountSum = 0 ;

	for(var i=0;i< CSRowNumber;i++){
		var amount = $("#amount"+i).val();
		if(isNaN(amount)){
		  continue;
		}
		var fAmount =  parseFloat(amount);
		if(fAmount > 0 ){
			amountSum  = amountSum + fAmount;
			amountSum  = Math.round(amountSum*CSDecimalDummy)/CSDecimalDummy;
		}
	}
	document.getElementById('amountTotal').innerHTML= CSFormatDecimalPlaces(amountSum,CSDecimalPlace);
	return true;
}

function CSFormatDecimalPlaces(pNumber,pDecimalPosition){
	
	var CSDecimalPlace = '<ffi:getProperty name="GetCurrencyNumDecimals" property="NumDecimals" />' ;// This variable shoud be later populated with the help of the user session locale

	var   sAmount = pNumber.toString();
	var   iNumberOfZeros = 0;
	var   iDotPostition = sAmount.indexOf('.');

	if ( iDotPostition == -1){
		 sAmount= sAmount+".";
		 iNumberOfZeros = pDecimalPosition;
	}else{
		iNumberOfZeros = CSDecimalPlace - sAmount.length + iDotPostition + 1 ;
		if (iNumberOfZeros >= CSDecimalPlace){
			iNumberOfZeros = 0;
		}
	}


	for (var i=1;i<= iNumberOfZeros;i++){
		sAmount = sAmount+"0";
	}
	return sAmount;

}
</script>

<ffi:removeProperty name="GetCurrencyNumDecimals"/>