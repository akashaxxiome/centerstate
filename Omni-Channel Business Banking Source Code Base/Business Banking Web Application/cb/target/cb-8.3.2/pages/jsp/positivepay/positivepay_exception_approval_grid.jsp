<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script >
	var rowElement ='';
</script>

<ffi:help id="cash_cashppay" />
<link rel='stylesheet' type='text/css' href='/cb/web/css/home-page.css' />

<s:url id="updatePositivePayDecisionsActionURL" value="/pages/jsp/positivepay/updatePositivePayDecisionsAction.action" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>

<ffi:setProperty name="reloadPPayIssues" value="true" />


	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<input type="hidden" name="sortColumnName" value="ACCOUNTID,CHECKNUMBER"/>
	<input type="hidden" name="isAscending" value="true"/>
	<input type="hidden" name="accountID" value="All"/>
	<input type="hidden" name="bankID" value="All"/>
	<input type="hidden" name="payRadioAll" id="payRadioAll"  value="off"/>
	<input type="hidden" name="returnRadioAll" id="returnRadioAll" value="off"/>
	<input type="hidden" name="holdRadioAll" id="holdRadioAll" value="off"/>


	<s:hidden id="updatePositivePayDecisionsActionURLValue" name="updatePositivePayDecisionsActionURLValue" value="%{#updatePositivePayDecisionsActionURL}" />

	<ffi:setGridURL grid="GRID_ppayExceptionGrid" name="ViewURL" url="/cb/pages/jsp/positivepay/SearchImageAction.action?module=POSITIVEPAY&pPayaccountID={0}&pPaybankID={1}&pPaycheckNumber={2}" parm0="CheckRecord.AccountID" parm1="CheckRecord.BankID" parm2="CheckRecord.CheckNumber" />

	<ffi:setProperty name="ppayExceptionGridTempURL" value="/pages/jsp/positivepay/getPositivePayIssuesAction.action?collectionName=PPayIssues&status=pendingApproval&GridURLs=GRID_ppayExceptionGrid&status=pendingApproval" URLEncrypt="true"/>
	<s:url id="ppayExceptionGridURL" value="%{#session.ppayExceptionGridTempURL}" escapeAmp="false"/>
	<sjg:grid
		id="ppayApprovalExceptionGridID"
		sortable="true"
		dataType="json"
		href="%{ppayExceptionGridURL}"
		sortname="account"
		sortorder="asc"
		pager="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
 		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onSortColTopics=""
		onGridCompleteTopics="addGridControlsEvents,PPayApprovExceptionEvent,ppayExceptionGridCompleteTopicForRadioRendering,pendingApprovalGridCompleteEvents"
		>

		<sjg:gridColumn name="issueDate" index="issueDate" title="%{getText('jsp.default_137')}" formatter="ns.cash.dateFormatter" sortable="true" width="80"/>
	    <sjg:gridColumn name="account" index="account" title="%{getText('jsp.default_15')}" sortable="true" width="348"/>
	    <sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}" sortable="true" width="80" cssClass="datagrid_actionColumn"/>
	    <sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="90"/>
	    <sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_350')}" sortable="true" width="110"/>

		<sjg:gridColumn name="map.routingNumber" index="rountingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_9')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.nickName" index="nickName" title="%{getText('jsp.default_293')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_44')}" sortable="true" hidden="true" hidedlg="true"/>

        <sjg:gridColumn name="map.defaultdecision" index="defaultdecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currentDecision" index="currentDecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="Submitted For" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverName" index="approverName" title="ApproverName" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="ApproverIsGroup" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.cash.formatApproversInfo" />
		<sjg:gridColumn name="userName" index="userName" title="UserName" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="checkRecord.bankID" index="bankIDColumn" title="%{getText('jsp.cash_19')}"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="checkRecord.accountID" index="accountIDColumn" title="%{getText('jsp.cash_11')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass = "PPAYImageUrlId"/>
	</sjg:grid>


	<%-- </ffi:cinclude> --%>


<div class="ppayHoverDetailsMaster">
	<div class="ppayDetailsWinOverlay"></div>
	<div class="ppayHoverDetailsHolder">
		<span class="winTitle">Check Image</span>
<!-- 		<div class="checkDetailsSection">
			<table border="0" width="100%" class="checkDetailsTable">
				<tr>
					<td class="fieldLbl" width="25%">Check Number:</td>
					<td class="fieldVal" width="25%">1016</td>
					<td class="fieldLbl" width="25%">Posting Date:</td>
					<td class="fieldVal" width="25%">04/30/1998</td>
				</tr>
				<tr>
					<td class="fieldLbl" width="25%">Account:</td>
					<td class="fieldVal" width="25%">50000-Savings</td>
					<td class="fieldLbl" width="25%">Debit Amount:</td>
					<td class="fieldVal" width="25%">$400,000.00</td>
				</tr>
			</table>
		</div> -->
		<hr />

	</div>
</div>

	<script type="text/javascript">
		var currPpayExceptionGridPage = 1;

	    $(document).ready(function () {


			var fnCustomPaging = function(nextPage){
				var currPpayExceptionGridPage = $("#ppayExceptionGridID").jqGrid('getGridParam', 'page');
				var rowNumberSelected = $('#gbox_ppayExceptionGridID .ui-pg-selbox :selected').val();

				//When user clicks on First and Last links, the page returned is not the actual pagem but the string is returned
				//Convert that string to the actual first or last pagenumber
				if(nextPage == 'first_ppayExceptionGridID_pager'){
					nextPage = 1;
				}else if(nextPage == 'last_ppayExceptionGridID_pager'){
					//Get the last page number of the grid
					nextPage = $("#ppayExceptionGridID").jqGrid('getGridParam', 'lastpage');
				}

				$.ajax({
				  url: $('#updatePositivePayDecisionsActionURLValue').val(),
				  data: $("#ppayExceptionFormID").serialize(),
				  success: function(data) {
					  $("#ppayExceptionGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
					  $('#ppayExceptionGridID').trigger("reloadGrid",[{page:nextPage}]);
					  $.publish('PPayExceptionPagingEvent');
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					 //set the grid page value back to original
					 $("#ppayExceptionGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
					 $("#ppayExceptionGridID").jqGrid('setGridParam', {page:currPpayExceptionGridPage});
				  }
				});
				return 'stop';
			};

			$("#ppayExceptionGridID").data('onCustomPaging', fnCustomPaging);

			var fnCustomSortCol = function (index, columnIndex, sortOrder) {
				$.publish("PPayExceptionOnSortTopic");
			};

			$("#ppayExceptionGridID").data('onCustomSortCol', fnCustomSortCol);


			//ppay exception grid complete event.
			$.subscribe('PPayExceptionPagingEvent', function(event,data) {
				$("#payRadioAll").val("off");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("off");
			});


			$.subscribe('ppayExceptionGridDummySubmit', function(event,data) {
				$.ajax({
					  url: $('#updatePositivePayDecisionsActionURLValue').val(),
					  data: $("#ppayExceptionFormID").serialize(),
					  success: function(data) {
						 //Trigger form submt action now
						 $("#ppayExceptionGridSubmit").click();
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
						 //console.log('Error occurred while submitting !');
					  }
					});
			});

			$.subscribe('PPayExceptionOnSortTopic', function(event,data) {
				$.ajax({
					  url: $('#updatePositivePayDecisionsActionURLValue').val(),
					  data: $("#ppayExceptionFormID").serialize(),
					  success: function(data) {
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
					  }
					});
			});
	    });

    </script>