<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%
	session.setAttribute("positivepayReload", request.getParameter("positivepayReload"));
	session.setAttribute("PPaySubmitted", request.getParameter("PPaySubmitted"));
	session.setAttribute("buildFromBlank", "true");
	session.setAttribute("cashppaybuildupload", request.getParameter("cashppaybuildupload"));
%>

<%-- Find out if the user can upload files --%>
<% boolean fileUploadEntitled = false; %>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>" >
<% fileUploadEntitled = true; %>
</ffi:cinclude>
<% if( !fileUploadEntitled ) { %>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
<% fileUploadEntitled = true; %>
</ffi:cinclude>
<% } %>
<% if( fileUploadEntitled ) { %>
	<ffi:setProperty name="tmp_uploadButton" value='<a href="${SecurePath}cash/cashppupload.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_uploadrecord.gif" alt="" width="150" height="16" border="0" hspace="6"></a>' />
<% } %>

<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='PageText' value='${PageText}${tmp_uploadButton} <a href="${SecurePath}cash/cashppaybuild.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_buildmanualfile.gif" alt="" width="120" height="16" border="0"></a>' />
<ffi:removeProperty name="tmp_uploadButton" />

<%-- Added for checkimaging --%>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>

		<%
			session.setAttribute("FFISearchImage", session.getAttribute("SearchImage"));
        %>

<%-- get the cut-off definition for the business --%>
<%
	com.ffusion.beans.business.Business business = (com.ffusion.beans.business.Business)session.getAttribute( com.ffusion.tasks.business.BusinessTask.BUSINESS );
	String affiliateBankIDStr = Integer.toString( business.getAffiliateBankID() );
%>
<ffi:object id="GetCutOffDefinition" name="com.ffusion.tasks.affiliatebank.GetCutOffDefinition" scope="session"/>
<ffi:setProperty name="GetCutOffDefinition" property="AffiliateBankID" value="<%= affiliateBankIDStr %>" />
<ffi:setProperty name="GetCutOffDefinition" property="CutOffType" value="<%=Integer.toString( com.ffusion.efs.adapters.profile.constants.ProfileDefines.AFFILIATE_CUTOFF_TYPE_POSITIVE_PAY )%>" />
<ffi:process name="GetCutOffDefinition" />
<ffi:removeProperty name="GetCutOffDefinition"/>

<%-- Want to re-blank objects for BuildManualCheckRecord --%>
<ffi:setProperty name="positivepay-buildFromBlank" value="true"/>

<sj:dialog id="searchImageDialog" cssClass="cashMgmtDialog" title="%{getText('jsp.default_95')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="600" >
</sj:dialog>

