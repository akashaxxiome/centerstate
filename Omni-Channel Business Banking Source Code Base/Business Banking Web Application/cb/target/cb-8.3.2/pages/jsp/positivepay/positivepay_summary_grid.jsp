<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="innerPortletsContainer">
<div id="ppay_summary_gridID">

	<%-- This Div was present in positivepaysummary.jsp. Since,it was hidden, kept this DIV hidden only while migrating to new architecture. --%>
	<div id="hiddenDivFromOldCode" style="display: none;">
	<% 
			String cutOffTimeStr="";
			com.ffusion.beans.business.Business business = (com.ffusion.beans.business.Business)session.getAttribute(com.ffusion.tasks.business.BusinessTask.BUSINESS);
			String ppayDefaultDecision = business.getPPayDefaultDecision();
			
			// get cutOffTimeStr only if the ppay default decision is not empty and is not "NONE"
			// i.e. the system will make the ppay decision for this business at that time
			if( !( ppayDefaultDecision == null || ppayDefaultDecision.length() <= 0 || ppayDefaultDecision.equalsIgnoreCase( com.ffusion.beans.business.BusinessDefines.PPAY_NONE ) ) ) {
				com.ffusion.beans.affiliatebank.CutOffDefinition cutOffDef = (com.ffusion.beans.affiliatebank.CutOffDefinition) session.getAttribute( com.ffusion.tasks.affiliatebank.AffiliateBankTask.CUTOFF_DEFINITION );
				
				if( cutOffDef!=null ) {
					com.ffusion.beans.affiliatebank.CutOffTimes cutOffTimes = cutOffDef.getCutOffs();
					com.ffusion.beans.affiliatebank.CutOffTime cutOffTime=(com.ffusion.beans.affiliatebank.CutOffTime)cutOffTimes.get( 0 );
					String timeOfDay = cutOffTime.getTimeOfDay();
		
					if( timeOfDay!=null ) {
						cutOffTimeStr = timeOfDay;
					}
				}
				
				if ( ppayDefaultDecision.equalsIgnoreCase( com.ffusion.beans.business.BusinessDefines.PPAY_PAY ) ) {
					ppayDefaultDecision="Pay";
				} else if( ppayDefaultDecision.equalsIgnoreCase( com.ffusion.beans.business.BusinessDefines.PPAY_RETURN ) ) {
					ppayDefaultDecision="Return";
				}
			}
		%>
		
		<ffi:cinclude value1="" value2="<%= cutOffTimeStr %>" operator="notEquals">
			<s:text name="jsp.cash_15"/> <%= cutOffTimeStr %> <s:text name="jsp.cash_55"/> <br>
			<s:text name="jsp.cash_50"/> <%= ppayDefaultDecision %> <s:text name="jsp.cash_16"/><br>
		</ffi:cinclude>
		</div>
		
		<ffi:help id="cash_cashppay" />
		
		<!-- selected account value should be take from session to display -->
		<% String selectedValue = (String)session.getAttribute("selectedValue"); 
			if(selectedValue == null){
				selectedValue="";
			}
		%>
		<input type="hidden" id="setSelValue" value="<%= selectedValue %>"/>
		<div class="actionButtonArea marginTop20">
			<span class="paddingRight10"><s:text name="jsp.cash.ppay.select.account"/></span>
			<s:select list="positivePayMap" class="txtbox" id="ppaySummaryGridID" onchange="getDetails()" headerKey="" headerValue="All Accounts"></s:select> 
		</div>
	</div>	
</div>

<script>	
			
		function getDetails(){
			var selectedValue = $('#ppaySummaryGridID').val();
			if(selectedValue!=''){
				var accountIdIndex = selectedValue.indexOf("A");
				var bankIndex = selectedValue.indexOf("B");
				var accountId = selectedValue.substring(accountIdIndex+1,bankIndex);
				var bankId = selectedValue.substring(bankIndex+1);
				var url ='/cb/pages/jsp/positivepay/getPositivePayIssuesForAccountAction_init.action?accountID='+accountId+"&bankID="+bankId+"&status=New"
				ns.cash.refreshExceptionGridForAccount(url);
			}
			else{
				ns.cash.refreshPPayGridURL = "<ffi:urlEncrypt url='/cb/pages/jsp/positivepay/positivepay_exception_grid.jsp'/>";
				ns.cash.refreshPPayGrid(ns.cash.refreshPPayGridURL);
			}
		}
		
		$(document).ready(function(){
			$("#jqgh_ppaySummaryGridID_account").addClass("gridNicknameField");
			var selVal = $('#setSelValue').val();
			
			if(selVal === null)
				selVal="";
				
			$('#ppaySummaryGridID').val(selVal);
		
			 if(ns.common.isInitialized($('#ppaySummaryGridID'),'ui-selectmenu')){
				 $('#ppaySummaryGridID').selectmenu("destroy");	
			 }	
		$("#ppaySummaryGridID").selectmenu({width: 350});
		});
</script>