<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="cash_cashppaybuildconfirm" />

<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='Temp1' value='${SecurePath}cash/cashppaybuild.jsp?positivepay-buildFromBlank=true' URLEncrypt="true"/>
<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}cash/cashppupload.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_uploadrecord.gif" alt="" width="150" height="16" border="0" hspace="6"></a><a href="${Temp1}"><img src="/cb/pages/${ImgExt}grafx/cash/button_buildmanualfile_dim.gif" alt="" width="120" height="16" border="0"></a>'/>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashppaybuildconfirm.jsp" URLEncrypt="true"/>
<ffi:setProperty name="positivepay-buildFromBlank" value="false"/>

       
    <div class="paneWrapper">
		<div class="paneInnerWrapper">
               <table width="100%" border="0" cellspacing="0" cellpadding="2" class="ltrow2_color">
	               
	                  <%--  <tr>
	                       <td class="tbrd_b" colspan="7" align="left" nowrap><div class="paddingLeft10 marginTop10 marginBottom10"><s:text name="jsp.cash_79"/></div></td>
	                   </tr> --%>
                    
                   <tr class="header" valign="middle" height="25">                       
                       <td class="sectionsubhead paddingLeft10" nowrap><s:text name="jsp.default_15"/></td>
					   <td class="sectionsubhead" nowrap><s:text name="jsp.default_137"/></td>
                       <td class="sectionsubhead" nowrap><s:text name="jsp.default_92"/></td>
                       <td class="sectionsubhead" style="text-align:right" nowrap><s:text name="jsp.default_43"/></td>
                       <td class="sectionsubhead" nowrap>&nbsp;&nbsp;</td>
                       <td class="sectionsubhead" nowrap><s:text name="jsp.cash_14"/></td>
                       <td class="sectionsubhead" nowrap>
                           <div align="center">
                               <s:text name="jsp.cash_122"/></div>
                       </td>
                   </tr>
					<!-- <tr><td>&nbsp;</td></tr> -->
					<% boolean toggle = false; String cls = ""; %>
	                   <s:iterator value="%{positivePayCheckRecords}" status="listStatus" var="ppayCheckRecord">
	
	                       <%--Only output non-empty records--%>
	                       <s:set var="checkNumberString" value="%{#ppayCheckRecord.checkNumber}"/>
					   <s:if test="%{#checkNumberString!=''}">
					                       
					<%	toggle = !toggle; cls = toggle ? "columndata_grey" : "columndata_white"; %>

                           <tr valign="middle">
                                   <td class="<%= cls %>" nowrap>
                                   	<s:property value='#ppayCheckRecord.hash.accountDisplayText'/>
                                   </td>
                                   
									<ffi:setProperty name="CheckRecord2" property="CheckDate.Format" value="MM'/'dd'/'yyyy"/>
									<td class="<%= cls %>  paddingLeft10" nowrap>
										<s:property value='#ppayCheckRecord.checkDate'/>
									</td>
								
                                   <td class="<%= cls %>" nowrap>
                                   	<s:property value='#ppayCheckRecord.checkNumber'/>
                                   </td>
                                   
                                   <td class="<%= cls %>" style="text-align:right" nowrap>
                                   	<s:property value='#ppayCheckRecord.amountNoSymbol'/>
                                   </td>
                                   
                                   <td class="<%= cls %>" nowrap>
                                   	&nbsp;&nbsp;
                                   </td>
                                   
                                   <td class="<%= cls %>" nowrap>
                                   	<s:set var="additionalData" value="%{#ppayCheckRecord.additionalData}"/>
		                    		<s:if test="%{#additionalData==''}">
		                    		&nbsp;-&nbsp;
									</s:if>
									<s:elseif test="%{#additionalData !=''}">
									<s:property value='#ppayCheckRecord.additionalData'/>															
									</s:elseif>
		                            </td>
                                   
                                   <td nowrap class="<%= cls %>">
                                           <div align="center">
                                               <input type="checkbox" name="checkbox" value="checkbox"
                                                   <s:set var="voidCheckFlag" value="%{#ppayCheckRecord.voidCheckValue}"/>
                          	<s:if test="%{#voidCheckFlag=='true'}">
                          		checked
							</s:if>
                                               disabled>
                                           </div>
                                       </td>
                               </tr>
                          	</s:if>
                   </s:iterator>

                              </tr>

						<tr valign="middle">
							<td nowrap class="sectionsubhead">
								&nbsp;
							</td>
							<td nowrap class="sectionsubhead">
								&nbsp;
							</td>
							<td nowrap class="sectionsubhead" size="30">
							<s:text name="jsp.default_432"/>
						
							<%-- <s:property value='#request.totalAmount.CurrencyString'/> --%>
						<span style="text-align:right"><s:property value='%{positivePayCheckRecords.totalAmount.CurrencyString}'/></span>
							</td>
							<td nowrap class="sectionsubhead">
								&nbsp;
							</td>
							<td nowrap class="sectionsubhead">
								&nbsp;
							</td>
						</tr>
							<tr><td>&nbsp;</td></tr>

                              <tr>
                                 <td colspan="7">
								<ffi:setProperty name="tmp_url" value="cashppay.jsp" URLEncrypt="true"/>
                          	 	<s:form name="form1" method="post" action="<ffi:getProperty name='tmp_url'/>" theme="simple">
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                              	 <div align="center">
                          		<sj:a 
								id="editBuildFormID"
								button="true"
								onclick="ns.cash.gotoStep(1);"
								><s:text name="jsp.default_178"/></sj:a>

                          <s:url id="buildUploadUrl" value="/pages/jsp/positivepay/buildPositivePayManualFileAction.action" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							id="uploadBuildFormID"
							href="%{buildUploadUrl}" 
							targets="thirdDiv" 
							button="true"
							onSuccessTopics="buildPPayVerifySuccess"
							onErrorTopics="buildPPayVerifyError"
							onCompleteTopics="buildPPayVerifyComplete"
							><s:text name="jsp.default_452"/></sj:a>
						                             	</div>				
						                           </s:form>
						<ffi:removeProperty name="tmp_url"/>
						                       </td>
						                   </tr>
						                   <tr><td>&nbsp;</td></tr>
						               </table>
								</div>
							</div>  