<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_9')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<span class="shortcutPathClass" style="display:none;">pmtTran_billpay,addSingleBillpayLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Payments</span>
<span id="PageHeading" style="display:none;"><s:text name="jsp.billpay_9"/></span>

<script language="JavaScript">
<!--

	$(function(){
		$("#selectName").selectmenu({escapeHtml:true, width: 200});
		$("#frequencyId").selectmenu({width: 100});
		$("#accountId").selectmenu({width: 150});
		$("#assignedId").selectmenu({width: 100});
		$("#repeatingId").selectmenu({width: 100});
	});

	function loadBillpayProp() {
		var urlString = '/cb/pages/jsp/billpay/billpaynewform.jsp';
		$.ajax({
			   url: urlString,
			   type: "POST",
			   data: $("#FormNameID").serialize(), 	
			   success: function(data){
			   $("#inputDiv").html(data);
		   }
		});
	}

// --></script>

		<div align="center">
			<div align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" height="7" width="728" border="0"></td>
					</tr>
					<tr>
						
						<td class="columndata ltrow2_color">
							<form action="billpaynew.jsp" method="post" name="FormName" id="FormNameID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<div align="left">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="5"><span class="sectionhead">&gt; <s:text name="jsp.billpay_70"/></span><span class="columndata"><s:text name="jsp.billpay_3"/></span></td>
<td  class="sectionhead tbrd_b ltrow2_color" align="right"> 
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FOREIGN_EXCHANGE_UTILITY %>">
			<sj:a 
				tooltip="%{getText('jsp.default_214')}"
		    	onclick="ns.common.openDialog('fx');"
		    	button="true"
		    >
				<img src="/cb/web/multilang/grafx/common/i_fx.gif" alt="" width="33" height="14" border="0">
		    </sj:a>
	</ffi:cinclude>
											</td>													
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right"></div>
											</td>
											<td colspan="4">
												<div align="center">
													<br>
												</div>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span><span class="required">*</span></div>
											</td>
											<td colspan="2">
											<select id="selectName" class="txtbox" name="changePayeeID" size="1" onchange="loadBillpayProp()">
												<option value=""><s:text name="jsp.billpay_98"/></option>
												<ffi:list collection="Payees" items="Payee">													
													<option value="<ffi:getProperty name="Payee" property="ID" />"><ffi:getProperty name="Payee" property="Name" /></option>
												</ffi:list>
												</select></td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td colspan="2"><input class="ui-widget-content ui-corner-all" type="text" name="textfield5" value="" disabled size="12"></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td colspan="2"><select id="accountId" class="txtbox" name="selectName" size="1" disabled>
													<option selected value="value"><s:text name="jsp.billpay_97"/></option>
												</select></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_76"/></span></div>
											</td>
											<td colspan="2"><input class="ui-widget-content ui-corner-all" type="text" name="textfield23" disabled size="12" maxlength="10" value="<ffi:getProperty name="UserLocale" property="DateFormat"/>"> <img class="ui-datepicker-trigger" src="/cb/struts/js/calendar.gif" alt="<s:text name="jsp.billpay_4"/>" title="..."></td>
										</tr>
										<tr style="display: none;">
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_215"/></span></div>
											</td>
											<td colspan="2"><select id="frequencyId" class="txtbox" name="select5" disabled>
													<option selected><s:text name="jsp.billpay_65"/></option>
												</select></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_351"/></span></div>
											</td>
											<td colspan="2" valign="middle"><select id="repeatingId" name="select2" class="txtbox" disabled>
													<option selected><s:text name="jsp.billpay_65"/></option>
												</select></td>
										</tr>
										<tr>
											<td valign="middle" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td colspan="2" valign="middle"><input type="radio" checked disabled><span class="columndata"><s:text name="jsp.default_446"/></span></td>
										</tr>
										<tr>
											<td class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td colspan="2" valign="middle"><input type="radio" disabled><span class="columndata"><s:text name="jsp.default_2"/></span><input type="text" name="NumberPayments" value="2" size="3" maxlength="3" class="ui-widget-content ui-corner-all" disabled></td>
										</tr>
										<tr>
											<td class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td colspan="2">---</td>
											<td valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td colspan="2" valign="middle"><input class="ui-widget-content ui-corner-all" type="text" name="textfield3232323" disabled size="25" maxlength="64"></td>
										</tr>
										<tr>
											<td class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td valign="middle" class="tbrd_l">
											<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span></div>
											</ffi:cinclude>&nbsp;
											</td>
											<td colspan="2" valign="middle">
											<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
												<select id="assignedId" name="select6" class="txtbox" disabled>
													<option value="0"><s:text name="jsp.default_445"/></option>
												</select>
											</ffi:cinclude>
											</td>
										</tr>
										<tr>
											<td class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td class="columndata tbrd_l">
												<div align="right">&nbsp;</div>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td colspan="2">---</td>
											<td class="columndata tbrd_l">
												<div align="right">&nbsp;</div>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right"></div>
											</td>
											<td colspan="4">
												<div align="center">
													<br>
													<sj:a id="cancelFormButtonOnInput"  
														summaryDivId="summary" buttonType="cancel"
										                button="true" 
														onClickTopics="showSummary,cancelBillpayForm"
										        	><s:text name="jsp.default_82"/></sj:a>
											</td>
											<td></td>
										</tr>
									</table>
								</div>
							</form>
						</td>
						
					</tr>
					<tr>
						<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" height="7" width="728" border="0"></td>
					</tr>
				</table>
				<br>
			</div>
		</div>