<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<%
	String Collection = request.getParameter("Collection");
	String ID = request.getParameter("ID");
	session.setAttribute("isPending", request.getParameter("isPending"));

%>


<ffi:setProperty name="OpenEnded" value="false" />
 <ffi:object name="com.ffusion.tasks.billpay.SetPayment" id="SetPayment" scope="session"/>
	<ffi:setProperty name="SetPayment" property="Name" value="<%=Collection %>"/>
	<ffi:setProperty name="SetPayment" property="ID" value="<%=ID %>"/>
<ffi:process name="SetPayment"/>
        <ffi:removeProperty name="FilteredBillPayAccounts,AccountFilter"/>
<%-- <ffi:setProperty name="Payment" property="RecPaymentID" value="171234"/> --%>

<ffi:cinclude value1="${Payment.RecPaymentID}" value2="" operator="notEquals" >
    <% String dateString = ""; String dateFormatString = ""; %>
    <ffi:getProperty name="Payment" property="DateFormat" assignTo="dateFormatString"/>
	<ffi:getProperty name="Payment" property="PayDate" assignTo="dateString"/>
	<%-- <ffi:object name="com.ffusion.tasks.billpay.GetRecPaymentByID" id="GetRecPaymentByID" scope="session"/>
		<ffi:setProperty name="GetRecPaymentByID" property="ID" value="${Payment.RecPaymentID}"/>
	<ffi:process name="GetRecPaymentByID"/> --%>
	<ffi:setProperty name="Payment" property="DateFormat" value="<%=dateFormatString%>"/>
	<ffi:setProperty name="Payment" property="PayDate" value="<%=dateString%>"/>
</ffi:cinclude>
<ffi:object id="EditPayment" name="com.ffusion.tasks.billpay.EditPayment" scope="session"/>
	<ffi:setProperty name="EditPayment" property="Initialize" value="TRUE" />
<ffi:process name="EditPayment"/>
<ffi:setProperty name="EditPayment" property="Initialize" value="FALSE"/>
<ffi:setProperty name="EditPayment" property="Process" value="FALSE"/>
<ffi:setProperty name="EditPayment" property="MinimumAmount" value="0.01"/>
<ffi:setProperty name="EditPayment" property="DateFormat" value="${UserLocale.DateFormat}"/>

<% if (session.getAttribute("Payment") instanceof com.ffusion.beans.billpay.RecPayment) { %>
	<ffi:setProperty name="EditPayment" property="NumberPayments" value="${EditPayment.RemainingPayments}" />
	<ffi:setProperty name="EditPayment" property="PayDate" value="${EditPayment.NextPayDate}" />
	<ffi:setProperty name="RepeatingRadioButton" value="NumPayments" />
	<ffi:setProperty name="OpenEnded" value="false" />
	<ffi:cinclude value1="${EditPayment.NumberPayments}" value2="999">
 		<ffi:setProperty name="OpenEnded" value="true" />
		<ffi:setProperty name="RepeatingRadioButton" value="NoEndDate" />
 	</ffi:cinclude>
<% } %>

<%--
// Check the date.  If it's before today, set it to today's date.
/*com.ffusion.beans.billpay.Payment payment = (com.ffusion.beans.billpay.Payment)session.getAttribute("EditPayment");
com.ffusion.beans.DateTime payDate = payment.getPayDateValue();
if (payDate != null) {
	com.ffusion.beans.DateTime today = new com.ffusion.beans.DateTime();
	if (payDate.before(today)) {
		payment.setPayDate(today);
	}
}*/
--%>

<%
session.setAttribute("FFIEditPayment", session.getAttribute("EditPayment"));
%>

