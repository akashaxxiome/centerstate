<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

    <s:url id="billpayBankUrl" value="/pages/jsp/billpay/getBillpayBanksAction.action" escapeAmp="false"/>
	<sjg:grid
		id="billpayBanksGridId"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{billpayBankUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true" 
		onGridCompleteTopics="addGridControlsEvents,billpayBankGridCompleteEvents"
	> 
		<sjg:gridColumn name="institutionName" index="institutionName" title="%{getText('jsp.default_283')}" sortable="false" width="20" formatter="ns.billpay.formatBillpayBanksNameLinks" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="street" index="Street" title="%{getText('jsp.billpay_105')}" sortable="false" width="30" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="city" index="City" title="%{getText('jsp.default_101')}" sortable="false" width="20" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="state" index="State" title="%{getText('jsp.default_386')}" sortable="false" width="15" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="country" index="Country" title="%{getText('jsp.default_115')}" sortable="false" width="20" cssClass="datagrid_textColumn"/>
	
		<sjg:gridColumn name="institutionId" index="institutionId" title="%{getText('jsp.billpay_58')}" width="7" sortable="false" formatter="ns.billpay.formatBillpayBanksActionLinks" cssClass="datagrid_actionIcons"/>
		
		<sjg:gridColumn name="achRoutingNumber" index="achRoutingNumber" title="%{getText('jsp.billpay_7')}" sortable="false" width="20" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>		
	</sjg:grid>
	
	<script type="text/javascript">
		$("#billpayBanksGridId").jqGrid('setColProp','institutionId',{title:false});
	</script>