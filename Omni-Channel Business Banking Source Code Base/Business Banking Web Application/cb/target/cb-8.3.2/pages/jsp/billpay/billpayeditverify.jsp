<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<ffi:help id="payments_billpayeditconfirm" className="moduleHelpClass" />
<div class="formPanel">
	<s:include value="billpayeditconfirm.jsp" />
			<%-- <td width="30%">
				<div id="templateForSingleBillpayVerify" class="templatePane">
					<s:set name="strutsActionName" value="'editBillpayAction'"
						scope="request" />
					<div id="billpayTemplateDiv" style="position:relative;">
						<s:include value="billpaytempname.jsp" />
					</div>
					<div id="templateForSingleBillpayVerifyResult"></div>
				</div></td> --%>
</div>

<script type="text/javascript">
	$('#billpayTemplateDiv').pane(
			{
				title : js_billpay_template_pane_title,
				minmax : false,
				helpCallback: function(){
					var helpFile = $('#billpayTemplateDiv').find('.moduleHelpClass').html();
					callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
				}
			});

	
</script>