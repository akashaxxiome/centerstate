<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaynewsend" className="moduleHelpClass"/>
		<div style="display:none">
			<s:if test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED ||
						payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED || 
						payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL ||
						payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_CREATED ||
						payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_CLEARED ||
						payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PAID ||
						payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_POSTED 
						}">
			<span id="billpayResultMessage"><s:text name="jsp.billpay_119"/></span>
			</s:if>
			<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_UNKNOWN ||
							payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_TO_PAY ||
							payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_NOT_EXCEPTED || 
							payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_DATE_TOO_SOON ||
							payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_FAILED ||
							payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_NOT_ALLOWED ||
							payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_APPROVAL
							}">
				<span id="billpayResultMessage"><s:text name="jsp.billpay_136"/></span>
			</s:elseif>
			<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_INSUFFICIENT_FUNDS}">
			<span id="billpayResultMessage"><s:text name="jsp.billpay_137"/></span>
			</s:elseif>
			<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_FAILEDON}">
			<span id="billpayResultMessage"><s:text name="jsp.billpay_138"/></span>
			</s:elseif>
			<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITCHECK_FAILED}">
			<span id="billpayResultMessage"><s:text name="jsp.billpay_139"/></span>
			</s:elseif>
		</div>
<form action="" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="Refresh" value="true">
<div class="leftPaneWrapper" role="form" aria-labeledby="billPaySummary"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="billPaySummary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="editConfirmBillPay_payeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id="editConfirmBillPay_payeeNameValue"><ffi:getProperty name="payment.payee" property="Name" /></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="editConfirmBillPay_fromAccountLabel"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue" id="editConfirmBillPay_fromAccountValue" ><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
	</div>
</div>
</form>
<div id="templateForSingleBillpayVerify" class="leftPaneInnerWrapper" role="form" aria-labeledby="saveTemplate">
<div class="header"><h2 id="saveTemplate"><s:text name='jsp.default_371' /></h2></div>
<s:set name="strutsActionName" value="'editBillpayAction'" scope="request" />
<div id="billpayTemplateDiv" class="leftPaneInnerBox">
	<s:include value="billpaytempname.jsp" />
</div>
<div id="templateForSingleBillpayVerifyResult"></div>
</div>

</div>
<div class="confirmPageDetails">
<div class="templateConfirmationWrapper">
		<div class="confirmationDetails">
			<span id="" class="sectionLabel"><s:text name="jsp.default_347"/></span>
	        <span id="" class="columndata"><s:property value="%{payment.TrackingID}"/></span>
		</div>
</div>

<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
<%-- <div class="blockContent">
	<div class="blockRow">
		<span id="" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
           <span id="" class="columndata">
            <s:if test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL}">
				<s:property value="%{payment.statusName}"/>
			</s:if>
			<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BATCH_INPROCESS || payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED }">
					<s:text name="jsp.default_237"/>
			</s:elseif>
			<s:else>
				<s:text name="jsp.default_530"/>
			</s:else>
		</span>
		<span style="float:right"><s:text name="jsp.billpay_125"/></span>
	</div>
</div> --%>
<div class="blockContent">
	
          			<s:set var="PMS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_TO_PAY==payment.status}" />
					<s:set var="PMS_INSUFFICIENT_FUNDS" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_INSUFFICIENT_FUNDS==payment.status}" />
					<s:set var="PMS_BPW_LIMITCHECK_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITCHECK_FAILED==payment.status}" />
					<s:set var="PMS_SCHEDULED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED==payment.status}" />
					<s:set var="PMS_FUNDS_ALLOCATED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED==payment.status}" />
					<s:set var="PMS_PENDING_APPROVAL" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL==payment.status}" />
					<s:set var="PMS_UNKNOWN" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_UNKNOWN==payment.status}" />
					<s:if
						test="%{#PMS_FAILED_TO_TRANSFER}">
						<s:set var="HighlightStatus" value="true" />
					</s:if> 
					<s:if test="%{#PMS_INSUFFICIENT_FUNDS}">
						<s:set var="HighlightStatus" value="true" />
					</s:if> 
					<s:if test="%{#PMS_BPW_LIMITCHECK_FAILED}">
						<s:set var="HighlightStatus" value="true" />
					</s:if> 
					<s:if test="%{#HighlightStatus=='true'}">
						<div class="blockRow failed">
							<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
           					<span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-decline"></span> <!-- failed mean give red here -->
									<s:property value="%{payment.statusName}"/>
							</span></span>
							<span style="float:right"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:if> 
					<s:elseif test="%{#PMS_SCHEDULED || #PMS_FUNDS_ALLOCATED}">
					<div class="blockRow pending">
						<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
			            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-future"></span><!-- this is scheduled/pending giv orange -->
									<s:property value="%{payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_TRANSFERED}">
						<div class="blockRow completed">
						 	<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
				            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> 
								<s:property value="%{payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_PENDING_APPROVAL}">
						<div class="blockRow pending">
							<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
				            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-pending"></span> <!-- this is pending approval give color orange icon different -->
								<s:property value="%{payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
</div>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="payment.payee" property="Name" /> (<s:property value="payment.Payee.NickName"/> - <ffi:getProperty name="payment.payee" property="UserAccountNumber" />)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_75"/>:</span>
				<span id="editConfirmBillPay_payeeNickNameValue" class="columndata"><s:property value="payment.Payee.NickName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_payeeAccountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_15"/>:</span>
				<span id="editConfirmBillPay_payeeAccountValue" class="columndata"><ffi:getProperty name="payment.payee" property="UserAccountNumber" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_firstAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span id="editConfirmBillPay_firstAddressValue" class="columndata"><ffi:getProperty name="payment.payee" property="Street" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_secondAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span id="editConfirmBillPay_secondAddressValue" class="columndata"><ffi:getProperty name="payment.payee" property="Street2" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_thirdAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="editConfirmBillPay_thirdAddressValue" class="columndata"><ffi:getProperty name="payment.payee" property="Street3" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_cityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="editConfirmBillPay_cityValue" class="columndata"><ffi:getProperty name="payment.payee" property="City" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_stateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="editConfirmBillPay_stateValue" class="columndata"><s:property value="%{payment.payee.stateDisplayName}"/>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_zipCodeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span id="editConfirmBillPay_zipCodeValue" class="columndata"><ffi:getProperty name="payment.payee" property="ZipCode" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_countryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span id="editConfirmBillPay_countryValue" class="columndata"><s:property value="%{payment.payee.countryDisplayName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_contactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span id="editConfirmBillPay_contactValue" class="columndata"><ffi:getProperty name="payment.payee" property="ContactName" /></span>
			</div>
		</div>
		<div class="blockRow">
			<span id="editConfirmBillPay_phoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/>:</span>
			<span id="editConfirmBillPay_phoneValue" class="columndata"><ffi:getProperty name="payment.payee" property="Phone" /></span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_amountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span id="editConfirmBillPay_amountValue" class="columndata" ><s:property value="payment.AmountValue.CurrencyStringNoSymbol"/><s:property value="%{payment.AmtCurrency}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_debitAmountLabel" class="sectionsubhead sectionLabel">
                   	<s:if test="%{payment.convertedAmount!=''}"><!-- do nothing --></s:if>
	        	    <s:else> <s:text name="jsp.billpay_40"/>:</s:else>
				</span>
				<span id="editConfirmBillPay_debitAmountValue" class="columndata" >
					 <s:if test="%{payment.convertedAmount!=''}"><!-- do nothing --></s:if>
					 <s:else>&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/></s:else>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_payDateLabel" class="columndata sectionsubhead sectionLabel"><s:text name="jsp.billpay_76"/>:</span>
				<span id="editConfirmBillPay_payDateValue" class="columndata" ><s:property value="%{payment.payDate}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_deliverByDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_deliver_by"/>:</span>
				<span id="editConfirmBillPay_deliverByDateValue" class="columndata"><s:property value="%{payment.deliverByDate}"/></span>	
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editConfirmBillPay_recurringLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
				<span id="editConfirmBillPay_recurringValue" class="columndata"><ffi:getProperty name="payment" property="Frequency" /></span>	
			</div>
			<div class="inlineBlock">
				<span id="editConfirmBillPay_frequencyLabel" valign="middle" class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
				<span id="editConfirmBillPay_frequencyValue" class="columndata"><s:if test="%{payment.openEnded}"><s:text name="jsp.default_446"/></s:if>
					<s:else><s:property value="%{payment.NumberPayments}"/></s:else>					
				</span>		
			</div>
		</div>
		<div class="blockRow">
			<span  id="editConfirmBillPay_memoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
			<span id="editConfirmBillPay_memoValue" class="columndata"><ffi:getProperty name="payment" property="Memo" /></span>
		</div>
		
	</div>
</div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<!-- <tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr> -->
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_125"/><br>
												</span></td>
										</tr>
                                       <!--  <tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr> -->
                                        <tr>
											<td id="editConfirmBillPay_payeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="editConfirmBillPay_payeeNickNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="NickName" />
											<s:property value="payment.Payee.NickName"/>
											</td>
											<td id="editConfirmBillPay_amountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td id="editConfirmBillPay_amountValue" class="columndata" colspan="2">

											<ffi:getProperty name="payment" property="AmountValue.CurrencyStringNoSymbol" />

											<ffi:getProperty name="EditPayment" property="AmountValue.CurrencyCode" />
											 <s:property value="payment.AmountValue.CurrencyStringNoSymbol"/>
												 <s:property value="payment.payee.PayeeRoute.CurrencyCode"/>
												 <s:property value="%{payment.AmtCurrency}"/>
											</td>
										</tr>
										<tr>
											<td id="editConfirmBillPay_payeeNameLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="editConfirmBillPay_payeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="payment.payee" property="Name" />
											
											</td>
											<td id="editConfirmBillPay_debitAmountLabel" class="columndata tbrd_l">
                                                <ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="notEquals" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
											    </ffi:cinclude>
                                                <ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="equals" >
                                                    &nbsp;
                                                </ffi:cinclude>
                                                 <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else> 
								        	    <div align="right">
													<span class="sectionsubhead">
													<s:text name="jsp.billpay_40"/> 
													</span>
												</div>
												</s:else>
                                            </td>

											<td id="editConfirmBillPay_debitAmountValue" class="columndata" colspan="2">
											<ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="notEquals" >
											    &#8776; <ffi:getProperty name="CONVERTED_CURRENCY_AMOUNT"/>
										        <ffi:getProperty name="EditPayment" property="Account.CurrencyCode"/>
											</ffi:cinclude>
											 <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  
								        	    	<s:property value="%{payment.account.currencyCode}"/>
								        	    </s:else>

											</td>
										</tr>

										<tr>
											<td id="editConfirmBillPay_payeeAccountLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="editConfirmBillPay_payeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="UserAccountNumber" />
											<ffi:getProperty name="payment.payee" property="UserAccountNumber" /></td>
											
											<td id="editConfirmBillPay_fromAccountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="editConfirmBillPay_fromAccountValue" class="columndata" colspan="2">
												<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
												<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>
												<s:set var="AccountDisplayTextTaskBean" value="#session.EditPayment.Account" scope="request" />
                								<ffi:process name="AccountDisplayTextTask"/>
                								<ffi:getProperty name="AccountDisplayTextTask" property="accountDisplayText"/>
                								 <s:property value="%{payment.account.accountDisplayText}"/>
											</td>
										</tr>
										<tr>
											<td id="editConfirmBillPay_firstAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="editConfirmBillPay_firstAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street" />
											<ffi:getProperty name="payment.payee" property="Street" /></td>
											<td id="editConfirmBillPay_payDateLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_76"/></span></div>
											</td>
											<td id="editConfirmBillPay_payDateValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayment" property="PayDate" />
											<s:property value="%{payment.payDate}"/>	
											</td>
										</tr>
										<tr>
											<td id="editConfirmBillPay_secondAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="editConfirmBillPay_secondAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street2" />
											<ffi:getProperty name="payment.payee" property="Street2" />
											</td>
											<td id="editConfirmBillPay_deliverByDateLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_deliver_by"/></span></div>
											</td>
											<td id="editConfirmBillPay_deliverByDateValue" class="columndata" colspan="2" valign="middle">
												<ffi:setProperty name="EditPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
												<ffi:getProperty name="EditPayment" property="DeliverByDate"/>												
												<s:property value="%{payment.deliverByDate}"/>			
											</td>											
										</tr>
										<tr>
											<td id="editConfirmBillPay_thirdAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="editConfirmBillPay_thirdAddressValue" class="columndata" colspan="2"
											><ffi:getProperty name="Payee" property="Street3" />
											<ffi:getProperty name="payment.payee" property="Street3" /></td>
											<td id="editConfirmBillPay_recurringLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_351"/></span></div>
											</td>
											<td id="editConfirmBillPay_recurringValue" class="columndata" colspan="2" valign="middle">
												<ffi:getProperty name="payment" property="Frequency" />
											</td>											
										</tr>
										<tr>
											<td id="editConfirmBillPay_cityLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="editConfirmBillPay_cityValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="City" />
											<ffi:getProperty name="payment.payee" property="City" />
											</td>
											<td id="editConfirmBillPay_frequencyLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_321"/></span>
												</div>
											</td>
											<td id="editConfirmBillPay_frequencyValue" class="columndata" colspan="2" valign="middle">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="EditPayment" property="NumberPayments" />
												</ffi:cinclude>
												<s:if test="%{payment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{payment.NumberPayments}"/>
												</s:else>
											</td>											
										</tr>
										<tr>
											<td id="editConfirmBillPay_stateLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="editConfirmBillPay_stateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{payment.payee.stateDisplayName}"/>
											</td>
											<td  id="editConfirmBillPay_memoLabel" valign="middle" class="tbrd_l">
													<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td id="editConfirmBillPay_memoValue" class="columndata" colspan="2" valign="middle">
											<ffi:getProperty name="payment" property="Memo" />
											</td>												
											<ffi:cinclude value1="${EditPayment.REGISTER_CATEGORY_ID}" value2="">
												<td  id="editConfirmBillPay_memoLabel" valign="middle" class="tbrd_l">
													<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
												</td>
												<td id="editConfirmBillPay_memoValue" class="columndata" colspan="2" valign="middle"><ffi:getProperty name="EditPayment" property="Memo" /></td>
											</ffi:cinclude>
											<ffi:cinclude value1="${EditPayment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
												<td id="editConfirmBillPay_registerCategoryLabel" class="columndata tbrd_l">
													<div align="right">
														<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span>
													</div>
												</td>
												<td id="editConfirmBillPay_registerCategoryValue" colspan="2" class="columndata">
													<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
													<ffi:list collection="RegisterCategories" items="Category">
														<ffi:cinclude value1="${Category.Id}" value2="${EditPayment.REGISTER_CATEGORY_ID}">
															<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
																<ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
																<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
														</ffi:cinclude>
													</ffi:list>
												</td>
											</ffi:cinclude>
										</tr>
										<tr>
											<td id="editConfirmBillPay_zipCodeLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="editConfirmBillPay_zipCodeValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<ffi:getProperty name="payment.payee" property="ZipCode" /></td>
											<td valign="middle" class="tbrd_l">
                                                <div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_388"/></span></div>
                                            </td>
											<td class="columndata" colspan="2" valign="middle">
											<s:if test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL}">
												<s:property value="%{payment.statusName}"/>
											</s:if>
											<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BATCH_INPROCESS ||
														payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED
														}">
													<s:text name="jsp.default_237"/>
											</s:elseif>
											<s:else>
												<s:text name="jsp.default_530"/>
											</s:else>
												<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	                                            	<ffi:getProperty name="Payment" property="StatusName" />
	                                            </ffi:cinclude>
	                                            <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		                                            <ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_BATCH_INPROCESS) %>">
		                                            	<s:text name="jsp.default_237"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>">
														<s:text name="jsp.default_237"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_BATCH_INPROCESS) %>" operator="notEquals">
														<ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>" operator="notEquals">
															<s:text name="jsp.default_530"/>
														</ffi:cinclude>
													</ffi:cinclude>
												</ffi:cinclude>
											</td>
										</tr>
										<tr>
											<td id="editConfirmBillPay_countryLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="editConfirmBillPay_countryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{payment.payee.countryDisplayName}"/></td></td>
											<td valign="middle" class="tbrd_l">
                                                <div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_347"/></span></div>
                                            </td>
											<td class="columndata" colspan="2" valign="middle">
											<ffi:getProperty name="Payment" property="TrackingID"/>
											 <s:property value="%{payment.TrackingID}"/></td>
										</tr>
										<tr>
											<td id="editConfirmBillPay_contactLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="editConfirmBillPay_contactValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ContactName" />
											<ffi:getProperty name="payment.payee" property="ContactName" /></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr>
										<tr>
											<td id="editConfirmBillPay_phoneLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div>
											</td>
											<td id="editConfirmBillPay_phoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Phone" />
											<ffi:getProperty name="payment.payee" property="Phone" /></td>
											<td class="columndata tbrd_l">&nbsp;</td>
											<td></td>
											<td></td>
										</tr>
                                        <ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td id="editConfirmBillPay_messageIndicator" class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude>
                                         <s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if>
                                        <tr>
											<td colspan="6"><div align="center">
													<br>
													<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
													All topics subscribed here can be found in pendingTransactionPortal.js
														<sj:a
															id="billPaymentDoneButton"
											                button="true"
											                buttonIcon="ui-icon-refresh"
															onClickTopics="billPaySendDone"
											        	><s:text name="jsp.default_175"/></sj:a>
											        </ffi:cinclude>
											        <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
											        <sj:a
											                button="true"
											                buttonIcon="ui-icon-refresh"
															onClickTopics="cancelBillpayForm,resetBillpayConfirmDiv"
											        	><s:text name="jsp.default_175"/></sj:a>
											        </ffi:cinclude>
									        		</div>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table> --%>
<s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
	<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
		<sj:a
			id="billPaymentDoneButton"
               button="true"
			onClickTopics="billPaySendDone"
       	><s:text name="jsp.default_175"/></sj:a>
       </ffi:cinclude>
       <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
       <sj:a
            button="true"
            summaryDivId="summary" buttonType="done"
			onClickTopics="showSummary,cancelBillpayForm,resetBillpayConfirmDiv"
       	><s:text name="jsp.default_175"/></sj:a>
       </ffi:cinclude>
</div>
</div>

<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>
<script type="text/javascript">
$(document).ready(function(){
	$( ".toggleClick" ).click(function(){ 
		if($(this).next().css('display') == 'none'){
			$(this).next().slideDown();
			$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
			$(this).removeClass("expandArrow").addClass("collapseArrow");
		}else{
			$(this).next().slideUp();
			$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
			$(this).addClass("expandArrow").removeClass("collapseArrow");
		}
	});
});

</script>