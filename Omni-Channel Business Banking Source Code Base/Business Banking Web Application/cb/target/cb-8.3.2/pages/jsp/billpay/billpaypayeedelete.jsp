<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_72')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:help id="payments_billpaypayeedelete" className="moduleHelpClass"/>
<%-- 
<ffi:object name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" id="GetBankIdentifierDisplayText" scope="session" />
<ffi:process name="GetBankIdentifierDisplayText"/>

Retrieve single and recurring payments of payees
<ffi:removeProperty name="<%=com.ffusion.tasks.billpay.Task.PAYMENTS%>"/>
<ffi:removeProperty name="<%=com.ffusion.tasks.billpay.Task.RECPAYMENTS%>"/>
<ffi:object name="com.ffusion.tasks.billpay.GetPayments" id="GetPayments"/>
<ffi:process name="GetPayments" />
<ffi:removeProperty name="GetPayments"/>

<ffi:object id="DeletePayee" name="com.ffusion.tasks.billpay.DeleteExtPayee" scope="session"/>
<ffi:setProperty name="DeletePayee" property="PayeeID" value="${ID}" />
<ffi:setProperty name="DeletePayee" property="Validate" value="PAYEENOTFOUND,PAYMENTSPENDING"/>
<ffi:setProperty name="DeletePayee" property="Process" value="TRUE"/>
<ffi:setProperty name="DeletePayee" property="AlternateServiceName" value="DirectoryBPService" />

<% 
	session.setAttribute("FFIDeletePayee", session.getAttribute("DeletePayee"));
	session.removeAttribute("ID"); 
%>

for localizing state name
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>
 --%>
<div class="approvalDialogHt">
<s:form action="/pages/jsp/billpay/deleteBillpayPayeeAction.action" method="post" name="billpayPayeeDelete" id="deleteBillpayPayeeFormId" >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden value="%{ID}" name="ID"></s:hidden>
   		<div  class="blockHead">Payee Info</div>
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_314"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.name}"/></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.nickName}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.contactName}"/></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.phone}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_19"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.userAccountNumber}"/></span>
				</div>
				<div class="inlineBlock">	
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.street}"/>
					</span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.street2}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.street3}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.city}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.stateDisplayName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.zipCode}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.countryDisplayName}"/>
					</span>
				</div>
			</div>
 		</div>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.bank.info"/></div>
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:property value="%{bankIdentifierDisplayText}"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.payeeRoute.BankIdentifier}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_account_number"/>:</span>
					<span class="columndata" >
						<s:property value="%{viewPayee.payeeRoute.accountID}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>
					<span class="columndata" >
					<s:if test="%{viewPayee.isGlobal}">
						<s:property value="%{viewPayee.payeeRoute.acctType}"/>
					</s:if>
					<s:else>
						<s:property value="%{accountTypeSelectList[viewPayee.payeeRoute.acctType]}"/>
					</s:else>
					</span>
				</div>
				 <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_account_currency"/>:</span>
					<span class="columndata" ><s:property value="%{viewPayee.payeeRoute.currencyCode}"/></span>
				</div>
				</ffi:cinclude>
			</div>
		</div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
				
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.default_166"/><br>
												</span></td>
										</tr>
											<tr>
												<td class="columndata ltrow2_color" colspan="6">
													<div align="center">
														<span class="sectionsubhead"><s:text name="jsp.billpay_22"/><br>
															<br>
														</span></div>
												</td>
											</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Name" />
											<s:property value="%{viewPayee.name}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street" />
											<s:property value="%{viewPayee.street}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="NickName" />
											<s:property value="%{viewPayee.nickName}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street2" />
											<s:property value="%{viewPayee.street2}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ContactName" />
											<s:property value="%{viewPayee.contactName}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street3" />
											<s:property value="%{viewPayee.street3}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Phone" />
											<s:property value="%{viewPayee.phone}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="City" />
											<s:property value="%{viewPayee.city}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="UserAccountNumber" />
											<s:property value="%{viewPayee.userAccountNumber}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{viewPayee.stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">
													<ffi:getProperty name="GetBankIdentifierDisplayText" property="BankIdentifierDisplayText"/>
													<s:property value="%{bankIdentifierDisplayText}"/>
													</span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="PayeeRoute.BankIdentifier" />
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<s:property value="%{viewPayee.zipCode}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_26"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="PayeeRoute.AccountID" />
											<s:property value="%{viewPayee.payeeRoute.accountID}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td class="columndata" colspan="2">
											<s:property value="%{viewPayee.countryDisplayName}"/>
											<ffi:getProperty name='CountryResource' property='Resource'/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_27"/></span></div>
											</td>
											<ffi:object id="GetTypeName" name="com.ffusion.tasks.util.Resource" />
											<ffi:setProperty name="GetTypeName" property="ResourceFilename" value="com.ffusion.beans.accounts.resources" />
											<ffi:setProperty name="GetTypeName" property="ResourceID" value="${Payee.PayeeRoute.AcctType}" />
											<ffi:process name="GetTypeName" />
											<td class="columndata" colspan="2">
											<ffi:getProperty name="GetTypeName" property="Resource" />
											<s:property value="%{accountTypeSelectList[viewPayee.payeeRoute.acctType]}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										  <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_25"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="PayeeRoute.CurrencyCode" />
											<s:property value="%{viewPayee.payeeRoute.currencyCode}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										</ffi:cinclude>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">

												</div>
											</td>
											<td colspan="4"><div align="center">
													<br>
													<sj:a id="cancelPayeeDeleteFormButton" 
										                button="true" 
														onClickTopics="closeDialog"
										        	><s:text name="jsp.default_82"/></sj:a>
										        <sj:a id="deleteBillpayPayeeLink" formIds="deleteBillpayPayeeFormId" targets="resultmessage" button="true"   
						  							title="%{getText('jsp.default_166')}"  onSuccessTopics="cancelBillpayPayeeComplete" onErrorTopics="errorDeleteBillpayPayee"
						  							effectDuration="1500" ><s:text name="jsp.default_166.1" />
						  						</sj:a>
													</div></td>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
			</table> --%>
<%-- 	<div align="center" class="columndata_error marginTop10"><s:text name="jsp.billpay_22"/></div> --%>
<div class="ffivisible" style="height:100px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter">
	<sj:a id="cancelPayeeDeleteFormButton" 
              button="true" 
		onClickTopics="closeDialog"
      	><s:text name="jsp.default_82"/></sj:a>
      <sj:a id="deleteBillpayPayeeLink" formIds="deleteBillpayPayeeFormId" targets="resultmessage" button="true"   
			title="%{getText('jsp.default_166')}"  onSuccessTopics="cancelBillpayPayeeComplete" onErrorTopics="errorDeleteBillpayPayee"
			effectDuration="1500" ><s:text name="jsp.default_162" />
		</sj:a>
</div>
</div>
</s:form>
