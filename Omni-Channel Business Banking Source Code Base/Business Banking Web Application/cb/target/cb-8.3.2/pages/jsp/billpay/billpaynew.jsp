<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_billpaynew" className="moduleHelpClass"/>
<div id="singleBillpayPanel" class="formPanel" align="center">
	<div class="leftPaneWrapper" role="form" aria-labelledby="loadTemplateHeader">
		<div class="leftPaneInnerWrapper">
			<div class="header"><h2 id="loadTemplateHeader"><s:text name="jsp.billpay_60" /></h2></div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
			<label id="lblTemplateLoad" for="SingleBillpayTemplateList">Template</label>
				<div id="templateForSingleBillpay" class="">
				<s:url id="ajax" value="billpaytempload.jsp">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		    	</s:url>
				<s:include value="%{ajax}" />
			</div>
			</div>
		</div>
		<div class="leftPaneInnerWrapper" role="form" aria-labelledby="loadPayeeHeader">
			<div class="header"><h2 id="loadPayeeHeader"><s:text name="jsp.billpay_load_payee" /> *</h2></div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
				<div><label id="lblLoadPayee">Payee</label></div>
				<div>
												<%-- <span class="sectionsubhead floatleft marginRight10"><s:text name="jsp.default_314"/><span class="required">*</span></span> --%>
					<%-- <select id="ChangePayeeId" class="txtbox" name="payeeID" size="1" onchange="ns.billpay.loadBillpayProp(this.value)">
						<ffi:getProperty name="Payee" property="Name" />
						<ffi:cinclude value1="" value2="${Payee.NickName}"
						 operator="notEquals" > (<ffi:getProperty name="Payee" property="NickName"/>
						 </ffi:cinclude><ffi:cinclude value1="" value2="${Payee.UserAccountNumber}" operator="notEquals" > - 
						 <ffi:getProperty name="Payee" property="UserAccountNumber"/></ffi:cinclude>)
						 </option>
					</select> --%>
					<s:set var="type" value="%{addRecPayment.paymentType}"></s:set>
					<!-- the check below is done when template load functionality is used -->
					<s:if test='%{#type=="TEMPLATE" || #type=="RECTEMPLATE"}'>
					<s:select list="addRecPayment.payee.iD" id="Payee" class="txtbox" name="payeeID" size="1"
					 listKey="addRecPayment.payee.iD" listValue="addRecPayment.payee.searchDisplayText" label="Select Option" onchange="ns.billpay.loadBillpayProp(this.value)"
					 aria-labelledby="lblLoadPayee" aria-required="true">
					</s:select>
					</s:if>
					<s:else>
					<select id="Payee" class="txtbox" name="payeeID" size="1" title="Payee select" onchange="ns.billpay.loadBillpayProp(this.value)"
					aria-labelledby="lblLoadPayee" aria-required="true">
						<ffi:getProperty name="Payee" property="Name" />
						<ffi:cinclude value1="" value2="${Payee.NickName}"
						 operator="notEquals" > (<ffi:getProperty name="Payee" property="NickName"/>
						 </ffi:cinclude><ffi:cinclude value1="" value2="${Payee.UserAccountNumber}" operator="notEquals" > - 
						 <ffi:getProperty name="Payee" property="UserAccountNumber"/></ffi:cinclude>)
						 </option>
					</select> 
					</s:else>
				</div>
			</div>
			<div>
				<span id="payeeErrorFinal" class="errorLabel"></span>
			</div>
		</div>
	</div>
	<div class="rightPaneWrapper w71">
	   		<div id="singleBillpayForm">
		        <%-- Old code <s:url id="ajax" value="billpaynewselect.jsp"> --%>
				<s:url id="ajax" value="billpaynewform.jsp">
			    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			    </s:url>
				<s:include value="%{ajax}"/>
	        </div>
	</div>
<!-- singleBillpayForm -->
</div><!-- singleBillpayPanel -->



<script language="JavaScript">

$(function(){
	 $("#Payee").lookupbox({
		"source":"/cb/pages/jsp/billpay/BillPayPayeesLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"35",
		"module":"billpayPayees",
	});
	
	 
});
</script>