<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_billpaypayee-company-add" className="moduleHelpClass"/>	
<ffi:setProperty name="AddPayee" property="NickName" value='<%=request.getParameter("nickName") %>' />
<ffi:setProperty name="AddPayee" property="UserAccountNumber" value='<%=request.getParameter("userAccountNumber") %>' />
<ffi:setProperty name="AddPayee" property="Process" value="TRUE" />
<ffi:cinclude value1="${Payee.PayeeRoute.CustAcctRequired}" value2="true" operator="equals">
<ffi:setProperty name="AddPayee" property="NoAccountNumber" value="false"/>
<ffi:setProperty name="AddPayee" property="Validate" value="USERACCOUNTNUMBER"/>
</ffi:cinclude>
<ffi:setProperty name="AddPayee" property="MaxUserAccountNumberLength" value="50" />
<ffi:process name="AddPayee" />
<ffi:removeProperty name="AddPayee" />
