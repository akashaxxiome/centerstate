<%-- <%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:cinclude value1="${payments_init_touched}" value2="true" operator="notEquals" >
	<s:include value="/pages/jsp/billpay/inc/payments-init.jsp" />
</ffi:cinclude>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_74')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%
	String startDate = request.getParameter("StartDate");
	String endDate = request.getParameter("EndDate");
	if(startDate != null && startDate.length() > 0){
		session.setAttribute("StartDate", startDate);
	}
	if(endDate != null && endDate.length() > 0){
		session.setAttribute("EndDate", endDate);
	}
	
%>

<%
	boolean initializeFlag = false;
	if(request.getParameter("Initialize") != null){
		initializeFlag = true;
	} // In case of portal page this will be request attribute.
	else if(request.getAttribute("Initialize") != null){
		initializeFlag = true;
	}
%>

<% if (initializeFlag){ %>
	<ffi:removeProperty name="Initialize" />

	<ffi:object id="GetDatesFromDateRange" name="com.ffusion.tasks.util.GetDatesFromDateRange" scope="session"/>
	<ffi:setProperty name="GetDatesFromDateRange" property="DateRangeValue" value="Last X Days and Next X Days"/>
	<ffi:setProperty name="GetDatesFromDateRange" property="PreviousDays" value="30"/>
	<ffi:setProperty name="GetDatesFromDateRange" property="FutureDays" value="30"/>
	<ffi:process name="GetDatesFromDateRange"/>
	<ffi:setProperty name="StartDate" value="${GetDatesFromDateRange.StartDate}" />
	<ffi:setProperty name="EndDate" value="${GetDatesFromDateRange.EndDate}" />
	
	<ffi:object id="GetPaymentAccounts" name="com.ffusion.tasks.billpay.GetPaymentAccounts" scope="session"/>
		<ffi:setProperty name="GetPaymentAccounts" property="AccountsName" value="BillPayAccounts"/>
		<ffi:setProperty name="GetPaymentAccounts" property="Reload" value="true"/>
	<ffi:process name="GetPaymentAccounts"/>

	<ffi:object id="GetPagedPendingPayments" name="com.ffusion.tasks.billpay.GetPagedPayments" scope="session"/>
	    <ffi:setProperty name="GetPagedPendingPayments" property="DateFormat" value="${UserLocale.DateFormat}" /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="DateFormatParam" value="${UserLocale.DateFormat}" />
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedPendingPayments" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetPagedPendingPayments" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedPendingPayments" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="ClearSortCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="SortCriteriaOrdinal" value="1" /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="SortCriteriaName" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_PAYDATE %>" /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="SortCriteriaAsc" value="False" /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="ClearSearchCriteria" value="" /> 
	    <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
    		<ffi:setProperty name="GetPagedPendingPayments" property="PageSize" value="" /> 
    	</ffi:cinclude>
    	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
			<ffi:setProperty name="GetPagedPendingPayments" property="PageSize" value="0"/>
    	</ffi:cinclude>
	    <ffi:setProperty name="GetPagedPendingPayments" property="AccountsCollection" value="PaymentAccounts" />
	    <ffi:setProperty name="GetPagedPendingPayments" property="CollectionSessionName" value="PendingPayments" /> 
	    <ffi:setProperty name="GetPagedPendingPayments" property="Status" value="<%= com.ffusion.beans.billpay.PaymentDefines.PAYMENT_STATUS_PENDING %>" /> 
		<ffi:setProperty name="GetPagedPendingPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPagedPendingPayments" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<ffi:setProperty name="GetPagedPendingPayments" property="Type" value="CURRENT,RECMODEL"/>
		</ffi:cinclude>
	
	Check if request is from Home -> Pending payments portal. If so, just process task and return
	<% if(null != session.getAttribute("fromPortalPage")){%>
		<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
			<ffi:process name="GetPagedPendingPayments"/>
			
			    <ffi:cinclude value1="${PaymentTemplates}" value2="" operator="equals">
			        <ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="session"/>
			        <ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
			        <ffi:process name="GetAllPaymentTemplates"/>
			    </ffi:cinclude>
		</ffi:cinclude>
	<% 
		return;// return back to calling page without processing anything.
	}
	%>
	
	    
	<ffi:object id="GetPagedApprovalPayments" name="com.ffusion.tasks.billpay.GetPagedPayments" scope="session"/>
	    <ffi:setProperty name="GetPagedApprovalPayments" property="DateFormat" value="${UserLocale.DateFormat}" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="DateFormatParam" value="${UserLocale.DateFormat}" />
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedApprovalPayments" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetPagedApprovalPayments" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedApprovalPayments" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="ClearSortCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="SortCriteriaOrdinal" value="1" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="SortCriteriaName" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_PAYDATE %>" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="SortCriteriaAsc" value="False" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="ClearSearchCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="PageSize" value="" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="AccountsCollection" value="PaymentAccounts" />
	    <ffi:setProperty name="GetPagedApprovalPayments" property="CollectionSessionName" value="PendingApprovalPayments" /> 
	    <ffi:setProperty name="GetPagedApprovalPayments" property="Status" value="<%= com.ffusion.beans.billpay.PaymentDefines.PAYMENT_STATUS_APPROVAL %>" /> 
		<ffi:setProperty name="GetPagedApprovalPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPagedApprovalPayments" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
        	<ffi:setProperty name="GetPagedApprovalPayments" property="Type" value="CURRENT,RECURRING"/>
        </ffi:cinclude>
	<ffi:object id="GetPagedCompletedPayments" name="com.ffusion.tasks.billpay.GetPagedPayments" scope="session"/>
	    <ffi:setProperty name="GetPagedCompletedPayments" property="DateFormat" value="${UserLocale.DateFormat}" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="DateFormatParam" value="${UserLocale.DateFormat}" />
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedCompletedPayments" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetPagedCompletedPayments" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedCompletedPayments" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="ClearSortCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="SortCriteriaOrdinal" value="1" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="SortCriteriaName" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_PAYDATE %>" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="SortCriteriaAsc" value="False" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="ClearSearchCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="PageSize" value="" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="AccountsCollection" value="PaymentAccounts" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="CollectionSessionName" value="CompletedPayments" /> 
	    <ffi:setProperty name="GetPagedCompletedPayments" property="Status" value="<%= com.ffusion.beans.billpay.PaymentDefines.PAYMENT_STATUS_COMPLETED %>" /> 
		<ffi:setProperty name="GetPagedCompletedPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPagedCompletedPayments" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<ffi:setProperty name="GetPagedCompletedPayments" property="Type" value="CURRENT,RECURRING"/>
		</ffi:cinclude>

		<ffi:object name="com.ffusion.tasks.billpay.SetPayment" id="SetPayment" scope="session"/>
		<ffi:setProperty name="SetPayment" property="Name" value="CompletedPayments"/>

    <ffi:cinclude value1="${PaymentTemplates}" value2="" operator="equals">
        <ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="session"/>
        <ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
        <ffi:process name="GetAllPaymentTemplates"/>
    </ffi:cinclude>
    
    <ffi:object id="GetLastPayments" name="com.ffusion.tasks.billpay.GetLastPayments" scope="request"/>
	<ffi:process name="GetLastPayments"/>
	<ffi:cinclude value1="${PaymentTemplates}" value2="" operator="equals">
		<ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="request"/>
		<ffi:process name="GetAllPaymentTemplates"/>
	</ffi:cinclude>
<% } else if (request.getParameter("Refresh") != null) { %>
	<ffi:setProperty name="GetPagedPendingPayments" property="FirstPage" value="true"/>
	<ffi:setProperty name="GetPagedApprovalPayments" property="FirstPage" value="true"/>
	<ffi:setProperty name="GetPagedCompletedPayments" property="FirstPage" value="true"/>
    <ffi:cinclude value1="${PaymentTemplates}" value2="" operator="equals">
        <ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="session"/>
        <ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
        <ffi:process name="GetAllPaymentTemplates"/>
    </ffi:cinclude>
<% } else { %>
    <ffi:cinclude value1="${PaymentTemplates}" value2="" operator="equals">
        <ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="session"/>
        <ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
        <ffi:process name="GetAllPaymentTemplates"/>
    </ffi:cinclude>
<% } %>
<ffi:cinclude value1="${StartDate}" value2="" operator="notEquals">
    	<ffi:setProperty name="GetPagedPendingPayments" property="StartDate" value="${StartDate}" /> 
    	<ffi:setProperty name="GetPagedApprovalPayments" property="StartDate" value="${StartDate}" /> 
    	<ffi:setProperty name="GetPagedCompletedPayments" property="StartDate" value="${StartDate}" /> 
</ffi:cinclude>
<ffi:cinclude value1="${EndDate}" value2="" operator="notEquals">
    	<ffi:setProperty name="GetPagedPendingPayments" property="EndDate" value="${EndDate}" /> 
    	<ffi:setProperty name="GetPagedApprovalPayments" property="EndDate" value="${EndDate}" /> 
    	<ffi:setProperty name="GetPagedCompletedPayments" property="EndDate" value="${EndDate}" /> 
</ffi:cinclude>

<ffi:process name="GetPagedPendingPayments" />
<ffi:process name="GetPagedApprovalPayments" />
<ffi:process name="GetPagedCompletedPayments" />



		
		<ffi:object id="GetPagedPayments" name="com.ffusion.tasks.billpay.GetPagedPayments" scope="session"/>
		<ffi:setProperty name="GetPagedPayments" property="DateFormat" value="${UserLocale.DateFormat}" /> 
	    <ffi:setProperty name="GetPagedPayments" property="DateFormatParam" value="${UserLocale.DateFormat}" />
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedPayments" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetPagedPayments" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedPayments" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' /> 
	    <ffi:setProperty name="GetPagedPayments" property="ClearSortCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedPayments" property="SortCriteriaOrdinal" value="1" /> 
	    <ffi:setProperty name="GetPagedPayments" property="SortCriteriaName" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_TEMPLATENAME %>" /> 
	    <ffi:setProperty name="GetPagedPayments" property="SortCriteriaAsc" value="False" /> 
	    <ffi:setProperty name="GetPagedPayments" property="ClearSearchCriteria" value="" /> 
	    <ffi:setProperty name="GetPagedPayments" property="PageSize" value="" /> 
	    <ffi:setProperty name="GetPagedPayments" property="AccountsCollection" value="PaymentAccounts" />
	    <ffi:setProperty name="GetPagedPayments" property="CollectionSessionName" value="PaymentTemplates" /> 
		<ffi:setProperty name="GetPagedPayments" property="Status" value="ACTIVE"/>
		<ffi:setProperty name="GetPagedPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPagedPayments" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPagedPayments" property="Type" value="TEMPLATE,RECTEMPLATE"/>
		<ffi:process name="GetPagedPayments"/>


	<ffi:object id="GetAllPaymentTemplates" name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" scope="session"/>
	<ffi:setProperty name="GetAllPaymentTemplates" property="DateFormat" value="MM/dd/yyyy" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="DateFormatParam" value="MM/dd/yyyy" />
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetAllPaymentTemplates" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetAllPaymentTemplates" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetAllPaymentTemplates" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
	<ffi:setProperty name="GetAllPaymentTemplates" property="ClearSortCriteria" value="" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="SortCriteriaOrdinal" value="1" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="SortCriteriaName" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_TEMPLATENAME %>" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="SortCriteriaAsc" value="False" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="ClearSearchCriteria" value="" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="PageSize" value="1000" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="AccountsCollection" value="PaymentAccounts" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="Status" value="<%= com.ffusion.beans.billpay.PaymentDefines.PAYMENT_STATUS_COMPLETED %>" />
	<ffi:setProperty name="GetAllPaymentTemplates" property="Type" value="CURRENT,RECURRING"/>
	<ffi:setProperty name="GetAllPaymentTemplates" property="DateFormat" value="${UserLocale.DateFormat}"/>
	<ffi:setProperty name="GetAllPaymentTemplates" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
 	<ffi:process name="GetAllPaymentTemplates"/>


<%
session.setAttribute("FFIGetPagedPendingPayments", session.getAttribute("GetPagedPendingPayments"));
session.setAttribute("FFIGetPagedApprovalPayments", session.getAttribute("GetPagedApprovalPayments"));
session.setAttribute("FFIGetPagedCompletedPayments", session.getAttribute("GetPagedCompletedPayments"));
session.setAttribute("FFIGetAllPaymentTemplates", session.getAttribute("GetAllPaymentTemplates"));
%>
		
<ffi:removeProperty name="PaymentTemplateFlag"/>
 --%>