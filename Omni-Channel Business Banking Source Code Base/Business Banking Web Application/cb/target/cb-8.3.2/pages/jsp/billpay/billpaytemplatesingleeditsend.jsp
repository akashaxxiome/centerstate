<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_billpaytemplatesinglesend" className="moduleHelpClass"/>
<%-- <%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %> --%>


<%--
<ffi:setProperty name="PaymentsUpdated" value="true"/> For Register Integration
<ffi:removeProperty name="Payment" /> --%>

<%--=================== PAGE SECURITY CODE - Part 1 Start ====================--%>
<%-- <%	session.setAttribute( "Credential_Op_Type", ""+ com.ffusion.beans.authentication.AuthConsts.OPERATION_TYPE_TRANSACTION) ; %>

<ffi:setProperty name="securitySubMenu" value="${subMenuSelected}"/>
<ffi:setProperty name='securityPageHeading' value='${PageHeading}'/>
 --%>
<%--=================== PAGE SECURITY CODE - Part 1 End ======================--%>
<%-- <ffi:removeProperty name="verified"/> --%>


<%-- for localizing state name --%>
<%-- <ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>
<ffi:setProperty name="EditPayment" property="AmountValue.CurrencyCode" value="${EditPayment.Payee.PayeeRoute.CurrencyCode}"/>
<ffi:setProperty name="EditPayment" property="AmtCurrency" value="${EditPayment.AmountValue.CurrencyCode}"/>
 --%>



<form action="" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="Refresh" value="true">
<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id=""><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id=""><s:property value="%{payment.payee.name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id=""><s:text name="jsp.default_15"/>:</span>
				<span id="" class="inlineSection floatleft labelValue"><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<div class="blockRow completed">
	<span id="billpayResultMessage"><span class="sapUiIconCls icon-accept "></span> <s:text name="jsp.billpay_120" /></span>
</div>
   		<div  class="blockHead toggleClick expandArrow">
			<s:text name="jsp.billpay.payee_summary" />: <s:property value="%{payment.payee.Name}"/> (<s:property value="%{payment.payee.NickName}"/> - <s:property value="%{payment.payee.userAccountNumber}"/>)
			<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
			<div class="toggleBlock hidden">
	   		<div class="blockContent">
	   			<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
						<span class="columndata" >
						     <s:property value="%{payment.payee.nickName}"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
						<span class="columndata" >
							<s:property value="%{payment.payee.UserAccountNumber}"/>
						</span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
						<span class="columndata" >
							<s:property value="%{payment.payee.contactName}"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
						<span class="columndata" >
							<s:property value="%{payment.payee.phone}"/>
						</span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
						<span class="columndata" >
							<s:property value="%{payment.payee.street}"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
						<span class="columndata" >
							<s:property value="%{payment.payee.street2}"/>
						</span>
					</div>
				</div>
				
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
						<span class="columndata"  >
							<s:property value="%{payment.payee.street3}"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
						<span class="columndata"  >
							<s:property value="%{payment.payee.city}"/>
						</span>
					</div>
				</div><div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
						<span  class="columndata">
							<s:property value="%{payment.payee.stateDisplayName}"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
						<span  class="columndata">
							<s:property value="%{payment.payee.zipCode}"/>
						</span>		
					</div>
				</div>
				<div class="blockRow">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
					<span class="columndata" >
						<s:property value="%{payment.payee.countryDisplayName}"/>
					</span>
				</div>
	 		</div>
	 	</div>
<div class="marginTop10"></div>
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/>:</span>											
					<span class="columndata" >
						<s:property value="%{payment.templateName}"/>
					</span>
					
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
					<span class="columndata" >
						<s:property value="%{payment.memo}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
					<span class="columndata" >
						<s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/>
						<s:property value="%{payment.AmountValue.CurrencyCode}"/>
					</span>	
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
	                     <s:if test="%{payment.convertedAmount!=''}">
		        	    	<!-- do nothing --> 
		        	    </s:if>
		        	    <s:else>
		        	    	
							<s:text name="jsp.billpay_40"/>:
						</s:else>											
					</span>
					<span class="columndata" >
						<s:if test="%{payment.convertedAmount!=''}">
		        	    	<!-- do nothing --> 
		        	    </s:if>
		        	    <s:else>
		        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
		        	    </s:else>												
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
					<span class="columndata" >
						<s:property value="%{payment.frequency}"/>
					</span>
					
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
					<span class="columndata" >
						<s:if test="%{payment.openEnded}">
							<s:text name="jsp.default_446"/>
						</s:if>
						<s:else>
							<s:property value="%{payment.NumberPayments}"/>
						</s:else>
					</span>
				</div>
			</div>
			
 		</div>
<s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
	<div class="required marginTop10"  align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
<sj:a  
             button="true" 
             summaryDivId="summary" buttonType="done"
	onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
     ><s:text name="jsp.default_175"/></sj:a>
</div>
</div>
</form>
<%-- 
<ffi:setProperty name="OpenEnded" value="false"/>
<ffi:removeProperty name="EditPayment" />
<ffi:removeProperty name="GetStateProvinceDefnForState"/> --%>

<%--=================== PAGE SECURITY CODE - Part 2 Start ====================--%>

<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>