<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="payments_billpaypayeebankselect" className="moduleHelpClass"/>

<%--<ffi:cinclude value1="${ACHCountryList}" value2="" operator="equals">--%>
<%--     <ffi:object name="com.ffusion.tasks.util.GetBankLookupStandardCountryNames" id="GetBankLookupStandardCountryNames" scope="session"/>
    <ffi:setProperty name="GetBankLookupStandardCountryNames" property="CollectionSessionName" value="BillPayCountryList"/>
    <ffi:process name="GetBankLookupStandardCountryNames"/>
    <ffi:removeProperty name="GetBankLookupStandardCountryNames"/> --%>
<%--</ffi:cinclude>--%>
<%-- 
<% if (request.getParameter("searchAgain") == null) { %>
	<ffi:object id="GetBillPayBanks" name="com.ffusion.tasks.billpay.GetBillPayBanks" scope="session"/>
<% } else { %>
	  <ffi:setProperty name="GetBillPayBanks" property="AchRTN" value="" />
<% } %>
<ffi:removeProperty name="searchAgain"/>
<%  String payeeSessionName; %>
    <ffi:getProperty name="payeeSessionName" assignTo="payeeSessionName"/>
<%  if (payeeSessionName != null) { %>
	<ffi:setProperty name="payeeSessionName" value="<%= payeeSessionName %>"/>
<% } %> --%>
<%--
<HTML>

	<head>
<ffi:include page="${PathExt}inc/timeout.jsp" />
		<TITLE>Bank Lookup</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="<ffi:getProperty name='ServletPath'/>FF.css" rel="stylesheet" media="screen">

	</head>

	<body>
--%>
        <div align="center">
			<table width="500" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				
				<tr>
					
					<td class="columndata" align="left" class="ltrow2_color">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							<%-- <tr>
								<td><span class="sectionhead">&gt; <s:text name="jsp.default_520" /></span></td>
							</tr> --%>
							<tr>
								<td>
									<s:form id="billPayBankFormId" action="billpaybankselect.jsp" method="post" name="BankForm">
									<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<input type="hidden" name="BillPayBankFormUsed" value="true">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_61" />&nbsp;</td>
											<td><input class="txtbox ui-widget-content ui-corner-all" type="text" name="financialInstitution.institutionName" size="30" 
											value=''></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_101" />&nbsp;</td>
											<td><input class="txtbox ui-widget-content ui-corner-all" type="text" name="financialInstitution.city" size="30" 
											value=""></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_386" />&nbsp;</td>
											<td><input class="txtbox ui-widget-content ui-corner-all" type="text" name="financialInstitution.state" size="30" 
											value=""></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_115" />&nbsp;</td>
											<td>
												<%-- <% String tempDefaultValue = ""; String tempCountry = ""; %>
	                                            <ffi:getProperty name="GetBillPayBanks" property="BankCountry" assignTo="tempDefaultValue"/>
												<% if (tempDefaultValue == null) tempDefaultValue = ""; %>
	                                            <% tempDefaultValue = ( tempDefaultValue == "" ? com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY : tempDefaultValue ); %>
	                                            <select id="billpay_bankCountryListSelectId" name="GetBillPayBanks.BankCountry" style="width: 162;"  class="txtbox">
	                                            <ffi:list collection="BillPayCountryList" items="country">
	                                                <ffi:getProperty name="country" property="BankLookupCountry" assignTo="tempCountry"/>
	                                                <option value="<ffi:getProperty name='tempCountry'/>" <%= ( tempDefaultValue.equals( tempCountry ) ? "selected" : "" ) %>><ffi:getProperty name="country" property='Name'/></option>
	                                            </ffi:list>
	                                            </select></td>
												<script>
													$("#billpay_bankCountryListSelectId").selectmenu({width: 200});
												</script> --%>
												<s:select id="billpay_bankCountryListSelectId" list="countryDefns" name="financialInstitution.country" 
												listKey="bankLookupCountry" listValue="Name" class="txtbox" theme="simple"
												value="%{financialInstitution.country}"></s:select>
											</td>
											<script>
												$("#billpay_bankCountryListSelectId").selectmenu({width: 240});
											</script>
										</tr>
										<!-- <tr>
											<td align="right" class="sectionhead">&nbsp;</td>
											<td>&nbsp;</td>
										</tr> -->
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_201" />&nbsp;</td>
											<td><input class="txtbox ui-widget-content ui-corner-all" type="text" name="financialInstitution.achRoutingNumber" size="30"></td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" colspan="2" class="ui-widget-header customDialogFooter">												
												<%-- <input class="submitbutton" type="submit" value="SEARCH" >&nbsp; --%>
												<sj:a id="searchBankBillPayPayee3" 
												  button="true" 
											      onClickTopics="searchBankBillpayPayeeTopics3"
												><s:text name="jsp.default_373"/></sj:a>
												<%-- <input class="submitbutton" type="button" value="CLOSE" onclick="self.close();return false;"> --%>
												<sj:a id="billpay_closeSearchBankbuttonID"
														button="true"
														onClickTopics="billpay_closeSearchBankListOnClickTopics"
												><s:text name="jsp.default_102"/></sj:a>
											</td>
										</tr>
									</table>
									&nbsp;
									</s:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<%--
	</body>
--%>
