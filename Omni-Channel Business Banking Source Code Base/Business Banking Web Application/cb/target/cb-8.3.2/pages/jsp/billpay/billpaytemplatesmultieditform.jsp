<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
        

<ffi:help id="payments_billpaymultipletemplateedit" className="moduleHelpClass"/>

<script language="javascript">
$(function(){
	//$("#AccountID").combobox({'size':'60'});
	
	$("#AccountID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"billpaymultinew",
		"size":"60",
		"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action"
	});
});

function clearRow(index){
$('#amount'+index).val('');
$('#memo'+index).val('');
}

</script>


<s:form id="BillpayMultipleNew" namespace="/pages/jsp/billpay" validate="true" action="editBillpayBatchTemplateAction_verify" method="post" name="BillpayMultipleNew" theme="simple">
<h1 class="portlet-title" style="display:none;" id="PageHeading"><s:text name="jsp.billpay_51"/></h1>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

<div class="leftPaneWrapper" role="form" aria-labeledby="save">
		<div class="leftPaneInnerWrapper">
			<div class="header"><h2 id="save"><s:text name="jsp.default_366" /></h2></div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
				<div id="multipleBillPayTemplate_templateNameLabel" class="sectionsubhead">
					<label for="templateName"><s:text name="jsp.billpay_template_name"/></label><span class="required" title="required">*</span></div>
			<div class="marginTop10">
				<input type="text" id="templateName" aria-required="true" class="ui-widget-content ui-corner-all" name="multiplePayment.templateName" size="32"maxlength="32" 
					value="<s:property value="%{multiplePayment.templateName}"/>"/>
			</div>
			<div class="marginTop10"><span id="multiplePayment.templateNameError"></span></div>
			</div>
		</div>
		<div class="leftPaneInnerWrapper" role="form" aria-labeledby="fromAccount">
			<div class="header" id=""><h2 id="fromAccount"><s:text name="jsp.default_217" /> </h2>*</div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
				<s:select 
					id="AccountID" 
					list="multiplePayment.acountId"
					listKey="multiplePayment.acountId" listValue="multiplePayment.account.accountDisplayText" 
					name="multiplePayment.acountId" 
					aria-required="true"
					value="%{multiplePayment.acountId}"
					class="txtbox" >
			</s:select>
				<div class="marginTop10 floatleft"><span id="multiplePayment.accountIDError"></span> <span id="acountIdError"></span></div>
			</div>
		</div>
</div>

<div class="rightPaneWrapper w71">
<div><span id="amountError"></span></div>
<div><span id="paymentsError"></span></div>
<div class="paneWrapper" role="form" aria-labeledby="paymentDetail">
  	<div class="paneInnerWrapper">
<div class="header"><h2 id="paymentDetail"><s:text name="jsp.billpay_77"/></h2></div>
<div class="paneContentWrapper">
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData formTablePadding10">
	<tr>
		<td colspan="2" align="center">
			<span id="amountError"></span>
		</td>
	</tr>
<s:iterator value="%{multiplePayment.payments}" status="listStatus" var="payment">
	<tr>
		<td colspan="2" id="multipleBillPayTemplate_payeeNickNameValue<s:property value="%{#listStatus.index}"/>" class="">
			<h3 class="transactionHeading">
				<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
				<s:property value="#payment.payee.userAccountNumber"/>)
			</h3>
		</td>
	</tr>
	<tr>
		<td class="sectionsubhead nowrap" width="30%" align="left"><label for="amount<s:property value="%{#listStatus.index}"/>"><s:text name="jsp.default_43"/></label><span class="required" title="required">*</span></td>
		<td class="sectionsubhead" colspan="3" width="34%" nowrap align="left">
			<label for="memo<s:property value="%{#listStatus.index}"/>"><s:text name="jsp.default_279"/></label>
        </td>
	</tr>	
		<tr>
			<td class="columndata" nowrap align="left">
			<input type="text" aria-required="true" class="amountCls ui-widget-content ui-corner-all" value="<s:property value='%{paymentBatchMap[#payment.payee.iD].AmountValue.AmountValue}'/>"
			name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].amount"
			size="10" maxlength="16" id="amount<s:property value="%{#listStatus.index}"/>" />
			
			<s:property value="#payment.payee.payeeRoute.currencyCode"/>
 			<s:hidden value="%{#payment.payee.payeeRoute.currencyCode}"
			name="multiplePayment.payments[%{#listStatus.index}].amtCurrency"></s:hidden>
			<s:hidden name="multiplePayment.payments[%{#listStatus.index}].iD" value="%{paymentBatchMap[#payment.payee.iD].iD}"></s:hidden>
			<s:hidden name="multiplePayment.payments[%{#listStatus.index}].status" value="%{paymentBatchMap[#payment.payee.iD].status}"></s:hidden></td>
            <td align="left">
				<input type="text" class="ui-widget-content ui-corner-all" 
                name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].memo" 
                maxlength="64" size="33" value="<s:property value='%{paymentBatchMap[#payment.payee.iD].memo}'/>"
                id='memo<s:property value="%{#listStatus.index}"/>'/>
            </td>
        </tr>
        <tr>
			<td><span id="amount<s:property value='%{#listStatus.index}'/>Error"></span></td>
			<td><span id="memo<s:property value='%{#listStatus.index}'/>Error"></span></td>
		</tr>

	</s:iterator>
        <ffi:cinclude value1="${showAmountTotal}" value2="true" operator="equals">
        <tr>
			<td class="sectionsubhead" nowrap align="right"><s:text name="jsp.default_431"/>:</td>
            <td class="sectionsubhead" nowrap colspan="2">
                &nbsp;
                <span id="Total">--------</span>
                <span id="payeecommoncurrency1"></span>
            </td>
		</tr>
        </ffi:cinclude>

</table>

<div align="center" class="required marginTop10">* <s:text name="jsp.default_240"/></div>
<div class="btn-row">
   	
	<sj:a id="cancelMultiFormButton" 
	    button="true" 
	    summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
	><s:text name="jsp.default_82"/></sj:a>

	<sj:a 
		id="verifyMutiBillpaySubmit"
		formIds="BillpayMultipleNew"
	    targets="verifyDiv" 
        button="true"
        validate="true"
		onBeforeTopics="beforeVerifyBillpayForm"
         validateFunction="customValidation"	       
        onCompleteTopics="verifyBillpayFormComplete"
		onErrorTopics="errorVerifyBillpayForm"
	    onSuccessTopics="successVerifyBillpayForm"
	 ><s:text name="jsp.default_291"/></sj:a>
</div></div></div></div></div>
<%-- </ffi:cinclude> --%>
<%-- ================= MAIN CONTENT END ================= --%>

</s:form>
