<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.billpay.PaymentDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_billpaytemplatesmultieditsend" className="moduleHelpClass"/>
<%-- 
 <s:set var="tmpI18nStr" value="%{getText('jsp.billpay_12')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>




		<div align="center">
			<div align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">



<ffi:object id="AccountTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
<ffi:object id="CurrencyCodeTable" name="com.ffusion.beans.util.StringTable" scope="session"/>

<ffi:list collection="BillPayAccounts" items="BAccount1">
   <ffi:setProperty name="AccountTable" property="Key" value="${BAccount1.ConsumerDisplayText}" />
   <ffi:setProperty name="AccountTable" property="Value" value="${BAccount1.CurrentBalance.Amount}" />

   <ffi:setProperty name="CurrencyCodeTable" property="Key" value="${BAccount1.ConsumerDisplayText}" />
   <ffi:setProperty name="CurrencyCodeTable" property="Value" value="${BAccount1.CurrencyCode}" />
</ffi:list> --%>


<form action="<ffi:urlEncrypt url="${SecurePath}payments/billpay.jsp?Refresh=true"/>" method="POST" name="MultiBillpayNewSend" id="MultiBillpayNewSend">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
		<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 5px;">
			<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="multipleBillPayTemplateConfirm_templateName"><s:text name="jsp.billpay_template_name"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePaymentView.TemplateName}"/>
			</div>
			<div style="width:100%" class="marginTop10 floatleft">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="multipleBillPayTemplateConfirm_fromAccount"><s:text name="jsp.default_217"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePaymentView.payments[0].account.accountDisplayText}"/></span>
			</div>
		</div>
	</div>
	<div class="leftPaneInnerWrapper totalAmountWrapper" id="multipleBillPayTemplateConfirm_total">
	                <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
	                <s:if test="%{isMultiCurrency}">
	                	&#8776;
	                </s:if>
	                 <span id="esimatedTotal">
	                 	<s:property value="%{total}"/>
	                 </span> 
	                 <span id="estimatedTotalCurrency">
	                 <%-- <ffi:getProperty name="selectedCurrencyCode"/> --%>
	                 <s:property value="%{multiplePaymentView.payments[0].account.currencyCode}"/>
	                 </span>
	</div>
</div>
<div class="confirmPageDetails">
<div class="blockRow completed">
			<span id="billpayResultMessage"><span class="sapUiIconCls icon-accept "></span> <s:text name="jsp.billpay_122"/></span>
</div>

<div class="blockWrapper">
<s:set var="payeeCount" value="1"></s:set>
<s:iterator value="%{multiplePaymentView.payments}" status="listStatus" var="payment">
	<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
		<h3 class="transactionHeading"><%-- <s:text name="jsp.default_313"/> <s:property value="#payeeCount"/> --%>
			<span id="multipleBillPayTemplateConfirm_payeeNickNameValue_<s:property value="%{#listStatus.index}"/>" class="columndata">
			 <s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
			<s:property value="#payment.payee.userAccountNumber"/>)
			</span>
		</h3>
		<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<%-- <span id="multipleBillPayTemplateConfirm_payeeNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_313"/>:</span> --%>
					<span id="multipleBillPayTemplateConfirm_amountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
					<span id="multipleBillPayTemplateConfirm_amountValue_<s:property value="%{#listStatus.index}"/>" class="columndata">
						<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
						<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/></span>
				</div>
				<div class="inlineBlock">
					<span id="multipleBillPayTemplateConfirm_memoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
					<span id="multipleBillPayTemplateConfirm_memoValue_<s:property value="%{#listStatus.index}"/>" class="columndata">
					 	<s:property value="#payment.memo"/>
					 </span>
				</div>
			</div>
 		</div>
 		<s:set var="payeeCount" value="#payeeCount+1"></s:set>
	</s:if>
</s:iterator>
</div>

<ffi:setProperty name="BillPayAccounts" property="Filter" value="All" />
<div class="btn-row">
    <ffi:cinclude value1="${DisplayEstimatedAmountKey}" value2="true">
        <span class="required"><ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals">&#8776; <s:text name="jsp.default_241"/></ffi:cinclude></span><br><br>
        <ffi:removeProperty name="DisplayEstimatedAmountKey"/>
    </ffi:cinclude>
    
	<sj:a id="cancelMultiBillpaySendForm" 
             button="true" 
             summaryDivId="summary" buttonType="done"
			onClickTopics="showSummary,showMultiTempSummary"
     	><s:text name="jsp.default_175"/></sj:a>

</div></div>
</form>


<%-- ================= MAIN CONTENT END ================= --%>
<%-- <script language="javascript">
 var total = 0;
 var total1 = 0;
 var tempValue=0;
 var tempValue1=0;
 var frm = document.getElementById("MultiBillpayNewSend");
 var length=0;
 var length1=0;
 if(frm.convertedAmount != null)
 
 length =frm.convertedAmount.length;
 if(frm.convertedAmount1 != null)
  length1=frm.convertedAmount1.length;
 
 var cnt=0;
  
 if(length == 0 ||isNaN(length)){
 
  if(frm.convertedAmount != null){
 tempValue= frm.convertedAmount.value;

 if (tempValue != "" && !isNaN(tempValue))
          {
              total += parseFloat(tempValue);
         }
 
  }
 }
 else{
 
 for(cnt=0;cnt<length;cnt++){
 
 tempValue= frm.convertedAmount[cnt].value;

 
 if (tempValue != "" && !isNaN(tempValue))
         {
             total += parseFloat(tempValue);
        }
 
     }
 }
 /*Code for ESt total calculation if currency is same*/
 
 if(length1 == 0 ||isNaN(length1)){
  if(frm.convertedAmount1 != null)
  {
  
  tempValue1= frm.convertedAmount1.value;
 
  if (tempValue1 != "" && !isNaN(tempValue1))
           {
               total1 += parseFloat(tempValue1);
          }
  
   }
  
  }
  else{
  
  for(cnt=0;cnt<length1;cnt++){
  
  tempValue1= frm.convertedAmount1[cnt].value;
 
  if (tempValue1 != "" && !isNaN(tempValue1))
          {
              total1 += parseFloat(tempValue1);
         }
  
      }
 }
 
 
 total=total+total1;
 total = ns.common.formatTotal(total);
 var checkMultiCurrency= document.getElementById("checkMultiCurrencyValue").value;
 var payeeCommonCurrency=document.getElementById("payeeCommonCurrency").value;
 var bankAccountCurrency=document.getElementById("bankAccountCurrency").value;
  if(checkMultiCurrency =="true")
{	

   document.getElementById("esimatedTotalSend").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
   
}
 if(checkMultiCurrency == "false"){
 if(payeeCommonCurrency == "" || payeeCommonCurrency != bankAccountCurrency){
 	
   document.getElementById("esimatedTotalSend").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
 }
 else{	
 
  document.getElementById("esimatedTotalSend").innerHTML ="";
  document.getElementById("esimatedTotalLabel").innerHTML ="";
  document.getElementById("esimatedTotalCurrency").innerHTML ="";
  }
 }
</script>

<ffi:cinclude value1="${isMultiCurrency}" value2="" operator="notEquals">
<ffi:removeProperty name="isMultiCurrency" />
</ffi:cinclude> --%>