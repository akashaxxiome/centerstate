<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="SetMappingDefinition"/>
</ffi:cinclude>
 <%--this is used for public --%>
  <% 
 	request.setAttribute("actionName","/pages/fileupload/billpayFileUploadAction.action"); 
    String fileType = request.getParameter("fileType");
	String SetMappingDefinition_ID = request.getParameter("SetMappingDefinition.ID");
 %>  
 <ffi:setProperty name="fileType" value="<%=fileType %>"/>
 <ffi:setProperty name="ProcessImportTask" value="CheckPaymentImport"/>
 <ffi:setProperty name="FileUpload" property="FileType" value="<%=fileType %>"/> 
 <ffi:setProperty name="SetMappingDefinition" property="ID" value="<%=SetMappingDefinition_ID %>"/> 
  <% 
	session.setAttribute("FFIFileUpload", session.getAttribute("FileUpload"));
	session.setAttribute("FFIProcessPaymentImport", session.getAttribute("ProcessPaymentImport"));
	session.setAttribute("FFINewReportBase", session.getAttribute("NewReportBase"));
	session.setAttribute("FFIGenerateReportBase", session.getAttribute("GenerateReportBase"));   
	session.setAttribute("FFICheckPaymentImport", session.getAttribute("CheckPaymentImport"));
%>
<s:include value="/pages/jsp/billpay/billpayuploader.jsp"/>

<script>
	ns.common.popupAfterUpload = false;
</script>