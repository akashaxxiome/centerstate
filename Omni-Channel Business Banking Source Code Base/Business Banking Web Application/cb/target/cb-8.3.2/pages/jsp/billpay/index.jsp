<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>



<s:include value="inc/index-pre.jsp"/>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/billpay/billpay%{#session.minVersion}.css'/>"></link>
<script type="text/javascript" src="<s:url value='/web/js/billpay/billpay%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/billpay/billpay_grid%{#session.minVersion}.js'/>"></script>

        <div id="desktop" align="center">          

			<div id="operationresult">
				<div id="resultmessage"><s:text name="jsp.default_498"/></div>
			</div><!-- result -->

			<div id="appdashboard">
				 <%-- <s:include value="billpay_dashboard.jsp" /> --%>
				 <s:action namespace="/pages/jsp/billpay" name="dashboardAction_loadDashboard" executeResult="true"></s:action>
            </div>
			<div id="details" >
				<s:include value="/pages/jsp/billpay/billpay_details.jsp"/>
			</div>

			<div id="fileupload">
				<s:include value="paymentbillpayimportconsole.jsp"/>
			</div>
			<div id="customMappingDetails">
				<s:include value="/pages/jsp/custommapping_workflow.jsp"/>
			</div>
			<div id="mappingDiv">
 	    	</div>

			<div id="summary">
				<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
					<ffi:cinclude value1="${summaryToLoad}" value2="Template"	operator="notEquals">
						<ffi:cinclude value1="${summaryToLoad}" value2="Payee"	operator="notEquals">
							<s:action namespace="/pages/jsp/billpay" name="initBillpay_summary" executeResult="true"/>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="Template"	operator="equals">
						<s:action namespace="/pages/jsp/billpay" name="getBillpayTemplatesAction_loadSummaryPage" executeResult="true">
							<s:param name="moduleId" value="'BillTemplate'"></s:param>
						</s:action>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="Payee"	operator="equals">
						<s:action namespace="/pages/jsp/billpay" name="getBillpayPayeesAction_goToPayees" executeResult="true"/>
					</ffi:cinclude>
				</ffi:cinclude>
			</div>
    </div>
<script type="text/javascript">
	$(document).ready(function(){
		ns.billpay.showBillPayDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>