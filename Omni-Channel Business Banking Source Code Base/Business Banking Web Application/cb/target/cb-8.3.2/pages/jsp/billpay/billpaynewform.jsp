<%@page import="com.ffusion.beans.billpay.Payment"%>
<%@page import="com.ffusion.tasks.billpay.Task"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_billpaynewpop" className="moduleHelpClass"/>
<span class="shortcutPathClass" style="display:none;">pmtTran_billpay,addSingleBillpayLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Payments</span>
<script language="JavaScript">
$(function(){
	
	$("#AccountID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"billpay",
		"size":"35",
		"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action"
	});
	$("#Frequency").selectmenu({width: 110});
	modifyFrequencyOnLoad();
});

function onSubmitCheck(submitRefresh)
{

    if (submitRefresh)
    {
        document.BillpayNew.submit();
    }
}
function toggleRecurring(){
	if ($('input[name="addRecPayment.openEnded"]')[1].checked == true){
		$('input[name="addRecPayment.numberPayments"]').attr('disabled',false);
	} else {
		$('input[name="addRecPayment.numberPayments"]').attr('disabled',true);
	}
}

function modifyFrequencyOnLoad() {
	/* this check is because when template is loaded for non recurring template the below element is not rendered so javascipt fails */
	if($('input[name="addRecPayment.openEnded"]')[1]!=undefined){
		$('input[name="addRecPayment.openEnded"]')[1].checked = true;
		if ($('input[name="addRecPayment.numberPayments"]').val()=="999") {
			$('input[name="addRecPayment.openEnded"]')[0].checked = true;
			$('input[name="addRecPayment.openEnded"]')[1].checked = false;
		}
		modifyFrequency();
	}
}

function modifyFrequency( ){
	if ($('#Frequency option:selected').val() == "None"){
		$('input[name="addRecPayment.openEnded"]')[0].disabled = true;
		$('input[name="addRecPayment.openEnded"]')[1].disabled = true;
		$('input[name="addRecPayment.openEnded"]')[0].checked = false;
		$('input[name="addRecPayment.openEnded"]')[1].checked = true;
		$('input[name="addRecPayment.numberPayments"]').attr('disabled',true);
		$('input[name="addRecPayment.numberPayments"]').val('1');
	} else {
		$('input[name="addRecPayment.openEnded"]')[0].disabled = false;
		$('input[name="addRecPayment.openEnded"]')[1].disabled = false;
		var numberOfPayments = $('input[name="addRecPayment.numberPayments"]').val();
		if ( parseInt(numberOfPayments)<parseInt("2") ){
			$('input[name="addRecPayment.numberPayments"]').val('2');
		}
		toggleRecurring();
	}

}

ns.billpay.loadBillpayProp = function(id) {
	var urlString = '/cb/pages/jsp/billpay/billpaynew.jsp';
	$.ajax({
		   url:"/cb/pages/jsp/billpay/addBillpayAction_loadPayee.action?payeeID="+id,
		   type: "POST",
		   data: $("#BillpayNew").serialize(),
		   success: function(data){
		   $("#payeeDetailsJsp").html('');
		   $("#payeeDetailsJsp").html(data);
		   $(".selecteBeneficiaryMessage").hide();
		   $.publish("common.topics.tabifyDesktop");
		   if(accessibility){
			   $("#inputDiv").setInitialFocus();
		   }
		   $('#expandBillpayPayee').removeClass('hidden');
	   }
	});
}

</script>
<s:form id="BillpayNew" namespace="/pages/jsp/billpay" validate="false" action="addBillpayAction_verify" method="post" name="BillpayNewForm" theme="simple">
<span id="payee.idError"></span>
<div align="left"><span id="payeeDetailsError" class="errorLabel"></span></div>
<span style="display:none;" id="PageHeading"><s:text name="jsp.billpay_9"/></span>
<div align="center">
<div class="paneWrapper" role="form" aria-labelledby="payeeDetail">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="payeeDetail"><s:text name="jsp.billpay.payee.info"/></h2></div>
   		<div class="paneContentWrapper">
			<!-- form errors -->
			<div><ul id="formerrors" style="margin: 0"></ul></div>
			<div id="payeeDetailsJsp">
				<!-- payee table -->
                <jsp:include page="payeeDetails.jsp"></jsp:include>
            </div>
            <div class="instructions" style="padding: 0 !important;">
				<!-- to load the instruction on selection of a payee, a javascript is written on the included page to append the message to this td. -->
			</div>
		</div>
	</div>
</div>
<%-- include page header --%>		
<div class="paneWrapper marginTop20" role="form" aria-labelledby="inputFormHeader">	
	<div class="paneInnerWrapper">
	<div class="header"><h2 id="inputFormHeader"><s:text name="jsp.billpay_77"/></h2></div>
	<div class="paneContentWrapper">

<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="RegEnabledAccounts" value="<ffi:list collection="PaymentAccounts" items="Account"><ffi:cinclude value1="${Account.REG_ENABLED}" value2="true"><ffi:getProperty name="Account" property="ID"/>,</ffi:cinclude></ffi:list>">
<table border="0" class="tableData formTablePadding5" cellspacing="0" cellpadding="0" width="100%">
<s:set var="type" value="%{addRecPayment.paymentType}"></s:set>
<div class="hidden"><span id="payeeError" class="payeeErrValidateCls"></span></div>
										
	<%-- <tr class="header"><td colspan="2"><h2><s:text name="jsp.billpay_77"/></h2></td></tr> --%>
	<!-- payment table -->
	<tr>
		<td width="50%" class="sectionsubhead"><label for="AccountID"><s:text name="jsp.default_15"/></label><span class="required" title="required">*</span></td>
		<td width="50%" class="sectionsubhead"><label for="Amount"><s:text name="jsp.default_43"/></label><span class="required" title="required">*</span></td>
	</tr>
	<tr>
		<td><s:if test='%{#type=="TEMPLATE" || #type=="RECTEMPLATE"}'>
			<s:select 
					id="AccountID" 
					list="addRecPayment.Account.accountDisplayText"
					listKey="addRecPayment.Account.ID" listValue="addRecPayment.Account.accountDisplayText" 
					name="addRecPayment.accountID" 
					value="%{addRecPayment.Account.accountDisplayText}"
					class="txtbox"
					aria-labelledby="AccountID"
					aria-required="true">
			</s:select>
			</s:if>
			<s:else>
			<select class="txtbox" id="AccountID" name="addRecPayment.accountID" size="1" onChange="" aria-labelledby="AccountID"
					aria-required="true"></select>
			</s:else>
		</td>
		<td><s:textfield name="addRecPayment.amount" id="Amount" size="12" maxlength="16" value="%{addRecPayment.AmountValue.AmountValue}" cssClass="ui-widget-content ui-corner-all" aria-required="true"/><span id="payeeCurrencyCode"></span>
			<!-- a java scipt is written on included jsp to load the payee curreny and append to this span -->
		</td>																
	</tr>
	<tr>
		<td><span id="addRecPayment.accountIDError"></span><span id="accountIDError"></span></td>
		<td><span id="amountError"></span></td>
	</tr>
	<tr>
		<td class="sectionsubhead"><label for="PayDate"><s:text name="jsp.billpay_76"/></label> <span class="required" title="required">*</span></td>
		<td class="sectionsubhead">
			<s:if test='%{#type=="RECTEMPLATE" || #type=="PAYMENT"}'>
				<label for="Frequency">
					<s:text name="jsp.default_351"/>
				</label>	
			</s:if>
			</label>
		</td>
	</tr>
	<tr>
		<td>
				<sj:datepicker
					value="%{addRecPayment.payDate}"
					id="PayDate"
					name="addRecPayment.payDate"
					label="%{getText('jsp.default_137')}"
					maxlength="10"
					buttonImageOnly="true"
					cssClass="ui-widget-content ui-corner-all" 
					aria-required="true"/>
			<script>
				var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=3&calDisplay=select&calForm=FormName&calTarget=addRecPayment.PayDate"/>';
				ns.common.enableAjaxDatepicker("PayDate", tmpUrl);
			</script>
		</td><ffi:removeProperty name="paymentDate"/>
		<td>
			<s:if test='%{#type=="RECTEMPLATE" || #type=="PAYMENT"}'>
				<s:if test='%{#type=="RECTEMPLATE"}'>
				<s:select id="Frequency" list="frequencyList" name="addRecPayment.frequency" value="%{addRecPayment.frequency}"
					class="txtbox" onChange="modifyFrequency();">
					</s:select>
				</s:if>
				<s:else>
					<s:select id="Frequency" list="frequencyList" name="addRecPayment.frequency" value="%{addRecPayment.frequency}"
					headerKey="None" headerValue="%{getText('jsp.billpay_65')}"
					class="txtbox" onChange="modifyFrequency();">
					</s:select>
				</s:else>
			</s:if>
			<s:if test='%{#type=="RECTEMPLATE" || #type=="PAYMENT"}'>
					<input type="radio" name="addRecPayment.openEnded" value="true" disabled onClick="toggleRecurring();">
					<span class="columndata"><s:text name="jsp.default_446"/></span>
					<input type="radio" name="addRecPayment.openEnded" value="false" disabled checked onClick="toggleRecurring();">
					<span class="columndata"><s:text name="jsp.default_2"/></span>
					<input type="text" name="addRecPayment.numberPayments" value="<s:property value="%{addRecPayment.numberPayments}"/>" size="3" maxlength="3" class="ui-widget-content ui-corner-all" disabled />
			</s:if>
		</td>
	</tr>
	<tr>
		<td><span id="payDateError"></span></td>
		<td><span id="numberPaymentsError"></span><span id="frequencyError"></span></td>
	</tr>
	<tr><td colspan="2" class="sectionsubhead"><label for="addRecPaymentMemo"><s:text name="jsp.default_279"/></td></tr>
	<tr><td colspan="2"><s:textfield name="addRecPayment.memo" id="addRecPaymentMemo" size="37" maxlength="255" value="%{addRecPayment.memo}" cssClass="ui-widget-content ui-corner-all"/></label></td></tr>
	<tr><td colspan="2"></td></tr>														

<ffi:setProperty name="checkAmount_acctCollectionName" value="PaymentAccounts"/>
<%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>
</table>
</s:form>
<div class="required" title="required" align="center">* <s:text name="jsp.default_240"/></div>
<div class="btn-row">
	<sj:a id="cancelFormButtonOnInput"
    button="true"
    summaryDivId="summary" buttonType="cancel"
	onClickTopics="showSummary,cancelBillpayForm"
  	><s:text name="jsp.default_82"/></sj:a>
<sj:a
	id="verifyBillpaySubmit"
	formIds="BillpayNew"
	targets="verifyDiv"
	button="true"
	validate="true"
	onAfterValidationTopics="verifyPaymentValidation"
	validateFunction="customValidation"
	onBeforeTopics="beforeVerifyBillpayForm"
	onCompleteTopics="verifyBillpayFormComplete"
	onErrorTopics="errorVerifyBillpayForm"
	onSuccessTopics="successVerifyBillpayForm"
><s:text name="jsp.default_291"/></sj:a>
</div>
</div>
</div>
</div>
		
<ffi:removeProperty name="editPayment"/>
