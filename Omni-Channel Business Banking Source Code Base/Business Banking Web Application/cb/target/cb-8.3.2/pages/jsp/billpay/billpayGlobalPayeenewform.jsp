<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpayaddpayee" className="moduleHelpClass"/>
<span class="shortcutPathClass" style="display:none;">pmtTran_billpay,addGlobalBillpayPayeeLink</span>
<h1 class="portlet-title" id="PageHeading" style="display:none;"><s:text name="jsp.billpay_11" /></h1>
<div class="leftPaneWrapper" >
		<div class="leftPaneInnerWrapper" role="form" aria-labelledby="selectPayee">
			<div class="header"><h2 id="selectPayee"><s:text name="jsp.billpay.select.payee" /></h2></div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
				<select class="txtbox" id="searchPayeeNameID" aria-labeledby="searchPayeeNameID" aria-required="true" name="term" size="1" onChange="sendRequest()"></select>
			</div>
		</div>
</div>
<div class="rightPaneWrapper w71">

<div class="paneWrapper" role="form" aria-labelledby="payeeInfo">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="payeeInfo"><s:text name="jsp.billpay.payee.info" /></h2></div>
   		<div class="paneContentWrapper" id="companyPayeeResultID">
   			<div id="data"></div>
			<s:text name="jsp.billpay.add.payee.text" />
			 
			 <div class="btn-row">
				<sj:a id="cancelBillPayPayee" button="true" onClickTopics="cancelBillpayForm,cancelPayeeLoadForm"><s:text name="jsp.default_82"/></sj:a>
			</div>
  		 </div>
 </div>
</div>  
</div>

<script language="javascript">
	$(function() {
		$("#searchPayeeNameID").lookupbox({
			"controlType" : "server",
			"collectionKey" : "1",
			"module" : "billpaypayee",
			"size" : "35",
			"source" : "/cb/pages/jsp/billpay/BillPayGlobalPayeesLookupBoxAction.action"
		});
	});

	function sendRequest() {
		showAddCompany($('#searchPayeeNameID').val(),
				ns.home.getSessionTokenVarForGlobalMessage);
	}
	function showAddCompany(id, token) {
		//var	urlString = "/cb/pages/jsp/billpay/billpaypayee-company-add3.jsp";
		var urlString = "/cb/pages/jsp/billpay/addBillpayPayeeAction_loadGlobalPayeeDetails.action";

		$.ajax({
			url : urlString,
			type : "POST",
			/* data: 'payeeID=' + id + "&selectedPayeeName=" + name + "&selectedLanguage=" + language + "&CSRF_TOKEN=" + token, */
			data : 'payeeID=' + id + "&CSRF_TOKEN=" + token,
			success : function(data) {
				$('#companyPayeeResultID').html(data);
				$('#companyPayeeResultID').show();
				$('#mainCompanyPayeeID').hide();
				$.publish("common.topics.tabifyDialog");
			}
		});
	}
</script>