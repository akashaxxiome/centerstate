<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_billpaypayeesSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_BillpayPayees" name="ViewURL" url="/cb/pages/jsp/billpay/viewBillpayPayeeAction.action?ID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_BillpayPayees" name="EditURL" url="/cb/pages/jsp/billpay/editBillpayPayeeAction_init.action?ID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_BillpayPayees" name="DeleteURL" url="/cb/pages/jsp/billpay/deleteBillpayPayeeAction_init.action?ID={0}" parm0="ID"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/billpay/getBillpayPayeesAction.action?GridURLs=GRID_BillpayPayees" URLEncrypt="true"/>

    <s:url id="billpayPayeeUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="billpayPayeeGridId"
		caption=""
		sortable="true"
		dataType="json"
		href="%{billpayPayeeUrl}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,billpayPayeeGridCompleteEvents"
		sortname="name"
		sortorder="desc"
	>
		<sjg:gridColumn name="nickName" index="payeeSearchCriteria.nickName" title="%{getText('jsp.default_292')}" sortable="true" width="55" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="name" index="payeeSearchCriteria.name" title="%{getText('jsp.default_314')}" sortable="true" width="55" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="userAccountNumber" index="payeeSearchCriteria.userAccountNumber" title="%{getText('jsp.default_19')}" sortable="true" width="100" cssClass="datagrid_numberColumn"/>

		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" width="100" sortable="false" search="false" formatter="ns.billpay.formatBillpayPayeesActionLinks"  hidedlg="true" hidden="true" cssClass="__gridActionColumn"/>

		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>


	</sjg:grid>

	<script type="text/javascript">
	<!--
		$("#billpayPayeeGridId").data("supportSearch",true);		
		$("#billpayPayeeGridId").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>
