<%@page import="com.ffusion.beans.billpay.PaymentStatus"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<% if( session.getAttribute( "PaymentView" ) != null ) { %>
	<ffi:object name="com.ffusion.tasks.billpay.GetAuditHistory" id="GetAuditHistory" scope="session"/>
		<ffi:setProperty name='GetAuditHistory' property='BeanSessionName' value='IncludeViewPayment'/>
		<ffi:setProperty name="GetAuditHistory" property="AuditLogSessionName" value="TransactionHistory" />
	<ffi:process name="GetAuditHistory"/>
<% } %>

<%
	com.ffusion.beans.billpay.Payment payment = (com.ffusion.beans.billpay.Payment) session.getAttribute("IncludeViewPayment");
	String accountID = payment != null ? payment.getAccount().getID() : "";
	String filter = "ID=" + accountID;
	String _strDisplayTxt = "";
	String _strNickName = "";
	String _strCurCode = "";
	String _fromAccount = "";
	String pmtCurrency = null;
	String acctCurrency = null;
	boolean restricted = false;
%>

<ffi:cinclude value1="<%= accountID %>" value2="" operator="notEquals">
	<ffi:setProperty name="BankingAccounts" property="Filter" value="<%= filter %>"/>
	<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
		<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
		<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>			
		<ffi:list collection="BankingAccounts" items="Account" startIndex="1" endIndex="1">
			<s:set var="AccountDisplayTextTaskBean" value="#attr.Account" scope="request" />
        	<ffi:process name="AccountDisplayTextTask"/>
        	<s:set var="_fromAccount" value="#request.AccountDisplayTextTask.accountDisplayText" scope="request"/>
			<ffi:setProperty name="_strAccountCurrencyCode" value="${Account.CurrencyCode}"/>
		</ffi:list>
	</ffi:cinclude>
	<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="equals">
		<s:set var="_fromAccount" scope="request"><s:text name="jsp.common.acct.restricted"/></s:set>
		<%
			restricted = true;
		%>
	</ffi:cinclude>
</ffi:cinclude>
<%-- for localizing state name --%>
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${IncludeViewPayment.Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${IncludeViewPayment.Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>

<ffi:object id="GetDefaultCurrency" name="com.ffusion.tasks.billpay.GetDefaultCurrency" scope="session"/>
<ffi:process name="GetDefaultCurrency"/>
<ffi:cinclude value1="${IncludeViewPayment.Payee.PayeeRoute.CurrencyCode}" value2="" operator="equals">
<ffi:setProperty name="IncludeViewPayment" property="Payee.PayeeRoute.CurrencyCode" value="${DEFAULT_CURRENCY.CurrencyCode}"/>
</ffi:cinclude>

<ffi:setProperty name="IncludeViewPayment" property="AmountValue.CurrencyCode" value="${IncludeViewPayment.AmtCurrency}"/>
<%-- <table width="720" border="0" cellspacing="0" cellpadding="3">
    
    <tr>
	<td class='<ffi:getProperty name="payment_details_background_color"/>' colspan="6" background='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" ></td>
    </tr>
    <ffi:cinclude value1="${isTemplate}" value2="true">
    	template
	    <tr>
	    	<td class="sectionsubhead" align="right" colspan="2">&nbsp;</td>
	    	<td class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.default_416"/></td>
	    	<td class="columndata"><ffi:getProperty name="IncludeViewPayment" property="templateName"/></td>
	    </tr>
    </ffi:cinclude>
    <tr> --%>
	<div align="center">
			<%-- include page header --%>
			<div align="left">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="columndata ltrow2_color">
							<form action="" method="post" name="FormName">
						                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<input type="hidden" name="Refresh" value="true">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
                                        	<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td class="columndata" >
											     <ffi:getProperty name="IncludeViewPayment" property="Payee.NickName" />
											</td>
											<td class="columndata tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_416"/></span></div>												
											</td>
											<td class="columndata" colspan="2"><ffi:getProperty name="IncludeViewPayment" property="TemplateName" /></td>
										</tr>

										<tr>
											<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td class="columndata" >
											    <ffi:getProperty name="IncludeViewPayment" property="Payee.Name" />
											</td>
											<td class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>												
											</td>
											<td class="columndata" colspan="2">
												 <ffi:getProperty name="IncludeViewPayment" property="AmountValue.CurrencyStringNoSymbol" />
												 <ffi:getProperty name="IncludeViewPayment" property="AmountValue.CurrencyCode"/>
											</td>											
										</tr>
										<tr>
											<td class="columndata" colspan="2">
                                               <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
                                            </td>
											<td class="columndata" >
												<ffi:getProperty name="IncludeViewPayment" property="Payee.UserAccountNumber" />
											</td>
											<s:set var="pmtCurrency" value="#session.IncludeViewPayment.AmtCurrency" scope="request"></s:set>
											<s:set var="acctCurrency" value="#session.IncludeViewPayment.Account.CurrencyCode" scope="request"></s:set>
											<td class="tbrd_l">
											<s:if test="%{#request.acctCurrency != null && #request.acctCurrency != '' 
													&& #request.pmtCurrency != null && #request.pmtCurrency != ''}">
												<ffi:cinclude value1="${IncludeViewPayment.AmtCurrency}" value2="${IncludeViewPayment.Account.CurrencyCode}" operator="notEquals" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
											    </ffi:cinclude>
                                                <ffi:cinclude value1="${IncludeViewPayment.AmtCurrency}" value2="${IncludeViewPayment.Account.CurrencyCode}" operator="equals" >
                                                    &nbsp;
                                                </ffi:cinclude>
											</s:if>
											</td>
											<td class="columndata" colspan="2">
											<s:if test="%{#request.acctCurrency != null && #request.acctCurrency != '' 
													&& #request.pmtCurrency != null && #request.pmtCurrency != ''}">
												<ffi:cinclude value1="${IncludeViewPayment.AmtCurrency}" value2="${IncludeViewPayment.Account.CurrencyCode}" operator="notEquals" >
                                                    <ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${IncludeViewPayment.AmountForBPW}" />
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${IncludeViewPayment.AmountValue.CurrencyCode}" />
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${IncludeViewPayment.Account.CurrencyCode}" />
                                                    <ffi:process name="ConvertToTargetCurrency"/>
                                                    <ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>
                                                    &#8776; <s:property value="#attr.ConvertToTargetCurrency.targetAmountWithCommaSeparator"/>
		 						                    <ffi:getProperty name="IncludeViewPayment" property="Account.CurrencyCode"/>
												</ffi:cinclude>
											</s:if>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/> </span></div>
											</td>
											<td class="columndata" >
												<ffi:getProperty name="IncludeViewPayment" property="Payee.Street" />
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_account"/></span></div>
												
											</td>
											<td class="columndata" colspan="2"><s:property value="#attr._fromAccount"/></td>												
										</tr>
										<tr>
											<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_37"/></span></div>
											</td>
											<td class="columndata" ><ffi:getProperty name="IncludeViewPayment" property="Payee.Street2" /></td>
											<td class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_351"/> </span></div>
												</td>
											<td class="columndata" colspan="2"><ffi:getProperty name="IncludeViewPayment" property="Frequency" /></td>

										</tr>
										<tr>
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_38"/></span></div>
											</td>
											<td class="columndata"  valign="middle">
												<ffi:getProperty name="IncludeViewPayment" property="Payee.Street3" />
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_321"/></span></div>
											</td>
											<td class="columndata" colspan="2">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="IncludeViewPayment" property="NumberPayments" />
												</ffi:cinclude>
											</td>

											</tr>
										<tr>
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_101"/></span>
												</div>
											</td>
											<td class="columndata"  valign="middle">
												<ffi:getProperty name="IncludeViewPayment" property="Payee.City" />
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td class="columndata" colspan="2">
												<ffi:getProperty name="IncludeViewPayment" property="Memo" />
											</td>

											</tr>
										<tr>
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td  valign="middle" class="columndata">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="IncludeViewPayment" property="Payee.State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
											</td>
											<ffi:cinclude value1="${IncludeViewPayment.REGISTER_CATEGORY_ID}" value2="">
												<td class="columndata tbrd_l">&nbsp;</td>
												<td colspan="2"></td>
											</ffi:cinclude>

											<ffi:cinclude value1="${IncludeViewPayment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
											<td class="columndata tbrd_l">
												<div align="right">
														<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span>
													</div>
											</td>
											<td class="columndata" colspan="2">
												<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
												<ffi:list collection="RegisterCategories" items="Category">
													<ffi:cinclude value1="${Category.Id}" value2="${IncludeViewPayment.REGISTER_CATEGORY_ID}">
														<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
														<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
															<ffi:getProperty name="Category" property="Name"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
															<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="Category" property="Name"/>
														</ffi:cinclude>
													</ffi:cinclude>
												</ffi:list>
											</td>
											</ffi:cinclude>
										</tr>
										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td  class="columndata"><ffi:getProperty name="IncludeViewPayment" property="Payee.ZipCode" />
											</td>											
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/> </span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td class="columndata" valign="middle"><ffi:getProperty name='CountryResource' property='Resource'/></td>
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/> </span></div>
											</td>
											<td class="columndata" valign="middle"><ffi:getProperty name="IncludeViewPayment" property="Payee.ContactName" /></td>
											<td class="tbrd_l">												
											</td>
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div></td>
											<td class="columndata" valign="middle"><ffi:getProperty name="IncludeViewPayment" property="Payee.Phone" /></td>
											<td class="tbrd_l">												
											</td>
											<td ></td>											
										</tr>
																				
                                        <ffi:cinclude value1="${IncludeViewPayment.AmountValue.CurrencyCode}" value2="${IncludeViewPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude>
                                    </table>
							</form>
						</td>
						<td></td>
					</tr>
					<% if( session.getAttribute( "PaymentView" ) != null && restricted == false ) { %>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
							<tr>
								<td colspan="6">
									<%-- include batch Transaction History --%>
									<ffi:include page="${PagesPath}common/include-view-transaction-history.jsp" />
								</td>
							</tr>
						</ffi:cinclude>
					<% } %>
					
				</table>
			</div>
		</div>
<ffi:removeProperty name="viewPaymentDoneURL"/>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<ffi:removeProperty name="IncludeViewPayment"/>		