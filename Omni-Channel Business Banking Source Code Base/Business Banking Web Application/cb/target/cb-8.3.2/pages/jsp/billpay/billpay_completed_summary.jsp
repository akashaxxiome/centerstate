<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_billpayCompleteSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_completedBillpay" name="ViewURL" url="/cb/pages/jsp/billpay/billpayviewbill.action?Collection=CompletedPayments&ID={0}&PaymentType={1}&RecPaymentID={2}" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>
	<ffi:setGridURL grid="GRID_completedBillpay" name="InquireURL" url="/cb/pages/jsp/billpay/sendBillPayFundsTransMessageAction_initBillpayInquiry.action?FundsID={0}&PaymentType={1}&recurringId={2}&Subject=Billpay Inquiry" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/billpay/getPaymentGridActions_getCompletedPayments.action?collectionName=CompletedPayments&GridURLs=GRID_completedBillpay" URLEncrypt="true"/>
    <s:url id="completedBillpayUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="completedBillpayGridId"  
		caption=""  
		sortable="true"  
		dataType="local"  
		href="%{completedBillpayUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="payDate"
		sortorder="desc"
		onGridCompleteTopics="addGridControlsEvents,completedBillpayGridCompleteEvents"
	> 
		
		<sjg:gridColumn name="payDate" index="payDate" title="%{getText('jsp.default_137')}" sortable="true" width="55"/>
		<sjg:gridColumn name="payeeName" index="payeeName" title="%{getText('jsp.default_313')}" sortable="true" width="100" hidden="true"/>
		
		
		<%-- <s:if test="%{#session.SecureUser.CorporateUser}">
			<sjg:gridColumn name="account.nickName" id="accountNickName" index="accountNickName" title="%{getText('jsp.billpay_6')}" formatter="ns.billpay.customAccountNicknameColumn" sortable="true" width="100"/>	
		</s:if>
		
		<s:if test="%{#session.SecureUser.ConsumerUser}">
			<sjg:gridColumn name="account.consumerDisplayText" id="consumerDisplayText" index="consumerDisplayText" title="%{getText('jsp.billpay_6')}"  formatter="" sortable="true" width="100"/>
		</s:if> --%>
		
		<sjg:gridColumn name="account.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.billpay.transactionDetails')}" search="false" sortable="false" width="120" hidedlg="" formatter="ns.billpay.formaAccountAndPayee"/>
		<sjg:gridColumn name="map.Frequency" index="mapFrequencyValue" title="%{getText('jsp.default_215')}" formatter="ns.billpay.formatBillpayFrequencyColumn" sortable="true" width="60"/>
		<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="60"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_43')}" formatter="ns.billpay.formatAmountColumn" sortable="true" width="60" align="right"/>
		
		<sjg:gridColumn name="account.currencyCode" index="accountCurrencyCode" title="%{getText('jsp.billpay_39')}" search="false" sortable="false" width="120" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="account.number" index="accountNumber" title="%{getText('jsp.default_19')}" width="100" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="memo" index="memo" title="Memo" sortable="false" width="120" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" sortable="false" search="false" width="80" formatter="ns.billpay.formatCompletedBillpayActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="InquireURL" index="InquireURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	</sjg:grid>
	
	<script type="text/javascript">
	<!--		
		$("#completedBillpayGridId").jqGrid('setColProp','ID',{title:false}); 
	//-->
	</script>	
