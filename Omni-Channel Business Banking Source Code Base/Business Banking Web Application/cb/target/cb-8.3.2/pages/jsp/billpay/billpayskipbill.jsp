<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaydeletebill" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_140')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<div class="approvalDialogHt">
<ffi:setProperty name='PageText' value=''/>
</div>
<s:form action="/pages/jsp/billpay/skipBillpayAction_execute.action" method="post" name="billpaySkip" id="skipBillpayFormId" >
<s:hidden value="%{ID}" name="ID"></s:hidden>
<s:hidden value="%{recPaymentID}" name="recPaymentID"></s:hidden>
<s:hidden value="%{paymentType}" name="paymentType"></s:hidden>
<input id="isSkipSingleInstance" name="isSkipInstance" type="hidden"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="templateConfirmationWrapper">
		<div class="confirmationDetails">
			<span id="" class="sectionLabel"><s:text name="jsp.billpay_referencenum"/></span>
	        <span id="" class="columndata"><ffi:getProperty name="payment" property="TrackingID"/></span>
		</div>
</div>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="Payment" property="PayeeName"/> (<ffi:getProperty name="Payment" property="PayeeNickName"/> - <ffi:getProperty name="payment.payee" property="UserAccountNumber" />)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_314"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="PayeeName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_75"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="PayeeNickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_last_payment_to_payee"/>:</span>
				<span class="columndata">
					<!-- below call will be migrated after payees flow is done -->
					<ffi:setProperty name="LastPayments" property="Filter" value="PAYEEID=${Payee.ID}"/>
					<ffi:cinclude value1="${LastPayments.Size}" value2="0" operator="notEquals">
						<ffi:list collection="LastPayments" items="lastPayment" startIndex="1" endIndex="1">
							<ffi:setProperty name="lastPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
							<ffi:getProperty name="lastPayment" property="Amount"/>&nbsp;-&nbsp;<ffi:getProperty name="lastPayment" property="PayDate"/>
						</ffi:list>
					</ffi:cinclude>
					<ffi:cinclude value1="${LastPayments.Size}" value2="0" operator="equals"><s:text name="jsp.billpay_none"/></ffi:cinclude>
			</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_15"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.UserAccountNumber"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_36"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.Street"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_37"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_38"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.Street3"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_101"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_386"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.StateName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_473"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.CountryName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.billpay_83"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.Phone"/></span>
			</div>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_43"/>:</span>
				<span class="columndata"  >
											<ffi:getProperty name="Payment" property="AmountValue.CurrencyStringNoSymbol" />&nbsp;&nbsp;
											<ffi:getProperty name="Payment" property="AmountValue.CurrencyCode" />
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" >
					 <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else> <s:text name="jsp.billpay_40"/>:</s:else>
				</span>
				<span class="columndata" >
				 	<s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing -->
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
	        	    </s:else>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
				<span class="columndata"  >
					 <s:property value="%{payment.account.accountDisplayText}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_76"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="PayDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_deliver_by"/>:</span>
				<span class="columndata"  >
					<ffi:setProperty name="Payment" property="DateFormat" value="${UserLocale.DateFormat}" />
					<ffi:getProperty name="Payment" property="DeliverByDate"/>												
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_351"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Frequency"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_321"/>:</span>
				<span class="columndata"  >
					<s:if test="%{Payment.NumberPayments=='999'}">
						<s:text name="jsp.default_446"/>
					</s:if>
					<s:else>
						<ffi:getProperty name="Payment" property="NumberPayments"/>
					</s:else>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_279"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Memo"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_submittedby"/>:</span>
			<span class="columndata"  ><s:property value="%{payment.submittedByUserName}"/></span>
		</div>
	</div>
</div>

<ffi:cinclude value1="${Payment.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_BILL_PAYMENT) %>">

</ffi:cinclude>
<s:if test="%{Payment.AmountValue.CurrencyCode!=Payment.Account.CurrencyCode}">
	<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="ffivisible" style="height:150px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
<sj:a id="cancelSkipSingleBillpayLink" button="true" onClickTopics="closeDialog">
<s:text name="jsp.default_82"/>
</sj:a>
<sj:a id="skipSingleInstance" formIds="skipBillpayFormId" targets="resultmessage" button="true"   
	title="%{getText('jsp.billpay_140')}" onCompleteTopics="skipSingleBillpayComplete"  onErrorTopics="errorSkipBillpay" 
	effectDuration="1500" onclick="skipInstance()"><s:text name="jsp.default_Skip"/>
</sj:a>
</div>
</div>
</s:form>
<%-- <s:bean name="com.ffusion.beans.billpay.Payment" var="IncludeViewPayment"></s:bean> --%>
<%-- <ffi:removeProperty name="GetStateProvinceDefnForState"/> --%>
<%--<ffi:removeProperty name="Payment"/>--%>
<script type="text/javascript">
$(document).ready(function(){
	$( ".toggleClick" ).click(function(){ 
		if($(this).next().css('display') == 'none'){
			$(this).next().slideDown();
			$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
			$(this).removeClass("expandArrow").addClass("collapseArrow");
		}else{
			$(this).next().slideUp();
			$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
			$(this).addClass("expandArrow").removeClass("collapseArrow");
		}
	});

});

function skipInstance(){
	$('#isSkipSingleInstance').val('true');
}
</script>
