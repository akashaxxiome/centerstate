<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%-- 
parameters need to be prepared:

fileType: 
actionName: 	
ProcessImportTask

uploadingID: default to ns.common.uploadingID defined in common.js 

--%>
<script>
/**
 * Uploading succeeded
 */
$.subscribe('successBillUploadingFile', function(event,data) {
	 $('#detailsPortlet').portlet('expand');
	 $('#detailsPortlet').show();
	 $.publish('closeDialog');
	 if(data.textContent.indexOf("EBANKING_FILE_UPLOADING_ERROR")=='-1'){
		 $('#fileupload').hide();
		 $('#BillPayFileImportEntryLink').hide();
		 
		 //hide File Import buttion in multiple payment once file successfully uploaded
		 $('#fileImportBtnDivId').hide();
	 }
	 if(ns.common.moduleType =="billpayTemplate"){
		 ns.common.refreshDashboard('dbTemplateSummary');
		 ns.common.selectDashboardItem("addMultipleTemplateLink");
	 }else{
		 ns.common.refreshDashboard('dbBillPaySummary');
		 ns.common.selectDashboardItem("addMultipleBillpayLink");
	 }
});


$.subscribe('wrongFileUploadError', function(event,data) {
	$("#openFileImportDialogID").dialog('close');
});

//This function is used to check event of enter key.
$("#uploaderID").keypress(function(e) {
	if (e.which == 13) {  
		//$("#billpayUploadSubmit").click();
		return false;
	}   
});	

</script>
<div align="center">
<fieldset id="fileUploadSelectFileFieldSetID">
<legend><s:text name="jsp.default_211"/></legend>
			<span class="progressbar" style="display:none;" id="uploadprogressbar"></span>
			<div id="showImportFileNameID"></div>
			<div id="importedFileFormDivID">
			<table width="282" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata_grey" colspan="2">
						<div align="left">
							<span class="columndata"><s:text name="jsp.default_187"/></span></div>
					</td>
				</tr>
				<tr>
					<td class="columndata_grey" colspan="2">
						<s:form id="uploaderID" name="FileForm" enctype="multipart/form-data" action="%{#request.actionName}" method="post" theme="simple">
			                <div align="center">
			                <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" id="CSRF_TOKEN"/>
							<input type="text" id="fakeImportFileID" readonly="true"/>
							<span class="fileUploadSpan">
								<sj:a
									id="browseFileButton"
									cssClass="fakeUploadLink"
									button="true" 
									onClickTopics=""
								><s:text name="jsp.default.label.browse"/></sj:a>
								<s:file id="importFileInputID" name="file" accept="text/plain" onchange="ns.common.updateFilePath();"/>
							</span><span id="FileError"></span>
							<br/><span id="errorFileType"></span>
							<input type="hidden" name="overWrite" value="true">
							<input type="hidden" name="uploadingID" value="TellurideUpload"/>
							<input type="hidden" name="ProcessImportTask" value="<ffi:getProperty name="ProcessImportTask"/>">
							<input type="hidden" name="fileType" value='<ffi:getProperty name="fileType"/>'/>
							<input type="hidden" name="TransactionsOnly" value='<ffi:getProperty name="TransactionsOnly"/>'/>
							<input type="hidden" name="isFromFileUpload" value='true'/>
							</div>
						</s:form>
						<ffi:removeProperty name="ProcessImportTask"/>
						<ffi:removeProperty name="actionName"/>
						<ffi:removeProperty name="fileType"/>
					</td>
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2" align="center">
						<sj:a 
                                button="true"
                                onClickTopics="closeUploadFileWindowTopic,tabifyNotes" 
		                        ><s:text name="jsp.default_82"/></sj:a>
						<sj:a 
								id="billpayUploadSubmit"
								formIds="uploaderID"
								targets="inputDiv"
								timeout="0"
                                button="true" 
								onclick="ns.common.updateRequestParams('uploaderID')"
                                onBeforeTopics="beforeUploadingFile"
								onErrorTopics="errorUploadingFile,wrongFileUploadError"
	                            onSuccessTopics="successBillUploadingFile"
								onCompleteTopics="completeUploadingFile,tabifyNotes"								
		                        ><s:text name="jsp.default_452"/></sj:a>
					</td>
				</tr>
			</table>
			<p></p>
			</div>
	<div id="uploadingStatus2" ></div>
	<iframe style="display: none;" name="progressFrame"></iframe>
	</fieldset>
</div>
