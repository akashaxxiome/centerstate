<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaypayee-company-add" className="moduleHelpClass"/>	
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_11')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%-- <%
	session.setAttribute("selectedPayeeID", request.getParameter("selectedPayeeID"));
	session.setAttribute("selectedLanguage",request.getParameter("selectedLanguage"));
	session.setAttribute("selectedPayeeName",request.getParameter("selectedPayeeName"));
%>

<ffi:setProperty name="AddGlobalPayees" property="PayeeLanguage" value="${selectedLanguage}"/>
<ffi:setProperty name="AddGlobalPayees" property="PayeeID" value="${selectedPayeeID}" />

<ffi:object name="com.ffusion.tasks.paymentsadmin.GetGlobalPayee" id="GetGlobalPayee" scope="session"/>
	<ffi:setProperty name="GetGlobalPayee" property="ID" value="${AddGlobalPayees.PayeeID}" />
	<ffi:setProperty name="GetGlobalPayee" property="CurrentLanguage" value="${selectedLanguage}" />
	<ffi:process name="GetGlobalPayee"/>
	<ffi:removeProperty name="GetGlobalPayee"/>
	
<ffi:object id="AddGlobalPayee" name="com.ffusion.tasks.billpay.AddPayee" scope="session"/>
<ffi:setProperty name="AddGlobalPayee" property="Initialize" value="TRUE" />
<ffi:process name="AddGlobalPayee"/>

<%
	session.setAttribute("FFIAddGlobalPayee", session.getAttribute("AddGlobalPayee"));
%> --%>

<div id="companyePayeeSaveReslutId"></div>
<s:form namespace="/pages/jsp/billpay" action="addBillpayPayeeAction_addGlobalPayee" method="post" id="companyPayeeFormId" name="companyPayeeFormId" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="payeeID" value="%{payeeID}"></s:hidden>
<s:hidden name="payeeType" value="global"></s:hidden>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding10">	
	<tr>
		<td id="verifyGlobalPayeeNickNameLabel" width="50%" class="sectionsubhead"><s:text name="jsp.default_294"/><span class="required">*</span></td>
		<td id="verifyGlobalPayeeAccountNoLabel" width="50%" class="sectionsubhead">
			<s:text name="jsp.default_19"/><s:if test='%{addPayee.payeeRoute.custAcctRequired}'><span class="required">*</span></s:if><s:else></s:else>
		</td>
	</tr>
	<tr>
		<td class="columndata">
			<input id="nickName" class="txtbox ui-widget-content ui-corner-all" type="text" name="nickName" size="30" maxlength="30"  value="<s:property value="%{NickName}"/>">
		</td>
		<td class="columndata">
			<input id="userAccountNumberId" class="ui-widget-content ui-corner-all" type="text"  name="userAccountNumber" size="30" maxlength="30"  value="<s:property value="%{UserAccountNumber}"/>">
		</td>	
	</tr>
	<tr>
		<td><span id="nickNameError"></span></td>
		<td><span id="Payee.userAccountNumberError"></span></td>
	</tr>
</table>	
<div class="blockContent marginTop10">
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="verifyGlobalPayeeNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_314"/>:</span>
			<span id="verifyGlobalPayeeNameValue" class="columndata" ><s:property value="%{name}"/><span id="nameGlobalError"></span></span>
		</div>
		<div class="inlineBlock">
			<span id="verifyGlobalPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
			<span id="verifyGlobalPayeeContactValue" class="columndata" ><s:property value="%{ContactName}"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="verifyGlobalPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_37"/>:</span>
			<span id="verifyGlobalPayeePhoneValue" class="columndata" >
				<s:property value="%{Phone}"/>
			</span>
		</div>
		<div class="inlineBlock">
			<span id="verifyGlobalPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_35"/> 1:</span>
			<span id="verifyGlobalPayeeAddress1Value" class="columndata" ><s:property value="%{Street}"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="verifyGlobalPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_35"/> 2:</span>
			<span id="verifyGlobalPayeeAddress2Value" class="columndata" ><s:property value="%{Street2}"/></span>
		</div>
		<div class="inlineBlock">
			<span id="verifyGlobalPayeeAddress3Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_35"/> 3:</span>
			<span id="verifyGlobalPayeeAddress3Value" class="columndata" ><s:property value="%{Street3}"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="verifyGlobalPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
			<span id="verifyGlobalPayeeCityValue" class="columndata" >
				<s:property value="%{City}"/>
			</span>
		</div>
		<div class="inlineBlock">
			<span id="verifyGlobalPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
			<span id="verifyGlobalPayeeStateValue" class="columndata" >
				<s:property value="%{stateDisplayName}"/>
			</span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="verifyGlobalPayeeZipLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
			<span id="verifyGlobalPayeeZipValue" class="columndata" >
				<s:property value="%{zipCode}"/>
			</span>
		</div>
		<div class="inlineBlock">
			<span id="verifyGlobalPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
			<span id="verifyGlobalPayeeCountryValue" class="columndata" >
				<s:property value="%{countryDisplayName}"/>
			</span>
		</div>
	</div>
</div>
							
		<%-- <div align="center">
		<br>
			<div align="center">
				<table width="750" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
						<s:form namespace="/pages/jsp/billpay" action="addBillpayPayeeAction_addGlobalPayee" method="post" id="companyPayeeFormId" name="companyPayeeFormId" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<s:hidden name="payeeID" value="%{payeeID}"></s:hidden>
						<s:hidden name="payeeType" value="global"></s:hidden>
						<ffi:setProperty name="AddGlobalPayees" property="Validate" value="" />
							<ffi:cinclude value1="${Payee.PayeeRoute.CustAcctRequired}" value2="true">
						<ffi:setProperty name="AddGlobalPayees" property="Validate" value="USERACCOUNTNUMBER" />
						</ffi:cinclude>
						<!-- <input type="Hidden" name="AddGlobalPayees.Process" value="true">
						<input type="Hidden" name="AddGlobalPayees.Filter" value="All"> -->
						
						<ffi:cinclude value1="${Payee.PayeeRoute.CustAcctRequired}" value2="true" operator="equals">
							<ffi:setProperty name="AddGlobalPayee" property="NoAccountNumber" value="false"/>
							<ffi:setProperty name="AddGlobalPayee" property="Validate" value="NAME,STREET,CITY,USERACCOUNTNUMBER,ZIPCODE"/>
						</ffi:cinclude>

						<ffi:cinclude value1="${Payee.PayeeRoute.CustAcctRequired}" value2="true" operator="notEquals">
							<ffi:setProperty name="AddGlobalPayee" property="Validate" value="NAME,USERACCOUNTNUMBER"/>		
						</ffi:cinclude>

						<ffi:setProperty name="AddGlobalPayee" property="MaxUserAccountNumberLength" value="50" />
						<ffi:setProperty name="AddGlobalPayee" property="MaxNameLength" value="50" />
						<!-- Set the additional parameters-->
						<ffi:setProperty name="AddGlobalPayee" property="PayeeRoute.RouteID" value="${Payee.PayeeRoute.RouteID}" />
						<ffi:setProperty name="AddGlobalPayee" property="PayeeRoute.AccountID" value="${Payee.PayeeRoute.AccountID}" />
						<ffi:setProperty name="AddGlobalPayee" property="PayeeRoute.AcctType" value="${Payee.PayeeRoute.AcctType}" />
						<ffi:setProperty name="AddGlobalPayee" property="PayeeRoute.CurrencyCode" value="${Payee.PayeeRoute.CurrencyCode}" />
						<s:set var="" value="#session.AddGlobalPayee.payeeRoute.custAcctRequired = #session.Payee.payeeRoute.custAcctRequired" />
						<ffi:setProperty name="AddGlobalPayee" property="PayeeRoute.BankIdentifier" value="${Payee.PayeeRoute.BankIdentifier}" />
						<s:set var="" value="#session.AddGlobalPayee.map['IDScope'] = #session.Payee.map['IDScope']"></s:set>
								<div align="center">
									<table width="720" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_15"/><br>
												</span></td>
										</tr>
											<tr>
												<td id="verifyGlobalPayeeMessage" class="columndata ltrow2_color" colspan="6">
													<div align="center">
														<span class="sectionsubhead"><s:text name="jsp.billpay_113"/><br>
															<br>
														</span></div>
												</td>
											</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										
										<ffi:list collection="AddGlobalPayees" items="Payee1" >
										
										        <ffi:cinclude value1="${selectedPayeeID}" value2="${Payee1.ID}" operator="equals" >
										         <ffi:cinclude value1="${selectedLanguage}" value2="${Payee1.searchLanguage}" operator="equals" >
        										<ffi:setProperty name="selectedPayeeName" value="${Payee1.Name}" />  
        										 
										<tr>
											<td id="verifyGlobalPayeeNameLabel" class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="verifyGlobalPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Name"/>
											<s:property value="%{name}"/>
											<ffi:setProperty name="AddGlobalPayee" property="Name" value="${Payee1.Name}"/>
											<ffi:setProperty name="AddGlobalPayee" property="HostID" value="${Payee1.ID}"/>	
											<ffi:setProperty name="AddGlobalPayee" property="ID" value="${Payee1.ID}"/>
											<span id="nameGlobalError"></span>
											</td>
											
											
											<td id="verifyGlobalPayeeAddress1Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_35"/> 1 </span></div>
											</td>
											<td id="verifyGlobalPayeeAddress1Value" class="columndata" colspan="2">
											<ffi:getProperty name="Payee1" property="Street" />
											<ffi:setProperty name="AddGlobalPayee" property="Street" value="${Payee1.Street}"/>
											<s:property value="%{Street}"/>
											</td>
											
										</tr>
										<tr>
											<td id="verifyGlobalPayeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span>
													<span class="required">*</span>
												</div>
											</td>
											<td class="columndata" colspan="2">
											<input id="nickName" class="txtbox ui-widget-content ui-corner-all" type="text" name="nickName" size="30" maxlength="30" 
											value="<s:property value="%{NickName}"/>">
											</td>
											<td id="verifyGlobalPayeeAddress2Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_35"/> 2 </span></div>
											</td>
											<td id="verifyGlobalPayeeAddress2Value" class="columndata" colspan="2">
											<ffi:getProperty name="Payee1" property="Street2" />
											<ffi:setProperty name="AddGlobalPayee" property="Street2" value="${Payee1.Street2}"/>
											<s:property value="%{Street2}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyGlobalPayeeContactLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="verifyGlobalPayeeContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee1" property="ContactName" />
											<ffi:setProperty name="AddGlobalPayee" property="ContactName" value="${Payee1.ContactName}"/>
											<s:property value="%{ContactName}"/>
											</td>
											<td id="verifyGlobalPayeeAddress3Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_35"/> 3 </span></div>
											</td>
											<td id="verifyGlobalPayeeAddress3Value" class="columndata" colspan="2">
											<ffi:getProperty name="Payee1" property="Street3" />
											<ffi:setProperty name="AddGlobalPayee" property="Street3" value="${Payee1.Street3}"/>
											<s:property value="%{Street3}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyGlobalPayeePhoneLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td id="verifyGlobalPayeePhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee1" property="Phone" />
											<ffi:setProperty name="AddGlobalPayee" property="Phone" value="${Payee1.Phone}"/>
											<s:property value="%{Phone}"/>
											</td>
											<td id="verifyGlobalPayeeCityLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/> </span></div>
											</td>
											<td id="verifyGlobalPayeeCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee1" property="City" />
											<ffi:setProperty name="AddGlobalPayee" property="City" value="${Payee1.City}"/>
											<s:property value="%{City}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyGlobalPayeeAccountNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span>
													<ffi:cinclude value1="${Payee.PayeeRoute.CustAcctRequired}" value2="true" operator="equals">
														<span class="required">*</span>
														<input type="hidden" id="userAccountNumberFlagId" value="true">
													</ffi:cinclude>
											<s:if test='%{addPayee.payeeRoute.custAcctRequired}'>
												<span class="required">*</span>
											</s:if>
											<s:else>
												<!-- no need of required star -->
											</s:else>
										</div>
											</td>
											<td class="columndata" colspan="2">
											<input id="userAccountNumberId" class="ui-widget-content ui-corner-all" type="text" 
											name="userAccountNumber" size="30" maxlength="30" 
											value="<s:property value="%{UserAccountNumber}"/>">
											<span id="userAccountNumberGlobalError"></span>
											
																					
											</td>
											
											<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
											<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payee1.Country}" />
											<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payee1.State}" />
											<ffi:process name="GetStateProvinceDefnForState"/>
											
											<td id="verifyGlobalPayeeStateLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="verifyGlobalPayeeStateValue" class="columndata" colspan="2">
											<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
											<ffi:getProperty name="Payee1" property="State"/>
											<ffi:setProperty name="AddGlobalPayee" property="State" value="${Payee1.State}"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
											<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
											<ffi:setProperty name="AddGlobalPayee" property="State" value="${Payee1.State}"/>
											</ffi:cinclude>
											<s:property value="%{stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color"></td>
											<td colspan="2"></td>
											<td id="verifyGlobalPayeeZipLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td id="verifyGlobalPayeeZipValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<ffi:setProperty name="AddGlobalPayee" property="ZipCode" value="${Payee1.ZipCode}"/>
											<s:property value="%{zipCode}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td id="verifyGlobalPayeeCountryLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee1.Country}" />
											<ffi:setProperty name="AddGlobalPayee" property="Country" value="${Payee1.Country}"/>
											<td id="verifyGlobalPayeeCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">			
												</div>
											</td>
											<td colspan="4"><div align="center">
													<br>
													
				<sj:a 
               		button="true" 
					onClickTopics="closeDialog"
       			><s:text name="jsp.default_82"/></sj:a>
       			<sj:a 
               		button="true" 
					onClickTopics="reviseBackSelectCompanyTopic"
       			><s:text name="jsp.billpay_90"/></sj:a>
				<sj:a 
						id="companyPayeeSubmit"
						formIds="companyPayeeFormId"
                        targets="companyePayeeSaveReslutId" 
						button="true" 
                        validate="true"
                        validateFunction="customValidation"
						onBeforeTopics="beforeVerifyBillpayForm" 
                        onSuccessTopics="completeCompanyPayeeSaveTopic,closeDialog"
                        ><s:text name="jsp.billpay_17"/></sj:a></div></td>
				<td></td>
				</tr>
				</ffi:cinclude>
				</ffi:cinclude>
				</ffi:list>
				</table>
				</div>
				
				</td>
				<td></td>
					</tr>
				</table>
				<br>
			</div>
		</div> --%>
<div class="btn-row marginTop10">
<sj:a 
               		button="true" 
					onClickTopics="cancelBillpayForm,cancelPayeeLoadForm"
       			><s:text name="jsp.default_82"/></sj:a>
       			<%-- <sj:a 
               		button="true" 
					onClickTopics="reviseBackSelectCompanyTopic"
       			><s:text name="jsp.billpay_90"/></sj:a> --%>
				<sj:a 
						id="companyPayeeSubmit"
						formIds="companyPayeeFormId"
                        targets="companyePayeeSaveReslutId" 
						button="true" 
                        validate="true"
                        validateFunction="customValidation"
						onBeforeTopics="beforeVerifyBillpayForm" 
                        onSuccessTopics="completeCompanyPayeeSaveTopic,closeDialog"
                        ><s:text name="jsp.default_366" /></sj:a>
</div>
</s:form>
<%-- <ffi:removeProperty name="GetStatesForCountry"/>
<ffi:removeProperty name="GetStateProvinceDefnForState"/> --%>