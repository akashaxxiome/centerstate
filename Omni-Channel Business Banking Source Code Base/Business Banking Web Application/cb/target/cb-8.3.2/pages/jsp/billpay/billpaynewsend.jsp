<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.beans.billpay.PaymentStatus" %>
<%@ page import="java.lang.Integer" %>
<%@ page import="com.ffusion.beans.SecureUser" %>

<ffi:help id="payments_billpaynewsend" className="moduleHelpClass"/>
<div style="display:none">
	<s:if test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED ||
			payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL ||
			payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_CREATED ||
			payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_CLEARED ||
			payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PAID ||
			payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_POSTED 
			}">
	<ffi:setProperty name="payment" property="Locale" value="${SecureUser.UserLocale.Locale}"/>
	<span id="billpayResultMessage"><s:text name="jsp.billpay_119"/></span>
	</s:if>
	<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_UNKNOWN ||
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_TO_PAY ||
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_NOT_EXCEPTED || 
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_DATE_TOO_SOON ||
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_FAILED ||
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_NOT_ALLOWED ||
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_APPROVAL ||
				payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_FAILEDON
				}">
	<ffi:setProperty name="payment" property="Locale" value="${SecureUser.UserLocale.Locale}"/>				
	<span id="billpayResultMessage"><s:text name="jsp.billpay_136"/></span>
	</s:elseif>
	<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_INSUFFICIENT_FUNDS}">
	<span id="billpayResultMessage"><s:text name="jsp.billpay_137"/></span>
	</s:elseif>
	<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_FAILEDON}">
	<span id="billpayResultMessage"><s:text name="jsp.billpay_138"/></span>
	</s:elseif>
	<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITCHECK_FAILED}">
	<span id="billpayResultMessage"><s:text name="jsp.billpay_139"/></span>
	</s:elseif>
</div>
<%-- include page header --%>

<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 10px;" id="leftPaneForDvc">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="payment.payee" property="Name" /></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue"><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
	</div>
	<div id="templateForSingleBillpayVerifyResult"></div>
</div>

<div id="templateForSingleBillpayVerify" class="leftPaneInnerWrapper">
	<div class="header"><s:text name='jsp.default_371' /></div>
	<s:set name="strutsActionName" value="'addBillpayAction'" scope="request" />
	<div id="billpayTemplateDiv" class="leftPaneInnerBox">
		<s:include value="billpaytempname.jsp" />
	</div>
	<div id="templateForSingleBillpayVerifyResult"></div>
</div>
</div>	
<form action="" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="Refresh" value="true">
<div class="confirmPageDetails">
<div class="templateConfirmationWrapper">
		<div class="confirmationDetails">
			<span id="confirmBillPayPayeeReferenceLabel" class="sectionLabel"><s:text name="jsp.default_347"/></span>
	        <span id="confirmBillPayPayeeReferenceValue" class="columndata"><s:property value="%{payment.TrackingID}"/></span>
		</div>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
<div class="blockContent">
	
          			<s:set var="PMS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_TO_PAY==payment.status}" />
					<s:set var="PMS_INSUFFICIENT_FUNDS" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_INSUFFICIENT_FUNDS==payment.status}" />
					<s:set var="PMS_BPW_LIMITCHECK_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITCHECK_FAILED==payment.status}" />
					<s:set var="PMS_SCHEDULED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED==payment.status}" />
					<s:set var="PMS_PENDING_APPROVAL" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL==payment.status}" />
					<s:set var="PMS_UNKNOWN" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_UNKNOWN==payment.status}" />
					<s:set var="PMS_FUNDS_ALLOCATED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED==payment.status}" />
					<s:set var="PMS_PAID" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_PAID==payment.status}" />
					<s:set var="PMS_FUNDS_FAILEDON" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_FAILEDON==payment.status}" />
					<s:set var="PMS_BPW_APPROVAL_NOT_ALLOWED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_NOT_ALLOWED==#payment.status}" />
					<s:set var="PMS_BPW_APPROVAL_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_FAILED==#payment.status}" />
					<s:set var="PMS_BPW_LIMITREVERT_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITREVERT_FAILED==#payment.status}" />
					
					<s:if test="%{#PMS_BPW_LIMITCHECK_FAILED || #PMS_BPW_APPROVAL_NOT_ALLOWED || #PMS_INSUFFICIENT_FUNDS || #PMS_FAILED_TO_TRANSFER || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_LIMITREVERT_FAILED || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_FUNDSREVERTED_NOTIF || #PMS_BPW_FUNDSREVERTED || #PMS_FUNDS_FAILEDON}">
						<s:set var="HighlightStatus" value="true" />
					</s:if>
					<s:if test="%{#HighlightStatus==true}">
						<div class="blockRow failed">
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-decline"></span> <!-- failed mean give red here -->
									<s:if test="%{payment.backendErrorDesc=='' || payment.backendErrorDesc==null }">
								<s:property	value="%{payment.statusName}" />
								</s:if> 
								<s:else>
								<s:property	value="%{payment.statusName}" /> - <s:property value="%{payment.backendErrorDesc}"/>
								</s:else>
							</span></span>
							<span style="float:right">
								<s:if test="%{#PMS_INSUFFICIENT_FUNDS}">
									<s:text name="jsp.billpay_137" />
								</s:if>
								<s:elseif test="%{#PMS_BPW_LIMITCHECK_FAILED}">
									<s:text name="jsp.billpay_139" />
								</s:elseif>
								<s:elseif test="%{#PMS_FUNDS_FAILEDON}">
									<s:text name="jsp.billpay_138" />
								</s:elseif>
								<s:elseif test="%{#PMS_FAILED_TO_TRANSFER || #PMS_BPW_APPROVAL_NOT_ALLOWED || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_LIMITREVERT_FAILED || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_FUNDSREVERTED_NOTIF || #PMS_BPW_FUNDSREVERTED}">
									<s:text name="jsp.billpay_136" />
								</s:elseif>
							</span>
						</div>
					</s:if> 
					<s:elseif test="%{#PMS_SCHEDULED || #PMS_FUNDS_ALLOCATED}">
					<div class="blockRow pending">
						<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
			            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-future"></span><!-- this is scheduled/pending giv orange -->
								<s:property	value="%{payment.statusName}" />
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_TRANSFERED}">
						<div class="blockRow completed">
						 	<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
				            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> 
								<s:property value="%{payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_PENDING_APPROVAL}">
						<div class="blockRow pending">
							<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
				            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-pending"></span> <!-- this is pending approval give color orange icon different -->
								<s:property value="%{payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_PAID}">
						<div class="blockRow completed">
						 	<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> 
								<s:property value="%{payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
</div>
<div  class="blockHead toggleClick"><s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="payment.payee" property="Name" /> (<s:property value="payment.Payee.NickName"/> - <ffi:getProperty name="payment.payee" property="UserAccountNumber" />)
 <span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeNicknameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_75"/>:</span>
				<span id="confirmBillPayPayeeNicknameValue" class="columndata" ><s:property value="payment.Payee.NickName"/></span>
			</div>
			<div class="inlineBlock">
				
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeAccountLabel" class="sectionsubhead sectionLabel "><s:text name="jsp.default_15"/>:</span>
				<span id="confirmBillPayPayeeAccountValue" class="columndata" ><ffi:getProperty name="payment.payee" property="UserAccountNumber" /></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPayeeFirstAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_20"/>:</span>
				<span id="confirmBillPayPayeeFirstAddressValue" class="columndata" ><ffi:getProperty name="payment.payee" property="Street" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeSecondAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_21"/>:</span>
				<span id="confirmBillPayPayeeSecondAddressValue" class="columndata" ><ffi:getProperty name="payment.payee" property="Street2" /></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPayeeThirdAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="confirmBillPayPayeeThirdAddressValue" class="columndata" ><ffi:getProperty name="payment.payee" property="Street3" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="confirmBillPayPayeeCityValue" class="columndata" ><ffi:getProperty name="payment.payee" property="City" /></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="confirmBillPayPayeeStateValue" class="columndata" >
					<s:property value="%{payment.payee.stateDisplayName}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeZipCodeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span id="confirmBillPayPayeeZipCodeValue" class="columndata" ><ffi:getProperty name="payment.payee" property="ZipCode" /></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span id="confirmBillPayPayeeCountryValue" class="columndata" ><s:property value="%{payment.payee.countryDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span id="confirmBillPayPayeeContactValue" class="columndata" ><ffi:getProperty name="payment.payee" property="ContactName" /></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/>:</span>
				<span id="confirmBillPayPayeePhoneValue" class="columndata" ><ffi:getProperty name="payment.payee" property="Phone" /></span>
			</div>
		</div>
</div>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 49%">
				<span id="confirmBillPayAmountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span id="confirmBillPayAmountValue" class="columndata" >
					 <s:property value="payment.AmountValue.CurrencyStringNoSymbol"/>
					 <s:property value="%{payment.AmtCurrency}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPayeeCurrencyLabel" class="sectionsubhead sectionLabel ">
				 <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else> 
						<span id="confirmBillPayAmountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_40"/>:</span>
					</s:else>
                 </span>
				 <span id="confirmBillPayPayeeCurrencyValue" class="columndata" >
					 <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
	        	    </s:else>
				 </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock">
				<span id="confirmBillPaymentDateLabel" class="sectionsubhead sectionLabel "><s:text name="jsp.billpay_76"/>:</span>
				<span id="confirmBillPaymentDateValue" class="columndata" ><s:property value="%{payment.payDate}"/></span>
			</div>
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_deliver_by"/>:</span>
				<span class="columndata"  >
					<s:property value="%{payment.deliverByDate}"/>												
				</span>	
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock">
				<span id="confirmBillPayPayeeFrequencyLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/></span>
				<span id="confirmBillPayPayeeFrequencyValue" class="columndata"  >
					<s:property value="%{payment.frequency}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmBillPayPaymentsLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/></span>
				<span id="confirmBillPayPaymentsValue" class="columndata"  >
					<s:if test="%{payment.openEnded}">
						<s:text name="jsp.default_446"/>
					</s:if>
					<s:else>
						<s:property value="%{payment.NumberPayments}"/>
					</s:else>
				</span>	
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmBillPayPayeeMemoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span id="confirmBillPayPayeeMemoValue" class="columndata" ><ffi:getProperty name="payment" property="Memo" /></span>
			</div>
			<div class="inlineBlock">
				
			</div>
		</div>
</div>
			<!-- 	<table width="750" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
								<div align="left">
									<table width="720" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr> -->
										<%-- <tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_125"/><br>
												</span></td>
										</tr> --%>
										<!-- <tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr> -->
										<%-- <tr>
											<td id="confirmBillPayPayeeNicknameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="confirmBillPayPayeeNicknameValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="NickName" />
											<s:property value="payment.Payee.NickName"/>
											</td>
											<td id="confirmBillPayAmountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td id="confirmBillPayAmountValue" class="columndata" colspan="2">
											     <ffi:getProperty name="AddPayment" property="AmountValue.CurrencyStringNoSymbol" />
												 <ffi:getProperty name="AddPayment" property="AmountValue.CurrencyCode"/>
												 <s:property value="payment.AmountValue.CurrencyStringNoSymbol"/>
												 <s:property value="payment.payee.PayeeRoute.CurrencyCode"/>
												 <s:property value="%{payment.AmtCurrency}"/>
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeNameLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="confirmBillPayPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="payment.payee" property="Name" />
											<ffi:getProperty name="Payee" property="Name" />
											</td>
											<td id="confirmBillPayPayeeCurrencyLabel" class="columndata tbrd_l">
											 <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else> 
								        	    <div align="right">
													<span class="sectionsubhead">
													<s:text name="jsp.billpay_40"/> 
													</span>
												</div>
												</s:else>
											    <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
											    </ffi:cinclude>
                                                <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="equals" >
                                                    &nbsp;
                                                </ffi:cinclude>
                                            </td>
											<td id="confirmBillPayPayeeCurrencyValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
                                                    <ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${AddPayment.AmountForBPW}" />
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${AddPayment.AmountValue.CurrencyCode}" />
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${AddPayment.Account.CurrencyCode}" />
                                                    <ffi:process name="ConvertToTargetCurrency"/>
                                                    <ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>

                                                    &#8776; <s:property value="#attr.ConvertToTargetCurrency.targetAmountWithCommaSeparator"/>
                                                    <ffi:getProperty name="AddPayment" property="Account.CurrencyCode"/>
												</ffi:cinclude>
												 <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
								        	    </s:else>
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeAccountLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="confirmBillPayPayeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="UserAccountNumber" />
											<ffi:getProperty name="payment.payee" property="UserAccountNumber" /></td>

											<td id="confirmBillPayFromAccountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>

											<td id="confirmBillPayFromAccountValue" class="columndata" colspan="2">
												<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
												<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>
												<s:set var="AccountDisplayTextTaskBean" value="#session.AddPayment.Account" scope="request" />
												<ffi:process name="AccountDisplayTextTask"/>
												<ffi:getProperty name="AccountDisplayTextTask" property="accountDisplayText"/>
												<ffi:getProperty name="AddPayment" property="Account.DisplayText"/>
												<ffi:cinclude value1="${AddPayment.Account.NickName}" value2="" operator="notEquals" >
													 - <ffi:getProperty name="AddPayment" property="Account.NickName"/>
										 		</ffi:cinclude>
												 - <ffi:getProperty name="AddPayment" property="Account.CurrencyCode"/>
												 <s:property value="%{payment.account.accountDisplayText}"/>
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeFirstAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_20"/></span></div>
											</td>
											<td id="confirmBillPayPayeeFirstAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street" />
											<ffi:getProperty name="payment.payee" property="Street" />
											</td>
											<td id="confirmBillPaymentDateLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_76"/></span></div>
											</td>
											<td id="confirmBillPaymentDateValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayment" property="PayDate" />
											<ffi:getProperty name="payment" property="PayDate" />
											<s:property value="%{payment.payDate}"/>	
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeSecondAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.billpay_21"/></span></div>
											</td>
											<td id="confirmBillPayPayeeSecondAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street2" />
											<ffi:getProperty name="payment.payee" property="Street2" /></td>
											<td valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_deliver_by"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle">
												<ffi:setProperty name="AddPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
												<ffi:getProperty name="AddPayment" property="DeliverByDate"/>	
												<s:property value="%{payment.deliverByDate}"/>												
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeThirdAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="confirmBillPayPayeeThirdAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street3" />
											<ffi:getProperty name="payment.payee" property="Street3" />
											</td>
											<td id="confirmBillPayPayeeFrequencyLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_351"/></span></div>
											</td>
											<td id="confirmBillPayPayeeFrequencyValue" class="columndata" colspan="2" valign="middle">
												<ffi:getProperty name="AddPayment" property="Frequency" />
												<ffi:getProperty name="payment" property="Frequency" />
												<s:property value="%{payment.frequency}"/>
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeCityLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/> </span></div>
											</td>
											<td id="confirmBillPayPayeeCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="payment.payee" property="City" />
											<ffi:getProperty name="Payee" property="City" /></td>
											<td id="confirmBillPayPaymentsLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_321"/></span>
												</div>
											</td>
											<td id="confirmBillPayPaymentsValue" class="columndata" colspan="2" valign="middle">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="AddPayment" property="NumberPayments" />
												</ffi:cinclude>
												<s:if test="%{payment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{payment.NumberPayments}"/>
												</s:else>
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeStateLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="confirmBillPayPayeeStateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{payment.payee.stateDisplayName}"/>
											</td>
											<td id="confirmBillPayPayeeMemoLabel"valign="middle" class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td id="confirmBillPayPayeeMemoValue" class="columndata" colspan="2" valign="middle">
											<ffi:getProperty name="payment" property="Memo" /></td>
											<ffi:cinclude value1="${AddPayment.REGISTER_CATEGORY_ID}" value2="">
												<td id="confirmBillPayPayeeMemoLabel"valign="middle" class="tbrd_l">
													<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
												</td>
												<td id="confirmBillPayPayeeMemoValue" class="columndata" colspan="2" valign="middle">
												<ffi:getProperty name="AddPayment" property="Memo" /></td>
											</ffi:cinclude>
											<ffi:cinclude value1="${AddPayment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
												<td id="confirmBillPayPayeeCategoryLabel" class="columndata tbrd_l">
													<div align="right">
														<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span>
													</div>
												</td>
												<td id="confirmBillPayPayeeCategoryValue" colspan="2" class="columndata">
													<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
													<ffi:list collection="RegisterCategories" items="Category">
														<ffi:cinclude value1="${Category.Id}" value2="${AddPayment.REGISTER_CATEGORY_ID}">
															<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
																<ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
																<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
														</ffi:cinclude>
													</ffi:list>
												</td>
											</ffi:cinclude>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeZipCodeLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="confirmBillPayPayeeZipCodeValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<ffi:getProperty name="payment.payee" property="ZipCode" />
											</td>
											<td id="confirmBillPayPayeeStatusLabel" valign="middle" class="tbrd_l">
                                                <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_388"/></span></div>
                                            </td>
                                            <td id="confirmBillPayPayeeStatusValue" class="columndata" colspan="2">
                                            	<s:if test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL}">
													<s:property value="%{payment.statusName}"/>
												</s:if>
												<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BATCH_INPROCESS ||
														payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED
														}">
													<s:text name="jsp.default_237"/>
												</s:elseif>
												<s:else>
													<s:text name="jsp.default_530"/>
												</s:else>
	                                            <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	                                            	<ffi:getProperty name="Payment" property="StatusName" />
	                                            </ffi:cinclude>
	                                            <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		                                            <ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_BATCH_INPROCESS) %>">
		                                            	<s:text name="jsp.default_237"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>">
														<s:text name="jsp.default_237"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_BATCH_INPROCESS) %>" operator="notEquals">
														<ffi:cinclude value1="${Payment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>" operator="notEquals">
															<s:text name="jsp.default_530"/>
														</ffi:cinclude>
													</ffi:cinclude>
												</ffi:cinclude>
											</td>
                                        </tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeCountryLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/></span></div>
											</td>
											<td id="confirmBillPayPayeeCountryValue" class="columndata" colspan="2">
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="confirmBillPayPayeeCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{payment.payee.countryDisplayName}"/></td>
											<td id="confirmBillPayPayeeReferenceLabel" valign="middle" class="tbrd_l">
                                                <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_347"/></span></div>
                                            </td>
                                            <td id="confirmBillPayPayeeReferenceValue" class="columndata" colspan="2">
                                            <ffi:getProperty name="Payment" property="TrackingID" />
                                            <s:property value="%{payment.TrackingID}"/></td>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeeContactLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="confirmBillPayPayeeContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ContactName" />
											<ffi:getProperty name="payment.payee" property="ContactName" /></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr> --%>
										<%-- <tr>
											<td id="confirmBillPayPayeePhoneLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div>
											</td>
											<td id="confirmBillPayPayeePhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="payment.payee" property="Phone" />
											<ffi:getProperty name="Payee" property="Phone" /></td>
											<td class="columndata tbrd_l">&nbsp;</td>
											<td></td>
											<td></td>
										</tr> --%>
                                        <%-- <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td id="confirmBillPayPayeeEstimatedAmountLabel" class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude> --%>
                                         <%-- <s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if> --%>
                                        <%-- <tr>
											<td colspan="6"><div align="center">
													<sj:a
										                button="true"
										                buttonIcon="ui-icon-refresh"
														onClickTopics="cancelBillpayForm,resetBillpayConfirmDiv"
										        ><s:text name="jsp.default_175"/></sj:a>
									        </div></td>
											<td></td>
										</tr>
									</table>
								</div>
							
						</td>
						<td></td>
					</tr>
</table> --%>
<s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
    <div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
	<sj:a
    button="true"
    summaryDivId="summary" buttonType="done"
	onClickTopics="showSummary,cancelBillpayForm,resetBillpayConfirmDiv">
		<s:text name="jsp.default_175"/>
	</sj:a>
										        
</div>
</div>
</form>
<script type="text/javascript">
$(document).ready(function(){
	$( ".toggleClick" ).click(function(){ 
		if($(this).next().css('display') == 'none'){
			$(this).next().slideDown();
			$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
			$(this).removeClass("expandArrow").addClass("collapseArrow");
		}else{
			$(this).next().slideUp();
			$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
			$(this).addClass("expandArrow").removeClass("collapseArrow");
		}
	});
});

</script>
<%--=================== PAGE SECURITY CODE - Part 2 Start ====================--%>

<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>
