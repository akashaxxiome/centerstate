<%-- <%@page import="com.ffusion.beans.billpay.Payee"%> --%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- <%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %> --%>

<ffi:help id="payments_billpaytemplatesingleconfirm" className="moduleHelpClass"/>
<script language="JavaScript">
function openSaveTemplate(URLExtra)  {
	MyWindow=window.open(''+URLExtra,'','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=750,height=340');
}
</script>
<s:form id="BillpayNewTempSend" namespace="/pages/jsp/billpay" validate="false" action="editBillPayTemplateAction" method="post" name="BillpayNewSend" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="template" value="true"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id=""><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id=""><s:property value="%{editPayment.payee.Name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue" id=""><s:property value="%{editPayment.account.accountDisplayText}"/>
				</span>
			</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<ffi:cinclude value1="${EditPayment.DateChanged}" value2="true">
	<div style="color:red" align="center"><s:text name="jsp.billpay_118"/></div>
</ffi:cinclude>
   		<div  class="blockHead toggleClick expandArrow">
			<s:text name="jsp.billpay.payee_summary" />: <s:property value="%{editPayment.payee.Name}"/> (<s:property value="%{editPayment.payee.NickName}"/> - <s:property value="%{editPayment.payee.userAccountNumber}"/>)
		<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
		<div class="toggleBlock hidden">
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_314"/>:</span>
					<span class="columndata" >
					     <s:property value="%{editPayment.payee.Name}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.NickName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.userAccountNumber}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.contactName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.phone}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.street}"/>												
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.street2}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
					<span class="columndata"  >
						<s:property value="%{editPayment.payee.street3}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
					<span class="columndata"  >
						<s:property value="%{editPayment.payee.city}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
					<span   class="columndata">
						<s:property value="%{editPayment.payee.stateDisplayName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
					<span  class="columndata">
						<s:property value="%{editPayment.payee.zipCode}"/>
					</span>	
											
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
					<span class="columndata" >
						<s:property value="%{editPayment.payee.countryDisplayName}"/>
					</span>
				</div>
			</div>
 		</div>
   </div>
<div class="marginTop10"></div>
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel">
												
												<s:text name="jsp.default_416"/>:												
											</span>
											<span class="columndata" >
											<%-- <ffi:getProperty name="EditPayment" property="TemplateName" /> --%>
												<s:property value="%{editPayment.TemplateName}"/>
											</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
												
													<s:text name="jsp.default_279"/>:
											</span>
											<span class="columndata" >
												<%-- <ffi:getProperty name="EditPayment" property="Memo" /> --%>
												<s:property value="%{editPayment.memo}"/>												
											</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel">
												
												<s:text name="jsp.default_43"/>:
												
											</span>
											<span class="columndata" >
											<s:property value="%{editPayment.AmountValue.CurrencyStringNoSymbol}"/>
											<s:property value="%{editPayment.Payee.PayeeRoute.CurrencyCode}"/>
												<%-- <ffi:getProperty name="EditPayment" property="AmountValue.CurrencyStringNoSymbol" />
												<ffi:getProperty name="EditPayment" property="Payee.PayeeRoute.CurrencyCode"/> --%>
											</span>											
											<%--required by validator as for template zero amount is allowed. --%>
											<input type="hidden" id="amount" name="amount" 
											value="<ffi:getProperty name="EditPayment" property="AmountValue.CurrencyStringNoSymbol"/>">
											</input>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
												
                                                <s:if test="%{editPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	
													<s:text name="jsp.billpay_40"/>:
												</s:else>
												
											</span>
											<span class="columndata" >
												
												<s:if test="%{editPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{editPayment.convertedAmount}"/>  <s:property value="%{editPayment.account.currencyCode}"/>
								        	    </s:else>
											</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel">
												
												<s:text name="jsp.default_351"/> :
												</span>
											<span class="columndata" >
											<%-- <ffi:getProperty name="EditPayment" property="Frequency" /> --%>
											<s:property value="%{editPayment.frequency}"/>
											</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
												
													<s:text name="jsp.default_321"/>:
											</span>
											<span class="columndata" >
												
												<s:if test="%{editPayment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{editPayment.NumberPayments}"/>
												</s:else>
											</span>
				</div>
			</div>
 		</div>
<s:if test="%{editPayment.AmtCurrency!=editPayment.Account.CurrencyCode}">
	<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
			<sj:a id="cancelBillpayConfirm" 
                button="true" 
                summaryDivId="summary" buttonType="cancel"
				onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
        	><s:text name="jsp.default_82"/></sj:a>
        	
        	<sj:a id="backInitiateFormButton" 
       						button="true" 
				onClickTopics="backToBillpayInput"
   						><s:text name="jsp.default_57"/></sj:a>
               				
			<sj:a 
				id="sendSingleBillpaySubmit"
				formIds="BillpayNewTempSend"
                            targets="confirmDiv" 
                            button="true" 
                            onBeforeTopics="beforeSendBillpayForm"
                            onSuccessTopics="sendBillpayTemplateFormSuccess"
                            onErrorTopics="errorSendBillpayForm"
                            onCompleteTopics="sendBillpayFormComplete"
                 				><s:text name="jsp.default_366"/></sj:a>
</div>
</div>
</s:form>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>