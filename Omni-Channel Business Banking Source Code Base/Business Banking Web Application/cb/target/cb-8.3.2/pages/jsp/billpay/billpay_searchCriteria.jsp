<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.ffusion.tasks.billpay.Task"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<script type="text/javascript">

$(document).ready(function(){

	
	ns.billpay.setup();
	
	//ns.billpay.loadDashBoard();
	ns.billpay.getAllPayments();
});	
		$(function(){
			$("#dateRangeValue").selectmenu({width: 150});
			//$("#AccountId").selectmenu({width: 280});
			
			var value = {value:"All Accounts", label: "All Accounts"};
			$("#AccountId").lookupbox({
				"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action",
				"controlType":"server",
				"collectionKey":"1",
				"size":"30",
				"module":"billpaydashboard",
				"defaultOption":value
			});
			
			var value = {value:"All Payees", label: js_all_payees_value};
			$("#PayeeId").lookupbox({
				"source":"/cb/pages/jsp/billpay/BillPayPayeesLookupBoxAction.action",
				"controlType":"server",
				"collectionKey":"1",
				"size":"30",
				"module":"billpayPayees",
				"defaultOption":value
			});
			
			/* init date range picker component*/
			var aUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?onlyOneDirection=true&calDirection=back&calForm=LogForm&calTarget=FindLogMessages.StartDate"/>';	
			var aConfig = {
				url:aUrl,
				startDateBox:$("#startDate"),
				endDateBox:$("#endDate")
			}
			$("#billpayDateRangeBox").extdaterangepicker(aConfig);
		});

		// resets the selected index of the given dropdown to 0
		function clearDropDown() {
			$("#dateRangeValue").selectmenu('value',"");
			//$("#AccountId").selectmenu('value',"");
			//$("#PayeeId").selectmenu('value',"");
			//$("#Amount").val("");
		}

		// clears the 2 text fields
		function clearTextFields( field1, field2, formName ) {		
			if($("#dateRangeValue").val() == null || $("#dateRangeValue").val() == ""){
				$("#"+field1).val($("#startDateDefault").val());
				$("#"+field2).val($("#endDateDefault").val());
			}
			else{
				$("#"+field1).val("");
				$("#"+field2).val("");
			}
				
			/* formName[field1].value = '';
			formName[field2].value = '';
			if ($("#dateRangeValue").val() == null || $("#dateRangeValue").val() == "") {
				 $("#StartDate").val($("#StartDateSessionValue").val());
				$("#EndDateID").val($("#EndDateSessionValue").val()); */
				
				
		}
	</script>
<div id="quicksearchcriteria" style="display: none;">
	<s:form action="/pages/jsp/billpay/dashboardAction_verify.action"
		method="post" id="QuickSearchBillpayFormID"
		name="QuickSearchBillpayForm" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
		<table border="0">
			<tr>
				<td>
					<table border="0" cellspacing="5">
						<s:hidden id="StartDateSessionValue" name="StartDateSessionValue"
							value="%{#session.GetDatesFromDateRange.StartDate}" />
						<s:hidden id="EndDateSessionValue" name="EndDateSessionValue"
							value="%{#session.GetDatesFromDateRange.EndDate}" />

						<%-- <tr>
							
							 <td><label for=""><s:text name="jsp.default_137" /></label>
							</td>
							<td><sj:datepicker title="%{getText('Choose_start_date')}"
									value="%{pendingPaymentSearchCriteria.StartDate}"
									id="startDate" name="pendingPaymentSearchCriteria.startDate"
									label="From Date" maxlength="10" buttonImageOnly="true"
									cssClass="ui-widget-content ui-corner-all"
									onfocus="clearDropDown();" /> <br><span
									id="pendingPaymentSearchCriteria.startDateError"></span>
									<s:hidden id="startDateDefault" value="%{pendingPaymentSearchCriteria.StartDate}"></s:hidden>
									</td>
							<td><label for=""><s:text name="jsp.default_423.1" /></label>
							</td>
							<td><sj:datepicker title="%{getText('Choose_to_date')}"
									value="%{pendingPaymentSearchCriteria.EndDate}" id="endDate"
									name="pendingPaymentSearchCriteria.endDate" label="To Date"
									maxlength="10" buttonImageOnly="true"
									cssClass="ui-widget-content ui-corner-all"
									onfocus="clearDropDown();" /> <br><span id="pendingPaymentSearchCriteria.endDateError"></span>
									<s:hidden id="endDateDefault" value="%{pendingPaymentSearchCriteria.endDate}"></s:hidden></td>
							<td align="left"><sj:a targets="quick"
									id="quicksearchbutton" formIds="QuickSearchBillpayFormID"
									button="true" buttonIcon="ui-icon-info" validate="true"
									validateFunction="customValidation"
									onclick="removeValidationErrors();"
									onCompleteTopics="quickSearchBillpayCompleteTopic">
									<span class="ui-icon-info"><s:text name="jsp.default_6" /></span>
								</sj:a></td> 

						</tr>
						<sj:a targets="quick"
									id="quicksearchbutton" formIds="QuickSearchBillpayFormID"
									button="true" buttonIcon="ui-icon-info" validate="true"
									validateFunction="customValidation"
									onclick="removeValidationErrors();"
									onCompleteTopics="quickSearchBillpayCompleteTopic">
									<span class="ui-icon-info"><s:text name="jsp.default_6" /></span>
								</sj:a> --%>
					</table>
				</td>
			</tr>

			<tr class="tdWithPadding5">
				<td>
					
					<span id="dateRangeValueError"></span>
				
				</td>
							<td width="155px">
								<span class="sectionsubhead"><s:text name="jsp.default.label.date.range"/></span></br>
								<input type="text" id="billpayDateRangeBox" />
								
								<input type="hidden" value="<ffi:getProperty name='defaultPaymentSearchCriteria' property='startDate' />" id="startDate" name="defaultPaymentSearchCriteria.startDate"/>
								<input type="hidden" value="<ffi:getProperty name='defaultPaymentSearchCriteria' property='endDate' />" id="endDate"	name="defaultPaymentSearchCriteria.endDate" />
								 
								<%-- <br><span id="pendingPaymentSearchCriteria.startDateError"></span>
								<br><span id="pendingPaymentSearchCriteria.endDateError"></span> --%>
								<s:hidden id="startDateDefault" value="%{defaultPaymentSearchCriteria.StartDate}"></s:hidden>
								<s:hidden id="endDateDefault" value="%{defaultPaymentSearchCriteria.endDate}"></s:hidden>
							</td>
						
						
							<%-- <td><label for=""><s:text
										name="jsp.transfers_text_Or" /></label></td>
							<td>
							<s:select
									list="%{pendingPaymentSearchCriteria.dateSpanList}"
									id="dateRangeValue" class="txtbox" value="Select"
									headerKey=""
									headerValue="Select"
									name="pendingPaymentSearchCriteria.dateRangeValue"
									onChange="clearTextFields( 'startDate', 'endDate', document.QuickSearchBillpayForm );"></s:select> --%>
								<%-- <select id="dateRangeValue" class="txtbox"
								name="paymentDashboardSearchCriteria.DateRangeValue"
								onChange="clearTextFields( 'StartDate', 'endDate', document.QuickSearchBillpayForm );">
									<option value="">
										<s:text name="jsp.transfers_text_select" />
									</option>
									<option value="<%= GetDatesFromDateRange.TODAY %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.TODAY %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_today"/></option>
								<option value="<%= GetDatesFromDateRange.YESTERDAY %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.YESTERDAY %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_yesterday"/></option>
								<option value="<%= GetDatesFromDateRange.CURRENT_WEEK %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.CURRENT_WEEK %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_current_task"/></option>
								<option value="<%= GetDatesFromDateRange.CURRENT_MONTH %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.THIS_MONTH_TO_DATE %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_current_month"/></option>
								<option value="<%= GetDatesFromDateRange.CURRENT_30_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.CURRENT_30_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_current30days"/></option>
								<option value="<%= GetDatesFromDateRange.LAST_WEEK %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_WEEK %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_lastweek"/></option>
								<option value="<%= GetDatesFromDateRange.LAST_MONTH %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_MONTH %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_lastmonth"/></option>
								<option value="<%= GetDatesFromDateRange.LAST_7_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_7_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last7days"/></option>
								<option value="<%= GetDatesFromDateRange.LAST_30_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_30_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last30days"/></option>
								<option value="<%= GetDatesFromDateRange.LAST_60_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_60_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last60days"/></option>
								<option value="<%= GetDatesFromDateRange.LAST_90_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_90_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last90days"/></option>
								<option value="<%= GetDatesFromDateRange.NEXT_WEEK %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.NEXT_WEEK %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_nextweek"/></option>
								<option value="<%= GetDatesFromDateRange.NEXT_MONTH %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.NEXT_MONTH %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_nextmonth"/></option>
								<option value="<%= GetDatesFromDateRange.NEXT_30_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.NEXT_30_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_next30days"/></option>
								<option value="<%= GetDatesFromDateRange.CURRENT_NEXT_30_DAYS %>" <ffi:cinclude value1="${GetDatesFromDateRange.DateRangeValue}" value2="<%= GetDatesFromDateRange.CURRENT_NEXT_30_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_plusminus30days"/></option>
							</select> --%>
							<%-- <% session.setAttribute("FFIGetDatesFromDateRange", session.getAttribute("GetDatesFromDateRange")); %> --%>

							<%-- <td align="left">
							<label for="" ><s:text name="jsp.billpay_account" /></label></td> --%>
							<td>
								<%-- <ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
							<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="PaymentAccounts"/>
							<ffi:process name="SetAccountsCollectionDisplayText"/>
							<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>
							
							<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
							<ffi:setProperty name="BuildIndex" property="CollectionName" value="PaymentAccounts"/>
							<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BILLPAY_ACCOUNTS_INDEX %>"/>
							<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
							<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
							<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
							<ffi:process name="BuildIndex"/>
							<ffi:removeProperty name="BuildIndex"/>
						
							
							<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
							<ffi:setProperty name="BuildIndex" property="CollectionName" value="PaymentAccounts"/>
							<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BILLPAY_ACCOUNTS_INDEX_BY_TYPE %>"/>
							<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
							<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
							<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
							<ffi:process name="BuildIndex"/>
							<ffi:removeProperty name="BuildIndex"/>
							
							<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
							<ffi:setProperty name="BuildIndex" property="CollectionName" value="PaymentAccounts"/>
							<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BILLPAY_ACCOUNTS_INDEX_BY_CURRENCY%>"/>
							<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
							<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
							<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
							<ffi:process name="BuildIndex"/>
							<ffi:removeProperty name="BuildIndex"/>
						
							<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
							<select class="txtbox" id="AccountId" name="AccountId" size="1">
								
							</select>
							<ffi:setProperty name="PaymentAccounts" property="FilterOnFilter" value="ALL"/> --%>
								<%-- <s:select class="txtbox" id="AccountId" name="AccountId" list="pendingPaymentSearchCriteria">
								</s:select> --%>
								<span class="sectionsubhead"><s:text name="jsp.billpay_account"/></span></br>
								<select class="txtbox" id="AccountId" name="defaultPaymentSearchCriteria.accountID" size="1">
									<option selected="selected"></option>
								</select>
							</td>
							<%-- <td align="left">
							<label for=""><s:text name="jsp.billpay_payee" /></label>
							</td> --%>
							<td>
								<%-- <ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
								<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= Task.PAYEES %>"/>
								<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BILLPAY_PAYEES_INDEX %>"/>
								<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
								<ffi:setProperty name="BuildIndex" property="BeanProperty" value="PayeeUniqueKey"/>
								<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
							<ffi:process name="BuildIndex"/>
							<ffi:removeProperty name="BuildIndex"/>
							<select id="PayeeId" name="PayeeId" class="form_elements2" style="width: 120px;">
							
							</select> --%> 
							<span class="sectionsubhead"><s:text name="jsp.billpay_payee"/></span></br>
							<select id="PayeeId" name="defaultPaymentSearchCriteria.payeeID"
								class="form_elements2" style="width: 120px;">
								<option selected="selected"></option>
							</select>
							</td>
							<%-- <td align="left"><label for=""><s:text
										name="jsp.billpay_amount" /></label></td> --%>
							<td nowrap="nowrap" width="130px">
							<span class="sectionsubhead"><s:text name="jsp.billpay_amount"/></span></br>
							<input type="text" id="SearchAmount"
								name="defaultPaymentSearchCriteria.amount" size="13"
								maxlength="20" value="<ffi:getProperty name='Amount'/>"
								class="ui-widget-content ui-corner-all">
						   </td>
				</td>
				<td >
					<span>&nbsp;</span></br>
					<sj:a targets="quick"
						id="quicksearchbutton" formIds="QuickSearchBillpayFormID"
						button="true" validate="true"
						validateFunction="customValidation"
						onclick="removeValidationErrors();"
						onCompleteTopics="quickSearchBillpayCompleteTopic">
						<s:text name="jsp.default_6" />
					</sj:a>
				</td>
			</tr>
			<tr>
				<td></td>
				<td  width="155px">
					<span id="defaultPaymentSearchCriteria.startDateError"></span>
					<span id="defaultPaymentSearchCriteria.endDateError"></span>
					<span id="defaultPaymentSearchCriteria.dateRangeValueError"></span>
				</td>
				<td></td>
				<td></td>
				<td style="vertical-align: top;"  width="130px">
					<span id="amountError"></span>
				</td>
			</tr>
		</table>
	</s:form>
	<ffi:removeProperty name="GetDatesFromDateRange" />
</div>