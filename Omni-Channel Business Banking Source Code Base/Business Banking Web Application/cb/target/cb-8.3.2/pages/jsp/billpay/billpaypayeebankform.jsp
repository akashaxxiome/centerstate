<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaypayeebankform" className="moduleHelpClass"/>	
<ffi:removeProperty name="searchBank"/>
<ffi:removeProperty name="achRTN"/>
<%-- <ffi:object name="com.ffusion.tasks.billpay.GetBillPayBanks" id="GetBillPayBanks"></ffi:object>

<%
	session.setAttribute("FFIGetBillPayBanks", session.getAttribute("GetBillPayBanks"));
%> --%>
		
		<div id="billpayBankResultdiv"></div>
        <div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td class="columndata" align="center" class="ltrow2_color">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							<tr>
								<td><span class="sectionhead">&gt; <s:text name="jsp.default_520"/></span></td>
							</tr>
							<tr>
								<td>
						<s:form action="/pages/jsp/billpay/getBillpayBanksAction.action" method="post"  theme="simple" id="BillpayPayeeBankID" name="BillpayPayeeBank">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="search" value="true">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td width="10%" align="right" class="sectionhead"><s:text name="jsp.default_61"/>&nbsp;</td>
											<td><input class="ui-widget-content ui-corner-all" type="text" name="institutionName" size="25" value="<ffi:getProperty name="GetBillPayBanks" property="BankName"/>"></td>
											<td align="right" class="sectionhead"><s:text name="jsp.default_101"/>&nbsp;</td>
											<td><input class="ui-widget-content ui-corner-all" type="text" name="city" size="25" value="<ffi:getProperty name="GetBillPayBanks" property="BankCity"/>"></td>
											<td align="right" class="sectionhead"><s:text name="jsp.default_386"/>&nbsp;</td>
											<td><input class="ui-widget-content ui-corner-all" type="text" name="state" size="25" value="<ffi:getProperty name="GetBillPayBanks" property="BankState"/>"></td>
										</tr>
										<tr>	
											<td align="right" class="sectionhead"><s:text name="jsp.default_115"/>&nbsp;</td>
											<td>
	                                          	<s:url id="remoteBankurl" value="/pages/util/getCountriesAction.action" escapeAmp="false"> 
	                                          		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
										       	</s:url>
										       	<sj:select 
											       href="%{remoteBankurl}" 
											       id="selectPayeeBankCountryID" 
											       onChangeTopics="reloadsecondlist"
											       name="country" 
											       list="countryObjList" 
											       listKey="key" 
											       listValue="value" 
											       emptyOption="false"
											       value="UNITED STATES"
											       cssClass="ui-widget-content ui-corner-all"
										      	/>
	                                            </td>
											<td align="right" class="sectionhead"><s:text name="jsp.default_201"/>&nbsp;</td>
											<td><input class="ui-widget-content ui-corner-all" type="text" name="achRTN" size="25" value="<ffi:getProperty name="GetBillPayBanks" property="AchRTN"/>"></td>
											<td></td>
										</tr>
										<tr>
										
											<td align="center" colspan="10">
											<br>	
												<sj:a 
										                button="true" 
														onClickTopics="closeDialog"
										        ><s:text name="jsp.default_82"/></sj:a>
										        
												<sj:a 
														id="billpayPayeeBankSubmit"
														formIds="BillpayPayeeBankID"
														targets="billpayBankResultdiv"
						                                button="true" 
						                                onCompleteTopics="completeBillpayPayeeBank"
			                        				><s:text name="jsp.default_373"/></sj:a>
											</td>
										</tr>
									</table>
									&nbsp;
									</s:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<div id="billpayBanksDiv" class="portlet-content">
		<s:include value="/pages/jsp/billpay/billpaypayeebanklist.jsp"/>
	</div>	
