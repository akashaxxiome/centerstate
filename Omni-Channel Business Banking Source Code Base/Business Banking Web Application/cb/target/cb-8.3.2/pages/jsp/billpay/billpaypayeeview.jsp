<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpayview" className="moduleHelpClass"/>
<%-- <s:set var="tmpI18nStr" value="%{getText('jsp.billpay_73')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude value1="${PaymentReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/payments_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0" hspace="3"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/payments/i_payees_on.gif" alt="" border="0" hspace="3"><a href="${SecurePath}payments/billpaytemplates.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_templates.gif" alt="" border="0" hspace="3"></a>'/>

for localizing state name
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>


<ffi:object id="GetDefaultCurrency" name="com.ffusion.tasks.billpay.GetDefaultCurrency" scope="session"/>
<ffi:process name="GetDefaultCurrency"/>
<ffi:cinclude value1="${Payee.PayeeRoute.CurrencyCode}" value2="" operator="equals">
<ffi:setProperty name="Payee" property="PayeeRoute.CurrencyCode" value="${DEFAULT_CURRENCY.CurrencyCode}"/>
</ffi:cinclude>


<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/> --%>
<div class="approvalDialogHt2">
<form action="billpayadd.jsp" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>	
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.info"/></div>
   		<div class="blockContent label130">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_314"/>:</span>
					<span id="viewPayeeNameValue" class="columndata" >
						<s:property value="%{viewPayee.name}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
					<span id="viewPayeeNickNameValue" class="columndata" ><s:property value="%{viewPayee.nickName}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
					<span id="viewPayeeContactValue" class="columndata" >
						<s:property value="%{viewPayee.contactName}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/>:</span>
					<span id="viewPayeePhoneValue" class="columndata" ><s:property value="%{viewPayee.phone}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeAccountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_19"/>:</span>
					<span id="viewPayeeAccountValue" class="columndata" ><s:property value="%{viewPayee.userAccountNumber}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
					<span id="viewPayeeAddress1Value" class="columndata" ><s:property value="%{viewPayee.street}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
					<span id="viewPayeeAddress2Value" class="columndata" ><s:property value="%{viewPayee.street2}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeeAddress3Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
					<span id="viewPayeeAddress3Value" class="columndata" ><s:property value="%{viewPayee.street3}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
					<span id="viewPayeeCityValue" class="columndata" ><s:property value="%{viewPayee.city}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
					<span id="viewPayeeStateValue" class="columndata" >
						<s:property value="%{viewPayee.stateDisplayName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeZipLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
					<span id="viewPayeeZipValue" class="columndata" >
						<s:property value="%{viewPayee.zipCode}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
					<span id="viewPayeeCountryValue" class="columndata" ><s:property value="%{viewPayee.countryDisplayName}"/></span>
				</div>
			</div>
 		</div>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.bank.info"/></div>
   		<div class="blockContent label130">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeRountingNoLabel" class="sectionsubhead sectionLabel"><s:property value="%{bankIdentifierDisplayText}"/>:</span>
					<span id="viewPayeeRountingNoValue" class="columndata" >
						<s:property value="%{viewPayee.payeeRoute.bankIdentifier}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="viewPayeeBankAccountNoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_account_number"/>:</span>
					<span id="viewPayeeBankAccountNoValue" class="columndata" ><s:property value="%{viewPayee.payeeRoute.accountID}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewPayeeBankAccountTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>
					<span id="viewPayeeBankAccountTypeValue" class="columndata" >
					<s:if test="%{viewPayee.isGlobal}">
						<s:property value="%{viewPayee.payeeRoute.acctType}"/>
					</s:if>
					<s:else>
						<s:property value="%{accountTypeSelectList[viewPayee.payeeRoute.acctType]}"/>
					</s:else>
					</span>
				</div>
				 <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
					<div class="inlineBlock">
						<span id="viewPayeeBankAccountCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_account_currency"/>:</span>
						<span id="viewPayeeBankAccountCurrencyValue" class="columndata" >
							<s:property value="%{viewPayee.payeeRoute.currencyCode}"/>
						</span>
					</div>
				</ffi:cinclude>
			</div>
 		</div>
<div class="ffivisible" style="height:100px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter">
	<sj:a id="donePayeeView" button="true" onClickTopics="closeDialog"><s:text name="jsp.default_175"/></sj:a>
</div>	
</div>
</form>		
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
							
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_114"/><br>
												</span></td>
										</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td id="viewPayeeNameLabel" class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="viewPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Name" />
											<s:property value="%{viewPayee.name}"/>
											</td>
											<td id="viewPayeeAddress1Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="viewPayeeAddress1Value" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street" />
											<s:property value="%{viewPayee.street}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span></div>
											</td>
											<td id="viewPayeeNickNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="NickName" />
											<s:property value="%{viewPayee.nickName}"/>
											</td>
											<td id="viewPayeeAddress2Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="viewPayeeAddress2Value" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street2" />
											<s:property value="%{viewPayee.street2}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeeContactLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="viewPayeeContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ContactName" />
											<s:property value="%{viewPayee.contactName}"/>
											</td>
											<td id="viewPayeeAddress3Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="viewPayeeAddress3Value" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street3" />
											<s:property value="%{viewPayee.street3}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeePhoneLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td id="viewPayeePhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Phone" />
											<s:property value="%{viewPayee.phone}"/></td>
											<td id="viewPayeeCityLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="viewPayeeCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="City" />
											<s:property value="%{viewPayee.city}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeeAccountLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span></div>
											</td>
											<td id="viewPayeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="UserAccountNumber" />
											<s:property value="%{viewPayee.userAccountNumber}"/>
											</td>
											<td id="viewPayeeStateLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="viewPayeeStateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{viewPayee.stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeeRountingNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">
													<ffi:getProperty name="TempBankIdentifierDisplayText"/>
													<s:property value="%{bankIdentifierDisplayText}"/>
													</span></div>
											</td>
											<td id="viewPayeeRountingNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="PayeeRoute.BankIdentifier" />
											<s:property value="%{viewPayee.payeeRoute.bankIdentifier}"/>
											</td>
											<td id="viewPayeeZipLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td id="viewPayeeZipValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<s:property value="%{viewPayee.zipCode}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeeBankAccountNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_26"/></span></div>
											</td>
											<td id="viewPayeeBankAccountNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="PayeeRoute.AccountID" />
											<s:property value="%{viewPayee.payeeRoute.accountID}"/>
											</td>
											<td id="viewPayeeCountryLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="viewPayeeCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{viewPayee.countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="viewPayeeBankAccountTypeLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_27"/></span></div>
											</td>
											<ffi:object id="GetTypeName" name="com.ffusion.tasks.util.Resource" />
											<ffi:setProperty name="GetTypeName" property="ResourceFilename" value="com.ffusion.beans.accounts.resources" />
											<ffi:setProperty name="GetTypeName" property="ResourceID" value="${Payee.PayeeRoute.AcctType}" />
											<ffi:process name="GetTypeName" />
											<td id="viewPayeeBankAccountTypeValue" class="columndata" colspan="2">
											<ffi:getProperty name="GetTypeName" property="Resource" /></td>
											<s:property value="%{accountTypeSelectList[viewPayee.payeeRoute.acctType]}"/>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										 <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
										<tr>
											<td id="viewPayeeBankAccountCurrencyLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_25"/></span></div>
											</td>
											<td id="viewPayeeBankAccountCurrencyValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="PayeeRoute.CurrencyCode" />
											<s:property value="%{viewPayee.payeeRoute.currencyCode}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										</ffi:cinclude>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right">

												</div>
											</td>
											<td colspan="4"><div align="center"><br>
												<sj:a id="donePayeeView"
										                button="true" 
														onClickTopics="closeDialog"
										        ><s:text name="jsp.default_175"/></sj:a>
										        	</div>
											</td>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table>
 --%>

				<%-- <ffi:object id="GetDefaultCurrency" name="com.ffusion.tasks.billpay.GetDefaultCurrency" scope="session"/>
				<ffi:process name="GetDefaultCurrency"/> --%>
				
<%-- <ffi:removeProperty name="GetStateProvinceDefnForState"/> --%>
