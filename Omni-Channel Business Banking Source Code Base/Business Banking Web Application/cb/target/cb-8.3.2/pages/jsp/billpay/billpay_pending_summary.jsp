<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page import="com.ffusion.beans.billpay.PaymentDefines" %>

<s:set name="recModelConstant" value="@com.ffusion.beans.billpay.PaymentDefines@PAYMENT_PMTTYPE_RECMODEL"/>

	<ffi:setProperty name="gridDataType" value="local"/>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:setProperty name="gridDataType" value="json"/>
	</ffi:cinclude>
	
<ffi:help id="payments_billpayPendingSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_pendingBillpay" name="DeleteURL" url="/cb/pages/jsp/billpay/cancelBillpayAction_init.action?ID={0}&PaymentType={1}&RecPaymentID={2}" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>
	<ffi:setGridURL grid="GRID_pendingBillpay" name="SkipURL" url="/cb/pages/jsp/billpay/skipBillpayAction_init.action?ID={0}&PaymentType={1}&RecPaymentID={2}&IsSkipInstance=true" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>
	<ffi:setGridURL grid="GRID_pendingBillpay" name="EditURL" url="/cb/pages/jsp/billpay/editBillpayAction_init.action?ID={0}&RecPaymentID={1}&PaymentType={2}&ValidPayDate={3}" parm1="RecPaymentID" parm0="ID" parm2="PaymentType" parm3="PayDate"/>
	<ffi:setGridURL grid="GRID_pendingBillpay" name="ViewURL" url="/cb/pages/jsp/billpay/billpayviewbill.action?ID={0}&PaymentType={1}&RecPaymentID={2}" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>
	<ffi:setGridURL grid="GRID_pendingBillpay" name="InquireURL" url="/cb/pages/jsp/billpay/sendBillPayFundsTransMessageAction_initBillpayInquiry.action?FundsID={0}&PaymentType={1}&recurringId={2}&Subject=Billpay Inquiry" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/billpay/getPaymentGridActions_getPendingPayments.action?collectionName=PendingPayments&GridURLs=GRID_pendingBillpay" URLEncrypt="true"/>
    <s:url id="pendingBillpayUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="pendingBillpayGridId"  
		caption=""  
		sortable="true"  
		dataType="%{#session.gridDataType}"  
		href="%{pendingBillpayUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="payDate"
		sortorder="asc"
		onGridCompleteTopics="addGridControlsEvents,pendingBillpayGridCompleteEvents"> 
		
		<sjg:gridColumn name="payDate" index="payDate" title="%{getText('jsp.default_137')}" sortable="true" width="55"/>
		<sjg:gridColumn name="payeeName" index="payeeName" title="%{getText('jsp.default_313')}" sortable="true" width="100" hidden="true"/>

		<%-- <s:if test="%{#session.SecureUser.CorporateUser}">
			<s:property value="%{#session.SecureUser.CorporateUser}"/>
			<sjg:gridColumn name="account.nickName" id="accountNickName" index="accountNickName" title="%{
			getText('jsp.billpay_6')}" formatter="ns.billpay.customAccountNicknameColumn" sortable="true" width="100"/>
		</s:if>	
		
		<s:if test="%{#session.SecureUser.ConsumerUser}">
			<s:property value="%{#session.SecureUser.ConsumerUser}"/>
			<sjg:gridColumn name="account.consumerDisplayText"
			id="consumerDisplayText" index="consumerDisplayText"
			title="%{getText('jsp.billpay_6')}"
			formatter="" sortable="true"
			width="100" />
		</s:if> --%>
		
		<sjg:gridColumn name="account.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.billpay.transactionDetails')}" search="false" sortable="false" width="120" hidedlg="" formatter="ns.billpay.formaAccountAndPayee"/>
		<sjg:gridColumn name="map.Frequency" index="mapFrequencyValue" title="%{getText('jsp.default_215')}" formatter="ns.billpay.formatBillpayFrequencyColumn" sortable="true" width="60"/>
		
		<s:if test="%{#session.SecureUser.ConsumerUser}">
			<s:if test="%{#recModelConstant.equals(paymentType)}">
				<sjg:gridColumn name="map.NumberPayments" index="numberPayments" title="#" formatter="ns.billpay.formatNumberPayments" sortable="false" width="40"/>
			</s:if>
			<sjg:gridColumn name="status" index="status" title="%{getText('jsp.default_388')}" formatter="ns.billpay.formatBillpayStatusColumn" sortable="true" width="60"/>
		</s:if>			
		
		<s:if test="%{#session.SecureUser.CorporateUser}">
			<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="60"/>
		</s:if>
		
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_43')}" formatter="ns.billpay.formatAmountColumn" sortable="true" width="60" align="right"/>
		
		<sjg:gridColumn name="account.currencyCode" index="accountCurrencyCode" title="%{getText('jsp.billpay_39')}" search="false" sortable="false" width="120" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="account.number" index="accountNumber" title="%{getText('jsp.default_19')}" width="100" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="ID" index="ID" title="" width="1" sortable="false" search="false" formatter="ns.billpay.formatPendingBillpayActionLinks" hidedlg="true" hidden="true" cssClass="__gridActionColumn"/>
		
		<sjg:gridColumn name="memo" index="memo" title="Memo" sortable="false" width="120" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="SkipURL" index="SkipURL" title="%{getText('jsp.default_600')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="InquireURL" index="InquireURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	</sjg:grid>

	<script type="text/javascript">
	<!--		
		$("#pendingBillpayGridId").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	

<ffi:removeProperty name="gridDataType"/>	