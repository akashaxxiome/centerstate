<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaysingle_template_summary" className="moduleHelpClass"/>

 <script language="JavaScript">

function loadBillpayProp(id) {
	var urlString = '/cb/pages/jsp/billpay/billpaytemplatesingleeditform.jsp';

	$.ajax({
		url:"/cb/pages/jsp/billpay/editBillPayTemplateAction_loadPayee.action?payeeID="+id,
		   type: "POST",
		   data: $("#BillpayTempNew").serialize(),
		   success: function(data){
		   /* $("#inputDiv").html(data); */
		   $("#payeeDetailsJsp").html('');
		   $("#payeeDetailsJsp").html(data);
		   $('#expandBillpayTemplatePayee').toggle();
	   }
	});
	
}

$(document).ready(function(){
	
	$(function(){
		$("#AccountID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"billpay",
			"size":"40",
			"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action"
		});
		
		$("#frequencyId").selectmenu({width: 120});
		
		$("#PayeeSelect").lookupbox({
			"source":"/cb/pages/jsp/billpay/BillPayPayeesLookupBoxAction.action",
			"controlType":"server",
			"collectionKey":"1",
			"size":"50",
			"module":"billpayPayees",
		});
	});
	
});	

function toggleRecurring(){
	if ($('input[name="editPayment.openEnded"]')[1].checked == true){
		$('input[name="editPayment.numberPayments"]').attr('disabled',false);
	} else {
		$('input[name="editPayment.numberPayments"]').attr('disabled',true);
	}	
}

function modifyFrequencyOnLoad() {
	document.AddEditTemplate.OpenEnded[1].checked = true;
	if (document.AddEditTemplate.numberPayments.value=="999") {
		document.AddEditTemplate.OpenEnded[0].checked = true;
		document.AddEditTemplate.OpenEnded[1].checked = false;
		document.AddEditTemplate.numberPayments.value = "";
	}
	modifyFrequency();
	if (document.AddEditTemplate["frequency"].value == "None"){
		$("#Frequency").selectmenu('disable');
	} else {

	}
}

function modifyFrequency( ){
	if (document.AddEditTemplate["frequency"].value == "None"){
		document.AddEditTemplate.OpenEnded[0].disabled = true;
		document.AddEditTemplate.OpenEnded[1].disabled = true;
		document.AddEditTemplate.OpenEnded[0].checked = false;
		document.AddEditTemplate.OpenEnded[1].checked = true;
		document.AddEditTemplate.numberPayments.disabled = true;
		document.AddEditTemplate.numberPayments.value="2";
	} else {
		document.AddEditTemplate.OpenEnded[0].disabled = false;
		document.AddEditTemplate.OpenEnded[1].disabled = false;
		toggleRecurring();
	}
}
// --></script>

<%--Check if frequency is available or not. If available then only call Javascript function dealing with frequency related controls. --%>
<s:if test="#session.AddEditBillPayTemplate.FrequencyValue != 0">
<script type="text/javascript">
$(function(){
modifyFrequencyOnLoad();
toggleRecurring();
});
</script>
</s:if>
<s:form id="BillpayTempNew" namespace="/pages/jsp/billpay" validate="true" action="editBillPayTemplateAction_verify" method="post" name="AddEditTemplate" theme="simple">
<h1 class="portlet-title" style="display:none;" id="PageHeading"><s:text name="jsp.billpay_47"/></h1>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden value="%{editPayment.iD}" name="editPayment.iD"></s:hidden>
<input type="hidden" name="template" value="true"/>
<input type="hidden" name="select" value="true"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="loadPayee">
		<div class="leftPaneInnerWrapper">
			<div class="header"><h2 id="loadPayee"><s:text name="jsp.billpay_load_payee" /> *</h2></div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
				<s:select 
													list="editPayment.payee.Name" 
													listKey="editPayment.payee.ID" listValue="editPayment.payee.searchDisplayText"
													class="txtbox" id="PayeeSelect" 
													name="payeeID" 
													
													size="1" 
													onchange="loadBillpayProp(this.value)" 
													value="%{editPayment.payee.ID}">
												</s:select>
			</div>
		</div>
</div>
<div class="rightPaneWrapper w71">
<div class="paneWrapper" role="form" aria-labeledby="payeeDetail">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="payeeDetail">Payee Details</h2></div>
   		<div class="paneContentWrapper" id="payeeDetailsJsp">
   			<!-- payee table -->
            <jsp:include page="payeeDetailsForEditTemplate.jsp"></jsp:include>
   </div>
 </div>
</div>  
<div class="marginTop10"></div>
<div class="paneWrapper" role="form" aria-labeledby="paymentDetail">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="paymentDetail"><s:text name="jsp.billpay_77"/></h2></div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding10">
                  <tr>
				<td width="50%" class="sectionsubhead ltrow2_color"><label for="templateName"><s:text name="jsp.default_416"/></label><span class="required" title="required">*</span></td>
				<td width="50%" class="sectionsubhead ltrow2_color"><label for="AccountID"><s:text name="jsp.default_15"/></label><span class="required" title="required">*</span></td>
			</tr>
			<tr>
				<td>
					<input class="ui-widget-content ui-corner-all"  type="text" id="templateName" aria-required="true" name="editPayment.templateName" value="<s:property value="%{editPayment.templateName}"/>" size="40" maxlength="32">
                </td>
                <td>
					<s:select id="AccountID"
							list="editPayment.Account.accountDisplayText"
							listKey="editPayment.accountID" listValue="editPayment.Account.accountDisplayText"
							name="editPayment.accountID"
							aria-labelledby="AccountID"
					         aria-required="true"
							value="%{editPayment.accountID}"
							class="txtbox">
					</s:select> 
				</td>
            </tr>
            <tr>
            	<td><span id="Duplicate.TemplateNameError"></span></td>
            	<td><span id="accountIDError"></span></td>
            </tr>
            <tr>
				<td class="sectionsubhead ltrow2_color"><label for="Amount"><s:text name="jsp.default_43"/></label></td>
				<td class="sectionsubhead ltrow2_color"><label for="Memo"><s:text name="jsp.default_279"/></label></td>
			</tr>
			<tr>
				<td>
					<s:textfield name="editPayment.amount" id="Amount" size="12" maxlength="16" 
					value="%{editPayment.AmountValue.CurrencyStringNoSymbol}" cssClass="ui-widget-content ui-corner-all"/>
					<%-- <ffi:getProperty name="Payee" property="PayeeRoute.CurrencyCode"/> --%>
					<span id="payeeCurrencyCode"></span>
				</td>
				<td><s:textfield name="editPayment.memo" id="Memo" size="47" maxlength="100" value="%{editPayment.memo}" cssClass="ui-widget-content ui-corner-all"/></td>
            </tr>
            <tr>
				<td><span id="amountError"></span></td>
				<td></td>
            </tr>
            <s:if test='%{editPayment.frequencyValue > 0 }'>
		<tr>
			<td class="sectionsubhead ltrow2_color" colspan="2"><s:text name="jsp.default_351"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select id="frequencyId" list="frequencyList" name="editPayment.frequency" value="%{editPayment.frequency}"
				class="txtbox" >
				</s:select>
				<input type="radio" name="editPayment.openEnded" value="true" 
				<s:if test="%{editPayment.openEnded}">checked</s:if> onClick="toggleRecurring();">
				<span class="columndata"><s:text name="jsp.default_446"/></span>
				<input type="radio" name="editPayment.openEnded" value="false"
				<s:if test="%{!editPayment.openEnded}">checked</s:if> 
				onClick="toggleRecurring();">
					<span class="columndata"><s:text name="jsp.default_2"/></span>
					<input type="text" id="NumberPayments" name="editPayment.numberPayments" Class="ui-widget-content ui-corner-all" 
					<%-- value="<ffi:getProperty name="NumberPayments"/>" --%>
					value='<s:property value="%{editPayment.numberPayments}"/>' 
					maxlength="3"
					size="4" >
				
       		</td>
        </tr>                          
		<tr>
			<td colspan="2"><span id="numberPaymentsError"></span></td>
        </tr>
 </s:if>



<%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>


										<tr>
											<td colspan="2"><div align="center">
													<sj:a id="cancelTempFormButtonOnInput"
										                button="true" summaryDivId="summary" buttonType="cancel"
														onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
										        	><s:text name="jsp.default_82"/></sj:a>
													<sj:a
														id="verifyBillpaySubmit"
														formIds="BillpayTempNew"
						                                targets="verifyDiv"
						                                button="true"
						                                validate="true"
						                                validateFunction="customValidation"
						                                onBeforeTopics="beforeVerifyBillpayForm"
						                                onCompleteTopics="verifyBillpayFormComplete"
														onErrorTopics="errorVerifyBillpayForm"
						                                onSuccessTopics="successVerifyBillpayForm"
								                        ><s:text name="jsp.default_395" /></sj:a>
											</td>
										</tr>
				</table>
			</div>
		</div>
	</div>
</s:form>
