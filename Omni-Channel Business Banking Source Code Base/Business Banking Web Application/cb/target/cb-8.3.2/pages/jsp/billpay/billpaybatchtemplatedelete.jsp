<%@page import="com.ffusion.tasks.accounts.Task"%>
<%@ page import="com.ffusion.beans.billpay.PaymentDefines"%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpaymultitempview" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_41')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<div class="approvalDialogHt2">
<ffi:setProperty name='PageText' value=''/>
<s:form id="deleteBillpayBatchTempFormId" action="/pages/jsp/billpay/deleteBillPayBatchTemplateAction_deleteBatchTemplate.action" method="post" name="billpayDelete" >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden value="%{ID}" name="ID"></s:hidden>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Template Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/>:</span>
	            <span class="columndata">
	            	<s:property value="%{multiplePaymentView.TemplateName}"/>
	            </span>
				
			</div>
			<div class="inlineBlock">
				 <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_217"/>:</span>
	             <span class="columndata">
					<s:property value="%{multiplePaymentView.payments[0].account.accountDisplayText}"/>
	             </span>
			</div>
		</div>
		<div class="blockRow" id="verifyMultipleBillPay_total">
			<span id="esimatedTotalLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_431" />:&nbsp;</span> 
			<s:if test="%{isMultiCurrency}">
            	&#8776;
            </s:if>
			<span id="esimatedTotal">
						 <s:property value="%{total}" />
			</span> 
			<span id="estimatedTotalCurrency">
					<s:property	value="%{multiplePaymentView.payments[0].account.currencyCode}" />
			</span>
		</div>
	</div>
</div>
<s:set var="payeeCount" value="1"></s:set>
<s:iterator value="%{multiplePaymentView.payments}" status="listStatus" var="payment">
	<h3 class="transactionHeading"><%-- <s:text name="jsp.default_313"/> <s:property value="#payeeCount"/> --%>
		<span>
			<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
			<s:property value="#payment.payee.userAccountNumber"/>)
		</span>
	</h3>
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<%-- <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_313"/>:</span> --%>
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_43"/>:</span>
				<span  class="columndata" >
				<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
				<s:property value="#payment.AmountValue.CurrencyCode"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"   ><s:text name="jsp.default_279"/>:</span>
				<span>
					 <s:property value="#payment.memo"/>
				</span>
			</div>
		</div>
	</div>
	<s:set var="payeeCount" value="#payeeCount+1"></s:set>
</s:iterator>
</s:form>
<div class="ffivisible" style="height:150px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter">
     	<sj:a id="cancelDeleteMultiTempFormButton" 
             button="true" 
	onClickTopics="closeDialog"
     	><s:text name="jsp.default_82"/></sj:a>
     <sj:a id="deleteMultiTempLink" formIds="deleteBillpayBatchTempFormId" targets="resultmessage" button="true"   
		title="%{getText('Delete_Template')}" onCompleteTopics="cancelMultipleBillpayTemplateComplete" onSuccessTopics="cancelMultipleBillpayTemplateSuccessTopics" onErrorTopics="errorDeleteBillpay" 
		effectDuration="1500" ><s:text name="jsp.default_162" />
	</sj:a>
</div>
</div>
<%-- ================= MAIN CONTENT END ================= --%>


						</td>
						<td></td>
					</tr>
					
				</table>
				<br>
			</div>
		</div>
	
<%-- <script language="javascript">
var total = 0;
var total1 = 0;
var tempValue=0;
var tempValue1=0;
var frm = document.getElementById("viewBillpayMultiTempBatchFormId");
var length=0;
var length1=0;
if(frm.convertedAmount != null)
length =frm.convertedAmount.length;
if(frm.convertedAmount1 != null)
 length1=frm.convertedAmount1.length;

var cnt=0;
 
if(length == 0 ||isNaN(length)){

	  if(frm.convertedAmount != null){
		 tempValue= frm.convertedAmount.value;
		 if (tempValue != "" && !isNaN(tempValue))
		          {
		              total += parseFloat(tempValue);
		         }
	 
	  }
}
else{

	 for(cnt=0;cnt<length;cnt++){

		 tempValue= frm.convertedAmount[cnt].value;
	
		 
		 if (tempValue != "" && !isNaN(tempValue))
		         {
		             total += parseFloat(tempValue);
		        }

    }
}
/*Code for ESt total calculation if currency is same*/

if(length1 == 0 ||isNaN(length1)){
	  if(frm.convertedAmount1 != null)
	  {
	  
	  tempValue1= frm.convertedAmount1.value;
	 
	  if (tempValue1 != "" && !isNaN(tempValue1))
	           {
	               total1 += parseFloat(tempValue1);
	          }
	  
	   }
 
 }
 else{
 
	  for(cnt=0;cnt<length1;cnt++){
	  
		  tempValue1= frm.convertedAmount1[cnt].value;
		 
		  if (tempValue1 != "" && !isNaN(tempValue1))
		  {
             total1 += parseFloat(tempValue1);
         }	
	  
	    }
}


total=total+total1;
total = ns.common.formatTotal(total);
var checkMultiCurrency= frm['checkMultiCurrencyValue'].value;
var payeeCommonCurrency=frm['payeeCommonCurrency'].value;
var bankAccountCurrency=frm['bankAccountCurrency'].value;
 if(checkMultiCurrency =="true")
 {	
	 $('#viewBillpayMultiTempBatchFormId').find('#estimatedTotal2').text(total);
	 $('#viewBillpayMultiTempBatchFormId').find('#calculatedAmount').val(total);
 } else{
	 $('#viewBillpayMultiTempBatchFormId').find('#total').text(total);
	 $('#viewBillpayMultiTempBatchFormId').find('#calculatedAmount').val(total);
 } 
</script>
</ffi:cinclude>

<ffi:removeProperty name="SetPaymentTemplateID"/>  
<ffi:removeProperty name="PaymentType"/>    --%> 

