<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<div id="fileuploadPortlet">
<div class="portlet-header ui-widget-header ui-corner-all">
  	  <span class="portlet-title"><s:text name="jsp.default_211"/></span>
</div>
<div class="portlet-content">
    <div id="fileUploaderPanelID" align="left">
    	<div id="selectImportFormatOfFileUploadID" align="left">

		</div>
        <div id="selectFileFormAndStatusID" >
			<%-- <div id="fileUploadingStatusID" style="display:none;">loading...</div> --%>
			<%-- <div id="fileUploadingStatusID"><a href="javascript:void(0)" onclick="checkFileImportResults();">CheckStatus</a></div> --%>
			<div id="selectFileOFFileUploadFormID"></div>
        </div>
    </div>
</div>
<div id="transferDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
       		  <li><a href='#' onclick="ns.common.showDetailsHelp('fileuploadPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
</div>
    <!-- 
	<div id="custommappingPanelID" class="formPanel" align="right">
	        <s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp" escapeAmp="false">
				<s:param name="Section" value="%{'WIRE'}"/>
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
            </s:url>
            <sj:a
                id="customMappingsLink"
                href="%{CustomMappingsUrl}"
                targets="mappingDiv"
                title="Custom Mappings"
                indicator="indicator"
                button="true"
                buttonIcon="ui-icon-disk"
                onClickTopics="beforeCustomMappingsOnClickTopics" 
				onCompleteTopics="customMappingsOnCompleteTopics" 
				onErrorTopics="errorCustomMappingsOnErrorTopics"
            >
                  &nbsp;CUSTOM MAPPINGS
            </sj:a>      
	</div>
	 -->
</div>

    <script>
  //Initialize portlet with settings icon
	ns.common.initializePortlet("fileuploadPortlet"); 
	/* $('#fileuploadPortlet').portlet({
		title: js_fileupload_portlet_title,
		bookmark: true,
		generateDOM: true,
		helpCallback: function(){
			var helpFile = $('#fileuploadPortlet').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){ 
			var settingsArray = ns.shortcut.getShortcutSettingWithPortlet( '#fileuploadPortlet' );
			var path = settingsArray[0];
			var ent  = settingsArray[1];
			ns.shortcut.openShortcutWindow( path, ent );
		}	
	}); */
    </script>