<%-- <%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- 
<ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="session"/>
<ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
<ffi:process name="GetAllPaymentTemplates"/>

<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
	<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.PAYMENTTEMPLATES %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BILLPAY_TEMPLATE_INDEX %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
	<ffi:setProperty name="BuildIndex" property="BeanProperty" value="TemplateUniqueKey"/>
	<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/> --%>

<script language="JavaScript" type="text/javascript">

$(function(){
	$("#MultiBillpayTemplateList").lookupbox({
		"source":"/cb/pages/jsp/billpay/BillPayTemplatesLookupBoxAction_loadBatchTemplate.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"30",
		"module":"billpayMultiTemplates",
	});

});

		enableLoadButton();
		var selectedvalue="";
		function enableLoadButton()
		{
			selectedvalue = $('#MultiBillpayTemplateList').val();
			if ( selectedvalue == null || selectedvalue == "-1"){
				$('#loadMultiBillpayTemplateSubmit').hide();
			}
			else{
				$('#loadMultiBillpayTemplateSubmit').trigger('click');
				//$('#loadMultiBillpayTemplateSubmit').removeAttr("style");
			}
		}

		</script>

        <div align="center">
                <s:form id="LoadMultiBillpayTemplateForm" name="LoadMultiBillpayTemplateForm" method="post" action="/pages/jsp/billpay/addBillpayBatchAction_loadTemplateData.action" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<s:hidden value="true" name="isTemplate"></s:hidden>
           				<input type="hidden" name="Initialize" value="true"/>

                        <table width="100%" border="0" cellspacing="3" cellpadding="3" >
                        	<%-- <tr>
								<td class="sectionhead" align="left"><s:text name="jsp.billpay_60" /></td>
							</tr> --%>
							<tr>
                                <td align="center">
									<ffi:setProperty name="FirstOption" value="true"/>
									
									<div class="selectBoxHolder">
									<s:set var="type" value="%{multiplePayment.batchType}"></s:set>
									<s:if test='%{#type=="TEMPLATE"}'>
										<s:select id="MultiBillpayTemplateList" class="txtbox" name="ID" size="1" 
										list="multiplePayment.templateName" onChange="enableLoadButton()"
										listKey="multiplePayment.templateID" listValue="multiplePayment.templateName" >
										</s:select>
									</s:if>
									<s:else>
									<select class="txtbox" id="MultiBillpayTemplateList" name="ID" onChange="enableLoadButton()">
									</select>
									</s:else>
									</div>

									<ffi:setProperty name="PaymentTemplates" property="Filter" value="All"/>

                                </td>
                            </tr>
                        </table>
                </s:form>
                
                <sj:a
					id="loadMultiBillpayTemplateSubmit"
					targets="multipleBillpayForm"
					formIds="LoadMultiBillpayTemplateForm"
                    button="true"
                    cssStyle="display:none"
                    onBeforeTopics="beforeLoadMultiBillpayTemplate"
                    onSuccessTopics="loadMultiBillpayTemplateSuccess"
                    onErrorTopics="errorLoadMultiBillpayTemplate"
                    onCompleteTopics="loadMultiBillpayTemplateComplete"
                ><s:text name="jsp.default_263" />  </sj:a>

        </div>
