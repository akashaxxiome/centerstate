<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_95')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>
<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
<ffi:process name="GetLanguageList" />
<ffi:setProperty name="languageFlag" value=""/>
<ffi:list collection="GetLanguageList.LanguagesList" items="language1">
<ffi:cinclude value1="${language1.Language}" value2="en_US" operator="equals">
<ffi:setProperty name="languageFlag" value="true"/>
</ffi:cinclude>
</ffi:list>


<ffi:cinclude value1="${PaymentReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/payments_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0" hspace="3"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/payments/i_payees_on.gif" alt="" border="0" hspace="3"><a href="${SecurePath}payments/billpaytemplates.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_templates.gif" alt="" border="0" hspace="3"></a>'/>

 <ffi:setProperty name="BackURL" value="${SecurePath}payments/billpayaddpayee.jsp"/><ffi:setProperty name="company" value="company"/>
<ffi:author page="billpaypayee-common-notfound.jsp"/>

      <div align="center">
    
    <%-- ================ MAIN CONTENT START ================ --%>

<% String v138_4a = ""; String v138_2b = ""; 
   String employeeCountry = null;   
%>
   

    <form action="<ffi:urlEncrypt url="${SecurePath}payments/billpaypayee-company-Showall-wait.jsp"/>" method="post" name="formAddCompanyShowAllPayee">    
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    					
    			
    				
    										
    				<tr>
    				  <td></td>
    				  <td class="columndata ltrow2_color">
    			          
    				  <div align="center">
    				  <table width="720" border="0" cellspacing="0" cellpadding="3">
    				 <tr>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				</tr>
    				<tr>
    				<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt;<s:text name="jsp.billpay_94"/><br>
    				</span></td></tr>		
    				<tr>
    				<td class="instructions" colspan=6>
    				<s:text name="jsp.billpay_64"/>
    				<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="equals">
    				<ffi:cinclude value1="${languageFlag}" value2="true" operator="equals">
    				<s:text name="jsp.billpay_66"/>
    				</ffi:cinclude>
    				</ffi:cinclude>
    				</td>
    				</tr>
    				
    			        <tr>
    				  
    				 </tr>
    				 </table>
  			            </div>
    			           
    			            </td>
    			            <td></td>
    				</tr>
    					   
    					   
					   </table>
					   
    <br><br>
    <center>
        
		<sj:a 
               button="true" 
			onClickTopics="closeDialog"
       ><s:text name="jsp.default_102"/></sj:a>
		<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="equals">
		 <ffi:cinclude value1="${languageFlag}" value2="true" operator="equals">
		       <input type="submit" name="SHOW ALL" value="<s:text name="jsp.billpay_101"/>" class="submitbutton">
        </ffi:cinclude>
        </ffi:cinclude>
    </center>
    
    </FORM>
</div>

<ffi:setProperty name="refresh" value="false"/>

	
