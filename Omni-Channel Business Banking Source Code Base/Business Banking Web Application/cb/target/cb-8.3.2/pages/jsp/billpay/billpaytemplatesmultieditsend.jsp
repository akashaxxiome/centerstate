<%-- <%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.billpay.PaymentDefines"%> --%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaytemplatesmultieditsend" className="moduleHelpClass"/>
 <s:set var="tmpI18nStr" value="%{getText('jsp.billpay_12')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<%-- <ffi:object id="GetPaymentBatch" name="com.ffusion.tasks.billpay.GetPaymentBatch" scope="request"/>
   <ffi:setProperty name="GetPaymentBatch" property="ID" value="${EditPaymentBatch.TemplateID}"/>
   <ffi:process name="GetPaymentBatch"/>
   <ffi:removeProperty name="GetPaymentBatch"/> --%>
<form action="<ffi:urlEncrypt url="${SecurePath}payments/billpay.jsp?Refresh=true"/>" method="POST" name="frmPayBill">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>  
 <div class="leftPaneWrapper" role="form" aria-labeledby="summary">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
		<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 5px;">
			<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="multipleBillPayTemplateVerify_templateName"><s:text name="jsp.billpay_79"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePaymentView.TemplateName}"/>
			</div>
			<div style="width:100%" class="marginTop10 floatleft">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="multipleBillPayTemplateVerify_fromAccount"><s:text name="jsp.default_217"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePaymentView.payments[0].account.accountDisplayText}"/></span>
			</div>
		</div>
	</div>
	<div class="leftPaneInnerWrapper totalAmountWrapper" id="multipleBillPayTemplateVerify_total">
	                <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
	                <%-- <ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals"> </ffi:cinclude> --%>
	                <s:if test="%{isMultiCurrency}">
	                	&#8776;
	                </s:if>
	                 <span id="esimatedTotal">
	                 	<s:property value="%{total}"/>
	                 </span> 
	                 <span id="estimatedTotalCurrency">
	                 <%-- <ffi:getProperty name="selectedCurrencyCode"/> --%>
	                 <s:property value="%{multiplePaymentView.payments[0].account.currencyCode}"/>
	                 </span>
	</div>
</div>  
<div class="confirmPageDetails">
<div class="blockRow completed">
			<span id="billpayResultMessage"><span class="sapUiIconCls icon-accept "></span> <s:text name="jsp.billpay_122"/></span>
</div>
<ffi:setProperty name="PaymentBatch" property="Payments.Filter" value="All" />
<s:set var="payeeCount" value="1"></s:set>
	<s:iterator value="%{multiplePaymentView.payments}" status="listStatus" var="payment">
		<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
			<h3 class="transactionHeading"><%-- <s:text name="jsp.default_313"/> <s:property value="#payeeCount"/> --%>
				<span id="multipleBillPayTemplateVerify_payeeNickNameValue[<s:property value="%{#listStatus.index}"/>]" class="">
					<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
					<s:property value="#payment.payee.userAccountNumber"/>)
				</span>
			</h3>
			<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
			<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<%-- <span id="multipleBillPayTemplateVerify_payeeNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_313"/>:</span> --%>
					<span id="multipleBillPayTemplateVerify_amountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
					<span id="multipleBillPayTemplateVerify_amountValue[<s:property value="%{#listStatus.index}"/>]" class="columndata">
						<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
						<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="multipleBillPayTemplateVerify_memoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
					<span id="multipleBillPayTemplateVerify_memoValue[<s:property value="%{#listStatus.index}"/>]" class="columndata">
	                	<s:property value="#payment.memo"/>
	                </span>
				</div>
			</div>
 		</div>
 		<s:set var="payeeCount" value="#payeeCount+1"></s:set>
		</s:if>
	</s:iterator>
	

<ffi:setProperty name="BillPayAccounts" property="Filter" value="All" />
<div class="btn-row">
    
	<sj:a id="cancelMultiBillpaySendForm" 
             button="true" 
             summaryDivId="summary" buttonType="done"
			onClickTopics="showSummary,showMultiTempSummary"
     	><s:text name="jsp.default_175"/></sj:a>
</div></div>
</form>

<%-- ================= MAIN CONTENT END ================= --%>


						</td>
						<td></td>
					</tr>
				</table>
				<br>
			</div>
		</div>
<%-- <script language="javascript">
 var total = 0;
 var total1 = 0;
 var tempValue=0;
 var tempValue1=0;
 var frm = document.getElementById("MultiBillpayNew");
 var length=0;
 var length1=0;
 if(frm.convertedAmount != null)
 
 length =frm.convertedAmount.length;
 if(frm.convertedAmount1 != null)
  length1=frm.convertedAmount1.length;
 
 var cnt=0;
  
 if(length == 0 ||isNaN(length)){
 
  if(frm.convertedAmount != null){
 tempValue= frm.convertedAmount.value;

 if (tempValue != "" && !isNaN(tempValue))
          {
              total += parseFloat(tempValue);
         }
 
  }
 }
 else{
 
 for(cnt=0;cnt<length;cnt++){
 
 tempValue= frm.convertedAmount[cnt].value;

 
 if (tempValue != "" && !isNaN(tempValue))
         {
             total += parseFloat(tempValue);
        }
 
     }
 }
 /*Code for ESt total calculation if currency is same*/
 
 if(length1 == 0 ||isNaN(length1)){
  if(frm.convertedAmount1 != null)
  {
  
  tempValue1= frm.convertedAmount1.value;
 
  if (tempValue1 != "" && !isNaN(tempValue1))
           {
               total1 += parseFloat(tempValue1);
          }
  
   }
  
  }
  else{
  
  for(cnt=0;cnt<length1;cnt++){
  
  tempValue1= frm.convertedAmount1[cnt].value;
 
  if (tempValue1 != "" && !isNaN(tempValue1))
          {
              total1 += parseFloat(tempValue1);
         }
  
      }
 }
 
 
 total=total+total1;
//Common function which will format the total accordingly
 total = ns.common.formatTotal(total);
 var checkMultiCurrency= document.getElementById("checkMultiCurrencyValue").value;
 var payeeCommonCurrency=document.getElementById("payeeCommonCurrency").value;
 var bankAccountCurrency=document.getElementById("bankAccountCurrency").value;
  if(checkMultiCurrency =="true")
{	

   document.getElementById("esimatedTotalSend").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
   
}
 if(checkMultiCurrency == "false"){
 if(payeeCommonCurrency == "" || payeeCommonCurrency != bankAccountCurrency){
 	
   document.getElementById("esimatedTotalSend").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
 }
 else{	
 
  document.getElementById("esimatedTotalSend").innerHTML ="";
  document.getElementById("esimatedTotalLabel").innerHTML ="";
  document.getElementById("esimatedTotalCurrency").innerHTML ="";
  }
 }
</script>
<ffi:cinclude value1="${isMultiCurrency}" value2="" operator="notEquals">
<ffi:removeProperty name="isMultiCurrency" />
</ffi:cinclude> --%>