<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.billpay.PaymentDefines"%>
<%@ page import="com.ffusion.beans.billpay.PaymentStatus" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaymentmultipleconfirmsend" className="moduleHelpClass"/>
 <%-- <s:set var="tmpI18nStr" value="%{getText('jsp.billpay_12')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>



		<div align="center">
			<div align="left">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">



<ffi:setProperty name="BackURL" value="${SecurePath}payments/billpaynewmulti.jsp"/>

<ffi:object id="AccountTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
<ffi:object id="CurrencyCodeTable" name="com.ffusion.beans.util.StringTable" scope="session"/>

<ffi:list collection="BillPayAccounts" items="BAccount1">
   <ffi:setProperty name="AccountTable" property="Key" value="${BAccount1.ConsumerDisplayText}" />
   <ffi:setProperty name="AccountTable" property="Value" value="${BAccount1.CurrentBalance.Amount}" />

   <ffi:setProperty name="CurrencyCodeTable" property="Key" value="${BAccount1.ConsumerDisplayText}" />
   <ffi:setProperty name="CurrencyCodeTable" property="Value" value="${BAccount1.CurrencyCode}" />
</ffi:list>
 --%>
<div style="display:none">
			<span id="billpayResultMessage"><s:text name="jsp.billpay_121"/></span>
</div>
<form action="<ffi:urlEncrypt url="${SecurePath}payments/billpay.jsp?Refresh=true"/>" method="POST" name="frmPayBill" id="MultiBillpayNewSend">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.billpay_bill_pay_summary" /></div>
		<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 10px;">
			<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="confirmMultipleBillPay_fromAccount"><s:text name="jsp.default_217"/>:</span>
					<span class="inlineSection floatleft labelValue"><ffi:setProperty name="BillPayAccounts" property="Filter" value="ID=${AddPaymentBatch.AccountID}" />
					<s:property value="%{multiplePaymentView.payments[0].account.accountDisplayText}"/></span>
				</div>
		</div>
	</div>
	</form>
	<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.default_371"/></div>
		<div id="templateForMultipleBillpayVerify" class="paneContentWrapper">
		       <s:include value="billpaymultitempname.jsp" />
		       <div id="templateForMultipleTransferVerifyResult"></div>
		</div>
	</div>
	<div class="leftPaneInnerWrapper totalAmountWrapper" id="confirmMultipleBillPay_total">
                <%-- <ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals"> </ffi:cinclude> --%>
                 <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
                <s:if test="%{isMultiCurrency}">
                	&#8776;
                </s:if>
                 <span id="esimatedTotalSend">
                 	<s:property value="%{total}"/>
                 </span> 
                 <span id="estimatedTotalCurrency">
                 <%-- <ffi:getProperty name="selectedCurrencyCode"/> --%>
                 <s:property value="%{multiplePaymentView.payments[0].account.currencyCode}"/>
                 </span>
	</div>
</div>
<div class="confirmPageDetails">	
<%-- <table width="100%" border="0" cellspacing="0" cellpadding="3" valign="top">
	<tr>
        <td colspan="2" class="sectionhead tbrd_b ltrow2_color">&gt;<s:text name="jsp.billpay_77"/></td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
	<tr>
        <td width="20"></td>
        <td id="confirmMultipleBillPay_fromAccount">
            <span class="sectionsubhead"><s:text name="jsp.default_217"/>&nbsp;&nbsp;</span>
            <span class="columndata">
            <ffi:setProperty name="BillPayAccounts" property="Filter" value="ID=${AddPaymentBatch.AccountID}" />
			<ffi:list collection="BillPayAccounts" items="BAccount" startIndex="1" endIndex="1"  >
				<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
				<s:set var="AccountDisplayTextTaskBean" value="#attr.BAccount" scope="request" />
				<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY%>"/>
				<ffi:process name="AccountDisplayTextTask"/>
                <ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
                <ffi:setProperty name="selectedCurrencyCode" value="${BAccount.CurrencyCode}"/>
			</ffi:list>
			<s:property value="%{multiplePaymentView.payments[0].account.accountDisplayText}"/>
            </span>
        </td>
	</tr>
</table> --%>
<s:iterator value="%{multiplePaymentView.payments}" status="listStatus" var="payment">
<div class="blockWrapper">
	<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
	<h3 class="transactionHeading">
		<span id="confirmMultipleBillPay_payee[<s:property value="%{#listStatus.index}"/>]" class="columndata" >
			<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
			<s:property value="#payment.payee.userAccountNumber"/>)
		</span>
	</h3>
	<div  class="blockHead"><s:text name="jsp.default_388" /> :</div>
	<div class="blockContent">
		<s:set var="PMS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_TO_PAY==#payment.status}" />
					<s:set var="PMS_INSUFFICIENT_FUNDS" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_INSUFFICIENT_FUNDS==#payment.status}" />
					<s:set var="PMS_BPW_LIMITCHECK_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITCHECK_FAILED==#payment.status}" />
					<s:set var="PMS_BPW_APPROVAL_NOT_ALLOWED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_NOT_ALLOWED==#payment.status}" />
					<s:set var="PMS_SCHEDULED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED==#payment.status}" />
					<s:set var="PMS_FUNDS_ALLOCATED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED==#payment.status}" />
					<s:set var="PMS_PENDING_APPROVAL" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL==#payment.status}" />
					<s:set var="PMS_UNKNOWN" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_UNKNOWN==#payment.status}" />
					<s:set var="PMS_BPW_APPROVAL_NOT_ALLOWED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_NOT_ALLOWED==#payment.status}" />
					<s:set var="PMS_BPW_FUNDSREVERTED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_FUNDSREVERTED==#payment.status}" />
					<s:set var="PMS_BPW_FUNDSREVERTED_NOTIF" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_FUNDSREVERTED_NOTIF==#payment.status}" />
					<s:set var="PMS_BPW_APPROVAL_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_APPROVAL_FAILED==#payment.status}" />
					<s:set var="PMS_BPW_LIMITREVERT_FAILED" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITREVERT_FAILED==#payment.status}" />
					<s:set var="PMS_FUNDS_FAILEDON" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_FAILEDON==payment.status}" />
					
					<s:if test="%{#PMS_BPW_LIMITCHECK_FAILED || #PMS_BPW_APPROVAL_NOT_ALLOWED || #PMS_INSUFFICIENT_FUNDS || #PMS_FAILED_TO_TRANSFER || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_LIMITREVERT_FAILED || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_FUNDSREVERTED_NOTIF || #PMS_BPW_FUNDSREVERTED || #PMS_FUNDS_FAILEDON}">
						<s:set var="HighlightStatus" value="true" />
					</s:if>
					<s:set var="PMS_PAID" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_PAID==#payment.status}" /> 
					<s:if test="%{#HighlightStatus==true}">
						<div class="blockRow failed">
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-decline"></span> <!-- failed mean give red here -->
									<s:if test="%{payment.backendErrorDesc=='' || payment.backendErrorDesc==null }">
								<s:property	value="%{payment.statusName}" />
								</s:if> 
								<s:else>
								<s:property	value="%{payment.statusName}" /> - <s:property value="%{payment.backendErrorDesc}"/>
								</s:else>
							</span></span>
							<span style="float:right">
								<s:if test="%{#PMS_INSUFFICIENT_FUNDS}">
									<s:text name="jsp.billpay_137" />
								</s:if>
								<s:elseif test="%{#PMS_BPW_LIMITCHECK_FAILED}">
									<s:text name="jsp.billpay_139" />
								</s:elseif>
								<s:elseif test="%{#PMS_FUNDS_FAILEDON}">
									<s:text name="jsp.billpay_138" />
								</s:elseif>
								<s:elseif test="%{#PMS_FAILED_TO_TRANSFER || #PMS_BPW_APPROVAL_NOT_ALLOWED || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_LIMITREVERT_FAILED || #PMS_BPW_APPROVAL_FAILED || #PMS_BPW_FUNDSREVERTED_NOTIF || #PMS_BPW_FUNDSREVERTED}">
									<s:text name="jsp.billpay_136" />
								</s:elseif>
							</span>
						</div>
					</s:if> 
					<s:elseif test="%{#PMS_SCHEDULED || #PMS_FUNDS_ALLOCATED}">
					<div class="blockRow pending">
						<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
			            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-future"></span><!-- this is scheduled/pending giv orange -->
									<s:property value="%{#payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_TRANSFERED}">
						<div class="blockRow completed">
						 	<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
				            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> 
								<s:property value="%{#payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_PENDING_APPROVAL}">
						<div class="blockRow pending">
							<span id="confirmBillPayPayeeStatusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
				            <span id="confirmBillPayPayeeStatusValue" class="columndata">
							<%-- Display Transfer Status --%>
							<%-- <span id="confirmTransfer_statusLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.account_191" /></span> --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-pending"></span> <!-- this is pending approval give color orange icon different -->
								<s:property value="%{#payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
					<s:elseif test="%{#PMS_PAID}">
						<div class="blockRow completed">
						 	<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> 
								<s:property value="%{#payment.statusName}"/>
							</span></span>
							<span class="floatRight"><s:text name="jsp.billpay_125"/></span>
						</div>
					</s:elseif>
	</div>
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<%-- <span id="confirmMultipleBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_313"/>:</span> --%>
				<span id="confirmMultipleBillPay_amountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span id="confirmMultipleBillPay_amount[<s:property value="%{#listStatus.index}"/>]" class="columndata" sectionsubhead sectionLabel sectionsubhead sectionLabel>
					<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
					<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmMultipleBillPay_sendDateLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_99"/>:</span>
				<span id="confirmMultipleBillPay_sendDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <s:property value="#payment.PayDate"/>
                </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmMultipleBillPay_deliverByDateLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_45"/>:</span>
				<span id="confirmMultipleBillPay_deliverByDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
               	 <s:property value="#payment.deliverByDate"/>	
                </span>
			</div>
			<div class="inlineBlock">
				<span id="confirmMultipleBillPay_memoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span id="confirmMultipleBillPay_memo[<s:property value="%{#listStatus.index}"/>]" class="columndata">
	                <s:property value="#payment.memo"/>	
	            </span>
			</div>
		</div>
	</div>
	</s:if>
</div>
</s:iterator>
<%-- <table border="0" cellspacing="0" cellpadding="3" width="100%">
    <tr class="header">
		<td id="confirmMultipleBillPay_payeeNameLabel" class="sectionsubhead" width="20%"><s:text name="jsp.default_313"/></td>
		<td id="confirmMultipleBillPay_amountLabel" class="sectionsubhead" width="15%" align="right"><s:text name="jsp.default_43"/>&nbsp;&nbsp;</td>


        <td id="confirmMultipleBillPay_sendDateLabel" class="sectionsubhead" width="10%"><s:text name="jsp.billpay_99"/>&nbsp;&nbsp;</td>
        <td id="confirmMultipleBillPay_deliverByDateLabel" class="sectionsubhead" width="10%"><s:text name="jsp.billpay_45"/>&nbsp;&nbsp;</td>

        <td id="confirmMultipleBillPay_memoLabel" class="sectionsubhead" width="20%" nowrap><s:text name="jsp.default_279"/></td>

        <td id="confirmMultipleBillPay_statusLabel" class="sectionsubhead" width="10%"><s:text name="jsp.default_388"/></td>

        <td id="confirmMultipleBillPay_registerCategoryLabel" class="sectionsubhead" width="15%"><ffi:cinclude value1="${RegisterEnabled}" value2="true"><s:text name="jsp.billpay_89"/>&nbsp;&nbsp;</ffi:cinclude></td>
	</tr>
	<% int idCount=0; %>
	<ffi:setProperty name="AddPaymentBatch" property="Payments.Filter" value="All" />
	<ffi:list collection="AddPaymentBatch.Payments" items="Payment1">
		<ffi:setProperty name="AddPaymentBatch" property="CurrentPayment" value="${Payment1.ID}" />
		<ffi:cinclude value1="${Payment1.AmountValue.IsZero}" value2="false" operator="equals">
		<s:iterator value="%{multiplePaymentView.payments}" status="listStatus" var="payment">
			<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
			<tr>
				<td id="confirmMultipleBillPay_payee[<s:property value="%{#listStatus.index}"/>]" class="columndata" height="20">
					<ffi:cinclude value1="" value2="${Payment1.PayeeName}" operator="notEquals" ><ffi:getProperty name="Payment1" property="PayeeName"/></ffi:cinclude>
					<ffi:cinclude value1="" value2="${Payment1.PayeeNickName}" operator="notEquals" > (<ffi:getProperty name="Payment1" property="PayeeNickName"/></ffi:cinclude><ffi:cinclude value1="" value2="${Payment1.Payee.UserAccountNumber}" operator="notEquals" > - <ffi:getProperty name="Payment1" property="Payee.UserAccountNumber"/></ffi:cinclude>)
					<br>
					<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
						<s:property value="#payment.payee.userAccountNumber"/>)
				</td>
				<td id="confirmMultipleBillPay_amount[<s:property value="%{#listStatus.index}"/>]" class="columndata" nowrap align="right">

				<ffi:getProperty name="Payment1" property="AmountValue.CurrencyStringNoSymbol"/>

				<ffi:getProperty name="Payment1" property="Payee.PayeeRoute.CurrencyCode"/>
				<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
				<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/>

				<ffi:setProperty name="Payment1" property="AmountValue.CurrencyCode" value="${Payment1.Payee.PayeeRoute.CurrencyCode}"/>
				<ffi:setProperty name="Payment1" property="AmtCurrency" value="${Payment1.AmountValue.CurrencyCode}"/>


			<ffi:cinclude value1="${Payment1.AmountValue.CurrencyCode}" value2="${selectedCurrencyCode}" operator="notEquals" >
			<ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
			<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${Payment1.AmountForBPW}" />
			<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${Payment1.AmountValue.CurrencyCode}" />
			<ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${selectedCurrencyCode}"/>
			<ffi:process name="ConvertToTargetCurrency"/>
			<ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>

  		 	<input type="hidden" name="convertedAmount" id="convertedAmount" value="<ffi:getProperty name="CONVERTED_CURRENCY_AMOUNT"/>">
			</ffi:cinclude> 

			<ffi:cinclude value1="${Payment1.AmountValue.CurrencyCode}" value2="${selectedCurrencyCode}" operator="equals" >

			<input type="hidden" name="convertedAmount1" id="convertedAmount1" value="<ffi:getProperty name="Payment1" property="AmountForBPW"/>">
			</ffi:cinclude>

				</td>

                <ffi:cinclude value1="${PaymentTemplateFlag}" value2="true" operator="notEquals">
                <td id="confirmMultipleBillPay_sendDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <ffi:getProperty name="Payment1" property="PayDate"/>
                <s:property value="#payment.PayDate"/>
                </td>
                <td id="confirmMultipleBillPay_deliverByDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <ffi:getProperty name="Payment1" property="DeliverByDate"/>
                <s:property value="#payment.deliverByDate"/>	
                </td>
                </ffi:cinclude>
                <td id="confirmMultipleBillPay_memo[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <ffi:getProperty name="Payment1" property="Memo"/>
                <s:property value="#payment.memo"/>	
                </td>

                <td id="confirmMultipleBillPay_status[<s:property value="%{#listStatus.index}"/>]" class="columndata">
	                <s:if test="%{#payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_BATCH_INPROCESS}">
	                	<s:text name="jsp.default_237"/>
	                </s:if>
	               	<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED}">
						<s:text name="jsp.default_237"/>
					</s:elseif>
					<s:elseif test="%{payment.Status == @com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED}">
						<s:text name="jsp.default_530"/>
					</s:elseif>
					<s:else>
						<s:property value="#payment.statusName"/>
					</s:else>
                	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
						<ffi:getProperty name="Payment1" property="StatusName" />
					</ffi:cinclude>
					<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
						<ffi:cinclude value1="${Payment1.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_BATCH_INPROCESS) %>">
							<s:text name="jsp.default_237"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${Payment1.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>">
							<s:text name="jsp.default_237"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${Payment1.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_SCHEDULED) %>">
							<s:text name="jsp.default_530"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${Payment1.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_BATCH_INPROCESS) %>" operator="notEquals">
							<ffi:cinclude value1="${Payment1.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>" operator="notEquals">
								<ffi:cinclude value1="${Payment1.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_SCHEDULED) %>" operator="notEquals">
									<ffi:getProperty name="Payment1" property="StatusName" />
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
					
                </td>

                <td id="confirmMultipleBillPay_registerCategory[<s:property value="%{#listStatus.index}"/>]" class="columndata">
				   <!-- ====================== BEGIN REGISTER INTEGRATION ===================== -->
					<ffi:cinclude value1="${RegisterEnabled}" value2="true" operator="equals">
						<ffi:setProperty name="RegisterCategories" property="Current" value="${Payment1.REGISTER_CATEGORY_ID}"/>
						<ffi:setProperty name="Parentfalse" value=""/>
						<ffi:setProperty name="Parenttrue" value=": "/>
						<ffi:getProperty name="RegisterCategories" property="ParentName"/><ffi:getProperty name="Parent${RegisterCategories.HasParent}"/><ffi:getProperty name="RegisterCategories" property="Name"/>
						<br>
					</ffi:cinclude>
					<!-- ====================== END REGISTER INTEGRATION ===================== -->
				</td>
				<% idCount++; %>
			</tr>
			</s:if>
			</s:iterator>
		</ffi:cinclude>
	</ffi:list>
	<tr>
		<td class="columndata">&nbsp;</td>
	</tr>
	<tr>
		<input type="hidden" name="checkMultiCurrencyValue" id="checkMultiCurrencyValue" value="<ffi:getProperty name="checkMultiCurrency"/>">
		<input type="hidden" name="payeeCommonCurrency" id="payeeCommonCurrency" value="<ffi:getProperty name="PayeeCommonCurrency"/>">
		<input type="hidden" name="bankAccountCurrency" id="bankAccountCurrency" value="<ffi:getProperty name="selectedCurrencyCode"/>">
		<ffi:setProperty name="bankAccountCurrency" value="${selectedCurrencyCode}"/>
		<ffi:cinclude value1="${checkMultiCurrency}" value2="false" operator="equals">
		  <ffi:cinclude value1="${PayeeCommonCurrency}" value2="${bankAccountCurrency}" operator="equals">
            <td class="sectionsubhead" align="right" colspan="2">
                <s:text name="jsp.default_431"/>:&nbsp;
                <span id="Total"><ffi:getProperty name="MultiplePaymentTotal"/></span> <ffi:getProperty name="PayeeCommonCurrency"/>
            </td>
          </ffi:cinclude>
	    </ffi:cinclude>
        <input type="hidden" name="calculatedAmount" id="calculatedAmount">
        <ffi:cinclude value1="${checkMultiCurrency}" value2="true" operator="equals">
        <ffi:setProperty name="DisplayEstimatedAmountKey" value="true"/>
        <tr>
            <td id="confirmMultipleBillPay_total" class="sectionsubhead" align="right" colspan="2">
                <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
                 <ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals"> &#8776;</ffi:cinclude> 
                 <span id="esimatedTotalSend"></span> <span id="estimatedTotalCurrency"><ffi:getProperty name="selectedCurrencyCode"/></span>
            </td>
        </tr>
        </ffi:cinclude>
        <td id="confirmMultipleBillPay_total" class="sectionsubhead" align="right" colspan="2">
                <ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals"> </ffi:cinclude>
                 <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
                <s:if test="%{isMultiCurrency}">
                	&#8776;
                </s:if>
                 <span id="esimatedTotalSend">
                 	<s:property value="%{total}"/>
                 </span> 
                 <span id="estimatedTotalCurrency">
                 <ffi:getProperty name="selectedCurrencyCode"/>
                 <s:property value="%{multiplePaymentView.payments[0].account.currencyCode}"/>
                 </span>
            </td>
        </tr>
</table> --%>


<ffi:setProperty name="BillPayAccounts" property="Filter" value="All" />
<div align="center">
    <ffi:cinclude value1="${DisplayEstimatedAmountKey}" value2="true">
        <span class="required"><ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals">&#8776; <s:text name="jsp.default_241"/></ffi:cinclude></span><br><br>
        <ffi:removeProperty name="DisplayEstimatedAmountKey"/>
    </ffi:cinclude>
</div>
<div class="btn-row">
<sj:a id="cancelMultiBillpaySendForm"
             button="true"
             summaryDivId="summary" buttonType="done"
			onClickTopics="showSummary,cancelBillpayForm,resetBillpayConfirmDiv"
     	><s:text name="jsp.default_175"/></sj:a>
</div>
</div>
<%-- ================= MAIN CONTENT END ================= --%>


<%-- <script language="javascript">
 var total = 0;
 var total1 = 0;
 var tempValue=0;
 var tempValue1=0;
 var frm = document.getElementById("MultiBillpayNewSend");
 var length=0;
 var length1=0;
 if(frm.convertedAmount != null)

 length =frm.convertedAmount.length;
 if(frm.convertedAmount1 != null)
  length1=frm.convertedAmount1.length;

 var cnt=0;

 if(length == 0 ||isNaN(length)){

  if(frm.convertedAmount != null){
 tempValue= frm.convertedAmount.value;

 if (tempValue != "" && !isNaN(tempValue))
          {
              total += parseFloat(tempValue);
         }

  }
 }
 else{

 for(cnt=0;cnt<length;cnt++){

 tempValue= frm.convertedAmount[cnt].value;


 if (tempValue != "" && !isNaN(tempValue))
         {
             total += parseFloat(tempValue);
        }

     }
 }
 /*Code for ESt total calculation if currency is same*/

 if(length1 == 0 ||isNaN(length1)){
  if(frm.convertedAmount1 != null)
  {

  tempValue1= frm.convertedAmount1.value;

  if (tempValue1 != "" && !isNaN(tempValue1))
           {
               total1 += parseFloat(tempValue1);
          }

   }

  }
  else{

  for(cnt=0;cnt<length1;cnt++){

  tempValue1= frm.convertedAmount1[cnt].value;

  if (tempValue1 != "" && !isNaN(tempValue1))
          {
              total1 += parseFloat(tempValue1);
         }

      }
 }


 total=total+total1;
 //Common function which will format the total accordingly
 total = ns.common.formatTotal(total);
 var checkMultiCurrency= document.getElementById("checkMultiCurrencyValue").value;
 var payeeCommonCurrency=document.getElementById("payeeCommonCurrency").value;
 var bankAccountCurrency=document.getElementById("bankAccountCurrency").value;
  if(checkMultiCurrency =="true")
{

   document.getElementById("esimatedTotalSend").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;

}
 if(checkMultiCurrency == "false"){
 if(payeeCommonCurrency == "" || payeeCommonCurrency != bankAccountCurrency){

   document.getElementById("esimatedTotalSend").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
 }
 else{

  document.getElementById("esimatedTotalSend").innerHTML ="";
  document.getElementById("esimatedTotalLabel").innerHTML ="";
  document.getElementById("esimatedTotalCurrency").innerHTML ="";
  }
 }
</script>
<ffi:cinclude value1="${isMultiCurrency}" value2="" operator="notEquals">
<ffi:removeProperty name="isMultiCurrency" />
</ffi:cinclude>
 --%>