<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<ffi:help id="payments_billpaypayeeeditconfirm" className="moduleHelpClass"/>
<s:form id="BillpayPayeeEditSend" namespace="/pages/jsp/billpay" validate="false" action="editBillpayPayeeAction_execute" method="post" name="BillpayPayeeEditSend" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper label130">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay.payee_summary" /></h2></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyPayeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"  style="width:50%"><s:property value="%{name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyPayeeContactLabel"><s:text name="jsp.default_19"/>:</span>
				<span class="inlineSection floatleft labelValue" style="width:50%"><s:property value="%{userAccountNumber}"/></span>
			</div>
	</div>
</div>
</div>
<div class="confirmPageDetails label130">
<%-- <div id="editVerifyPayeeInfoMessage" align="center"><s:text name="jsp.billpay_113"/></div> --%>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.info"/></div>
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
					<span id="editVerifyPayeeNickNameValue" class="columndata" >
						<s:property value="%{nickName}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editVerifyPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
					<span id="editVerifyPayeeContactValue" class="columndata" >
						<s:property value="%{contactName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_37"/>:</span>
					<span id="editVerifyPayeePhoneValue" class="columndata" >
						<s:property value="%{phone}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editVerifyPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
					<span id="editVerifyPayeeAddress1Value" class="columndata" >
						<s:property value="%{street}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
					<span id="editVerifyPayeeAddress2Value" class="columndata" >
						<s:property value="%{street2}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editVerifyPayeeAddress3Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
					<span id="editVerifyPayeeAddress3Value" class="columndata" >
						<s:property value="%{street3}"/>
					</span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
					<span id="editVerifyPayeeCityValue" class="columndata" >
						<s:property value="%{city}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editVerifyPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
					<span id="editVerifyPayeeStateValue" class="columndata" >
						<s:property value="%{stateDisplayName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeeZipLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
					<span id="editVerifyPayeeZipValue" class="columndata" >
						<s:property value="%{zipCode}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editVerifyPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
					<span id="editVerifyPayeeCountryValue" class="columndata" >
						<s:property value="%{countryDisplayName}"/>
					</span>
				</div>
			</div>
 		</div>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.bank.info"/></div>
   		<div class="paneContentWrapper">
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeeRountingNoLabel" class="sectionsubhead sectionLabel">
						<s:property value="%{bankIdentifierDisplayText}"/>:
						<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden></span>
					<span id="editVerifyPayeeRountingNoValue" class="columndata" ><s:property value="%{payeeRoute.bankIdentifier}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="editVerifyPayeeBankAccountNoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_26"/>:</span>
					<span id="editVerifyPayeeBankAccountNoValue" class="columndata" >
						<s:property value="%{payeeRoute.accountID}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editVerifyPayeeBankAccountTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>
					<span id="editVerifyPayeeBankAccountTypeValue" class="columndata" >
					<s:if test="%{addPayee.isGlobal}">
						<s:property value="%{payeeRoute.acctType}"/>
					</s:if>
					<s:else>
						<s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/>
					</s:else>
					</span>
				</div>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
					<div class="inlineBlock">
						<span id="editVerifyPayeeBankAccountCurrencyLabel" class="sectionsubhead sectionLabel">
								<s:text name="jsp.billpay_25"/>:
						</span>
						<span id="editVerifyPayeeBankAccountCurrencyValue" class="columndata" >
							<s:property value="%{payeeRoute.currencyCode}"/>
						</span>
					</div>
				</ffi:cinclude>
			</div>
 		</div>
   </div>

				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_52"/><br>
												</span></td>
										</tr>
											<tr>
												<td id="editVerifyPayeeInfoMessage" class="columndata ltrow2_color" colspan="6">
													<div align="center">
														<span class="sectionsubhead"><s:text name="jsp.billpay_113"/><br>
															<br>
														</span></div>
												</td>
											</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td id="editVerifyPayeeNameLabel" class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="editVerifyPayeeNameValue" class="columndata" colspan="2">
											<s:property value="%{name}"/>
											</td>
											<td id="editVerifyPayeeAddress1Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="editVerifyPayeeAddress1Value" class="columndata" colspan="2">
											<s:property value="%{street}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span></div>
											</td>
											<td id="editVerifyPayeeNickNameValue" class="columndata" colspan="2">
											<s:property value="%{nickName}"/>
											</td>
											<td id="editVerifyPayeeAddress2Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="editVerifyPayeeAddress2Value" class="columndata" colspan="2">
											<s:property value="%{street2}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeeContactLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="editVerifyPayeeContactValue" class="columndata" colspan="2">
											<s:property value="%{contactName}"/>
											</td>
											<td id="editVerifyPayeeAddress3Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="editVerifyPayeeAddress3Value" class="columndata" colspan="2">
											<s:property value="%{street3}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeePhoneLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td id="editVerifyPayeePhoneValue" class="columndata" colspan="2">
											<s:property value="%{phone}"/>
											</td>
											<td id="editVerifyPayeeCityLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="editVerifyPayeeCityValue" class="columndata" colspan="2">
											<s:property value="%{city}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeeAccountLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span></div>
											</td>
											<td id="editVerifyPayeeAccountValue" class="columndata" colspan="2">
											<s:property value="%{userAccountNumber}"/>
											</td>
											<td id="editVerifyPayeeStateLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="editVerifyPayeeStateValue" class="columndata" colspan="2">
												<s:property value="%{stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeeRountingNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">
													<s:property value="%{bankIdentifierDisplayText}"/>
													<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden>
													</span></div>
											</td>
											<td id="editVerifyPayeeRountingNoValue" class="columndata" colspan="2">
											<s:property value="%{payeeRoute.bankIdentifier}"/>
											</td>
											<td id="editVerifyPayeeZipLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td id="editVerifyPayeeZipValue" class="columndata" colspan="2">
											<s:property value="%{zipCode}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeeBankAccountNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_26"/></span></div>
											</td>
											<td id="editVerifyPayeeBankAccountNoValue" class="columndata" colspan="2">
											<s:property value="%{payeeRoute.accountID}"/>
											</td>
											<td id="editVerifyPayeeCountryLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<td id="editVerifyPayeeCountryValue" class="columndata" colspan="2">
											<s:property value="%{countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="editVerifyPayeeBankAccountTypeLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_27"/></span></div>
											</td>
											<td id="editVerifyPayeeBankAccountTypeValue" class="columndata" colspan="2">
												<s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
										<tr>
											<td id="editVerifyPayeeBankAccountCurrencyLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_25"/></span></div>
											</td>
											<td id="editVerifyPayeeBankAccountCurrencyValue" class="columndata" colspan="2">
											<s:property value="%{payeeRoute.currencyCode}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										</ffi:cinclude>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right"></div>
											</td>
											<td colspan="4"><div align="center">
													<br>
													<sj:a id="cancelBillpayPayeeConfirm"
										                button="true" 
														onClickTopics="cancelBillpayForm"
										        	><s:text name="jsp.default_82"/></sj:a>
										        	
										        	<sj:a id="backInitiateBillpayPayeeEdit" 
					            						button="true" 
														onClickTopics="backToBillpayInput"
					        						><s:text name="jsp.default_57"/></sj:a>
	                        				
													<sj:a 
														id="sendSingleBillpayPayeeEditSubmit"
														formIds="BillpayPayeeEditSend"
						                                targets="confirmDiv" 
						                                button="true" 
						                                onBeforeTopics="beforeSendBillpayForm"
						                                onSuccessTopics="sendBillpayPayeeFormSuccess"
						                                onErrorTopics="errorSendBillpayForm"
						                                onCompleteTopics="sendBillpayPayeeFormComplete"
			                        				><s:text name="jsp.default_366"/></sj:a>
													</div></td>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table> --%>
<div class="btn-row">
	<sj:a id="cancelBillpayPayeeConfirm"
            button="true" 
            summaryDivId="summary" buttonType="cancel"
onClickTopics="showSummary,cancelBillpayForm,cancelPayeeLoadForm"
    	><s:text name="jsp.default_82"/></sj:a>

<sj:a id="backInitiateBillpayPayeeEdit" 
   						button="true" 
onClickTopics="backToBillpayInput"
					><s:text name="jsp.default_57"/></sj:a>

<sj:a 
	id="sendSingleBillpayPayeeEditSubmit"
	formIds="BillpayPayeeEditSend"
                         targets="confirmDiv" 
                         button="true" 
                         onBeforeTopics="beforeSendBillpayForm"
                         onSuccessTopics="sendBillpayPayeeFormSuccess"
                         onErrorTopics="errorSendBillpayForm"
                         onCompleteTopics="sendBillpayPayeeFormComplete"
              				><s:text name="jsp.default_366"/></sj:a>
</div>
</div>
</s:form>

