<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="innerPortletsContainer">
<div id="rppay_summary_gridID">

	<%-- This Div was present in reversepositivepaysummary.jsp. Since,it was hidden, kept this DIV hidden only while migrating to new architecture. --%>
	<div id="hiddenDivFromOldCode" style="display: none;">
	<% 
			String cutOffTimeStr="";
			com.ffusion.beans.business.Business business = (com.ffusion.beans.business.Business)session.getAttribute(com.ffusion.tasks.business.BusinessTask.BUSINESS);
			
			String rppayDefaultDecision = null;
			
			if(business != null) {
				
				rppayDefaultDecision = business.getRPPayDefaultDecision();
				
			}
			
			// get cutOffTimeStr only if the rppay default decision is not empty and is not "NONE"
			// i.e. the system will make the rppay decision for this business at that time
			if( !( rppayDefaultDecision == null || rppayDefaultDecision.length() <= 0 || rppayDefaultDecision.equalsIgnoreCase( com.ffusion.beans.business.BusinessDefines.RPPAY_NONE ) ) ) {
				com.ffusion.beans.affiliatebank.CutOffDefinition cutOffDef = (com.ffusion.beans.affiliatebank.CutOffDefinition) session.getAttribute( com.ffusion.tasks.affiliatebank.AffiliateBankTask.CUTOFF_DEFINITION );
				
				if( cutOffDef!=null ) {
					com.ffusion.beans.affiliatebank.CutOffTimes cutOffTimes = cutOffDef.getCutOffs();
					com.ffusion.beans.affiliatebank.CutOffTime cutOffTime=(com.ffusion.beans.affiliatebank.CutOffTime)cutOffTimes.get( 0 );
					String timeOfDay = cutOffTime.getTimeOfDay();
		
					if( timeOfDay!=null ) {
						cutOffTimeStr = timeOfDay;
					}
				}
				
				if ( rppayDefaultDecision.equalsIgnoreCase( com.ffusion.beans.business.BusinessDefines.RPPAY_PAY ) ) {
					rppayDefaultDecision="Pay";
				} else if( rppayDefaultDecision.equalsIgnoreCase( com.ffusion.beans.business.BusinessDefines.RPPAY_RETURN ) ) {
					rppayDefaultDecision="Return";
				}
			}
		%>
		
		<ffi:cinclude value1="" value2="<%= cutOffTimeStr %>" operator="notEquals">
			<s:text name="jsp.cash_15"/> <%= cutOffTimeStr %> <s:text name="jsp.cash_55"/> <br>
			<s:text name="jsp.cash_50"/> <%= rppayDefaultDecision %> <s:text name="jsp.cash_16"/><br>
		</ffi:cinclude>
		</div>
		
		<!-- selected account value should be take from session to display -->
		<% String selectedValue = (String)session.getAttribute("selectedValue"); 
			if(selectedValue == null){
				selectedValue="";
			}
		%>
		<input type="hidden" id="setSelValue" value="<%= selectedValue %>"/>
		<div class="actionButtonArea marginTop20">
			<span class="paddingRight10"><s:text name="jsp.cash.ppay.select.account"/></span>
			<s:select list="reversePositivePayMap" class="txtbox" id="rppaySummaryGridID" onchange="getDetails()" headerKey="" headerValue="All Accounts"></s:select> 
		</div>
	</div>	
</div>

<script>	
			
		function getDetails(){
			
			var selectedValue = $('#rppaySummaryGridID').val();
			if(selectedValue!=''){
				var accountIdIndex = selectedValue.indexOf("A");
				var bankIndex = selectedValue.indexOf("B");
				var accountId = selectedValue.substring(accountIdIndex+1,bankIndex);
				var bankId = selectedValue.substring(bankIndex+1);
				var url ='/cb/pages/jsp/reversepositivepay/getReversePositivePayIssuesForAccountAction_init.action?accountID='+accountId+"&bankID="+bankId+"&status=New"
				ns.cash.refreshExceptionGridForAccount(url);
			}
			else{
				ns.cash.refreshRPPayGridURL = "<ffi:urlEncrypt url='/cb/pages/jsp/reversepositivepay/reversepositivepay_exception_grid.jsp'/>";
				ns.cash.refreshRPPayGrid(ns.cash.refreshRPPayGridURL);
			}
		}
		
		$(document).ready(function(){
			$("#jqgh_rppaySummaryGridID_account").addClass("gridNicknameField");
			var selVal = $('#setSelValue').val();
			
			if(selVal === null)
				selVal="";
				
			$('#rppaySummaryGridID').val(selVal);
		
			 if(ns.common.isInitialized($('#rppaySummaryGridID'),'ui-selectmenu')){
				 $('#rppaySummaryGridID').selectmenu("destroy");	
			 }	
		$("#rppaySummaryGridID").selectmenu({width: 350});
		});
</script>