<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script>	
 	    
	var helpGridId = "approvalRPPaySummaryGrid";
		
</script>
		
		<span id="normalInputTab" style="display:none;"><s:text name="jsp.default_390"/></span>
    	<span id="normalVerifyTab" style="display:none;"><s:text name="jsp.default_391"/></span>
    	<span id="normalSummaryTab" style="display:none;"><s:text name="jsp.cash_132"/></span>
		<span id="uploadInputTab" style="display:none;"><s:text name="jsp.cash_fileUploadStep1"/></span>
    	<span id="uploadVerifyTab" style="display:none;"><s:text name="jsp.cash_fileUploadStep2"/></span>

        <div id="rppay_workflowID" class="portlet wizardSupportCls">
	        <div class="portlet-header">
	        	<span class="portlet-title hidden" title="Reverse Positive Pay Summary"><s:text name="jsp.cash_132" /></span>
				<div class="searchHeaderCls hidden">
					<span class="searchPanelToggleAreaCls" onclick="$('#allTypesOfAccount').slideToggle();">
						<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
					</span>
					<div class="summaryGridTitleHolder" style="margin-left:20px;">
						<span id="selectedGridTab" class="selectedTabLbl" style="padding-left:0px;">
							<s:text name="jsp.default_606"/>
						</span>
						<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
						<div class="gridTabDropdownHolder">
							<span class="gridTabDropdownItem" id='approvalRPPay' onclick="showRPPayGridSummary('approvalRPPay')" >
								<s:text name="jsp.default_606"/>
							</span>
							<span class="gridTabDropdownItem" id='pendingRPPay' onclick="showRPPayGridSummary('pendingRPPay')">
								<s:text name="jsp.cash_132"/>
							</span>							
						</div>
					</div>
				</div>
			</div>
			<div class="portlet-content">
				<div id="gridContainer" class="summaryGridHolderDivCls">
				<s:include value="/pages/jsp/reversepositivepay/inc/reversepositivepaySearchCriteria.jsp"/>
				
					<div id="approvalRPPaySummaryGrid" gridId="rppayApprovalExceptionGridID" class="gridSummaryContainerCls" >
						 <s:action namespace="/pages/jsp/reversepositivepay" name="initReversePostivePaySummaryAction" executeResult="true">
							<s:param name="collectionName">PendingApprovalRPPayIssues</s:param>
						 </s:action> 
					</div>
					
					<div id="pendingRPPaySummaryGrid" gridId="rppayExceptionGridID" class="gridSummaryContainerCls hidden" >
					
						<sj:tabbedpanel id="RPPayWizardTabs" cssClass="marginTop20" >
						
							<sj:tab id="inputTab" target="firstDiv" label="%{getText('jsp.default_390')}"/>
							<sj:tab id="verifyTab" target="secondDiv" label="%{getText('jsp.default_392')}"/>
							<sj:tab id="confirmTab" target="thirdDiv" label="%{getText('jsp.default_393')}"/>
							<div id="firstDiv" class="borderNone">
								<%-- <s:text name="jsp.cash_84"/> --%>
								<s:action namespace="/pages/jsp/reversepositivepay" name="initReversePostivePaySummaryAction" executeResult="true">
									<s:param name="collectionName">PendingRPPayIssues</s:param>
								</s:action> 
							</div>
							<div id="secondDiv" class="borderNone">
								<s:text name="jsp.cash_120"/>
							</div>
							 <div id="thirdDiv" class="borderNone">
								<s:text name="jsp.cash_120"/>
							</div>
						</sj:tabbedpanel>
						
					</div>
			</div>
			<div id="reversepositivePayDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
		       <ul class="portletHeaderMenu" style="background:#fff">
		       		  <%-- <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li> --%>
		              <li><a href='#' onclick="showDetailsHelp();"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
		       </ul>
			</div>
		</div>
		<input type="hidden" id="actionType" value="<s:property value='#parameters.summaryToLoad' />"/>
    <script>	
 	    //Initialize portlet with settings icon		
		ns.common.initializePortlet("rppay_workflowID");
 	    ns.common.updatePortletTitle("rppay_workflowID",jsp.cash_132,true);
		
		function showDetailsHelp() {
			ns.common.showDetailsHelp(helpGridId);
		}
	</script>