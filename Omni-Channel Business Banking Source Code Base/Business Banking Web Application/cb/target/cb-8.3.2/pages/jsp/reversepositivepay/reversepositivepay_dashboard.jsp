<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

   	<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-positivePay"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_263" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/cashflowSubmenuDropdown.jsp" />
    	</div>
    	
    	<!-- dashboard items listing for PositivePay -->
        <div id="dbReversePositivePaySummary" class="dashboardSummaryHolderDiv">
       		<sj:a
				id="rppaySummaryBtn"
				indicator="indicator"
				cssClass="summaryLabelCls"
				button="true"
				onclick="$('li[menuId=cashMgmt_rppay]').find('a').eq(0).click()"
				title="%{getText('jsp.default_400')}"
			><s:text name="jsp.default_400"/></sj:a>

		</div>

		
		
	</div>
	
	<sj:dialog id="InquiryTransferDialogID" cssClass="cashMgmtDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>
	
	
<script>
	$("#rppaySummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>