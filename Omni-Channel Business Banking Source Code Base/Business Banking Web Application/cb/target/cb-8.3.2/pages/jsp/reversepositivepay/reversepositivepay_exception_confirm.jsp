<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script >
	helpGridId = "rppay_workflowID";
</script>

<ffi:help id="cash_cashrppconfirm" />

		
    <ffi:setProperty name="tempURL" value="/pages/jsp/reversepositivepay/getReversePositivePayConfirmIssuesAction_complete.action" URLEncrypt="true"/>
    <s:url id="rppayExceptionCompleteGridURL" value="%{#session.tempURL}"/>
	<sjg:grid  
		id="rppayExceptionCompleteGridID"  
		sortable="true"  
		dataType="json"  
		href="%{rppayExceptionCompleteGridURL}"  
		pager="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"		
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="RPPayExceptionCompleteEvent"
		> 
		
		<sjg:gridColumn name="issueDate" index="issueDate" title="%{getText('jsp.default_137')}" formatter="ns.cash.dateFormatter" sortable="false" width="80"/>
	    <sjg:gridColumn name="account" index="account" title="%{getText('jsp.default_15')}" sortable="false" width="248"/>
	    <sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}" sortable="false" width="80"/>
	    <sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" sortable="false" width="90"/>
	    <sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_350')}" sortable="false" width="110"/>
	    <sjg:gridColumn name="map.statusMsg" index="statusMsg" title="%{getText('jsp.default_438')}" sortable="false" width="210"/>
		
		<sjg:gridColumn name="decision" index="decision" title="%{getText('jsp.default_160')}" sortable="false" width="50" hidden="true" hidedlg="true"/> 
		<sjg:gridColumn name="map.routingNumber" index="routingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_9')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.nickName" index="nickName" title="%{getText('jsp.default_293')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_44')}" sortable="true" hidden="true" hidedlg="true"/>
	</sjg:grid>
	<br>
			
			
	<div class="submitButtonsDiv">
		<sj:a
			id="doneBtn"
			button="true"
			onClickTopics="rpPayExpSubmitForm"
			onSuccessTopics="ns.common.getBankingEvents,common.refreshApprovals,reloadReversePositivePayPortletGrid">
			<s:text name="jsp.default_175"/>
		</sj:a>
	</div>
             
      
    <script>
	ns.common.addGridControls("#rppayExceptionCompleteGridID", false);
	ns.common.updatePortletTitle("rppay_workflowID",js_rppay_verify_portlet_title,false);
	</script>
