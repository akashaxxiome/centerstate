<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

	<script type="text/javascript" src="<s:url value='/web/js/reversepositivepay/reversepositivepay%{#session.minVersion}.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/reversepositivepay/reversepositivepay_grid%{#session.minVersion}.js'/>"></script>

	<s:include value="inc/index-pre.jsp"/> 

	<s:set var="tmpI18nStr" value="%{getText('jsp.default_327')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	<ffi:setProperty name="PortalFlag" value="false"/>
	
	<ffi:setProperty name="subMenuSelected" value="reverse positive pay"/>
		
        <div id="desktop" align="center">

            <div id="operationresult">
				<div id="resultmessage"><s:text name="jsp.default_498"/></div>
			</div>

			<div id="appdashboard" class="clearBoth">
				<s:action namespace="/pages/jsp/reversepositivepay" name="reversePositivePayDashBoardAction" executeResult="true"/>
 	    	</div>
 	    	
 	    	<div id="rpPayList">
 	    	</div>
 	    	
			<div id="rpPaysummary" class="marginTop10">
					<s:include value="/pages/jsp/reversepositivepay/reversepositivepay_details.jsp">
						<s:param name="actionType" value="#parameters.summaryToLoad"></s:param>
					</s:include>
			</div>
			
			<div id="customMappingDetails" class="marginTop10">
				<s:include value="/pages/jsp/custommapping_workflow.jsp"/>
			</div>

			<div id="mappingDiv" class="marginTop10"> 
	 	    </div>

    </div>

	<script type="text/javascript">
		$(document).ready(function(){
			ns.cash.showDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
			$('#rppay_workflowID .wizardStatusContainerCls').hide();
			$('#rppay_workflowID .searchPanelToggleAreaCls').hide()
		});
	</script>	