<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


<% String taskErrorCode; %>
<ffi:getProperty name="CurrentTask" property="Error"  assignTo="taskErrorCode"/>
 <%
boolean useAdditionalErrorMessage = com.ffusion.util.CommBankIdentifier.isValidAdditionalServiceCode(taskErrorCode);
 %>


<div id="taskErrorMessageDiv" align="center">
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_189')}" scope="request" /><ffi:setProperty name="navTitle" value="${tmpI18nStr}"/>
    <ffi:setProperty name="showDate" value="true"/>
    <ffi:removeProperty name="navTitle"/>
    <ffi:removeProperty name="showDate"/>

	<ffi:cinclude value1="${CurrentTask.Error}" value2="<%= String.valueOf( com.ffusion.csil.CSILException.ERROR_EXCEED_LIMIT ) %>" operator="equals">
		<script>
			window.location.href = "<ffi:getProperty name='SecurePath'/>approvals/exceedlimitsnoapproval.jsp";
		</script>
	</ffi:cinclude>
	<%-- ================ MAIN CONTENT START ================ --%>
	<ffi:setProperty name="${touchedvar}" value="false"/>

	<%-- ERROR MESSAGE BEGIN --%>
	<br>
     <center>
         <table cellpadding="1" cellspacing="2" border="0" class="mainfont">
             <tr>
                 <td align="center" colspan="2">
                      <% if ( useAdditionalErrorMessage ) { %>
                        <ffi:setProperty name="ServiceErrorsResource" property="ResourceID" value="ErrorA${CurrentTask.Error}_descr" />
                      <% } else { %>                                          
                        <ffi:setProperty name="ServiceErrorsResource" property="ResourceID" value="Error${CurrentTask.Error}_descr" />
                      <% } %>
					<ffi:cinclude value1="${ServiceErrorsResource.Resource}" value2="" operator="equals" >
						<s:text name="jsp.default_114"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ServiceErrorsResource.Resource}" value2="" operator="notEquals" >								     
						<ffi:getProperty name="ServiceErrorsResource" property="Resource"/>										
					</ffi:cinclude>
                 </td>
             </tr>
			<%-- SE_WHY will have a BPW specific error, text gotten from BPW --%>
			<% if (session.getAttribute("SE_WHY") != null) { %>
             <tr>
                 <td align="right" class="homesub"><b><s:text name="jsp.default_190"/></b>&nbsp;</td>
                 <td class="errorBackground">
                     <ffi:getProperty name="SE_WHY"/>
                 </td>
             </tr>
			<% session.removeAttribute("SE_WHY");
			} %>
             <tr>
                 <td align="center" colspan="2">&nbsp;<br><b><s:text name="jsp.default_213"/></b></td>
             </tr>
             <tr>
                 <td align="right" class="homesub"><b><s:text name="jsp.default_404"/></b>&nbsp;</td>
                 <td class="errorBackground">
                     <ffi:getProperty name="CurrentTask"/>
                 </td>
             </tr>
             <tr>
                 <td align="right" class="homesub"><b><s:text name="jsp.default_403"/></b>&nbsp;</td>
                 <td class="errorBackground">
                     <ffi:getProperty name="CurrentTask" property="Error"/>
                 </td>
             </tr>
             <tr>
                 <td align="right" class="homesub"><b><s:text name="jsp.default_191"/></b>&nbsp;</td>
                 <td class="errorBackground">
                     <ffi:getProperty name="ServiceErrorsResource" property="ResourceFilename"/>
                 </td>
             </tr>
         </table>
         <br>
			<sj:a id="doneButtonOnServiceError" 
	                button="true" 
					onClickTopics="%{#session.TopicOnTaskError}"
	        ><s:text name="jsp.default_303"/></sj:a>
     </center>
</div>
