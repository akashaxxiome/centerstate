<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

    
    <div id="appdashboard-right" >
    	<div style="float:right;">
		    <s:url id="addmappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
				<s:param name="MappingDefinitionID" value="0"/>
			    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>		
			</s:url>
			<sj:a
				id="addmappingLink"
				href="%{addmappingUrl}" 
				targets="inputCustomMappingDiv" 
				indicator="indicator" 
				button="true" 
				buttonIcon="ui-icon-copy"
				onCompleteTopics="addMappingSuccess"
			><s:text name="jsp.default_3"/></sj:a>
		</div>
		<span class="ui-helper-clearfix">&nbsp;</span>	
    </div>
	<span class="ui-helper-clearfix">&nbsp;</span>	
	<br>
	<%-- Following three dialogs are used for view, delete and inquiry transfer --%>
	
	<sj:dialog id="InquiryTransferDialogID" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>