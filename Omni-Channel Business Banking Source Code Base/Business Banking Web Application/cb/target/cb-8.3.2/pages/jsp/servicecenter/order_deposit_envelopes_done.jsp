<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="servicecenter/order_deposit_envelopes_done.jsp"/>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/order_deposit_envelopes_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>

<ffi:removeProperty name="OrderDepositEnvelopes" />
