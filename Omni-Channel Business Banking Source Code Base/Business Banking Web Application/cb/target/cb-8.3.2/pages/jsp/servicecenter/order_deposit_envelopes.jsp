<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<ffi:author page="servicecenter/order_deposit_envelopes.jsp" />
<ffi:help id="servicecenter_services_onlineCustomerServices_orderDepositEnvelopes" />

<ffi:object id="OrderDepositEnvelopes"
	name="com.ffusion.tasks.messages.SendOrderDepositEnvelopesMessage" />
<%
	session.setAttribute("FFIOrderDepositEnvelopes",
			session.getAttribute("OrderDepositEnvelopes"));
%>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_order_deposit_envelopes" /> </span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,orderDepositEnvelopesLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<div align="center">
	<ul id="formerrors"></ul>
	<s:form id="orderEnvelopesForm" namespace="/pages/jsp/servicecenter"
		action="orderDepositEnvelopesAction" validate="true" method="post"
		name="FormName" theme="simple">
		<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="instructions" colspan="2"><ffi:getL10NString
						rsrcFile="cb"
						msgKey="jsp/servicecenter/order_deposit_envelopes.jsp-1" />
				</td>
			</tr>
			<tr>
				<td align="left" colspan="3">&nbsp;</td>
			</tr>
			<tr valign="top">
				<td colspan="3" align="center" class="filter_table"><sj:a
						button="true" onClickTopics="cancelServicesForm">
						<s:text name="jsp.default_82" />
					</sj:a> 
					<sj:a id="orderEnvelopesFormSubmit"
						formIds="orderEnvelopesForm" targets="resultmessage" button="true"
						validate="true" validateFunction="customValidation"
						onBeforeTopics="beforeConfirmOrderEnvelopesForm"
						onCompleteTopics="confirmOrderEnvelopesComplete"
						onErrorTopics="errorConfirmOrderEnvelopesForm"
						onSuccessTopics="successConfirmOrderEnvelopesForm">
						<s:text name="jsp.servicecenter_order_envelopes" />
					</sj:a>	
				</td>
			</tr>
			<tr>
				<td align="left" colspan="3">&nbsp;</td>
			</tr>
		</table>
	</s:form>
</div>
