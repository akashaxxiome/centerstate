<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>



<div id="serviceSummaryTabs" class="portlet showTitle" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="SERVICES"/></span>
	</div>
	<div class="portlet-content" id="serviceSummaryTabsContent">
		<!-- <div id="onlineCustomerServicesPortID"> -->
			<s:include value="/pages/jsp/servicecenter/inc/online_customer_services.jsp"/>
		<!-- </div> -->
	</div>
	<div id="serviceSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('serviceSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<div class="clearBoth"  style="padding: 10px 0"></div>

<div id="docCenterSummaryTabs" class="portlet showTitle" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.servicecenter_document_center"/></span>
	</div>
	<div class="portlet-content" id="docCenterSummaryTabsContent">
		<!-- <div id="documentCenterPortID">  -->
			<s:include value="/pages/jsp/servicecenter/inc/document_center.jsp"/>
		<!-- </div> -->
	</div>
	<div id="docCenterSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('docCenterSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("serviceSummaryTabs");
	ns.common.initializePortlet("docCenterSummaryTabs");
</script>