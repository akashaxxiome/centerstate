<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.ffusion.tasks.messages.ServiceCenterTask"%>
<%@ page
	import="com.ffusion.tasks.messages.SendOrderCopyClearedChecksMessage"%>
<ffi:help id="servicecenter_services_onlineCustomerServices_orderClearedChecks" />

<%-- clean up --%>	
<ffi:removeProperty name="${OrderClearedChecks.CheckNumberPrefix}" startsWith="true" />
<ffi:removeProperty name="OrderClearedChecks" />

<ffi:setProperty name="topMenu" value="serviceCenter" />
<ffi:setProperty name="subMenu" value="services" />
<ffi:setProperty name="sub2Menu" value="" />
<ffi:setL10NProperty name="PageTitle" value="Service Center" />
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,orderClearedChecksLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries"
	name="com.ffusion.tasks.banking.GetSummariesForAccountDate"
	scope="session" />
<ffi:setProperty name="GetAccountSummaries"
	property="UpdateAccountBalances" value="true" />
<ffi:setProperty name="GetAccountSummaries" property="SummariesName"
	value="LatestSummaries" />
<ffi:process name="GetAccountSummaries" />
<ffi:removeProperty name="GetAccountSummaries" />
<ffi:removeProperty name="LatestSummaries" />

<ffi:cinclude value1="${OrderClearedChecks}" value2="" operator="equals">
	<ffi:object id="OrderClearedChecks"
		name="com.ffusion.tasks.messages.SendOrderCopyClearedChecksMessage" />
</ffi:cinclude>

<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" />
<ffi:setProperty name="SetAccount" property="AccountsName"
	value="BankingAccounts" />
<ffi:setProperty name="SetAccount" property="AccountName"
	value="SelectedAccount" />

<%
	session.setAttribute("FFIOrderClearedChecks",
			session.getAttribute("OrderClearedChecks"));
	session.setAttribute("FFISetAccount",
			session.getAttribute("SetAccount"));
%>

<%
	int numChecks = 6;
	SendOrderCopyClearedChecksMessage orderClearedChecks = (SendOrderCopyClearedChecksMessage) session
			.getAttribute("OrderClearedChecks");

	if (orderClearedChecks != null) {
		numChecks = orderClearedChecks.getNumberOfChecks();
	}
%>

<script language="JavaScript" type="text/javascript">
<!--
	function onSubmitCheck() {
		orderClearedChecksForm.submit();
	}
// -->
</script>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_order_copies_of_cleared_checks" />
</span>
<div align="center">
	<ul id="formerrors"></ul>
	<s:form id="orderClearedChecksForm"
		namespace="/pages/jsp/servicecenter" action="orderClearedChecksAction"
		validate="true" method="post" name="FormName" theme="simple">
		<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		
		 <table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="35%" valign="top">
					<div class="paneWrapper">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="jsp.billpay_96" />
							</div>
							<div class="paneContentWrapper">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td><span> <s:text name="jsp.default_21" /> </span> <span class="required">*</span></td>
										<td>
											<div class="selectBoxHolder">
												<select id="accountNumberId" name="AccountNumber"
												class="txtbox">
													<option value=""><s:text name="jsp.default_375" /></option>
													<ffi:setProperty name="Compare" property="Value1"
														value="${AccountNumber}" />
													<ffi:list collection="BankingAccounts" items="BAccount">
														<ffi:setProperty name="Compare" property="Value2"
															value="${BAccount.ID}" />
														<option value="<ffi:getProperty name="BAccount" property="ID"/>"
															<ffi:getProperty name="selected${Compare.Equals}"/>
															><ffi:getProperty name="BAccount" property="ConsumerMenuDisplayText" /></option>
													</ffi:list>
											</select>
											</div>
											 <script type="text/javascript">
												$("#accountNumberId").combobox({
													'size' : '30'
												});
											</script> <br><span id="AccountNumberError" style="display:block; float:left;"></span>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</td>
				<td width="1%">&nbsp;</td>
				<td align="left" class="adminBackground" width="64%" valign="top">
				<div class="paneWrapper">
			   	<div class="paneInnerWrapper">
					<div class="header">
						<s:text name="jsp.servicecenter_check_details" />
					</div>
					<div class="paneContentWrapper">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<ffi:setProperty name="BankingAccounts" property="Filter"
							value="HIDE=0,COREACCOUNT=1,TYPE=1,AND" />
						<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
							operator="equals">
						<tr>
							<td colspan="2" align="center"><ffi:getL10NString
									rsrcFile="cb"
									msgKey="jsp/servicecenter/order_cleared_checks.jsp-1" /></td>
						</tr>
						</ffi:cinclude>
						<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
						
						<tr>
						<%
							int j =0;
							for (int i = 1; i <= numChecks; i++) {
						%>
						<ffi:setProperty name="index" value="<%= String.valueOf ( i ) %>" />
							<td width="50%" class="sectionsubhead ltrow2_color"><ffi:cinclude
									value1="${firstDisplayed}" value2="true" operator="equals">
									<span><ffi:getL10NString rsrcFile="cb"
											msgKey="checkNumberWithIndex" parm0="${index}" /> </span>
								</ffi:cinclude> <ffi:cinclude value1="${firstDisplayed}" value2="true"
									operator="notEquals">
									<span><ffi:getL10NString rsrcFile="cb"
											msgKey="checkNumberWithIndex" parm0="${index}" /> </span>
									<span class="required">*</span>
									<ffi:setProperty name="firstDisplayed" value="true" />
								</ffi:cinclude>
							</td>
							<%
								Boolean isNext = true; 
								if(numChecks>i)
								j=i+1;
								else
								isNext = false;
							%>
							
							<% if(isNext){%>
							<ffi:setProperty name="newIndex" value="<%= String.valueOf ( j ) %>" />
							<td width="50%" class="sectionsubhead ltrow2_color"><ffi:cinclude
									value1="${firstDisplayed}" value2="true" operator="equals">
									<span><ffi:getL10NString rsrcFile="cb"
											msgKey="checkNumberWithIndex" parm0="${newIndex}" /> </span>
								</ffi:cinclude> <ffi:cinclude value1="${firstDisplayed}" value2="true"
									operator="notEquals">
									<span><ffi:getL10NString rsrcFile="cb"
											msgKey="checkNumberWithIndex" parm0="${newIndex}" /> </span>
									<span class="required">*</span>
									<ffi:setProperty name="firstDisplayed" value="true" />
								</ffi:cinclude>
							</td>
							<%}%>
						</tr>
						
						<tr>
							<td>
							
							  <input name="<ffi:getProperty name="OrderClearedChecks" property="CheckNumberPrefix" /><ffi:getProperty name="index" />"
								type="text" size="25" maxlength="40" border="0"
								class="txtbox ui-widget-content ui-corner-all"
								value='<ffi:getProperty name="${OrderClearedChecks.CheckNumberPrefix}${index}" />'>
								<br><span
								id="<ffi:getProperty name="OrderClearedChecks" property="CheckNumberPrefix" /><ffi:getProperty name="index" />Error"></span>
							</td>
							<% if(isNext){%>
							<td>
							   <input name="<ffi:getProperty name="OrderClearedChecks" property="CheckNumberPrefix" /><ffi:getProperty name="newIndex" />"
								type="text" size="25" maxlength="40" border="0"
								class="txtbox ui-widget-content ui-corner-all"
								value='<ffi:getProperty name="${OrderClearedChecks.CheckNumberPrefix}${newIndex}" />'>
								<span
								id="<ffi:getProperty name="OrderClearedChecks" property="CheckNumberPrefix" /><ffi:getProperty name="newIndex" />Error"></span>
							</td>
							<%}%>
						</tr>
						<%
							i=i+1;
						%>
						<%
							}
						%>
						<ffi:removeProperty name="index" />
						<ffi:removeProperty name="firstDisplayed" />
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong><s:text name="jsp.servicecenter_delivery_method" />: </span></strong>
								<span class="required">*</span>
							</td>
						</tr>
						<tr>
							<td>
								<input name="OrderClearedChecks.DeliveryMethod" type="radio" value="<%=ServiceCenterTask.ADDRESS_OF_RECORD%>" checked="true" />
								&nbsp; <span><s:text name="jsp.servicecenter_send_to_address_of_record" /> </span>
						    </td>
						    <td>
								<ffi:setProperty name="Compare" property="Value1"
									value="${OrderClearedChecks.DeliveryMethod}" />
								<ffi:setProperty name="Compare" property="Value2"
								value="<%= String.valueOf ( ServiceCenterTask.FAX ) %>" />
								<input name="OrderClearedChecks.DeliveryMethod" type="radio" value="<%= ServiceCenterTask.FAX %>" <ffi:getProperty name="checked${Compare.Equals}" />
								
								<span class="sectionsubhead ltrow2_color"><s:text
								name="jsp.servicecenter_fax_to_this_number" /> <s:text
								name="jsp.default_colon" /> </span><span> 
						    </td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<input name="OrderClearedChecks.FaxNumber" type="text" size="20" maxlength="25" border="0" class="txtbox ui-widget-content ui-corner-all" value='<ffi:getProperty name="OrderClearedChecks" property="FaxNumber"/>'>
								<span id="OrderClearedChecks.FaxNumberError"></span>
							</td>
						</tr>
						</ffi:cinclude>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
						</tr>
						<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
						<tr>
							<td align="center" class="required" colspan="2">* 
								<s:text name="jsp.default_240" />
							</td>
						</tr>
						</ffi:cinclude>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<sj:a button="true"
									onClickTopics="cancelServicesForm">
									<s:text name="jsp.default_82" />
								</sj:a> <ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
									operator="notEquals">
									<sj:a id="orderClearedChecksFormSubmit"
										formIds="orderClearedChecksForm" targets="resultmessage"
										button="true" validate="true" validateFunction="customValidation"
										onBeforeTopics="beforeConfirmOrderClearedChecksForm"
										onCompleteTopics="confirmOrderClearedChecksComplete"
										onErrorTopics="errorConfirmOrderClearedChecksForm"
										onSuccessTopics="successConfirmOrderClearedChecksForm">
										<s:text name="jsp.servicecenter_send_order" />
										</sj:a>	
								</ffi:cinclude></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
						</tr>
					</table>
					</div>
					</div>
					</div>
				</td>
			</tr>
		 </table>	
	</s:form>
</div>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
