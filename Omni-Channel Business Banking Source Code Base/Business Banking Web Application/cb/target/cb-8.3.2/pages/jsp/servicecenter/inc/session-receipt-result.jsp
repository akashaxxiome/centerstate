<%@ page import="com.ffusion.beans.user.UserLocale"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<div id="sessionReceiptReportID">
		<div id="exportReportID">
			<s:form id="exportSessionReceiptReportFormID" theme="simple" target="_blank">
				<input type="hidden" name="GenerateReportBase.Destination"
					value="<%=com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS%>">
				<input type="hidden" name="CSRF_TOKEN"
					value="<ffi:getProperty name='CSRF_TOKEN'/>" />
				<input type="hidden" name="returnjsp" value="false" />
				<s:hidden key="reportID" />
						<select id="exportReportSelectSessionRectResID"
						onchange="$('#exportSessionReceiptReportButtonId').trigger('click')"
							class="txtbox floatRight" name="GenerateReportBase.Format">
								<%-- <option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML%>"><s:text name="jsp.default_231" /></option>
	
								<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>"><s:text name="jsp.default_105" /></option>
	
								<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>"><s:text name="jsp.default_402" /></option>
	
								<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>"><s:text name="jsp.default_323" /></option>
	
								<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>"><s:text name="jsp.default_325" /></option> --%>
								<option value="0">
							<s:text name="jsp.default_195"/>
						</option>
						
						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>">
							<s:text name="jsp.default_231"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>">
							<s:text name="jsp.default_105"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>">
							<s:text name="jsp.default_402"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>">
							<s:text name="jsp.default_323"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>">
							<s:text name="jsp.default_325"/>
						</option>
						</select>&nbsp;&nbsp; <input type="hidden"
							name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX%><%=com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION%>"
							value="<%=com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS%>">
						<sj:a
								id="exportSessionReceiptReportButtonId"
								cssStyle="display:none"
								onclick="{ns.report.exportReport('/cb/pages/jsp/reports/UpdateReportCriteriaAction_generateReport.action', $('#exportSessionReceiptReportFormID')[0]);}"
								effect="pulsate" value="Export" indicator="indicator"
								button="true">
								<s:text name="jsp.default_195" />
							</sj:a>
			</s:form>
		</div>
		<span class="ui-helper-clearfix">&nbsp;</span>
	<div id="sessionReceiptReportTableID" class="sessionActivityReportTable tableData marginTop10 approvalDialogConsumerHt">
		
		<ffi:setProperty name="ReportData"
						property="ReportCriteria.CurrentReportOption"
						value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>" />
					<ffi:object id="exportForDisplay"
						name="com.ffusion.tasks.reporting.ExportReport" /> <ffi:setProperty
						name="exportForDisplay" property="ReportName" value="ReportData" />
					<ffi:setProperty name="exportForDisplay" property="ExportFormat"
						value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
					<ffi:process name="exportForDisplay" /> <ffi:getProperty
						name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>"
						encode="false" /> <ffi:removeProperty name="exportForDisplay" />
		<div class="ffivisible" style="height:80px;">&nbsp;</div>				
	</div>
</div>

<script>
	/* $('#exportReportID').portlet({
			generateDOM: true,
			title: js_export_report
	}); */
	
	/* $('#sessionReceiptReportTableID').portlet({
		generateDOM: true,
		title: js_report
	}); */
	
	$("#exportReportSelectSessionRectResID").selectmenu({
		width : '14em'
	});
</script>
