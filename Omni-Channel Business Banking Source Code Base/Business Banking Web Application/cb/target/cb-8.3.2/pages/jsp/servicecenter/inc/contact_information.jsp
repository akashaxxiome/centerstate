<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>



<div id="contactInfoTab" class="portlet">
	<ffi:help id="servicecenter_services_contactInformation" />
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.servicecenter_contact_information"/></span>
	</div>
	<div id="contactInfoContentDiv" class="portlet-content">
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
		    <tr class="filter_table" valign="top"> 
			    <td align="left" class="filter_table">
					<div class="txt_normal marginTop20" style="margin-left:20px;">
						<b><s:text name="jsp.default_35" />:</b>&nbsp;&nbsp;
					    SAP Online Bank, 12345 Elm Street, 
						Box 8000, Anytown, 
						NT 00000-0000<br/>
						<s:text name="jsp.servicecenter_toll_free" /><s:text name="jsp.default_colon" />&nbsp;&nbsp; 1-800-123-4567</br>
						<s:text name="jsp.servicecenter_phone" /><s:text name="jsp.default_colon" />&nbsp;&nbsp;000-123-4567		
					</div>
			    </td>
		    </tr>
		    <tr>
			<td align="center" colspan="2">
				<span class="approvalsCls">
					<hr />
				</span>
			    <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<td width="2%">
				</td>
				<td width="96%" >
				<div class="paneWrapper marginTop20">
				   	<div class="paneInnerWrapper">
						<div class="header">
						    <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="header" align="left">
							    <td width="23%">&nbsp;</td>
							    <td width="33%"><s:text name="jsp.servicecenter_hours" /></td>
							    <td width="17%"><s:text name="jsp.servicecenter_telephone_number" /></td>
							    <td width="27%"><s:text name="jsp.servicecenter_email" /></td>
							</tr>
							</table>
						</div>			
					<div class="paneContentWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr align="left">
						    <td width="23%" class="filter_table" align="left">
							<span class="txt_normal_bold" >
							    <s:text name="jsp.servicecenter_general_information" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
								<s:text name="jsp.servicecenter_24_hrs_7_days" />
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-5555
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:general_info@saponlinebank.com')">general_info@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_checking_and_savings" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_24_hrs_7_days" />
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-7854
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:accounts@saponlinebank.com')">accounts@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_credit_cards" />
							<span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_24_hrs_7_days" />
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-4662
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:credit_cards@saponlinebank.com')">credit_cards@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_financial_planning" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
								<s:text name="jsp.servicecenter_mon_to_fri_7am_to_9pm" /></br>
								<s:text name="jsp.servicecenter_sat_8am_to_5pm" /></br>
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-1236
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:financial_planning@saponlinebank.com')">financial_planning@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_investments" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_24_hrs_7_days" />
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-5662
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:investments@saponlinebank.com')">investments@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_mortgages" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_mon_to_fri_7am_to_9pm" /></br>
								<s:text name="jsp.servicecenter_sat_8am_to_5pm" /></br>
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-7111
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:mortgages@saponlinebank.com')">mortgages@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_online_banking" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_mon_to_fri_7am_to_9pm" /></br>
								<s:text name="jsp.servicecenter_sat_8am_to_5pm" /></br>
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-7894
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" onclick="ns.common.openEmailClient('mailto:online_banking@saponlinebank.com')">online_banking@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_personal_loans" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_mon_to_fri_7am_to_9pm" /></br>
								<s:text name="jsp.servicecenter_sat_8am_to_5pm" /></br>
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-7813
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a  href="#" onclick="ns.common.openEmailClient('mailto:personal_loans@saponlinebank.com')">personal_loans@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
						<tr align="left">
						    <td width="23%" class="filter_table" align="left" >
							<span class="txt_normal_bold" >
								<s:text name="jsp.servicecenter_student_loans" />
							</span>
						    </td>
						    <td width="33%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <s:text name="jsp.servicecenter_mon_to_fri_7am_to_9pm" /></br>
								<s:text name="jsp.servicecenter_sat_8am_to_5pm" /></br>
							</span>
						    </td>
						    <td width="17%" class="filter_table" align="left">
							<span class="txt_normal" >
							    1-800-555-7951
							</span>
						    </td>
						    <td width="27%" class="filter_table" align="left" >
							<span class="txt_normal" >
							    <a href="#" href="#" onclick="ns.common.openEmailClient('mailto:student_loans@saponlinebank.com')">student_loans@saponlinebank.com</a>
							</span>
						    </td>
						</tr>
					  </table>
					</div>
				  </div>   
				</td>
				<td width="2%">
				</td>
			    </table>
			</td>
		    </tr>
		    <tr>
			<td align="left" class="txt_normal" colspan="2" >&nbsp;</td>
		    </tr>
		</table>
	</div>
	<div id="contactInfoTab-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('contactInfoTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>



<script>    
ns.common.initializePortlet("contactInfoTab");
</script>
