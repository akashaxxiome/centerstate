<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.tasks.messages.ServiceCenterTask" %>
<ffi:author page="servicecenter/order_copy_statement.jsp"/>
<ffi:help id="servicecenter_services_onlineCustomerServices_orderCopyStatement" />

<%-- clean up --%>	
<ffi:removeProperty name="SendOrderStatement" />

<ffi:setProperty name="topMenu" value="serviceCenter"/>
<ffi:setProperty name="subMenu" value="services"/>
<ffi:setProperty name="sub2Menu" value=""/>
<ffi:setL10NProperty name="PageTitle" value="Service Center"/>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries" name="com.ffusion.tasks.banking.GetSummariesForAccountDate" scope="session"/>
<ffi:setProperty name="GetAccountSummaries" property="UpdateAccountBalances" value="true"/>
<ffi:setProperty name="GetAccountSummaries" property="SummariesName" value="LatestSummaries"/>
<ffi:process name="GetAccountSummaries"/>
<ffi:removeProperty name="GetAccountSummaries"/>
<ffi:removeProperty name="LatestSummaries"/>

<ffi:cinclude value1="${BackURLOld}" value2="" operator="notEquals" >
    <ffi:setProperty name="BackURL" value="${BackURLOld}" />
</ffi:cinclude>

<!--<ffi:cinclude value1="${SendOrderStatement}" value2="" operator="equals" >-->
    <ffi:object id="SendOrderStatement" name="com.ffusion.tasks.messages.SendOrderStatementCopyMessage" />
    <ffi:setProperty name="SendOrderStatement" property="BeginDate" value="${UserLocale.DateFormat}" />
    <ffi:setProperty name="SendOrderStatement" property="EndDate" value="${UserLocale.DateFormat}" />
    <ffi:setProperty name="SendOrderStatement" property="DateFormat" value="${UserLocale.DateFormat}" />
<!--</ffi:cinclude>-->

<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" />
<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts" />
<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount" />

<%
session.setAttribute("FFISendOrderStatement", session.getAttribute("SendOrderStatement"));
session.setAttribute("FFISetAccount", session.getAttribute("SetAccount"));
%>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_order_statement_copies" /></span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,orderCopyStatementLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<div align="center">
	<ul id="formerrors"></ul>
	<s:form id="orderStatementForm" namespace="/pages/jsp/servicecenter" action="orderCopyStatementAction" validate="true" method="post" name="FormName" theme="simple">
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	    <table width="60%" border="0" cellspacing="0" cellpadding="3">
	    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND" />
	    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,TYPE=1,AND" />
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="equals">
			<tr>
				<td colspan="3" class="sectionsubhead ltrow2_color" align="center">
				    <ffi:getL10NString rsrcFile="cb" msgKey="jsp/servicecenter/order_copy_statement.jsp-1" />
				</td>
			</tr>
		</ffi:cinclude>
		
		 <table width="100%" border="0" cellspacing="0" cellpadding="5">
		 <ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
			<tr>
				<td width="30%" valign="top">
					<div class="paneWrapper">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="jsp.billpay_96" />
							</div>
							<div class="paneContentWrapper">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td valign="top"><span><s:text name="jsp.default_21" /></span><span class="required" >*</span></td>
										<td>
										<div class="selectBoxHolder">
											<select class="txtbox" id="accountNumberId" name="AccountNumber" >
											    <option value=""><s:text name="jsp.default_375" /></option>
											    <ffi:setProperty name="Compare" property="Value1" value="${AccountNumber}" />
											    <ffi:list collection="BankingAccounts" items="BAccount">
												<ffi:setProperty name="Compare" property="Value2" value="${BAccount.ID}" />
												<option value="<ffi:getProperty name="BAccount" property="ID"/>" <ffi:getProperty name="selected${Compare.Equals}"/>
												><ffi:getProperty name="BAccount" property="ConsumerMenuDisplayText"/></option>
											    </ffi:list>
											</select>
											</div>
											<script type="text/javascript">
												$("#accountNumberId").combobox({'size':'30'});
											</script>
											<br><br><span id="AccountNumberError"></span>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</td>
				<td width="3%">&nbsp;</td>
				<td align="left" class="adminBackground" width="67%" valign="top">
					<div class="paneWrapper">
				   	<div class="paneInnerWrapper">
						<div class="header">
							<s:text name="jsp.servicecenter_statement_details" />
						</div>
						<div class="paneContentWrapper">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="50%"><span><s:text name="jsp.servicecenter_beginning_date" /><s:text name="jsp.default_colon" /></span><span class="required" >*</span></td>
							<td width="50%"><s:text name="jsp.servicecenter_ending_date" /><s:text name="jsp.default_colon" /></span><span class="required" >*</span></td>
						</tr>
						<tr>
							<td>
								<sj:datepicker title="%{getText('Choose_start_date')}" value="" id="BeginDateID" name="BeginDate" label="Beginning Date" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
								<script>
									var tmpUrl1 = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=orderStatementForm&calTarget=SendOrderStatement.BeginDate"/>';
					                  ns.common.enableAjaxDatepicker("BeginDate", tmpUrl1);
								</script>
							</td>
							<td>
								<sj:datepicker title="%{getText('Choose_to_date')}" value="" id="EndDateID" name="EndDate" label="Ending Date" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
								<script>
									var tmpUrl2 = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=orderStatementForm&calTarget=SendOrderStatement.EndDate"/>';
					                  ns.common.enableAjaxDatepicker("EndDate", tmpUrl2);
								</script>
							</td>
						</tr>
						<tr>
							<td><span id="BeginDateError"></span></td>
							<td> <span id="EndDateError"></span></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong><s:text name="jsp.servicecenter_delivery_method" />: </span></strong>
								<span class="required">*</span>
							</td>
						</tr>
						<tr>
						<td><input name="DeliveryMethod" type="radio" value="<%= ServiceCenterTask.ADDRESS_OF_RECORD %>" checked="true" >
							&nbsp; <span><s:text name="jsp.servicecenter_send_to_address_of_record" /></span>
						</td>
						<td>
							 <ffi:setProperty name="Compare" property="Value1" value="${SendOrderStatement.DeliveryMethod}" />
			   				 <ffi:setProperty name="Compare" property="Value2" value="<%= String.valueOf ( ServiceCenterTask.FAX ) %>" />
			   				
			   				 <input name="DeliveryMethod" type="radio" value="<%= ServiceCenterTask.FAX %>" <ffi:getProperty name="checked${Compare.Equals}" /> >
			   				 
			   				 <span class="sectionsubhead ltrow2_color"><s:text
								name="jsp.servicecenter_fax_to_this_number" /> <s:text
								name="jsp.default_colon" /></span>
						</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								 <input name="FaxNumber" type="text" size="20" maxlength="25" border="0" class="txtbox ui-widget-content ui-corner-all" value='<ffi:getProperty name="SendOrderStatement" property="FaxNumber"/>' onfocus="document.forms['orderStatementForm'].elements['SendOrderStatement.DeliveryMethod'][1].checked = true;">
								<span id="FaxNumberError"></span>
							</td>
						</tr>
						<tr>
							<td colpsan="2">&nbsp;</td>
						</tr>
						<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
				        <tr>
				            <td align="center" colspan="2" class="required">* <s:text name="jsp.default_240" /></td>
				        </tr>
				        <tr>
						    <td colspan="2" align="center">
								<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82" /></sj:a>
								<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
										<sj:a  id="orderStatementFormSubmit"
										formIds="orderStatementForm"
										targets="resultmessage" button="true" validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeConfirmOrderStatementForm"
										onCompleteTopics="confirmOrderStatementComplete"
										onErrorTopics="errorConfirmOrderStatementForm"
										onSuccessTopics="successConfirmOrderStatementForm">
										<s:text name="jsp.servicecenter_send_order" /></sj:a>
								</ffi:cinclude>
						    </td>
						</tr>
						<tr>
							<td colpsan="2">&nbsp;</td>
						</tr>
				    	</ffi:cinclude>
					</table>
					</div>
					</div>
					</div>
				</td>
			</tr>
			</ffi:cinclude>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			
			</table>				
	    </table>
	</s:form>
</div>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
