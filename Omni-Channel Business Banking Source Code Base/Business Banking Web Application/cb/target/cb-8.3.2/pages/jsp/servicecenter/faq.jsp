<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<ffi:help id="serviceCenter_faq" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<title><ffi:getProperty name="PageTitle" /></title>


<script type="text/javascript"
	src="<s:url value='/web/js/servicecenter/jquery.smooth-scroll%{#session.minVersion}.js'/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('a.scrollsomething').click(function(event) {
			event.preventDefault();
			var divOffset = $('#notes').offset().top;
			var $target = $(this.hash);
			var pOffset = $target.offset().top;
			var pScroll = pOffset - divOffset-20;
			$('#notes').animate({
				scrollTop : '+=' + pScroll + 'px'
			}, 1000);
		});
		
		
		//toggle list
		$('.sc_question_link').click(function(event) {
			$sc_question_link = $(this);
			$(this).parent().toggleClass('questionListDown','questionList');
		    $contentDiv = $sc_question_link.next();
		    $contentDiv.slideToggle(500);
		});
	});
</script>

</head>

<body style="overflow: auto">
	<div id="top">
		<div id="scrollable" class="scrollme">
			<div id="faqLinkHolder" class="paddingLeft10">
				<%-- <ul class="portlet-header">
					<li>
						<a id="faqLinks" href="#faqLinkHolder"><s:text
								name="jsp.servicecenter_choose_a_category" /> 
						</a>
					</li>
				</ul> --%>
				<div>
				<table width="100%" border="0" cellspacing="0" cellpadding="3" class="renderFaqLinkBullets">
					<tr>
						<td align="left" valign="top" class="filter_table" width="14%"><span
							class="sectionhead ltrow2_color"> <a
								href="#gettingStartedTitle" id="gettingstarted" class="scrollsomething anchorText"><s:text
										name="jsp.servicecenter_getting_started" /> </a> </span>
						</td>
						<td align="left" valign="top" class="filter_table" width="13%"><span
							class="sectionhead ltrow2_color"> <a
								href="#accountAccessTitle" id="accountAccess" class="scrollsomething anchorText"><s:text
										name="jsp.servicecenter_account_access" /> </a> </span>
						</td>
						<td align="left" valign="top" class="filter_table" width="15%"><span
							class="sectionhead ltrow2_color"> <a
								href="#transferringFundsTitle" id="transferring_funds"  class="scrollsomething anchorText"><s:text
										name="jsp.servicecenter_transferring_funds" /> </a> </span>
						</td>
						<td align="left" valign="top" class="filter_table" width="10%"><span
							class="sectionhead ltrow2_color"> <a
								href="#billPayTitle" id="billPay" class="scrollsomething anchorText"><s:text
										name="jsp.default_74" /> </a> </span>
						</td>
						<td align="left" valign="top" class="filter_table" width="12%"><span
							class="sectionhead ltrow2_color"> <a
								href="#selfServiceTitle" id="selfService" class="scrollsomething anchorText"><s:text
										name="jsp.servicecenter_self_service" /> </a> </span>
						</td>
						<td align="left" valign="top" class="filter_table" width="21%"><span
							class="sectionhead ltrow2_color"> <a
								href="#quickenMicroTitle" id="mMoney" class="scrollsomething anchorText"><s:text
										name="jsp.servicecenter_quicken_and_microsoft_money" /> </a> </span>
						</td>
						<td align="left" valign="top" class="filter_table" width="15%"><span
							class="sectionhead ltrow2_color"> <a
								href="#techSecTitle" id="techSecurity" class="scrollsomething anchorText"><s:text
										name="jsp.servicecenter_technical_and_security" /> </a> </span>
						</td>
					</tr>
				</table>	

			</div>
			</div>
			<div class="filter_table marginTop20 tableData columnHeading0" style="text-align: left">
				<div class="sectionhead transactionHeading marginBottom20">
					<a name="gettingStartedTitle" id="gettingStartedTitle"></a>
					<s:text name="jsp.servicecenter_getting_started" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="gettingStarted1_img" name="gettingStarted1_img"><span
							class="sc_question_link"> <s:text
									name="jsp.servicecenter_faq_q1" /> </span>
							<div class="sc_question" id="gettingStarted1"
								name="gettingStarted1">
								<p>
								<ul style="list-style-image: none;">
									<li><s:text name="jsp.servicecenter_faq_a11" /></li>
									<li><s:text name="jsp.servicecenter_faq_a12" /></li>
									<li><s:text name="jsp.servicecenter_faq_a13" /></li>
									<li><s:text name="jsp.servicecenter_faq_a14" /></li>
									<li><s:text name="jsp.servicecenter_faq_a15" /></li>
									<li><s:text name="jsp.servicecenter_faq_a16" /></li>
									<li><s:text name="jsp.servicecenter_faq_a17" /></li>
									<li><s:text name="jsp.servicecenter_faq_a18" /></li>
									<li><s:text name="jsp.servicecenter_faq_a19" /></li>
								</ul>
								</p>
							</div>
						</li>
						<li id="gettingStarted2_img" name="gettingStarted2_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q2" /> </span>
							<div class="sc_question" id="gettingStarted2"
								name="gettingStarted2">
								<p>
								<ul style="list-style-image: none;">
									<li><s:text name="jsp.servicecenter_faq_a21" /></li>
									<li><s:text name="jsp.servicecenter_faq_a22" /></li>
									<li><s:text name="jsp.servicecenter_faq_a23" /></li>
								</ul>
								</p>
							</div>
						</li>
						<li id="gettingStarted3_img" name="gettingStarted3_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q3" /> </span>
							<div class="sc_question" id="gettingStarted3"
								name="gettingStarted3">
								<p>
									<s:text name="jsp.servicecenter_faq_a31" />
								</p>
							</div>
						</li>
						<li id="gettingStarted4_img" name="gettingStarted4_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q4" /> </span>
							<div class="sc_question" id="gettingStarted4"
								name="gettingStarted4">
								<p>
									<s:text name="jsp.servicecenter_faq_a41" />
								</p>
							</div>
						</li>
						<li id="gettingStarted5_img" name="gettingStarted5_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q5" /> </span>
							<div class="sc_question" id="gettingStarted5"
								name="gettingStarted5">
								<p>
									<s:text name="jsp.servicecenter_faq_a51" />
								</p>
							</div>
						</li>
						<li id="gettingStarted6_img" name="gettingStarted6_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q6" /> </span>
							<div class="sc_question" id="gettingStarted6"
								name="gettingStarted6">
								<p>
									<s:text name="jsp.servicecenter_faq_a61" />
								</p>
							</div>
						</li>
						<li id="gettingStarted7_img" name="gettingStarted7_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q7" /> </span>
							<div class="sc_question" id="gettingStarted7"
								name="gettingStarted7">
								<p>
									<s:text name="jsp.servicecenter_faq_a71" />
								</p>
								<p>
									<s:text name="jsp.servicecenter_faq_a72" />
								</p>
								<p>
								<ul style="list-style-image: none;">
									<li><s:text name="jsp.servicecenter_faq_a721" /></li>
									<li><s:text name="jsp.servicecenter_faq_a722" /></li>
									<li><s:text name="jsp.servicecenter_faq_a723" /></li>
								</ul>
								</p>
								<p>
									<s:text name="jsp.servicecenter_faq_a724" />
								</p>
							</div>
						</li>
						<li id="gettingStarted8_img" name="gettingStarted8_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q8" /> </span>
							<div class="sc_question" id="gettingStarted8"
								name="gettingStarted8">
								<p>
									<s:text name="jsp.servicecenter_faq_a81" />
								</p>
							</div>
						</li>
						<li id="gettingStarted9_img" name="gettingStarted9_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q9" /> </span>
							<div class="sc_question" id="gettingStarted9"
								name="gettingStarted9">
								<p>
									<s:text name="jsp.servicecenter_faq_a91" />

									<span class="sectionsubhead ltrow2_color"> <s:url
											id="bankURL" value="http://www.fusionbank.com"
											escapeAmp="false" /> <s:a href="%{bankURL}" target="_blank">
											<s:text name="jsp.default_fusion_bank" />
										</s:a> </span>
									<s:text name="jsp.servicecenter_faq_a92" />
								</p>
							</div>
						</li>
						<li id="gettingStarted10_img" name="gettingStarted10_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q10" /> </span>
							<div class="sc_question" id="gettingStarted10"
								name="gettingStarted10">
								<p>
									<s:text name="jsp.servicecenter_faq_a10" />
								</p>
							</div>
						</li>
						<li id="gettingStarted11_img" name="gettingStarted11_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q11" /> </span>
							<div class="sc_question" id="gettingStarted11"
								name="gettingStarted11">
								<p>
									<s:text name="jsp.servicecenter_faq_a111" />
								</p>
							</div>
						</li>
					</ul>
				</div>

				<div class="sectionhead transactionHeading marginBottom20">
					<a name="accountAccessTitle" id="accountAccessTitle"></a>
					<s:text name="jsp.servicecenter_account_access" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="accountAccess1_img" name="accountAccess1_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q12" /> </span>
							<div class="sc_question" id="accountAccess1"
								name="accountAccess1">
								<p>
									<s:text name="jsp.servicecenter_faq_a121" />
								</p>
							</div>
						</li>
						<li id="accountAccess2_img" name="accountAccess2_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q13" /> </span>
							<div class="sc_question" id="accountAccess2"
								name="accountAccess2">
								<p>
									<s:text name="jsp.servicecenter_faq_a131" />
								</p>
							</div>
						</li>
						<li id="accountAccess3_img" name="accountAccess3_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q14" /> </span>
							<div class="sc_question" id="accountAccess3"
								name="accountAccess3">
								<p>
									<s:text name="jsp.servicecenter_faq_a141" />
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="sectionhead transactionHeading marginBottom20">
					<a name="transferringFundsTitle" id="transferringFundsTitle"></a>
					<s:text name="jsp.servicecenter_transferring_funds" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="transferringFunds1_img" name="transferringFunds1_img">
							<span class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q15" /> </span>
							<div class="sc_question" id="transferringFunds1"
								name="transferringFunds1">
								<p>
									<s:text name="jsp.servicecenter_faq_a151" />
								</p>
							</div>
						</li>
						<li id="transferringFunds2_img" name="transferringFunds2_img">
							<span class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q16" /> </span>
							<div class="sc_question" id="transferringFunds2"
								name="transferringFunds2">
								<p>
									<s:text name="jsp.servicecenter_faq_a161" />
								</p>
							</div>
						</li>
						<li id="transferringFunds3_img" name="transferringFunds3_img">
							<span class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q17" /> </span>
							<div class="sc_question" id="transferringFunds3"
								name="transferringFunds3">
								<p>
									<s:text name="jsp.servicecenter_faq_a171" />
								</p>
							</div>
						</li>
						<li id="transferringFunds4_img" name="transferringFunds4_img">
							<span class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q18" /> </span>
							<div class="sc_question" id="transferringFunds4"
								name="transferringFunds4">
								<p>
									<s:text name="jsp.servicecenter_faq_a181" />
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="sectionhead transactionHeading marginBottom20">
					<a name="billPayTitle" id="billPayTitle"></a>
					<s:text name="jsp.default_74" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="billPay1_img" name="billPay1_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q3" /> </span>
							<div class="sc_question" id="billPay1" name="billPay1">
								<p>
									<s:text name="jsp.servicecenter_faq_a31" />
									</span>
								</p>
							</div>
						</li>
						<li id="billPay2_img" name="billPay2_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q19" /> </span>
							<div class="sc_question" id="billPay2" name="billPay2">
								<p>
									<s:text name="jsp.servicecenter_faq_a191" />
								</p>
							</div>
						</li>
						<li id="billPay3_img" name="billPay3_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q20" /> </span>
							<div class="sc_question" id="billPay3" name="billPay3">
								<p>
									<s:text name="jsp.servicecenter_faq_a201" />
								</p>
							</div>
						</li>
						<li id="billPay4_img" name="billPay4_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q21" /> </span>
							<div class="sc_question" id="billPay4" name="billPay4">
								<p>
									<s:text name="jsp.servicecenter_faq_a211" />
								</p>
							</div>
						</li>
						<li id="billPay5_img" name="billPay5_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q22" /> </span>
							<div class="sc_question" id="billPay5" name="billPay5">
								<p>
									<s:text name="jsp.servicecenter_faq_a221" />
								</p>
							</div>
						</li>
						<li id="billPay6_img" name="billPay6_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q23" /> </span>
							<div class="sc_question" id="billPay6" name="billPay6">
								<p>
									<s:text name="jsp.servicecenter_faq_a231" />
								</p>
							</div>
						</li>
						<li id="billPay7_img" name="billPay7_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q24" /> </span>
							<div class="sc_question" id="billPay7" name="billPay7">
								<p>
									<s:text name="jsp.servicecenter_faq_a241" />
								</p>
							</div>
						</li>
						<li id="billPay8_img" name="billPay8_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q25" /> </span>
							<div class="sc_question" id="billPay8" name="billPay8">
								<p>
									<s:text name="jsp.servicecenter_faq_a251" />
								</p>
							</div>
						</li>
						<li id="billPay9_img" name="billPay9_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q26" /> </span>
							<div class="sc_question" id="billPay9" name="billPay9">
								<p>
									<s:text name="jsp.servicecenter_faq_a261" />
								</p>
							</div>
						</li>
						<li id="billPay10_img" name="billPay10_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q27" /> </span>
							<div class="sc_question" id="billPay10" name="billPay10">
								<p>
									<s:text name="jsp.servicecenter_faq_a271" />
								</p>
							</div>
						</li>
						<li id="billPay11_img" name="billPay11_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q28" /> </span>
							<div class="sc_question" id="billPay11" name="billPay11">
								<p>
									<s:text name="jsp.servicecenter_faq_a281" />
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="sectionhead transactionHeading marginBottom20">
					<a name="selfServiceTitle" id="selfServiceTitle"></a>
					<s:text name="jsp.servicecenter_self_service" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="selfService1_img" name="selfService1_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q29" /> </span>
							<div class="sc_question" id="selfService1" name="selfService1">
								<p>
									<s:text name="jsp.servicecenter_faq_a291" />
								</p>
								<p>
								<ul style="list-style-image: none;">
									<li><s:text name="jsp.servicecenter_faq_a292" /></li>
									<li><s:text name="jsp.servicecenter_faq_a293" /></li>
									<li><s:text name="jsp.servicecenter_faq_a294" /></li>
									<li><s:text name="jsp.servicecenter_faq_a295" /></li>
									<li><s:text name="jsp.servicecenter_faq_a296" /></li>
									<li><s:text name="jsp.servicecenter_faq_a297" /></li>
									<li><s:text name="jsp.servicecenter_faq_a298" /></li>
									<li><s:text name="jsp.servicecenter_faq_a299" /></li>
								</ul>
								</p>
							</div>
						</li>
						<li id="selfService2_img" name="selfService2_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q30" /> </span>
							<div class="sc_question" id="selfService2" name="selfService2">
								<p>
									<s:text name="jsp.servicecenter_faq_a301" />
								</p>
							</div>
						</li>
						<li id="selfService3_img" name="selfService3_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q31" /> </span>
							<div class="sc_question" id="selfService3" name="selfService3">
								<p>
									<s:text name="jsp.servicecenter_faq_a311" />
								</p>
							</div>
						</li>
						<li id="selfService4_img" name="selfService4_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q32" /> </span>
							<div class="sc_question" id="selfService4" name="selfService4">
								<p>
									<s:text name="jsp.servicecenter_faq_a321" />
								</p>
							</div>
						</li>
						<li id="selfService5_img" name="selfService5_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q33" /> </span>
							<div class="sc_question" id="selfService5" name="selfService5">
								<p>
									<s:text name="jsp.servicecenter_faq_a331" />
								</p>
							</div>
						</li>
						<li id="selfService6_img" name="selfService6_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q34" /> </span>
							<div class="sc_question" id="selfService6" name="selfService6">
								<p>
									<s:text name="jsp.servicecenter_faq_a341" />
								</p>
							</div>
						</li>
						<li id="selfService7_img" name="selfService7_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q35" /> </span>
							<div class="sc_question" id="selfService7" name="selfService7">
								<p>
									<s:text name="jsp.servicecenter_faq_a351" />
								</p>
							</div>
						</li>
						<li id="selfService8_img" name="selfService8_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q36" /> </span>
							<div class="sc_question" id="selfService8" name="selfService8">
								<p>
									<s:text name="jsp.servicecenter_faq_a361" />
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="sectionhead transactionHeading marginBottom20">
					<a name="quickenMicroTitle" id="quickenMicroTitle"></a>
					<s:text name="jsp.servicecenter_quicken_and_microsoft_money" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="quickenMicro1_img" name="quickenMicro1_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q5" /> </span>
							<div class="sc_question" id="quickenMicro1" name="quickenMicro1">
								<p>
									<s:text name="jsp.servicecenter_faq_a52" />
								</p>
							</div>
						</li>
						<li id="quickenMicro2_img" name="quickenMicro2_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q37" /> </span>
							<div class="sc_question" id="quickenMicro2" name="quickenMicro2">
								<p>
									<s:text name="jsp.servicecenter_faq_a371" />
								</p>
								<p>
									<s:text name="jsp.servicecenter_faq_a372" />
								</p>
								<p>
								<ol style="list-style-image: none;">
									<li><s:text name="jsp.servicecenter_faq_a373" /></li>
									<li><s:text name="jsp.servicecenter_faq_a374" /></li>
									<li><s:text name="jsp.servicecenter_faq_a375" />
										<ul style="list-style-image: none;">
											<li><s:text name="jsp.servicecenter_faq_a376" /></li>
											<li><s:text name="jsp.servicecenter_faq_a377" /></li>
										</ul>
									</li>
									<li><s:text name="jsp.servicecenter_faq_a378" /></li>
									<li><s:text name="jsp.servicecenter_faq_a379" /></li>
									<li><s:text name="jsp.servicecenter_faq_a3710" /></li>
									<li><s:text name="jsp.servicecenter_faq_a3711" /></li>
									<li><s:text name="jsp.servicecenter_faq_a3712" /></li>
								</ol>
								</p>
							</div>
						</li>
						<li id="quickenMicro3_img" name="quickenMicro3_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q38" /> </span>
							<div class="sc_question" id="quickenMicro3" name="quickenMicro3">
								<p>
									<s:text name="jsp.servicecenter_faq_a381" />
								</p>
							</div>
						</li>
						<li id="quickenMicro4_img" name="quickenMicro4_img"><span
							class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q39" /> </span>
							<div class="sc_question" id="quickenMicro4" name="quickenMicro4">
								<p>
									<s:text name="jsp.servicecenter_faq_a391" />
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="sectionhead transactionHeading marginBottom20">
					<a name="techSecTitle" id="techSecTitle"></a>
					<s:text name="jsp.servicecenter_technical_and_security" />
				</div>
				<div class="txt_normal">
					<ul class="questionList">
						<li id="techSec1_img" name="techSec1_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q10" /> </span>
							<div class="sc_question" id="techSec1" name="techSec1">
								<p>
									<s:text name="jsp.servicecenter_faq_a10" />
								</p>
							</div>
						</li>
						<li id="techSec2_img" name="techSec2_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q11" /> </span>
							<div class="sc_question" id="techSec2" name="techSec2">
								<p>
									<s:text name="jsp.servicecenter_faq_a111" />
								</p>
							</div>
						</li>
						<li id="techSec3_img" name="techSec3_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q40" /> </span>
							<div class="sc_question" id="techSec3" name="techSec3">
								<p>
									<s:text name="jsp.servicecenter_faq_a401" />
								</p>
							</div>
						</li>
						<li id="techSec4_img" name="techSec4_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q41" /> </span>
							<div class="sc_question" id="techSec4" name="techSec4">
								<p>
									<s:text name="jsp.servicecenter_faq_a411" />
								</p>
							</div>
						</li>
						<li id="techSec5_img" name="techSec5_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q42" /> </span>
							<div class="sc_question" id="techSec5" name="techSec5">
								<p>
									<s:text name="jsp.servicecenter_faq_a421" />
								</p>
							</div>
						</li>
						<li id="techSec6_img" name="techSec6_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q43" /> </span>
							<div class="sc_question" id="techSec6" name="techSec6">
								<p>
									<s:text name="jsp.servicecenter_faq_a431" />
									<span class="sectionsubhead ltrow2_color"><a
										href="#browsers" class="scrollsomething"><s:text
												name="jsp.servicecenter_faq_recommended_internet_browser" />
									</a>
									</span>.
								</p>
							</div>
						</li>
						<li id="techSec7_img" name="techSec7_img"><a name="browsers"
							id="browsers"></a> <span class="sc_question_link"><s:text
									name="jsp.servicecenter_faq_q44" /> </span>
							<div class="sc_question" id="techSec7" name="techSec7">
								<p>
									<s:text name="jsp.servicecenter_faq_a441" />
								</p>
							</div>
						</li>
						<li id="techSec8_img" name="techSec8_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q45" /> </span>
							<div class="sc_question" id="techSec8" name="techSec8">
								<p>
									<s:text name="jsp.servicecenter_faq_a451" />
								</p>
								<p>
								<ul style="list-style-image: none;">
									<li><span class="sectionsubhead ltrow2_color"><s:text
												name="jsp.servicecenter_faq_a452" /> </span> <s:text
											name="jsp.servicecenter_faq_a453" /></li>
									<li><span class="sectionsubhead ltrow2_color"> <s:text
												name="jsp.servicecenter_faq_a454" /> </span> <s:text
											name="jsp.servicecenter_faq_a455" /></li>
									<li><span class="sectionsubhead ltrow2_color"><s:text
												name="jsp.servicecenter_faq_a456" /> </span> <s:text
											name="jsp.servicecenter_faq_a457" /></li>
									<li><span class="sectionsubhead ltrow2_color"><s:text
												name="jsp.servicecenter_faq_a458" /> </span> <s:text
											name="jsp.servicecenter_faq_a459" /></li>
								</ul>
								</p>
							</div>
						</li>
						<li id="techSec9_img" name="techSec9_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q46" /> </span>
							<div class="sc_question" id="techSec9" name="techSec9">
								<p>
									<s:text name="jsp.servicecenter_faq_a461" />
								</p>
							</div>
						</li>
						<li id="techSec10_img" name="techSec10_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q47" /> </span>
							<div class="sc_question" id="techSec10" name="techSec10">
								<p>
									<s:text name="jsp.servicecenter_faq_a431" />
									<span class="sectionsubhead ltrow2_color"><a
										href="#browsers" class="scrollsomething"><s:text
												name="jsp.servicecenter_faq_recommended_internet_browser" />
									</a>
									</span>
									<s:text name="jsp.servicecenter_faq_a471" />
								</p>
							</div>
						</li>
						<li id="techSec11_img" name="techSec11_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q48" /> </span>
							<div class="sc_question" id="techSec11" name="techSec11">
								<p>
									<s:text name="jsp.servicecenter_faq_a481" />
								</p>
							</div>
						</li>
						<li id="techSec12_img" name="techSec12_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q49" /> </span>
							<div class="sc_question" id="techSec12" name="techSec12">
								<p>
									<s:text name="jsp.servicecenter_faq_a491" />
									<span class="sectionsubhead ltrow2_color"><s:text
											name="jsp.servicecenter_faq_encryption" />
									</span>
									<s:text name="jsp.servicecenter_faq_a492" />
								</p>
								<p>
									<s:text name="jsp.servicecenter_faq_a493" />
									<span class="sectionsubhead ltrow2_color"><s:text
											name="jsp.servicecenter_faq_internal_bank_network" />
									</span>.
									<s:text name="jsp.servicecenter_faq_a494" />
								</p>
								<p>
									<s:text name="jsp.servicecenter_faq_a495" />
									<span class="sectionsubhead ltrow2_color"><s:text
											name="jsp.servicecenter_faq_own_responsibilities" />
									</span>
									<s:text name="jsp.servicecenter_faq_a496" />
								</p>
							</div>
						</li>
						<li id="techSec13_img" name="techSec13_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q50" /> </span>
							<div class="sc_question" id="techSec13" name="techSec13">
								<p>
									<s:text name="jsp.servicecenter_faq_a501" />
								</p>
								<p>
									<s:text name="jsp.servicecenter_faq_a502" />
								</p>
							</div>
						</li>
						<li id="techSec14_img" name="techSec14_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q51" /> </span>
							<div class="sc_question" id="techSec14" name="techSec14">
								<p>
									<s:text name="jsp.servicecenter_faq_a511" />
								</p>
							</div>
						</li>
						<li id="techSec15_img" name="techSec15_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q52" /> </span>
							<div class="sc_question" id="techSec15" name="techSec15">
								<p>
									<s:text name="jsp.servicecenter_faq_a521" />
								</p>
							</div>
						</li>
						<li id="techSec16_img" name="techSec16_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q53" /> </span>
							<div class="sc_question" id="techSec16" name="techSec16">
								<p>
									<s:text name="jsp.servicecenter_faq_a531" />
									<span class="sectionsubhead ltrow2_color"><s:text
											name="jsp.servicecenter_faq_high" />
									</span>
									<s:text name="jsp.servicecenter_faq_and" />
									<span class="sectionsubhead ltrow2_color"><s:text
											name="jsp.servicecenter_faq_standard" />
									</span>.
									<s:text name="jsp.servicecenter_faq_a532" />
								</p>
							</div>
						</li>
						<li id="techSec17_img" name="techSec17_img"><span
							class="sc_question_link">
								<s:text name="jsp.servicecenter_faq_q54" /> </span>
							<div class="sc_question" id="techSec17" name="techSec17">
								<p>
									<s:text name="jsp.servicecenter_faq_a541" />
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div
					style="padding-top: 10px; padding-bottom: 10px; text-align: right">
					<a href="#top" class="scrollsomething"><img
						src="/cb/web/multilang/grafx/i_moveup.gif" width="14" height="14"
						border="0" align="absmiddle" style="padding-right: 5px"><span
						class="sectionsubhead ltrow2_color"><s:text
								name="jsp.servicecenter_back_to_top" /></span>
					</a>
				</div>
			</div>
		</div>
		<p></p>
	</div>
</body>

</html>