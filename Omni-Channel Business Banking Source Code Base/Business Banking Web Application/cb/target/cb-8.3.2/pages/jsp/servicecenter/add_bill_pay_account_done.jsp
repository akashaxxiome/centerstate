<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setL10NProperty name='PageHeading' value='Bill Pay Account'/>
<ffi:setL10NProperty name="SCPageTitle" value="Add Bill Pay To An Account" />
<ffi:setProperty name="messageKey" value="jsp/servicecenter/add_bill_pay_account_done.jsp-1" />
<ffi:setProperty name="parameter0" value="${SelectedAccount.ConsumerDisplayText}" />

<s:include value="/pages/jsp/servicecenter/done.jsp"/>


<ffi:removeProperty name="AddBillPayAccount" />
<ffi:removeProperty name="SelectedAccount" />
<ffi:removeProperty name="AccountNumber" />
<ffi:removeProperty name="NonBillPayAccounts" />
