<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<script type="text/javascript" src="<s:url value='/web/js/servicecenter/servicecenter%{#session.minVersion}.js'/>"></script>

<%
	String isSummaryCSSVisible = "visible";
%>

<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="notEquals">
	<%
		isSummaryCSSVisible = "hidden";
	%>
</ffi:cinclude>

<div id="desktop" align="center">

		<div id="appdashboard">
			<s:include value="serviceCenter_dashboard.jsp"/> 
		</div>
		<div id="operationresult">
			<div id="resultmessage"><s:text name="jsp.default_498"/></div>
		</div><!-- result -->
		
		<div id="summary" class="<%= isSummaryCSSVisible %>">
				<s:include value="serviceCenter_summary.jsp"/> 
		</div>

		<div id="details" style="display:none;">
			<s:include value="/pages/jsp/servicecenter/inc/servicecenter_details.jsp"/>
		</div>
		
</div>

<script type="text/javascript">
	$(document).ready(function(){
		ns.servicecenter.showServiceCenterDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>