<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/account_history_inquiry_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>


<ffi:removeProperty name="AccountHistoryInquiry" />
<ffi:removeProperty name="BackURLOld" />
<ffi:removeProperty name="TransactionTypesList" />
<ffi:removeProperty name="AccountForAccountHistoryInquiry" />
<ffi:removeProperty name="InquiryMessage" />
<ffi:removeProperty name="SelectedAccount" />
