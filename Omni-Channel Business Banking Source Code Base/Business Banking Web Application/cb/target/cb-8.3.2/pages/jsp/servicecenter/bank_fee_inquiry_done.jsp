<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/bank_fee_inquiry_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>


<ffi:removeProperty name="BankFeeInquiry" />
<ffi:removeProperty name="AccountForBankFee" />
<ffi:removeProperty name="SelectedAccount" />
<ffi:removeProperty name="InquiryMessage" />
