<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/transfer_inquiry_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>

<ffi:removeProperty name="TransferInquiry" />
<ffi:removeProperty name="TransferInquiryToAccount" />
<ffi:removeProperty name="TransferInquiryFromAccount" />
<ffi:removeProperty name="BackURLOld" />
<ffi:removeProperty name="InquiryMessage" />
