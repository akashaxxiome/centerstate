<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="servicecenter_services_onlineCustomerServices_transferInq" />
<%-- clean up --%>
<ffi:removeProperty name="TransferInquiry" />

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries" name="com.ffusion.tasks.banking.GetSummariesForAccountDate" scope="session"/>
<ffi:setProperty name="GetAccountSummaries" property="UpdateAccountBalances" value="true"/>
<ffi:setProperty name="GetAccountSummaries" property="SummariesName" value="LatestSummaries"/>
<ffi:process name="GetAccountSummaries"/>
<ffi:removeProperty name="GetAccountSummaries"/>
<ffi:removeProperty name="LatestSummaries"/>

<%-- load Transfer accounts into session --%>
<ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetConsumerTransferAccounts" scope="request"/>

<%-- check if this is a secondary user --%>
<ffi:setProperty name="GetTransferAccounts" property="SourceAccounts" value="BankingAccounts" />

<ffi:setProperty name="GetTransferAccounts" property="DestinationToAccounts" value="TransferToAccounts" />
<ffi:setProperty name="GetTransferAccounts" property="DestinationFromAccounts" value="TransferFromAccounts" />
<ffi:process name="GetTransferAccounts" />
<ffi:removeProperty name="GetTransferAccounts" />


<ffi:cinclude value1="${TransferInquiry}" value2="" operator="equals" >
    <ffi:object id="TransferInquiry" name="com.ffusion.tasks.messages.SendFundsTransactionMessage" scope="session"/>
  	<ffi:setProperty name="TransferInquiry" property="TransactionType" value="<%= Integer.toString( com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_TRANSFER) %>" />
    <ffi:setProperty name="TransferInquiry" property="Process" value="false" />
    <ffi:setProperty name="TransferInquiry" property="IsConsumer" value="true" />
   	<ffi:setProperty name="TransferInquiry" property="FromType" value="1" />
	<ffi:setProperty name="TransferInquiry" property="Subject" value="Service Center Request: Transfer Inquiry" />
	<ffi:setProperty name="TransferInquiry" property="TransferDate" value="${UserLocale.DateFormat}" />
	<ffi:setProperty name="TransferInquiry" property="DateFormat" value="${UserLocale.DateFormat}" />

    <% String question = null; %>
    <ffi:getL10NString rsrcFile="cb" msgKey="jsp/servicecenter/inquiry.jsp-1" assignTo="question" />
    <ffi:setProperty name="InquiryMessage" value="<%= question %>" />
</ffi:cinclude>

<%
session.setAttribute("FFITransferInquiry", session.getAttribute("TransferInquiry"));
%>
<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />
<script language="javascript">
<!--

$(function(){
	reload();
	$("#FromAccount").combobox({'size':'30'});
	$("#ToAccount").combobox({'size':'30'});
});

function reload()
{
	fromSel = document.getElementById("FromAccount");
	toSel = document.getElementById("ToAccount");

    reloadFromToAccounts(fromSel, toSel);
}

//--></script>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_transfer_inquiry" /></span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,transferInquiryLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<div>
<ul id="formerrors"></ul>
<s:form id="transferInquiryForm" namespace="/pages/jsp/servicecenter" validate="true" action="transferInquiryAction" method="post" name="transferInquiryForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<div style="padding-left:10px;padding-right:10px;">
	    <table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">
	    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0" />
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="equals">
			<tr>
				<td colspan="2" class="columndata ltrow2_color" align="center">
				    <ffi:getL10NString rsrcFile="cb" msgKey="jsp/servicecenter/transfer_inquiry.jsp-1" />
				</td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" >
					<span class="sectionsubhead"><s:text name="jsp.default_219" /></span><span class="required" >*</span>
			    </td>
			    <td align="left" class="columndata ltrow2_color" width="75%" >
		            <ffi:setProperty name="Compare" property="Value1" value="${TransferInquiry.FromAccountID}" />
		            
		            <div class="selectBoxHolder">
			            <select class="txtbox" name="FromAccountID" id="FromAccount" size="1" onchange="reload()">
			                <option value=""><s:text name="jsp.default_375" /></option>
			                <ffi:setProperty name="TransferFromAccounts" property="Filter" value="CoreAccount=1,HIDE=0,AND" />
			                <ffi:list collection="TransferFromAccounts" items="Account">
			                    <option value="<ffi:getProperty name="Account" property="ID"/>"
			                        <ffi:cinclude value1="${TransferInquiry.FromAccountID}" value2="${Account.ID}" operator="equals">selected</ffi:cinclude>
			                    ><ffi:getProperty name="Account" property="ConsumerMenuDisplayText"/></option>
			                </ffi:list>
			                <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
			                    <ffi:setProperty name="TransferFromAccounts" property="Filter" value="ExternalTransferFrom,CoreAccount!1,HIDE=0,ExternalTransferACCOUNT.StatusValue=1,AND" />
			                    <ffi:cinclude value1="${TransferFromAccounts.Size}" value2="0" operator="notEquals">
	                                <option value=""><s:text name="jsp.default_8" /></option>
			                        <ffi:list collection="TransferFromAccounts" items="BAccount1" >
			                            <option value="<ffi:getProperty name="BAccount1" property="ID"/>"
				                            <ffi:cinclude value1="${TransferInquiry.FromAccountID}" value2="${BAccount1.ID}">selected</ffi:cinclude>
				                            ><ffi:getProperty name="BAccount1" property="ConsumerMenuDisplayText"/></option>
			                        </ffi:list>
			                    </ffi:cinclude>
			                </ffi:cinclude>
			            </select>
			           </div>
		           <br><br> <span id="FromAccountIDError"></span>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" >
					<span class="sectionsubhead"><s:text name="jsp.default_425" /></span><span class="required" >*</span>
			    </td>
			    <td align="left" class="columndata ltrow2_color" width="75%" >
		            <ffi:setProperty name="Compare" property="Value1" value="${TransferInquiry.ToAccountID}" />
			        <div class="selectBoxHolder">
			            <select class="txtbox" name="ToAccountID" id="ToAccount" size="1"  onchange="reload()">
			                <option value=""><s:text name="jsp.default_375" /></option>
			                <ffi:setProperty name="TransferToAccounts" property="Filter" value="CoreAccount=1,HIDE=0,AND" />
			                <ffi:list collection="TransferToAccounts" items="Account">
			                    <option value="<ffi:getProperty name="Account" property="ID"/>"
			                        <ffi:cinclude value1="${TransferInquiry.ToAccountID}" value2="${Account.ID}" operator="equals">selected</ffi:cinclude>
			                    ><ffi:getProperty name="Account" property="ConsumerMenuDisplayText"/></option>
			                </ffi:list>
			                <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
			                    <ffi:setProperty name="TransferToAccounts" property="Filter" value="ExternalTransferTo,CoreAccount!1,HIDE=0,ExternalTransferACCOUNT.StatusValue=1,AND" />
			                    <ffi:cinclude value1="${TransferToAccounts.Size}" value2="0" operator="notEquals">
	                                <option value=""><s:text name="jsp.default_8" /></option>
			                        <ffi:list collection="TransferToAccounts" items="BAccount1" >
			                            <option value="<ffi:getProperty name="BAccount1" property="ID"/>"
				                            <ffi:cinclude value1="${TransferInquiry.ToAccountID}" value2="${BAccount1.ID}">selected
				                            </ffi:cinclude>><ffi:getProperty name="BAccount1" property="ConsumerMenuDisplayText"/></option>
			                        </ffi:list>
			                    </ffi:cinclude>
			                </ffi:cinclude>
			            </select>
			        </div>
		            <br><br><span id="ToAccountIDError"></span>
				    <script type="text/javascript">
						$('#toAccount').selectmenu({width:250});
					</script>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" ><span class="sectionsubhead"><s:text
								name="jsp.default_143" /></span><span class="required" >*</span></td>
			    <td align="left" width="75%" class="columndata ltrow2_color" >
			    	<sj:datepicker
						id="TransferDate"
						name="TransferDate"
						maxlength="10"
						buttonImageOnly="true"
						cssClass="ui-widget-content ui-corner-all" 
						cssStyle="width:127px;" />
					<br><span id="TransferDateError"></span>
					<script>
						var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=transferForm&calTarget=TransferInquiry.TransferDate"/>';
	                    ns.common.enableAjaxDatepicker("PayDate", tmpUrl);
					</script>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" ><span class="sectionsubhead"><s:text
								name="jsp.default_45" /></span><span class="required" >*</span></td>
			    <td align="left" width="75%" class="columndata ltrow2_color" >
				<input name="Amount" type="text" size="15" maxlength="25" border="0" class="txtbox ui-widget-content ui-corner-all" value='<ffi:getProperty name="TransferInquiry" property="Amount"/>'>
				<br><span id="AmountError"></span>
			    </td>
			</tr>
			<%-- <tr>
				<td align="center" colspan="2" class="sectionsubhead ltrow2_color">
					<span class="sectionsubhead" >
						<ffi:getProperty name="InquiryMessage" /><span class="required" >*</span>
					</span>
				</td>
			</tr> --%>
			<tr>
				<td style="vertical-align:top;" align="right">
					<span class="sectionsubhead" >
						<ffi:getProperty name="InquiryMessage" /><span class="required" >*</span>
					</span>
				</td>
			    <td width="75%" class="columndata ltrow2_color">
				<textarea name="Memo" id="transferMemo" type="text" cols="50" rows="5" align="left" class="txtbox ui-widget-content ui-corner-all" wrap="virtual" ><ffi:getProperty name="TransferInquiry" property="Memo" /></textarea>
				<br><span id="MemoError"></span>
			    </td>
			</tr>
		</ffi:cinclude>
		<tr>
		    <td align="left" class="sectionsubhead ltrow2_color" colspan="2" >&nbsp;</td>
		</tr>
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
	        <tr>
	            <td align="center" class="required" colspan="2">* <s:text name="jsp.default_240" /></td>
	        </tr>
	    </ffi:cinclude>
		<tr>
		    <td align="left" class="sectionsubhead ltrow2_color" colspan="2" >&nbsp;</td>
		</tr>
		<tr>
		    <td colspan="2" align="center" class="columndata ltrow2_color" >
		    	<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82" /></sj:a>
		    	<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
			    	<sj:a id="transferInquiryFormSubmit"
						formIds="transferInquiryForm"
						targets="resultmessage" button="true" validate="true"
						validateFunction="customValidation"
						onBeforeTopics="beforeConfirmTransferInquiryForm"
						onCompleteTopics="confirmTransferInquiryComplete"
						onErrorTopics="errorConfirmTransferInquiryForm"
						onSuccessTopics="successConfirmTransferInquiryForm"
						><s:text name="jsp.default_378" /></sj:a>
				</ffi:cinclude>
		    </td>
		</tr>
	    </table>
	   </div>
	</s:form>
</div>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<ffi:removeProperty name="TransferToAccounts" />
<ffi:removeProperty name="TransferFromAccounts" />
<script>
	$(document).ready(function() {
		setTimeout(function(){ns.common.renderRichTextEditor('transferMemo','500');}, 500);
	});
</script>