<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="servicecenter_services_onlineCustomerServices_billpayInq" />
<%-- clean up --%>
<ffi:removeProperty name="BillPayInquiry" />

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries"
	name="com.ffusion.tasks.banking.GetSummariesForAccountDate"
	scope="session" />
<ffi:setProperty name="GetAccountSummaries"
	property="UpdateAccountBalances" value="true" />
<ffi:setProperty name="GetAccountSummaries" property="SummariesName"
	value="LatestSummaries" />
<ffi:process name="GetAccountSummaries" />
<ffi:removeProperty name="GetAccountSummaries" />
<ffi:removeProperty name="LatestSummaries" />

<ffi:object id="BillpaySignOn" name="com.ffusion.tasks.billpay.SignOn"
	scope="session" />
<ffi:setProperty name="BillpaySignOn" property="UserName"
	value="${Business.BusinessName}" />
<ffi:setProperty name="BillpaySignOn" property="Password"
	value="${Business.BusinessCIF}" />
<ffi:process name="BillpaySignOn" />

<ffi:object id="GetBillPayAccounts"
	name="com.ffusion.tasks.billpay.GetAccounts" scope="session" />
<ffi:setProperty name="GetBillPayAccounts" property="UseAccounts"
	value="BankingAccounts" />
<ffi:process name="GetBillPayAccounts" />

<ffi:object id="GetPayees" name="com.ffusion.tasks.billpay.GetExtPayees"
	scope="session" />
<ffi:setProperty name="GetPayees" property="Reload" value="true" />
<ffi:process name="GetPayees" />
<ffi:removeProperty name="GetPayees" />

<ffi:cinclude value1="${BillPayInquiry}" value2="" operator="equals">
	<ffi:object id="BillPayInquiry"
		name="com.ffusion.tasks.messages.SendFundsTransactionMessage"
		scope="session" />
	<ffi:setProperty name="BillPayInquiry" property="TransactionType"
		value="<%= Integer.toString( com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_BILL_PAYMENT ) %>" />
	<ffi:setProperty name="BillPayInquiry" property="Process" value="false" />
	<ffi:setProperty name="BillPayInquiry" property="IsConsumer"
		value="true" />
	<ffi:setProperty name="BillPayInquiry" property="FromType" value="1" />
	<ffi:setProperty name="BillPayInquiry" property="Subject"
		value="Service Center Request: Bill Payment Inquiry" />
	<ffi:setProperty name="BillPayInquiry" property="PayDate"
		value="${UserLocale.DateFormat}" />
	<ffi:setProperty name="BillPayInquiry" property="DateFormat"
		value="${UserLocale.DateFormat}" />

	<%
		String question = null;
	%>
	<ffi:getL10NString rsrcFile="cb"
		msgKey="jsp/servicecenter/inquiry.jsp-1" assignTo="question" />
	<ffi:setProperty name="InquiryMessage" value="<%= question %>" />
</ffi:cinclude>

<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" />
<ffi:setProperty name="SetAccount" property="AccountsName"
	value="BankingAccounts" />
<ffi:setProperty name="SetAccount" property="AccountName"
	value="SelectedAccount" />

<%
	session.setAttribute("FFIBillPayInquiry",
			session.getAttribute("BillPayInquiry"));
	session.setAttribute("FFISetAccount",
			session.getAttribute("SetAccount"));
%>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_bill_payment_inquiry" /> </span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,billPaymentInquiryLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<div>
	<ul id="formerrors"></ul>
	<s:form id="billPaymentInquiryForm"
		namespace="/pages/jsp/servicecenter" validate="true"
		action="billPaymentInquiryAction" method="post"
		name="billPayInquiryForm" theme="simple">
		<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<div style="padding-left:10px;padding-right:10px;">
		<table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">
			<ffi:setProperty name="BankingAccounts" property="Filter"
				value="HIDE=0,BillPay,COREACCOUNT=1,AND" />
			<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
				operator="equals">
				<tr>
					<td colspan="2" class="txt_normal" align="center">
						<ffi:getL10NString rsrcFile="cb" msgKey="jsp/servicecenter/bill_payment_inquiry.jsp-1" />
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
				operator="notEquals">
				<tr>
					<td align="right" width="25%" valign="middle">
						<span><s:text name="jsp.default_316" /> </span><span
						class="required">*</span>
					</td>
					<td align="left" width="75%">
					<div class="selectBoxHolder">
						<select
							id="payeeID" onChange="ns.common.adjustSelectMenuWidth('payeeID')" class="txtBox" name="PayeeID">
								<option value=""><s:text name="jsp.default_375" /></option>
								<ffi:setProperty name="Compare" property="Value1"
									value="${BillPayInquiryPayee}" />
								<ffi:list collection="Payees" items="thePayee">
									<ffi:setProperty name="Compare" property="Value2"
										value="${thePayee.ID}" />
									<option value="<ffi:getProperty name="thePayee" property="ID"/>"
										<ffi:getProperty name="selected${Compare.Equals}"/>
										><ffi:cinclude value1="" value2="${thePayee.Nickname}" operator="equals"><ffi:getProperty name="thePayee" property="Name" /></ffi:cinclude><ffi:cinclude value1="" value2="${thePayee.NickName}" operator="notEquals">(<ffi:getProperty name="thePayee" property="NickName" />)</ffi:cinclude></option>
								</ffi:list>
						</select> 
						
						</div>
						<br><br><span id="PayeeIDError"></span> 
						<script type="text/javascript">
						/* $('#payeeID').selectmenu({
							width : 250
						}); */
						$("#payeeID").lookupbox({
							"source":"/cb/pages/jsp/billpay/BillPayPayeesLookupBoxAction.action",
							"controlType":"server",
							"collectionKey":"1",
							"size":"35",
							"module":"billpayPayees",
						});
					</script></td>
				</tr>
				<tr>
					<td align="right" width="25%" class="sectionsubhead ltrow2_color">
						<span><s:text name="jsp.default_21" /> </span><span
						class="required">*</span>
					</td>
					<td align="left" width="75%" class="filter_table">
						<div class="selectBoxHolder">
							<select id="payeeAccountID" class="txtBox" name="PayAccountID">
								<option value=""><s:text name="jsp.default_375" /></option>
								<ffi:setProperty name="Compare" property="Value1" value="${BillPayInquiryAccount}" />
								<ffi:list collection="BankingAccounts" items="Account">
									<ffi:setProperty name="Compare" property="Value2" value="${Account.ID}" />
									<option value="<ffi:getProperty name="Account" property="ID"/>"
										<ffi:getProperty name="selected${Compare.Equals}"/>
										><ffi:getProperty name="Account" property="ConsumerMenuDisplayText" /></option>
								</ffi:list>
							</select>
						</div>
						<br><br><span id="PayAccountIDError"></span>
						<script type="text/javascript">
							$('#payeeAccountID').combobox({
								'size' : '24'
							});
						</script>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%" class="sectionsubhead ltrow2_color">
						<span><s:text name="jsp.default_143" /> </span><span class="required">*</span>
					</td>
					<td align="left" width="75%" class="filter_table">
						<sj:datepicker
							value='<ffi:getProperty name="BillPayInquiry" property="PayDate"/>'
							id="PayDate" name="PayDate" maxlength="10" buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all" cssStyle="width:127px;"/>
						<br><span id="PayDateError"></span>
						<script>
							var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=billPayInquiryForm&calTarget=BillPayInquiry.PayDate"/>';
							ns.common.enableAjaxDatepicker("PayDate", tmpUrl);
						</script>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%" class="sectionsubhead ltrow2_color">
						<span><s:text name="jsp.default_45" /></span><span class="required">*</span>
					</td>
					<td align="left" width="75%" class="filter_table">
					<input name="Amount" type="text" size="15" maxlength="25" border="0" class="txtbox ui-widget-content ui-corner-all"
						value='<ffi:getProperty name="BillPayInquiry" property="Amount"/>'>
						<br><span id="AmountError"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="center" colspan="2">
						<span class="txt_normal"><ffi:getProperty name="InquiryMessage" /><span class="required">*</span></span>
					</td>
				</tr> --%>
				<tr>
					<td width="25%" style="vertical-align:top;" align="right"><span class="txt_normal"><ffi:getProperty name="InquiryMessage" /><span class="required">*</span></span></td>
					<td width="75%" class="txt_normal">
						<textarea name="Memo" id="billPayMemo"  type="text" cols="50" rows="5" align="left" class="txtbox ui-widget-content ui-corner-all" wrap="virtual"><ffi:getProperty name="BillPayInquiry" property="Memo" /></textarea>
						<br><span id="MemoError"></span>
					</td>
				</tr>
			</ffi:cinclude>
			<tr>
				<td align="left" class="txt_normal" colspan="2">&nbsp;</td>
			</tr>
			<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
				operator="notEquals">
				<tr>
					<td align="center" class="required" colspan="2">* <s:text name="jsp.default_240" />
					</td>
				</tr>
			</ffi:cinclude>
			<tr>
				<td align="left" class="txt_normal" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center" class="filter_table">
					<sj:a button="true" onClickTopics="cancelServicesForm">
						<s:text name="jsp.default_82" />
					</sj:a>
					<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
						operator="notEquals">
						<sj:a id="billPaymentInquiryFormSubmit"
							formIds="billPaymentInquiryForm" targets="resultmessage"
							button="true" validate="true" validateFunction="customValidation"
							onBeforeTopics="beforeConfirmBillPaymentInquiryForm"
							onCompleteTopics="confirmBillPaymentInquiryComplete"
							onErrorTopics="errorConfirmBillPaymentInquiryForm"
							onSuccessTopics="successConfirmBillPaymentInquiryForm"
							><s:text name="jsp.default_378" /></sj:a>
					</ffi:cinclude>
				</td>
			</tr>
		</table>
		</div>
	</s:form>
</div>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<script>
	$(document).ready(function() {
		setTimeout(function(){ns.common.renderRichTextEditor('billPayMemo','500');}, 500);
	});
</script>