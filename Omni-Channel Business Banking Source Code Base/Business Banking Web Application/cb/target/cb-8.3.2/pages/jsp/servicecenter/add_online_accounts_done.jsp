<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/add_online_accounts_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>

<ffi:removeProperty name="${AddOnlineAccounts.AccountNumberPrefix}" startsWith="true" />
<ffi:removeProperty name="${AddOnlineAccounts.AccountTypePrefix}" startsWith="true" />
<ffi:removeProperty name="AddOnlineAccounts" />
