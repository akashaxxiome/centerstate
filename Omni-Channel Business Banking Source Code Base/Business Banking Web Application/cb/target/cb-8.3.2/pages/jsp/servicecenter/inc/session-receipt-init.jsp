<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="servicecenter/session-receipt-init.jsp"/>

<ffi:setProperty name="topMenu" value="serviceCenter"/>
<ffi:setProperty name="subMenu" value="receipt"/>
<ffi:setProperty name="sub2Menu" value=""/>
<ffi:setL10NProperty name="PageTitle" value="Session Receipt"/>
<ffi:setProperty name="BackURL" value="${SecurePath}servicecenter/session-receipt.jsp?showReport=false" URLEncrypt="true"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<ffi:removeProperty name="IntegerMath" />
<ffi:removeProperty name="hasSecondaryUsers" />
<ffi:removeProperty name="SecondaryUsers" />
<ffi:removeProperty name="ReportData" />
<ffi:removeProperty name="formName" />
<ffi:removeProperty name="showReport" />
<ffi:removeProperty name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>" startsWith="true" />
<ffi:removeProperty name="UpdateReport" />
<ffi:removeProperty name="GenerateReportBase" />
<ffi:removeProperty name="GenerateReportBaseExporter" />
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="formName" />

<ffi:object id="NewReport" name="com.ffusion.tasks.reporting.NewReportBase" />
<ffi:setProperty name="NewReport" property="ReportName" value="<%= com.ffusion.beans.efsreport.EFSReportConsts.RPT_TYPE_SESSION_RECEIPT %>" />
<ffi:setProperty name="NewReport" property="Target" value="${SecurePath}servicecenter/session-receipt.jsp" />
<ffi:process name="NewReport" />
