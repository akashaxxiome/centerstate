<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/order_copy_statement_done.jsp-1" />
<ffi:setProperty name="parameter0" value="${SelectedAccount.ConsumerDisplayText}" />
<ffi:setProperty name="parameter1" value="${SendOrderStatement.BeginDate}" />
<ffi:setProperty name="parameter2" value="${SendOrderStatement.EndDate}" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>


<ffi:removeProperty name="SendOrderStatement" />
<ffi:removeProperty name="SelectedAccount" />
<ffi:removeProperty name="AccountForStatement" />
