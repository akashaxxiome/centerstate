<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<script type="text/javascript" src="<s:url value='/web/js/servicecenter/servicecenter%{#session.minVersion}.js'/>"></script>

<div id="desktop" align="center">

		<div id="appdashboard">
			<s:include value="faq_dashboard.jsp"/> 
		</div>
		<div id="operationresult">
			<div id="resultmessage"><s:text name="jsp.default_498"/></div>
		</div><!-- result -->
		
		<div id="summary">
			<s:include value="faq_summary.jsp"/> 
		</div>
</div>
<script>
	$(document).ready(function(){
		ns.servicecenter.showServiceCenterDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>