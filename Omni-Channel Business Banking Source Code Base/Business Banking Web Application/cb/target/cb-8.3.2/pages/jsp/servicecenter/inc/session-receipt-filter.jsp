<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.efsreport.EFSReportConsts" %>
<%@ page import="com.ffusion.tasks.reporting.UpdateReportCriteria" %>
<%@ page import="com.ffusion.beans.reporting.ReportConsts" %>


<ffi:author page="servicecenter/session-receipt.jsp"/>


<ffi:setProperty name="topMenu" value="serviceCenter"/>
<ffi:setProperty name="subMenu" value="receipt"/>
<ffi:setProperty name="sub2Menu" value=""/>
<ffi:setL10NProperty name="PageTitle" value="Session Receipt"/>
<ffi:setProperty name="BackURL" value="${SecurePath}servicecenter/session-receipt-init.jsp" />

<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" />
<ffi:setProperty name="StringUtil" property="Value1" value="<%= com.ffusion.tasks.Task.SESSION_LOGIN_TIME %>" />
<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ReportCriteria.HIDE_SUFFIX %>" />

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="${StringUtil.Concatenate}" />
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${SessionLoginTime.TimeInMillis}" />

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" />
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${SecureUser.ProfileID}" />

<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="equals" >
	<ffi:cinclude value1="${SecondaryUsers}" value2="" operator="equals" >
	    <ffi:object id="GetSecondaryUsers" name="com.ffusion.tasks.multiuser.GetSecondaryUsers" />
	    <ffi:process name="GetSecondaryUsers" />
	    <ffi:removeProperty name="GetSecondaryUsers" />

	    <ffi:cinclude value1="${SecondaryUsers.Size}" value2="0" operator="notEquals" >
		<ffi:setProperty name="hasSecondaryUsers" value="true" />
	    </ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="UpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria" />
<ffi:setProperty name="UpdateReportCriteria" property="ReportName" value="ReportData" />

<ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase" />

<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="UsersCritList" />
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" />
<ffi:setProperty name="UsersCritList" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<ffi:process name="CriterionListChecker" />
<ffi:removeProperty name="UsersCritList" />

<ffi:setProperty name="formName" value="sessionReceiptForm" />
<% session.setAttribute("FFIGenerateReportBase", session.getAttribute("GenerateReportBase")); %>
<% session.setAttribute("FFIUpdateReportCriteria", session.getAttribute("UpdateReportCriteria")); %>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_session_receipt" /> </span>
<div align="center">

<ffi:help id="service_center_session_receipt" />
	
	<s:form id="sessionReceiptFilterForm" namespace="/pages/jsp/servicecenter" validate="true" action="sessionReceiptAction" method="post" name="sessionReceiptFilterForm" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
	<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" >
	<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_ORIENTATION %>" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_PORTRAIT %>" >
	<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>" >
	<input type="hidden" name="destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>" >

	<table width="100%" border="0" cellspacing="0" cellpadding="4">
		<tr>
				<td width="33%" valign="top">
						<%-- dates --%>
						<s:include value="/pages/jsp/servicecenter/inc/datetime.jsp"/></td>
						
						<%-- <script type="text/javascript">
							$("#dateRangeDropDown").selectmenu({width: 150});
							$("input[name='RSeC_StartDate'], input[name='RSeC_EndDate']").attr("size", "18");
							
						</script> --%>
		    <td width="41%">
			<ffi:cinclude value1="${hasSecondaryUsers}" value2="true" operator="equals" >
			    <div style="padding-top : 5px; padding-bottom : 5px;" >
					<s:text name="jsp.servicecenter_users"/>
			    </div>
			    <ffi:setProperty name="Compare" property="Value1" value="true" />
			    <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${SecureUser.ProfileID}" />
			    <ffi:setProperty name="Compare" property="Value2" value="${CriterionListChecker.FoundInList}" />

				<select id="userList" class="" name="<%= UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" size="5" multiple style="width:350px">
					<option selected user="primary" type="checkbox" value="<ffi:getProperty name="SecureUser" property="ProfileID" />" <ffi:getProperty name="checked${Compare.Equals}" /> >
						<ffi:getProperty name="User" property="Name" />
					</option>
					
					<% String userValue = null; %>
				    <ffi:list collection="SecondaryUsers" items="user" >
					<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${user.Id}" />
					<ffi:setProperty name="Compare" property="Value2" value="${CriterionListChecker.FoundInList}" />
	
					<div class="txt_normal_bold" >
					    <option user="secondary" type="checkbox" value="<ffi:getProperty name="user" property="Id" />" <ffi:getProperty name="checked${Compare.Equals}" /> >
					    	<ffi:getProperty name="user" property="FullName" />
					    </option>
					</div>
				    </ffi:list>
				</select>
				
			   <%--  <div class="txt_normal_bold" >
				<input user="primary" type="checkbox" name="<%= UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" value="<ffi:getProperty name="SecureUser" property="ProfileID" />" <ffi:getProperty name="checked${Compare.Equals}" /> >
				<ffi:getProperty name="User" property="Name" />
			    </div> --%>

			   <%--  <% String userValue = null; %>
			    <ffi:list collection="SecondaryUsers" items="user" >
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${user.Id}" />
				<ffi:setProperty name="Compare" property="Value2" value="${CriterionListChecker.FoundInList}" />

				<div class="txt_normal_bold" >
				    <input user="secondary" type="checkbox" name="<%= UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" value="<ffi:getProperty name="user" property="Id" />" <ffi:getProperty name="checked${Compare.Equals}" /> >
				    <ffi:getProperty name="user" property="FullName" />
				</div>
			    </ffi:list> --%>
			</ffi:cinclude>
			<ffi:cinclude value1="${hasSecondaryUsers}" value2="true" operator="notEquals" >
			    <span>&nbsp;</span>
			</ffi:cinclude>
		    </td>
		    <td width="25%" valign="top">
		     <sj:a id="sessionReceiptFilterFormSubmit"
				formIds="sessionReceiptFilterForm" targets="sessionActivityHolderDiv"
				button="true" validate="true" validateFunction="customValidation"
				onBeforeTopics="beforeSessionReceiptFilterForm"
				onCompleteTopics="confirmSessionReceiptFilterComplete"
				onErrorTopics="errorConfirmSessionReceiptFilterForm"
				onSuccessTopics="successConfirmSessionReceiptFilterForm"
				><s:text name="jsp.servicecenter_view_session_receipt"></s:text></sj:a>
		    </td>
		</tr>
	    </table>
	</s:form>
	<s:form id="otherPrefs" name="otherPrefs" method="post" action="QuickEditPreferenceAction_updateSessionSettings.action" namespace="/pages/jsp/user" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" id="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<div id="PreferenceDesk" style="width:100%;">
	<!-- Session report area -->
	<div id="sessionReportPortletId" class="sessionReportCls" width="99%" style="font-size: 13px;">
		<s:text name="jsp.user_163"/>&nbsp;
			<input type="checkbox" name="test" onClick="setSessionActivityFlag()" id="sessionActivityCheck" style="vertical-align: middle;"
			<s:if test="#session.DISPLAY_SESSION_SUMMARY_REPORT">checked</s:if>>
			<input type="hidden" name="DisplaySessionSummary" id="sessionActivity"> 
			<!-- bDisplaySessionSummary -->
	</div>
	<div id="otherPreferenceBtn" class="ui-widget-header customDialogFooter">
		<%-- <sj:a id="resetOtherpreferenceButtonId" onclick="ns.userPreference.resetPreferences('MISC_TAB');" button="true">
			<s:text name="jsp.default_358"/>
		</sj:a> --%>
		<sj:a id="cancelOtherpreferenceButtonId" button="true" onclick="ns.common.closeDialog('sessionActivityDialogId')" cssClass="hiddenWhilePrinting">
			<s:text name="jsp.default_102"/>
		</sj:a>
		<sj:a id="printSessionActivityDialog" button="true" onclick="ns.common.printPopup();" cssClass="hiddenWhilePrinting">
			<s:text name="jsp.default_331"/>
		</sj:a>
		<sj:a id="saveOtherPrefButton" formIds="otherPrefs" button="true" validate="false" targets="Miscellaneous"
			onCompleteTopics="quickSessionOtherPreferences" cssClass="hiddenWhilePrinting">
			<s:text name="jsp.default_366"/>
		</sj:a>
	</div>
	</div>
</s:form>
</div>
<script>
	$(document).ready(function(){	
		ns.servicecenter.criteriaCurrentSession = '<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_SESSION %>' ;
		ns.servicecenter.absoluteRange = '<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>' ;
		ns.servicecenter.initSecondaryUsersEvents();		
		$("#userList").extmultiselect().multiselectfilter();
		//disable if secondary user list is available
		ns.servicecenter.disableUsers(true);
	});
	
	function setSessionActivityFlag(){
		if($('#sessionActivityCheck').is(":checked")){
			$('#sessionActivity').val(true);
		}else{
			$('#sessionActivity').val(false);
		}
	}
</script>
