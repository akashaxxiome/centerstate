<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Account register reports --%>
<tr height="1">
	<td class="tbrd_l"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="50" height="1" border="0"></td>
	<td><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td></td>
	<td><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
	<td><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="50" height="1" border="0"></td>
	<td class="tbrd_r"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="50" height="1" border="0"></td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports_479')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>
<tr>
	<ffi:object id="GetReportsByCategoryRegCashFlow" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
	<ffi:setProperty name="GetReportsByCategoryRegCashFlow" property="ReportCategory" value="<%= com.ffusion.beans.register.RegisterRptConsts.RPT_TYPE_CASH_FLOW %>" />
	<ffi:setProperty name="GetReportsByCategoryRegCashFlow" property="ReportsName" value="ReportsRegCashFlow" />
	<ffi:process name="GetReportsByCategoryRegCashFlow" />
	
	<ffi:object id="GetRpt8" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GetRpt8" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRpt8" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRpt8" property="ReportsName" value="ReportsRegCashFlow"/>
	<ffi:setProperty name="GetRpt8" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateRpt8" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GenerateRpt8" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRpt8" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRpt8" property="ReportsName" value="ReportsRegCashFlow"/>
	<ffi:setProperty name="GenerateRpt8" property="ReportName" value="ReportData"/>
	
	<form method="POST" action="<ffi:getProperty name="SecureServletPath"/>GenerateRpt8" name="frm_register">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="reportsName" value="ReportsRegCashFlow">
		
		<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>
		<ffi:cinclude value1="${ReportsRegCashFlow.size}" value2="0" operator="equals" >
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableStyle" value="class=\"disabledbutton\""/>
		</ffi:cinclude>
		
		<td class="tbrd_l ${table_bgstyle}" nowrap><span class="columndata"><ffi:getProperty name="TypeColumnValue"/></span></td>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td class="columndata ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryRegCashFlow.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Standard">
				<s:text name="jsp.default_87"/>
			</ffi:link>
		</td>
		<td class="tbrd_l ${table_bgstyle}">&nbsp;</td>
		<td class="columndata ${table_bgstyle}">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="txtbox" name="reportID" size="1" <ffi:getProperty name="disableEdit"/>>
							<ffi:cinclude value1="${ReportsRegCashFlow.size}" value2="0" operator="equals">
								<option><s:text name="jsp.reports_681"/></option>
							</ffi:cinclude>
							<ffi:list collection="ReportsRegCashFlow" items="Report8">
								<option value="<ffi:getProperty name="Report8" property="ReportID.ReportID"/>"><ffi:getProperty name="Report8" property="ReportID.ReportName"/></option>
							</ffi:list>
						</select>
					</td>
					<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>
					<td><input <ffi:getProperty name="disableStyle" encode="false"/> type="submit" value="<s:text name="jsp.reports_593"/>" border="0" <ffi:getProperty name="disableEdit"/>></td>
				</tr>
			</table>
		</td>
		<td align="right" class="columndata ${table_bgstyle}">
			<input type="image" src="/cb/web/multilang/grafx/i_edit.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecureServletPath'/>GetRpt8'">&nbsp;
			<input type="image" src="/cb/web/multilang/grafx/i_trash.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecurePath'/>reports/rptconfirmdelete.jsp'">
		</td>
		<td class="tbrd_r ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryRegCashFlow.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom">
				<s:text name="jsp.reports_618"/>
			</ffi:link>
		</td>
	</form>
</tr>

<tr>
	<ffi:object id="GetReportsByCategoryPayeeTotals" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
	<ffi:setProperty name="GetReportsByCategoryPayeeTotals" property="ReportCategory" value="<%= com.ffusion.beans.register.RegisterRptConsts.RPT_TYPE_PAYEE_TOTALS %>" />
	<ffi:setProperty name="GetReportsByCategoryPayeeTotals" property="ReportsName" value="ReportsPayeeTotals" />
	<ffi:process name="GetReportsByCategoryPayeeTotals" />
	
	<ffi:object id="GetRpt9" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GetRpt9" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRpt9" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRpt9" property="ReportsName" value="ReportsPayeeTotals"/>
	<ffi:setProperty name="GetRpt9" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateRpt9" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GenerateRpt9" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRpt9" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRpt9" property="ReportsName" value="ReportsPayeeTotals"/>
	<ffi:setProperty name="GenerateRpt9" property="ReportName" value="ReportData"/>
	
	<form method="POST" action="<ffi:getProperty name="SecureServletPath"/>GenerateRpt9" name="frm_register">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="reportsName" value="ReportsPayeeTotals">
		
		<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>
		<ffi:cinclude value1="${ReportsPayeeTotals.size}" value2="0" operator="equals" >
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableStyle" value="class=\"disabledbutton\""/>
		</ffi:cinclude>
		
		<td class="tbrd_l ${table_bgstyle}" nowrap><span class="columndata"><ffi:getProperty name="TypeColumnValue"/></span></td>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td class="columndata ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryPayeeTotals.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Standard">
				<s:text name="jsp.default_315"/>
			</ffi:link>
		</td>
		<td class="tbrd_l ${table_bgstyle}">&nbsp;</td>
		<td class="columndata ${table_bgstyle}">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="txtbox" name="reportID" size="1" <ffi:getProperty name="disableEdit"/>>
							<ffi:cinclude value1="${ReportsPayeeTotals.size}" value2="0" operator="equals">
								<option><s:text name="jsp.reports_681"/></option>
							</ffi:cinclude>
							<ffi:list collection="ReportsPayeeTotals" items="Report9">
								<option value="<ffi:getProperty name="Report9" property="ReportID.ReportID"/>"><ffi:getProperty name="Report9" property="ReportID.ReportName"/></option>
							</ffi:list>
						</select>
					</td>
					<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>
					<td><input <ffi:getProperty name="disableStyle" encode="false"/> type="submit" value="<s:text name="jsp.reports_593"/>" border="0" <ffi:getProperty name="disableEdit"/>></td>
				</tr>
			</table>
		</td>
		<td align="right" class="columndata ${table_bgstyle}">
			<input type="image" src="/cb/web/multilang/grafx/i_edit.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecureServletPath'/>GetRpt9'">&nbsp;
			<input type="image" src="/cb/web/multilang/grafx/i_trash.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecurePath'/>reports/rptconfirmdelete.jsp'">
		</td>
		<td class="tbrd_r ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryPayeeTotals.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom">
				<s:text name="jsp.reports_618"/>
			</ffi:link>
		</td>
	</form>
</tr>

<tr>
	<ffi:object id="GetReportsByCategoryReconcStatus" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
	<ffi:setProperty name="GetReportsByCategoryReconcStatus" property="ReportCategory" value="<%= com.ffusion.beans.register.RegisterRptConsts.RPT_TYPE_RECONCILIATION_STATUS %>" />
	<ffi:setProperty name="GetReportsByCategoryReconcStatus" property="ReportsName" value="ReportsReconcStatus" />
	<ffi:process name="GetReportsByCategoryReconcStatus" />
	
	<ffi:object id="GetRpt10" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GetRpt10" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRpt10" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRpt10" property="ReportsName" value="ReportsReconcStatus"/>
	<ffi:setProperty name="GetRpt10" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateRpt10" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GenerateRpt10" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRpt10" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRpt10" property="ReportsName" value="ReportsReconcStatus"/>
	<ffi:setProperty name="GenerateRpt10" property="ReportName" value="ReportData"/>
	
	<form method="POST" action="<ffi:getProperty name="SecureServletPath"/>GenerateRpt10" name="frm_register">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="reportsName" value="ReportsReconcStatus">
		
		<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>
		<ffi:cinclude value1="${ReportsReconcStatus.size}" value2="0" operator="equals" >
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableStyle" value="class=\"disabledbutton\""/>
		</ffi:cinclude>
		
		<td class="tbrd_l ${table_bgstyle}" nowrap><span class="columndata"><ffi:getProperty name="TypeColumnValue"/></span></td>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td class="columndata ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryReconcStatus.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Standard">
				<s:text name="jsp.default_341"/>
			</ffi:link>
		</td>
		<td class="tbrd_l ${table_bgstyle}">&nbsp;</td>
		<td class="columndata ${table_bgstyle}">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="txtbox" name="reportID" size="1" <ffi:getProperty name="disableEdit"/>>
							<ffi:cinclude value1="${ReportsReconcStatus.size}" value2="0" operator="equals">
								<option><s:text name="jsp.reports_681"/></option>
							</ffi:cinclude>
							<ffi:list collection="ReportsReconcStatus" items="Report10">
								<option value="<ffi:getProperty name="Report10" property="ReportID.ReportID"/>"><ffi:getProperty name="Report10" property="ReportID.ReportName"/></option>
							</ffi:list>
						</select>
					</td>
					<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>
					<td><input <ffi:getProperty name="disableStyle" encode="false"/> type="submit" value="<s:text name="jsp.reports_593"/>" border="0" <ffi:getProperty name="disableEdit"/>></td>
				</tr>
			</table>
		</td>
		<td align="right" class="columndata ${table_bgstyle}">
			<input type="image" src="/cb/web/multilang/grafx/i_edit.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecureServletPath'/>GetRpt10'">&nbsp;
			<input type="image" src="/cb/web/multilang/grafx/i_trash.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecurePath'/>reports/rptconfirmdelete.jsp'">
		</td>
		<td class="tbrd_r ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryReconcStatus.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom">
				<s:text name="jsp.reports_618"/>
			</ffi:link>
		</td>
	</form>
</tr>

<tr>
	<ffi:object id="GetReportsByCategoryTaxStatus" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
	<ffi:setProperty name="GetReportsByCategoryTaxStatus" property="ReportCategory" value="<%= com.ffusion.beans.register.RegisterRptConsts.RPT_TYPE_TAX_STATUS %>" />
	<ffi:setProperty name="GetReportsByCategoryTaxStatus" property="ReportsName" value="ReportsTaxStatus" />
	<ffi:process name="GetReportsByCategoryTaxStatus" />
	
	<ffi:object id="GetRpt11" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GetRpt11" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRpt11" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRpt11" property="ReportsName" value="ReportsTaxStatus"/>
	<ffi:setProperty name="GetRpt11" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateRpt11" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GenerateRpt11" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRpt11" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRpt11" property="ReportsName" value="ReportsTaxStatus"/>
	<ffi:setProperty name="GenerateRpt11" property="ReportName" value="ReportData"/>
	
	<form method="POST" action="<ffi:getProperty name="SecureServletPath"/>GenerateRpt11" name="frm_register">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="reportsName" value="ReportsTaxStatus">
		
		<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>
		<ffi:cinclude value1="${ReportsTaxStatus.size}" value2="0" operator="equals" >
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableStyle" value="class=\"disabledbutton\""/>
		</ffi:cinclude>
		
		<td class="tbrd_l ${table_bgstyle}" nowrap><span class="columndata"><ffi:getProperty name="TypeColumnValue"/></span></td>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td class="columndata ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryTaxStatus.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Standard">
				<s:text name="jsp.default_408"/>
			</ffi:link>
		</td>
		<td class="tbrd_l ${table_bgstyle}">&nbsp;</td>
		<td class="columndata ${table_bgstyle}">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="txtbox" name="reportID" size="1" <ffi:getProperty name="disableEdit"/>>
							<ffi:cinclude value1="${ReportsTaxStatus.size}" value2="0" operator="equals">
								<option><s:text name="jsp.reports_681"/></option>
							</ffi:cinclude>
							<ffi:list collection="ReportsTaxStatus" items="Report11">
								<option value="<ffi:getProperty name="Report11" property="ReportID.ReportID"/>"><ffi:getProperty name="Report11" property="ReportID.ReportName"/></option>
							</ffi:list>
						</select>
					</td>
					<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>
					<td><input <ffi:getProperty name="disableStyle" encode="false"/> type="submit" value="<s:text name="jsp.reports_593"/>" border="0" <ffi:getProperty name="disableEdit"/>></td>
				</tr>
			</table>
		</td>
		<td align="right" class="columndata ${table_bgstyle}">
			<input type="image" src="/cb/web/multilang/grafx/i_edit.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecureServletPath'/>GetRpt11'">&nbsp;
			<input type="image" src="/cb/web/multilang/grafx/i_trash.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecurePath'/>reports/rptconfirmdelete.jsp'">
		</td>
		<td class="tbrd_r ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryTaxStatus.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom">
				<s:text name="jsp.reports_618"/>
			</ffi:link>
		</td>
	</form>
</tr>

<tr>
	<ffi:object id="GetReportsByCategoryTransTypeTotals" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
	<ffi:setProperty name="GetReportsByCategoryTransTypeTotals" property="ReportCategory" value="<%= com.ffusion.beans.register.RegisterRptConsts.RPT_TYPE_TRANSACTION_TYPE_TOTALS %>" />
	<ffi:setProperty name="GetReportsByCategoryTransTypeTotals" property="ReportsName" value="ReportsTransTypeTotals" />
	<ffi:process name="GetReportsByCategoryTransTypeTotals" />
	
	<ffi:object id="GetRpt12" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GetRpt12" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRpt12" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRpt12" property="ReportsName" value="ReportsTransTypeTotals"/>
	<ffi:setProperty name="GetRpt12" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateRpt12" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GenerateRpt12" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRpt12" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRpt12" property="ReportsName" value="ReportsTransTypeTotals"/>
	<ffi:setProperty name="GenerateRpt12" property="ReportName" value="ReportData"/>
	
	<form method="POST" action="<ffi:getProperty name="SecureServletPath"/>GenerateRpt12" name="frm_register">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="reportsName" value="ReportsTransTypeTotals">
		
		<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>
		<ffi:cinclude value1="${ReportsTransTypeTotals.size}" value2="0" operator="equals" >
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableStyle" value="class=\"disabledbutton\""/>
		</ffi:cinclude>
		
		<td class="tbrd_l ${table_bgstyle}" nowrap><span class="columndata"><ffi:getProperty name="TypeColumnValue"/></span></td>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td class="columndata ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryTransTypeTotals.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Standard">
				<s:text name="jsp.default_440"/>
			</ffi:link>
		</td>
		<td class="tbrd_l ${table_bgstyle}">&nbsp;</td>
		<td class="columndata ${table_bgstyle}">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="txtbox" name="reportID" size="1" <ffi:getProperty name="disableEdit"/>>
							<ffi:cinclude value1="${ReportsTransTypeTotals.size}" value2="0" operator="equals">
								<option><s:text name="jsp.reports_681"/></option>
							</ffi:cinclude>
							<ffi:list collection="ReportsTransTypeTotals" items="Report12">
								<option value="<ffi:getProperty name="Report12" property="ReportID.ReportID"/>"><ffi:getProperty name="Report12" property="ReportID.ReportName"/></option>
							</ffi:list>
						</select>
					</td>
					<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>
					<td><input <ffi:getProperty name="disableStyle" encode="false"/> type="submit" value="<s:text name="jsp.reports_593"/>" border="0" <ffi:getProperty name="disableEdit"/>></td>
				</tr>
			</table>
		</td>
		<td align="right" class="columndata ${table_bgstyle}">
			<input type="image" src="/cb/web/multilang/grafx/i_edit.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecureServletPath'/>GetRpt12'">&nbsp;
			<input type="image" src="/cb/web/multilang/grafx/i_trash.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecurePath'/>reports/rptconfirmdelete.jsp'">
		</td>
		<td class="tbrd_r ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryTransTypeTotals.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom">
				<s:text name="jsp.reports_618"/>
			</ffi:link>
		</td>
	</form>
</tr>
<tr>
	<ffi:object id="GetReportsByCategoryCatTotals" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
	<ffi:setProperty name="GetReportsByCategoryCatTotals" property="ReportCategory" value="<%= com.ffusion.beans.register.RegisterRptConsts.RPT_TYPE_CATEGORY_TOTALS %>" />
	<ffi:setProperty name="GetReportsByCategoryCatTotals" property="ReportsName" value="ReportsCatTotals" />
	<ffi:process name="GetReportsByCategoryCatTotals" />
	
	<ffi:object id="GetRpt13" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GetRpt13" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRpt13" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRpt13" property="ReportsName" value="ReportsCatTotals"/>
	<ffi:setProperty name="GetRpt13" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateRpt13" name="com.ffusion.tasks.reporting.GetReportFromReports"/>
	<ffi:setProperty name="GenerateRpt13" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRpt13" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRpt13" property="ReportsName" value="ReportsCatTotals"/>
	<ffi:setProperty name="GenerateRpt13" property="ReportName" value="ReportData"/>
	
	<form method="POST" action="<ffi:getProperty name="SecureServletPath"/>GenerateRpt13" name="frm_register">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="reportsName" value="ReportsCatTotals">
		
		<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>
		<ffi:cinclude value1="${ReportsCatTotals.size}" value2="0" operator="equals" >
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableStyle" value="class=\"disabledbutton\""/>
		</ffi:cinclude>
		
		<td class="tbrd_l ${table_bgstyle}" nowrap><span class="columndata"><ffi:getProperty name="TypeColumnValue"/></span></td>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td class="columndata ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryCatTotals.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Standard">
				<s:text name="jsp.default_89"/>
			</ffi:link>
		</td>
		<td class="tbrd_l ${table_bgstyle}">&nbsp;</td>
		<td class="columndata ${table_bgstyle}">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="txtbox" name="reportID" size="1" <ffi:getProperty name="disableEdit"/>>
							<ffi:cinclude value1="${ReportsCatTotals.size}" value2="0" operator="equals">
								<option><s:text name="jsp.reports_681"/></option>
							</ffi:cinclude>
							<ffi:list collection="ReportsCatTotals" items="Report13">
								<option value="<ffi:getProperty name="Report13" property="ReportID.ReportID"/>"><ffi:getProperty name="Report13" property="ReportID.ReportName"/></option>
							</ffi:list>
						</select>
					</td>
					<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>
					<td><input <ffi:getProperty name="disableStyle" encode="false"/> type="submit" value="<s:text name="jsp.reports_593"/>" border="0" <ffi:getProperty name="disableEdit"/>></td>
				</tr>
			</table>
		</td>
		<td align="right" class="columndata ${table_bgstyle}">
			<input type="image" src="/cb/web/multilang/grafx/i_edit.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecureServletPath'/>GetRpt13'">&nbsp;
			<input type="image" src="/cb/web/multilang/grafx/i_trash.gif" border="0" <ffi:getProperty name="disableEdit"/> onClick="document.frm_customer_perm.action='<ffi:getProperty name='SecurePath'/>reports/rptconfirmdelete.jsp'">
		</td>
		<td class="tbrd_r ${table_bgstyle}">
			<ffi:link url="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryCatTotals.ReportCategory}&NewReportBase.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom">
				<s:text name="jsp.reports_618"/>
			</ffi:link>
		</td>
	</form>
</tr>
<ffi:removeProperty name="Report8"/>
<ffi:removeProperty name="Report9"/>
<ffi:removeProperty name="Report10"/>
<ffi:removeProperty name="Report11"/>
<ffi:removeProperty name="Report12"/>
<ffi:removeProperty name="Report13"/>
