<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_cash_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr height="1">
						<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
						<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
						<s:set var="tmpI18nStr" value="%{getText('jsp.default_327')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
					</tr>

<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_REVERSE_POSITIVE_PAY_DECISION_HISTORY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetRPPayDecisionsRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetRPPayDecisionsRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRPPayDecisionsRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRPPayDecisionsRpt" property="ReportsIDsName" value="ReportsRPPayDecis"/>
	<ffi:setProperty name="GetRPPayDecisionsRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GenerateRPPayDecisionsRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateRPPayDecisionsRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRPPayDecisionsRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRPPayDecisionsRpt" property="ReportsIDsName" value="ReportsRPPayDecis"/>
	<ffi:setProperty name="GenerateRPPayDecisionsRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_REVERSE_POSITIVE_PAY_DECISION_HISTORY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports_701')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>

<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_REVERSE_POSITIVE_PAY_ACTIVITY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetRPPayActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetRPPayActivityRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetRPPayActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetRPPayActivityRpt" property="ReportsIDsName" value="ReportsRPPayActivity"/>
	<ffi:setProperty name="GetRPPayActivityRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GenerateRPPayActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateRPPayActivityRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateRPPayActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateRPPayActivityRpt" property="ReportsIDsName" value="ReportsRPPayActivity"/>
	<ffi:setProperty name="GenerateRPPayActivityRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_REVERSE_POSITIVE_PAY_ACTIVITY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Reverse_Positive_Pay_Activity')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryPPayIssues"/>
<ffi:removeProperty name="GetReportsByCategoryPPayDecis"/>
