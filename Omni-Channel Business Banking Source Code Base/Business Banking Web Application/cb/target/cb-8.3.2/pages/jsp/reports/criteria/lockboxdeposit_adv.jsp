<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.lockbox.LockboxRptConsts" %>
<ffi:help id="deposit-item-search_report" className="moduleHelpClass" />
<s:include value="%{#session.PagesPath}common/checkAmount_js.jsp"/>

<%-- Search criteria names associated with other1 and other2 --%>
<%
String[] criteriaStart = {
	LockboxRptConsts.SEARCH_CRITERIA_DOC_TYPE,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_ACC_NUM_START,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_AMOUNT1_MIN,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_AMOUNT2_MIN,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_DATE1_START,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_DATE2_START,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_REF_NUM_START,
	LockboxRptConsts.SEARCH_CRITERIA_CHECK_ROUTING_NUM_START,
	LockboxRptConsts.SEARCH_CRITERIA_CHECK_ACC_NUM_START,
	LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_WORK_TYPE,
	LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_BATCH_NUM_START,
	LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_SEQ_NUM_START
	};
String[] criteriaEnd = {
	"",
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_ACC_NUM_END,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_AMOUNT1_MAX,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_AMOUNT2_MAX,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_DATE1_END,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_DATE2_END,
	LockboxRptConsts.SEARCH_CRITERIA_COUPON_REF_NUM_END,
	LockboxRptConsts.SEARCH_CRITERIA_CHECK_ROUTING_NUM_END,
	LockboxRptConsts.SEARCH_CRITERIA_CHECK_ACC_NUM_END,
	"",
	LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_BATCH_NUM_END,
	LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_SEQ_NUM_END
	};
%>
<script type="text/javascript"><!--
ns.report.validateForm = function( formName )
{
    // Clear all of the old values
    $(".optionalCriteria").val("");
    
// if other1.value != "", then use the rangeStart and rangeEnd fields to set the
// criteriaStartX and criteriaEndX (where X is the value of other1
    var selectOther1 = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other1"];
    var rangeStartOther1 = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other1_rangeStart"];
    var rangeEndOther1 = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other1_rangeEnd"];
    if( selectOther1.value != "" ) {
        if (document.getElementById("criteriaStart"+selectOther1.value) != null)
            document.getElementById("criteriaStart"+selectOther1.value).value = rangeStartOther1.value;
        if (document.getElementById("criteriaEnd"+selectOther1.value) != null)
            document.getElementById("criteriaEnd"+selectOther1.value).value = rangeEndOther1.value;  
    }

// if other2.value != "", then use the rangeStart and rangeEnd fields to set the
// criteriaStartX and criteriaEndX (where X is the value of other2
    var selectOther2 = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other2"];
    var rangeStartOther2 = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other2_rangeStart"];
    var rangeEndOther2 = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other2_rangeEnd"];
    if( selectOther2.value != "" ) {
        if (document.getElementById("criteriaStart"+selectOther2.value) != null)
            document.getElementById("criteriaStart"+selectOther2.value).value = rangeStartOther2.value;
        if (document.getElementById("criteriaEnd"+selectOther2.value) != null)
            document.getElementById("criteriaEnd"+selectOther2.value).value = rangeEndOther2.value;
    }
	return true;
}
// --></script>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="LOCKBOX_ACCOUNTS" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="LOCKBOX_ACCOUNTS"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>

<%-- data classification --%>
<% 
boolean ent_previous_day=false;
boolean ent_intra_day=false;
%>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
	<% ent_previous_day=true; %>
</ffi:cinclude>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
	<% ent_intra_day=true; %>
</ffi:cinclude>

<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
<%-- This is a detail report so we will filter accounts on the detail view entitlement --%>
<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="LOCKBOX_ACCOUNTS"/>
<%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
<%-- entitled to view under that data classification --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
</ffi:cinclude>
<ffi:process name="AccountEntitlementFilterTask"/>

<tr>
	<td align="right" class="sectionsubhead">
	<% 
	if( ent_previous_day || ent_intra_day ) { 
	%>
		<s:text name="jsp.default_354"/>&nbsp;
	<%
	}	// end if
	%>
	</td>
	<td>
	<% 
	if( ent_previous_day || ent_intra_day ) { 
	%>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh()">
			<% 
			if( ent_previous_day ) { 
			%>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_634"/>
			</option>
			<%
			}
			if( ent_intra_day ) {
			%>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_603"/>
			</option>
			<%
			}
			%>
		</select>
	<%
	}	// end if
	%>
	</td>
	<td>
		&nbsp;
	</td>
	<td>
		&nbsp;
	</td>
</tr>

<%-- lockbox --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_268"/>&nbsp;&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_NUM %>" />
		<ffi:setProperty name="curr_val" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<%-- set default: all lockboxes --%>
		<ffi:cinclude value1="" value2="${curr_val}" operator="equals" >
			<ffi:setProperty name="curr_val" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_VALUE_ALL_LOCKBOXES %>" />
		</ffi:cinclude>
		<select id="lockboxDepositSrchDropdown" style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_NUM %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${curr_val}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_VALUE_ALL_LOCKBOXES %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_VALUE_ALL_LOCKBOXES %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

			<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
			<ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="LOCKBOX_ACCOUNT_1">
				<option value="<ffi:getProperty name='LOCKBOX_ACCOUNT_1' property='AccountID' />:<ffi:getProperty name='LOCKBOX_ACCOUNT_1' property='BankID'/>: :<ffi:getProperty name='LOCKBOX_ACCOUNT_1' property='RoutingNumber'/>"
					<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${LOCKBOX_ACCOUNT_1.AccountID}:${LOCKBOX_ACCOUNT_1.BankID}: :${LOCKBOX_ACCOUNT_1.RoutingNumber}" />
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="LOCKBOX_ACCOUNT_1" property="RoutingNumber"/> : 
					
					<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${LOCKBOX_ACCOUNT_1.AccountID}" />
					<ffi:process name="GetAccountDisplayText" />
					<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" /> - 
					
					<ffi:getProperty name="LOCKBOX_ACCOUNT_1" property="Nickname"/> - 
					<ffi:getProperty name="LOCKBOX_ACCOUNT_1" property="CurrencyType"/>
				</option>
			</ffi:list>
			<%-- clean up the mess --%>
			<ffi:removeProperty name="curr_val"/>
			<ffi:removeProperty name="LOCKBOX_ACCOUNT_1"/>
			<ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<%-- start/end dates --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%--
	payor, other 1. 
	Note: "Other" selects have option values equal to the array indices in
		"criteriaStart" and "criteriaEnd" defined in scriptlets on top of this file.
		We use these indices to find out what is selected.
--%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_629"/>&nbsp;</td>	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_PAYOR%>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_PAYOR %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12" maxlength="<%= com.ffusion.beans.lockbox.LockboxRptConsts.PAYOR_MAXLENGTH %>">
	</td>

	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_626"/>&nbsp;</td>
	<td>
		< ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="other1" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> ></option>
			<option value="0" <ffi:cinclude value1="0" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_575"/>
			</option>
			<option value="1" <ffi:cinclude value1="1" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_540"/>
			</option>
			<option value="2" <ffi:cinclude value1="2" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_541"/>
			</option>
			<option value="3" <ffi:cinclude value1="3" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_542"/>
			</option>
			<option value="4" <ffi:cinclude value1="4" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_116"/>
			</option>
			<option value="5" <ffi:cinclude value1="5" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_117"/>
			</option>
			<option value="6" <ffi:cinclude value1="6" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_543"/>
			</option>
			<option value="7" <ffi:cinclude value1="7" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_536"/>
			</option>
			<option value="8" <ffi:cinclude value1="8" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_533"/>
			</option>
			<option value="9" <ffi:cinclude value1="9" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_691"/>
			</option>
			<option value="10" <ffi:cinclude value1="10" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_521"/>
			</option>
			<option value="11" <ffi:cinclude value1="11" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_662"/>
			</option>
		</select>
		<p>< ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="other1_rangeStart" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other1_rangeStart" value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentSearchCriterionValue'/>" size="12">
               	<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp; 
		< ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="other1_rangeEnd" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other1_rangeEnd" value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentSearchCriterionValue'/>" size="12"></p>
	</td>
</tr>
<%--
	check number, other 2
	Note: "Other" selects have option values equal to the array indices in
		"criteriaStart" and "criteriaEnd" defined in scriptlets on top of this file.
		We use these indices to find out what is selected.
--%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_534"/>&nbsp;</td>	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_START_CHECK_NUMBER%>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_START_CHECK_NUMBER %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12" maxlength="<%= com.ffusion.beans.lockbox.LockboxRptConsts.CHECK_NUMBER_MAXLENGTH %>">
		<span class="columndata">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp;
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_END_CHECK_NUMBER%>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_END_CHECK_NUMBER %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12" maxlength="<%= com.ffusion.beans.lockbox.LockboxRptConsts.CHECK_NUMBER_MAXLENGTH %>">
	</td>

	<td>
	</td>
	<td>
		< ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="other2" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other2">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> ></option>
			
			<option value="0" <ffi:cinclude value1="0" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_575"/>
			</option>
			<option value="1" <ffi:cinclude value1="1" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_540"/>
			</option>
			<option value="2" <ffi:cinclude value1="2" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_541"/>
			</option>
			<option value="3" <ffi:cinclude value1="3" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_542"/>
			</option>
			<option value="4" <ffi:cinclude value1="4" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_116"/>
			</option>
			<option value="5" <ffi:cinclude value1="5" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_117"/>
			</option>
			<option value="6" <ffi:cinclude value1="6" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_543"/>
			</option>
			<option value="7" <ffi:cinclude value1="7" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_536"/>
			</option>
			<option value="8" <ffi:cinclude value1="8" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_533"/>
			</option>
			<option value="9" <ffi:cinclude value1="9" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_691"/>
			</option>
			<option value="10" <ffi:cinclude value1="10" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_521"/>
			</option>
			<option value="11" <ffi:cinclude value1="11" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_662"/>
			</option>
		</select><p>
		< ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="other2_rangeStart" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other2_rangeStart" value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentSearchCriterionValue'/>" size="12">
               	<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp; 
		< ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="other2_rangeEnd" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>other2_rangeEnd" value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentSearchCriterionValue'/>" size="12"></p>
	</td>
</tr>
<%-- amount, sort 1 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12">
		<span class="columndata">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp;
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12">
	</td>
  </tr>
  
  <tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<ffi:setProperty name="curr_sort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="" <ffi:cinclude value1="" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> ><s:text name="jsp.default_375"/></option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.default_15"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PROCESS_DATE %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_334"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PAYOR %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PAYOR %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_629"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_534"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_DOC_TYPE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_DOC_TYPE %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_575"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_ACC_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_ACC_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_540"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT1 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT1 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_541"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT2 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT2 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_542"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE1 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE1 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_116"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE2 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE2 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_117"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_REF_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_REF_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				  <s:text name="jsp.reports_543"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ROUTING_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ROUTING_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_536"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ACC_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ACC_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_533"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_WORK_TYPE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_WORK_TYPE %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_691"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_BATCH_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_BATCH_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_521"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_SEQ_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_SEQ_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_662"/>
			</option>
		</select>
		
		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<ffi:setProperty name="curr_sort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="" <ffi:cinclude value1="" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> ><s:text name="jsp.default_375"/></option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.default_15"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PROCESS_DATE %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_334"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PAYOR %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_PAYOR %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_629"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_534"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_DOC_TYPE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_DOC_TYPE %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_575"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_ACC_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_ACC_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_540"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT1 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT1 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_541"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT2 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_AMOUNT2 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_542"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE1 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE1 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_116"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE2 %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_DATE2 %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_117"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_REF_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_COUPON_REF_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				  <s:text name="jsp.reports_543"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ROUTING_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ROUTING_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_536"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ACC_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_CHECK_ACC_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_533"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_WORK_TYPE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_WORK_TYPE %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_691"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_BATCH_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_BATCH_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_521"/>
			</option>
			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_SEQ_NUM %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SORT_CRITERIA_LOCKBOX_SEQ_NUM %>" value2="${curr_sort}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_268"/>&nbsp; <s:text name="jsp.reports_662"/>
			</option>
		</select>
	</td>	
</tr>
<%-- Hidden fields used to hold the select values and value ranges for other1 and other2 --%>
<%
	for( int i=0; i<criteriaStart.length; i++ ) {
		String critStart = criteriaStart[i];
		String critEnd = criteriaEnd[i];
%>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= critStart %>"/>
		<input id="criteriaStart<%= i %>" class="optionalCriteria" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + critStart %>" value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentSearchCriterionValue'/>" />
		<ffi:cinclude value1="<%= critEnd %>" value2="" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= critEnd %>"/>
		<input id="criteriaEnd<%= i %>" class="optionalCriteria" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + critEnd %>" value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentSearchCriterionValue'/>" />
		</ffi:cinclude>
<%
	}
%>

<%-- Housekeeping --%>
<ffi:removeProperty name="curr_sort"/>
<ffi:removeProperty name="CriterionListChecker" />
<script>
	$("#lockboxDepositSrchDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>	