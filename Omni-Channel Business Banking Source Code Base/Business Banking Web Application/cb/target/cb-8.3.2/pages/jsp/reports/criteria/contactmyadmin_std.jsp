<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="contact_my_admin_audit" className="moduleHelpClass" />
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" value="<ffi:getProperty name='UserLocale' property='DateFormat'/>">
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" value="<ffi:getProperty name='UserLocale' property='TimeFormat'/>">
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_PRIMARY_ADMIN %>" value="<ffi:getProperty name='User' property='Id'/>">

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="BizEmployeeCriteria" scope="session" />
	<ffi:setProperty name="BizEmployeeCriteria" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="BizEmployeeCriteria" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>

<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="SearchBusinessEmployees" scope="request" />
	<ffi:setProperty name="SearchBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="BizEmployeeCriteria"/>
<ffi:process name="SearchBusinessEmployees"/>

<ffi:removeProperty name="BizEmployeeCriteria"/>

<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.reports_688"/></td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>"/>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>"
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.reports_505"/>
			</option>
            <ffi:list collection="BusinessEmployees" items="BusinessEmployeeItem">
                <ffi:cinclude value1="${User.Id}" value2="${BusinessEmployeeItem.Id}" operator="notEquals">
                    <ffi:cinclude value1="${User.Id}" value2="${BusinessEmployeeItem.PrimaryAdmin}">
                        <option value="<ffi:getProperty name='BusinessEmployeeItem' property='Id'/>"
                        <ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                            <ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>
                        </option>
                    </ffi:cinclude>
                </ffi:cinclude>
            </ffi:list>
		</select>
	</td>
</tr>
<ffi:setProperty name="DisplayTime" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<ffi:removeProperty name="BusinessEmployees"/>
