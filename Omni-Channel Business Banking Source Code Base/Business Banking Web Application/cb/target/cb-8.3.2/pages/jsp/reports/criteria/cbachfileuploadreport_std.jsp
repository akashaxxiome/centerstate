<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />
<ffi:help id="ach-file-upload_report" className="moduleHelpClass" />
<ffi:setProperty name="NoConsumer" value="TRUE" />

<tr>
    <td class="sectionsubhead" align="right" nowrap><s:text name="jsp.default_388"/></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_STATUS %>" />  
        <select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_STATUS %>" size="1">
            <ffi:object id="StatusList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
            <ffi:setProperty name="StatusList" property="ResourceFilename" value="com.ffusion.beansresources.reporting.ach_status" />
            <ffi:setProperty name="StatusList" property="ResourceID" value="ACHFileUploadStatuses" />
            <ffi:process name="StatusList"/>
            
            <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
            <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.ach_status" />
            <ffi:process name="StatusResource" />
            
            <ffi:getProperty name="StatusList" property="Resource"/>
            <ffi:list collection="StatusList" items="item">
                <ffi:setProperty name="StatusResource" property="ResourceID" value="${item}" />
                <option <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="${item}">selected</ffi:cinclude> value="<ffi:getProperty name="item"/>"><ffi:getProperty name='StatusResource' property='Resource'/></option>
            </ffi:list>
        </select>
    </td>
</tr>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_567"/>&nbsp;</td>
    <td align="left">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" />

        <select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" size="1">
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_FILE_SUMMARY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_FILE_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_583"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_523"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_565"/>
                </option>
        </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

