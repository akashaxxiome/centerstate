<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="System Activity Report" operator="equals">
	<ffi:help id="system_activity" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="On Behalf Of Report" operator="equals">
	<ffi:help id="obo_report" className="moduleHelpClass" />
</ffi:cinclude>

<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" value="${UserLocale.DateFormat}">
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" value="${UserLocale.TimeFormat}">

<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.default_225"/></td>
	<td class="sectionsubhead" colspan="3">
		<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
		<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
		<ffi:process name="GetGroupsAdministeredBy"/>
		<ffi:removeProperty name="GetGroupsAdministeredBy"/>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>"/>
		<%--<ffi:getProperty name="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/> --%>
		<select style="width:240px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>" onchange="ns.report.refresh();">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" 
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
				<s:text name="jsp.reports_495"/>
			</option>			
			<ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
				<ffi:setProperty name="group_pre" value=""/>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
					<ffi:setProperty name="group_pre" value="&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Group" operator="equals">
					<ffi:setProperty name="group_pre" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>

				<option value="<ffi:getProperty name='ENT_GROUPS_1' property='GroupId'/>" 
				<ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name="group_pre" encode="false"/>
					<ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
				</option>
			</ffi:list>
		</select>
	</td>
</tr>
<tr>	
	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="equals">
		<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroups" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
		<ffi:process name="GetBusinessEmployeesByEntGroups"/>
		<ffi:removeProperty name="GetBusinessEmployeesByEntGroups"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="notEquals">
		<ffi:object id="GetBusinessEmployeesByEntGroupId" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="EntitlementGroupId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
		<ffi:process name="GetBusinessEmployeesByEntGroupId"/>
	</ffi:cinclude>

	<td class="sectionsubhead" align="right"><s:text name="jsp.reports_688"/></td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>"/>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>">
			
		<% int userNumber = 0; %>
		<ffi:list collection="AdministeredBusinessEmployees" items="CountUserNumber">
			<% userNumber++; %>
		</ffi:list>
		<% String userNumberString = new Integer(userNumber).toString(); %>
		
		<ffi:cinclude value1="<%= userNumberString %>" value2="0" operator="notEquals">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>"
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.reports_504"/>
			</option>
            <ffi:setProperty name="AdministeredBusinessEmployees" property="SortedBy" value="LAST,FIRST"/>
		</ffi:cinclude>
			
			<ffi:list collection="AdministeredBusinessEmployees" items="BusinessEmployeeItem">
				<option value="<ffi:getProperty name='BusinessEmployeeItem' property='Id'/>" 
				<ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>
				</option>
			</ffi:list>
			<ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="" operator="equals">
				<option value="<ffi:getProperty name='BusinessEmployee' property='Id'/>"
					<ffi:cinclude value1="<ffi:getProperty name='BusinessEmployee' property='Id'/>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name='BusinessEmployee' property='lastName'/>, <ffi:getProperty name='BusinessEmployee' property='firstName'/>  (<ffi:getProperty name='BusinessEmployee' property='userName'/>)
				</option>
			</ffi:cinclude>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
        <ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:removeProperty name="GetEntitlementGroup"/>
<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Business" operator="equals">
            <ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="BizEmployeeCriteria" scope="session" />
                <ffi:setProperty name="BizEmployeeCriteria" property="BusinessId" value="${Business.Id}"/>
                <ffi:setProperty name="BizEmployeeCriteria" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
                <ffi:setProperty name="BizEmployeeCriteria" property="AccountStatus" value="<%=com.ffusion.beans.user.BusinessEmployee.STATUS_DELETED%>"/>

            <ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="SearchBusinessEmployees" scope="request" />
                <ffi:setProperty name="SearchBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="BizEmployeeCriteria"/>
                <ffi:setProperty name="SearchBusinessEmployees" property="BusinessEmployeesSessionName" value="DeletedUserList"/>

            <ffi:process name="SearchBusinessEmployees"/>
            <ffi:cinclude value1="${DeletedUserList.Size}" value2="0" operator="notEquals">
                <ffi:setProperty name="DeletedUserList" property="SortedBy" value="LAST,FIRST"/>
                <option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_AND_ALL_USERS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_AND_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <s:text name="jsp.reports_506"/>
                </option>
                <option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_USERS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <s:text name="jsp.reports_563"/>
                </option>
                <ffi:list collection="DeletedUserList" items="BusinessEmployeeItem">
                	<ffi:setProperty name="DeletedPrefix" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_PREFIX%>" />
                    <ffi:setProperty name="DeletedUserID" value="${DeletedPrefix}${BusinessEmployeeItem.Id}" />
                    <option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_PREFIX %><ffi:getProperty name='BusinessEmployeeItem' property='Id'/>"
                    <ffi:cinclude value1="${DeletedUserID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                        <ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>(<s:text name="jsp.reports_562"/>)
    
                    </option>
                    <ffi:removeProperty name="DeletedPrefix" />
                    <ffi:removeProperty name="DeletedUserID" />
                </ffi:list>
            </ffi:cinclude>
            <ffi:removeProperty name="DeletedUserList,SearchBusinessEmployees,BizEmployeeCriteria"/>
</ffi:cinclude>
		</select>
	</td>
</tr>
<ffi:setProperty name="DisplayTime" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<ffi:removeProperty name="AdministeredBusinessEmployees"/>
