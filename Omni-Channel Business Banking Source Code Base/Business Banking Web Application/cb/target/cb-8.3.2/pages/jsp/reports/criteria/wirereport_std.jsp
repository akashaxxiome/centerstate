<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Completed Wires" operator="equals">
	<ffi:help id="completed-wires_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Failed Wires" operator="equals">
	<ffi:help id="custom-summary_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="InProcess Wires" operator="equals">
	<ffi:help id="completed-wires_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Wire By Status" operator="equals">
	<ffi:help id="custom-summary_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Wire By Source" operator="equals">
	<ffi:help id="completed-wires_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Wire By Template" operator="equals">
	<ffi:help id="custom-summary_report" className="moduleHelpClass" />
</ffi:cinclude>

<%-- If there is no wire accounts in session, get transfer accounts --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS ) == null ) { %>
	<ffi:object id="GetWiresAccounts" name="com.ffusion.tasks.wiretransfers.GetWiresAccounts" scope="session"/>
		<ffi:setProperty name="GetWiresAccounts" property="Reload" value="true"/>
	<ffi:process name="GetWiresAccounts"/>
<% } %>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<%-- account --%>
<tr>
	<td align="right" class="sectionsubhead" valign="top"><s:text name="jsp.default_217"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_ACCOUNT %>" />
		<select id="wireAcc" style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_ACCOUNT %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value=""
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		<ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS %>" property="Filter" value="All"/>
		<ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS %>" items="acct">
			<ffi:setProperty name="tmp_val" value="${acct.ID}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="acct" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="acct" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="acct" property="CurrencyCode"/>
			</option>
		</ffi:list>
		<ffi:removeProperty name="tmp_val"/>
		<ffi:removeProperty name="acct" />
		<ffi:removeProperty name="GetAccountDisplayText"/>
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- Detail Level --%>
<tr>
        <td align="right" class="sectionsubhead"><s:text name="jsp.default_259"/></td>
        <td align="left">
                <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL %>" />
                <select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL %>" size="1">
                        <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_SUMMARY %>" <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_400"/></option>
                        <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_DETAILED %>" <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_DETAILED %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.reports_568"/></option>
                </select>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
</tr>

<%-- housekeeping --%>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS %>" />
<ffi:removeProperty name="GetWiresAccounts"/>

<script>
	$("#wireAcc").extmultiselect({header: ""}).multiselectfilter();
</script>	