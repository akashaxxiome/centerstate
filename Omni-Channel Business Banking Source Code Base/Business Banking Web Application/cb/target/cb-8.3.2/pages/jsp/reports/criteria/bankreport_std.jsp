<%@ taglib uri="/WEB-INF/ffi.tld"  prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
	<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<% String selectedReport = ( String )session.getAttribute( "selectedReport" );
   String changed = ( String )session.getAttribute( "reportChanged" );
   if( changed == null ) { changed = ""; }
   if( selectedReport == null ) { selectedReport = ""; }
   boolean reportChanged = changed.equals( "true" );
   String reportType;
   String reportAccounts = "";
   String rptCount = "0";
%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="SampleBankReport-AcctSummary" operator="equals">
	<ffi:help id="acct-summary_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="SampleBankReport-BusSummary" operator="equals">
	<ffi:help id="biz-summary_report" className="moduleHelpClass" />
</ffi:cinclude>


<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>"/>
<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="reportType" />
<%-- if the selected report wasn't passed in as a parameter, get it out of the search criteria for the report in the session --%>
<ffi:cinclude value1="<%= selectedReport %>" value2="" operator="equals">
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_REPORT %>" />
	<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="selectedReport" />
</ffi:cinclude>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
<% 
    String accountsList = ((com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" )).getReportCriteria().getCurrentSearchCriterionValue();
    if( selectedReport == null ) { selectedReport = ""; } 
    java.util.HashSet accountsSet = new java.util.HashSet();
    if( accountsList != null ) {
	java.util.StringTokenizer st = new java.util.StringTokenizer( accountsList, "|" );
	// create a set of the account IDs in the search criteria so we can determine which 
	// to set as selected in the list

	while( st.hasMoreTokens() ) {
	    accountsSet.add( st.nextToken() );
	}
    }
    session.setAttribute( "SELECTED_ACCOUNTS_SET", accountsSet );
    
    // determine whether or not we should display the report date as well as the time
    String dateFormat = "";
    %>
    <ffi:getProperty name="UserLocale" property="DateFormat" assignTo="dateFormat"/>
    <%
    com.ffusion.beans.bankreport.BankReportDefinitions defns = (com.ffusion.beans.bankreport.BankReportDefinitions) session.getAttribute( "ReportDefinitions" );
    java.util.Iterator it = defns.iterator();
    com.ffusion.beans.bankreport.BankReportDefinition def = null;
    while( it.hasNext() ) {
    	com.ffusion.beans.bankreport.BankReportDefinition tmp = (com.ffusion.beans.bankreport.BankReportDefinition) it.next();
	if( tmp.getReportType().equals( reportType ) ) {
	    def = tmp;
	}
    }
    String updateable = (String)(def.getOptions().get( "Updateable" ));
    if( updateable.equals( "FALSE" ) ) {
    %>
    <ffi:getProperty name="UserLocale" property="DateTimeFormat" assignTo="dateFormat"/>
    <%
    }
%>

<ffi:object id="GetDefaultReportCriteriaTask" name="com.ffusion.tasks.reporting.GetDefaultReportCriteria"/>
<ffi:setProperty name="GetDefaultReportCriteriaTask" property="ReportType" value="<%= reportType %>"/>
<ffi:setProperty name="GetDefaultReportCriteriaTask" property="ReportCriteriaName" value="tmpReportCriteria"/>
<ffi:process name="GetDefaultReportCriteriaTask"/>
<ffi:removeProperty name="GetDefaultReportCriteriaTask"/>
<ffi:setProperty name="tmpReportCriteria" property="CurrentSearchCriterion" value="<%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
<ffi:getProperty name="tmpReportCriteria" property="CurrentSearchCriterionValue" assignTo="reportAccounts"/>

<% if( reportAccounts == null ) { reportAccounts = ""; } %>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_REPORT %>" />
<ffi:object id="GetReportsTask" name="com.ffusion.tasks.bankreport.GetReports" scope="request"/>
<ffi:setProperty name="GetReportsTask" property="ReportType" value="<%= reportType %>" />
<ffi:setProperty name="GetReportsTask" property="ReportsName" value="AllBankReports" />
<ffi:process name="GetReportsTask" />

<script type="text/javascript">
	function reloadAccounts( formName ) {
		// re-populates the list of accounts dynamically when the selected report changes
		document.criteriaFormName['selectedReport'].value = document.criteriaFormName['<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_REPORT %>'].value;
		document.criteriaFormName['reportChanged'].value = "true";
		document.criteriaFormName.action = "<ffi:getProperty name='FullPagesPath'/>reports/reportcriteria.jsp";
		$.ajax({
				type: "POST"
			,	url: document.criteriaFormName.action
			,	data: $('#criteriaForm').serialize()
			,	success: function(data) { 
					$('#reportCriteriaDiv').html(data);
			  	}   
		});
	}
	
	function repopulate() {
		// repopulates hidden field in main form with pipe-separated list of account IDs
		any = false;
		listField = "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_ACCOUNTS %>";
		for( i = 0; i < document.criteriaFormName['accountsList'].length; i++ ) {
			if( document.criteriaFormName['accountsList'].options[i].selected ) {
			    if( any == false ) {
				    any = true;
				    document.criteriaFormName[listField].value = document.criteriaFormName['accountsList'].options[i].value;
			    } else {
				    document.criteriaFormName[listField].value = document.criteriaFormName[listField].value + ", " + document.criteriaFormName['accountsList'].options[i].value;
			    }
			}
		}
		if( any == false ) {
			document.criteriaFormName[listField].value = "";
		}
	}
	
	ns.report.validateForm = function() {
		var reportDateArray = new Array(
		    <% 
			String reportDateStr = "";
			String comma = "";
		    %>
		    <ffi:list collection="AllBankReports" items="breport">
			<ffi:getProperty name="breport" property="GeneratedTime" assignTo="reportDateStr" />
			
			<%= comma %> "<%= reportDateStr %>"
			<%
			    comma = ",";
			%>
		    </ffi:list>
		);
		
		<ffi:cinclude value1="<%= reportAccounts %>" value2="" operator="notEquals">
		    repopulate();
		</ffi:cinclude>

		selectBoxDate = document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_REPORT %>"];
		selectedDate = reportDateArray[selectBoxDate.selectedIndex];
		
		document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>Report Date"].value = selectedDate;
		if( selectBoxDate.value == "" ) {
			$('#reportAlertDialog').find('.alertTxt').html( js_no_report_title);
			$('#reportAlertDialog').dialog('open');
			return false;
		}

		<ffi:cinclude value1="<%= reportAccounts %>" value2="" operator="notEquals">
		    else if( document.criteriaFormName["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_ACCOUNTS %>"].value == "" ) {
			    //alert( "Please select one or more accounts." );
			    //return false;
		    }
		</ffi:cinclude>
		return true;
	}
	
	function selectAllAccounts() {
		for( i = 0; i < document.criteriaFormName['accountsList'].length; i++ ) {
			document.criteriaFormName['accountsList'].options[i].selected = true;
		}
	}
		
</script>
	
	

<%-- this field is used only for displaying the report date in the header of the generated report --%>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %>Report Date" value="">

<%-- report/account --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_650"/>&nbsp;</td>
	<td align="left" colspan="3">
		<input type="hidden" name="selectedReport" value="">
		<input type="hidden" name="reportChanged" value="">
		<input type="hidden" name="reportUsesAccounts" value="<%= reportAccounts %>">
		
		<ffi:removeProperty name="GetReportsTask"/>
		<%
		    String critFieldsDisabled = "";
		    if( session.getAttribute( "AllBankReports" ) != null ) {
		       rptCount = String.valueOf( ( (com.ffusion.beans.bankreport.BankReports)session.getAttribute( "AllBankReports" ) ).size() );
		    }
		%>
		<ffi:cinclude value1="<%= rptCount %>" value2="0" operator="equals">
		    <%
		        critFieldsDisabled = "disabled";
		    %>
		</ffi:cinclude>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_REPORT %>" size="1" onchange="reloadAccounts( this.form );" <%= critFieldsDisabled %>>
			<ffi:cinclude value1="<%= rptCount %>" value2="0" operator="equals">
				<option value=""><s:text name="jsp.reports_622"/></option>
				<% com.ffusion.beans.bankreport.BankReport bankReport = 
					new com.ffusion.beans.bankreport.BankReport( java.util.Locale.getDefault() );
				   bankReport.setType( reportType );
				session.setAttribute( com.ffusion.beans.bankreport.BankReportConsts.SELECTED_BANK_REPORT,
				                         bankReport );
				%>
			</ffi:cinclude>
			<% 
			    String currentReportIDStr = "";
			    String currentReportDateStr = "";
			    int ordinal = 0;
			%>
			<ffi:list collection="AllBankReports" items="breport">
				<ffi:getProperty name="breport" property="ReportID" assignTo="currentReportIDStr" />
				<ffi:setProperty name="breport" property="GeneratedTime.Format" value="<%= dateFormat %>" />
				<ffi:getProperty name="breport" property="GeneratedTime" assignTo="currentReportDateStr" />
				
				
				<%-- if no report was selected by the user, then use the first report --%>
				<ffi:cinclude value1="<%= String.valueOf(ordinal) %>" value2="0" operator="equals">
					<ffi:cinclude value1="<%= selectedReport %>" value2="" operator="equals">
						<% 
						    // need to set a dummy BankReport object in the session, containing the correct report ID, so we can retrieve accounts
						    com.ffusion.beans.bankreport.BankReport selected = new com.ffusion.beans.bankreport.BankReport( java.util.Locale.getDefault() );
						    selected.setReportID( Integer.parseInt( currentReportIDStr ) );
						    selected.setType( reportType );
						    session.setAttribute( com.ffusion.beans.bankreport.BankReportConsts.SELECTED_BANK_REPORT, selected );
						%>
					</ffi:cinclude>
				</ffi:cinclude>
						
				<option value="<%= currentReportIDStr %>"
					<%-- if we hit a report that matches the id of the report selected by the user, then use that report --%>
					<ffi:cinclude value1="<%= selectedReport %>" value2="<%= currentReportIDStr %>" operator="equals">selected</ffi:cinclude> >
					<ffi:cinclude value1="<%= selectedReport %>" value2="<%= currentReportIDStr %>" operator="equals">
						<% 
						    // need to set a dummy BankReport object in the session, containing the correct report ID, so we can retrieve accounts
						    com.ffusion.beans.bankreport.BankReport selected = new com.ffusion.beans.bankreport.BankReport( java.util.Locale.getDefault() );
						    selected.setReportID( Integer.parseInt( currentReportIDStr ) );
						    selected.setType( reportType );
						    session.setAttribute( com.ffusion.beans.bankreport.BankReportConsts.SELECTED_BANK_REPORT, selected );
						%>
					</ffi:cinclude>
					<%= currentReportDateStr %>
				</option>
				<% ordinal++; %>
			</ffi:list>
		</select>
	</td>
</tr>
<tr>
	<ffi:cinclude value1="<%= reportAccounts %>" value2="" operator="notEquals">
	
		<td align="right" class="sectionsubhead" valign="top"><s:text name="jsp.reports_480"/>&nbsp;</td>
		<td align="left" colspan="3">
			<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bankreport.BankReportConsts.SEARCH_CRITERIA_ACCOUNTS %>" value="">
			<select class="ui-widget-content ui-corner-all" style="margin-bottom:7px;" name="accountsList" multiple size="5" onchange="repopulate();" <%= critFieldsDisabled %>>
				<ffi:object id="GetReportAccountsTask" name="com.ffusion.tasks.bankreport.GetReportAccounts" />
				<ffi:setProperty name="GetReportAccountsTask" property="ReportName" value="<%= com.ffusion.beans.bankreport.BankReportConsts.SELECTED_BANK_REPORT %>"/>
				<ffi:setProperty name="GetReportAccountsTask" property="AccountsName" value="ReportAccounts"/>
				<ffi:process name="GetReportAccountsTask" />
				<ffi:removeProperty name="GetReportAccountsTask"/>
					
				<%  String acctCount = "0";
					if( session.getAttribute( "ReportAccounts" ) != null ) {
						acctCount = String.valueOf( ( (com.ffusion.beans.accounts.Accounts)session.getAttribute( "ReportAccounts" ) ).size() );
					}
				%>
				<ffi:cinclude value1="<%= acctCount %>" value2="0" operator="equals"><option><s:text name="jsp.reports_619"/></option></ffi:cinclude>

				<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
				<ffi:list collection="ReportAccounts" items="Account1">
					<% String acctIDTemp; %>
					<ffi:getProperty name="Account1" property="ID" assignTo="acctIDTemp" />
					<option value="<ffi:getProperty name="Account1" property="ID"/>" 
						<% if( !reportChanged && ((java.util.HashSet)session.getAttribute( "SELECTED_ACCOUNTS_SET" )).contains( acctIDTemp ) ) { out.print( "selected" ); } %>
					>
						<ffi:cinclude value1="${Account1.RoutingNum}" value2="" operator="notEquals">
							<ffi:getProperty name="Account1" property="RoutingNum"/> :
						</ffi:cinclude>
						
						<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${Account1.ID}" />
						<ffi:process name="GetAccountDisplayText" />
						<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />
						
						<ffi:cinclude value1="${Account1.NickName}" value2="" operator="notEquals" >
							 - <ffi:getProperty name="Account1" property="NickName"/>
						</ffi:cinclude>
					</option>
				</ffi:list>
				<ffi:removeProperty name="GetAccountDisplayText"/>
			</select>
			<br>
			 <sj:a 
			 	 id="BankReportingSelectAllAccounts"
			     button="true" 
		         onClick="selectAllAccounts();"
		         ><s:text name="jsp.reports.SelectAllAccounts" /></sj:a>
			<%--<button class="submitbutton" type="button" onclick="selectAllAccounts();" id="BankReportingSelectAllAccounts"></button> --%>
			<img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="1" height="1" border="0" onload="repopulate();">
		</td>
	</ffi:cinclude>
</tr>
		
<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="AllBankReports"/>
