<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set name="reportsName" value="%{#parameters.reportsName}" scope="session"/>
<s:set name="reportID" value="%{#parameters.reportID}" scope="session"/>

<ffi:help id="reports_rptconfirmdelete" />

<div align="center">
			<s:form id="deleteReportForm" action="DeleteReportAction" namespace="/pages/jsp/reports" method="post" theme="simple">

			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

			<table width=350" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="left"><span class="sectionhead errorcolor"><s:text name="jsp.reports_510"/></span></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="columndata" valign="top" align="left" width="35%">
						<s:hidden name="IdentificationID" value="%{#parameters.reportID}"/>
						<span class="sectionhead labelCls"><s:text name="jsp.reports_652"/> </span>
					
						<span class="valueCls"><s:property value="%{#parameters.reportName}"/></span>
					</td>
				</tr>
				<tr>
					<td class="columndata" valign="top" align="left">
						<s:hidden name="IdentificationName" value="%{#parameters.reportName}"/>
						<s:hidden name="IdentificationDesc" value="%{#parameters.reportDesc}"/>
						<span class="sectionhead labelCls"><s:text name="jsp.default_171"/> </span>
					
						<span class="valueCls"><s:property value="%{#parameters.reportDesc}"/></span>
					</td>
				</tr>
				<tr>
					<td height="40">&nbsp;</td>
				</tr>
				<tr>
					<td  class="ui-widget-header customDialogFooter" valign="middle" align="left">

							<sj:a button="true" onClickTopics="closeDialog"><s:text name="jsp.default_82"/></sj:a>
						
							<sj:a
								formIds="deleteReportForm"
								targets="resultmessage"
								button="true"
								onBeforeTopics="beforeDeleteReportCriteria" 
								onSuccessTopics="successDeleteReportCriteria"
								onErrorTopics="errorDeleteReportCriteria"  
								onCompleteTopics="completeDeleteReportCriteria" 
							><s:text name="jsp.default_162"/></sj:a>

					</td>					
				</tr>
			</table>
			<br>
			</s:form>
</div>
<ffi:removeProperty name="reportNames"/>
<ffi:removeProperty name="reportName"/>
<ffi:removeProperty name="category"/>
<ffi:removeProperty name="fromSavedReportsJsp"/>
