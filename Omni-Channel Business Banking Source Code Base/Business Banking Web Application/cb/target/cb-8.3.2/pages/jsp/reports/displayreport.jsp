<%@ page import="com.ffusion.beans.ach.ACHReportConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<ffi:help id="reports_displayreport" className="moduleHelpClass" />
<script type="text/javascript" src="<s:url value='/web/js/reports/reports%{#session.minVersion}.js'/>"></script>

<%  boolean canSaveReport = true;
    boolean showDone = false; %>
<ffi:cinclude
	value1="${ReportData.ReportCriteria.ReportOptions.REPORTTYPE}"
	value2="<%=ACHReportConsts.RPT_TYPE_FILE_UPLOAD%>" operator="equals">
	<%  showDone = true;
	    canSaveReport = false; %>
</ffi:cinclude>
<ffi:cinclude
	value1="${ReportData.ReportCriteria.ReportOptions.REPORTTYPE}"
	value2="<%=ACHReportConsts.RPT_TYPE_FILE_IMPORT%>" operator="equals">
	<%  showDone = true;
	    canSaveReport = false; %>
</ffi:cinclude>
<ffi:cinclude
	value1="${ReportData.ReportCriteria.ReportOptions.REPORTTYPE}"
	value2="<%=ACHReportConsts.RPT_TYPE_FILE_IMPORT_BATCHES%>"
	operator="equals">
	<%  showDone = true;
	    canSaveReport = false; %>
</ffi:cinclude>


<%-- If the report is not of destination object, then the report isn't meant for this JSP.
	 The user has probably run a saved export report and has navigating back to this page.
	 If that has happened, we should load the saved ReportData from session and use it, as
	 the current data won't display here --%>
<ffi:setProperty name="needReload" value="false" />
<ffi:setProperty name="ReportData"	property="ReportCriteria.CurrentReportOption"
	value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
<ffi:cinclude value1="${GenerateReportBase.LastDestination}" value2=""	operator="equals">
	<%-- GenerateReportBase.LastDestination is empty, so it will use the ReportCriteria destination --%>
	<ffi:cinclude
		value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>"
		value2="${ReportData.ReportCriteria.CurrentReportOptionValue}"
		operator="notEquals">
		<ffi:setProperty name="needReload" value="true" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${GenerateReportBase.LastDestination}" value2=""
	operator="notEquals">
	<%-- GenerateReportBase.LastDestination is not empty, so it will use its destination --%>
	<ffi:cinclude
		value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>"
		value2="${GenerateReportBase.LastDestination}" operator="notEquals">
		<ffi:setProperty name="needReload" value="true" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${needReload}" value2="true" operator="equals">
	<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportDataSaved" );
	if( report != null ){
	    session.setAttribute( "ReportData", report );
	    //need to regenerate the data, since it has been removed
	%>
	<ffi:object id="GenerateSavedReportBase"
		name="com.ffusion.tasks.reporting.GenerateReportBase" />
	<ffi:setProperty name="ReportData"
		property="ReportCriteria.CurrentReportOption"
		value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
	<ffi:setProperty name="GenerateSavedReportBase" property="Destination"
		value="${ReportData.ReportCriteria.CurrentReportOptionValue}" />
	<ffi:setProperty name="ReportData"
		property="ReportCriteria.CurrentReportOption"
		value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" />
	<ffi:setProperty name="GenerateSavedReportBase" property="Format"
		value="${ReportData.ReportCriteria.CurrentReportOptionValue}" />
	<ffi:process name="GenerateSavedReportBase" />
	<%
	}
%>
</ffi:cinclude>
<ffi:removeProperty name="needReload" />

<script type="text/javascript"><!--
	function validateName() {
<ffi:cinclude value1="${ReportData.ReportID.ReportName}" value2="" operator="equals">
		// Validation has been removed in order to support i18n specific validation.
		// Validation is already done by the tasks AddReport and ModifyReport.
</ffi:cinclude>
		return true;
	}
// --></script>


<%-- Currently the data source load start time, and data source load end time search criteria are used only internally
     by default, therefore, they will be hidden from the user --%>
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATASOURCE_LOAD_START_TIME%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATASOURCE_LOAD_END_TIME%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TIME_FORMAT%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATE_FORMAT%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />



<div align="center" id="rptDisplayMain">
<% String opt_pagewidth; %> 
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentReportOption"
	value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>" />
<ffi:getProperty name="ReportData"
	property="ReportCriteria.CurrentReportOptionValue"
	assignTo="opt_pagewidth" /> 
	<%
		String real_pagewidth = String.valueOf( Integer.parseInt( opt_pagewidth ) + 2 );

		session.setAttribute( "real_pagewidth", real_pagewidth );
	%>


<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0"
	operator="notEquals">
	<form action="reportcriteria.jsp" method="post" name="FormName">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" /> 
		<input type="hidden" name="reportID"  value="<ffi:getProperty name="ReportData" property="ReportID.ReportID"/>">
		<input type="hidden" name="resetReportOptions" value="TRUE">
	
		<table width="750px" border="0">
			<tr>
				<td style="padding-top: 0.8em; width: 50%" align="left" valign="top"><span
					style="padding-left: 1.3em" class="sectionsubhead"><s:text name="jsp.default_283.1"/>:&nbsp;</span>
				<span style="padding: 0px;" class="columndata"><ffi:getProperty
					name="ReportData" property="ReportID.ReportName" /></td>
				<td style="padding-top: 0.8em; width: 50%" align="left" valign="top"><span
					style="padding-left: 0" class="sectionsubhead"><s:text name="jsp.default_170"/>:&nbsp;</span>
				<span style="padding: 0px;" class="columndata"><ffi:getProperty
					name="ReportData" property="ReportID.Description" /></span></td>
			</tr>
		</table>
	</form>
</ffi:cinclude>
<table width="<ffi:getProperty name="real_pagewidth"/>" border="0"
	cellspacing="0" cellpadding="0">
	
	<tr>
		<td class="tbrd_b">&nbsp;</td>
	</tr>
	<tr>
		<td class="tbrd_lr dkback"><ffi:object id="exportHeader"
			name="com.ffusion.tasks.reporting.ExportHeaderOptions" /> <ffi:setProperty
			name="exportHeader" property="ReportName" value="ReportData" /> <ffi:setProperty
			name="exportHeader" property="ExportFormat"
			value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
		<ffi:process name="exportHeader" /> <ffi:getProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>"
			encode="false" /> <ffi:removeProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>" />
		<ffi:removeProperty name="exportHeader" /></td>
	</tr>
	<tr>
		<td class="tbrd_ltr"><br>
					
		<ffi:object id="getPagedReport" name="com.ffusion.tasks.reporting.GetPagedReport"/>
		<ffi:setProperty name="getPagedReport" property="ReportName" value="ReportData"/>
		<ffi:setProperty name="getPagedReport" property="PagedReportName" value="PagedReport"/>
		<ffi:process name="getPagedReport" />

		<div id="pagedReportDiv"></div>
		</td>
	</tr>
	<tr>
		<td class="tbrd_ltr dkback"><ffi:object id="exportFooter"
			name="com.ffusion.tasks.reporting.ExportFooterOptions" /> <ffi:setProperty
			name="exportFooter" property="ReportName" value="ReportData" /> <ffi:setProperty
			name="exportFooter" property="ExportFormat"
			value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
		<ffi:process name="exportFooter" /> <ffi:getProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>"
			encode="false" /> <ffi:removeProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>" />
		<ffi:removeProperty name="exportFooter" /></td>
	</tr>
	<tr>
		<td class="tbrd_t">&nbsp;</td>
	</tr>
	<ffi:removeProperty name="opt_pagewidth" />
	<ffi:removeProperty name="real_pagewidth" />
	
	
		<table>
			<tr align="right">
				<td align="right">
				
				<sj:a 
					id="getFirstPageID" 
					button="true" 
					onClick="getFirstPage()"
					>
					<<
				</sj:a>
				<sj:a 
					id="getPreviousPageID" 
					button="true" 
					onClick="getPreviousPage()"
					>
					<
				</sj:a>
				<sj:a 
					id="getNextPageID" 
					button="true" 
					onClick="getNextPage()"
					>
					>
				</sj:a>
				<sj:a 
					id="getLastPageID" 
					button="true" 
					onClick="getLastPage()"
					>
					>>
				</sj:a>
				
				<select id="pagedSelectID" onChange="goToCertainPage(this.options[this.selectedIndex].value);">
					
					<%  
						Integer pagedReportTotalNumber = (Integer)session.getAttribute( com.ffusion.tasks.reporting.ReportingConsts.PAGED_REPORT_TOTAL_NUMBER );	
						for(int i = 0; i < pagedReportTotalNumber.intValue(); i++){
					%>
						<option value="<%= i %>" selected><%= i+1 %></option>
							
					<% } %>					
				</select >

				</td>
			</tr>
		</table>
		
		<br>
		

</table>

 <ffi:setProperty name="BackURL" value="${tmpBackURL}" />
		<div align="center" id="rptDisplayControlls">
		<%-- Show Print Ready button only if the User is entitled to "Export Reports" --%>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
			<sj:a indicator="indicator" button="true" onClick="ns.report.printerReady();" theme="simple"
			><s:text name="jsp.default_332"/></sj:a>
		</ffi:cinclude>
		<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
			<sj:a button="true" onClick="ns.report.saveReport();"
			><s:text name="jsp.reports_658"/></sj:a>
		</ffi:cinclude>
	
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
		
	<sj:a indicator="indicator" button="true"
		openDialog="innerexportreportdialog1" theme="simple"
		><s:text name="jsp.default_195"/></sj:a>
	<sj:dialog id="innerexportreportdialog1" autoOpen="false" modal="true"
		cssClass="reportDisplayInnerDialog staticContentDialog" title="%{getText('jsp.default_198')}" height="180" width="450" overlayColor="#000"
		overlayOpacity="0.7" resizable="false">
		<div align="center">
		<s:form id="innerExportReportForm1"
			theme="simple">
			<input type="hidden" name="GenerateReportBase.Destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>">
			<input type="hidden" name="CSRF_TOKEN"	value="<ffi:getProperty name='CSRF_TOKEN'/>" />
			<input type="hidden" name="returnjsp"   value="false" />
			<s:hidden key="reportID" />
			<b>
				<p id="exportDialogMessage"><s:text name="jsp.reports_696"/></p>
			</b>
			<select class="txtbox" name="GenerateReportBase.Format">
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option	value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"
						><s:text name="jsp.default_231"/></option>
				</ffi:cinclude>
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>"
						><s:text name="jsp.default_105"/></option>
				</ffi:cinclude>
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>"
					><s:text name="jsp.default_402"/></option>
				</ffi:cinclude>

				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>"
					><s:text name="jsp.default_323"/></option>
				</ffi:cinclude>

				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>"
						><s:text name="jsp.default_59"/></option>
				</ffi:cinclude>

				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>"
						><s:text name="jsp.default_325"/></option>
				</ffi:cinclude>
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="true" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>"
						><s:text name="jsp.default_124"/></option>
				</ffi:cinclude>
			</select>
			<br />
			<br />
			<input id="exportReportId" type="hidden" name="exportReport" value="false">
			<div class="ui-widget-header customDialogFooter">
				<sj:a id="exportDialogCancelBtn1" button="true"
					onclick="{ns.common.closeDialog();}" theme="simple"><s:text name="jsp.default_82"/></sj:a>
	
				<%--This request for generating report may take long time, hence explicitly making ajax timeout as infinite. --%>
				<sj:a id="exportDialogSubmitBtn1"
					onClickTopics="onClickExportReport"
					timeout="0" 
					effect="pulsate" value="Export" indicator="indicator" button="true"
					theme="simple" ><s:text name="jsp.default_195"/></sj:a>
			</div>
		</s:form></div>
	</sj:dialog>
	<script>
		$("#innerexportreportdialog1 select").selectmenu({width: 150});
	</script>
	</ffi:cinclude>
	
	<sj:a button="true" id="cancelDisplayReportBtn" onClickTopics="onCancelDisplayReportClick"
		><s:text name="jsp.default_82"/></sj:a> 
		</br></br>
</div>

<script type="text/javascript">
ns.report.printReport = function(){
	$('#printerReadyReportDialog').print();
}
</script>
		
 	<sj:dialog id="printerReadyReportDialog" autoOpen="false" modal="true"
	cssClass="reportDisplayInnerDialog" title="%{getText('jsp.reports_639')}" height="600" width="700" overlayColor="#000"
	overlayOpacity="0.7"
	buttons="{ 
    		 	'%{getText(\"jsp.default_82\")}':function() { ns.common.closeDialog('printerReadyReportDialog'); } 
			,	'%{getText(\"jsp.default_331\")}': function(){ ns.report.printReport();}
   		}">
   	</sj:dialog>
</div>

<%-- Dialog Definitions --%>

<%-- since the report data may be very large, we remove it from session.  the report criteria
     should be left alone to avoid breaking the "save report" page --%>
<% ( (com.ffusion.beans.reporting.Report)pageContext.findAttribute( "ReportData" ) ).setReportResult( null ); %>

<%-- save copy of ReportData in case user navigates back from non-display page report--%>
<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportData" );
	com.ffusion.beans.reporting.Report report2 = (com.ffusion.beans.reporting.Report) report.clone();
    session.setAttribute( "ReportDataSaved", report2 );
%>
<ffi:removeProperty name="GenerateSavedReportBase" />

<script>

	$(document).ready(function(){
		$.ajax({
			url:  '<ffi:urlEncrypt url="/cb/pages/jsp/reports/GetPagedReportAction.action"/>',
			success: function(data){
				processData(data);
			}
		});
		
	});

	
	$.subscribe('onClickExportReport', function(event,data){
		document.getElementById('exportReportId').value = "true";
		ns.report.exportReport('/cb/pages/jsp/reports/UpdateReportCriteriaAction_generateReport.action', $('#innerExportReportForm1')[0]);
		ns.common.closeDialog("innerexportreportdialog1");
	});
	
getNextPage = function(){
		$.ajax({
			url:  '<ffi:urlEncrypt url="/cb/pages/jsp/reports/GetPagedReportAction.action?pageFlag=next"/>',			
			success: function(data){
				processData(data);
			}
		});
	}
	getPreviousPage = function(){
		$.ajax({
			url:  '<ffi:urlEncrypt url="/cb/pages/jsp/reports/GetPagedReportAction.action?pageFlag=previous"/>',
			success: function(data){
				processData(data);
			}
		});
	}
	getFirstPage = function(){
		$.ajax({
			url:  '<ffi:urlEncrypt url="/cb/pages/jsp/reports/GetPagedReportAction.action?pageFlag=first"/>',
			success: function(data){
				processData(data);
			}
		});
	}
	getLastPage = function(){
		$.ajax({
			url:  '<ffi:urlEncrypt url="/cb/pages/jsp/reports/GetPagedReportAction.action?pageFlag=last"/>',
			success: function(data){
				processData(data);
			}
		});
	}
	
	goToCertainPage = function(pageNumber){
		$.ajax({
			url:  "/cb/pages/jsp/reports/GetPagedReportAction.action" ,
			type: "POST",
			data: {pageFlag:pageNumber, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			success: function(data){
				processData(data);
			}
		});
	}
	
	processData = function(data){
	
		var pageIndex = data.indexOf("@");
		var pageNumber = data.substring(0, pageIndex);
		var pageContent = data.substring(pageIndex + 1);
		$('#pagedReportDiv').html(pageContent);
		changePageNumber(pageNumber);
		
	}
	
	changePageNumber = function(pageNumber){
	
		$("#pagedSelectID option[value=" + pageNumber + "]").attr("selected",true);
		if(ns.common.isInitialized($('#pagedSelectID'),'ui-selectmenu')){
			$('#pagedSelectID').selectmenu("destroy");	
		}		
		$('#pagedSelectID').selectmenu({width:50});

	}
	
	
</script>