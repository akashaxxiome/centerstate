<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Transfer History Report" operator="equals">
	<ffi:help id="Transaction_History_Reports" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Transfer Detail Report" operator="equals">
	<ffi:help id="transfer-detail_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Pending Transfer Report" operator="equals">
	<ffi:help id="pending-transfer_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Transfer By Status" operator="equals">
	<ffi:help id="transferby-status_report" className="moduleHelpClass" />
</ffi:cinclude>

<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.EXTRA_PREFIX %><%= com.sap.banking.common.constants.ServiceConstants.SERVICE %>" value="<%= com.ffusion.tasks.banking.Task.BANKING %>"/>

<%-- If there is no transfer accounts in session, get transfer accounts --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS ) == null ) { %>
	<ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="session"/>
        <ffi:setProperty name="GetTransferAccounts" property="Reload" value="true"/>
		<ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="BankingAccounts"/>
	<ffi:process name="GetTransferAccounts"/>
<% } %>

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="">
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayExtTransferEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.transfer.external.sameday.support"/> 
	<ffi:process name="GetBPWProperty"/>
</ffi:cinclude>

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="true">
	<ffi:getProperty name='SameDayExtTransferForBusiness'/>
	 <ffi:cinclude value1="${SameDayExtTransferForBusiness}" value2="">	 
		<ffi:object id="CheckSameDayEnabledForBusiness" name="com.ffusion.tasks.util.CheckSameDayEnabledForBusiness" scope="session" />
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="BusinessID" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="ModuleName" value="<%=com.ffusion.ffs.bpw.interfaces.ACHConsts.ACH_BATCH_EXT_TRANSFER%>"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="SessionName" value="SameDayExtTransferForBusiness"/>
		<ffi:process name="CheckSameDayEnabledForBusiness"/>
	 </ffi:cinclude>
 </ffi:cinclude>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<%-- account --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_217"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_FROM_ACCOUNT %>" />
		<select id="transferHistAccDropdown"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_FROM_ACCOUNT %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value=""
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		<ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" property="Filter" value="All"/>
		<ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" items="acct">
			<ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <% boolean displayEntitled = true; %>
            <ffi:cinclude value1="${acct.CoreAccount}" value2="<%=com.ffusion.beans.accounts.Account.CORE%>" operator="notEquals">
                <ffi:setProperty name="acct" property="IsFilterableCheck" value="<%=com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_FROM_FILTER%>"/>
                <ffi:cinclude value1="${acct.IsFilterable}" value2="false">
                    <% displayEntitled = false; %>
                </ffi:cinclude>
            </ffi:cinclude>
            <ffi:cinclude value1="${acct.CoreAccount}" value2="<%=com.ffusion.beans.accounts.Account.CORE%>" operator="equals">
                <ffi:setProperty name="acct" property="IsFilterableCheck" value="<%=com.ffusion.beans.accounts.AccountFilters.TRANSFER_FROM_FILTER%>"/>
                <ffi:cinclude value1="${acct.IsFilterable}" value2="false">
                    <% displayEntitled = false; %>
                </ffi:cinclude>
            </ffi:cinclude>
            <% if (displayEntitled) { %>
                <option value="<ffi:getProperty name='tmp_val'/>"
                    <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
                    <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
                >
                    <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                        <ffi:getProperty name="acct" property="RoutingNum"/> :
                    </ffi:cinclude>

                    <ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
                    <ffi:process name="GetAccountDisplayText" />
                    <ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

                    <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                         - <ffi:getProperty name="acct" property="NickName"/>
                    </ffi:cinclude>
                     - <ffi:getProperty name="acct" property="CurrencyCode"/>
                </option>
            <% } %>
		</ffi:list>
		<ffi:removeProperty name="tmp_val"/>
		<ffi:removeProperty name="acct" />
		<ffi:removeProperty name="GetAccountDisplayText"/>
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- final housekeeping --%>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" />
<ffi:removeProperty name="GetTransferAccounts"/>
<ffi:removeProperty name="CriterionListChecker" />

<script>
	$("#transferHistAccDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>