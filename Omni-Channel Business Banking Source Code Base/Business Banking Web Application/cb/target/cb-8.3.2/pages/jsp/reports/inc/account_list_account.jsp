<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="reports_account_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
							<%-- <td width="15%" align="center"><span class="sectionsubhead"><s:text name="jsp.home.column.label.action"/></span></td> --%>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MGMT_REPORTING %>">
				
					<ffi:object id="CheckPerAccountReportingEntitlements"
						name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" />
					<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
					<ffi:setProperty name="CheckPerAccountReportingEntitlements"
						property="AccountsName" value="BankingAccounts" />
					<ffi:process name="CheckPerAccountReportingEntitlements" />
				
					<%-- IF there is no reports to display. --%>
					<ffi:cinclude
						value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryAnyDataClassification}"
						value2="false" operator="equals">
						
						<tr>
							<%-- <td class="tbrd_l " nowrap><span class="columndata"><ffi:getProperty
								name="TypeColumnValue" encode="false" /></span></td> --%>
							<ffi:setProperty name="TypeColumnValue" value="" />
							<!-- <td class="columndata "></td>
							<td class="tbrd_l columndata" nowrap>&nbsp;</td> -->
							<td colspan="3" align="center"><s:text name="jsp.reports_679"/></td>
						</tr>
					</ffi:cinclude>		<%-- end include on report NOT entitled to detail or summary for either intra day or previous day --%>
				
					<%-- IF there are some reports to display. --%>
					<ffi:cinclude
						value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryAnyDataClassification}"
						value2="true" operator="equals">
						<tr height="1">
							<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
							<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
							<s:set var="tmpI18nStr" value="%{getText('jsp.reports_602')}" scope="request" /><ffi:setProperty name="TypeColumnValue"
								value="${tmpI18nStr}" />
						</tr>
				
						<ffi:object      id="CheckReportEntitlement"  name="com.ffusion.tasks.reporting.CheckReportEntitlement" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
								value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_SUMMARY %>" />
						<ffi:process 	 name="CheckReportEntitlement" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value="" />
				
						<%
							HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
						%>
				
						<%-- TO show Balance Summary Row --%>
						<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}"
							value2="false" operator="equals">
							<ffi:cinclude
								value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}"
								value2="true" operator="equals">
							<%
								String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_SUMMARY;
								session.setAttribute("category", category);
								ArrayList reportNames = (ArrayList)(hashmap.get(category));
								int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
								session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
								session.setAttribute("reportNames", reportNames);
								session.setAttribute("rowTitle",  com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.reports.Balance_Summary", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
							%>
							<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
							</ffi:cinclude>		<%-- End entitled to Summary reports for any data dlassification --%>
						</ffi:cinclude>		<%-- End entitled to balance summary report --%>
						
						
						<%-- READY TO show 'Balance Detail Report' Row --%>
						<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied"	value="false" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportType"   	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_DETAIL %>" />
						<ffi:process 	 name="CheckReportEntitlement" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" 	value="" />
						
						<%-- IF show 'Balance Detail Report' Row --%>
						<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}"
							value2="false" operator="equals">
							<ffi:cinclude
								value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}"
								value2="true" operator="equals">
								<%
									String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_DETAIL;
									session.setAttribute("category", category);
									ArrayList reportNames = (ArrayList)(hashmap.get(category));
									int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
									session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
									session.setAttribute("reportNames", reportNames);
									session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.reports.Balance_Detail_Report", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
								%>
								<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
							</ffi:cinclude>		<%-- End entitled to Detail reports for any data dlassification --%>
						</ffi:cinclude>		<%-- End entitled to balance detail report --%>
						
						
						<%-- READY TO show 'Balance Summary and Detail' Row --%>
						<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied"	value="false" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
							value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_SUMMARY_AND_DETAIL %>" />
						<ffi:process     name="CheckReportEntitlement" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value="" />
				
						<%-- IF TO show 'Balance Summary and Detail' Row --%>
						<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailAnyDataClassification}"
								value2="true" operator="equals">
								<%
									String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_SUMMARY_AND_DETAIL;
									session.setAttribute("category", category);
									ArrayList reportNames = (ArrayList)(hashmap.get(category));
									int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
									session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
									session.setAttribute("reportNames", reportNames);
									session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.reports.Balance_Summary_and_Detail", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
									%>
									<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
							</ffi:cinclude>		<%-- End entitled to Summary and Detail reports for any data dlassification --%>
						</ffi:cinclude>		<%-- End entitled to balance summary and detail report --%>
						
						
						<%-- READY TO show 'Deposit Detail' Row --%>
						<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied"	value="false" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
							value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_DEPOSIT_DETAIL %>" />
						<ffi:process 	 name="CheckReportEntitlement" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportClear"	value="" />
						
						<%-- IF TO show 'Deposit Detail' Row --%>
						<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}"
							value2="false" operator="equals">
							<ffi:cinclude
								value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}"
								value2="true" operator="equals">
								<%
									String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_DEPOSIT_DETAIL;
									session.setAttribute("category", category);
									ArrayList reportNames = (ArrayList)(hashmap.get(category));
									int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
									session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
									session.setAttribute("reportNames", reportNames);
									session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.reports.Deposit_Detail", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
									%>
									<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
								
							</ffi:cinclude>		<%-- End entitled to Detail reports for any data dlassification --%>
						</ffi:cinclude>		<%-- End entitled to deposit detail report --%>
						
						
						<%-- READY TO show 'Deposit Detail' Row --%>	
						<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
							value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_TRANSACTION_DETAIL %>" />
						<ffi:process 	 name="CheckReportEntitlement" />
						<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value="" />
						
						<%-- IF TO show 'Deposit Detail' Row --%>	
						<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}"
							value2="false" operator="equals">
							<ffi:cinclude
								value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}"
								value2="true" operator="equals">
								<%
									String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_TRANSACTION_DETAIL;
									session.setAttribute("category", category);
									ArrayList reportNames = (ArrayList)(hashmap.get(category));
									int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
									session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
									session.setAttribute("reportNames", reportNames);
									session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.account_214", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
									%>
									<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
							</ffi:cinclude>		<%-- End entitled to Detail reports for any data dlassification --%>
						</ffi:cinclude>		<%-- End entitled to transaction detail report --%>
						
						
						<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied"	value="false" />
						
						<%-- Include the report list part 2 --%>
						<s:include	value="%{#session.PagesPath}reports/inc/account_list_account2.jsp" />
						
					</ffi:cinclude>		<%-- End is entitled to detail or summary on intra day or previous day --%>
				</ffi:cinclude>		<%-- End is entitled to management (information) reporting --%>
				</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>


<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="CheckReportEntitlement" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="GetReportsIDsByCategoryBalDetail" />
<ffi:removeProperty name="GetReportsIDsByCategoryTranDetail" />
<ffi:removeProperty name="GetReportsIDsByCategoryBalSum" />
<ffi:removeProperty name="GetReportsIDsByCategoryDepDetail" />
<ffi:removeProperty name="reportNames" />
<ffi:removeProperty name="reportNamesSize" />
<ffi:removeProperty name="category" />
