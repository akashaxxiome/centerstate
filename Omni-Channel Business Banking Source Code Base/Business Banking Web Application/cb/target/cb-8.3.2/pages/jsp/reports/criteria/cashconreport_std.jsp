<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- todo: Get CashCon companies --%>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies"/>
<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="false"/>
<ffi:process name="GetACHCompanies"/>
<ffi:setProperty name="${GetACHCompanies.CompaniesInSessionName}" property="SortedBy" value="CONAME" />

<%-- todo: Get Divisions entitled to --%>

<%-- todo: Get Locations for division selected --%>

<%-- CashCon Company --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_86"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY %>" />
		<%
// todo: change to CashConCompany when exists
			com.ffusion.beans.ach.ACHCompany company = null;
		%>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_493"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
				<option value="<ffi:getProperty name='company' property='CompanyID'/>"
				<ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
					<% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
					selected
				</ffi:cinclude> >
					<ffi:getProperty name="company" property="CompanyName"/>
				</option>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- Divisions --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_574"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" />
		<%
// todo: change to CashConCompany when exists
			com.ffusion.beans.ach.ACHCompany company1 = null;
		%>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_494"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
				<option value="<ffi:getProperty name='company' property='CompanyID'/>"
				<ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
					<% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
					selected
				</ffi:cinclude> >
					<ffi:getProperty name="company" property="CompanyName"/>
				</option>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- Locations --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_608"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_LOCATION %>" />
		<%
// todo: change to CashConCompany when exists
			com.ffusion.beans.ach.ACHCompany company2 = null;
		%>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_496"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
				<option value="<ffi:getProperty name='company' property='CompanyID'/>"
				<ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
					<% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
					selected
				</ffi:cinclude> >
					<ffi:getProperty name="company" property="CompanyName"/>
				</option>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- housekeeping --%>
<ffi:removeProperty name="${GetACHCompanies.CompaniesInSessionName}" />
<ffi:removeProperty name="GetACHCompanies" />

