<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="exception-summary_report" className="moduleHelpClass" />
<s:include value="%{#session.PagesPath}common/checkAmount_js.jsp"/>
<script type="text/javascript"><!--
ns.report.validateForm = function( formName )
{
	return true;
}
// --></script>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<ffi:object id="GetPPaySummaries" name="com.ffusion.tasks.positivepay.GetSummaries" scope="session"/>
<ffi:process name="GetPPaySummaries"/>

<tr>
	<td align="right" valign="top" nowrap><span class="sectionsubhead"><s:text name="jsp.default_15"/></span></td>
	<td align="left" colspan="3" nowrap>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_ACCOUNT %>"/>
		<select  style="width:350px;" id="account" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_ACCOUNT %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.reports_488"/>
			</option>
			
			<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
			<ffi:list collection="PPaySummaries" items="Summary1">
				<% 
					String accID = null;
					String bankID = null;
					String routingNum = null;
				%>
				<ffi:getProperty name="Summary1" property="PPayAccount.AccountID" assignTo="accID"/>
				<ffi:getProperty name="Summary1" property="PPayAccount.BankID" assignTo="bankID"/>
				<ffi:getProperty name="Summary1" property="PPayAccount.RoutingNumber" assignTo="routingNum"/>
				<option value="<ffi:getProperty name="accID"/>:<ffi:getProperty name="bankID"/>:<ffi:getProperty name="routingNum"/>"
					<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${Summary1.PPayAccount.AccountID}:${Summary1.PPayAccount.BankID}:${Summary1.PPayAccount.RoutingNumber}" />
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:cinclude value1="${Summary1.PPayAccount.RoutingNumber}" value2="" operator="equals">
						<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${Summary1.PPayAccount.AccountID}" />
						<ffi:process name="GetAccountDisplayText" />
						<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" /> -

						<ffi:cinclude value1="${Summary1.PPayAccount.NickName}" value2="" operator="notEquals" >
							<ffi:getProperty name="Summary1" property="PPayAccount.NickName"/> -
						</ffi:cinclude>
						<ffi:getProperty name="Summary1" property="PPayAccount.CurrencyType"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Summary1.PPayAccount.RoutingNumber}" value2="" operator="notEquals">
						<ffi:getProperty name="Summary1" property="PPayAccount.RoutingNumber"/> :

						<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${Summary1.PPayAccount.AccountID}" />
						<ffi:process name="GetAccountDisplayText" />
						<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" /> -

						<ffi:cinclude value1="${Summary1.PPayAccount.NickName}" value2="" operator="notEquals" >
							<ffi:getProperty name="Summary1" property="PPayAccount.NickName"/> -
						</ffi:cinclude>
						<ffi:getProperty name="Summary1" property="PPayAccount.CurrencyType"/>
					</ffi:cinclude>
				</option>
			</ffi:list>
			<ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<% String tempValue; %>
<tr>
	<td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.reports_534"/></span></td>
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_START_CHECK_NUMBER %>"/>
		<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="tempValue"/>
		<input style="width:73px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_START_CHECK_NUMBER %>" value="<ffi:getProperty name="tempValue"/>" size="12" maxlength="<%= com.ffusion.beans.positivepay.PPayRptConsts.CHECK_NUMBER_MAXLENGTH %>">
		<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp; 
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_END_CHECK_NUMBER %>"/>
		<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="tempValue"/>
		<input style="width:74px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_END_CHECK_NUMBER %>" value="<ffi:getProperty name="tempValue"/>" size="12" maxlength="<%= com.ffusion.beans.positivepay.PPayRptConsts.CHECK_NUMBER_MAXLENGTH %>">
	<%-- </td>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
	<td> --%>
	
	    <span class="reportsCriteriaContent"> <s:text name="jsp.default_43"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_MINIMUM_AMOUNT %>" />
		<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="tempValue"/>
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_MINIMUM_AMOUNT %>" value="<ffi:getProperty name="tempValue"/>" size="12">
		<span class="columndata">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp;
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_MAXIMUM_AMOUNT %>" />
		<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="tempValue"/>
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_MAXIMUM_AMOUNT %>" value="<ffi:getProperty name="tempValue"/>" size="12">
	</td>
</tr>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_690"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_CHECK_VOID_STATUS %>" />
		<select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_CHECK_VOID_STATUS %>">
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_ALL_CHECKS %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_ALL_CHECKS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_570"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_VOID_CHECKS %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_VOID_CHECKS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_5721"/> 
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_NO_VOID_CHECKS %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_VALUE_NO_VOID_CHECKS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_572"/>
			</option>
		</select>
	</td>
</tr>

<tr>	
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_NUMBER %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_NUMBER %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_535"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_94"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_REJECT_REASON %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_REJECT_REASON %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_349"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
		</select>
		
		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_NUMBER %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_NUMBER %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_535"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_CHECK_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_94"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_REJECT_REASON %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_REJECT_REASON %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_349"/>
			</option>
			<option value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.positivepay.PPayRptConsts.SORT_CRITERIA_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
		</select>
	</td>
</tr>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_349"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_REJECT_REASON %>" />
		<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="tempValue"/>
		<input style="width:186px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.positivepay.PPayRptConsts.SEARCH_CRITERIA_REJECT_REASON %>" value="<ffi:getProperty name="tempValue"/>" size="30" maxlength="<%= com.ffusion.beans.positivepay.PPayRptConsts.REJECT_REASON_MAXLENGTH %>">
	</td>
</tr>


<ffi:removeProperty name="GetPPaySummaries"/>
<ffi:removeProperty name="CriterionListChecker"/>
<script>
	$("#account").extmultiselect({header: ""}).multiselectfilter();
</script>