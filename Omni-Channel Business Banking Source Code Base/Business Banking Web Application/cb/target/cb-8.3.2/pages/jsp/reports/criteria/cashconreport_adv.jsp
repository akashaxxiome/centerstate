<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- todo: Get CashCon companies --%>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies"/>
<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="false"/>
<ffi:process name="GetACHCompanies"/>
<ffi:setProperty name="${GetACHCompanies.CompaniesInSessionName}" property="SortedBy" value="CONAME" />

<%-- todo: Get Divisions entitled to --%>

<%-- todo: Get Locations for division selected --%>

<%-- CashCon Company --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_86"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY %>" />
		<%
// todo: change to CashConCompany when exists
			com.ffusion.beans.ach.ACHCompany company = null;
		%>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_493"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
				<option value="<ffi:getProperty name='company' property='CompanyID'/>"
				<ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
					<% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
					selected
				</ffi:cinclude> >
					<ffi:getProperty name="company" property="CompanyName"/>
				</option>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- Divisions --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_574"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" />
		<%
// todo: change to CashConCompany when exists
			com.ffusion.beans.ach.ACHCompany company1 = null;
		%>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_494"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
				<option value="<ffi:getProperty name='company' property='CompanyID'/>"
				<ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
					<% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
					selected
				</ffi:cinclude> >
					<ffi:getProperty name="company" property="CompanyName"/>
				</option>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- Locations --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_608"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_LOCATION %>" />
		<%
// todo: change to CashConCompany when exists
			com.ffusion.beans.ach.ACHCompany company2 = null;
		%>
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CASHCON_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_496"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
				<option value="<ffi:getProperty name='company' property='CompanyID'/>"
				<ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
					<% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
					selected
				</ffi:cinclude> >
					<ffi:getProperty name="company" property="CompanyName"/>
				</option>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- Amount, sort 1 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" />
		<input class="txtbox" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">

		<span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1"/>&nbsp;</span>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" />
		<input class="txtbox" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
	</td>

	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_67"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
		</select>
	</td>
</tr>

<%-- Type, sort 2 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_67"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_BATCH_TYPE %>" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_BATCH_TYPE %>" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_503"/>
			</option>
			<%
			if( company==null ) {
			%>
			<ffi:list collection="ACHClassCodes" items="ACHClass">
				<option value="<ffi:getProperty name='ACHClass'/>" <ffi:cinclude value1="${ACHClass}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
					<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${ACHClass}"/>
					<ffi:getProperty name="ACHResource" property="Resource"/>
				</option>
			</ffi:list>
			<%
			} else {
				java.util.ArrayList entryClasses = company.getEntryClasses();
				if( entryClasses != null ){
					for( int i=0; i<entryClasses.size(); i++ ) {
						String currValue = entryClasses.get(i).toString();
			%>
			<ffi:list collection="ACHClassCodes" items="ACHClass">
				<option value="<%= currValue %>" <ffi:cinclude value1="<%= currValue %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
					<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc<%= currValue %>"/>
					<ffi:getProperty name="ACHResource" property="Resource"/>
				</option>
			</ffi:list>
			<%
					}
				}
			}
			%>
		</select>
	</td>

	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_668"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_67"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
		</select>
	</td>
</tr>

<%-- Status --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_522"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_BATCH_STATUS %>" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_BATCH_STATUS %>" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_500"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_SCHEDULED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_SCHEDULED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_660"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_TRANSFERED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_TRANSFERED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_647"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_FAILED_TO_TRANSFER ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_FAILED_TO_TRANSFER ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_582"/>
			</option>
		</select>
	</td>

	<td>
	</td>
	<td>
	</td>
</tr>

<%-- housekeeping --%>
<ffi:removeProperty name="${GetACHCompanies.CompaniesInSessionName}" />
<ffi:removeProperty name="GetACHCompanies" />
