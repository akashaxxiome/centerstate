<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="user-entitlement_report" className="moduleHelpClass" />
<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.default_225"/></td>
	<td class="sectionsubhead" colspan="3">
		<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
		<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
		<ffi:process name="GetGroupsAdministeredBy"/>
		<ffi:removeProperty name="GetGroupsAdministeredBy"/>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>"/>
		<%-- <ffi:getProperty name="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/> --%>
		<select style="width:240px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>" onchange="ns.report.refresh();">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" 
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
				<s:text name="jsp.reports_495"/>
			</option>

<%
	String bizEntitlementGroupId = "";
%>
			
			<ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
				<ffi:setProperty name="group_pre" value=""/>
				
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
					<ffi:setProperty name="group_pre" value="&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Group" operator="equals">
					<ffi:setProperty name="group_pre" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>

				<option value="<ffi:getProperty name="ENT_GROUPS_1" property="GroupId"/>" 
				<ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" assignTo="bizEntitlementGroupId" /> 
					<ffi:getProperty name="group_pre" encode="false"/>
					<ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
					<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
				</option>
				
				

				
			</ffi:list>
			
			
		</select>
	</td>
</tr>
	<tr>
	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="equals">
		<ffi:object id="GetSourceEntitlementProfileForMemberClone" name="com.ffusion.efs.tasks.entitlements.GetEntitlementProfilesInGroup" scope="session"/>
		<ffi:setProperty name="GetSourceEntitlementProfileForMemberClone" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups"/>
		<ffi:setProperty name="GetSourceEntitlementProfileForMemberClone" property="sessionName" value="EntitlementProfilesForClone"/>
		<ffi:process name="GetSourceEntitlementProfileForMemberClone"/>
		<ffi:removeProperty name="GetSourceEntitlementProfileForMemberClone"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="notEquals">
		<ffi:object id="GetSourceEntitlementProfileForMemberClone" name="com.ffusion.efs.tasks.entitlements.GetEntitlementProfilesInGroup" scope="session"/>
		<ffi:removeProperty name="EntitlementGroupsSessionName"/>
		<ffi:removeProperty name="Entitlement_EntitlementGroups"/>
		<ffi:setProperty name="GetSourceEntitlementProfileForMemberClone" property="EntGroupId" value="${bizEntitlementGroupId}"/>
		<ffi:setProperty name="GetSourceEntitlementProfileForMemberClone" property="sessionName" value="EntitlementProfilesForClone"/>
		<ffi:process name="GetSourceEntitlementProfileForMemberClone"/>
		
	</ffi:cinclude>
	
<td class="sectionsubhead" align="right"><s:text name="jsp.reports_698"/></td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_PROFILE %>"/>
		<select class="txtbox" style="width:200px" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_PROFILE %>">
		<% int profileNumber = 0; %>
		<ffi:list collection="EntitlementProfilesForClone" items="CountProfileNumber">
			<% profileNumber++; %>
		</ffi:list>
		<% String profileNumberString = new Integer(profileNumber).toString(); %>
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_PROFILES %>"
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_PROFILES %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.reports_698"/>
			</option>
			<ffi:list collection="EntitlementProfilesForClone" items="EntitlementProfile">
				<ffi:cinclude value1="${EntitlementProfile.ChannelId}" value2="X" operator="equals">
				
					<option value="<ffi:getProperty name="EntitlementProfile" property="ProfileId"/>" 
					<ffi:cinclude value1="${EntitlementProfile.ProfileName}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude>>
						<ffi:getProperty name="EntitlementProfile" property="ProfileName"/>
					</option>
				</ffi:cinclude>
			</ffi:list>
		</select>
	</td>
	</tr>
	
<ffi:removeProperty name="EntitlementProfilesForClone"/>