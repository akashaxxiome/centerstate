<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<link rel="stylesheet" type="text/css" href="<s:url value='/web/css/reports/reports%{#session.minVersion}.css'/>"></link>

<script type="text/javascript" src="<s:url value='/web/js/reports/reports%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/reports/report_grid%{#session.minVersion}.js'/>"></script>

<script>
$(document).ready(function(){
	jQuery.i18n.properties({
		name: 'JavaScript',
		path:'<s:url value="/web/js/i18n/" />',
		mode:'both',
		language: '<ffi:getProperty name="UserLocale" property="Locale" />',
		cache:true,
		callback: function(){
		}
	});
});
</script>

<div id="desktop" align="center">
<%
	session.setAttribute( "subpage", request.getParameter("subpage") );
%>
	<div id="appdashboard">
		<s:include value="reports_dashboard.jsp"/> 
	</div>
	
	<div id="operationresult">
		<div id="resultmessage"><s:text name="jsp.reports_624"/></div>
	</div>

	<div id="details" style="display:none;">
		<s:include value="reports_details.jsp"/>
	</div>
	<div id="summary">
		
		<s:include value="reports_summary.jsp"/>
	</div>
	<iframe id="ifrmDownload" name="ifrmDownload" src="" style="visibility:hidden; width: 0px; height: 0px;">
 </iframe>
</div>

<%-- This task will not be referenced in pages, so we can add prefix "FFI" directly. --%>
<ffi:object id="FFINewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>

<%-- This task will not be referenced in pages, so we can add prefix "FFI" directly. --%>
<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria"/>
<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>

<ffi:object id="FFIGenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<%-- Adding the followed statement is because that the first print report function won't work well without it. --%>
<% session.setAttribute("GenerateReportBase", session.getAttribute("FFIGenerateReportBase")); %>

<ffi:object id="FFIAddReport" name="com.ffusion.tasks.reporting.AddReport"/>
<ffi:setProperty name="FFIAddReport" property="ReportName" value="ReportData" />

<ffi:object id="FFIModifyReport" name="com.ffusion.tasks.reporting.ModifyReport"/>

<ffi:object id="FFIDeleteReport" name="com.ffusion.tasks.reporting.DeleteReport"/>

<ffi:object id="FFIGetReportFromName" name="com.ffusion.tasks.reporting.GetReportFromName"/>
<ffi:setProperty name="FFIGetReportFromName" property="IDInSessionName" value="reportID" />
<ffi:setProperty name="FFIGetReportFromName" property="ReportName" value="ReportData"/>

<ffi:object id="FFIGetReportsIDsByCategory" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
<ffi:setProperty name="FFIGetReportsIDsByCategory" property="reportCategory" value="all"/>


   	
    <sj:dialog id="enableCascadingDialogID" cssClass="reportingDialog" title="%{getText('jsp.default_185')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>
    
    
    <s:url id="displayStallConditionUrl" value="%{#session.PagesPath}%{#session.approval_display_stall_condition_jsp}" escapeAmp="false">
	   		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
	</s:url>
	<sj:dialog id="stallConditionDialogID" cssClass="reportingDialog" title="%{getText('jsp.default_385')}" href="%{displayStallConditionUrl}" 
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650">
	</sj:dialog>
	
	<sj:dialog id="viewPermissionDialogID" cssClass="reportingDialog" title="%{getText('jsp.default_463')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

	<sj:dialog id="deletereportdialog" cssClass="reportingDialog" title="%{getText('jsp.reports_561')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>
	<sj:dialog id="savereportdialog" cssClass="reportingDialog" title="%{getText('jsp.reports_657')}" overlayColor="#000" overlayOpacity="0.7"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="550" height="270" cssStyle="overflow:hidden;">
	</sj:dialog>

	<sj:dialog id="setheadfootdialog" cssClass="reportingDialog" title="%{getText('jsp.reports_651')}" overlayColor="#000" overlayOpacity="0.7"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="550">
	</sj:dialog>
	
	<sj:dialog id="reportAlertDialog" cssClass="reportingDialog staticContentDialog" title="%{getText('jsp.reports_487')}" overlayColor="#000" overlayOpacity="0.7"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="450">
			<br/><div class="alertTxt" align="center"></div>
			<br/><br/>
			<div align="center"><sj:a button="true" onClick="ns.common.closeDialog('reportAlertDialog');"><s:text name="jsp.default_303"/></sj:a></div>
	</sj:dialog>
	
	<sj:dialog id="lookupUserDialog" cssClass="reportingDialog" title="%{getText('jsp.reports_610')}" overlayColor="#000" overlayOpacity="0.7"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="340">
	</sj:dialog>