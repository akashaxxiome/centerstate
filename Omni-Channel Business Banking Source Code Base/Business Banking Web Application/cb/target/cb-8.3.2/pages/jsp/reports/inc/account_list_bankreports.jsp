<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_account_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead"><s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead"><s:text name="jsp.load.saved.report"/></span></td>
							<%-- <td width="15%" align="center"><span class="sectionsubhead"><s:text name="jsp.home.column.label.action"/></span></td> --%>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.BANK_REPORTING %>" >
					
					    <ffi:removeProperty name="<%= com.ffusion.beans.bankreport.BankReportConsts.SELECTED_BANK_REPORT %>"/>
					
					
					<ffi:object id="GetBankReportDefinitions" name="com.ffusion.tasks.bankreport.GetReportDefinitions"/>
					<ffi:setProperty name="GetBankReportDefinitions" property="ReportDefinitionsName" value="ReportDefinitions"/>
					<ffi:process name="GetBankReportDefinitions" />
					<ffi:removeProperty name="GetBankReportDefinitions"/>
					
					<% java.util.HashSet displayed = new java.util.HashSet(); %>
					<%
						HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
					%>
					
					<% int formNumber=0; %>
					<ffi:list collection="ReportDefinitions" items="ReportDefn">
						<% formNumber++; %>
					
						<%  String reportType; %>
						<ffi:getProperty name="ReportDefn" property="ReportType" assignTo="reportType"/>
						<%  String display = "FALSE";
						    if( !(displayed.contains( reportType )) ) {
						        displayed.add( reportType );
							display = "TRUE";
						    }
						%>
					
						<ffi:cinclude value1="<%= display %>" value2="TRUE" operator="equals">
					
					    <tr>
						    <ffi:object id="GetDefaultReportCriteriaTask" name="com.ffusion.tasks.reporting.GetDefaultReportCriteria"/>
						    <ffi:setProperty name="GetDefaultReportCriteriaTask" property="ReportType" value="${ReportDefn.ReportType}"/>
						    <ffi:setProperty name="GetDefaultReportCriteriaTask" property="ReportCriteriaName" value="tmpReportCriteria"/>
						    <ffi:process name="GetDefaultReportCriteriaTask"/>
						    <ffi:removeProperty name="GetDefaultReportCriteriaTask"/>
					
						    <% session.setAttribute( "curReportCriteria", session.getAttribute( "tmpReportCriteria" ) ); %>
						    <ffi:setProperty name="curReportCriteria" property="CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_CATEGORY %>" />
						    <ffi:setProperty name="tmpReportCategory" value="${curReportCriteria.CurrentReportOptionValue}" />
					
						    <% String rptCategoryTmp = (String)session.getAttribute( "tmpReportCategory" );
						       String getRptTaskTmp = "GetBankRpt" + rptCategoryTmp;
						       String generateRptTaskTmp = "GenerateBankRpt" + rptCategoryTmp;
						       String reportsNameTmp = "ReportsBank" + rptCategoryTmp;
						    %>
					
						    <ffi:object id="<%= getRptTaskTmp %>" name="com.ffusion.tasks.reporting.GetReportFromName"/>
						    <ffi:setProperty name="<%= getRptTaskTmp %>" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
						    <ffi:setProperty name="<%= getRptTaskTmp %>" property="IDInSessionName" value="reportID" />
						    <ffi:setProperty name="<%= getRptTaskTmp %>" property="ReportIDsName" value="<%= reportsNameTmp %>"/>
						    <ffi:setProperty name="<%= getRptTaskTmp %>" property="ReportName" value="ReportData"/>
					
						    <ffi:object id="<%= generateRptTaskTmp %>" name="com.ffusion.tasks.reporting.GetReportFromName"/>
						    <ffi:setProperty name="<%= generateRptTaskTmp %>" property="SuccessURL" value="${SecurePath}reports/GenerateReportBase"/>
						    <ffi:setProperty name="<%= generateRptTaskTmp %>" property="IDInSessionName" value="reportID" />
						    <ffi:setProperty name="<%= generateRptTaskTmp %>" property="ReportIDsName" value="<%= reportsNameTmp %>"/>
						    <ffi:setProperty name="<%= generateRptTaskTmp %>" property="ReportName" value="ReportData"/>
					
							<%
							String category = rptCategoryTmp;
							session.setAttribute("category", category);
							ArrayList reportNames = (ArrayList)(hashmap.get(category));
							int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
							session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
							session.setAttribute("reportNames", reportNames);
							%>
							<ffi:setProperty name="tmpReportCriteria" property="CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" />
							<ffi:setProperty name="rowTitle" value="${tmpReportCriteria.CurrentReportOptionValue}" />
							<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
						
						    <ffi:removeProperty name="tmpReportCategory"/>
						    <ffi:removeProperty name="tmpReportsName"/>
						    <ffi:removeProperty name="tmpReportName"/>
						    <ffi:removeProperty name="curReportCriteria"/>
						</tr>
						</ffi:cinclude>
					</ffi:list>
					
					</ffi:cinclude>
					</table>
				</div>
			</div>
		</div>		
	</td>
	</tr>
</table>
