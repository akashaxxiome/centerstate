<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setProperty name="subMenuSelected" value="report_payment"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.reports_628')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<%-- Make a reference to this page, so the report criteria page can go back to it on cancel --%>
<ffi:setProperty name="reportListPage" value="${FullPagesPath}/reports/payments_list.jsp"/>

<ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
<ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<ffi:setProperty name="GenerateReportBase" property="Target" value="${FullPagesPath}reports/displayreport.jsp"/>

<ffi:setProperty name="reportTopMenuId" value="reporting_paytran"/>

<div id="paymentSummaryTabs" class="portlet gridPannelSupportCls">
	<div class="portlet-header" >
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<s:if test="%{#parameters.dashboardElementId[0] == 'wireReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_465" />
				</span>
				</s:if>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'achReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_22" />
				</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'taxReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_405" />
				</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'childSupportReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_97" />
				</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'achFileUploadReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.reports_481" />
				</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'billPayReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_74" />
				</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'cashConReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_85" />
				</span>
				</s:elseif>
				<s:else>
					<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_443" />
				</span>
				</s:else>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight marginRight10"></span>
				
				<div class="gridTabDropdownHolder">
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS %>" >
					<span class="gridTabDropdownItem" id='transferReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)" >
						<s:text name="jsp.default_443" />
					</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>" >
					<span class="gridTabDropdownItem" id='wireReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.default_465" />
					</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_BATCH %>" >
					<span class="gridTabDropdownItem" id='achReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.default_22" />
					</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TAX_PAYMENTS %>" >
					<span class="gridTabDropdownItem" id='taxReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.default_405" />
					</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT %>" >
					<span class="gridTabDropdownItem" id='childSupportReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.default_97" />
					</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_FILE_UPLOAD %>" >
	            	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>" >
					<span class="gridTabDropdownItem" id='achFileUploadReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.reports_481" />
					</span>
					</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYMENTS %>" >
					<span class="gridTabDropdownItem" id='billPayReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.default_74" />
					</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_REPORTING %>" >
					<span class="gridTabDropdownItem" id='cashConReport' onclick="ns.report.renderSelectedReport('/cb/pages/jsp/reports/payments_list.jsp',this)">
						<s:text name="jsp.default_85" />
					</span>
					</ffi:cinclude>
				</div>
			</div>
		</div>
	</div>
	<%
	if( session.getAttribute("ReportNamesAndCategories")==null ) {
	%>
		<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
		<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
		<ffi:process name="GetHashmapInSession"/>
		<ffi:removeProperty name="GetHashmapInSession"/>
	<%
	}
	%>
	<div class="portlet-content" id="paymentSummaryTabsContent">
		<s:if test="%{#parameters.dashboardElementId[0] == 'wireReport'}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>" >
			    <ffi:setProperty name="table_bgstyle" value=""/>
			    <s:include value="%{#session.PagesPath}reports/inc/payments_list_wire.jsp" />
			</ffi:cinclude>
		</s:if>
		<% boolean showACH = false;
			String businessGroupId=null; %>
		<ffi:getProperty name="Business" property="EntitlementGroupId" assignTo="businessGroupId"/>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'achReport'}">
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_BATCH %>" >
				<% showACH = true; %>
			</ffi:cinclude>
			<% if (!showACH) {		// make sure that ACH isn't disabled at Service Package level
			%>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
				<%-- Only consider the Manage ACH Participants entitlement if
				     ACH Payments is enabled at the service package level --%>
				<ffi:object id="GetAncestorGroupByType" name="com.ffusion.tasks.admin.GetAncestorGroupByType" scope="session"/>
				<ffi:setProperty name="GetAncestorGroupByType" property="GroupID" value="<%= businessGroupId %>"/>
				<ffi:setProperty name="GetAncestorGroupByType" property="GroupType"
						 value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SERVICES_PACKAGE %>"/>
				<ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="ServicePackageEntGroup"/>
				<ffi:process name="GetAncestorGroupByType"/>
				<ffi:removeProperty name="GetAncestorGroupByType"/>
			
				<ffi:object id="CheckEntACHBatchForSP" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
				<ffi:setProperty name="CheckEntACHBatchForSP" property="GroupId" value="${ServicePackageEntGroup.GroupId}"/>
				<ffi:setProperty name="CheckEntACHBatchForSP" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH %>"/>
				<ffi:process name="CheckEntACHBatchForSP"/>
				<ffi:cinclude value1="${CheckEntACHBatchForSP.Entitled}" value2="TRUE" operator="equals">
					<% showACH = true; %>
				</ffi:cinclude>
				<ffi:removeProperty name="CheckEntACHBatchForSP"/>
				<ffi:removeProperty name="ServicePackageEntGroup"/>
			</ffi:cinclude>
			<% }
			if (showACH) { %>
						    <ffi:setProperty name="table_bgstyle" value=""/>
						    <s:include value="%{#session.PagesPath}reports/inc/payments_list_ach.jsp" />
			<% } %>
		</s:elseif>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'taxReport'}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TAX_PAYMENTS %>" >
			    <ffi:setProperty name="table_bgstyle" value=""/>
			    <s:include value="%{#session.PagesPath}reports/inc/payments_list_tax.jsp" />
			</ffi:cinclude>
		</s:elseif>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'childSupportReport'}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT %>" >
			    <ffi:setProperty name="table_bgstyle" value=""/>
			    <s:include value="%{#session.PagesPath}reports/inc/payments_list_child.jsp" />
			</ffi:cinclude>
		</s:elseif>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'achFileUploadReport'}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_FILE_UPLOAD %>" >
	            <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>" >
				    <ffi:setProperty name="table_bgstyle" value=""/>
				    <s:include value="%{#session.PagesPath}reports/inc/payments_list_ach_file_upload.jsp" />
				</ffi:cinclude>
			</ffi:cinclude>
		</s:elseif>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'billPayReport'}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYMENTS %>" >
			    <ffi:setProperty name="table_bgstyle" value=""/>
			    <s:include value="%{#session.PagesPath}reports/inc/payments_list_billpay.jsp" />
			</ffi:cinclude>
		</s:elseif>
		<s:elseif test="%{#parameters.dashboardElementId[0] == 'cashConReport'}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_REPORTING %>" >
				<% boolean showCASHCON = false; %>
					<%-- Only consider the CASH_CON_REPORTING entitlement if
					     CASH_CON_DEPOSIT_ENTRY or CASH_CON_DISBURSEMENT_REQUEST is enabled at the business level --%>
					<ffi:object id="CheckEntACHBatchForBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
					<ffi:setProperty name="CheckEntACHBatchForBusiness" property="GroupId" value="<%= businessGroupId %>"/>
					<ffi:setProperty name="CheckEntACHBatchForBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>"/>
					<ffi:process name="CheckEntACHBatchForBusiness"/>
					<ffi:cinclude value1="${CheckEntACHBatchForBusiness.Entitled}" value2="TRUE" operator="equals">
						<% showCASHCON = true; %>
					</ffi:cinclude>
				<% if (!showCASHCON) { %>
						<ffi:object id="CheckEntACHBatchForBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
						<ffi:setProperty name="CheckEntACHBatchForBusiness" property="GroupId" value="<%= businessGroupId %>"/>
						<ffi:setProperty name="CheckEntACHBatchForBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>"/>
						<ffi:process name="CheckEntACHBatchForBusiness"/>
						<ffi:cinclude value1="${CheckEntACHBatchForBusiness.Entitled}" value2="TRUE" operator="equals">
							<% showCASHCON = true; %>
						</ffi:cinclude>
				<% } %>
					<ffi:removeProperty name="CheckEntACHBatchForBusiness"/>
				
				<% if (showCASHCON) { %>
				
							    <ffi:setProperty name="table_bgstyle" value=""/>
							    <s:include value="%{#session.PagesPath}reports/inc/payments_list_cashcon.jsp" />
				<% } %>
			</ffi:cinclude>
		</s:elseif>
		<s:else>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS %>" >
			    <ffi:setProperty name="table_bgstyle" value=""/>
			    <s:include value="%{#session.PagesPath}reports/inc/payments_list_transfers.jsp" />
			</ffi:cinclude>
		</s:else>
	</div>
	<div id="paymentSummaryTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('paymentSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>


<script type="text/javascript">
	//Initialize portlet with settings icon
	ns.common.initializePortlet("paymentSummaryTabs");
	
</script>

<ffi:setProperty name="BackURL" value="${FullPagesPath}reports/payments_list.jsp"/>
<ffi:setProperty name="saveBackURL" value=""/>
<ffi:removeProperty name="reportNames"/>
<ffi:removeProperty name="reportNamesSize"/>
<ffi:removeProperty name="category"/>
<ffi:removeProperty name="Sort"/>
<ffi:removeProperty name="reportTopMenuId"/>
<ffi:removeProperty name="reportTypeID"/>