<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Make a reference to this page, so the report criteria page can go back to it on cancel --%>
<ffi:setProperty name="reportListPage" value="${SecurePath}/reports/childreport_list.jsp"/>

<ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
<ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<ffi:setProperty name="GenerateReportBase" property="Target" value="${SecurePath}reports/displayreport.jsp"/>

  <div align="center">
		
		<table width="676" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="6">
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
    					<tr>
    						<td width="98%" background="/cb/web/multilang/grafx/reports/sechdr_blank.gif" class="sectiontitle bgclr1" style="line-height:14px;">&nbsp;&nbsp;<s:text name="jsp.reports_537"/></td>
    					</tr>
    				</table>
				</td>
			</tr>
 			<tr>
				<td class="tbrd_lt" colspan="5"><span class="columndata"><s:text name="jsp.reports_692"/></span></td>
				<td class="tbrd_tr"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="20" height="25" border="0"></td>
			</tr>
			<tr>
				<td class="tbrd_ltb grayback2" colspan="2" align="center"><span class="sectionsubhead"><s:text name="jsp.reports_672"/></span></td>
				<td class="tbrd_ltb grayback2" align="center">&nbsp;</td>
				<td class="tbrd_trb grayback2" colspan="3" align="center"><span class="sectionsubhead"><s:text name="jsp.reports_549"/></span>&nbsp;</td>
			</tr>
			<tr height="1">
				<td class="tbrd_lb"><span class="sectionsubhead">&nbsp;<s:text name="jsp.default_444"/></span></td>
				<td class="tbrd_b"><span class="sectionsubhead"><s:text name="jsp.reports_655"/></span></td>
				<td class="tbrd_lb">&nbsp;</td>
				<td colspan="2" class="tbrd_b"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.reports_659"/></span></td>
				<td class="tbrd_br"><span class="sectionsubhead"><s:text name="jsp.reports_618"/></span></td>
			</tr>
			<s:include value="%{#session.PathExt}reports/inc/payments_list_child.jsp" />
			<tr>
				<td colspan="6"><img src="/cb/web/multilang/grafx/reports/sechdr_btm.gif" height="11" width="750"></td>
			</tr>
		</table>
	</div>

<ffi:setProperty name="BackURL" value="${SecurePath}reports/childreport_list.jsp"/>
<ffi:setProperty name="saveBackURL" value=""/>
