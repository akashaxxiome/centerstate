<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="Transaction_History_Reports" className="moduleHelpClass" />
<ffi:setProperty name="DisplayTime" value="TRUE" />
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<tr>
	<td align="right" class="sectionsubhead nowrap"><s:text name="jsp.default_435"/>&nbsp;<span class="required">*</span></td>
	<td align="left">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_TRACKING_ID %>" />
		<input style="width:186px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_TRACKING_ID %>" size="25" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>">
	</td>
	<td colspan="2">&nbsp;</td>
</tr>