<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="child-support-payments_report" className="moduleHelpClass" />
<%-- Get ACH companies --%>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies"/>
<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
<ffi:process name="GetACHCompanies"/>
<ffi:setProperty name="${GetACHCompanies.CompaniesInSessionName}" property="SortedBy" value="CONAME" />

<%-- account --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_24"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_COMPANY_ID %>" />
		<%
			com.ffusion.beans.ach.ACHCompany company = null;
		%>
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_COMPANY_ID %>" onchange="ns.report.refresh();" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_489"/>
			</option>
			<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
                <ffi:cinclude value1="${company.ACHPaymentEntitled}" value2="TRUE" operator="equals" >
                    <option value="<ffi:getProperty name='company' property='CompanyID'/>"
                    <ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
                        <% company = (com.ffusion.beans.ach.ACHCompany)session.getAttribute( "company" ); %>
                        selected
                    </ffi:cinclude> >
                        <ffi:getProperty name="company" property="CompanyName"/>
                    </option>
                </ffi:cinclude>
			</ffi:list>
		</select>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- Amount, sort 1 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" />
		<input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">

		<span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1"/>&nbsp;</span>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" />
		<input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
	</td>
</tr>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
			<s:text name="jsp.common_Select"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_67"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_119"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_EFFECTIVE_DATE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_EFFECTIVE_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_182"/>
			</option>
            <option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_STATUS %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_STATUS %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
		</select>

		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
			<s:text name="jsp.common_Select"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_CLASS %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_67"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_SUBMISSION_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_119"/>
			</option>
			<option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_EFFECTIVE_DATE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_BATCH_EFFECTIVE_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_182"/>
			</option>
            <option value="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_STATUS %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SORT_CRITERIA_ACH_STATUS %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
		</select>
	</td>
</tr>

<%-- Status, sort 2 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_522"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_BATCH_STATUS %>" />
		<select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_BATCH_STATUS %>" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_500"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_SCHEDULED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_SCHEDULED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_660"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_TRANSFERED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_TRANSFERED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_538"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_FAILED_TO_TRANSFER ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_FAILED_TO_TRANSFER ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_580"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_APPROVAL_PENDING ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_APPROVAL_PENDING ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_508"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_APPROVAL_FAILED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_APPROVAL_FAILED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_509"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_CANCELED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_CANCELED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_562"/>
			</option>
			<option value="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_SKIPPED ) %>" <ffi:cinclude value1="<%= Integer.toString( com.ffusion.beans.ach.ACHStatus.ACH_SKIPPED ) %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >selected</ffi:cinclude> >
				<s:text name="jsp.reports_697"/>
			</option>
		</select>
	</td>
</tr>

<ffi:cinclude value1="${SameDayChildSPEnabledForUser}" value2="true">
<tr>
<td align="right" class="sectionsubhead"><s:text name="jsp.report.SameDayACH"/>&nbsp;</td>
<td class="columndata" align="left">
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH %>" />
<input type="checkbox"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH %>" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH_TRUE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH_TRUE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >checked</ffi:cinclude>/>
</td>
</tr>
</ffi:cinclude>

<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_567"/>&nbsp;</td>
    <td align="left">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" />

        <select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX + com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" size="1">
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_523"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_565"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY_W_ADDENDA %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY_W_ADDENDA %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_566"/>
                </option>
        </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<%-- housekeeping --%>
<ffi:removeProperty name="${GetACHCompanies.CompaniesInSessionName}" />
<ffi:removeProperty name="GetACHCompanies" />
