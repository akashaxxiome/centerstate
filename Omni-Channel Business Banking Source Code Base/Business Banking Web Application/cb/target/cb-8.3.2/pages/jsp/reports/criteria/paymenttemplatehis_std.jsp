<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:help id="payment_template_history" className="moduleHelpClass" />
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" value="${UserLocale.DateFormat}">
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" value="${UserLocale.TimeFormat}">

<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.default_416"/></td>
	<td class="sectionsubhead" colspan="3">			
	 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TEMPLATENAME %>"/>
	 <input style="width:270px;" class="ui-widget-content ui-corner-all" type="text" size="50" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TEMPLATENAME %>" value="" >
      	
	</td>
	</tr>
	<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.default_420"/></td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TEMPLATETYPE %>"/>
		<select style="width:196px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TEMPLATETYPE %>">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_TRANSFER %>"
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_TRANSFER %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.default_441"/>
			</option>
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_RECTRANSFER %>"		
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_RECTRANSFER %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.default_346"/>
			</option>
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_MULTIPLETRANSFER %>"	
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_MULTIPLETRANSFER %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.reports_616"/>
			</option>
			
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.REC_ACH_BATCH %>">
				<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ACHBATCH %>"	
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ACHBATCH %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<s:text name="jsp.default_23"/>
				</option>
				<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_RECACHBATCH %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_RECACHBATCH %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<s:text name="jsp.default_345"/>
				</option>
				<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_MULTIPLEACH %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_MULTIPLEACH %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<s:text name="jsp.reports_615"/>
				</option>
			</ffi:cinclude>
			
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
				<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_WIRE %>"	
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_WIRE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<s:text name="jsp.default_466"/>
				</option>
				<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_RECWIRE %>"	
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_RECWIRE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<s:text name="jsp.reports_642"/>
				</option>
			</ffi:cinclude>
		</select>
	</td>
</tr>
<ffi:setProperty name="DisplayTime" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<ffi:removeProperty name="AdministeredBusinessEmployees"/>
