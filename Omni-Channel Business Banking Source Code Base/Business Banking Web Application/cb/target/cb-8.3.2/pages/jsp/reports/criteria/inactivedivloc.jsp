<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="inactive-divisions-locations_report" className="moduleHelpClass" />
<script type="text/javascript"><!--
ns.report.validateForm = function( formName ) {
        return true;
    }
//--> </script>

<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_600"/>&nbsp;</td>
    <td align="left" class="sectionsubhead">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_INACTIVE_PERIOD %>" />

        <ffi:setProperty name="InactiveFor" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
        <ffi:cinclude value1="" value2="${InactiveFor}" operator="equals">
            <ffi:setProperty name="InactiveFor" value="1" />
        </ffi:cinclude>

        <input class="ui-widget-content ui-corner-all txtbox" type="text" size="5" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_INACTIVE_PERIOD %>" value="<ffi:getProperty name="InactiveFor" />" >&nbsp;
        <s:text name="jsp.reports_554"/>

    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_BUSINESS %>" value="<ffi:getProperty name="Business" property="Id" />" >
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

