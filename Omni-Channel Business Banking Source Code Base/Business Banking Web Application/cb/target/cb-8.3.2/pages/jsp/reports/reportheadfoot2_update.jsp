<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="reportTitleFieldName" value="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE%>"/>
<ffi:setProperty name="extraFooterLineFieldName" value="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_EXTRA_FOOTER_LINE%>"/>
<script>
$(document).ready(function(){
	$.ajax({
		type: "post",
		url: $("#HeaderFooterUpdateForm")[0].action,
		data: $("#HeaderFooterUpdateForm").serialize(),
		success: function(data){
			$("#resultmessage").html(data);
			ns.common.showStatus();
		}
	});
});
</script>
		<%--
			Update session report header/footer session values with values from the
			newly submitted form.
		--%>
	<s:form action="UpdateReportCriteriaAction_headfoot" method="post" name="HeaderFooter2" namespace="/pages/jsp/reports" id="HeaderFooterUpdateForm" theme="simple">
            	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + "FORMAT" ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %>FORMAT" 
			value="<%= com.ffusion.util.HTMLUtil.encode(request.getParameter(com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + "FORMAT" )) %>">
	<% }%>
	
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_COMPANY_NAME ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_COMPANY_NAME %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_COMPANY_NAME %>" value="FALSE"/>
	<% } %>
	
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE %>" value="TRUE">
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" value="<ffi:getProperty name="${reportTitleFieldName}"/>">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE %>" value="FALSE"/>
	<% } %>
	
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_DATE ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_DATE %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_DATE %>" value="FALSE"/>
	<% } %>
	
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TIME ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TIME %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TIME %>" value="FALSE"/>
	<% } %>

        <ffi:cinclude value1="${format}" value2="" operator="notEquals">
			<s:if test="%{#session.format.equals(@com.ffusion.beans.reporting.ExportFormats@FORMAT_HTML) || #session.format.equals(@com.ffusion.beans.reporting.ExportFormats@FORMAT_PDF)}" >
               <% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_BANK_LOGO ) != null ) { %>
                <input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_BANK_LOGO %>" value="TRUE">
            <% } else { %>
                <input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_BANK_LOGO %>" value="FALSE"/>
            <% } %>
        	</s:if>
		</ffi:cinclude>

       <% if( request.getParameter( "__checkbox_ROpt_SHOWHEADER" ) != null ) { %>
       <% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER %>" value="FALSE"/>
	<% } 
		}%>

        <ffi:cinclude value1="${format}" value2="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" operator="equals">
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_PAGE_NUMBER_FORMAT ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_PAGE_NUMBER_FORMAT %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_PAGE_NUMBER_FORMAT %>" value="FALSE"/>
	<% } %>
        </ffi:cinclude>
	
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_EXTRA_FOOTER_LINE ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_EXTRA_FOOTER_LINE %>" value="TRUE">
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_EXTRA_FOOTER_LINE %>" value="<ffi:getProperty name="${extraFooterLineFieldName}"/>">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_EXTRA_FOOTER_LINE %>" value="FALSE"/>
	<% } %>
	
        <ffi:cinclude value1="${format}" value2="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" operator="equals">
	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_FOOTER ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_FOOTER %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_FOOTER %>" value="FALSE"/>
	<% } %>
        </ffi:cinclude>

	<% if( request.getParameter( com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX + com.ffusion.beans.reporting.ReportCriteria.OPT_REPEAT_COLUMN_HEADER ) != null ) { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_REPEAT_COLUMN_HEADER %>" value="TRUE">
	<% } else { %>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_REPEAT_COLUMN_HEADER %>" value="FALSE"/>
	<% } %>
	</s:form>

<ffi:removeProperty name="reportTitleFieldName"/>
<ffi:removeProperty name="extraFooterLineFieldName"/>
