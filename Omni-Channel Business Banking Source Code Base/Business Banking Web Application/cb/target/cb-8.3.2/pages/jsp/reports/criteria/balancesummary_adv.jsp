<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="bal-summary_report" className="moduleHelpClass" />
<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 
 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
 <tr>
 	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
 	
 	<td>
 		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 		<% String rptOnName = com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX 
 							+ com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION; %> 
 		<select style="width:160px" class="txtbox" id="report_crtr_balsumadv_<%= rptOnName %>"
 		 	name="<%= rptOnName %>" size="1" onchange="ns.report.refresh();" >
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_634"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to summary previous day --%>
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_603"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to summary intra day --%>
 		</select>
 	</td>
 	
 	<td>
 	</td>
 	
 	<td>
 	</td>
 </tr>
<% 
int nAccountsSelected=0;
%>
 
<%-- account --%>
 <ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 <%-- This is a summary and detail report so we will filter accounts on the summary and detail view entitlement --%>
 <ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 <%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 <%-- entitled to view under that data classification --%>
 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
 </ffi:cinclude>
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
 </ffi:cinclude>
 
 <ffi:process name="AccountEntitlementFilterTask"/>
 
 <input type='hidden' name='hidden_refreshZBA' value="">
 
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<select class="" style="width:350px;" id="balSummaryAccountDropdownAdv"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" size="5" MULTIPLE onchange="this.form['hidden_refreshZBA'].value='true';ns.report.refresh();">
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals"><% nAccountsSelected=2; %>selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
 		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">
					<% 
					nAccountsSelected++;
					if( nAccountsSelected==1 ) {
					%>
					<ffi:setProperty name="acctShowPrevDayOL" value="${bankAccount.ShowPreviousDayOpeningLedger}" />
					<ffi:cinclude value1="${acctShowPrevDayOL}" value2="" operator="equals">
						<ffi:setProperty name="acctShowPrevDayOL" value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW %>" />
					</ffi:cinclude>
					<%
					} else {
					%>
					<ffi:removeProperty name="acctShowPrevDayOL"/>
					<%
					}
					%>
					selected
				</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
			 	</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
		 		</ffi:cinclude>	
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<%-- date-time selector --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime-previous-biz-day.jsp"/>

<%-- Show previous-day opening ledger( only shows if there is exactly 1 account selected ), if more than 2 selected, remove this criterion from search criteria --%>
<%
	if( nAccountsSelected==1 ){
%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_PREV_DAY_OPENING_LEDGER%>" />
	<ffi:cinclude value1="${hidden_refreshZBA}" value2="true" operator="equals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value=""/>
	</ffi:cinclude>
	<ffi:setProperty name="currShowOL" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
	<ffi:cinclude value1="${currShowOL}" value2="" operator="equals">
		<ffi:setProperty name="currShowOL" value="${acctShowPrevDayOL}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${currShowOL}" value2="" operator="equals">
		<ffi:setProperty name="currShowOL" value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW%>" />
	</ffi:cinclude>
	<tr>			
		<td align="right" class="sectionsubhead"><s:text name="jsp.reports_635"/>&nbsp;</td>
		<td>
		<%
			String openLedgerName = com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX 
								+ com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_PREV_DAY_OPENING_LEDGER;
		%>
			<select style="width:70px" class="txtbox" id="report_balsumadv_<%=openLedgerName.replace(" ", "_")%>" name="<%=openLedgerName%>" >
				<ffi:cinclude value1="${acctShowPrevDayOL}" value2="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_HIDE %>" operator="notEquals">
					<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW %>"
						<ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW %>" value2="${currShowOL}" operator="equals">selected</ffi:cinclude> >
						<s:text name="jsp.default_381"/>
					</option>
				</ffi:cinclude>
				<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_HIDE %>"
					<ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_HIDE %>" value2="${currShowOL}" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.reports_594"/>
				</option>
			</select>
		</td>
		<td></td>
		<td></td>
	</tr>
<%
} else {
%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value=""/>
<%
}
%>
<script>
	$("#balSummaryAccountDropdownAdv").extmultiselect({header: ""}).multiselectfilter();
</script>
<%-- Housekeeping --%>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="hidden_refreshZBA"/>
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
<ffi:removeProperty name="acctShowPrevDayOL"/>
<ffi:removeProperty name="currShowOL"/>
