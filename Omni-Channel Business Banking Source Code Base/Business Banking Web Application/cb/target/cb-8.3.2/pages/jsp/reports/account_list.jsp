<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<%-- The LastIntradayTransactionViewDateForExport is used to ensure that when the last intraday option is selected, the exported report's data matches the data on the screen.  If we get to this page, then we need to clear this variable, as we are seeking to run a New Report --%>
<ffi:removeProperty name="LastIntradayTransactionViewDateForExport"/>

<%-- Remove the variable that indicates whether the report should allow the user to select an option to display the header
     with each new detail entry.  This variable only exists (as TRUE) for reports displaying detail information.  It should
     be cleared here, and it will be set again when the user visits a particular report's criteria page --%>
<ffi:removeProperty name="ShowColumnHeaderOption"/>

<%-- Make a reference to this page, so the report criteria page can go back to it on cancel --%>
<ffi:setProperty name="reportListPage" value="${SecurePath}/reports/account_list.jsp"/>

<ffi:setProperty name="reportTopMenuId" value="reporting_acctmgt"/>

<div id="accountSummaryTabs" class="portlet gridPannelSupportCls">
	<div class="portlet-header" >
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<s:if test="%{#parameters.dashboardElementId[0] == 'bankReport'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.reports.bank.reporting" />
					</span>
				</s:if>
				<s:else>
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.reports.information.reporting" />
					</span>
				</s:else>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight marginRight10"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='infoReport' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/account_list.jsp',this)" >
						<s:text name="jsp.reports.information.reporting" />
					</span>
					<span class="gridTabDropdownItem" id='bankReport' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/account_list.jsp',this)">
						<s:text name="jsp.reports.bank.reporting" />
					</span>
				</div>
			</div>
		</div>
	</div>
	<%
	if( session.getAttribute("ReportNamesAndCategories")==null ) {
	%>
		<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
		<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
		<ffi:process name="GetHashmapInSession"/>
		<ffi:removeProperty name="GetHashmapInSession"/>
	<%
	}
	%>
	<div class="portlet-content" id="accountSummaryTabsContent">
		<s:if test="%{#parameters.dashboardElementId[0] != ''}">
			<!-- do nothing this is a reverse check for fail save scenario -->
			<s:if test="%{#parameters.dashboardElementId[0] == 'bankReport'}">
				<s:include value="%{#session.PagesPath}reports/inc/account_list_bankreports.jsp" />
			</s:if>
			<s:else>
				<s:include value="%{#session.PagesPath}reports/inc/account_list_account.jsp" />
			</s:else>
		</s:if>
		<s:else>
			<s:include value="%{#session.PagesPath}reports/inc/account_list_account.jsp" />
		</s:else>
	</div>
	<div id="accountSummaryTabsDataDiv-menu" class="portletHeaderMenuContainer permissionMenuWrapper" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('accountSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">
	//Initialize portlet with settings icon
	ns.common.initializePortlet("accountSummaryTabs");
	
</script>

<ffi:setProperty name="BackURL" value="${SecurePath}reports/account_list.jsp"/>
<ffi:setProperty name="saveBackURL" value=""/>
<ffi:removeProperty name="reportNames"/>
<ffi:removeProperty name="reportNamesSize"/>
<ffi:removeProperty name="category"/>
<ffi:removeProperty name="Sort"/>
<ffi:removeProperty name="reportTopMenuId"/>
<ffi:removeProperty name="reportTypeID"/>
