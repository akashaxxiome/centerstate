<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />
<ffi:help id="cashcon-activity_report" className="moduleHelpClass" />
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
        <ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:removeProperty name="GetEntitlementGroup"/>
<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="notEquals">	
<tr>
    <td class="sectionsubhead" align="right"><s:text name="jsp.default_174"/></td>
    <td class="columndata">

        <ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
        <ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
        <ffi:process name="GetEntitlementGroup"/>
	<ffi:removeProperty name="GetEntitlementGroup"/>
	
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
	<ffi:process name="GetBPWProperty"/>


	<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
		<ffi:object id="CheckSameDayEnabledForBusiness" name="com.ffusion.tasks.util.CheckSameDayEnabledForBusiness" scope="session" />
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="BusinessID" value="${User.BUSINESS_ID}"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="ModuleName" value="<%=com.ffusion.ffs.bpw.interfaces.ACHConsts.ACH_BATCH_CASH_CON%>"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="FiId" value="${SecureUser.BPWFIID}" />
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="SessionName" value="SameDayCashConEnabledForBusiness"/>
		<ffi:process name="CheckSameDayEnabledForBusiness"/>
	</ffi:cinclude>
	
        <ffi:setProperty name="DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="equals">
			<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="notEquals">			
			<ffi:setProperty name="DivisionId" value="${GetDivisionGroup.Entitlement_EntitlementGroup.ParentId}"/>			
		</ffi:cinclude>

        <ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="GroupId" value="${SecureUser.EntitlementID}"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
        <ffi:process name="GetGroupsAdministeredBy"/>
	<ffi:removeProperty name="GetGroupsAdministeredBy"/>

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>"/>

        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
            <ffi:getProperty name="DivisionName"/>
            <ffi:setProperty name="ReportData.ReportCriteria.CurrentSearchCriterionValue" value="${DivisionId}"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            <select style="width:240px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" onchange="ns.report.refresh();">
                <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
                    <s:text name="jsp.reports_494"/>
                </option>

                <ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
                    <ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
                        <option value="<ffi:getProperty name='ENT_GROUPS_1' property='GroupId'/>"
                        <ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                            <ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
                        </option>
                    </ffi:cinclude>
                </ffi:list>
            </select>
        </ffi:cinclude>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</ffi:cinclude>

<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
	<ffi:setProperty name="DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>		
	<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	
	<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="GroupId" value="${SecureUser.EntitlementID}"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
    <ffi:process name="GetGroupsAdministeredBy"/>
	<ffi:removeProperty name="GetGroupsAdministeredBy"/>
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">	
<tr>
    <td class="sectionsubhead" align="right"><s:text name="jsp.default_174"/></td>
    <td class="columndata">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>"/>
        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
<%
	int parentId = ((com.ffusion.csil.beans.entitlements.EntitlementGroup)session.getAttribute("Entitlement_EntitlementGroup")).getParentId();
	com.ffusion.csil.beans.entitlements.EntitlementGroup division = com.sap.banking.web.util.entitlements.EntitlementsUtil.getEntitlementGroup(parentId);
	String divisionName = division.getGroupName();
%>
			<ffi:setProperty name="DivisionName" value="<%= divisionName %>"/>	        		
            <ffi:getProperty name="DivisionName"/>
            <ffi:setProperty name="ReportData.ReportCriteria.CurrentSearchCriterionValue" value="${DivisionId}"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            <select style="width:240px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" onchange="ns.report.refresh();">
                <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
                    <s:text name="jsp.reports_494"/>
                </option>

                <ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
                    <ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
                        <option value="<ffi:getProperty name='ENT_GROUPS_1' property='GroupId'/>"
                        <ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                            <ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
                        </option>
                    </ffi:cinclude>
                </ffi:list>
            </select>
        </ffi:cinclude>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
	</ffi:cinclude>
</ffi:cinclude>

<tr>
    <td class="sectionsubhead" align="right"><s:text name="jsp.default_266"/></td>
    <td class="columndata" colspan="3">

        <ffi:object id="GetEntitledLocations" name="com.ffusion.efs.tasks.entitlements.GetEntitledLocations" scope="session"/>
        <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
        	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">
        	<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${DivisionId}"/>
        	</ffi:cinclude>
        	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">	
        		<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${DivisionId}"/>
        		</ffi:cinclude>
        		<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        		</ffi:cinclude>
        	</ffi:cinclude>
        </ffi:cinclude>
        <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="notEquals">
        	<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${DivisionId}"/>
        	</ffi:cinclude>
        	<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        	</ffi:cinclude>
        </ffi:cinclude>        

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_LOCATION_NAME %>" />
        <ffi:setProperty name="GetEntitledLocations" property="LocationName" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        <ffi:process name="GetEntitledLocations"/>
	<ffi:removeProperty name="GetEntitledLocations"/>

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_LOCATION %>"/>
        <ffi:cinclude value1="${Locations.Size}" value2="1" operator="notEquals">
            <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_LOCATION %>" onchange="ns.report.refresh();">
            <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_LOCATIONS %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_LOCATIONS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
                <s:text name="jsp.reports_496"/>
            </option>

            <ffi:list collection="Locations" items="ENT_GROUPS_1">
                <option value="<ffi:getProperty name='ENT_GROUPS_1' property='LocationBPWID'/>"
                <ffi:cinclude value1="${ENT_GROUPS_1.LocationBPWID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <ffi:getProperty name="ENT_GROUPS_1" property="LocationName"/>
                </option>
            </ffi:list>
            </select>
        </ffi:cinclude>

        <ffi:cinclude value1="${Locations.Size}" value2="1" operator="equals">
            <ffi:list collection="Locations" items="ENT_GROUPS_1">
                <ffi:getProperty name="ENT_GROUPS_1" property="LocationName"/>
            </ffi:list>
            <ffi:setProperty name="ReportData.ReportCriteria.CurrentSearchCriterionValue" value="${LocationName}"/>
        </ffi:cinclude>

    </td>
   </tr>
   <tr> 
    <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_LOCATION_NAME %>" />
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_267"/></td>
    <td align="left" colspan="3">
        <table border="0" cellspacing="1" cellpadding="0">
            <tr>
                <td align="left"><input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_LOCATION_NAME %>" size="25" border="0" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />"></td>
                <td><span class="columndata">&nbsp;&nbsp;</span></td>
                <td align="left" nowrap><sj:a
								onClick="ns.report.refresh();"
								button="true"
								><s:text name="jsp.default_373"/></sj:a></td>
            </tr>
        </table>
    </td>
</tr>


<tr>
    <td class="sectionsubhead" align="right" nowrap><s:text name="jsp.default_388"/></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_STATUS %>" />
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_STATUS %>" size="1">

		
			<ffi:cinclude value1="${StatusList}" value2="" operator="equals">
				<ffi:object id="StatusList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
				<ffi:setProperty name="StatusList" property="ResourceFilename" value="com.ffusion.beansresources.reporting.cashcon_status" />
				<ffi:setProperty name="StatusList" property="ResourceID" value="CashConStatuses" />
				<ffi:process name="StatusList"/>
				<ffi:getProperty name="StatusList" property="Resource"/>
			</ffi:cinclude>

            <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
            <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.cashcon_status" />
            <ffi:process name="StatusResource" />

            <ffi:list collection="StatusList" items="item">
                <ffi:setProperty name="StatusResource" property="ResourceID" value="${item}" />
                <option <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="${item}">selected</ffi:cinclude> value="<ffi:getProperty name="item"/>"><ffi:getProperty name='StatusResource' property='Resource'/></option>
            </ffi:list>
	    <ffi:removeProperty name="StatusList"/>
        </select>
    </td>
    <td></td>
    <td></td>
</tr>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_BUSINESS %>" value="<ffi:getProperty name="Business" property="Id" />" >


<ffi:removeProperty name="DivisionName" />
<ffi:removeProperty name="Entitlement_EntitlementGroup" />
<ffi:removeProperty name="Entitlement_EntitlementGroups" />
<ffi:removeProperty name="Locations" />
<ffi:removeProperty name="StatusResource"/>
