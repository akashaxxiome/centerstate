<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_payments_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="1">
	<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_22')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>

<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH %>" >
    <ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_COMPLETED_ACH_PMTS %>"/>
    <ffi:process name="CheckReportEntitlement"/>
    
    <ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	    
    <ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
    
	    <ffi:object id="GetCompletedACHPaymentsRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	    <ffi:setProperty name="GetCompletedACHPaymentsRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	    <ffi:setProperty name="GetCompletedACHPaymentsRpt" property="IDInSessionName" value="reportID" />
	    <ffi:setProperty name="GetCompletedACHPaymentsRpt" property="ReportsIDsName" value="ReportsACH"/>
	    <ffi:setProperty name="GetCompletedACHPaymentsRpt" property="ReportName" value="ReportData"/>
	    
	    <ffi:object id="GenerateCompletedACHPaymentsRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	    <ffi:setProperty name="GenerateCompletedACHPaymentsRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	    <ffi:setProperty name="GenerateCompletedACHPaymentsRpt" property="IDInSessionName" value="reportID" />
	    <ffi:setProperty name="GenerateCompletedACHPaymentsRpt" property="ReportsIDsName" value="ReportsACH"/>
	    <ffi:setProperty name="GenerateCompletedACHPaymentsRpt" property="ReportName" value="ReportData"/>
	    
	    <%
	    String category = com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_COMPLETED_ACH_PMTS;
	    session.setAttribute("category", category);
	    ArrayList reportNames = (ArrayList)(hashmap.get(category));
	    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	    session.setAttribute("reportNames", reportNames);
		%>
		<s:set var="tmpI18nStr" value="%{getText('jsp.default_25')}" scope="request" />
		<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
		<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

    </ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_PARTICIPANT_PRENOTE%>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<%
	String category = com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_PARTICIPANT_PRENOTE;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.ACH_Participant_Prenote_Report')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>

<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryACH"/>
