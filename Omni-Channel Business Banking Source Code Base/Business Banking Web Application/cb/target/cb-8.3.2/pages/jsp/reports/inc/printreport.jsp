<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- set format and destination, have to change report option properties directly. --%>
<%
String target = null;
com.ffusion.beans.reporting.ReportCriteria criteria = ((com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" )).getReportCriteria();
java.util.Properties reportOptions = (java.util.Properties)criteria.getReportOptions();
String exportFormat = reportOptions.getProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT );
String destination = reportOptions.getProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION );
String pageWidth = reportOptions.getProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH );


String printerReadyPageWidth = reportOptions.getProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY_PAGEWIDTH );
reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT, com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML );
reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION, com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT );
reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY, com.ffusion.beans.reporting.ReportCriteria.VAL_TRUE );
// if the printer ready page width is specified in the reporting xml, send it down as the desired page width
if ( printerReadyPageWidth != null ) {
	reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH, printerReadyPageWidth );
}
%>

<ffi:setProperty name="isPrinterReady" value="TRUE"/>

<ffi:getProperty name="GenerateReportBase" property="Target" assignTo="target"/>
<%-- Set data display page as the target for the task. --%>
<ffi:setProperty name="GenerateReportBase" property="Target" value="${FullPagesPath}reports/inc/reportdata.jsp" />
<%-- regenerate data --%>
<ffi:process name="GenerateReportBase"/>

<ffi:removeProperty name="isPrinterReady"/>

<%-- restore old settings --%>
<%
reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT, exportFormat);
reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION, destination);
reportOptions.setProperty( com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH, pageWidth );
%>
<ffi:setProperty name="GenerateReportBase" property="Target" value="<%= target %>" />
