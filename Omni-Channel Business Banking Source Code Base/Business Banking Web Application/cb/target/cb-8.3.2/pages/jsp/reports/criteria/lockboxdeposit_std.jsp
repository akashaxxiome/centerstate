<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="deposit-item-search_report" className="moduleHelpClass" />
<script type="text/javascript"><!--
ns.report.validateForm = function( formName )
{
	return true;
}
// --></script>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="LOCKBOX_ACCOUNTS" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="LOCKBOX_ACCOUNTS"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>

<%-- data classification --%>
<% 
boolean ent_previous_day=false;
boolean ent_intra_day=false;
%>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
	<% ent_previous_day=true; %>
</ffi:cinclude>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
	<% ent_intra_day=true; %>
</ffi:cinclude>

<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
<%-- This is a detail report so we will filter accounts on the detail view entitlement --%>
<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="LOCKBOX_ACCOUNTS"/>
<%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
<%-- entitled to view under that data classification --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
</ffi:cinclude>
<ffi:process name="AccountEntitlementFilterTask"/>

<tr>
	<td align="right" class="sectionsubhead">
	<% 
	if( ent_previous_day || ent_intra_day ) { 
	%>
		<s:text name="jsp.default_354"/>&nbsp;
	<%
	}	// end if
	%>
	</td>
	<td>
	<% 
	if( ent_previous_day || ent_intra_day ) { 
	%>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh()">
			<% 
			if( ent_previous_day ) { 
			%>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_634"/>
			</option>
			<%
			}
			if( ent_intra_day ) {
			%>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_603"/>
			</option>
			<%
			}
			%>
		</select>
	<%
	}	// end if
	%>
	</td>
	
	<td>
	</td>
	
	<td>
	</td>
</tr>

<%-- lockbox --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_268"/>&nbsp;&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_NUM %>" />
		<ffi:setProperty name="curr_val" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<%-- set default: all lockboxes --%>
		<ffi:cinclude value1="" value2="${curr_val}" operator="equals" >
			<ffi:setProperty name="curr_val" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_VALUE_ALL_LOCKBOXES %>" />
		</ffi:cinclude>
		<select  id="lockboxDepositSrchDropdown" style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_LOCKBOX_NUM %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${curr_val}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_VALUE_ALL_LOCKBOXES %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_VALUE_ALL_LOCKBOXES %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

			<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
			<ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="LOCKBOX_ACCOUNT_1">
				<option value="<ffi:getProperty name='LOCKBOX_ACCOUNT_1' property='AccountID' />:<ffi:getProperty name='LOCKBOX_ACCOUNT_1' property='BankID'/>: :<ffi:getProperty name='LOCKBOX_ACCOUNT_1' property='RoutingNumber'/>"
					<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${LOCKBOX_ACCOUNT_1.AccountID}:${LOCKBOX_ACCOUNT_1.BankID}: :${LOCKBOX_ACCOUNT_1.RoutingNumber}" />
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="LOCKBOX_ACCOUNT_1" property="RoutingNumber"/> : 
					
					<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${LOCKBOX_ACCOUNT_1.AccountID}" />
					<ffi:process name="GetAccountDisplayText" />
					<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" /> - 
					
					<ffi:getProperty name="LOCKBOX_ACCOUNT_1" property="Nickname"/> - 
					<ffi:getProperty name="LOCKBOX_ACCOUNT_1" property="CurrencyType"/>
				</option>
			</ffi:list>
			<%-- clean up the mess --%>
			<ffi:removeProperty name="curr_val"/>
			<ffi:removeProperty name="LOCKBOX_ACCOUNT_1"/>
			<ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<%-- date range --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- payor, check number --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_629"/>&nbsp;</td>	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_PAYOR%>" />
		<input style="width:152px" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_PAYOR %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12" maxlength="<%= com.ffusion.beans.lockbox.LockboxRptConsts.PAYOR_MAXLENGTH %>">
	</td>
</tr>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_534"/>&nbsp;</td>	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_START_CHECK_NUMBER%>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_START_CHECK_NUMBER %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12" maxlength="<%= com.ffusion.beans.lockbox.LockboxRptConsts.CHECK_NUMBER_MAXLENGTH %>">
		<span class="columndata">&nbsp;<s:text name="jsp.default_423.1"/></span>&nbsp;
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_END_CHECK_NUMBER%>" />
		<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.lockbox.LockboxRptConsts.SEARCH_CRITERIA_END_CHECK_NUMBER %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="12" maxlength="<%= com.ffusion.beans.lockbox.LockboxRptConsts.CHECK_NUMBER_MAXLENGTH %>">
	</td>
</tr>

<ffi:removeProperty name="CriterionListChecker" />
<script>
	$("#lockboxDepositSrchDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>	