<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>

<%-- Account report list, part 2 --%>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CREDIT_RPT %>" />
<ffi:process 	 name="CheckReportEntitlement" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value="" />

<%--
<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
<ffi:process name="GetHashmapInSession"/>
<ffi:removeProperty name="GetHashmapInSession"/>
--%>
<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<%--<ffi:removeProperty name="ReportNamesAndCategories"/> <%-- removing from session, but stays in page context due to prev line --%>

<%-- IF TO show 'Credit' row --%>
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
	<ffi:cinclude
		value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}"
		value2="true" operator="equals">
		<%
			String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CREDIT_RPT;
			session.setAttribute("category", category);
			ArrayList reportNames = (ArrayList)(hashmap.get(category));
			int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
			session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
			session.setAttribute("reportNames", reportNames);
			session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_120", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
			%>
			<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	</ffi:cinclude>		<%-- End entitled to Detail reports for any data dlassification --%>
</ffi:cinclude>		<%-- End entitled to credit detail report --%>


<%-- READY TO show 'Debit' row --%>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_DEBIT_RPT %>" />
<ffi:process 	 name="CheckReportEntitlement" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value="" />


<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
	<ffi:cinclude
		value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}"
		value2="true" operator="equals">
		<%
			String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_DEBIT_RPT;
			session.setAttribute("category", category);
			ArrayList reportNames = (ArrayList)(hashmap.get(category));
			int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
			session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
			session.setAttribute("reportNames", reportNames);
			session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_146", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
			%>
			<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

	</ffi:cinclude>		<%-- End entitled to Detail reports for any data dlassification --%>
</ffi:cinclude>		<%-- End entitled to debit detail report --%>


<%-- READY TO show 'Custom Summary' row --%>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CUSTOM_SUMMARY %>" />
<ffi:process 	 name="CheckReportEntitlement" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportClear"  value="" />

<%-- IF TO show 'Custom Summary' row --%>
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}"
	value2="false" operator="equals">
	<ffi:cinclude
		value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}"
		value2="true" operator="equals">
		<%
			String category = com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CUSTOM_SUMMARY;
			session.setAttribute("category", category);
			ArrayList reportNames = (ArrayList)(hashmap.get(category));
			int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
			session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
			session.setAttribute("reportNames", reportNames);
			session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.reports.Custom_Summary", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
			%>
			<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
		
	</ffi:cinclude>		<%-- End entitled to Summary reports for any data dlassification --%>
</ffi:cinclude>		<%-- End entitled to custom summary report --%>


<%-- Added Non-zero Balance Summary --%>
<%-- entitlement part, need to change --%>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportType"
	value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.RPT_TYPE_NON_ZERO_BALANCE %>" />
<ffi:process 	 name="CheckReportEntitlement" />
<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value="" />

<%-- IF TO show 'Non-zero Balance' row --%>
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
	<ffi:cinclude
		value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}"
		value2="true" operator="equals">
		<%
			String category = com.ffusion.beans.treasurydirect.TDReportConsts.RPT_TYPE_NON_ZERO_BALANCE;
			session.setAttribute("category", category);
			ArrayList reportNames = (ArrayList)(hashmap.get(category));
			int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
			session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
			session.setAttribute("reportNames", reportNames);
			session.setAttribute("rowTitle", com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.reports.Non_zero_Balance", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) );
		%>
			<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

	</ffi:cinclude>		<%-- End entitled to Summary reports for any data dlassification --%>
</ffi:cinclude>		<%-- End entitled to custom summary report --%>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryCredRpt" />
<ffi:removeProperty name="GetReportsByCategoryDebit" />
<ffi:removeProperty name="GetReportsByCategoryCustomSum" />
<ffi:removeProperty name="reportNames" />
<ffi:removeProperty name="reportNamesSize" />
<ffi:removeProperty name="category" />
