<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script language="JavaScript" type="text/javascript"><!--
	/**
	* Date range checking when creating a report.
	*/
	ns.report.validateDateRange = function( startDateStr, endDateStr, msg1, msg2, msg3 ) {
		if( msg1 == null || msg1.length <= 0) {
			msg1 = js_invalid_start_date;
		}
		
		if( msg2 == null || msg2.length <= 0) {
			msg2 = js_invalid_end_date;
		}
		
		if( msg3 == null || msg3.length <= 0) {
			msg3 =js_start_after_end_date;
		}
		
		// If string values are date format, set to empty string
		if( startDateStr == '<ffi:getProperty name="UserLocale" property="DateFormat"/>' ) startDateStr = "";
		if( endDateStr == '<ffi:getProperty name="UserLocale" property="DateFormat"/>' ) endDateStr = "";
		
		// If both strings are empty, return true
		if( startDateStr.length<=0 && endDateStr.length<=0 ) {
			<ffi:cinclude value1="${OverrideDateSearchDefault}" value2="" operator="equals">
				ns.report.showDateError('absDateRangeError',js_date_outof_range_message);
                return false;
            </ffi:cinclude>
            <ffi:cinclude value1="${OverrideDateSearchDefault}" value2="" operator="notEquals">
				ns.report.showDateError('absDateRangeError','<s:text name="jsp.reports_693"/> <ffi:getProperty name="OverrideDateSearchDefault"/> <s:text name="jsp.reports_555"/>');
                return false;
            </ffi:cinclude>
		}
		
		// Validate strings against dateformat, currently '<ffi:getProperty name="UserLocale" property="DateFormat"/>'
		if (! ns.report.validateDateEntry( startDateStr, msg1 ) ) {
			return false;
		}
		
		if( !ns.report.validateDateEntry( endDateStr, msg2 ) ) {
			return false;
		}
		
		// If it reaches here, the date entries are valid dates
		// If either string is empty, it is valid
		if( startDateStr.length<=0 || endDateStr.length<=0 ) return true;
		
		// Parsing the dates.
		d1 = ns.report.parseDate( startDateStr, msg1 );
		d2 = ns.report.parseDate( endDateStr, msg2 );
		
		// If it reaches here, we know that both dates are filled with valid dates
		if( d1.valueOf() > d2.valueOf() ) {
			ns.report.showDateError('absDateRangeError',msg3);
			return false;
		}

<ffi:cinclude value1="${OverrideDateSearchDefault}" value2="" operator="equals">
		if( (d2.valueOf()-d1.valueOf())/(60*60*1000) > (30*24+1) ) {
			ns.report.showDateError('absDateRangeError',js_date_outof_range_message);
			return false;
		}
</ffi:cinclude>
<ffi:cinclude value1="${OverrideDateSearchDefault}" value2="" operator="notEquals">
	<ffi:cinclude value1="${OverrideDateSearchDefault}" value2="-1" operator="notEquals">
		if( (d2.valueOf()-d1.valueOf())/(24*60*60*1000) > <ffi:getProperty name="OverrideDateSearchDefault"/> ) {
			ns.report.showDateError('absDateRangeError','<s:text name="jsp.reports_693"/> <ffi:getProperty name="OverrideDateSearchDefault"/> <s:text name="jsp.reports_555"/>');
			return false;
		}
	</ffi:cinclude>
<ffi:removeProperty name="OverrideDateSearchDefault"/>
</ffi:cinclude>
		
		return true;
	}
	
	ns.report.validateDateEntry = function( dateStr, msg ) {

		if( dateStr==null || dateStr.length<=0 ) {
			return true;
		}

		var seperateDate = new ns.report.ParseMyDateFormat ('<ffi:getProperty name="UserLocale" property="DateFormat" />', dateStr, msg);
		dateStr = seperateDate.sMonth+'/'+seperateDate.sDay+'/'+seperateDate.sYear;
		
		dateFormat = '<ffi:getProperty name="UserLocale" property="DateFormat"/>';
		
		// Month
		var temp = dateStr.indexOf("/");
		if (temp == -1) {
			return false;
		}
		var valStr = dateStr.substring( 0, temp );
		dateStr = dateStr.substring( temp + 1, dateStr.length);
		
		// Get rid of the leading "0" in string. We only need non-zeros
		if( valStr.charAt( 0 )=='0' ) valStr = valStr.substring( 1 );
		
		// See if month part is valid integer string
		month = parseInt( valStr, 10 );
		if( isNaN( month ) ){
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;	// totally unparsable
		}
		
		// To see if the month string is totally parsable to int
		if( month.toString() != valStr ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		// To see if it is a valid month
		if( month<=0 || month > 12 ){
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		// Day
		temp = dateStr.indexOf("/");
		if (temp == -1) {
			return false;
		}
		valStr = dateStr.substring( 0, temp );
		dateStr = dateStr.substring( temp + 1, dateStr.length);
		
		// Get rid of the leading "0" in string. We only need non-zeros
		if( valStr.charAt( 0 )=='0' ) valStr = valStr.substring( 1 );
		
		// See if date part is valid integer string. Same logic as the month part above
		dayOfMonth = parseInt( valStr, 10 );
		if( isNaN( dayOfMonth ) ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		if( dayOfMonth.toString() != valStr ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		if( dayOfMonth<=0 || dayOfMonth>31 ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		// Year
		valStr = dateStr;
		yr = parseInt( valStr, 10  );
		if( isNaN( yr ) ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		if( yr.toString() != valStr ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		if( yr<1900 ) {
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		var date = new Date(yr,month-1,dayOfMonth);
		if(dayOfMonth != date.getDate() || ((month-1) != date.getMonth()) || 
         				yr != date.getFullYear()){
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+dateFormat+'.');
			return false;
		}
		
		return true;
	}
	
	ns.report.parseDate = function( dateStr, msg ) {
	
    	var seperateDate = new ns.report.ParseMyDateFormat ('<ffi:getProperty name="UserLocale" property="DateFormat" />', dateStr, msg);
    	dateStr = seperateDate.sMonth+'/'+seperateDate.sDay+'/'+seperateDate.sYear;
    	    	
		// Month
		var temp = dateStr.indexOf("/");
		if (temp == -1) {
			return false;
		}
		var valStr = dateStr.substring( 0, temp );
		dateStr = dateStr.substring( temp + 1, dateStr.length);
		month =  parseInt( valStr, 10 );
		
		// Day
		temp = dateStr.indexOf("/");
		if (temp == -1) {
			return false;
		}
		valStr = dateStr.substring( 0, temp );
		dateStr = dateStr.substring( temp + 1, dateStr.length);
		dayOfMonth = parseInt( valStr, 10 );
		
		// Year
		yr = parseInt( dateStr, 10 );

		// Note: at Date construction, use month 0-11 for Jan-Dec.	
		dt = new Date( yr, month-1, dayOfMonth );
		return dt;
	}
	
	
    /** Taken from code added by TCG for I18N on BC
     * This function seperates the Month Day and Year parts depending 
     * upon the format of the Date specified.
    */
	ns.report.ParseMyDateFormat = function(validDateFormat, dtValue, msg) {

		//Check what is the seperator
		var delim = '';
		var sMonth = '';
		var sDay = '';
		var sYear = '';
	
		//Check what is the seperator in the DateFormat
		//Note : It is assumed that the date delimiters can be '/', '-' or space only
		if (validDateFormat.indexOf('/') != -1) {
			delim = '/';
		} else if (validDateFormat.indexOf('-') != -1) {
			delim = '-';
		} else if (validDateFormat.indexOf(' ') != -1) {
			delim = ' ';
		} else if (validDateFormat.indexOf('.') != -1) {
			delim = '.';		
		} else {
			ns.report.showDateError('absDateRangeError',js_incorrect_date_format_message);
			return;
		}
	
		//Got the valid delimiter, now checking whether client has provided
		// the required format
		//alert('delim '+delim);
	
		if (dtValue.indexOf(delim) == -1) {
			//Delimiter is not the valid delimiter
			ns.report.showDateError('absDateRangeError',msg + ' <s:text name="jsp.reports_631"/> '+validDateFormat+'.');
			return;
		}
		
		idelim3 = dtValue.indexOf(delim)
		idelim4 = dtValue.indexOf(delim , idelim3 + 1 )
		clienttoken1 = dtValue.substr(0 , idelim3)
		clienttoken2 = dtValue.substr(idelim3 + 1 , idelim4 - (idelim3+1) )
		clienttoken3 = dtValue.substr(idelim4 + 1)
	
		var clientToken = new Array(clienttoken1, clienttoken2, clienttoken3);
	
		idelim3 = validDateFormat.indexOf(delim)
		idelim4 = validDateFormat.indexOf(delim , idelim3 + 1 )
		token1 = validDateFormat.substr(0 , idelim3)
		token2 = validDateFormat.substr(idelim3 + 1 , idelim4 - (idelim3+1) )
		token3 = validDateFormat.substr(idelim4 + 1)
	
		var token = new Array(token1, token2, token3);
	
		//Find our the Month, Day and Year and assign to respective variables.
		for (var i=0; i<token.length; i++) {
	
			switch (token[i].charAt(0)) {
				case 'y' :
				this.sYear = clientToken[i];
				break;
	
				case 'Y' :
				this.sYear = clientToken[i];
				break;
	
				case 'd' :
				this.sDay = clientToken[i];
				break;
	
				case 'D' :
				this.sDay = clientToken[i];
				break;
	
				case 'M' :
				this.sMonth = clientToken[i];
				break;
	
				default :
					ns.report.showDateError('absDateRangeError',js_incorrect_date_format_message);
				return;
				break;
			}
		}
		
		
		if (this.sMonth.length < 2 ){ this.sMonth='0'+this.sMonth; }
		
		if (this.sDay.length < 2 ){ this.sDay='0'+this.sDay; }
	
	}

	ns.report.showDateError = function(elemId, msg){
		$('#'+elemId).html('<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+msg);
		$('#'+elemId).addClass('errorLabel');
	} 	
	
	
// --></script>
