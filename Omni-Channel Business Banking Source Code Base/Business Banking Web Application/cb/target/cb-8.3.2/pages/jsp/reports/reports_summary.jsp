<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<%-- <s:url id="account_list_url" value="/pages/jsp/reports/account_list.jsp"/>	
<s:url id="cash_list_url" value="/pages/jsp/reports/cash_list.jsp"/>	
<s:url id="payments_list_url" value="/pages/jsp/reports/payments_list.jsp"/>	
<s:url id="audit_list_url" value="/pages/jsp/reports/audit_list.jsp"/>	 --%>


<%
	String subpage = (String) session.getAttribute("subpage");
	if( subpage != null && subpage.equals("audit") ) {
%>
    <s:include value="/pages/jsp/reports/audit_list.jsp" />
<% } else if( subpage != null && subpage.equals("cashmgmt")) { %>
    <s:include value="/pages/jsp/reports/cash_list.jsp" />
<% } else if( subpage != null && subpage.equals("pandt")) { %>
    <s:include value="/pages/jsp/reports/payments_list.jsp" />
<% } else{ %>
	<s:include value="/pages/jsp/reports/account_list.jsp" />
<% } %>


<ffi:removeProperty name="subpage"/>