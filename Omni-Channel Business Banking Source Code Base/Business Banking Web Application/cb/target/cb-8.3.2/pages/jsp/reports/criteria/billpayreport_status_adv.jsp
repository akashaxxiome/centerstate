<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript"><!--

ns.report.validateForm = function( formName )
{
	var form = document.forms[formName];
	
    var s = "";
    var p = form.statusType;
    var t = p.options;
    if (!p.disabled)
    {
        for(var i = 0; i < p.length; i++){
            if (t[i].selected)
                s+=t[i].value+",";
        }
        // if no status is selected, select all statuses
        if (s == "") {
            for(var i = 0; i < p.length; i++) s+=t[i].value+",";
        }
    }
    form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_STATUS %>.value= s;

    return true;
}

function statuses(s) {
    frm = document.criteriaFormName;
    if (s == "all") {
        frm.statusType.disabled = true;
        //document.getElementsByName("statusType").item(0).style.backgroundColor="#CCC";
    } else {
        frm.statusType.disabled = false;
        //document.getElementsByName("statusType").item(0).style.backgroundColor="#FFF";
    }
}
// --></script>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:help id="billpay-bystatus_report" className="moduleHelpClass" />
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<%-- If there is no payment accounts in session, get payment accounts --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS ) == null ) { %>
	<ffi:object id="GetPaymentAccounts" name="com.ffusion.tasks.billpay.GetPaymentAccounts" scope="session"/>
		<ffi:setProperty name="GetPaymentAccounts" property="AccountsName" value="BankingAccounts"/>
		<ffi:setProperty name="GetPaymentAccounts" property="Reload" value="true"/>
	<ffi:process name="GetPaymentAccounts"/>
<% } %>

<%-- account --%>
<tr>
    <td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
    <td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_ACCOUNT %>" />
		<select  id="payHistRptAccStatus"  style="width:350px;"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_ACCOUNT %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value=""
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		<ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS %>" property="Filter" value="All"/>
		<ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS %>" items="acct">
			<ffi:setProperty name="tmp_val" value="${acct.ID}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="acct" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="acct" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="acct" property="CurrencyCode"/>
			</option>
		</ffi:list>
		<ffi:removeProperty name="tmp_val"/>
		<ffi:removeProperty name="acct" />
		<ffi:removeProperty name="GetAccountDisplayText"/>


<%--
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_ACCOUNT %>" />
        <select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_ACCOUNT %>" size="1">
            <option value=""
                    <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                    <s:text name="jsp.reports_488" />
            </option>
        <ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS %>" property="Filter" value="All"/>
        <ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS %>" items="acct">
            <ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <option value="<ffi:getProperty name='tmp_val'/>"
                <ffi:cinclude value1="${tmp_val}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                    <ffi:getProperty name="acct" property="RoutingNum"/> :
                </ffi:cinclude>
                <ffi:getProperty name="acct" property="ID"/>
				<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="acct" property="NickName"/>
		 		</ffi:cinclude>	
            </option>
        </ffi:list>
        <ffi:removeProperty name="tmp_val"/>
        <ffi:removeProperty name="acct" />
--%>
    </td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- Amounts, sort 1 --%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" />
        <input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
        <span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1"/>&nbsp;</span>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" />
        <input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
    </td>
</tr>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
        <ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
            <option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_ACCOUNT %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_15"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_AMOUNT %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_43"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_DATE %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_PAYEE %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_PAYEE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_313"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_REF_NO %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_REF_NO %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.reports_643"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_STATUS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
        </select>
		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
        <ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
            <option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_ACCOUNT %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_15"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_AMOUNT %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_43"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_DATE %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_PAYEE %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_PAYEE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_313"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_REF_NO %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_REF_NO %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.reports_643"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_STATUS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.billpay.PaymentReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
        </select>
    </td>
</tr>

<%-- payee, sort 2 --%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_313"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_PAYEE %>" />
        <select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_PAYEE %>" size="1">
            <option value=""
                    <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                    <s:text name="jsp.reports_498"/>
            </option>
        <ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.PAYEES %>" property="Filter" value="All"/>
        <ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.PAYEES %>" items="payee">
            <option value="<ffi:getProperty name='payee' property='ID'/>"
                <ffi:cinclude value1="${payee.ID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <ffi:getProperty name="payee" property="Name"/>
            </option>
        </ffi:list>
        <ffi:removeProperty name="payee" />
    </td>

   
</tr>

<%-- statusType --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_STATUS %>" />
<%
    com.ffusion.beans.reporting.Report rpt = (com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" );
    String stati = rpt.getReportCriteria().getCurrentSearchCriterionValue();
    java.util.ArrayList statusList = new java.util.ArrayList();
    if( stati!=null && stati.length() > 0 ) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer( stati, "," );
        while( tokenizer.hasMoreTokens() ) {
            String s = tokenizer.nextToken();
            if( s!=null && s.length() > 0 ) {
                statusList.add( s );
            }
        }
    }
    System.out.println("stati " + stati);
%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_388"/>&nbsp;</td>
    <td class="sectionsubhead">
        <input type="Radio" name="reportAllStatuses" value="all" onclick="statuses(this.value)" <%= statusList.size() == 0 ? "checked" : "" %>> <s:text name="jsp.reports_500"/>
    </td>
    <td colspan="2"><br></td>
</tr>
<tr>
    <td><br></td>
    <td class="sectionsubhead">
        <input type="Radio" name="reportAllStatuses" value="select" onclick="statuses(this.value)" <%= statusList.size() > 0 ? "checked" : "" %>> <s:text name="jsp.reports_661"/>
    </td>
    <td colspan="2"><br></td>
</tr>
<tr>
    <td><br></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_STATUS %>" />
        <select style="width:160px" class="txtbox" size="9" multiple name="statusType" <%= statusList.size() == 0 ? "disabled" : "" %>>
                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_COMPLETED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_COMPLETED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_538"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_CANCELLED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_CANCELLED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_528"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_NOFUNDSON %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_NOFUNDSON)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_621"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_FAILEDON %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_FAILEDON)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_580"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_SCHEDULED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_SCHEDULED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_660"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_FUNDS_ALLOCATED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_FUNDS_ALLOCATED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_587"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_BATCH_INPROCESS %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_BATCH_INPROCESS)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_520"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_LIMIT_CHECK_FAILED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_LIMIT_CHECK_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_606"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_LIMIT_REVERT_FAILED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_LIMIT_REVERT_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_607"/>
            </option>

                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_APPROVAL_FAILED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_APPROVAL_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_507"/>
            </option>
                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_APPROVAL_PENDING %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_APPROVAL_PENDING)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_508"/>
            </option>
                    <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_APPROVAL_REJECTED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_APPROVAL_REJECTED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_509"/>
            </option>
            <option value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_SKIPPED %>"
            <% if (statusList.contains(com.ffusion.beans.billpay.PaymentReportConsts.RPT_STATUS_SKIPPED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_697"/>
            </option>            
        </select>
    </td>

    <td>
    </td>
    <td>
    </td>
</tr>

<%-- hidden field that holds the status type string used in backend --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_STATUS %>" />
<input type="hidden" name='<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.billpay.PaymentReportConsts.SEARCH_CRITERIA_STATUS %>' value='<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>' />

<%-- final housekeeping --%>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS %>" />
<ffi:removeProperty name="GetPaymentAccounts"/>
<ffi:removeProperty name="currSort"/>
<script>
	$("#payHistRptAccStatus").extmultiselect({header: ""}).multiselectfilter();
</script>