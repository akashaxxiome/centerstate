<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div align="center">

	<s:form action="AddReportAction" method="post" name="AddReport" namespace="/pages/jsp/reports" id="addReport" theme="simple">
              <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<table width="100%" border="0" cellspacing="3" cellpadding="0">
			<%-- <tr>
				<td class="tbrd_b" align="left" colspan="2" nowrap><span class="sectionhead"><s:text name="jsp.reports_671"/></span></td>
			</tr> --%>
			<tr>
				<td>&nbsp;</td>
				<td align="left" height="30">
				        <!-- A List for Global Error Messages -->
				    <ul id="formerrors" class="errorMessage" style="padding:0;"></ul>
				</td>
			</tr>
			<tr>
				<td align="right" class="sectionsubhead" width="33%"><s:text name="jsp.default_283.1"/>&nbsp;</td>
				<td align="left">
			     	<s:textfield 
						id="name" 
						name="name"
						label="name"	
						maxlength="40"  
						cssClass="ui-widget-content ui-corner-all"																	
					/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><span id="nameError"></span></td>
			</tr>
			<tr>
				<td align="right" class="sectionsubhead"><s:text name="jsp.default_170"/>&nbsp;</td>
				<td align="left">
			     	<s:textfield 
						id="description" 
						name="description"						 
						label="description"
						maxlength="255"  
						cssClass="ui-widget-content ui-corner-all"
					/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><span id="descriptionError"></span></td>
			</tr>
			<tr>
				<td colspan="2" height="30">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" nowrap colspan="2">
					<div class="ui-widget-header customDialogFooter">
						<sj:a button="true" onClickTopics="closeDialog"><s:text name="jsp.default_82"/></sj:a>
	
						<sj:a 
								targets="resultmessage"
								formIds="addReport"
								button="true"
								validate="true"
								validateFunction="customValidation"
								onBeforeTopics="removeErrorsOnBegin,beforeSaveReportCriteria" 
								onSuccessTopics="successSaveReportCriteria"
								onErrorTopics="errorSaveReportCriteria"  
								onCompleteTopics="completeSaveReportCriteria" 
							><s:text name="jsp.default_366"/></sj:a>
					</div>
                </td>
			</tr>
		</table>
	</s:form>
				
</div>

		