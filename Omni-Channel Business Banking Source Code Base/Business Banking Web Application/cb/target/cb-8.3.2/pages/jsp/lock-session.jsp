<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%-- WARNING!!! init-session.jsp invalidated the existing session and created a new session before this page was called --%>
<%-- Struts does not like the session being changed out from under it. No Struts stuff that depends on session properties will work.  Use ffi tags here. --%>
<ffi:object id="LockUserSession" name="com.ffusion.tasks.user.LockUserSession" scope="session"/>
	<ffi:setProperty name="LockUserSession" property="SessionInUseURL" value="error-concurrent-login.jsp"/>
	<ffi:setProperty name="LockUserSession" property="AffiliateBankSessionName" value="AffiliateBank"/>
<ffi:process name="LockUserSession" />
