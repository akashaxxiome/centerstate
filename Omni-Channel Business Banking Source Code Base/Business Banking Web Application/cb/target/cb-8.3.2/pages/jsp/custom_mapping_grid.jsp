<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="custom_mapping_gridID">
		<div class="portlet-header" role="section" aria-labelledby="summaryHeader">
			<h1 class="portlet-title" id="summaryHeader"><s:text name="jsp.cash.ppay.custom.mapping" /></h1>
		</div>
		<div class="portlet-content">

		<ffi:help id="custommapping" />

		<ffi:setGridURL grid="GRID_customMapping" name="EditURL" url="/cb/pages/jsp/custommapping.jsp?MappingDefinitionID={0}" parm0="MappingID"/>
	
		<ffi:setGridURL grid="GRID_customMapping" name="DeleteURL" url="/cb/pages/jsp/custommapping_delete_confirm.jsp?MappingDefinitionID={0}" parm0="MappingID"/>

		<ffi:setProperty name="tempURL" value="/pages/mapping/GetMappingDefinitionsAction.action?collectionName=MappingDefinitions&GridURLs=GRID_customMapping" URLEncrypt="true"/>
	    <s:url id="custommappingGridURL" value="%{#session.tempURL}" escapeAmp="false"/>
		<sjg:grid  
			id="custommappingGridID"  
			sortable="true"  
			dataType="json"  
			href="%{custommappingGridURL}"  
			pager="true" 
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			gridModel="gridModel" 
			rownumbers="false"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			onGridCompleteTopics="customMappingGridCompleteEvents"> 
			
			<sjg:gridColumn name="name" index="name" title="%{getText('jsp.default_283')}" sortable="true" width="150" cssClass="datagrid_textColumn"/>
		    <sjg:gridColumn name="description" index="description" title="%{getText('jsp.default_170')}" sortable="true" width="150" cssClass="datagrid_textColumn"/>
		    <sjg:gridColumn name="map.outputFormat" index="outputFormat" title="%{getText('jsp.default_308')}" sortable="true" width="428" cssClass="datagrid_textColumn"/>
		    
		    <sjg:gridColumn name="mappingID" index="mappingID" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatCustomMappingEditandDelete" width="50" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/> 
		    
		    
	<%-- 	    <sjg:gridColumn name="mappingID" index="mappingID" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatCustomMappingEditandDelete"  width="50" cssClass="datagrid_textColumn"/> 
			<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
			<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/> --%>
		</sjg:grid>
	</div>
	
	<div id="customMappingDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('custom_mapping_gridID')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>	
	
</div>

<script>
//Initialize portlet with settings icon
ns.common.initializePortlet("custom_mapping_gridID");
</script>
