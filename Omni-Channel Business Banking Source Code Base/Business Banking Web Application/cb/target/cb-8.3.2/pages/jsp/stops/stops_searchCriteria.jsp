<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<script>
	$(function(){
		$("#dateRangeValue").selectmenu({width: 150});
		
		var value = {value:"All Accounts", label: "All Accounts"};
		$("#AccountsName").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"stopspayment",
			"size":"40",
			"source":"/cb/pages/jsp/stops/StopsPaymentAccountsLookupBoxAction.action",
			"defaultOption":value
		});
		
		var aUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=back&calOneDirectionOnly=true&calForm=View&calTarget=startDate"/>';
		var aConfig = {
			url:aUrl,
			startDateBox:$("#StartDateId"),
			endDateBox:$("#EndDateId")
		}
		$("#stopsDateRangeBox").extdaterangepicker(aConfig);
	});

	// resets the selected index of the given dropdown to 0
	function clearDropDown() {
		$("#dateRangeValue").selectmenu('value',"");
	}

	// clears the 2 text fields
	function clearTextFields( field1, field2, formName ) {
		formName[field1].value = '';
		formName[field2].value = '';
		if ($("#dateRangeValue").val() == null || $("#dateRangeValue").val() == "") {
			$("#StartDateID").val($("#StartDateSessionValue").val());
			$("#EndDateID").val($("#EndDateSessionValue").val());
		}
	}
	
	$.subscribe('accountSelect', function(event, data) {
		var selectedVal = $('#AccountsName :selected').val();
		if (selectedVal != null && selectedVal != ""
				&& selectedVal != "undefined") {
			var accntId = selectedVal.split(",", 1);
			$('#selectedAcctId').val(accntId);
		}
	});
	
</script>

<div id="stopquicksearchcriteria" style="">
            <s:form  action="/pages/jsp/stops/getStopChecks_verify.action" method="post" id="QuickSearchStopsFormID" name="QuickSearchStopsForm" theme="simple">
                <s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
                <input type="hidden" name="selectedAccountOption"  id="selectedAcctId">
				<ul id="fieldErrors"></ul>
				<table border="0">
					<tr>
						<td width="100">
								<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.default_137"/></span>
								<input type="text" id="stopsDateRangeBox" />
								<input type="hidden" id="StartDateId" name="StartDate" value="<ffi:getProperty name='stopsSearchCriteria' property='StartDate' />" />
								<input type="hidden" id="EndDateId" name="EndDate" value="<ffi:getProperty name='stopsSearchCriteria' property='EndDate' />" />
						</td>
						<td>
							<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.billpay_account"/></span>
							<select class="txtbox" name="stopsSearchCriteria.accountID" id="AccountsName" size="1">
							</select>
						</td>
						<td>
							<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.billpay_payee"/></span>
							<input class="ui-widget-content ui-corner-all" type="text" id="PayeeName" name="payeeName" value='<s:property value="stopsSearchCriteria.payeeName"/>' size="25">
						</td>
						<td>
							<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.billpay_amount"/></span>
							<input type="text" id="Amount" name="amount" size="16" maxlength="20" value='<s:property value="stopsSearchCriteria.amount"/>' class="ui-widget-content ui-corner-all">
						</td>
							
						<td align="left" >
							<span class="dashboardLabelMargin" style="display:block;">&nbsp;</span>
							<sj:a targets="quick"
									id="stopsquicksearchbutton"
									title="%{getText('jsp.default_6')}"
									formIds="QuickSearchStopsFormID"
									button="true" 
		                            validate="true"
									cssStyle="position: relative \9; top: 1px \9; margin-left: 5px;"
		                            validateFunction="customValidation"
									onclick="removeValidationErrors();"
									onClickTopics="accountSelect"
									onCompleteTopics="quickSearchStopsComplete">
								<s:text name="jsp.default_6" /></span>
							</sj:a>
						</td>
					</tr>
					<tr>
						<td width="100" style="vertical-align:top;"><span id="dateRangeValueError">&nbsp;</span></td>
						<td style="vertical-align:top;"><span id="stopsSearchCriteria.accountIDError">&nbsp;</span></td>
						<td style="vertical-align:top;"><span id="stopsSearchCriteria.payeeNameError">&nbsp;</span></td>
						<td style="vertical-align:top;"><span id="stopsSearchCriteria.amountError">&nbsp;</span></td>
					</tr>
				</table>
			</s:form>
        </div>