<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="payments_stopstransferInquiry" className="moduleHelpClass"/>

<div class="blockWrapper label130">
	<div  class="blockHead">Stop Payment Details</div>
	<div class="blockContent">
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel">
				
					<s:text name="jsp.default_216"/>:
			</span>
			<span class="columndata lightBackground">
				<s:property value="stopCheck.accountDisplayText" />
			</span>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
							<s:text name="jsp.stops_23"/>:
					</span>
					<span class="columndata lightBackground"><s:text name="jsp.stops_17"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
							<s:text name="jsp.default_43"/>:
					</span>
					<span class="columndata lightBackground">
						<s:property value="stopCheck.AmountValue.CurrencyStringNoSymbol"/>&nbsp;<s:property value="stopCheck.account.currencyCode"/>
					</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
							<s:text name="jsp.default_338"/>:
					</span>
					<span class="columndata lightBackground"><s:property value="stopCheck.reason"/></span>
			</div>
			<div class="inlineBlock">
			</div>
		</div>
	</div>
</div>
<div class="sectionsubhead marginTop10"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></div>
<s:form id="stopinquiryForm" name="form4" theme="simple" method="post" action="/pages/jsp/stops/sendStopCheckInquiryAction.action" style="text-align: left;">
<input type="hidden" name="StopsSendMessage.Process" value="true"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="subject" value='<s:property value="subject"/>' />
<input type="hidden" name="stopCheckID" value='<s:property value="stopCheckID"/>' />
<div align="left">
		<textarea class="txtbox ui-widget-content ui-corner-all" name="StopsSendMessage.Memo" rows="10" cols="75" style="overflow-y: auto; width:450px;" wrap="virtual"></textarea>
</div>
</s:form>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter"> 
      <sj:a id="closeInquireStopsLink" button="true"
            onClickTopics="closeDialog"
            title="%{getText('jsp.default_83')}"><s:text name="jsp.default_82"/></sj:a>

      <sj:a id="cancelStopsSubmitButton"
            formIds="stopinquiryForm"
            targets="resultmessage"
            button="true"
            validate="false"
            title="%{getText('Send_Inquiry')}"
            onSuccessTopics="sendInquiryStopsFormSuccess"><s:text name="jsp.default_378" /></sj:a>
</div>
<script>
	setTimeout(function(){ns.common.renderRichTextEditor('StopsSendMessage.Memo','600',true);}, 600);
</script>