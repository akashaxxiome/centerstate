<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_confirmstop" className="moduleHelpClass"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<div class="leftPaneWrapper">
			<div class="leftPaneInnerWrapper">
				<div class="header">Stop Payment Summary</div>
				<div class="leftPaneInnerBox summaryBlock">
					<div style="width:100%">
						<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
						<span class="inlineSection floatleft labelValue"><s:property value="modStopCheck.accountDisplayText" /></span>
					</div>
				</div>
			</div>
		</div>
		<div class="rightPaneWrapper w71">
			<div class="paneWrapper">
			  	<div class="paneInnerWrapper">
			   		<div class="header">Stop Payment Details</div>
			   		<div class="paneContentWrapper">
			   			<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"  >
											 <s:text name="jsp.default_92"/>:
									</span>
									<span class="columndata lightBackground"  >
										<s:property value="modStopCheck.checkNumbers"/></span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel" >
											<s:text name="jsp.default_43"/>:
									</span>
									<span class="columndata" >
										<s:property value="modStopCheck.AmountValue.CurrencyStringNoSymbol"/>&nbsp;<s:property value="modStopCheck.account.currencyCode"/>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"  >
											<s:text name="jsp.default_338"/>:
									</span>
									<span class="columndata lightBackground"  >
										<s:property value="modStopCheck.reason"/>
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel" >
											<s:text name="jsp.default_313"/>:
									</span>
									<span class="columndata" >			
										<s:property value="modStopCheck.payeeName"/>
									</span>
								</div>
							</div>
						</div>
			   		</div>
			   		<div class="btn-row">
						<sj:a
								button="true"
								onClickTopics="donePaymentStopForm"
						><s:text name="jsp.default_175"/></sj:a>
					</div>
			   	</div>
			 </div>
		</div>
