<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_stopdetail" className="moduleHelpClass"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<div class="blockWrapper">
	<div  class="blockHead">Stop Payment Details</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"   ><s:text name="jsp.default_21"/>:
				</span>
				<span class="columndata lightBackground"   >											
					<s:property value="stopCheck.accountDisplayText" />
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"   ><s:text name="jsp.default_92"/>:
				</span>
				<span class="columndata lightBackground"   >
					<s:property value="stopCheck.checkNumbers"/> 
					</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_45"/>:
				</span>
				<span class="columndata" > 
					<s:property value="stopCheck.AmountValue.CurrencyStringNoSymbol"/><s:property value="stopCheck.account.currencyCode"/>
					</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"   ><s:text name="jsp.stops_11"/></span>
				<span class="columndata lightBackground"   >
					<s:property value="stopCheck.reason"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_316"/></span>
			<span class="columndata" >
				<s:property value="stopCheck.payeeName"/>
			</span>
		</div>
	</div>
	<div class="ffivisible" style="height:50px;">&nbsp;</div>
	<div align="center" class="ui-widget-header customDialogFooter">
		<sj:a button="true" cssClass="buttonlink ui-state-default ui-corner-all" title="%{getText('jsp.default_538')}" onClickTopics="closeViewStopsDialog"><s:text name="jsp.default_175"/></sj:a>
	</div>
</div>
