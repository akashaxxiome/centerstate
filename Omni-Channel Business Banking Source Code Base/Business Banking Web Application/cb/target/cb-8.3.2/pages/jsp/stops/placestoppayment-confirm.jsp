<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_confirmstop" className="moduleHelpClass"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<div class="leftPaneWrapper" role="form" aria-labelledby="stopPaymentSummary">
			<div class="leftPaneInnerWrapper">
				<div class="header"><h2 id="stopPaymentSummary">Stop Payment Summary</h2></div>
				<div class="leftPaneInnerBox summaryBlock">
					<div style="width:100%">
						<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
						<span class="inlineSection floatleft labelValue"><s:property value="stopCheck.accountDisplayText" /></span>
					</div>
				</div>
			</div>
		</div>
		<div class="rightPaneWrapper w71">
			<div class="paneWrapper" role="form" aria-labelledby="stopPaymentDetail">
			  	<div class="paneInnerWrapper">
			   		<div class="header"><h2 id="stopPaymentDetail">Stop Payment Details</h2></div>
			   		<div class="paneContentWrapper">
			   			<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"  >
											 <s:text name="jsp.default_92"/>:
									</span>
									<span class="columndata"  >
										<s:property value="stopCheck.checkNumbers"/>
									</span>
								</div>
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel" >
											<s:text name="jsp.default_43"/>:
									</span>
									<span class="columndata" >
										<s:property value="stopCheck.AmountValue.CurrencyStringNoSymbol"/>&nbsp;<s:property value="stopCheck.account.currencyCode"/>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"  >
											<s:text name="jsp.default_338"/>:
									</span>
									<span class="columndata"  >
										<s:property value="stopCheck.reason"/>
									</span>
								</div>
								
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel" >
											<s:text name="jsp.default_313"/>:
									</span>
									<span class="columndata" >			
										<s:property value="stopCheck.payeeName"/>
									</span>
								</div>
							</div>
						</div>
			   		</div>
			   	</div>
			   	<div class="btn-row">
					<sj:a button="true" onClickTopics="donePaymentStopForm"><s:text name="jsp.default_175"/></sj:a>
				</div>
			 </div>
		</div>