<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<!--
<style type="text/css">
	.ui-jqgrid tr.jqgrow td {
		//white-space: normal !important;
		//height:auto;
		//vertical-align:text-top;
		//vertical-align:middle;
		//padding-top:2px;
		border:0;
		//text-align: center;
	}
</style>
-->

	<ffi:object id="stopstabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="stopstabs" property="TransactionID" value="Stop"/>
	
	<ffi:setProperty name="stopstabs" property="Tab" value="stopPayment"/>
	<ffi:setProperty name="stopstabs" property="Href" value="/cb/pages/jsp/stops/stops_grid.jsp"/>
	<ffi:setProperty name="stopstabs" property="LinkId" value="stopPayment"/>
	<ffi:setProperty name="stopstabs" property="Title" value="jsp.stops_21"/>
		
	<ffi:process name="stopstabs"/>

<div id="stopsGridTabs" class="portlet" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">

		<div class="portlet-header">
		<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.stops_30" /></h1>
			<div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#stopquicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.stops_21" />
					</span>
				</div>
			</div>
			</div>
	   
	    <div class="portlet-content">
	    	<div class="searchDashboardRenderCls">
					<s:include value="/pages/jsp/stops/stops_searchCriteria.jsp" />
				</div>
	    	<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="stopsSummaryGrid" gridId="completedStopsGrid" class="gridSummaryContainerCls" >
					<s:include value="/pages/jsp/stops/stops_grid.jsp" />
				</div>
			</div>
	    </div>
	    <div id="stopsSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('stopsGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	 </div>	
	    <%-- <ul class="portlet-header">
		<ffi:list collection="stopstabs.Tabs" items="tab">
	        <li>
	            <a href='<ffi:getProperty name="tab" property="Href"/>' id='<ffi:getProperty name="tab" property="LinkId"/>'>
	            	<ffi:setProperty name="TabTitle" value="${tab.Title}"/><s:text name="%{#session.TabTitle}"/>
	            </a>
	        </li>
		</ffi:list>
			<a id="tabChangeNote" style="height:0px;  position:relative; top:3px; left:10px; display:inline-block;"></a>
			<a id="tabRevertNote" style="height:0px;  position:relative; top:3px; left:20px; display:inline-block;"></a>
            Stop Payments don't require a calendar.....
	        <s:url id="calendarURL" value="/pages/jsp/calendar/index.jsp?moduleName=TransferCalendar"/>
	        <sj:a id="openCalendar" href="%{calendarURL}" targets="summary" style="float:right;padding-right:5px;">CALENDER VIEW</sj:a>
	       
	        
	    </ul> --%>
		<input type="hidden" id="getTransID" value="<ffi:getProperty name='stopstabs' property='TransactionID'/>" />
		<s:hidden name="transfersSummaryType" value="PendingTransfers" id="transfersSummaryTypeID"/>
	</div>


<script>    

	/* $('#stopsGridTabs').portlet({
		helpCallback: function(){
			
			var helpFile = $('#stopsGridTabs').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	}); */
	//Initialize portlet with settings icon
	ns.common.initializePortlet("stopsGridTabs");
</script>