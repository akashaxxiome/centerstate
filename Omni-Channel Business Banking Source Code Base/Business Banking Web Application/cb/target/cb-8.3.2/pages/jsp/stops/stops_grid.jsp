<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_stopsummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_stops" name="ViewURL" url="/cb/pages/jsp/stops/viewStopCheckAction.action?SelectedStopPaymentID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_stops" name="EditURL" url="/cb/pages/jsp/stops/initModStopCheck.action?Collection=StopChecks&StopID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_stops" name="DeleteURL" url="/cb/pages/jsp/stops/initDeleteStopCheck.action?stopCheckID={0}" parm0="ID" />
	<ffi:setGridURL grid="GRID_stops" name="InquireURL" url="/cb/pages/jsp/stops/initSndStopCheckInquiryAction.action?CollectionName=StopChecks&StopCheckID={0}&Subject=Stop Check Inquiry" parm0="ID" />
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/stops/getStopChecks.action?GridURLs=GRID_stops" URLEncrypt="true"/>
<s:url id="remoteurl" value="%{#session.tempURL}" escapeAmp="false"/>
<sjg:grid
	id="completedStopsGrid"
	caption=""
	sortable="true"  
	dataType="json"
	href="%{remoteurl}"
	gridModel="gridModel"
	shrinkToFit="true"
	altRows="true"
	viewrecords="true"
	rownumbers="false"
	pager="true" 
	rowList="%{#session.StdGridRowList}" 
	rowNum="%{#session.StdGridRowNum}"		
	navigator="true"
	navigatorAdd="false"
	navigatorDelete="false"
	navigatorEdit="false"
	navigatorRefresh="false"
	navigatorSearch="false"
	navigatorView="false"
	scroll="false"
	scrollrows="true"
	onGridCompleteTopics="addGridControlsEvents,completedStopsGridCompleteEvents"
	footerrow="true"
	sortname="createDate"
	sortorder="desc"
	userDataOnFooter="true">
  <sjg:gridColumn name="createDate" width="100" index="CREATE_DATE" title="%{getText('jsp.default_119')}" sortable="true" />
  <sjg:gridColumn name="checkDate" width="100" index="CHECKDATE" title="%{getText('jsp.default_94')}" formatter="ns.stops.formatCheckDateColumn" sortable="true" />
  <sjg:gridColumn name="payeeName" width="100" index="PAYEENAME" title="%{getText('jsp.default_313')}" formatter="ns.stops.formatPayeeNameColumn" sortable="true"/>
  <sjg:gridColumn name="checkNumbers" width="100" index="CHECKNUMBERS" title="%{getText('jsp.default_91')}" sortable="true"/>
  
  <sjg:gridColumn name="account.accountDisplayText" width="200" index="ACCOUNTNICKNAME" title="%{getText('jsp.default_18')}" sortable="true" />
  <sjg:gridColumn name="statusMessage" width="90" index="STATUS" title="%{getText('jsp.default_388')}" sortable="true"/>
  <sjg:gridColumn name="amountValueSymbol" width="90" index="AMOUNT" title="%{getText('jsp.default_43')}" align="right" sortable="true" formatter="ns.stops.formatAmoutColumn"/>
 
  <sjg:gridColumn name="totalLabel" index="totalLabel" title="#" sortable="false" width="40" hidedlg="true" hidden="true"/>
  <sjg:gridColumn name="totalAmount" index="totalAmount" title="#" formatter="ns.stops.formatTotalAmount" sortable="false" width="40" hidedlg="true" hidden="true"/>
 
  <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.stops.formatCompletedStopsActionLinks" width="100" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/> 

	<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	<sjg:gridColumn name="map.InquireURL" index="InquireURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			
</sjg:grid>

<script>
	$("#completedStopsGrid").jqGrid('setColProp','ID',{title:false});
</script>

