<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertAccountBalanceSummaryForm" class="portlet">
	
	<div id="alertAccountBalanceSummaryTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="AccountBalanceSummaryAccessPortletTitle" alertcode="12"><s:text name='alerts.accountBalanceSummaryAlertForm.portlettitle'/></span>	</div>
	
	<div id="alertAccountBalanceSummaryFormElemnts" class="portlet-content">
		<ffi:help id="alerts_account_balance_summary_alert"  className="moduleHelpClass"/>
		<s:form id="accountBalanceSummaryAlertForm" action="addAccountBalanceSummaryAlert" namespace="/pages/jsp/alert" method="post" name="AccountBalanceSummaryAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
				<div id="alertAccountBalanceSummaryHeader">
					<div id="accountBalanceSummaryAccessHeader" alertcode="12" class="instructions"><s:text name='alerts.accountBalanceSummaryAlertForm.header'/></div>
				</div>
			
			<table class="tableData">
				<tr>
					<td class="sectionsubhead"><s:text name='jsp.default_15'/></td>
					<td>
						<div alertcontainer="accountSelectBox" alertcode="2">
						</div>
					</td>
					<td><span id="accountError" class="errorLabel"></span></td>
				</tr>
			</table>
			<div alertcontainer="delOptionsGrid" alertcode="2" align="center" class="marginTop10"></div>				
		  	<div class="btn-row">
		  		<sj:a id="cancelAccountBalanceSummaryAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a></td>
	  			<sj:a id="addEditAccountBalanceSummaryAlert" alertbuttontype="addNew" formIds="accountBalanceSummaryAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateAccountBalanceSummaryAlert" button="true" alertbuttontype="save" formIds="accountBalanceSummaryAlertForm"	targets="targetDiv" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
  		  	</div>
	  
		</s:form>	
	</div>
	<div id="alertAccountBalanceSummaryFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertAccountBalanceSummaryForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
	<div id="accountBalanceSummaryFooter">
		<span messageFor="footer"><s:text name='alerts.accountBalanceSummaryAlertForm.footer'/></span><br>
		<span messageFor="inactiveAccount"><s:text name='alerts.inactiveAccounts'/></span>		
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertAccountBalanceSummaryForm"); 
</script> 
