<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertInvalidAccountForm" class="portlet">
	<ffi:help id="alerts_invalid_account_access"  className="moduleHelpClass"/>
	<div id="alertInvalidAccountTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="invalidAccountAccessPortletTitle" alertcode="14"><s:text name='alerts.invalidAccountAccess.portlettitle'/></span>	</div>
		
	<div id="alertInvalidAccountFormElemnts" class="portlet-content">
		<div id="alertInvalidAccountHeader">
			<div id="invalidAccountAccessHeader" alertcode="14" class="instructions"><s:text name='alerts.invalidAccountAccessAlertForm.header'/></div>
		</div>
		<s:form id="invalidAccountAlertForm" action="AddEditInvalidAccountAccessAlertAction" namespace="/pages/jsp/alert" method="post" name="invalidAccountAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			
			<div alertcontainer="delOptionsGrid" alertcode="14" align="center"></div>				
			
			
			<div class="btn-row">
			  		<sj:a id="cancelInvalidAccountAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a>
		  			<sj:a id="addEditInvalidAccountAlert" alertbuttontype="addNew" formIds="invalidAccountAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
		  			<sj:a id="updateInvalidAccountAlert" button="true" alertbuttontype="save" formIds="invalidAccountAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
	  		</div>
	  
		</s:form>	
	</div>
	<div id="alertInvalidAccountFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertInvalidAccountForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
	<div id="alertFooter"></div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertInvalidAccountForm"); 
</script> 