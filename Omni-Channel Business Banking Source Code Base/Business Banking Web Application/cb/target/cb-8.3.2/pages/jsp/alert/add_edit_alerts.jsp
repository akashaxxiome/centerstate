<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<div id="addEditAlerts" class="portlet">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.alert_91" /> <s:text name="jsp.default_400"/></span>
	</div>
	<div id="currentSubscribedAlerts" class="portlet-content">
		<s:include value="/pages/jsp/alert/inc/subscribed_alerts_grid.jsp" />
		<span class="ui-helper-clearfix">&nbsp;</span>
	</div>
	<div id="subscribedAlertsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('addEditAlerts')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
</div>
<s:include value="/pages/jsp/alert/alerts_main_summary.jsp" />			
<script>
	ns.common.initializePortlet("addEditAlerts");
	
	$(document).ready(function(){
		var config = {
			actions:{
				consumer:{
					"1":{
						add:"<s:url action="addAccountBalanceThresholdAlert"></s:url>",
						edit:"<s:url action="modifyAccountBalanceThresholdAlert"/>"
					},
					"2":{
						add:"<s:url action="addAccountBalanceSummaryAlert"/>",
						edit:"<s:url action="modifyAccountBalanceSummaryAlert"/>"
					},	
					"3":{
						add:"<s:url action="addTransactionAlert"/>",
						edit:"<s:url action="modifyTransactionAlert"/>"
					},
					"4":{
						add:"<s:url action="addInsufficientFundsAlert"/>",
						edit:"<s:url action="modifyInsufficientFundsAlert"/>"
					},
					"5":{
						add:"<s:url action="addStockAlertCons"/>",
						edit:"<s:url action="modifyStockAlert"/>"
					},
					"6":{
						add:"<s:url action="addUnreadBankMessageAlert"/>",
						edit:"<s:url action="modifyUnreadBankMessageAlert"/>"
					},
					"11":{
						add:"<s:url action="addChangePasswordAlert"/>",
						edit:"<s:url action="modifyChangePasswordAlert"/>"
					},
					"12":{
						add:"<s:url action="addBankEnquiryResponseAlert"/>",
						edit:"<s:url action="modifyBankEnquiryResponseAlert"/>"
					},
					"13":{
						add:"<s:url action="addFailedAttemptLockoutAlert"/>",
						edit:"<s:url action="modifyFailedAttemptLockoutAlert"/>"
					},
					"14":{
						add:"<s:url action="addInvalidAccountAccessAlert"/>",
						edit:"<s:url action="modifyInvalidAccountAccessAlert"/>"
					},
					"19":{
						add:"<s:url action="addPanicPayAlert"/>",
						edit:"<s:url action="modifyPanicPayAlert"/>"
					},
					"21":{
						add:"<s:url action="addReversePositivePayAlert"/>",
						edit:"<s:url action="modifyReversePositivePayAlert"/>"
					},
					"300":{
						add:"<s:url action="addImmediateTransactionAlert"/>",
						edit:"<s:url action="modifyImmediateTransactionAlert"/>"
					}
				},
				corporate:{
					"6":{
						add:"<s:url action="addUnreadBankMessageAlert"/>",
						edit:"<s:url action="modifyUnreadBankMessageAlert"/>"
					},
					"17":{
						add:"<s:url action="addAccountBalanceThresholdAlert"/>",
						edit:"<s:url action="modifyAccountBalanceThresholdAlert"/>"
					},
					"9":{
						add:"<s:url action="addStockAlert"/>",
						edit:"<s:url action="modifyStockAlert"/>"
					},	
					"16":{
						add:"<s:url action="addInsufficientFundsAlert"/>",
						edit:"<s:url action="modifyInsufficientFundsAlert"/>"
					},
					"18":{
						add:"<s:url action="addTransactionAlert"/>",
						edit:"<s:url action="modifyTransactionAlert"/>"
					},
					"10":{
						add:"<s:url action="addPaymentApprovalsAlert"/>",
						edit:"<s:url action="modifyPaymentApprovalsAlert"/>"
					},
					"15":{
						add:"<s:url action="addPositivePayAlert"/>",
						edit:"<s:url action="modifyPositivePayAlert"/>"
					},
					"21":{
						add:"<s:url action="addReversePositivePayAlert"/>",
						edit:"<s:url action="modifyReversePositivePayAlert"/>"
					},
					"7":{
						add:"<s:url action="addStockAlert"/>",
						edit:"<s:url action="modifyStockAlert"/>"
					},
					"11":{
						add:"<s:url action="addChangePasswordAlert"/>",
						edit:"<s:url action="modifyChangePasswordAlert"/>"
					},
					"12":{
						add:"<s:url action="addBankEnquiryResponseAlert"/>",
						edit:"<s:url action="modifyBankEnquiryResponseAlert"/>"
					},
					"13":{
						add:"<s:url action="addFailedAttemptLockoutAlert"/>",
						edit:"<s:url action="modifyFailedAttemptLockoutAlert"/>"
					},
					"14":{
						add:"<s:url action="addInvalidAccountAccessAlert"/>",
						edit:"<s:url action="modifyInvalidAccountAccessAlert"/>"
					},
					"19":{
						add:"<s:url action="addPanicPayAlert"/>",
						edit:"<s:url action="modifyPanicPayAlert"/>"
					}
				}
			}	
		}	
		ns.useralerts.setup(config);
	});
	
</script>
