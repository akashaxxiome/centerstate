<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<div id="alertAccountBalanceThresholdForm" class="portlet">
	<ffi:help id="alerts_alertsbalance" className="moduleHelpClass"/>
	<div id="alertAccountBalanceThresholdTitle"class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="AccountBalanceThresholdAccessPortletTitle" alertcode="1"><s:text name='alerts.accountBalanceThresholdAlertForm.portlettitle'/></span>	</div>
	<div id="alertAccountBalanceThresholdFormElemnts" class="portlet-content">
		<s:form id="accountBalanceThresholdAlertForm" action="addAccountBalanceThresholdAlert" namespace="/pages/jsp/alert" method="post" name="accountBalanceThresholdAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value="1"/>
			<div id="alertAccountBalanceThresholdHeader">
				<div id="accountBalanceThresholdAccessHeader" alertcode="1" class="instructions"><s:text name='alerts.accountBalanceThresholdAlertForm.header'/></div>
			</div>
			
			<table class="tableData" width="100%">
				<tr>
					<td><s:text name='jsp.default_15'/></td>
					<td><s:text name='alerts.accountBalanceThresholdAlertForm.minAmount'/></td>
					<td><s:text name='alerts.accountBalanceThresholdAlertForm.maxAmount'/></td>
					<td id="accountBalanceThresholdFrequency" alertcode="1"><s:text name='alert.notification.frequency.label'/></td>
				</tr>
				<tr>
					<td>
						<div alertcontainer="accountSelectBox" alertcode="1">			
							<div id="alertsAccountSelect">
								<select name="account" id="alertsAccountSelectBox"></select>
							</div>						
						</div>
					</td>
					<td><input class="txtbox ui-widget-content ui-corner-all" name="minAmount" id="minAmount" size="15" maxlength="15"></td>
					<td><input class="txtbox ui-widget-content ui-corner-all" name="maxAmount" id="maxAmount" size="15" maxlength="15"></td>
					<td>
						<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><span><s:text name='alerts.frequency.everytime'/></span>
						<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><s:text name='alerts.frequency.firsttime'/></span>
					</td>
				</tr>
				<tr>
					<td><span id="accountError" class="errorLabel"></span></td>
					<td><span id="minAmountError" class="errorLabel"></span></td>
					<td><span id="maxAmountError" class="errorLabel"></span></td>
					<td></td>
				</tr>
				<%-- <tr>
					<td class="sectionsubhead"><s:text name='jsp.default_15'/></td>
					<td>
						<div alertcontainer="accountSelectBox" alertcode="1">			
							<div id="alertsAccountSelect">
								<select name="account" id="alertsAccountSelectBox"></select>
								<br><span id="accountError" class="errorLabel"></span>
							</div>						
						</div>
					</td>
				</tr> --%>
			</table>
			
			<%-- <div id="minMaxAmountBox" alertcode="1">
					<table>
						<tr><td class="sectionsubhead"><s:text name='alerts.accountBalanceThresholdAlertForm.minAmount'/></td>
							<td>
								<input class="ui-widget-content ui-corner-all" name="minAmount" id="minAmount" size="15" maxlength="15">
							</td>
						</tr>
						<tr><td colspan="2"><span id="minAmountError" class="errorLabel"></td></tr>
						<tr><td class="sectionsubhead"><s:text name='alerts.accountBalanceThresholdAlertForm.maxAmount'/></td>
							<td>
								<input class="ui-widget-content ui-corner-all" name="maxAmount" id="maxAmount" size="15" maxlength="15">
							</td>
						</tr>
						<tr><td colspan="2"><span id="maxAmountError" class="errorLabel"></td></tr>
					</table>
			</div>	
			
			
			<br>
				<div alertcontainer="alertFrequencey" align="center">
					<table class="tableData">
						<tr><td align="center">
							<div id="accountBalanceThresholdFrequency" alertcode="1" class="instructions"><s:text name='alerts.accountBalanceThresholdAlertForm.frequency'/></div>
						</td></tr>
						<tr><td align="center">
							<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><span><s:text name='alerts.frequency.everytime'/></span>
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><s:text name='alerts.frequency.firsttime'/></span>
						</td></tr>
						<tr><td align="center">
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><b><s:text name='alerts.frequency.firsttime'/></b></span>
						</td></tr>
					</table>
				</div> --%>
	
			
			<div alertcontainer="delOptionsGrid" alertcode="1" align="center" class="marginTop10"></div>		
			
			
		  	<div class="btn-row">
		  		<sj:a id="cancelAccountBalanceThresholdAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a></td>
	  			<sj:a id="addEditAccountBalanceThresholdAlert" alertbuttontype="addNew" formIds="accountBalanceThresholdAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateAccountBalanceThresholdAlert" button="true" alertbuttontype="save" formIds="accountBalanceThresholdAlertForm"	targets="targetDiv" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		  	</div>
	  
		</s:form>	
	</div>
	<div id="transferDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertAccountBalanceThresholdForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	<div id="accountBalanceThresholdFooter">
		<span messageFor="footer"><s:text name='alerts.accountBalanceThresholdAlertForm.footer'/></span><br>
		<span messageFor="inactiveAccount"><s:text name='alerts.inactiveAccounts'/></span>		
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertAccountBalanceThresholdForm"); 
</script> 