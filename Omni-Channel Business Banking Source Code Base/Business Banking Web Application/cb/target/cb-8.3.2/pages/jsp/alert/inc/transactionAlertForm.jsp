<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<script>
$('#transactionAmountSelectBox').selectmenu({
	width : 150
});
</script>
<div id="alertTransactionForm" class="portlet">
	<ffi:help id="alerts_alertstransaction"  className="moduleHelpClass"/>
	<div id="alertTransactionTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="TransactionAccessPortletTitle" alertcode="3">
		<s:text name='alerts.transactionAlertForm.portlettitle'/></span>	</div>
	<div id="alertTransactionFormElemnts" class="portlet-content">
		<s:form id="transactionAlertForm" action="addTransactionAlert" namespace="/pages/jsp/alert" method="post" name="TransactionAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
				<div id="alertTransactionHeader">
					<div id="TransactionAccessHeader" alertcode="3" class="instructions"><s:text name='alerts.transactionAlertForm.header'/></div>
				</div>
			<table width="100%" class="tableData">
				<tr>
					<td class="sectionsubhead"><s:text name='jsp.default_15'/></td>
					<td class="sectionsubhead"><s:text name='jsp.default_439'/></td>
					<td class="sectionsubhead"><s:text name='jsp.default_43'/></td>
					<td id="transactionFrequency" alertcode="1"><s:text name='alert.notification.frequency.label'/></td>
				</tr>
				<tr>
					<td>
						<div alertcontainer="accountSelectBox" alertcode="3"></div>
					</td>
					<td><select name="transactionType" id="transactionTypeSelectBox"></select></td>
					<td>
						<select name="transactionQuery" id="transactionAmountSelectBox"></select>
						<input class="txtbox ui-widget-content ui-corner-all" name="transactionAmount" id="transactionAmount" size="15" maxlength="15">
					</td>
					<td>
							<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><s:text name='alerts.frequency.everytime'/>
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><s:text name='alerts.frequency.firsttime'/>
					</td>
				</tr>
				<tr>
					<td><span id="accountError" class="errorLabel"></span></td>
					<td></td>
					<td><span id="transactionAmountError" class="errorLabel"></span></td>
					<td></td>
				</tr>
				<%-- <tr>
					<td class="sectionsubhead"><s:text name='jsp.default_439'/></td>
					<td><select name="transactionType" id="transactionTypeSelectBox"></select></td>							
				</tr>
				<tr>
					<td class="sectionsubhead"><s:text name='jsp.default_43'/></td>
					<td>
						<select name="transactionQuery" id="transactionAmountSelectBox"></select>
						<input class="ui-widget-content ui-corner-all" name="transactionAmount" id="transactionAmount" size="15" maxlength="15">
						<span id="transactionAmountError" class="errorLabel"></span>
					</td>
				</tr>	 --%>					
			</table>
			
				<%-- <div alertcontainer="alertFrequencey" >
					<table>
						<tr><td align="center">
							<div id="transactionFrequency" alertcode="1" class="instructions"><s:text name='alerts.transactionAlertForm.frequency'/></div>
						</td></tr>
						<tr><td align="center">
							<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><span><b><s:text name='alerts.frequency.everytime'/></b></span>
						</td></tr>
						<tr><td align="center">
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><b><s:text name='alerts.frequency.firsttime'/></b></span>
						</td></tr>
					</table>
				</div> --%>
			<div alertcontainer="delOptionsGrid" alertcode="3" class="marginTop10" align="center"></div>				
			<div class="btn-row">
					<sj:a id="cancelTransactionAlert" button="true" alertbuttontype="cancel" alertcode="3"><s:text name="jsp.default_82"/></sj:a></td>
		  			<sj:a id="addEditTransactionAlert" alertbuttontype="addNew" formIds="transactionAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
		  			<sj:a id="updateTransactionAlert" button="true" alertbuttontype="save" formIds="transactionAlertForm" 	targets="targetDiv" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		 	</div>
		</s:form>	
	</div>
	<div id="alertTransactionFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertTransactionForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
	<div id="TransactionFooter">
		<span messageFor="footer"><s:text name='alerts.transactionAlertForm.footer'/></span><br>
		<span messageFor="inactiveAccount"><s:text name='alerts.inactiveAccounts'/></span>					
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertTransactionForm"); 
</script> 
