<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertStockPortfolioForm" class="portlet">
	<ffi:help id="alerts_stock_portfolio"  className="moduleHelpClass"/>
	<div id="alertStockPortfolioTitle"class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="stockPortfolioAccessPortletTitle" alertcode="5"><s:text name='alerts.stockPortfolioAlertForm.portlettitle'/></span>	</div>
	<div id="alertStockPortfolioFormElemnts" class="portlet-content">
		<div id="alertStockPortfolioHeader" align="center">
			<div id="stockPortfolioAccessHeader" alertcode="5" class="instructions"><s:text name='alerts.stockPortfolioAlertForm.header'/><s:text name='alerts.stockPortfolioAlertForm.header2'/></div>
		</div>
		<div id="alertStockPortfoliovutton">
			<sj:a id="editPortfolioLink" button="true"><s:text name='alerts.stockPortfolioAlertForm.editPortFolioLink'/></sj:a>
		</div>
		<s:form id="stockPortfolioAlertForm" action="addStockAlertCons" namespace="/pages/jsp/alert" method="post" name="StockPortfolioAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			<div id="stockBox" alertcode="5">
				<table id="portfolioStocksTable" style="display:none;">						
					<tr><td colspan="6"><s:text name="alerts.stockPortfolioAlertForm.portfolioStocks"></s:text></td></tr>
					<tr>
					 
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
						<td><input type="checkbox" name="portfolioStocks" ><span></span></td>
					</tr>						
				</table>
				<table>						
					<tr><td colspan="6"><s:text name="alerts.stockPortfolioAlertForm.otherStocks"></s:text></td></tr>
					<tr>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
					</tr>
					<tr>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
						<td><input class="txtbox ui-widget-content ui-corner-all" name="otherStocks" ></td>
					</tr>						
				</table>
			</div>
			<div alertcontainer="delOptionsGrid" alertcode="5"align="center" class="marginTop10">				
			</div>
			
		  <div class="btn-row">
		  		<sj:a id="cancelStockPortfolioAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a>
	  			<sj:a id="addEditStockPortfolioAlert" alertbuttontype="addNew" formIds="stockPortfolioAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateStockPortfolioAlert" button="true" alertbuttontype="save" formIds="stockPortfolioAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		  </div>
		</s:form>	
	</div>
	<div id="alertStockPortfolioFormmDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertStockPortfolioForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	<div align="">		
		<div id="stockPortfolioFooter" alertcode="5"><s:text name='alerts.stockPortfolioAlertForm.footer'/></div>	
	</div>	
</div>
<script>
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertStockPortfolioForm"); 
</script>