<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="alerts_alert_inbox"/>
<span class="shortcutPathClass" style="display:none;">home_alert,showAlertsInboxLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Alerts</span>
    <form id="frmDelAlertFromInbox" action="/pages/jsp/message/messagesdelete.jsp" method="post" name="frmDelAlertFromInbox">
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<input type="hidden" name="selectedMsgIDs" id="selectedAlertsIDs" />
	<input type="hidden" name="messageIDs" id="allSelectedAlertsIDs" />
	<input type="hidden" id="multicheck" value="false"/>
		
	<ffi:setGridURL grid="GRID_alertInbox" name="LinkURL" url="/cb/pages/jsp/alert/alertViewAction.action?alertId={0}" parm0="ID"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/alert/getInboxAlertsAction.action?GridURLs=GRID_alertInbox" URLEncrypt="true"/>
	
	<s:url id="inboxAlertsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="inboxAlertsGridId"  
		dataType="json"  
		sortable="true"  
		href="%{inboxAlertsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortname="receiveddate"
        multiselect="true"
        multiselectWidth="30"
		sortorder="desc"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onPagingTopics="inboxAlertsGridPagingEvents"
		onGridCompleteTopics="addGridControlsEvents,inboxAlertsGridCompleteEvents"
		> 
		
		<sjg:gridColumn 
			title="" 
			name="Delete"
			width="35" 
			sortable="false"
			hidedlg="true"
			cssClass="datagrid_textColumn"/>
			
		<sjg:gridColumn 
			name="alertIds" 
			title="alertIds" 
			width="35" 
			cssClass="datagrid_textColumn"
			formatter="ns.alert.deleteAllIds"
			hidedlg="true"
			hidden="true"/>
			
		<sjg:gridColumn 
			name="subject" 
			index="subject" 
			title="%{getText('jsp.default_394')}" 
			sortable="true" 
			formatter="ns.alert.formatSubjectLink" 
			width="250"
			hidedlg="true"
			cssClass="datagrid_textColumn"/>
			
	
		<sjg:gridColumn 
			name="date" 
			index="receivedDate" 
			title="%{getText('jsp.default_340')}" 
			sortable="true" 
			hidden="false" 
			width="450"
			cssClass="datagrid_dateColumn"/>

		<sjg:gridColumn 
			name="readDate" 
			index="readdate" 
			title="%{getText('jsp.default_337')}" 
			sortable="false" 
			hidden="true" 
			hidedlg="true"
			width="0"
			cssClass="datagrid_dateColumn"/>
			
			<sjg:gridColumn 
			name="map.LinkURL" 
			index="LinkURL" 
			title="%{getText('jsp.default_262')}" 
			sortable="true" 
			width="160" 
			hidden="true" 
			hidedlg="true" 
			/>

		<sjg:gridColumn 
			name="ID" 
			index="id" 
			title="" 
			hidden="true" 
			hidedlg="true" 
			cssClass="__gridActionColumn"
			search="false"  
			sortable="false" 
			formatter="ns.alert.formatDeleteLinks" 
			width="30"/>
		
		
			
	</sjg:grid>
	
	</form>
	
	<span id="unreadMessage" class="unreadMessage ffiHidden"><s:text name="alert.unreadMessage"/></span>
