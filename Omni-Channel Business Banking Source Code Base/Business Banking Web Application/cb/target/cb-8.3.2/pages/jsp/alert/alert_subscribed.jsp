<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


	<ffi:setGridURL grid="GRID_alertsubScribe" name="DeleteURL" url="/cb/pages/jsp/alert/alert_unsub_confirm.jsp?AlertID={0}" parm0="ID"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/alert/GetSubscribedAlertsAction.action?GridURLs=GRID_alertsubScribe" URLEncrypt="true"/>
	<s:url id="subscribedAlertsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="subscribedAlertsGridId"  
		caption="%{getText('jsp.alert_91')}"  
		sortable="true"  
		dataType="json"  
		href="%{subscribedAlertsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		reloadTopics="reloadSubscribedAlerts"
		onGridCompleteTopics="subscribedAlertsGridCompleteEvents"
		> 
		
		<sjg:gridColumn 
			name="map.alertname" 
			index="alertname" 
			title="%{getText('jsp.alert_12')}" 
			sortable="true" 
			width="285"
			cssClass="datagrid_textColumn"/>
			
		
		<sjg:gridColumn 
			name="ID" 
			index="id" 
			title="%{getText('jsp.default_163')}" 
			hidedlg="true" 
			search="false"  
			sortable="false" 
			hidden="false" 
			formatter="ns.alert.formatDeleteIcons" 
			width="15"
			cssClass="datagrid_actionIcons"/>
			
		<sjg:gridColumn 
			name="map.DeleteURL" 
			index="DeleteURL" 
			title="%{getText('jsp.default_167')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
			
	</sjg:grid>
	

    <script type="text/javascript">

	    $(document).ready(function () {

	    	//jQuery("#list1").jqGrid('navGrid','#pager1',{edit:false,add:false,del:false,search:false,refresh:false}); 
	    	$("#subscribedAlertsGridId").jqGrid('navButtonAdd', "#subscribedAlertsGridId_pager", {
	    	    caption: js_search,
	    	    title: js_search,
	    	    buttonicon: 'ui-icon-search',
	    	    onClickButton: function(){
	    			$("#subscribedAlertsGridId")[0].toggleToolbar()
	    	    }
	    	});

	    	$("#subscribedAlertsGridId").jqGrid('navButtonAdd', "#subscribedAlertsGridId_pager", {
	    	    caption: js_clear_search,
	    	    title: js_clear_search,
	    	    buttonicon: 'ui-icon-refresh',
	    	    onClickButton: function(){
	    			$("#subscribedAlertsGridId")[0].clearToolbar()
	    	    }
	    	});

	    	$("#subscribedAlertsGridId").jqGrid('navButtonAdd', "#subscribedAlertsGridId_pager", {
	    	    caption: js_select_column,
	    	    title: js_select_column,
	    	    buttonicon: 'ui-icon-grip-dotted-vertical',
	    	    onClickButton: function(){
	    			$("#subscribedAlertsGridId").jqGrid('columnChooser');
	    	    }
	    	});

	    	
	    	$("#subscribedAlertsGridId").jqGrid('filterToolbar');
	    	$("#subscribedAlertsGridId")[0].toggleToolbar();

	    	if($("#subscribedAlertsGridId tbody").data("ui-jqgrid") != undefined){
				$("#subscribedAlertsGridId tbody").sortable("destroy");
	    	}

	    });

    </script>
