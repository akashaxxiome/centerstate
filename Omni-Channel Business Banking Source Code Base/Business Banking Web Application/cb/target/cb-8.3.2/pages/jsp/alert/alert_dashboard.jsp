<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<style>
	#sectionsubhead,#sectionsubhead, #StartDateID, #EndDateID, #filterAlertLogLink, #showAlertsInboxLink, #showAlertsLogsLink, #deleteInboxAlertsLink, #subscribeAlertsLink {margin-top: 0px;}

</style>
<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-alert"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_29" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/homeSubmenuDropdown.jsp" />
    	</div>
    	 <!-- dashboard items listing for alerts -->
         <div id="dbAlertsSummary" class="dashboardSummaryHolderDiv">
        	<sj:a
				id="showAlertsInboxLink"
				button="true"
		    	onClickTopics="beforeShowAlertsInbox"
		    	onSuccessTopics="successShowAlertsInbox"
		    	onErrorTopics="errorShowAlertsInbox"
		    	cssClass="summaryLabelCls"
		    	onCompleteTopics="ShowAlertsInboxComplete"><%-- <s:text name="jsp.alert_51" />  --%><s:text name="jsp.billpay_summary"/></sj:a>
        </div>
        
        <div id="dbAddEditAlerts" class="dashboardSummaryHolderDiv">
        
        	<s:url id="subscribeAlertsUrl" value="/pages/jsp/alert/addEditAlertsViewAction.action">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
				
        	 <sj:a
				id="showSubscribeAlertsSummaryLink" href="%{subscribeAlertsUrl}"
				targets="details" button="true"
		    	title="%{getText('jsp.alert_91')}"
		    	onClickTopics="beforeLoadAlertsForm"
		    	onSuccessTopics="successLoadAlertsForm"
		    	onErrorTopics="errorLoadAlertsForm"
		    	onCompleteTopics="loadAlertsFormComplete"
		    	cssClass="summaryLabelCls"><s:text name="jsp.alert_91" /></sj:a>
        	
        	 
		    <sj:a 
		    	id="subscribeAlertsLink" href="%{subscribeAlertsUrl}" 
		    	button="true" targets="details"
		    	title="%{getText('jsp.alert_79')}"
		    	onClickTopics="loadAddEditForm"
		    	onSuccessTopics="successAddEditForm"
		    	onErrorTopics=""
		    	onCompleteTopics="loadAlertsFormComplete">
			  	<s:text name="jsp.alert_79"/>
		    </sj:a>	
        </div>
        
        <div id="dbAlertLogsSummary" class="dashboardSummaryHolderDiv">
        	<sj:a
				id="showAlertsLogsLink"
				button="true"
		    	onClickTopics="beforeShowAlertLogs"
		    	onSuccessTopics="successShowAlertLogs"
		    	onErrorTopics="errorShowAlertLogs"
		    	onCompleteTopics="ShowAlertLogsComplete" cssClass="summaryLabelCls"><s:text name="jsp.alert_2"/></sj:a>
        </div>
        
        <div id="dbContactPointsSummary" class="dashboardSummaryHolderDiv dashboardCustomDiv">
        	<sj:a id="contactPointsLink" button="true" cssClass="summaryLabelCls" onClickTopics="loadContactPoints"><s:text name="alerts.dashboard.button.contactpointS"/></sj:a>
        	<span id="cpControlBox">
				<sj:a id="addNewCPLink"  onClickTopics="loadContactPoints" button="true"><s:text name="alerts.dashboard.button.addcontactpoint" /></sj:a>
			</span>
        </div>
        
        <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MBANKING%>" >
        <div id="dbMobileDevicesSummary" class="dashboardSummaryHolderDiv dashboardCustomDiv">
        	<sj:a id="mobileDevicesLink" button="true" cssClass="summaryLabelCls"><s:text name="alerts.dashboard.button.Mobile.Devices"/></sj:a>
        	<span id="mobileControlBox">
				<sj:a id="addNewDeviceLink" button="true"><s:text name="alerts.dashboard.button.add.Mobile.Devices"/></sj:a>
			</span>
        </div>
        </ffi:cinclude>
        
        <!-- More list option listing -->
		<div class="dashboardSubmenuItemCls">
			<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
			<!-- UL for rendering tabs -->
			<ul class="dashboardSubmenuItemList" id="alertsMenuItems">
					<li class="dashboardSubmenuItem"><a id="showdbAlertsSummary" summarytype="showdbAlertsSummary" ><s:text name="jsp.alert_51"/></a></li>
					<li class="dashboardSubmenuItem"><a id="showdbAddEditAlerts" summarytype="showdbAddEditAlerts" ><s:text name="jsp.alert_91"/></a></li>
					<li class="dashboardSubmenuItem"><a id="showdbAlertLogsSummary" summarytype="showdbAlertLogsSummary" ><s:text name="jsp.alert_2"/></a></li>
					<li class="dashboardSubmenuItem"><a id="showdbContactPointsSummary" summarytype="showdbContactPointsSummary" ><s:text name="alerts.dashboard.button.contactpointS"/></a></li>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MBANKING%>" >
						<li class="dashboardSubmenuItem"><a id="showdbMobileDevicesSummary" summarytype="showdbMobileDevicesSummary" ><s:text name="alerts.dashboard.button.Mobile.Devices"/></a></li>
					</ffi:cinclude>
					
			</ul>
		</div>
</div>
 
	<sj:dialog id="DeleteAlertsConfirmDialogID" title="%{getText('jsp.alert_26')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500" cssStyle="overflow:hidden;">
	</sj:dialog>

	<sj:dialog id="UnsubscribeAlertConfirmDialogID" title="%{getText('jsp.alert_65')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
	</sj:dialog>

	<sj:dialog id="configAlertSubscriptionDialogID" title="%{getText('jsp.alert_23')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	
<script>
	$(document).ready(function() {

		ns.alert.successFilteringAlertLogsURL = '<ffi:urlEncrypt url="/cb/pages/jsp/alert/alertLogViewAction.action"/>';

		ns.alert.beforeShowAlertsInboxURL = '<ffi:urlEncrypt url="/cb/pages/jsp/alert/alert_summary.jsp"/>';		
		
		$("#alertsMenuItems a").click(function(e){			
			 var refreshSummary = $(e.currentTarget).attr("summarytype");
			if(refreshSummary){				
				if(refreshSummary !== "showdbMobileDevicesSummary"){
					if(typeof sap != "undefined" && typeof sap == "object"){
						var router = sap.banking.ui.core.routing.ApplicationRouter.getRouter("deviceManagement");
						router.navOut();
					}
				}
				ns.common.refreshDashboard(refreshSummary,true);
			}
			
		});	
	});
	
	$("#showAlertsInboxLink").find("span").addClass("dashboardSelectedItem");


</script>
