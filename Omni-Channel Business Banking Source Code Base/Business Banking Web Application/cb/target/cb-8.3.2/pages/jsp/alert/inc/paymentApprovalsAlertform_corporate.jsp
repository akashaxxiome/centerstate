<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertPaymentApprovalsForm" class="portlet">
	<ffi:help id="alerts_payment_approval"  className="moduleHelpClass"/>
	<div id="alertPaymentApprovalsTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span id="paymentApprovalsTitle" alertcode="10" class="portlet-title"><s:text name='alerts.paymentApprovalsAlertForm.portlettitle' /></span>	
	</div>
		
		<div id="alertPaymentApprovalsFormElemnts" class="portlet-content">
			<div id="alertPaymentApprovalsHeader" align="center">
				<div id="paymentApprovalsHeader" alertcode="10" class="instructions"><s:text name='alerts.paymentApprovalsAlert.header'/></div>
			</div>
			<s:form id="paymentApprovalsAlertForm" action="AddEditPaymentApprovalsAlertAction" namespace="/pages/jsp/alert" method="post" name="paymentApprovalsAlertForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<input type="hidden" name="operation" value=""/>
				<input type="hidden" name="alertId" value=""/>
				<input type="hidden" name="alertType" value=""/>
				<div alertcontainer="delOptionsGrid" alertcode="10" align="center">					
				</div>
				<div class="btn-row">
					<sj:a id="cancelPaymentApprovalsAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a></td>
		  			<sj:a id="addEditPaymentApprovalsAlert" alertbuttontype="addNew" formIds="paymentApprovalsAlertForm" targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
		  			<sj:a id="updatePaymentApprovalsAlert"  alertbuttontype="save" formIds="paymentApprovalsAlertForm" targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
				</div>  
			</s:form>	
		</div>
		<div id="alertPanicPayFormFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('alertPaymentApprovalsForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertPaymentApprovalsForm"); 
</script> 