<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertPanicPayForm" class="portlet">
	<ffi:help id="alerts_panicpay_alerts"  className="moduleHelpClass"/>
	<div id="alertPanicPayTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="panicPayTitle" alertcode="19"><s:text name='alerts.panicPayAlertForm.portlettitle'/></span>	
	</div>
		
	<div id="alertPanicPayFormElemnts" class="marginTop20 portlet-content" align="center">
		<div id="alertPanicPayHeader" align="center">
			<div id="panicPayHeader" alertcode="19" class="instructions"><s:text name='alerts.panicPayAlertForm.header'/></div>
		</div>
		<s:form id="panicPayAlertForm" action="AddPanicPayAlertAction" namespace="/pages/jsp/alert" method="post" name="PanicPayAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			
			<div alertcontainer="delOptionsGrid" alertcode="19" align="center"></div>
			
			  <table>
			  	<tr>
			  		<td><sj:a id="cancelPanicPayAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a></td>
			  		<td>
			  			<sj:a id="addPanicPayAlert" alertbuttontype="addNew" formIds="panicPayAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
			  			<sj:a id="updatePanicPayAlert" button="true" alertbuttontype="save" formIds="panicPayAlertForm"	 targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
			  		</td>
			  	</tr>
			  </table>
		</s:form>	
	</div>
	<div id="alertPanicPayFormFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertPanicPayForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertPanicPayForm"); 
</script> 