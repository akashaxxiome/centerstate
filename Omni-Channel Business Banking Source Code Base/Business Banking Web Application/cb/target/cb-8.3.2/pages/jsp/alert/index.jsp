<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
	<script type="text/javascript" src="<s:url value='/web/js/alert/alert%{#session.minVersion}.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/alert/alert_grid%{#session.minVersion}.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/alert/useralerts%{#session.minVersion}.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/alert/contactpoints%{#session.minVersion}.js'/>"></script>
        <div id="desktop" align="center">

			<div id="operationresult">
				<div id="resultmessage"><s:text name="jsp.default_498"/></div>
			</div><!-- result -->

            <div id="appdashboard">
		        <s:include value="alert_dashboard.jsp" />
            </div>

			<div id="details" style="display:block;">
				<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
					<ffi:cinclude value1="${summaryToLoad}" value2="SubscribedAlerts" operator="equals">
						<s:action namespace="/pages/jsp/alert" name="addEditAlertsViewAction" executeResult="true"></s:action>
					</ffi:cinclude>
				 </ffi:cinclude>
			</div>

			<div id="summary">
				<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
				
					<ffi:cinclude value1="${summaryToLoad}" value2="" operator="equals">
						<s:include value="/pages/jsp/alert/alert_summary.jsp"/>					
					</ffi:cinclude>
					
					<ffi:cinclude value1="${summaryToLoad}" value2="ContactPoints">
						<s:action namespace="/pages/jsp/alert" name="contactPointsInit" executeResult="true"></s:action>
					</ffi:cinclude>
					
					<%-- Mobile Devices summary handled in alerts.js router call --%>
					
					<ffi:cinclude value1="${summaryToLoad}" value2="AlertLogs">
						<s:action namespace="/pages/jsp/alert" name="alertLogViewAction" executeResult="true"></s:action>
					</ffi:cinclude>
					
				</ffi:cinclude>
			</div>

			<div id="details2" style="display:none;">
				<s:include value="/pages/jsp/alert/alert_details2.jsp"/>
			</div>

			<div id="newContactPointPortlet" style="display:none; float: left; width: 100%;"  class="portlet">
				 <div class="portlet-header">
			    	<span class="portlet-title" title="Received Alerts"><s:text name="contactPoints.addNewForm.form.title" /></span>
			    </div>
				<span class="shortcutPathClass" style="display:none;">home_alert,addNewCPLink</span>
				<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>	
				<div class="portlet-content">
					<s:include value="/pages/jsp/alert/contactpoints_addNew.jsp"/>
				</div>
				<div id="alertsTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
		       		<ul class="portletHeaderMenu" style="background:#fff">
		              <li><a href='#' onclick="ns.common.showDetailsHelp('newContactPointPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
		      		</ul>
			</div>
			</div>

			<div id="deleteCPDialogContainer">
			</div>


			<div id="alerts_main_summary">
				<%--<s:include value="/pages/jsp/alert/add_edit_alerts.jsp"/>  --%>
			</div>


			<div id="deleteAlertDialogContainer">
			</div>

			<div id="logsPlaceHolder">				
			</div>

			<div id="carrierTCDialogContainer">
			</div>
			

    </div>
	
<script>
//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("newContactPointPortlet");

$(document).ready(function(){
	ns.alert.showAlertDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
});

</script>

