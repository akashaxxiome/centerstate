<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

		<div id="subjectInViewAlert" style="display:none"><ffi:getProperty name="message" property="Subject" /></div>

		<div class="alertMsgHolder">
			
			<!-- Subject area -->
			<div class="alertContentArea">
				<span class="sectionsubhead labelCls"><s:text name="jsp.default_516"/> </span><span class="columndata"><ffi:getProperty name="Message" property="Subject" /></span>
			</div>
			
			<!-- Date area -->
			<div class="alertContentArea">
				<span class="sectionsubhead labelCls"><s:text name="jsp.default_143"/> </span><span class="columndata"><ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateFormat}" /><ffi:getProperty name="Message" property="DateValue" /></span>
			</div>
			
			<!-- Time area -->
			<div class="alertContentArea">
				<span class="sectionsubhead labelCls"><s:text name="jsp.alert_62"/> </span><span class="columndata"><ffi:setProperty name="Message" property="DateFormat" value="HH:mm" /><ffi:getProperty name="Message" property="DateValue" /></span>
			</div>
			
			<!-- Message area -->
			<div class="alertContentArea">
				<hr>
				<span class="columndata">
					<ffi:list collection="message.MemoLines" items="line">                                                
					  <ffi:getProperty name="line" encodeLeadingSpaces="true"/><br>
					</ffi:list> 								
				</span>
			</div>
			<div class="marginTop30" style="float: left;">&nbsp;</div>
			<!-- Action buttons -->
			<div class="ui-widget-header customDialogFooter">
				<sj:a button="true"	onClickTopics="closeDialog" onclick="ns.home.loadReadAlerts(5),ns.home.loadUnReadAlerts(5)"><s:text name="jsp.default_175"/></sj:a>

			    <s:url id="deleteAlertUrl" value="/pages/jsp/alert/deleteInboxAlertsAction_confirm.action" escapeAmp="false">
				    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
				    <s:param name="selectedMsgIDs" value="%{#attr.message.ID}"></s:param>		
				    <s:param name="quickDelete" value="%{'true'}"></s:param>		
				</s:url>
				
	            <sj:a id="quickDeletAlertConfirmSubmit"
					href="%{deleteAlertUrl}"
					targets="quickViewAlertDlgBand2" 
					button="true" 
					onBeforeTopics="beforeQuickDeletingAlertConfirm"
					onCompleteTopics="quickdeletingAlertConfirmComplete"
					onErrorTopics="errorQuickDeletingAlertConfirm"
					onSuccessTopics="successQuickDeletingAlertConfirm"
					><s:text name="jsp.default_162"/>
				</sj:a>
			</div>
		</div>
		<%-- <div align="left">

				<table width="100%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground">
					<tr>
						<td colspan="3" nowrap width="565"><span class="sectionsubhead"><s:text name="jsp.default_516"/> </span><span class="columndata"><ffi:getProperty name="Message" property="Subject" /></span></td>
					</tr>
					<tr>
						<td colspan="3" nowrap width="565"><span class="sectionsubhead"><s:text name="jsp.default_143"/> </span><span class="columndata"><ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateFormat}" /><ffi:getProperty name="Message" property="DateValue" /></span></td>
					</tr>
					<tr>
						<td colspan="3" nowrap width="565"><span class="sectionsubhead"><s:text name="jsp.alert_62"/> </span><span class="columndata"><ffi:setProperty name="Message" property="DateFormat" value="hh:mm a" /><ffi:getProperty name="Message" property="DateValue" /></span></td>
					</tr>
					<tr>
						<td colspan="3" nowrap></td>
					</tr>
					<tr>
						<td class="tbrd_t" colspan="3" nowrap width="565"><span class="columndata">
							<ffi:list collection="message.MemoLines" items="line">                                                
							  <ffi:getProperty name="line" encodeLeadingSpaces="true"/><br>
							</ffi:list> 								
						</span></td>
					</tr>
					<tr>
						<td colspan="3" nowrap></td>
					</tr>
					<tr>
						<td colspan="3" align="center" nowrap width="707">							
							
							<!-- <input class="submitbutton" type="button" value="DELETE" onClick="location.replace('<ffi:getProperty name="SecureServletPath"/>DeleteMessage')">&nbsp;
							<input class="submitbutton" type="button" value="DONE" onClick="location.replace('<ffi:getProperty name="SecurePath"/>alerts.jsp')"> -->
							
							<sj:a   button="true" 
									onClickTopics="closeDialog"
					        ><s:text name="jsp.default_175"/></sj:a>

						    <s:url id="deleteAlertUrl" value="/pages/jsp/alert/deleteInboxAlertsAction_confirm.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
							    <s:param name="selectedMsgIDs" value="%{#attr.message.ID}"></s:param>		
							    <s:param name="quickDelete" value="%{'true'}"></s:param>		
							</s:url>
				            <sj:a
								id="quickDeletAlertConfirmSubmit"
								href="%{deleteAlertUrl}"
	                            targets="quickViewAlertDlgBand2" 
	                            button="true" 
	                            onBeforeTopics="beforeQuickDeletingAlertConfirm"
	                            onCompleteTopics="quickdeletingAlertConfirmComplete"
								onErrorTopics="errorQuickDeletingAlertConfirm"
	                            onSuccessTopics="successQuickDeletingAlertConfirm"
	                        ><s:text name="jsp.default_162"/></sj:a>

						</td>
					</tr>
				</table>
			<br>
		</div> --%>
