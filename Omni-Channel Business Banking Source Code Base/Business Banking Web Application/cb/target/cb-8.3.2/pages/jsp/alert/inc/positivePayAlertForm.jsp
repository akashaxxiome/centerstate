<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertPositivePayForm" class="portlet">
	<ffi:help id="alerts_positive_pay"  className="moduleHelpClass"/>
	<div id="alertPositivePayTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="positivePayTitle" alertcode="15"><s:text name='alerts.positivePayAlertForm.portlettitle'/></span>	
	</div>
	<div id="alertPositivePayFormElemnts" class="portlet-content">
		<div id="alertPositivePayHeader">
			<div id="positivePayHeader" alertcode="15" class="instructions"><s:text name='alerts.positivePayAlertFrom.header'/></div>
		</div>
		<s:form id="positivePayAlertForm" action="AddEditPositivePayAlertAction" namespace="/pages/jsp/alert" method="post" name="positivePayAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			<div alertcontainer="delOptionsGrid" alertcode="15" align="center"></div>
			<div class="btn-row">
			  		<sj:a id="cancelPositivePayAlertAlert" button="true" alertbuttontype="cancel" alertcode="6"><s:text name="jsp.default_82"/></sj:a>
		  			<sj:a id="addEditPositivePayAlert" alertbuttontype="addNew" formIds="positivePayAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
		  			<sj:a id="updatepositivePayAlert" button="true" alertbuttontype="save" formIds="positivePayAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		  	</div>
		</s:form>	
	</div>
	<div id="alertPositivePayFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertPositivePayForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertPositivePayForm"); 
</script> 