<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertUnreadBankMessageForm" class="portlet">
	<ffi:help id="alerts_unread_bank_msgs"  className="moduleHelpClass"/>
<%-- 	<div id="alertUnreadBankMessageTitle">
		<span id="unreadBankMessageTitle" alertcode="6" style="display:none;"><s:text name=''/></span>	
	</div>
 --%>		
 		<div id="alertUnreadBankMessageHeader" class="portlet-header ui-widget-header ui-corner-all">
			<span class="portlet-title" id="unreadBankMessageHeader" alertcode="6" class="instructions"><s:text name='alerts.unreadBankMessageAlertForm.portlet_title'/></span>
		</div>
		<div id="alertUnreadBankMessageFormElemnts" class="portlet-content">
			<p align="left"><s:text name='alerts.unreadBankMessageAlertForm.header'/></p>
			<s:form id="unreadBankMessageAlertForm" action="AddEditUnreadBankMessageAlertAction" namespace="/pages/jsp/alert" method="post" name="UnreadBankMessageAlertForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<input type="hidden" name="operation" value=""/>
				<input type="hidden" name="alertId" value=""/>
				<input type="hidden" name="alertType" value=""/>
				<div alertcontainer="delOptionsGrid" alertcode="6" align="center" class="marginTop10">
					<s:include value="/pages/jsp/alert/inc/delivery_options_grid.jsp"/>
				</div>
				  <div class="btn-row">
				  	<sj:a id="cancelAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a></td>
		  			<sj:a id="addEditUnreadBankMessageAlert" alertbuttontype="addNew" formIds="unreadBankMessageAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
		  			<sj:a id="updateUnreadBankMessageAlert" button="true" alertbuttontype="save" formIds="unreadBankMessageAlertForm"	 targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
				  </div>
			</s:form>	
		</div>
		<div id="alertUnreadBankMessageFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('alertUnreadBankMessageForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertUnreadBankMessageForm"); 
</script>