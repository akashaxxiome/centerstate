<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertBankInquiryResponseForm" class="portlet">
	<ffi:help id="alerts_bank_inquiry_response"  className="moduleHelpClass"/>
	<div id="alertBankInquiryResponseTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="bankInquiryResponseAccessPortletTitle" alertcode="12"><s:text name='alerts.bankEnquiryResponse.portlettitle'/></span>	</div>
		
	<div id="alertBankInquiryResponseFormElemnts" class="portlet-content">
		<div id="alertBankInquiryResponseHeader">
			<div id="bankInquiryResponseAccessHeader" alertcode="12" class="instructions"><s:text name='alerts.bankEnquiryResponseAlertForm.header'/></div>
		</div>
		<s:form id="bankInquiryResponseAlertForm" action="AddEditBankEnquiryResponseAlertAction" namespace="/pages/jsp/alert" method="post" name="BankInquiryResponseAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			
			<div alertcontainer="delOptionsGrid" alertcode="12" align="center"></div>			
			
			
		  <div class="btn-row">
		  		<sj:a id="cancelBankInquiryResponseAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a>
	  			<sj:a id="addEditBankInquiryResponseAlert" alertbuttontype="addNew" formIds="bankInquiryResponseAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateBankInquiryResponseAlert" button="true" alertbuttontype="save" formIds="bankInquiryResponseAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
	  	 </div>
		</s:form>	
	</div>
	<div id="alertBankInquiryResponseFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertBankInquiryResponseForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertBankInquiryResponseForm"); 
</script> 