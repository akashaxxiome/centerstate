<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertInsufficientFundsForm" class="portlet">
	<ffi:help id="alerts_insufficient_funds" className="moduleHelpClass"/>
	<div id="alertInsufficientFundsTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="InsufficientFundsAccessPortletTitle" alertcode="4">
		<s:text name='alerts.insufficientFundsAlertForm.portlettitle'/></span>	</div>
	<div id="alertInsufficientFundsFormElemnts"  class="portlet-content">
		<s:form id="insufficientFundsAlertForm" action="addInsufficientFundsAlert" namespace="/pages/jsp/alert" method="post" name="InsufficientFundsAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			<div id="alertInsufficientFundsHeader">
				<div id="InsufficientFundsAccessHeader" alertcode="4" class="instructions"><s:text name='alerts.insufficientFundsAlertForm.header'/></div>
			</div>
			
			<table width="100%" class="tableData">
				<tr>
					<td width="50%" class="sectionsubhead"><s:text name='jsp.default_15'/></td>
					<td width="50%" class="sectionsubhead" id="accountBalanceThresholdFrequency" alertcode="4"><s:text name='alert.notification.frequency.label'/></td>
				</tr>
				<tr>
					<td>
						<div alertcontainer="accountSelectBox" alertcode="4"></div>
					</td>
					<td>
							<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><span><b><s:text name='alerts.frequency.everytime'/></b></span>
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><b><s:text name='alerts.frequency.firsttime'/></b></span>
					</td>
				</tr>
				<tr><td colspan="2"><span id="accountError" class="errorLabel"></span></td></tr>
			</table>
				<%-- <div alertcontainer="alertFrequencey" >
					<table>
						<tr><td align="center">
							<div id="accountBalanceThresholdFrequency" alertcode="4" class="instructions"><s:text name='alerts.accountBalanceThresholdAlertForm.frequency'/></div>
						</td></tr>
						<tr><td align="center">
							<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><span><b><s:text name='alerts.frequency.everytime'/></b></span>
						</td></tr>
						<tr><td align="center">
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><b><s:text name='alerts.frequency.firsttime'/></b></span>
						</td></tr>
					</table>
				</div> --%>
				
			<div alertcontainer="delOptionsGrid" alertcode="4" align="center" class="marginTop10"></div>			
			
			
		  <div class="btn-row">
		  	<sj:a id="cancelInsufficientFundsAlert" button="true" alertbuttontype="cancel" alertcode="4"><s:text name="jsp.default_82"/></sj:a></td>
  			<sj:a id="addEditInsufficientFundsAlert" alertbuttontype="addNew" formIds="insufficientFundsAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
  			<sj:a id="updateInsufficientFundsAlert" button="true" alertbuttontype="save" formIds="insufficientFundsAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		 </div>
	  
		</s:form>	
	</div>
	<div id="alertInsufficientFundsFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertInsufficientFundsForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	<div id="InsufficientFundsFooter">
		<span messageFor="footer"><s:text name='alerts.insufficientFundsAlertForm.footer'/></span><br>
		<span messageFor="inactiveAccount"><s:text name='alerts.inactiveAccounts'/></span>			
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertInsufficientFundsForm"); 
</script> 