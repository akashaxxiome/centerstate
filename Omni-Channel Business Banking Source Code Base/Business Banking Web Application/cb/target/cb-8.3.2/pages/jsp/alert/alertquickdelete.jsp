<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
		<div align="center">

    		<s:form id="deleteInboxAlertsForm" namespace="/pages/jsp/alert" validate="false" action="deleteInboxAlertsAction" method="post" name="deleteInboxAlertsForm" theme="simple">
	   		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>

                    <td colspan="2" align="center" width="100%">
                        <span class="sectionhead"><font color="red">
                            <ffi:cinclude value1="${deleteMessageList.Size}" value2="1">
                                <s:text name="jsp.alert_17"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${deleteMessageList.Size}" value2="1" operator="notEquals">
                                <s:text name="jsp.alert_16"/>
                            </ffi:cinclude>
                        </font></span>
                    </td>
                    <tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tr>
				</table>
				<div class="paneWrapper">
		  		<div class="paneInnerWrapper">
		  			<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">	
						<tr>
							<td width="60%" height="11">                        
		                        <span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_394"/></span>
		                    </td>
		                    <td width="40%">
								<span class="sectionsubhead"><s:text name="jsp.default_339"/></span>
							</td>
						</tr>
					</table>
					</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">	
                <% boolean toggle = false; %>
                <ffi:list collection="deleteMessageList" items="Message">
				<ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
                <% toggle = !toggle; %>
                <ffi:setProperty name="band" value='<%= toggle ? "ltrow" : "dkrow" %>'/>

                <tr class="<ffi:getProperty name="band" encode="false"/>">
					<td class="columndata" width="60%" >&nbsp;&nbsp;<ffi:getProperty name="Message" property="Subject"/></td>
                    <td class="columndata" width="40%"><ffi:getProperty name="Message" property="DateValue"/></td>
				</tr>
				<input type="hidden" name="selectedMsgIDs" value="<ffi:getProperty name='Message' property='ID'/>"></input>
                </ffi:list>
				</table>
				</div>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">	
                <tr>
					<td colspan="2"><span class="columndata"><br></span></td>
				</tr>
				<tr>
					<td colspan="2"><img src="/cb/web/multilang/grafx/spacer.gif" height="11" width="1" border="0"></td>
				</tr>
				<tr>
					<td colspan="2" valign="middle" align="center" class="ui-widget-header customDialogFooter">
						<%-- 
						<input class="submitbutton" type="button" value="CANCEL" onClick="location.replace('<ffi:getProperty name="SecurePath"/>alerts.jsp')">&nbsp;
						<input class="submitbutton" type="button" value="DELETE" onClick="location.replace('<ffi:getProperty name="SecureServletPath"/>DeleteMessages')">
						--%>
						
						<sj:a   button="true" 
								onClickTopics="cancelQuickDeleteAlert"
				        ><s:text name="jsp.default_82"/></sj:a>

			            <sj:a
							id="quickDeleteAlertSubmit"
							formIds="deleteInboxAlertsForm"
                            targets="quickMessagingResponse" 
                            button="true" 
                            onBeforeTopics="beforeQuickDeletingAlert"
                            onCompleteTopics="quickDeletingAlertComplete"
							onErrorTopics="errorQuickDeletingAlert"
                            onSuccessTopics="successQuickDeletingAlert"
                        ><s:text name="jsp.default_162"/></sj:a>
					
						
					</td>
				</tr>
			</table>
			</s:form>
			<br>
        </div>
