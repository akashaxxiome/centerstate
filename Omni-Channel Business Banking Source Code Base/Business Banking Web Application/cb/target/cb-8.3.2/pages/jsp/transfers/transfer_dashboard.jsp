<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.csil.core.common.PaymentEntitlementsDefines" %>

<%@ page import="com.sap.banking.transferconfig.beans.TransferSearchCriteria,com.sap.banking.transferconfig.constants.TransferConstants"%>
<%@ page import="com.ffusion.beans.user.UserLocale,
                 com.ffusion.beans.accounts.Accounts" %>

<!-- Check AppType of user -->
<ffi:setProperty name="IsConsumerUser" value="false"/>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

    <div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-transfer"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_222" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
    	</div>
    	
        <!-- dashboard items listing for transfers -->
        <div id="dbTransferSummary" class="dashboardSummaryHolderDiv">
	        <s:url id="goBackSummaryUrl" value="/pages/jsp/transfers/TransferSummaryAction.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
	
			<!-- transfer summary link -->
			<sj:a id="summaryBtnLink" href="%{goBackSummaryUrl}" targets="summary" button="true" onClickTopics="loadTransferSummary" onCompleteTopics="calendarToSummaryLoadedSuccess" cssClass="summaryLabelCls" title="%{getText('jsp.transfers.transfer_summary')}">
				<s:text name="jsp.transfers.transfer_summary"/>
			</sj:a>
			
			<!-- back to transfer summary link, appears while user clicks calendar button -->
		    <sj:a id="goBackSummaryLink" href="%{goBackSummaryUrl}" targets="summary" cssStyle="display:none" button="true" buttonIcon="ui-icon-transfer-e-w" title="%{getText('jsp.transfers_97')}" onCompleteTopics="calendarToSummaryLoadedSuccess">
			  	<s:text name="SUMMARY" />
			</sj:a>
			
			<!-- Transfers link -->
			<s:url id="transfersUrl" value="/pages/jsp/transfers/TransferSummaryAction.action" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="transfersLink"
				href="%{transfersUrl}"
				targets="summary"
				indicator="indicator"
				button="true"
				cssStyle="display:none"
				onSuccessTopics="transfersSummaryLoadedSuccess"
			><s:text name="jsp.transfers_7"/></sj:a>
			
	
			<!-- single transfer link -->
			<s:url id="singleTransferUrl" value="/pages/jsp/transfers/AddTransferAction_init.action" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
		    
		    <sj:a id="addSingleTransferLink" href="%{singleTransferUrl}" targets="inputDiv" button="true" title="%{getText('jsp.transfers_17')}" onClickTopics="beforeLoadTransferForm,toggleTransferGrids,loadSingleTransferForm" onCompleteTopics="loadSigleTransferFormComplete" onErrorTopics="errorLoadTransferForm">
			  	<s:text name="jsp.transfers_57.1" />
			</sj:a>
	
		    <!-- multiple transfer link -->
			<s:url id="multipleTransferUrl" value="/pages/jsp/transfers/AddTransferBatchAction_init.action" escapeAmp="false">
	        	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>		    
		    
		    <sj:a id="addMultipleTransferLink" href="%{multipleTransferUrl}" targets="inputDiv" button="true" title="%{getText('jsp.transfers_102')}" onClickTopics="beforeLoadTransferForm,loadMultipleForm,toggleTransferGrids,loadMultiTransferForm" onCompleteTopics="loadTransferFormComplete" onErrorTopics="errorLoadTransferForm">
			  	<s:text name="jsp.transfers_98" />
			</sj:a>
		</div>
		
		<s:set var="entitledDashboardSummariesCount" value="1"/>
		
		<!-- dashboard items listing for templates, hidden by default -->
		<div id="dbTemplateSummary" class="dashboardSummaryHolderDiv hideDbOnLoad ">
			<ffi:cinclude ifEntitled="<%= PaymentEntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
				<s:set var="entitledDashboardSummariesCount" value="%{#entitledDashboardSummariesCount+1}"/>
				<s:url id="templatesUrl" value="/pages/jsp/transfers/TransferTemplateSummaryAction.action" escapeAmp="false">
				    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				
				<!-- templates summary link -->
				<sj:a
					id="templatesLink"
					href="%{templatesUrl}"
					targets="summary"
					indicator="indicator"
					cssClass="summaryLabelCls"
					button="true"
					onClickTopics="loadTransferTemplateSummary"
					onSuccessTopics="templatesSummaryLoadedSuccess"
				><s:text name="jsp.default.templates.summary"/></sj:a>
				
				<s:url id="singleTransferTemplateUrl" value="/pages/jsp/transfers/AddTransferTemplateAction_init.action" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="templateEdit" value="true"/>
				</s:url>
				<sj:a id="addSingleTemplateLink" href="%{singleTransferTemplateUrl}" targets="inputDiv" button="true"
					title="%{getText('jsp.transfers_18')}"
					onClickTopics="beforeLoadTransferForm,loadSingleTempTransferForm" onCompleteTopics="loadSingleTransferTemplateFormComplete" onErrorTopics="errorLoadTransferForm">
					<s:text name="jsp.transfers_18" /></sj:a>
				<s:url id="multipleTransferTemplateUrl" value="/pages/jsp/transfers/AddTransferTemplateBatchAction_init.action" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="templateEdit" value="true"/>
				</s:url>
				<sj:a id="addMultipleTemplateLink" href="%{multipleTransferTemplateUrl}" targets="inputDiv" button="true"
					title="%{getText('jsp.transfers_99')}"
					onClickTopics="beforeLoadTransferForm,loadMultipleForm,toggleTransferGrids,loadMultiTempTransferForm"
					onCompleteTopics="loadTransferFormComplete"
					onErrorTopics="errorLoadTransferForm">
					<s:text name="jsp.transfers_100"/></sj:a>		
					
				<%-- edit single template --%>
			    <s:url id="editSingleTransferTemplateUrl" value="/pages/jsp/transfers/accounttransferedit.jsp" escapeAmp="false">
			    	<s:param name="templateEdit" value="true"/>
			    	<s:param name="SetFundsTran.Name" value="%{'SingleTransferTemplateList'}"/>
			   		 <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
			    <sj:a id="editSingleTransferTemplateLink" href="%{editSingleTransferTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
			    	 cssStyle="display:none;" title="%{getText('jsp.transfers_38')}"
			    	onClickTopics="beforeLoadTransferForm" onCompleteTopics="editSingleTransferTemplateFormComplete" 
			    	onErrorTopics="errorLoadTransferForm"><s:text name="jsp.transfers_38" />
			    </sj:a>   
			</ffi:cinclude>
		</div>
		
		<!-- dashboard items listing for file upload, hidden by default -->
		<div id="dbFileUploadSummary" class="dashboardSummaryHolderDiv hideDbOnLoad ">
				<!-- File upload summary -->
				<sj:a
					id="transferFileUploadSummary"
					indicator="indicator"
					cssClass="summaryLabelCls"
					button="true"
				><s:text name="jsp.default.file.upload.summary"/></sj:a>
				
			<% boolean canImport = false; %>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	    		<% canImport = true; %>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS_SWIFT_FILE_UPLOAD %>">
	   			 <% canImport = true; %>
			</ffi:cinclude>
	
			<%-- if Logged in user is consumer then dont show file import --%>
			<ffi:cinclude value1="${IsConsumerUser}" value2="true">
	   			 <% canImport = false; %>
			</ffi:cinclude>
			
			<% if (canImport) { %>
			<s:url id="transferFileImportUrl" value="/pages/jsp/transfers/transferimport.jsp" escapeAmp="false">
			    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="transferFileImportLink"
				href="%{transferFileImportUrl}"
				targets="selecttransferImportFormatOfFileUploadID"
				title="%{getText('jsp.transfers_103')}"
				indicator="indicator"
				button="true"
				cssClass="hidden"
				buttonIcon="ui-icon-disk"
				onClickTopics="onClickUploadTransferFormTopics"
				onSuccessTopics="transferFileImportSuccessHandler"
				onCompleteTopics="tabifyNotes"
			>
			  	&nbsp;<s:text name="jsp.default_539.1" /></sj:a>
			<% } %>
			
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				     <s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp" escapeAmp="false">
						 <s:param name="Section" value="%{'TRANSFER'}"/>
			             <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			        </s:url>
					<sj:a
						id="transferCustomMappingsLink"
						href="%{CustomMappingsUrl}"
						targets="mappingDiv"
						title="%{getText('jsp.default_135')}"
						indicator="indicator"
						button="true"
						onCompleteTopics="transfermappingSuccess"
					>
						  &nbsp;<s:text name="jsp.cash_1" /></sj:a>
					<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
						<s:param name="SetMappingDefinitionID" value="0"/>
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
					</s:url>
					<sj:a
						id="transferAddCustomMappingsLink"
						href="%{addCustomMappingUrl}"
						targets="inputCustomMappingDiv"
						title="%{getText('jsp.default_135')}"
						indicator="indicator"
						button="true"
						onClickTopics="beforeCustomMappingsOnClickTopics" 
						onCompleteTopics="addCustomMappingsOnCompleteTopics" 
						onErrorTopics="errorCustomMappingsOnErrorTopics"
						style="display:none;"
					>
						<s:text name="jsp.default_34" />
					</sj:a> 
					
					<!-- file import link -->
					<sj:a id="transferFileUploadLink" button="true" style="display:none;"
						onclick="$('#transferFileImportLink').trigger('click');" 
						onClickTopics="transferFileUploadLinkClickTopics" 
						title="%{getText('jsp.default_539')}"
					>
						<s:text name="jsp.default_539"/>
					</sj:a>
			
				</ffi:cinclude>
			</ffi:cinclude>
		</div>
		
		<!-- dashboard items listing for external account, hidden by default -->
		<div id="dbExternalAccountSummary" class="dashboardSummaryHolderDiv hideDbOnLoad ">
			    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
			    	<s:set var="entitledDashboardSummariesCount" value="%{#entitledDashboardSummariesCount+1}"/>
				    	<ffi:cinclude value1="${IsConsumerUser}" value2="true">
				    			<%-- additional links for consumer --%>
								
								<s:url id="manageExternalAccountsUrl" value="/pages/jsp/transfers/manage_external_transfer_accounts_summary.jsp" escapeAmp="false">
								    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
								<span id="externalAccountDiv">
									<sj:a
										id="manageExternalAccountsLink"
										href="%{manageExternalAccountsUrl}"
										targets="summary"
										indicator="indicator"
										button="true"
										onClickTopics="loadExternalAccSummary"
										onErrorTopics="errorLoadExternalAccounts"
										onSuccessTopics="externalAccountsSummaryLoadedSuccess"
										cssClass="summaryLabelCls"
									><s:text name="jsp.transfers_external_accounts"/></sj:a>
								</span>
								<s:url id="addExternalTransferAccountUrl" value="/pages/jsp/transfers/addExtTransferAccount_init.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							    <s:param name="init" value="true"></s:param>
								</s:url>
							    <sj:a id="addExternalTransferAccountLink" href="%{addExternalTransferAccountUrl}" targets="inputDiv" button="true"
							    	title="%{getText('jsp.transfers_add_external_transfer_account')}"
							    	onClickTopics="beforeLoadExternalTransferAccountForm,loadExternalTransferAccount" onCompleteTopics="loadExternalTransferAccountComplete" onErrorTopics="errorLoadExternalTransferAccountForm">
								  	&nbsp;<s:text name="jsp.transfers_add_account" /></sj:a>
								<s:url id="depositExternalTransferAccountUrl" value="/pages/jsp/transfers/external_transfer_account_deposit.jsp" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
							    <sj:a id="depositExternalTransferAccountLink" href="%{depositExternalTransferAccountUrl}" targets="inputDiv" button="true"
							    	cssStyle="display:none;" title="%{getText('jsp.transfers_17')}"
							    	onClickTopics="beforeLoadExternalTransferAccountForm" onCompleteTopics="loadExternalTransferAccountComplete" onErrorTopics="errorLoadExternalTransferAccountForm">
								  	&nbsp;<s:text name="jsp.transfers_deposit_account" /></sj:a>
								<s:url id="verifyExternalTransferAccountUrl" value="/pages/jsp/transfers/external_transfer_account_verify.jsp" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
							    <sj:a id="verifyExternalTransferAccountLink" href="%{verifyExternalTransferAccountUrl}" targets="inputDiv" button="true"
							    	cssStyle="display:none;" title="%{getText('jsp.transfers_17')}"
							    	onClickTopics="beforeLoadExternalTransferAccountForm" onCompleteTopics="loadExternalTransferAccountComplete" onErrorTopics="errorLoadExternalTransferAccountForm">
								  	&nbsp;<s:text name="jsp.transfers_verify_account" /></sj:a>
								
								
						</ffi:cinclude>
						<ffi:cinclude value1="${IsConsumerUser}" value2="false">
			    			 <script>
								extTransferAccountsURL = "<ffi:urlEncrypt url='/cb/pages/jsp/transfers/getExternalTransferAccountsAction_init.action'/>";
							</script>
							<div id="externalAccountDiv" class="hidden">
					            <sj:a
									id="externalAccounts"
									indicator="indicator"
									button="true"
									onSuccessTopics="externalAccountLoadedSuccess"
									onClick="ns.transfer.viewExternalAccounts(extTransferAccountsURL)"
								><s:text name="jsp.transfers_4"/></sj:a>
							</div>
			    		</ffi:cinclude>
				</ffi:cinclude>
		</div>
		
		<s:if test="#entitledDashboardSummariesCount > 1">
			<!-- More list option listing -->
			<div class="dashboardSubmenuItemCls">
				<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
	    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
				<!-- UL for rendering tabs -->
				<ul class="dashboardSubmenuItemList">
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS%>" >
						<li class="dashboardSubmenuItem"><a id="showdbTransferSummary" onclick="ns.common.refreshDashboard('showdbTransferSummary',true)"><s:text name="jsp.transfers_7"/></a></li>
					</ffi:cinclude>	
					
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
						<li class="dashboardSubmenuItem"><a id="showdbTemplateSummary" onclick="ns.common.refreshDashboard('showdbTemplateSummary',true)"><s:text name="jsp.default_5"/></a></li>
					</ffi:cinclude>
					
				  	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
					  	<ffi:cinclude value1="${IsConsumerUser}" value2="false">
							<li class="dashboardSubmenuItem"><a id="showdbExternalAccountSummary" onclick="$('#externalAccounts').trigger('click');"><s:text name="jsp.transfers_4"/></a></li>
						</ffi:cinclude>
				   		<ffi:cinclude value1="${IsConsumerUser}" value2="true">
				   			<li class="dashboardSubmenuItem"><a id="showdbExternalAccountSummary" onclick="ns.common.refreshDashboard('showdbExternalAccountSummary',true);$('#manageExternalAccountsLink').click()"><s:text name="jsp.transfers_4"/></a></li>
				   		</ffi:cinclude>
	  				</ffi:cinclude>
				</ul>
			</div>
		</s:if>

    </div>
    
	<%-- Following three dialogs are used for view, delete and inquiry transfer --%>
	<sj:dialog id="InquiryTransferDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="600">
	</sj:dialog>

	<sj:dialog id="viewTransferDetailsDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_11')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="auto" cssStyle="overflow-y:hidden;">
	</sj:dialog>
	
	<sj:dialog id="viewTransferTemplateDetailsDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_11.1')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="auto" cssStyle="overflow-y:hidden;">
	</sj:dialog>

	<sj:dialog id="deleteTransferDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_34')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	
	<sj:dialog id="skipTransferDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_109')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

	<sj:dialog id="deleteSingleTransferTemplatesDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_35')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" cssStyle="overflow-y:hidden;">
	</sj:dialog>

	<sj:dialog id="deleteMultipleTransferTemplatesDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_36')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" cssStyle="overflow-y:hidden;">
	</sj:dialog>

	<sj:dialog id="viewExternalAccountsDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_43')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="1200"  height="auto" cssStyle="overflow-y:hidden;">
	</sj:dialog>

	<sj:dialog id="viewExternalTransferDetailsDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_view_external_transfer_account')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	<sj:dialog id="deleteExternalTransferAccountDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_delete_external_transfer_account')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	
<script>
	$("#summaryBtnLink").find("span").addClass("dashboardSelectedItem");
</script>