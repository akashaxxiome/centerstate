<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<script language="JavaScript" type="text/javascript">
<!--

var templateSize = new Array();

$(function(){
   	$("#MultipleTransferTemplateList").lookupbox({
		"source":"/cb/pages/jsp/transfers/TransferTemplatesLookupBoxAction.action",
		"controlType":"server",
		"size":"30",
		"module":"multipletransferTemplates",
	});
   });
   
	var selectedvalue = $('#MultipleTransferTemplateList').val();
	enableLoadButton(selectedvalue);

	function enableLoadButton(selectedvalue)
	{
		$('#loadMultipleTransferTemplateSubmit').trigger('click');
	} 
	function checkTemplateType()
	{
	    var tempSel = document.forms.TemplateName.LoadFromTransferTemplate;
	    var selVal = tempSel.options[tempSel.selectedIndex].value;
	    if (selVal.length > 6 && selVal.substring(0,6) == "MULTI_")
	        document.forms.TemplateName.action = "accounttransfermultiplenew-wait.jsp";
	
	    document.forms.TemplateName.submit();
	}

// --></script>


               		 		<s:form id="LoadMultipleTransferTemplateForm" name="LoadMultipleTransferTemplateForm" method="post" action="/pages/jsp/transfers/AddTransferBatchAction_initTemplateLoad.action" theme="simple">
                    			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                        
                        			<s:set var="type" value="%{transferBatch.batchType}"></s:set>
									<s:if test='%{#type=="BATCH"}'>
										<s:select id="MultipleTransferTemplateList" class="txtbox" name="LoadFromTransferTemplate" size="1" 
										list="transferBatch.templateName" onChange="enableLoadButton(this.value)"
										listKey="transferBatch.templateName" listValue="transferBatch.templateName" >
										</s:select>
									</s:if>
									<s:else>
									 <select class="txtbox" id="MultipleTransferTemplateList" name="LoadFromTransferTemplate" onChange="enableLoadButton(this.value)">
                                </select>
									</s:else>
									
                            </s:form>
                        
                         	<sj:a
								id="loadMultipleTransferTemplateSubmit"
								targets="inputDiv"
								formIds="LoadMultipleTransferTemplateForm"
			                    button="true" 
			                    onBeforeTopics="beforeLoadSingleTransferTemplate"
			                    onSuccessTopics="loadSingleTransferTemplateSuccess"
			                    onErrorTopics="errorLoadSingleTransferTemplate"
								templatetype="mulitple"
			                    onCompleteTopics="loadMultipleTransferTemplateComplete"
			                    cssStyle="display:none"
			                ><s:text name="jsp.default_263" />  </sj:a>
