<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page
	import="com.ffusion.beans.accounts.Accounts,
					com.ffusion.csil.core.common.EntitlementsDefines,
					com.ffusion.beans.banking.TransferStatus"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<s:set var="transfer" value="#session.editTransfer" />

<s:set var="isRecurring" value="%{isRecurring=='true'}" />

<s:set var="isTemplate" value="#request.istemplateflow" />

<ffi:help id="payments_accounttransferedit" />

<span id="PageHeading" style="display: none;"><h1 id="editSingleTransferTitle" class="portlet-title"><s:text
		name="jsp.transfers_38" /></h1></span>

<s:set name="OpenEnded" value="#request.isOpenEnded" />
<s:set  name="Frequency" value="None" />

<!-- Check AppType of user -->
<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />

<s:include value="/pages/jsp/transfers/inc/accounttransferedit-js.jsp" />

<div class="leftPaneWrapper" role="form" aria-labelledby="transferSummary">
	<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="transferSummary"><s:text name="jsp.transfer.summary.label" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_217" /> :</span>
				<span id="editFromAcct" class="inlineSection floatleft labelValue">
					<s:property value="#transfer.fromAccount.accountDisplayText" />
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text	name="jsp.transfers_76" /> :</span>
				<span id="editToAcct" class="inlineSection floatleft labelValue">
					<s:property value="#transfer.toAccount.accountDisplayText" />
				</span>
			</div>
			<s:if test="%{#transfer.recTransferID!=null && #transfer.frequency != 'None'}">
					<div class="marginTop20 floatleft fxActionBtnCls"><sj:a id="loadRecModelFormButton"
		                button="true"
		                onclick="loadRecModel()"
		            ><s:text name="jsp.default.edit_orig_trans"/></sj:a></div>
		    </s:if>
	</div>
</div>
	
</div>

<div id="" class="formPanel formLeftMargin">
   <div id="" class="rightPaneWrapper transEditRgtPaneWidth rightPaneConfirmDvc">
		<div class="paneWrapper" role="form" aria-labelledby="transferDetails">
			<div class="paneInnerWrapper">
				<div class="header"><h2 id="transferDetails"><s:text name="jsp.transfer.details.label" /></h2></div>
				<div class="paneContentWrapper">	
					<s:form id="TransferEdit" namespace="/pages/jsp/transfers" validate="false" action="editTransferAction_verify" method="post" name="TransferEdit" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
						<input type="Hidden" name="transferDestination" id="TransferDest" value="<s:property value="#transfer.transferDestination"/>">
						<input id="recTransferID" name="recTransferID" value="<s:property value="%{recTransferID}"/>" type="hidden"/>
						<input id="transferType" name="transferType" value="<s:property value="%{transferType}"/>" type="hidden"/>
						<input id="ID" name="ID" value="<s:property value="%{#transfer.ID}"/>" type="hidden"/>
						<input id="TransferID" name="TransferID" value="<s:property value="%{TransferID}"/>" type="hidden"/>
						<input id="isRecModelEdit" name="isRecModel" value="<s:property value="%{isRecModel}"/>" type="hidden"/>
						<ffi:removeProperty name="tmp_url" />
						<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData tblFixedLayout">
						
						<tr>
							<td colspan="3" align="center"><ul id="formerrors"></ul></td>
						</tr>
						
							<tr class="ltrow_color">
								<td id="editTransferFromAccountLabel" class="sectionsubhead ltrow2_color"><label for="FromAccount"></label><s:text
										name="jsp.default_217" /></label> <span class="required" title="required">*</span></td>
								<td width="4%"></td>
								<td id="editTransferToAccountLabel" class="sectionsubhead ltrow2_color">
									<label for="ToAccount"><s:text name="jsp.transfers_76" /></label><span class="required" title="required">*</span></td>
								
							</tr>
							
							<tr>
								<td nowrap class="ltrow2_color" align="left" width="420px">
									<select class="txtbox" name="fromAccountID" id="FromAccount"
									size="1" onChange="onChangeCheck()" aria-labelledby="editTransferFromAccountLabel" aria-required="true">
										<s:if test="%{#transfer.FromAccountID!=''}">
											<option
												value="<s:property value='#transfer.FromAccountID'  />:<s:property value='#transfer.fromAccount.currencyCode' />" 
												selected>
												<s:property value="#transfer.fromAccount.accountDisplayText"/>
											</option>
										</s:if>
								</select>
								</td>
								<td></td>
								<td nowrap class="ltrow2_color" align="left"><select
									class="txtbox" name="toAccountID" id="ToAccount" size="1"
									onchange="onChangeCheck()" aria-labelledby="editTransferToAccountLabel" aria-required="true">
										<s:if test="%{#transfer.ToAccountID!=''}">
											<option
												value="<s:property value='#transfer.ToAccountID'  />:<s:property value='#transfer.toAccount.currencyCode' />"
												selected><s:property value="#transfer.toAccount.accountDisplayText"/>
											</option>
										</s:if>
								</select></td>
							</tr>
							
							<tr class="ltrow_color">
								<td><span id="fromAccountIDError"></span></td>
								<td></td>
								<td><span id="toAccountIDError"></span></td>
							</tr>
							
							<tr class="ltrow_color">
								<td id="editTransferAmountLabel" class="sectionsubhead ltrow2_color"><span style="display:inline-block; width:120px;"><label for="Amount"><s:text name="jsp.default_43" /></label><span class="required" title="required">*</span></span>
									<span id="CurrencySectionLabel" style="display: none">
										<span id="editTransferCurranyLabel" class="sectionsubhead ltrow2_color"><label for="Currency"><s:text name="jsp.default_125" /></label></span> <span class="required" title="required">*</span>
									</span>
								</td>
								<td></td>
								<td id="editTransferDateLabel" class="sectionsubhead ltrow2_color"
									nowrap><label for"Date"><s:text name="jsp.default_137" /></label> <span
									class="required">*</span></td>
							</tr>
							<tr class="ltrow_color">
								
								<td class="ltrow2_color" align="left"><s:textfield
										name="amount" id="Amount1" size="12" maxlength="15"
										value="%{#transfer.userAssignedAmount}" 
										cssClass="ui-widget-content ui-corner-all" aria-labelledby="Amount" aria-required="true"/> <span id="CurrencyLabel" class="columndata" style="display: none">&nbsp;</span>
									<span id="CurrencySection" style="display: none"><select
										class="txtbox" name="userAssignedAmountFlagName"
										id="CurrencySelect" aria-labelledby="Currency" aria-required="true">
											<option value="single" selected><s:text
													name="jsp.transfers_70" /></option>
									</select> <input type="hidden" id="OtherAmount" value="0.00">
								</span></td>
								<td></td>
								<td class="columndata ltrow2_color" align="left"><sj:datepicker
										value="%{#transfer.date}" id="AccountTransferEditDateId"
										name="date" label="%{getText('jsp.default_137')}" maxlength="10"
										buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all" aria-labelledby="editTransferDateLabel" aria-required="true"/>
									 <script>
										var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=1&calDisplay=select&calForm=TransferEdit&calTarget=EditTransfer.Date"/>';
										ns.common.enableAjaxDatepicker(
												"AccountTransferEditDateId", tmpUrl);
										$('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
									</script>
									
									</td>
							</tr>
							
							<tr>
								<td>
									<span id="amountError" style="width:120px;"></span>
									<span id="toAmountError" style="width:120px;"></span>
									<span id="userAssignedAmountError"></span>
								</td>
								<td></td>
								<td><span id="dateError"></span></td>
							</tr>
				
							<tr class="ltrow_color">
								<td id="editTransferMemoLabel" class="sectionsubhead ltrow2_color"><label for="Memo"><s:text name="jsp.default_279" /></label></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								
								<td class="columndata ltrow2_color" align="left">
								<s:textfield name="memo" size="47" maxlength="100"
									value="%{#transfer.memo}"
									cssClass="ui-widget-content ui-corner-all"  style="width: 265px" aria-labelledby="editTransferMemoLabel" />
							</td>
							<td></td><td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr class="ltrow_color">
								<s:if test="%{#isRecurring}">
									<td id="editTransferFrequencyLabel" class="sectionsubhead ltrow2_color">
										<s:text name="jsp.default_351" /></td>
								</s:if>
								<td></td>
								<td></td>
							</tr>
							<tr>
							<s:if test="%{#isRecurring}">
							   <td valign="top" align="left" colspan="3" id="repaetingTransfers">
								<%-- <div id="editRecModel">
									<s:text name="#transfer.frequency" /> &#45;
									<s:if test="%{#transfer.NumberTransfers==999}">
										<b><s:text name="jsp.default_446" /></b>
									</s:if> 
									<s:else>
										<b><s:property value="#transfer.numberTransfers" /> time(s)</b>
									</s:else>
								</div>
								<div id="editOrginalRecModel">
								 --%>	<s:select
										id="single_transfer_frequency_edit"
										cssClass="txtbox ui-corner-all" onchange="modifyFrequency();"
										list="%{#request.frequencyList}" name="frequency"
										value="#transfer.frequency" />  <script>
										$('#single_transfer_frequency_edit').selectmenu({
											width : 110
										});
									</script>
							
								<span id="editTransferNoOfTransfersLabel" class="columndata ltrow2_color"><input
									onclick="toggleRecurring();" type="radio" align="middle" 
									name="openEnded" value="true" <s:if test="%{isRecModel == 'false'}">disabled</s:if> <ffi:cinclude value1="${OpenEnded}" value2="true">checked</ffi:cinclude>"
									border="0"> <s:text name="jsp.default_446" /> <input
									onclick="toggleRecurring();" type="radio" align="middle" 
									name="openEnded" value="false" <s:if test="%{isRecModel == 'false'}">disabled</s:if> <ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals" >checked</ffi:cinclude>
									border="0" checked> <s:text name="jsp.transfers_1" />
									&nbsp; <input id="editTransferNoOfTransfersValue" type="text"
									name="numberTransfers" <s:if test="%{isRecModel == 'false'}">disabled</s:if>
									value="<s:property value="#transfer.numberTransfers"/>" size="3"
									maxlength="3" class="ui-widget-content ui-corner-all"> </span></div>
								</td>
							</s:if>
								
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><span id="frequencyValueError"></span> <span id="numberTransfersError"></span></td>
							</tr>
							<tr>
								<td class="ltrow2_color" colspan="3" nowrap valign="top"
									align="center"><br> 
									<s:if test="fromPortal=='true'">
									
									<sj:a id="cancelFormButtonOnInput" button="true" summaryDivId="summary" buttonType="cancel"
											onClickTopics="showSummary,cancelEditPendingTransferForm">
											<s:text name="jsp.default_82" />
										</sj:a>
				
										<sj:a id="editTransferSubmit" formIds="TransferEdit"
											targets="PendingTransactionWizardTabs #verifyDiv" button="true" validate="true"
											validateFunction="customValidation"
											onclick="onSubmitCheck(false)"
											onBeforeTopics="beforeVerifyPortalForm"
											onErrorTopics="errorHandlingEditTransfer"
											onSuccessTopics="updateVerifySectionEditTransfer">
											<s:text name="jsp.default_395" />
										</sj:a>
										<input type="hidden" name="fromPortal" value="true"></input>
									</s:if><s:else>
									
									
										<sj:a id="cancelFormButtonOnInput" button="true" summaryDivId="summary" buttonType="cancel"
											onClickTopics="showSummary,cancelForm">
											<s:text name="jsp.default_82" />
										</sj:a>
				
										<sj:a tooltip="%{getText('jsp.transfers_59')}"
											openDialog="innerdeletetransferdialog" button="true">
											<s:text name="jsp.default_162" />
										</sj:a>
				
										<sj:a id="editTransferSubmit" formIds="TransferEdit"
											targets="verifyDiv" button="true" validate="true"
											validateFunction="customValidation"
											onBeforeTopics="beforeVerifyTransferForm"
											onCompleteTopics="verifyTransferFormComplete"
											onErrorTopics="errorVerifyTransferForm"
											onSuccessTopics="successVerifyTransferForm"
											onClick="onSubmitCheck(false)">
											<s:text name="jsp.default_395" />
										</sj:a>
									</s:else>
									</td>
							</tr>
						</table>
					</s:form>
				</div>
			</div>
		</div>
   </div>
</div>

<s:set name="deleteDialogTitle"
	value="%{'Delete Account Transfer Confirm'}" scope="request" />
	
	<s:if test="fromPortal!='true'">


	<sj:dialog id="innerdeletetransferdialog" autoOpen="false" modal="true"
		cssClass="pmtTran_transferDialog staticContentDialog"
		title="%{#request.deleteDialogTitle}" height="auto" width="450"
		overlayColor="#000" overlayOpacity="0.7" showEffect="fold"
		hideEffect="clip" resizable="false">
		<div align="center">
			
			<s:form validate="false"
				action="/pages/jsp/transfers/deleteTransferAction.action"
				method="post" name="innerDeleteTransferForm"
				id="innerDeleteTransferForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
				<input type="hidden" name="transferID"
					value="<s:property value='#transfer.ID'/>" />
				<input type="hidden" name="transferType"
					value="<s:property value='#transfer.transferType'/>" />
				<input type="hidden" name="recTransferID"
					value="<s:property value='#transfer.recTransferID'/>" />
				<input type="hidden" name="isRecurring"
					value="<s:property value='%{isRecurring}'/>" />
				<input type="hidden" name="isRecModel"
					value="<s:property value='%{isRecModel}'/>" />
				<p id="deleteDialogMessage">
					<s:text name="jsp.transfers_21" />
				</p>

				<s:if test="%{#isRecurring && isRecModel == 'true'}">
					<span style="color: red" align="center"> <s:text
							name="jsp.transfers_90" /><br>
					<br>
					</span>
				</s:if>
				<div id="deleteDialogReturnMessage"></div>
				<sj:a id="deleteDialogCancelBtn" button="true"
					onClickTopics="closeDialog" title="%{getText('jsp.default_83')}">
					<s:text name="jsp.default_82" />
				</sj:a>
					<s:if test="fromPortal=='true'">
					<sj:a id="deleteSingleTransferLink"
						formIds="innerDeleteTransferForm" targets="loadingstatus"
						button="true" title="%{getText('jsp.default_162')}"
						onSuccessTopics="closeDialog,homeTransferDeleteSuccessTopics"
						effectDuration="1500">
						<s:text name="jsp.default_162" />
					</sj:a>
					</s:if>
					<s:else>
					<sj:a id="deleteDialogSubmitBtn" button="true"
						formIds="innerDeleteTransferForm" targets="resultmessage"
						onBeforeTopics="beforeDeleteTransfer"
						onCompleteTopics="deleteTransferComplete"
						onErrorTopics="errorDeleteTransfer"
						onSuccessTopics="successDeleteTransfer">
						<s:text name="jsp.default_162" />
					</sj:a>
					</s:else>
				
			</s:form>
		</div>

	</sj:dialog>
</s:if>

<ffi:removeProperty name="IsConsumerUser" />
