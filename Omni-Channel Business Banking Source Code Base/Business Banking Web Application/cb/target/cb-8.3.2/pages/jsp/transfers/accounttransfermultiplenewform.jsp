<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page
	import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.banking.TransferBatch"%>
<%@ page import="com.ffusion.beans.banking.Transfers"%>
<ffi:help id="payments_accounttransfermultiplenew" />

<%--Set the variables to be displayed on the form--%>
<s:include
	value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />
<s:set var="transferBatch" value="#session.transferBatch" />
<script>
				var transferSize = <s:property value="#transferBatch.transferCount" />;
				if(transferSize < 2) transferSize = 2;
		</script>
<%
			String sizeByRequest = com.ffusion.util.HTMLUtil.encode(request.getParameter("transferSize"));
			if(sizeByRequest!=null){
		%>
<script>
				transferSize = <%= sizeByRequest %>;
			</script>
<%}%>
<script>
			for(var i = 2; i < transferSize; i++){
				$(".columndataauto" + i).show();
				$(".columnLabel" + i).show();
				$(".columnHeading" + i).show();
				$(".columnError" + i).show();
			}
</script>


<!-- Check AppType of user -->
<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />
<style type="text/css">
.columndataauto0, .columnLabel0, .columnError0, .columnHeading0 {
	padding: 8px 0;
}

.columndataauto1, .columnLabel1, .columnError1, .columnHeading1{
	padding: 3px
}

.columndataauto2, .columnLabel2, .columnError2, .columnHeading2{
	padding: 3px;
	display: none
}

.columndataauto3, .columnLabel3, .columnError3, .columnHeading3{
	padding: 3px;
	display: none
}

.columndataauto4, .columnLabel4, .columnError4, .columnHeading4{
	padding: 3px;
	display: none
}
#multipleTransferForm input.hasDatepicker{width: 120px}
ul#formerrors{margin: 0 !important; text-align: center !important;}
</style>


<%--JS file used by the flow --%>
<s:include
	value="/pages/jsp/transfers/inc/accounttransfermultiplenewform-js.jsp" />
<div id="multipleTransferPanel" class="paneWrapper" role="form" aria-labelledby="transferDetails">
	<div id="multipleTransferForm" class="paneInnerWrapper">
	<div class="header"><h2 id="transferDetails"><s:text name="jsp.transfer.details.label" /></h2></div>
	<div class="paneContentWrapper">
		<span id="PageHeading" style="display:none;"><s:text name="jsp.transfers_14"/></span>
		<%--Form For getting mutliple trnsfer--%>
		<s:form id="TransferMultipleNew" namespace="/pages/jsp/transfers"
			validate="false" action="AddTransferBatchAction_verify"
			method="post" name="TransferMultipleNew" theme="simple">
			<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">
				<tr>
					<td colspan="3" align="center"><ul id="formerrors"></ul></td>
				</tr>
				<s:set var="count" value="0" />
				
				<%--Getting the Individual Transfer object by iterating over the transfers  --%>
				<s:set var="transfers" value="#transferBatch.transfers" />
				<s:iterator value="#transfers" var="transfer">
					<tr class="columnHeading<s:property value="#count" />">
						<td colspan="3"><h3 class="transactionHeading"><s:text name="jsp.default_441" /><s:property value="#count+1"/></h3></td>
					</tr>
					<tr>
						<td  colspan="3"  align="center" ><span id="transfers_<s:property value="#count" />_Error"></span></td>
					</tr>
					<tr class="columndataauto<s:property value='#categoryCount' />">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="columnLabel<s:property value="#count" />">
						<td id="addMultipleTransfer_fromAccountLabelId"
							class="sectionsubhead ltrow2_color" align="left"  width="45%" style="padding:0"><label for="addMultipleTransfer_fromAccountLabelId"><s:text
								name="jsp.default_217" /></label>
								<s:if test="%{#count==0}"><span class="required" title="required">*</span></s:if>
								</td>
						<td style="width: 10%">&nbsp;</td>
						<td id="addMultipleTransfer_toAccountLabelId"
							class="sectionsubhead ltrow2_color" align="left"  width="45%" style="padding:0"><label for="addMultipleTransfer_toAccountLabelId"><s:text
								name="jsp.default_424" /></label> <s:if test="%{#count==0}"><span class="required" title="required">*</span></s:if></td>
					</tr>
				
					
					<%--Transfer Destination Section --%>
					<input type="Hidden"
						name="transferBatch.transfers[<s:property value="#count" />].transferDestination"
						id="TransferDest<s:property value="#count" />"
						value="<s:property value='#transfer.transferDestination'/>">
					<tr class="columndataauto<s:property value="#count" />">
						
						<%--From Account Section --%>
						<td align="left" nowrap><select class="txtbox" 
							name="transferBatch.transfers[<s:property value="#count" />].fromAccountID"
							id="FromAccount<s:property value="#count" />" aria-labelledby="addMultipleTransfer_fromAccountLabelId" aria-required="true"
							onChange="onAccountChangeCheck('from',<s:property value="#count" />);">
								<s:if test="%{#transfer.fromAccountID!=null}">
									
									<option
										value="<s:property value='%{#transfer.fromAccountID}' />:<s:property value='#transfer.fromAccount.currencyCode'/>"
										selected><s:property value="#transfer.fromAccount.accountDisplayText"/>
									</option>
								</s:if>
						</select></td>
					
						<td style="width: 10%">&nbsp;</td>
						
						<%--To Account Section --%>
						<td align="left" nowrap><select class="txtbox" aria-labelledby="addMultipleTransfer_toAccountLabelId" aria-required="true"
							name="transferBatch.transfers[<s:property value="#count" />].toAccountID"
							id="ToAccount<s:property value="#count" />"
							onChange="onAccountChangeCheck('to',<s:property value="#count" />);">
								<s:if test="%{#transfer.toAccountID!=null}">
									
									<option
										value="<s:property value="%{#transfer.ToAccountID}" />:<s:property value='#transfer.toAccount.currencyCode'/>"
										selected><s:property value="#transfer.toAccount.accountDisplayText"/>
									</option>
								</s:if>
						</select></td>
					</tr>
					<tr class="columnError<s:property value="#count" />">
						<td colspan=""><span id="transfers_<s:property value="#count" />_fromAccountIDError"></span></td>
						<td style="width: 10%">&nbsp;</td>
						<td colspan=""><span id="transfers_<s:property value="#count" />_toAccountIDError"></span></td>
					</tr>
					<tr class="columnLabel<s:property value="#count" />">
				
						<td id="addMultipleTransfer_amountLabelId" class="sectionsubhead ltrow2_color" style="padding:0">
							<span style="display:inline-block; width:125px;"><label for="Amount"><s:text name="jsp.default_43" /></label><s:if test="%{#count==0}"><span class="required" title="required">*</span></s:if></span>
							<span id="CurrencyColumnHeader<s:property value="#count" />" style="display: none;"><label for="Currency"><s:text
										name="jsp.default_125" /></label> <s:if test="%{#count==0}"><span class="required">*</span></s:if></span>
						</td>
						<td style="width: 10%">&nbsp;</td>
						<td id="addMultipleTransfer_dateLabelId"
							class="sectionsubhead ltrow2_color" align="left" style="padding:0"><label for="Date">
							<s:text name="jsp.default_137" /></label> <s:if test="%{#count==0}"><span class="required" title="required">*</span></s:if>
						</td>
				   </tr>
				
					<tr class="columndataauto<s:property value="#count" />">
				
						<%--Amount Section --%>
						<td align="left" nowrap  width="45%"><input
							class="txtbox ui-widget-content ui-corner-all" type="text"
							name="transferBatch.transfers[<s:property value="#count" />].amount"
							size="12" maxlength="15" class="form_fields" aria-labelledby="addMultipleTransfer_amountLabelId" aria-required="true"
							value="<s:if test="%{!#transfer.userAssignedAmountValue.isZero}"><s:property value='#transfer.userAssignedAmount'/></s:if>" 
							id="Amount<s:property value="#count" />"
							onChange="calculateTotal();" onBlur="calculateTotal();"
							onkeyup="calculateTotal();">
				
							<span id="CurrencyLabel<s:property value='#count' />"
							style="display: none; margin-left:10px;">&nbsp;</span><span
							id="CurrencySection<s:property value='#count' />"
							style="display: none;"> <select
								class="ui-widget-content ui-corner-all"
								name="transferBatch.transfers[<s:property value="#count" />].userAssignedAmountFlagName"
								id="CurrencySelect<s:property value="#count" />" aria-labelledby="CurrencyColumnHeader<s:property value="#count" />" 
								onChange="calculateTotal();">
									<option value="single" selected><s:text
											name="jsp.transfers_110" /></option>
							</select> <input type="hidden"
								name="transferBatch.transfers[<s:property value="#count" />].toAmount"
								id="OtherAmount<s:property value="#count" />" value="0.00">
						</span> &nbsp;&nbsp;</td>
					<td style="width: 10%">&nbsp;</td>
					<%--Transfer Date Section --%>
						<td align="left" nowrap width="45%"><s:set var="transferID"
								value="#transfer.ID" /> <sj:datepicker
								value="%{#transfer.date}" id="%{'Date'+#count}"
								name="%{'transferBatch.transfers['+#count+'].date'}"
								label="%{getText('jsp.default_137')}" maxlength="10"
								buttonImageOnly="true" aria-labelledby="addMultipleTransfer_dateLabelId" aria-required="true"
								cssClass="ui-widget-content ui-corner-all" /> <script>
											var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=1&calDisplay=select&calForm=MultipleTransferNew"/>';
											ns.common.enableAjaxDatepicker("Date<s:property value='#count' />", tmpUrl);
											$('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
											</script>
											</td>
						<input id="hiddenDefaultDateId<s:property value="#count"/>" type="hidden" value="<s:property value="#transfer.date"/>" />
					</tr>
					<tr class="columnError<s:property value="#count" />">
						<td>
							<span id="transfers_<s:property value="#count" />_amountError" style="width:125px; display:inline-blcok"></span>
							<span id="transfers_<s:property value="#count" />_toAmountError" style="width:125px; display:inline-blcok"></span>
							<span id="transfers_<s:property value="#count" />_userAssignedAmountError"></span>
						</td>
						<td style="width: 10%">&nbsp;</td>
						<td><span id="transfers_<s:property value="#count" />_dateError"></span></td>
					</tr>
					
					<tr class="columnLabel<s:property value="#count" />">
						<td id="addMultipleTransfer_memoLabelId"
										class="columndata ltrow2_color columndataauto<s:property value="#count" />"><label for="Memo"><s:text
											name="jsp.default_279" /></label>
						</td>
						<td style="width: 10%">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="columndataauto<s:property value="#count" />">
						<td align="left" valign="middle">
							
									<%--Transfer Memo Section --%>
									 <input
										class="txtbox ui-widget-content ui-corner-all"
										name="transferBatch.transfers[<s:property value="#count" />].memo"
										id="Memo<s:property value="#count" />" size="42" aria-labelledby="addMultipleTransfer_memoLabelId"
										maxlength="100" class="form_fields"
										value="<s:property value="#transfer.memo"/>">&nbsp;&nbsp;&nbsp;&nbsp;
										<span id="Category<s:property value="#count" />"
										style="display: none"> Category: <select
											class="ui-widget-content ui-corner-all"
											name="transferBatch.transfers[<s:property value="#count" />].REGISTER_CATEGORY_ID"
											id="CategorySelect<s:property value="#count" />"
											class="form_elements">
												<option value="0">Unassigned</option>
												<ffi:setProperty name="RegisterCategories" property="Filter"
													value="PARENT_CATEGORY=-1,ID!0,AND" />
												<ffi:setProperty name="RegisterCategories"
													property="FilterSortedBy" value="NAME" />
												<ffi:list collection="RegisterCategories" items="Category">
													<option
														value="<ffi:getProperty name='Category' property='Id'/>"
														<ffi:cinclude value1="${Transfer.REGISTER_CATEGORY_ID}" value2="${Category.Id}">selected</ffi:cinclude>><ffi:getProperty
															name="Category" property="Name" /></option>
													<ffi:setProperty name="RegisterCategoriesCopy"
														property="Filter" value="PARENT_CATEGORY=${Category.Id}" />
													<ffi:setProperty name="RegisterCategoriesCopy"
														property="FilterSortedBy" value="NAME" />
													<ffi:list collection="RegisterCategoriesCopy"
														items="Category1">
														<ffi:setProperty name="Compare" property="Value2"
															value="${Category1.Id}" />
														<option
															value="<ffi:getProperty name="Category1" property="Id"/>"
															<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;
															<ffi:getProperty name="Category" property="Name" />:
															<ffi:getProperty name="Category1" property="Name" /></option>
													</ffi:list>
												</ffi:list>
										</select>
									</span>
						</td>
						<td style="width: 10%">&nbsp;</td>
						<%--Clear Row Button --%>
						<td align="right">
							<sj:a id="clearRow%{#count}" button="true" onclick="return clearRow('%{#count}')" title="%{getText('Clear')}">
								<s:text name="Clear" />
							</sj:a>
						</td>
					</tr>

					<%--Next Row for the same Transfer --%>
					<s:set var="count" value="%{#count+1}" />
				</s:iterator>
			
				
				<input type="hidden" name="CSRF_TOKEN"
					value="<ffi:getProperty name='CSRF_TOKEN'/>" />
				<tr>
					<td colspan="3" align="right">
					<hr style="border:1px solid #b0b0b0; margin:10px 0" width="100%" height="1">
					<%--Delete Row Button --%>
							<span class="deleteRow" align="right"><sj:a id="deleteRowID"
									button="true">
									<s:text name="jsp.transfers_6" />
								</sj:a></span>
						
						
							<%--Add Row Button --%>
							<span class="addRow"><sj:a id="addRowID"
									button="true">
									<s:text name="jsp.transfers_3" />
								</sj:a></span>
						</td>
				</tr>
			
				<%--Form Buttons Section --%>
				<tr class="ltrow_color">
					<td class="columndata ltrow2_color" align="center" colspan="3">
						<br> <sj:a id="cancelMultipleTransferSubmit" button="true" summaryDivId="summary" buttonType="cancel"
							onClickTopics="showSummary,cancelMultipleTransferNewForm">
							<s:text name="jsp.default_82" />
						</sj:a> <sj:a id="verifyTransferSubmit2" formIds="TransferMultipleNew"
							targets="verifyDiv" button="true" validate="true"
							validateFunction="customValidation"
							onBeforeTopics="beforeVerifyTransferForm"
							onCompleteTopics="verifyTransferFormComplete"
							onClickTopics="onSubmitCheckTopics"
							onErrorTopics="errorVerifyTransferForm"
							onSuccessTopics="successVerifyTransferForm">
							<s:text name="jsp.default_395" />
						</sj:a>
					</td>
				</tr>
			</table>
		</s:form>
		</div>
	</div>
</div>
<ffi:removeProperty name="fromPortalPage" />
<ffi:removeProperty name="IsConsumerUser" />

<%--Field Error Rows --%>
			