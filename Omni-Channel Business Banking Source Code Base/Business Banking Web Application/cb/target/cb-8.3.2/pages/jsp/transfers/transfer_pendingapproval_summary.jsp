<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_accounttransferPendingApprovalSummary" />

	<ffi:setProperty name="gridDataType" value="json"/>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:setProperty name="gridDataType" value="local"/>
	</ffi:cinclude>

	<ffi:setGridURL grid="GRID_transferPendingApprovalSummary" name="ViewURL" url="/cb/pages/jsp/transfers/viewTransferAction.action?transferID={0}&transferType={1}&recTransferID={2}&isRecModel={3}" parm0="ID" parm1="TransferType" parm2="RecTransferID" parm3="RecModel" />
	<ffi:setGridURL grid="GRID_transferPendingApprovalSummary" name="EditURL" url="/cb/pages/jsp/transfers/editTransferAction_input.action?transferID={0}&transferType={1}&recTransferID={2}&isRecModel={3}" parm0="ID" parm1="TransferType" parm2="RecTransferID" parm3="RecModel" />
	<ffi:setGridURL grid="GRID_transferPendingApprovalSummary" name="DeleteURL" url="/cb/pages/jsp/transfers/deleteTransferAction_init.action?transferID={0}&transferType={1}&recTransferID={2}&isRecModel={3}" parm0="ID" parm1="TransferType" parm2="RecTransferID" parm3="RecModel" />
	<ffi:setGridURL grid="GRID_transferPendingApprovalSummary" name="SkipURL" url="/cb/pages/jsp/transfers/skipTransferAction_init.action?transferID={0}&transferType={1}&recTransferID={2}&isSkipInstance=true" parm0="ID" parm1="TransferType" parm2="RecTransferID" />
	<ffi:setGridURL grid="GRID_transferPendingApprovalSummary" name="InquireURL" url="/cb/pages/jsp/transfers/sendTransferInquiryAction_initTransferInquiry.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID" />

		<ffi:setProperty name="tempURL" value="/pages/jsp/transfers/getTransfersAction_getPendingApprovalTransfers.action?collectionName=PendingApprovalTransfers&GridURLs=GRID_transferPendingApprovalSummary" URLEncrypt="true"/>
        <s:url id="pendingApprovalTransfersUrl" value="%{#session.tempURL}" escapeAmp="false"/>
		<sjg:grid
			id="pendingApprovalTransfersID"
			caption=""
			dataType="%{#session.gridDataType}"
			href="%{pendingApprovalTransfersUrl}"
			pager="true"
			gridModel="gridModel"
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			rownumbers="true"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			sortable="true"
			sortname="date"
			sortorder="asc"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			onGridCompleteTopics="addGridControlsEvents,pendingApprovalTransfersGridCompleteEvents"
			>

			<sjg:gridColumn name="date" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="80"/>
			<sjg:gridColumn name="type" index="type" title="%{getText('jsp.default_444')}" formatter="ns.transfer.customTypeColumn" sortable="true" width="100"/>

			<sjg:gridColumn name="combinedAccount" index="Accounts" title="%{getText('jsp.transfers_108')}" sortable="false" width="250" formatter="ns.transfer.formatCombinedAccount"/>

			<ffi:cinclude value1="${UserType}" value2="Corporate" operator="equals">
				<sjg:gridColumn name="fromAccount.accountDisplayText" index="fromAccountNickName" title="%{getText('jsp.transfers_46')}" sortable="true" width="150" hidden="true" hidedlg="false"/>
				<sjg:gridColumn name="toAccount.accountDisplayText" index="toAccountNickName" title="%{getText('jsp.transfers_77')}" sortable="true" width="150" hidden="true" hidedlg="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${UserType}" value2="Consumer" operator="equals">
				<sjg:gridColumn name="fromAccount.accountDisplayText" index="consumerFromAccount" title="%{getText('jsp.transfers_46')}" sortable="true" width="150" hidden="true" hidedlg="false"/>
				<sjg:gridColumn name="toAccount.accountDisplayText" index="consumerToAccount" title="%{getText('jsp.transfers_77')}" sortable="true" width="150" hidden="true" hidedlg="false"/>
			</ffi:cinclude>
			<sjg:gridColumn name="map.Frequency" index="mapFrequencyValue" title="%{getText('jsp.default_215')}" formatter="ns.transfer.formatTransferFrequencyColumn" sortable="true" width="100"/>
			<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="120"/>

			<sjg:gridColumn name="combinedAmount" index="amount" title="%{getText('jsp.transfers_text_amount')}" sortable="true" width="120" formatter="ns.transfer.formatCombinedAmount"/>

			<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_489')}" formatter="ns.transfer.formatFromAmountColumn" sortable="true" width="80" align="right" hidden="true" hidedlg="false"/>
			<sjg:gridColumn name="toAmountValue.currencyStringNoSymbol" index="toAmountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_512')}" formatter="ns.transfer.formatToAmountColumn" sortable="true" width="60" align="right" hidden="true" hidedlg="false"/>
			<sjg:gridColumn name="isAmountEstimated" index="isAmountEstimated" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="isToAmountEstimated" index="isToAmountEstimated" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="memo" index="memo" title="Memo" sortable="false" width="120" hidden="true" hidedlg="true"/>

			<sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.transfer.formatPendingApprovalsTransfersActionLinks" width="90" search="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
			<sjg:gridColumn name="trackingID" index="trackingID" title="%{getText('jsp.default_435')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="fromAccount.displayText" index="fromAccountDisplayText" title="%{getText('jsp.transfers_48')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="fromAccount.currencyCode" index="fromAccountCurrencyCode" title="%{getText('jsp.transfers_49')}" hidden="true" hidedlg="true"/>

			<sjg:gridColumn name="toAccount.displayText" index="toAccountDisplayText" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="toAccount.currencyCode" index="toAccountCurrencyCode" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="transferDestination" index="transferDestination" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="transferType" index="transferType" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="amount" index="amount" title="" sortable="true" hidden="true" hidedlg="true"/>

			<sjg:gridColumn name="approverName" index="approverName" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="userName" index="userName" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="status" index="status" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="rejectReason" index="rejectReason" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canEdit" index="canEdit" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canDelete" index="canDelete" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="frequency" index="frequency" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="displayTextRoA" index="displayTextRoA" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.transfer.formatApproversInfo" />

			<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.InquireURL" index="InquireURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>

		</sjg:grid>
		<script>
			$("#pendingApprovalTransfersID").jqGrid('setColProp','ID',{title:false});

		</script>

<ffi:removeProperty name="gridDataType"/>