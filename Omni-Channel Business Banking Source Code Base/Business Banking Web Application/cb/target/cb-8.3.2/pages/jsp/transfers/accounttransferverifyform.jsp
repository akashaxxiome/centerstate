<%--
Name: 			accounttransferverifyform.jsp

Flow(s):		Add Single Transfer
				Add Recurring Transfer
				Edit Single Transfer
				Edit Recurring Transfer
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.banking.TransferDefines"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.banking.TransferStatus"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%--Set variables to be used on the form --%>

<s:set var="isNewTransferFlow"
	value="%{flowType=='NewTransfer'}" />
<s:if test="%{#isNewTransferFlow}">
	<s:set var="transfer" value="%{#session.newTransfer}" />
	<s:set name="strutsActionName" value="%{'AddTransferAction'}" />
</s:if>
<s:else>
	<s:set var="transfer" value="#session.editTransfer" />
	<s:set name="strutsActionName" value="%{'editTransferAction'}" />
</s:else>
<s:set var="date" value="%{#transfer.date}" />

<ffi:help id="payments_accounttransferverify" />


<%--Form for Verify Transfer Page.To be Shared between New and Edit Transfer Flow --%>
<s:form id="TransferNewVerify" namespace="/pages/jsp/transfers"
	validate="false" action="%{#strutsActionName + '_execute'}"
	method="post" name="TransferNewVerify" theme="simple">
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<input id="recTransferID" name="recTransferID" value="<s:property value="%{recTransferID}"/>" type="hidden"/>
	<input id="transferType" name="transferType" value="<s:property value="%{transferType}"/>" type="hidden"/>
	<input id="ID" name="ID" value="<s:property value="%{#transfer.ID}"/>" type="hidden"/>
	<input id="TransferID" name="TransferID" value="<s:property value="%{TransferID}"/>" type="hidden"/>
	<input id="isRecModel" name="isRecModel" value="<s:property value="%{isRecModel}"/>" type="hidden"/>
	<s:if test="%{#transfer.transferDestination != 'ITOE'}">
		<input type="hidden" name="ignoreMfa" value="true"/>
	</s:if>
<div class="leftPaneWrapper" role="form" aria-labelledby="transferSummary"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="transferSummary"><s:text name="jsp.transfer.summary.label" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%" class="floatleft">
			<span id="verifyTransfer_fromAccountLabelId"  class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_217" /> :</span>
			<span id="verifyTransfer_fromAccountValueId" class="inlineSection floatleft labelValue" ><s:property value="#transfer.fromAccount.accountDisplayText" /></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
			<span id="verifyTransfer_toAccountLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_424" /> :</span>
			<span id="verifyTransfer_toAccountValueId" class="inlineSection floatleft labelValue">			
				<s:property value="#transfer.toAccount.accountDisplayText" />
			</span>
		</div>
	</div>
</div>
</div>
<div class="confirmPageDetails">
	<div class="blockWrapper">
		<s:if test="isRequestDateAdjusted=='true'">
			<div id="verifyTransferDateWarningMessageID" class="sectionsubhead" style="color: red" align="center">
				<strong><s:text name="jsp.transfers_91" /></strong>
			</div>
		</s:if>
		
		
		<%-- Show warning message in case the flow is edit for recurring transfer --%>
		<s:if test="!#isNewTransferFlow && isRecurring=='true' && isRecModel == 'true'">
			<div id="verifyTransferWarningMessageID" class="sectionsubhead" style="color: red" align="center">
				<strong><s:text name="jsp.transfers_89" /></strong>
			</div>
		</s:if>
		
		<div  class="blockHead" role="form" aria-labelledby="transactionSummary"><h2 id="transactionSummary"><s:text name="jsp.transaction.summary.label" /></h2></div>
		<div class="blockContent label140">
			<%--If the Transfer Date is  adjusted i.e. transfer  date entered by user is different than transfer effective date --%>
				<s:if test="isRequestDateAdjusted=='true'">
					<div class="blockRow">
						<span id="verifyTransferRequestedDateLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.transfers_66" /> :</span>
						<s:set var="mapKey" value="%{'REQUESTED_DATE'}" />
		
						<span id="verifyTransferRequestedDateValueId"><s:property value="#transfer.get(#mapKey)" /></span>
					</div>
					<div class="blockRow">
						<span id="verifyTransferActualDateLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.transfers_12" /> :</span>
						<span id="verifyTransferActualDateValueId"><s:property value="#transfer.date" />
						<span class="sectionsubhead"><s:text name="jsp.transfers_8" /></span></span>
					</div>
				</s:if>
			
			<%--If the Transfer Date is not adjusted i.e. transfer  date entered by user is same as transfer effective date --%>
			<s:if test="isRequestDateAdjusted=='false'">
				<div class="blockRow">
					<span id="verifyTransfer_dateLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_137" /> :</span>
					<span id="verifyTransfer_dateValueId"><s:property value="#transfer.date" /></span>
				</div>
			</s:if>
			
			<div class="blockRow">		
				<%--Transfer From Account Display --%>
				<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
						<div class="inlineBlock" style="width: 50%">
							<span id="verifyTransfer_amountLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43" /> :</span>
							<span id="verifyTransfer_amountValueId"><s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" /> 
								<s:property value="#transfer.AmountValue.CurrencyCode" />
							</span>
						</div>
				</s:if>
				<s:else>
						<div class="inlineBlock" style="width: 50%">
							<span id="verifyTransferFrom_amountLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_489" /> :</span>
							<span id="verifyTransferFrom_amountValueId">
								<s:if test="#transfer.isAmountEstimated">
			                                        &#8776;
													<s:set var="DisplayEstimatedAmountKey" value="true" />
								</s:if> <s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" />
								<s:property value="#transfer.AmountValue.CurrencyCode" /></span>
						</div>
				</s:else>
				<s:set var="DisplayEstimatedAmountKey" value="false" />
			
				
				
				<%--Transfer From Account Display --%>
			
				<%--Transfer Amount display.If MultiCurrency Transfer Amount and To Amount is displayed --%>
				<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
				</s:if>
				<s:else>
					<div class="inlineBlock">
						<span id="verifyTransferTo_amountLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_512" /> :</span>
						<span id="verifyTransferTo_amountValueId">
							<s:if test="#transfer.isToAmountEstimated">
		                                        &#8776;
												<s:set var="DisplayEstimatedAmountKey" value="true" />
							</s:if> <s:property value="#transfer.ToAmountValue.CurrencyStringNoSymbol" />
							<s:property value="#transfer.ToAmountValue.CurrencyCode" /></span>
					</div>
				</s:else>
			</div>
		 </div>
			<div  class="blockHead"><s:text name="jsp.miscellaneous.label" /></div>
			<div class="blockContent">
				
					<%--Display the Transfer Destinition.If the Transfer is Internal to Internal, External to Internal or Internal to External --%>
				<ffi:cinclude
					ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
					<s:set var="TRANSFER_BOOK" value="%{'ITOI'}" />
					<%--Values to be in sync with com.ffusion.beans.banking.TransferDefines --%>
					<s:set var="TRANSFER_ITOE" value="%{'ITOE'}" />
					<s:set var="TRANSFER_ETOI" value="%{'ETOI'}" />
					<div class="blockRow">
						<span id="verifyTransfer_typeLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_444" /> :</span>
						<span id="verifyTransfer_typeValueId">
						<s:if test="%{#transfer.transferDestination==#TRANSFER_BOOK || #transfer.transferDestination=='INTERNAL'}">
								<s:text name="jsp.transfers_53" />
							</s:if>  <s:if test="%{#transfer.transferDestination==#TRANSFER_ITOE}">
								<s:text name="jsp.transfers_52" />
							</s:if> <s:if test="%{#transfer.transferDestination==#TRANSFER_ETOI}">
								<s:text name="jsp.transfers_44" />
							</s:if></span>
					</div>
				</ffi:cinclude>
				<%--Transfer Memo --%>
				<s:if test="%{memo!=''}">
					<div class="blockRow">
						<span id="verifyTransfer_memoLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279" /> :</span>
						<span id="verifyTransfer_memoValueId"><s:property value="#transfer.memo" /></span>
					</div>
				</s:if>
			</div>
			<s:if test="isRecurring=='true'">
			<div  class="blockHead"><s:text name="jsp.recurrence.label" /></div>
			<div class="blockContent">
				<%-- If Transfer is Recurring Show Frequency and Transfer count--%>
					<%-- Display Transfer Frequency --%>
					<div class="blockRow">
						<span id="verifyTransfer_recuringLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351" /> :</span>
						<span id="verifyTransfer_recuringValueId"><s:property value="#transfer.frequency" /></span>
					</div>
					<%-- Display Transfer Count --%>
				
					<div class="blockRow">
						<span id="verifyTransfer_noOfTransfersLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_443" /> :</span>
						<span id="verifyTransfer_noOfTransfersValueId">
							<s:if
								test="%{#transfer.isOpenEnded()}">
								<s:text name="jsp.default_446" />
							</s:if> <s:else>
								<s:property value="#transfer.numberTransfers" />
							</s:else></span>
					</div>
			</div>
			</s:if>
		</div>
		<div class="mltiCurrenceyMessage">
			<%--Display Message in case of Mult Currecny Transfer --%>
			<s:if test="%{#DisplayEstimatedAmountKey}"><span class="required">&#8776; <s:text name="jsp.default_241" />
					</span>
			</s:if>
			<s:else>
			</s:else>
		</div>
		<s:if test="isSameDayTransfer">
			<div id="sameDayMsg" class="sectionsubhead" align="left">
				<strong><s:text name="jsp.transfers_114" /></strong>
			</div>
		</s:if>
		<div class="btn-row">
			<s:if
					test="fromPortal=='true'">
					<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
					<sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel"
						onClickTopics="showSummary,cancelEditPendingTransferForm">
						<s:text name="jsp.default_82" />
					</sj:a>
					<sj:a id="backFormButton" button="true"
						onClickTopics="backPortalEditTransfer,hideOperationResult">
						<s:text name="jsp.default_57" />
					</sj:a>
					<sj:a id="sendSingleTransferSubmit" formIds="TransferNewVerify"
						targets="PendingTransactionWizardTabs #confirmDiv" button="true"
						onSuccessTopics="sendTransferPortalFormSuccess"
						onErrorTopics="errorSendTransferFormPortal">
						<s:text name="jsp.default_378" />
					</sj:a>
					<input type="hidden" name="fromPortal" value="true"></input>
				</s:if> <s:else>
					<s:if test="%{#newTransfer.template!=null}">
						<sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel"
							onClickTopics="showSummary,cancelTemplatesForm">
							<s:text name="jsp.default_82" />
						</sj:a>
					</s:if>
					<s:else>
						<sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel"
							onClickTopics="showSummary,cancelForm">
							<s:text name="jsp.default_82" />
						</sj:a>
					</s:else>
					<sj:a id="backFormButton" button="true"
						onClickTopics="backToInput,hideOperationResult">
						<s:text name="jsp.default_57" />
					</sj:a>
					<sj:a id="sendSingleTransferSubmit" formIds="TransferNewVerify"
						targets="confirmDiv" button="true"
						onBeforeTopics="beforeSendTransferForm"
						onSuccessTopics="sendTransferFormSuccess"
						onErrorTopics="errorSendTransferForm"
						onCompleteTopics="sendTransferFormComplete">
						<s:text name="jsp.default_378" />
					</sj:a>
				</s:else>
		</div>
</div>
</s:form>
