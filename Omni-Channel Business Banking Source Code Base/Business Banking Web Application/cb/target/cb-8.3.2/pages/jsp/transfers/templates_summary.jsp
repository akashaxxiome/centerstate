<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<!-- Check AppType of user -->
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="UserType" value="Corporate"/>
</ffi:cinclude>	
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="UserType" value="Consumer"/>
</ffi:cinclude>
	
	<div id="transferTemplateGridTabs" class="portlet gridPannelSupportCls">
		<div class="portlet-header">
			<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.transfers_111"/></h1>
		    <div class="searchHeaderCls">
				<div class="summaryGridTitleHolder" style="margin-left: 20px;">
					<span id="selectedGridTab" class="selectedTabLbl" style="padding-left: 0;">
						<s:text name="jsp.transfers_72"/>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
						<s:iterator value="%{orderedTransferTemplateTabs}" var="tabName">
							<s:if test="%{#tabName == 'single'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('single')" >
									<s:text name="jsp.transfers_72"/>
								</span>
							</s:if>
							<s:elseif test="%{#tabName == 'multiple'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('multiple')">
									<s:text name="jsp.transfers_56"/>
								</span>
							</s:elseif>
						</s:iterator>
					</div>
				</div>
			</div>
	    </div>
	    
	    <div class="portlet-content">
	    	<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="singleSummaryGrid" gridId="singleTransfersTemplatesGridID" class="gridSummaryContainerCls" >
					<s:action namespace="/pages/jsp/transfers" name="initTransferTemplatesSummaryGridAction_getSingleTemplatesGrid" executeResult="true"/>
				</div>
				<div id="multipleSummaryGrid" gridId="multipleTransfersTemplatesGridID" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/transfers" name="initTransferTemplatesSummaryGridAction_getMultipleTemplatesGrid" executeResult="true"/>
				</div>
			</div>
	    </div>
	
		<%-- Set following hidden field to identify the module to which tab belong to, this is required in case of save/load tab settings --%>
		<input type="hidden" id="getTransID" value="<s:property value="%{moduleID}"/>" />
		
		<div id="transferTemplateSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('transferTemplateGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	</div>

<script>   
	//Initialize portlet with settings icon
	ns.common.initializePortlet("transferTemplateGridTabs");
</script>