
<%--
Name: 			accounttransferform.jsp

Flow(s):		Add Single Transfer
				Add Recurring Transfer
				Load from Transfer Template
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<%--Set the bean that is to be used on the JSP --%>
<s:set var="newTransfer" value="#session.newTransfer" />


<ffi:help id="payments_accounttransfernew" />
<span id="PageHeading" style="display: none;"><s:text
		name="jsp.transfers_17" /></span>
<span class="shortcutPathClass" style="display: none;">pmtTran_transfer,transfersLink,addSingleTransferLink</span>
<span class="shortcutEntitlementClass" style="display: none;">Transfers</span>



<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<%-- Included JS scripts --%>
<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />
<s:include value="/pages/jsp/transfers/inc/accounttransferform-js.jsp" />
<style type="text/css">
#TransferNew .errorLabel{display:inline-block;}
</style>
<div class="paneWrapper" role="form" aria-labelledby="transferDetail">
<div class="paneInnerWrapper">
<div class="header"><h2 id="transferDetail"><s:text name="jsp.transfers_113" /></h2><s:text name="jsp.transfer.details.label" /></div>
<%-- New Transfer Form started  --%>
<s:form id="TransferNew" namespace="/pages/jsp/transfers" validate="false" action="AddTransferAction_verify" method="post" name="TransferNew" theme="simple">
<div class="paneContentWrapper">	
	
	<%-- Hidden form fields --%>
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<input type="Hidden" name="transferDestination" id="TransferDest"
		value="<s:property value='%{#newTransfer.transferDestination}'/>">

	<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">
		
		<%-- Display Header + Template Name (if the transfer is loaded from template)  --%>
			<tr>
				<td colspan="3" align="center"><ul id="formerrors"></ul></td>
			</tr>
		
		<%--From Account ID Section --%>
		
			<tr>
				<td width="49%" id="addTransfer_fromAccountLabelId" class="sectionsubhead ltrow2_color"><label for="FromAccount"><s:text name="jsp.default_217" /></label><span class="required" title="required">*</span></td>
				<td width="1%">&nbsp;</td>
				<td width="50%" id="addTransfer_toAccountLabelId"  class="sectionsubhead ltrow2_color"><label for="ToAccount"><s:text name="jsp.default_424" /></label><span class="required" title="required">*</span></td>
			</tr>
		
			<tr>
				<td class="columndata ltrow2_color">
					<ffi:setProperty name="TransferAccounts"
							property="Filter" value="All" /> <select class="txtbox"
						name="fromAccountID" id="FromAccount" size="1"
						onchange="onChangeCheck('from')"
						aria-labelledby="FromAccount"
					aria-required="true">
							<s:if test="%{#newTransfer.fromAccountID!=null}">
		
								<option
									value="<s:property value='%{#newTransfer.fromAccountID}' />:<s:property value="#newTransfer.fromAccount.currencyCode" />"
									selected><s:property value="#newTransfer.fromAccount.accountDisplayText" />
								</option>
							</s:if>
					</select>
			</td>
			<td>&nbsp;</td>
			<%--TO Account ID Section --%>
			<td class="columndata ltrow2_color" align="left"><ffi:setProperty
					name="TransferAccounts" property="Filter" value="All" /> <select
				class="txtbox" name="toAccountID" id="ToAccount" size="1"
				onchange="onChangeCheck('to')" aria-labelledby="ToAccount"
					aria-required="true" >
					<s:if test="%{#newTransfer.toAccountID!=null}">
						<option
							value="<s:property value='%{#newTransfer.toAccountID}' />:<s:property value="#newTransfer.toAccount.currencyCode" />"
							selected><s:property value="#newTransfer.toAccount.accountDisplayText" />
						</option>
					</s:if>
			</select></td>
			
		</tr>
		<tr>
			<td><span id="fromAccountIDError"></span></td>
			<td></td>
			<td><span id="toAccountIDError"></span></td>
		</tr>
		<%--Input Amount for Transfer --%>
	<tr>
			<td id="addTransfer_amountLabelId" class="sectionsubhead ltrow2_color">
			<span style="display:inline-block; width:120px;"><label for="Amount1"><s:text name="jsp.default_43" /></label><span class="required" title="required">*</span></span>
				<span id="CurrencySectionLabel" style="display: none"><label for="Currency"><span id="addTransfer_CurrencyLabel" class="sectionsubhead">
					<s:text	name="jsp.default_125" /></label></span> <span class="required"></span>&nbsp;
			</td>
			<td>&nbsp;</td>
			<td id="addTransfer_dateLabelId" class="sectionsubhead ltrow2_color"><label for="AccountTransferFormDateId"><s:text name="jsp.default_137" /></label><span class="required" title="required">*</span></td>
		</tr> 
      		<tr>
			<td class="columndata ltrow2_color" align="left"><s:textfield
					name="amount" id="Amount1" size="12" maxlength="15"
					value="%{#newTransfer.userAssignedAmount}"
					cssClass="txtbox ui-widget-content ui-corner-all" aria-labelledby="Amount1" aria-required="true" /> <span
				id="CurrencyLabel" style="display: none">&nbsp;</span> <span
				id="CurrencySection" style="display: none" aria-labelledby="Currency">
				<select
					class="ui-widget-content ui-corner-all"
					name="userAssignedAmountFlagName" id="CurrencySelect">
						<option value="single" selected><s:text
								name="jsp.transfers_110" /></option>
				</select> <input type="hidden" id="OtherAmount" value="0.00">
			</span></td>
			<td>&nbsp;</td>
			<td class="columndata ltrow2_color" align="left">
				<sj:datepicker
				value="%{#newTransfer.date}" id="AccountTransferFormDateId"
				name="date" label="%{getText('jsp.default_137')}" maxlength="10" aria-labelledby="AccountTransferFormDateId" aria-required="true"
				buttonImageOnly="true" 
				cssClass="txtbox ui-widget-content ui-corner-all" /> <script>
				ns.common
						.enableAjaxDatepicker(
								"AccountTransferFormDateId",
								'<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=1&calDisplay=select&calForm=TransferNew&calTarget=newTransfer.Date"/>');
				 $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
				</script>
			
			
			</td>
			<%--Select Date for Transfer --%>
		</tr>
		<tr>
			<td>
				<span id="amountError" style="width:160px;"></span>
				<span id="toAmountError" style="width:160px;"></span>
				<span id="userAssignedAmountError"></span></td>
			<td></td>
			<td><span id="dateError"></span></td>
		</tr>
		<%-- Input for Memo for Transfer --%>
		<tr>
			<td id="addTransfer_memoLabelId" class="sectionsubhead ltrow2_color"><label for="memo"><s:text name="jsp.default_279" /></label></td>
			<td>&nbsp;</td>
			<td id="addTransfer_recuringLabelId" class="isRecTemplate sectionsubhead ltrow2_color"><s:text name="jsp.transfers_65.1" /></td>
		</tr>
		
			 <tr>
                  <td colspan="3" align="center"><ul id="formerrors"></ul></td>
             </tr>
		
		<tr>
			<td class="columndata ltrow2_color" align="left"><s:textfield
			name="memo" size="35" maxlength="100"
			cssClass="txtbox ui-widget-content ui-corner-all"
			value="%{#newTransfer.memo}" aria-labelledby="addTransfer_memoLabelId"/></td>
			<td>
			<%-- Selection  for Frequency for Transfer --%>			
			<input value="<s:property value="%{#newTransfer.transferType}"/>" id="transferTypeHidden" type="hidden"/>	
			</td>		
			<td class="isRecTemplate columndata ltrow2_color" align="left" style="width:430px">
			<s:if test='%{#newTransfer.transferType=="RECTEMPLATE"}'>
				<s:select
					id="single_transfer_frequency"
					cssClass="ui-widget-content ui-corner-all"
					onchange="modifyFrequency();"
					headerValue="%{getText('jsp.default_296')}" 		
					list="frequencyList" name="frequency"
					value="%{#newTransfer.frequency}" />  <script>
					$('#single_transfer_frequency').selectmenu({
						width : 110
					});
				</script>
				</s:if>
				<s:else>
					<s:select
					id="single_transfer_frequency"
					cssClass="ui-widget-content ui-corner-all"
					onchange="modifyFrequency();"
					headerValue="%{getText('jsp.default_296')}" 
					headerKey="None"
					list="frequencyList" name="frequency"
					value="%{#newTransfer.frequency}" />  <script>
					$('#single_transfer_frequency').selectmenu({
						width : 110
					});
				</script>
				</s:else>
				<input
				id="unlimitedTransfers" onclick="toggleRecurring();" type="radio"
				align="middle" name="openEnded" value="true" border="0"> <s:text
					name="jsp.default_446" /> <input id="noOfTransfers"
				onclick="toggleRecurring();" type="radio" align="middle"
				name="openEnded" value="false" border="0" checked> <s:text
					name="jsp.transfers_1" /> &nbsp; <input id="numberOfTransfersValue"
				type="text" name="numberTransfers"
				value="<s:property value='#newTransfer.numberTransfers'/>" maxlength="3" size="3" class="txtbox ui-widget-content ui-corner-all">
				</td>
				
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><span id="frequencyValueError"></span><span id="numberTransfersError"></span></td>
		</tr>
		<%-- Input and Selection for Transfer Count for Transfer --%>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td id="addTransfer_requiredFieldLabelId" colspan="3" align="center">
			<span class="required">* <s:text name="jsp.default_240" /></span>
			</td>
		</tr>
		<%-- New Transfer Form Buttons --%>
		<tr class="ltrow_color" height="8">
			<td class="columndata ltrow2_color" colspan="3" align="center">
				<sj:a id="cancelFormButtonOnInput" button="true" summaryDivId="summary" buttonType="cancel"
					onClickTopics="showSummary,cancelForm">
					<s:text name="jsp.default_82" />
				</sj:a>
				<sj:a id="verifyTransferSubmit2" formIds="TransferNew"
					targets="verifyDiv" button="true" validate="true"
					validateFunction="customValidation"
					onBeforeTopics="beforeVerifyTransferForm"
					onCompleteTopics="verifyTransferFormComplete"
					onErrorTopics="errorVerifyTransferForm"
					onSuccessTopics="successVerifyTransferForm"
					onClickTopics="beforeVerifyTransferForm">
					<s:text name="jsp.default_395" />
				</sj:a>
			</td>
		</tr>
	</table></div>
</s:form>
</div></div>
<script>
					
$(document).ready(function(){

	if($('#transferTypeHidden').val()=='RECTEMPLATE' || "Repetitive"==$('#transferTypeHidden').val() ){
		// do nothing just for condition							
	}else{
		// if not repetitive or rec template then hide recurring related information
		$('.isRecTemplate').hide();
	}
});	
</script>
      
