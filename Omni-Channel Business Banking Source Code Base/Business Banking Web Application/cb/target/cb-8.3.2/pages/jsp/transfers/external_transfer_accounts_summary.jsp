<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<ffi:help id="payments_external_accounts"/>	
<ffi:author page="manage-external.jsp"/>
	
	<% session.setAttribute("FFIModifyExtTransferAccounts", session.getAttribute("ModifyExtTransferAccounts")); %>
	
	<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
	<ffi:process name="BankIdentifierDisplayTextTask"/>
	<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
	<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>
	
	<ffi:setGridURL grid="GRID_externalAccounts" name="VerifyURL" url="/cb/pages/jsp/transfers/external_transfer_account_verify_selected.jsp?accountID={0}" parm0="BpwID"/>
	<ffi:setGridURL grid="GRID_externalAccounts" name="ViewURL" url="/cb/pages/jsp/transfers/viewExternalTransferAccounts.action?accountID={0}" parm0="BpwID"/>
	<ffi:setGridURL grid="GRID_externalAccounts" name="DeleteURL" url="/cb/pages/jsp/transfers/deleteExtTransferAccount_init.action?accountID={0}" parm0="BpwID"/>
	
	<ffi:setProperty name="tempGridURL" value="/pages/jsp/transfers/getExtTransferAccounts.action?GridURLs=GRID_externalAccounts" URLEncrypt="true"/>
	<s:url id="externalTransferAccountsURL" value="%{#session.tempGridURL}" escapeAmp="false"/>
	<div id="externalAccountsSummary">
		<s:form id="externalAccountsSummaryFormID" name="externalAccountsSummaryForm" namespace="/pages/jsp/transfers" method="post" theme="simple">
	    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<sjg:grid id="externalTransferAccountsGrid"
			dataType="json"
			href="%{externalTransferAccountsURL}"
			sortable="false"
			pager="false"
			gridModel="gridModel"
			rownumbers="false"
			shrinkToFit="true"
			scroll="false"
			scrollrows="true"
			onGridCompleteTopics="makeRowsEditable">
			<sjg:gridColumn name="status" index="status"
				title="%{getText('jsp.default_28')}" width="15" align="left"
				sortable="false" edittype="checkbox" editable="true" value="%{status}" editoptions="{value:'Active:Inactive'}"/>
			<sjg:gridColumn name="bpwID" index="bpwID" title="" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="numberMasked" index="NumberMasked" 
				title="%{getText('jsp.default_19')}" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.routingNumBankName" index="routingNumBankName" width="100" align="left" sortable="false" 
				title="%{getText('jsp.default_365')+' / '+getText('jsp.default_63')}"/>
			<sjg:gridColumn name="consumerMenuDisplayText" index="consumerMenuDisplayText"
				title="%{getText('jsp.default_16')}" sortable="false" width="80" align="left"/>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>" >
				<sjg:gridColumn name="nickname" index="nickname" title="%{getText('jsp.default_292')}"
					width="85" align="left" sortable="false" edittype="text" editable="true" editoptions="{size:40, maxlength: 40}"/>
			</ffi:cinclude>
			<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>" >
				<sjg:gridColumn name="nickname" index="nickname" width="85" align="left" sortable="false"
					title="%{getText('jsp.default_292')}"/>
			</ffi:cinclude>
			<sjg:gridColumn name="type" index="type" width="40" align="left" sortable="false"
				title="%{getText('jsp.default_20')}" />
			<sjg:gridColumn name="verifyStatusValue" index="verifyStatusValue" title="" sortable="false" hidden="true" hidedlg="true"/>

 			<sjg:gridColumn name="map.AccountVerifiedStatus" index="AccountVerifiedStatus" width="30" align="left" sortable="false"
 				title="%{getText('jsp.default_388')}" />
			<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" width="40" align="right" sortable="false" search="false" formatter="ns.transfer.formatExternalTransferActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
			<sjg:gridColumn name="map.canVerify" index="canVerify" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.canEdit" index="canEdit" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.canDelete" index="canDelete" title="" hidden="true" hidedlg="true"/>
		</sjg:grid>
		<br>
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>" >
			<div align="center" class="marginBottom10">
				<sj:a id="resetExternalAccountsBtn" button="true" onclick="ns.transfer.resetExternalAccounts();"><s:text name="jsp.default_358"/></sj:a>
				&nbsp;&nbsp;&nbsp;
				<sj:a id="saveExternalAccountsBtn" button="true" onclick="ns.transfer.saveExternalAccounts();"><s:text name="jsp.default_366"/></sj:a>
			</div>
		</ffi:cinclude>
		</s:form>
	</div>
	<ffi:removeProperty name="tempGridURL"/>

	<script type="text/javascript">
		// add Grid control id to maintain it on the page
		ns.common.addGridControls("#externalTransferAccountsGrid", false);
	</script>