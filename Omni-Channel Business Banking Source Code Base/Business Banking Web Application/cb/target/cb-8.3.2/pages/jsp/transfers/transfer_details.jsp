<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<div id="detailsPortlet" class="portlet wizardSupportCls ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
     <div class="portlet-header ui-widget-header ui-corner-all">
        	  <h1 class="portlet-title"><s:text name="jsp.transfers_17"/></h1>
		</div>
        <div class="portlet-content">

		    <sj:tabbedpanel id="TransactionWizardTabs" >
		        <sj:tab id="inputTab" target="inputDiv" label="%{getText('jsp.enter.details.label')}"/>
		        <sj:tab id="verifyTab" target="verifyDiv" label="%{getText('jsp.verify.details.label')}"/>
		        <sj:tab id="confirmTab" target="confirmDiv" label="%{getText('jsp.confirm.details.label')}"/>
		       	<s:if test="%{#session.SecureUser.AppType=='Business'}">
			        <span id="transferFileImportHolder" class="hidden">
				        <a href="javascript:void(0)" class="dashboardTabsAnchor" id="transferFileImport" onclick="$('#transferFileImportLink').trigger('click');">
				   			<span style="padding-right:7px;"></span><s:text name="jsp.default_539.1" />
				   		</a>
			        </span>
		        </s:if>
		        <div id="inputDiv">
					<s:text name="jsp.transfers_42"/>
		            <br><br>
		        </div>
		        <div id="verifyDiv">
		            <s:text name="jsp.transfers_87"/>
		            <br><br>
		        </div>
		        <div id="confirmDiv">
		            <s:text name="jsp.transfers_33"/>
		            <br><br>
		        </div>
		    </sj:tabbedpanel>
		</div>
		<div id="transferDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
       		  <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
</div>
    </div>



<script>    
	//Initialize portlet with settings icon
	ns.common.initializePortlet("detailsPortlet"); 
	 
	ns.transfer.detailsClose =  function(){
			$('#details').slideUp();
			$('#transferGridTabs').portlet('expand');
			$('#transferTemplateGridTabs').portlet('expand');
	};
</script>