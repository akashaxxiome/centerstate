<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="request"/>
    <ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="GetTransferAccounts" property="Reload" value="true"/>
<ffi:process name="GetTransferAccounts"/>

<script language="JavaScript" type="text/javascript">
    function setLocation()
    {
       location.replace("<ffi:urlEncrypt url='accounttransfernew.jsp'/>");
	}
    window.onload=setLocation;
</script>