<%@ page import="com.ffusion.tasks.banking.AddTransfer"%>
<%@ page import="com.ffusion.tasks.banking.EditTransfer"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<style type="text/css">
#formerrors{margin:0}
</style>
<script type="text/javascript">
<!--
	//This function is used to check event of enter key.
	$("#templateNameKey").keypress(function(e) {  
		if (e.which == 13) {  
			$("#saveSingleTransferTemplateSubmit").click();
			return false;
		}   
	});	
//-->
</script>
<ffi:help id="payments_accounttransfertempleSaveAs" />
		<div>
				<s:form namespace="/pages/jsp/transfers" validate="false" action="%{'AddTransferTemplateAction_saveAsTemplate'}" method="post" id="NewSingleTransferTemplate" name="NewSingleTransferTemplate" theme="simple">

                	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                	<div>
                		<input type="hidden" name="template" value="true">
						<s:if test="flowType=='EditTransfer'">
							<input type="hidden" name="flowType" value="EditTransfer"/>
						</s:if>
						<input id="templateNameKey" name="templateName" placeholder="<s:text name='jsp.template.name.label' />" size="26" maxlength="32" class="ui-widget-content ui-corner-all" style="display:inline-block; margin:0 10px 10px 0; width:60%;vertical-align: top;"/>
						<s:if test="fromPortal=='true'">
	                              
                              <sj:a id="saveSingleTransferTemplateSubmit" formIds="NewSingleTransferTemplate"
								targets="resultmessage" button="true"
								validate="true" 
		                    	validateFunction="customValidation"
								onBeforeTopics="beforeSaveSingleTransferTemplatePortal"
								onSuccessTopics="saveSingleTransferTemplateSuccessPortal"
								onErrorTopics="errorSaveSingleTransferTemplatePortal">
								
								<s:text name="jsp.default_366" />
							</sj:a>
	                       	</s:if>
	                       	<s:else>
	                              
	                              <sj:a id="saveSingleTransferTemplateSubmit" formIds="NewSingleTransferTemplate"
									targets="resultmessage" button="true"
									onBeforeTopics="beforeSaveSingleTransferTemplate"
									onSuccessTopics="saveSingleTransferTemplateSuccess"
									onErrorTopics="errorSaveSingleTransferTemplate"
									onCompleteTopics="saveSingleTransferTemplateComplete"
									validate="true" 
		                    		validateFunction="customValidation">
									<%-- <span class="sapUiIconCls icon-save"></span> --%>
									<s:text name="jsp.default_366" />
								  </sj:a>
							
	                       	</s:else> 
                       </div>
						<div><span id="templateNameError"></span></div>
			</s:form>
		</div>
