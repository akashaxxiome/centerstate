<%@ page import="com.ffusion.beans.banking.TransferDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<%
	session.setAttribute("templateEdit", request.getParameter("templateEdit"));
	String fromPortalPage = request.getParameter("fromPortalPage");
	session.setAttribute("fromPortalPage",fromPortalPage);

%>

<s:set var="transfer" value="#request.transfer" />

 <ffi:help id="payments_accounttransfertempdelete" />
<div class="approvalDialogHt2">
<s:form action="/pages/jsp/transfers/deleteTransferTemplateAction.action?collectionName=%{#attr.transfersCollectionName}" method="post" name="TransferDelete" id="deleteTransfeFormId" >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="transferID" value="<s:property value='#transfer.ID'/>"/>
<input type="hidden" name="transferType" value="<s:property value='#transfer.transferType'/>"/>

<div class="confirmationDetails" style="margin:10px 0 0;">
	<span id="deleteTransfer_templateNameId" class="sectionLabel"><s:text name="jsp.default_416" />:</span>
	<span id="deleteTransfer_templateValueId"><s:property value="%{#transfer.templateName}"/></span>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /> :</div>
<div class="blockContent">
		
		<s:if test="%{#transfer.StatusName!='Scheduled'}">
				<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
				<s:set var="TRS_INSUFFICIENT_FUNDS"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
				<s:set var="TRS_BPW_LIMITCHECK_FAILED"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
				<s:set var="TRS_FAILED_APPROVAL"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_APPROVAL==#transfer.status}" />
				
				<s:if test="%{#TRS_FAILED_TO_TRANSFER}">
						<s:set var="HighlightStatus" value="true" />
				</s:if>
				<s:elseif test="%{#TRS_INSUFFICIENT_FUNDS}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				<s:elseif test="%{#TRS_BPW_LIMITCHECK_FAILED}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				<s:elseif test="%{#TRS_FAILED_APPROVAL}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				
				<s:set var="TRS_SCHEDULED"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
					
				<s:set var="TRS_PENDING_APPROVAL"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
						
				<s:if test="%{#HighlightStatus=='true'}">
					<div class="blockRow failed">
						<span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-decline"></span>
							<s:property	value="#transfer.statusName" />
						</span>
					</div>
				</s:if> 
				<s:else>
					<s:if test="%{#TRS_PENDING_APPROVAL}">
						<div class="blockRow pending"><span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-pending"></span>
							<s:property value="%{#transfer.statusName}" />
						</span></div>
					</s:if>
					<s:else>
					<div class="blockRow completed"><span id="viewTransfer_statusValueId">
						<span class="sapUiIconCls icon-accept"></span>
						<s:property value="%{#transfer.statusName}" />
					</span></div>
					</s:else>
					<%-- <s:elseif test="%{!#TRS_SCHEDULED}">
						<s:property value="%{#transfer.statusName}" />
					</s:elseif> --%>
				</s:else>
		</s:if>
		<s:if test="%{#transfer.StatusName=='Scheduled'}">
			<div class="blockRow pending"><span id="viewTransfer_statusValueId">
				<span class="sapUiIconCls icon-future"></span>
				<s:property value="%{#transfer.statusName}" />
				</span></div>
		</s:if>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
<s:set  var="DisplayEstimatedAmountKey" value="%{'false'}"/>
<div class="blockContent">
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="deleteTransfer_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217"/>: </span>
            <span id="deleteTransfer_fromAccountValueId">
   			  	<s:property  value="#transfer.fromAccount.accountDisplayText"/>           
            </span>
		</div>
		<div class="inlineBlock">
			<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
				<span id="deleteTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43"/>: </span>
				<span id="deleteTransfer_amountValueId">
                    <s:property value="%{#transfer.amountValue.currencyStringNoSymbol}"/>
                    <s:property value="%{#transfer.AmountValue.currencyCode}"/>
                </span>
            </s:if>
            <s:if test="%{#transfer.userAssignedAmountFlagName!='single'}">
                  <span id="deleteTransfer_fromAmountLabelId" class="sectionLabel"><s:text name="jsp.default_489"/>: </span>
                  <span id="deleteTransfer_fromAmountValueId">
	                  <s:if test="%{#transfer.isAmountEstimated=='true'}">
	                          &#8776;<s:set var="DisplayEstimatedAmountKey" value="%{'true'}"/>
	                  </s:if>
	                  <s:property value="%{#transfer.amountValue.currencyStringNoSymbol}"/>
	                  <s:property value="%{#transfer.amountValue.currencyCode}"/>
                  </span>
            </s:if>
		</div>
	</div>
	
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
	         <span id="deleteTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424"/>: </span>
	         <span id="deleteTransfer_toAccountValueId">
 				<s:property  value="#transfer.toAccount.accountDisplayText"/>           
	 		</span>
		</div>
		<div class="inlineBlock">
			<s:if test="%{#transfer.userAssignedAmountFlagName!='single'}">
                  <span id="deleteTransfer_toAmountLabelId" class="sectionLabel"><s:text name="jsp.default_512"/>: </span>
                  <span id="deleteTransfer_toAmountValueId">
                     <s:if test="%{#transfer.isAmountEstimated=='true'}">
                          &#8776;<s:set var="DisplayEstimatedAmountKey" value="%{'true'}"/>
                      </s:if>
                      <s:property value="%{#transfer.toAmountValue.currencyStringNoSymbol}"/>
                      <s:property value="%{#transfer.toAmountValue.currencyCode}"/>
                  </span>
			</s:if>
		</div>
	</div>
	<div class="blockRow">
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
           <span id="deleteTransfer_TypeLabelId" class="sectionLabel"><s:text name="jsp.default_444"/>: </span>
           <span id="deleteTransfer_TypeValueId">
              <s:set var="transferDestination" value="%{#transfer.transferDestination}" /> 
                 <s:set var="TRANS_BOOK" value="@com.ffusion.beans.banking.TransferDefines@TRANSFER_BOOK" />
                 <s:set var="TRANS_ITOI" value="@com.ffusion.beans.banking.TransferDefines@TRANSFER_ITOI"/>
                 <s:set var="TRANS_ITOE" value="@com.ffusion.beans.banking.TransferDefines@TRANSFER_ITOE"/>
                 <s:set var="TRANS_ETOI" value="@com.ffusion.beans.banking.TransferDefines@TRANSFER_ETOI"/>
                 <s:if test="%{#transferDestination==#TRANS_BOOK}">
					<s:text name="jsp.transfers_53" />
				</s:if>
				<s:elseif test="%{#transferDestination==#TRANS_ITOI}">
					<s:text name="jsp.transfers_53" />
				</s:elseif>
				<s:elseif test="%{#transferDestination==#TRANS_ITOE}">
					<s:text name="jsp.transfers_52" />
				</s:elseif>
				<s:elseif test="%{#transferDestination==#TRANS_ETOI}">
					<s:text name="jsp.transfers_44" />
				</s:elseif>
         </span>
      </ffi:cinclude>
	</div>
	<s:if test="%{#request.isRecurring=='true'}">
		<div class="blockRow">
            <span id="deleteTransfer_frequencyLabelId" class="sectionLabel"><s:text name="jsp.default_351"/>: </span>
            <span id="deleteTransfer_frequencyValueId"><ffi:getProperty name="Transfer" property="Frequency"/></span>
        </div>
        <div class="blockRow">
            <span id="deleteTransfer_noOfTransfersLabelId" class="sectionLabel"><s:text name="jsp.default_443"/>: </span>
            <span id="deleteTransfer_noOfTransfersValueId">
	            <s:if test="%{#transfer.numberTransfers==999}">	<s:text name="jsp.default_446"/></s:if><s:else>
	            <s:property value="#transfer.numberTransfers"/>
	            </s:else>	                           
			</span>
		</div>
	                           </s:if>
	<div class="blockRow">
		<span id="deleteTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279"/></span>
		<span id="deleteTransfer_memoValueId"><s:property value="#transfer.memo"/></span>
	</div>
</div>
<s:if test="%{#DisplayEstimatedAmountKey=='true'">
	<div class="mltiCurrenceyMessage">
	     <span id="deleteTransfer_estimatedAmountLabelId" class="required">&#8776; <s:text name="jsp.default_241"/></span>
	</div>
</s:if>
</s:form>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
		<sj:a id="cancelDeleteTransferTemplateLink" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
			<s:text name="jsp.default_82" />
		</sj:a>
		<sj:a id="deleteTransferTemplateLink" formIds="deleteTransfeFormId" targets="resultmessage" button="true"   
			  title="%{getText('Delete_Template')}" onCompleteTopics="cancelSingleTransferTemplateComplete" onSuccessTopics="cancelSingleTransferTemplateSuccessTopics" onErrorTopics="errorDeleteTransfer" 
			  effectDuration="1500" ><s:text name="jsp.default_162" /></sj:a>
</div>
</div>
<ffi:removeProperty name="transfersCollectionName"/>
