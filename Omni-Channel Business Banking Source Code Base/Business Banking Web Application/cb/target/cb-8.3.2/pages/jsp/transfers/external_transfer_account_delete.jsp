<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.exttransfers.ExtTransferAccountDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
 <%@ page contentType="text/html; charset=UTF-8" %>
 
 <ffi:help id="payments_externalaccounts" />

<s:set var="extTransferAccount" value="#session.extTransferAccount" />
<s:set name="strutsActionName" value="%{'DeleteExtTransferAccount'}" />

<span id="PageHeading" style="display:none;"><s:text name="jsp.transfers_delete_external_transfer_account" /></span>
	<%
	if (request.getParameter("accountID") != null) {
		String accountID = request.getParameter("accountID");
		session.setAttribute("accountID", accountID);
	%>
	
	<s:set var="accountId"><%=accountID%></s:set>
	<ffi:removeProperty name="accountID"/>
	<% } %>

<s:if test="%{#request.baSBackendEnabledFlag == 'TRUE'}">
	<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
		<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
	<ffi:process name="CountryResource" />
</s:if>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:setProperty name="BackURL" value="${SecurePath}transfers/manage-external-delete.jsp"/>

<% session.setAttribute("FFIDeleteExtTransferAccount", session.getAttribute("DeleteExtTransferAccount")); %>


<s:form id="formDeleteExternalTransferAccount" namespace="/pages/jsp/transfers" validate="false" action="deleteExtTransferAccount" method="post" name="formDeleteExternalTransferAccount" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="accountID" value="<s:property value='accountId'/>"/>
<div class="blockWrapper label140">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock " style="width: 50%">
				<s:set var="acctBankIDTypeText"><%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%> </s:set>
				<s:set var="acctBankIDType" value="extTransferAccount.acctBankIDType" />
				<s:if test="%{#acctBankIDType != #acctBankIDTypeText}">
					<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_63" />/<s:property value="bankIdentifierDisplayText"/>:
					</span>
				</s:if>
					
				<s:if test="%{#acctBankIDType == #acctBankIDTypeText}">
					<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_63" />/<s:text name="jsp.exttransferaccount_swift_bic" />:
					</span>
				</s:if>
				
				<span class="sectionsubhead valueCls"><s:property value="extTransferAccount.bankName"/> / <s:property value="extTransferAccount.routingNumber"/> </span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_16" />:
				</span>
				<span class="sectionsubhead valueCls"><s:property value="extTransferAccount.consumerMenuDisplayText"/>
					&nbsp;<s:property value="extTransferAccount.currencyCode"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_294" />:
				</span>
				<span class="sectionsubhead valueCls"> <s:property value="extTransferAccount.nickname"/> </span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_20" />:
				</span>
				<span class="sectionsubhead valueCls"> <s:property value="extTransferAccount.type"/> </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.exttransferaccount_ownership" />:
				</span>
				
				<s:set var="verifyStatusText"><%=ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED%> </s:set>
				<s:set var="verifyStatus" value="extTransferAccount.verifyStatusString" />
				<s:if test="%{#verifyStatus != #verifyStatusText}">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.exttransferaccount_this_is_my_own_account" /></span>
				</s:if>
				
				<s:if test="%{#verifyStatus == #verifyStatusText}">
					<span class="sectionsubhead valueCls"><s:text name="jsp.exttransferaccount_this_account_belongs_to_someone_else" /></span>
				</s:if>
			</div>
			<s:if test="%{#request.baSBackendEnabledFlag == 'TRUE'}">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.user_97"/>
					</span>
					<span class="sectionsubhead valueCls">
						<s:set var="countryResourceId">Country<s:property value="extTransferAccount.countryCode"/></s:set>
						<ffi:setProperty name="CountryResource" property="ResourceID" value="${countryResourceId}" />
						<ffi:getProperty name='CountryResource' property='Resource'/>
					</span>
				</div>
			</s:if>
		</div>
	</div>
</div>
				
				<div class="marginTop30">&nbsp;</div>
				<div class="ui-widget-header customDialogFooter">
					<sj:a id="cancelDeleteExternalTransferAccountLink" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
						<s:text name="jsp.default_82" />
					</sj:a>
					<sj:a id="deleteExternalTransferAccountLink" formIds="formDeleteExternalTransferAccount" targets="resultmessage" button="true"   
						  title="%{getText('jsp.default_163')}" onCompleteTopics="deleteExtTransferAccountFormComplete" onSuccessTopics="successDeleteExtTransferAccountForm" onErrorTopics="errorDeleteExtTransferAccountForm" 
						  effectDuration="1500" ><s:text name="jsp.default_162" /></sj:a>
					
				</div>
</s:form>
<%-- ================= MAIN CONTENT END ================= --%>

<ffi:removeProperty name="CountryResource" />
