<%@ page import="com.ffusion.beans.banking.TransferBatch"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>





<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">


    <div align="center">
	<div align="center">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tr>
              
                <td class="ltrow2_color">
                    <div align="left">
       				<s:form id="TemplateComment" namespace="/pages/jsp/transfers" validate="false" action="viewTemplateHistoryAction_saveComment" method="post" name="TemplateComment" theme="simple">
                    	
						<input type="hidden" name="index" value="<s:property value='index'/>"/>
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    <table border="0" cellspacing="0" cellpadding="3" width="100%">
                        <tr>
                            <td class="sectionhead ltrow2_color tbrd_b" colspan="2"><s:text name="jsp.default_414"/></td>
                            <td class="ltrow2_color tbrd_b" align="right">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" height="15"></td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead ltrow2_color" align="right"><s:text name="jsp.default_130"/></td>
                            <td class="columndata ltrow2_color"><s:property value="status"/></td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" style="padding-top:5px"><span class="sectionsubhead"><s:text name="jsp.default_33"/></span></td>
                            <td class="columndata">
                                <textarea  class="txtbox" name="templateComment" rows="2" cols="40"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="columndata ltrow2_color" colspan="3">
                                <div align="left">
                                    <s:include value="/pages/jsp/transfers/inc/include-view-transaction-history.jsp"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                
                          
                                    <sj:a id="cancelFormButton" 
						                button="true" 
										onClickTopics="closeDialog"
									><s:text name="jsp.default_82"/></sj:a>
									<sj:a	
										id="saveCommentSubmit"
										formIds="TemplateComment"
		                                targets="resultmessage" 
		                                button="true" 
					                    onBeforeTopics="beforeSaveTemplateComment"
					                    onCompleteTopics="saveTemplateCommentComplete"
										onErrorTopics="errorSaveTemplateComment"
					                    onSuccessTopics="successSaveTemplateComment"
				                        ><s:text name="jsp.transfers_68"/></sj:a>
                            </td>
                        </tr>
                    </table>
                    </s:form>
                    </div>
                </td>
		    </tr>

	    </table>
	</div>
    </div>

