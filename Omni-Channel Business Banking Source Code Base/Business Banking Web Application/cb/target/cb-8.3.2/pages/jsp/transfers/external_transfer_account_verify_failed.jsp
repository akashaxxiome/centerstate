<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import="com.ffusion.beans.exttransfers.ExtTransferAccount,
				 com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<span id="PageHeading" style="display:none;">Deposit Verification Failed</span>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:setProperty name="BackURL" value="${SecurePath}transfers/manage-external-verify-failed.jsp"/>

<s:form id="formExtTransferAccountVerifyFailed" name="formExtTransferAccountVerifyFailed" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<div align="center">
			<s:text name="jsp.exttransferaccount_verification_failed" />
			<s:text name="jsp.exttransferaccount_add_to_blocked_account_list" />
	</div>
	<div class="leftPaneWrapper">
<div class="leftPaneInnerWrapper">
		<div class="header">External Account Transfer Summary</div>
		<div id="" class="paneContentWrapper summaryBlock">
			<div>
				<span class="sectionsubhead sectionLabel floatleft inlineSection">
					<s:text name="jsp.default_16" />:</span>
				<span class="inlineSection floatleft labelValue"> <ffi:getProperty name="ExternalTransferACCOUNT" property="ConsumerMenuDisplayText"/>
					
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">
					<s:text name="jsp.exttransferaccount_primary_account_holder"/>:</span>
				<span class="inlineSection floatleft labelValue"> <ffi:getProperty name="ExternalTransferACCOUNT" property="PrimaryAcctHolder"/>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="confirmPageDetails">
<div class="blockWrapper label150"><div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_63" />/
					  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
					:
				</span>
				<span><ffi:getProperty name="ExternalTransferACCOUNT" property="BankName"/> / <ffi:getProperty name="ExternalTransferACCOUNT" property="RoutingNumber"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_294" />:
				</span>
				<span> <ffi:getProperty name="ExternalTransferACCOUNT" property="Nickname"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel">
				<s:text name="jsp.default_20" />:
			</span>
			<span> <ffi:getProperty name="ExternalTransferACCOUNT" property="Type"/></span>
		</div></div></div>
	<div class="btn-row">
		<sj:a id="verifyFailedExtTransferAccountOKButton" button="true" onClickTopics="cancelExternalTransferAccountForm"><s:text name="jsp.default_303"/></sj:a>
	</div></div>
</s:form>
<%-- ================= MAIN CONTENT END ================= --%>
<ffi:removeProperty name="SetExtTransferAccount"/>
