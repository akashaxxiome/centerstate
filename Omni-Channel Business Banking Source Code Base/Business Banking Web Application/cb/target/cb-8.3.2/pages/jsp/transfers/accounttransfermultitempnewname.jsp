<%@ page import="com.ffusion.tasks.banking.AddTransferBatch"%>
<%@ page import="com.ffusion.tasks.banking.EditTransferBatch"%>
<%@ page import="java.util.Iterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>


<script type="text/javascript">
<!--
	//This function is used to check event of enter key.
	/* removed for cr 758630 as form gets submitted multiple times */
	/* $("#NewMultipleTransferTemplate").keypress(function(e) {  
		if (e.which == 13) {  
			$("#saveMultipleTransferTemplateSubmit").click();
			return false;
		}   
	});	 */
//-->
</script>

<s:form namespace="/pages/jsp/transfers" validate="false" action="AddTransferTemplateBatchAction_saveAsTemplate" method="post" id="NewMultipleTransferTemplate" name="NewMultipleTransferTemplate" theme="simple">
				
             <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" name="template" value="true">
	<div>
			<input type="text" name="templateName" id="templateName" value="" size="26" maxlength="32" class="ui-widget-content ui-corner-all" placeholder="<s:text name='jsp.default_290'/>" style="display:inline-block; margin:0 10px 10px 0; width:60%"/>
				<sj:a id="saveMultipleTransferTemplateSubmit" formIds="NewMultipleTransferTemplate"
					targets="resultmessage" button="true"
					validate="true" 
                    validateFunction="customValidation" 
					onBeforeTopics="beforeSaveSingleTransferTemplate"
					onSuccessTopics="saveSingleTransferTemplateSuccess"
					onErrorTopics="errorSaveSingleTransferTemplate"
					onCompleteTopics="saveSingleTransferTemplateComplete">
					<%-- <span class="sapUiIconCls icon-save"></span> --%>
					<s:text name="jsp.default_366" />
				 </sj:a>
	</div>
	<div><span id="templateNameError"></span></div>
</s:form>
