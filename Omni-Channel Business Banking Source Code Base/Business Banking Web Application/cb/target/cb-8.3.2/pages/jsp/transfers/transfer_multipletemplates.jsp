<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_accounttransferMultipleTemplates" />

	<ffi:setGridURL grid="GRID_transferMultipletemplates" name="ViewURL" url="/cb/pages/jsp/transfers/viewTransferTemplateBatchAction.action?transferBatchID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_transferMultipletemplates" name="LoadURL" url="/cb/pages/jsp/transfers/AddTransferBatchAction_initTemplateLoad.action?LoadFromTransferTemplate=MULTI_{0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_transferMultipletemplates" name="EditURL" url="/cb/pages/jsp/transfers/EditTransferTemplateBatchAction_init.action?transferBatchID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_transferMultipletemplates" name="DeleteURL" url="/cb/pages/jsp/transfers/deleteTransferTemplateBatchAction_init.action?transferBatchID={0}" parm0="ID"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/transfers/getMultipleTransferTemplates.action?GridURLs=GRID_transferMultipletemplates" URLEncrypt="true"/>
    <s:url id="multipleTransfersTemplatesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
			id="multipleTransfersTemplatesGridID"
			caption=""
			sortable="true"
			dataType="local"
			href="%{multipleTransfersTemplatesUrl}"
			pager="true"
			gridModel="gridModel"
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			rownumbers="true"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			sortname="TEMPLATENAME"
			sortorder="asc"
			onGridCompleteTopics="addGridControlsEvents,multileTemplatesGridCompleteEvents"
			>

			<sjg:gridColumn name="templateName" index="TEMPLATENAME" title="%{getText('jsp.default_283')}" sortable="true"/>
			<sjg:gridColumn name="type" index="TRANSFER_DESTINATION" title="%{getText('jsp.default_444')}" formatter="ns.transfer.customTypeColumnForMultipleTemplate" sortable="true" width="90"/>

			<sjg:gridColumn name="combinedAccount" index="Accounts" title="Accounts" sortable="false" width="120" formatter="ns.transfer.formatBatchCombinedAccount"/>

			<sjg:gridColumn name="amount" index="AMOUNT" title="%{getText('jsp.default_43')}" sortable="true" formatter="ns.transfer.formatMultipleTemplateAmountColumn" width="70"/>
			<sjg:gridColumn name="commonStatusName" index="STATUS" title="%{getText('jsp.default_388')}" sortable="true" formatter="ns.transfer.formatMultipleTemplateCommonStatusNameColumn" width="60"/>

			<sjg:gridColumn name="map.FrequencyValue" index="mapFrequencyValue" title="%{getText('jsp.default_215')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.transfer.formatMultipleTransferTemplatesActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

			<sjg:gridColumn name="trackingID" index="trackingID" title="%{getText('jsp.default_435')}" width="80" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonFromAccount.displayText" index="commonFromAccountDisplayText" title="%{getText('jsp.transfers_27')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonFromAccount.currencyCode" index="commonFromAccountCurrencyCode" title="%{getText('jsp.transfers_26')}" hidden="true" hidedlg="true"/>

        <sjg:gridColumn name="approverName" index="approverName" title="" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="userName" index="userName" title="" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="rejectReason" index="rejectReason" title="" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonToAccount.displayText" index="commonToAccountDisplayText" title="%{getText('jsp.transfers_30')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonToAccount.currencyCode" index="commonToAccountCurrencyCode" title="%{getText('jsp.transfers_29')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonDestination" index="commonDestination" title="%{getText('jsp.transfers_31')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="transferType" index="transferType" title="%{getText('jsp.transfers_86')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="AMOUNT" title="%{getText('jsp.default_489')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="amountValue.currencyCode" index="amountValueCurrencyCode" title="%{getText('jsp.transfers_47')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="toAmountValue.currencyStringNoSymbol" index="TOAMOUNT" title="%{getText('jsp.default_512')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canLoad" index="canLoad" title="%{getText('jsp.default_81')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canDelete" index="canDelete" title="%{getText('jsp.default_79')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canEdit" index="canEdit" title="%{getText('jsp.default_80')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="isAmountEstimated" index="isAmountEstimated" title="%{getText('jsp.transfers_54')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="isToAmountEstimated" index="isToAmountEstimated" title="%{getText('jsp.transfers_55')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonFromAccount.ID" index="commonFromAccount.ID" title="%{getText('jsp.transfers_25')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="commonToAccount.ID" index="commonToAccountID" title="%{getText('jsp.transfers_28')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="%{getText('jsp.default_359')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="displayTextRoA" index="displayTextRoA" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canLoadForMultipleTemplates" index="canLoadForMultipleTemplates" title="%{getText('jsp.default_81')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canEditForMultipleTemplates" index="canEditForMultipleTemplates" title="%{getText('jsp.default_80')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canDeleteForMultipleTemplates" index="canDeleteForMultipleTemplates" title="%{getText('jsp.default_79')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.transfer.formatApproversInfo" />
			<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.LoadURL" index="LoadURL" title="%{getText('jsp.default_265')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>

	</sjg:grid>

	<script type="text/javascript">
		$("#multipleTransfersTemplatesGridID").jqGrid('setColProp','ID',{title:false});
	</script>
