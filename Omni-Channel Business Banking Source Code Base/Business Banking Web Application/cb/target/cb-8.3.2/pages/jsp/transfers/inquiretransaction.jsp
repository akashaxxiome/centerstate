<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="payments_accounttransferInquiry" />


<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<s:set var="IsConsumer" value="false" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<s:set var="IsConsumer" value="true" />
</ffi:cinclude>

<s:set var="fromAccountDisplayText" value="transfer.fromAccount.AccountDisplayText"/>
<s:set var="toAccountDisplayText" value="transfer.toAccount.AccountDisplayText"/>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_219"/></span>
				<span  class="columndata lightBackground"><s:property value="%{#fromAccountDisplayText}" /></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_425"/></span>
				<span  class="columndata lightBackground"><s:property value="%{#toAccountDisplayText}" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>
				<span  class="columndata lightBackground"><s:property value="transfer.date" /></span>
			</div>
			 <s:if test="transfer.userAssignedAmountFlagName=='single'}">
                   <div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_45"/></span>
					<span  class="columndata lightBackground">
						<s:property value="transfer.amountValue.CurrencyStringNoSymbol" /> <s:property value="transfer.amountValue.CurrencyCode" />
                    </span>
				</div>
             </s:if>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_221"/></span>
				<span  class="columndata lightBackground">
                 	<s:if test="transfer.isToAmountEstimated=='true'">&#8776;</s:if>
                        <s:property value="transfer.amountValue.CurrencyStringNoSymbol" /> <s:property value="transfer.amountValue.CurrencyCode" />
                 </span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_427"/></span>
				<span  class="columndata lightBackground">
	                         <s:if test="transfer.isToAmountEstimated=='true'">&#8776;</s:if>
	                          <s:property value="transfer.toAmountValue.CurrencyStringNoSymbol" /> <s:property value="transfer.toAmountValue.CurrencyCode" />
                </span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_430"/></span>
			<span  class="columndata lightBackground"><s:text name="jsp.default_442"/></span>
		</div>
	</div>
</div>
<div align="center" class="sectionsubhead marginTop10"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></div>
<div class="marginTop10">
	<s:form id="inquiryForm" name="form4" theme="simple" method="post" action="/pages/jsp/transfers/sendTransferInquiryAction_sendTransferFundsInquiry.action" style="text-align: left;">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="transferID" value="<s:property value='transferID'/>"/>
		<input type="hidden" name="recTransferID" value="<s:property value='recTransferID'/>"/>
		<input type="hidden" name="transferType" value="<s:property value='transferType'/>"/>
		<div ><textarea class="txtbox ui-widget-content ui-corner-all" name="memo" id="memo" rows="10" cols="75" style="overflow-y: auto; text-align: left;" wrap="virtual" ></textarea></div>
	</s:form>
</div>						

<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
<sj:a id="closeInquireTransferLink" button="true" 
							  onClickTopics="closeDialog" 
							  title="%{getText('jsp.default_83')}"><s:text name="jsp.default_82"/></sj:a>
							  
						<sj:a id="cancelTransferSubmitButton" 
							  formIds="inquiryForm" 
							  targets="resultmessage" 
							  button="true" 
							  validate="true"
							  validateFunction="customValidation"
							  title="%{getText('Send_Inquiry')}"
							  onSuccessTopics="sendInquiryTransferFormSuccess"><s:text name="jsp.default_378" /></sj:a>
</div>

<script>
	setTimeout(function(){ns.common.renderRichTextEditor('memo','600',true);}, 600);
</script>
		