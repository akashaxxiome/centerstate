<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<script>
$(document).ready(function () { 
	ns.common.initializePortlet("extTransferAccountGridTabs");
});
</script>
	
	<s:include value="external_transfer_init.jsp" />
	
	<ffi:object id="externalAccountsTabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="externalAccountsTabs" property="TransactionID" value="ExternalTransfer"/>
	<ffi:setProperty name="externalAccountsTabs" property="Tab" value="externalAccounts"/>
	<ffi:setProperty name="externalAccountsTabs" property="Href" value="/cb/pages/jsp/transfers/external_transfer_accounts_summary.jsp"/>
	<ffi:setProperty name="externalAccountsTabs" property="LinkId" value="externalAccounts"/>
	<ffi:setProperty name="externalAccountsTabs" property="Title" value="jsp.transfers_external_transfer_accounts"/>
	<ffi:process name="externalAccountsTabs"/>

	<div id="extTransferAccountGridTabs" class="portlet" style="float: left; width: 100%;">
	    
	    <div class="portlet-header">
	    	<span class="portlet-title"><s:text name='jsp.transfers_external_accounts' /></span>
	    </div>
	    <div class="portlet-content">
	    	<div id="gridContainer" class="summaryGridHolderDivCls">
					<s:include value="/pages/jsp/transfers/external_transfer_accounts_summary.jsp"/>
			</div>
	    </div>
		<input type="hidden" id="getTransID" value="<ffi:getProperty name='externalAccountsTabs' property='TransactionID'/>" />
		<div id="transferSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('extTransferAccountGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	</div>
