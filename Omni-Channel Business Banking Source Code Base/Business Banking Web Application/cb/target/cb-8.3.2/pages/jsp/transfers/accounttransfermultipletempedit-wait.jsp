<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% 
String TemplateID = request.getParameter("TemplateID");
session.setAttribute("templateEdit", request.getParameter("templateEdit"));
%>
<ffi:setProperty name="SetFundsTran" property="BeanSessionName" value="TransferBatch"/>
<ffi:setProperty name="SetFundsTran" property="Name" value="MultipleTransferTemplateList"/>
<ffi:setProperty name="SetFundsTran" property="ID" value="<%= TemplateID%>" />
<ffi:process name="SetFundsTran"/>

<ffi:object id="EditTransferBatch" name="com.ffusion.tasks.banking.EditTransferBatch" scope="session"/>
   <ffi:setProperty name="EditTransferBatch" property="Locale" value="${UserLocale.Locale}" />
   <ffi:setProperty name="EditTransferBatch" property="Initialize" value="true" />
   <ffi:setProperty name="EditTransferBatch" property="Validate" value="" />
   <ffi:setProperty name="EditTransferBatch" property="Process" value="false" />
   <ffi:setProperty name="EditTransferBatch" property="MinimumAmount" value="0.01" />
   <ffi:setProperty name="EditTransferBatch" property="DateFormat" value="${UserLocale.DateFormat}" />
   <ffi:setProperty name="EditTransferBatch" property="SubmitDate" value="${GetCurrentDate.Date}" />
   <ffi:setProperty name="EditTransferBatch" property="BatchType" value="<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_BATCH_TYPE_BATCH %>" />
   <ffi:setProperty name="EditTransferBatch" property="UseTemplate" value="true" />
<ffi:process name="EditTransferBatch" />
<%
session.setAttribute("FFIEditTransferBatch", session.getAttribute("EditTransferBatch"));
%>