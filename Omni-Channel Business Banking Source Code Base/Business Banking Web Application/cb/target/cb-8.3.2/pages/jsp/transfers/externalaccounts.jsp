<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_externalaccounts" />
<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<% String toggleSortedBy = request.getParameter("ToggleSortedBy"); %>
<ffi:setProperty name="TransferAccounts" property="ToggleSortedBy" value="<%=toggleSortedBy%>"/>

<script language="JavaScript">
<!--
function doSort(SortURLParams)
{
	$.ajax({
		url:    SortURLParams , 
		success: function(data){ 
				$("#viewExternalAccountsDialogID").html(data);
				$.publish("common.topics.tabifyDialog");
				if(accessibility){
					$("#viewExternalAccountsDialogID").setFocus();
				}
		}
	}); 
}
// -->
</script>

        <div align="center">
            <ffi:setProperty name="subMenuSelected" value="transfers"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<ffi:setProperty name="TransferAccounts" property="Filter" value="ExternalTransferFrom,ExternalTransferTo,or"/>
<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="TransferAccounts"/>
<ffi:process name="SortImage"/>
    <tr>
<ffi:setProperty name="SortImage" property="Compare" value="BankName"/>
<ffi:setProperty name="SortURL" value="/cb/pages/jsp/transfers/externalaccounts.jsp?ToggleSortedBy=BankName" URLEncrypt="true"/>
        <td class="tbrd_ltb sectionsubhead" nowrap><s:text name="jsp.default_63"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
<ffi:setProperty name="SortImage" property="Compare" value="RoutingNum"/>
<ffi:setProperty name="SortURL" value="/cb/pages/jsp/transfers/externalaccounts.jsp?ToggleSortedBy=RoutingNum" URLEncrypt="true"/>
        <td class="tbrd_tb sectionsubhead" nowrap><ffi:getProperty name="TempBankIdentifierDisplayText"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
<ffi:setProperty name="SortImage" property="Compare" value="Number"/>
<ffi:setProperty name="SortURL" value="/cb/pages/jsp/transfers/externalaccounts.jsp?ToggleSortedBy=Number" URLEncrypt="true"/>
        <td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_16"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
<ffi:setProperty name="SortImage" property="Compare" value="Type"/>
<ffi:setProperty name="SortURL" value="/cb/pages/jsp/transfers/externalaccounts.jsp?ToggleSortedBy=Type" URLEncrypt="true"/>
        <td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_444"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
<ffi:setProperty name="SortImage" property="Compare" value="CurrencyCode"/>
<ffi:setProperty name="SortURL" value="/cb/pages/jsp/transfers/externalaccounts.jsp?ToggleSortedBy=CurrencyCode" URLEncrypt="true"/>
        <td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_125"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
<ffi:setProperty name="SortImage" property="Compare" value="NickName"/>
<ffi:setProperty name="SortURL" value="/cb/pages/jsp/transfers/externalaccounts.jsp?ToggleSortedBy=NickName" URLEncrypt="true"/>
        <td class="tbrd_trb sectionsubhead" nowrap><s:text name="jsp.default_294"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
    </tr>

    <% boolean toggle = false; %>
    <ffi:list collection="TransferAccounts" items="acct">
    <%  toggle = !toggle; %>
        <tr class="<%= toggle ? "pltrow1" : "pdkrow" %>">
            <td class="columndata"><ffi:getProperty name="acct" property="BankName"/></td>
            <td class="columndata"><ffi:getProperty name="acct" property="RoutingNum"/></td>
            <td class="columndata"><ffi:getProperty name="acct" property="DisplayText"/></td>
            <td class="columndata"><ffi:getProperty name="acct" property="Type"/></td>
            <td class="columndata"><ffi:getProperty name="acct" property="CurrencyCode"/></td>
            <td class="columndata"><ffi:getProperty name="acct" property="NickName"/></td>
        </tr>
    </ffi:list>

</table>
<br>
									<sj:a id="cancelFormButton" 
						                button="true" 
										onClickTopics="closeDialog"
									><s:text name="jsp.default_83"/></sj:a>
<ffi:setProperty name="TransferAccounts" property="Filter" value="All"/>
        </div>
