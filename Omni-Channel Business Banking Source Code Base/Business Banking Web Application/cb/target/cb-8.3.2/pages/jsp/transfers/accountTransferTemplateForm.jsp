<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page
	import="com.ffusion.beans.user.UserLocale,
                 com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<style type="text/css">
.errorLabel{display: block; clear: both; float:left; width:100%}
#TransferNew tr{margin-bottom:5px;}
#CurrencySectionLabel{margin: 0 0 0 10px;}
</style>

<ffi:help id="payments_transferTemplateNew" />
<span id="PageHeading" style="display: none;"><h1 id="singleTemplateTitle" class="portlet-title"><s:text
		name="jsp.transfers_18" /></h1></span>
<span class="shortcutPathClass" style="display: none;">pmtTran_transfer,templatesLink,addSingleTemplateLink</span>
<span class="shortcutEntitlementClass" style="display: none;">Transfers</span>
<ffi:setProperty name="IsConsumerUser" value="false"/>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<s:set name="isTemplate" value="true" scope="request"/>
<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />
<s:include value="/pages/jsp/transfers/inc/accounttransferform-js.jsp" />

<%--Add Transfer Template Form  --%>
<s:form id="TransferNew" namespace="/pages/jsp/transfers"
	validate="false" action="AddTransferTemplateAction_verify"
	method="post" name="TransferNew" theme="simple">

<%--Hidden variables --%>
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<input type="Hidden" name="transferDestination" id="TransferDest"
		value="">
<div class="leftPaneWrapper" role="form" aria-labelledby="saveTemplateHeader">
	<div id="addTransfer_transferTemplateName" class="templatePane leftPaneInnerWrapper leftPaneLoadPanel">
		<div class="header"><h2 id="saveTemplateHeader"><s:text name="jsp.default_366" /></h2></div>
			<div id="" class="leftPaneInnerBox">
				<span style="margin:0 0 10px; display:block" id="addTransfer_transferTemplateName"><label for="templateName"><s:text name="jsp.default_416" /></label> <span class="required" title="required">*</span></span>
				<div class="transferTemplateName">
					<s:textfield name="templateName" size="32" maxlength="32" value="%{#session.newTransferTemplate.templateName}" 
					cssClass="txtbox ui-widget-content ui-corner-all" aria-labelledby="addTransfer_transferTemplateName" aria-required="true"/> 
					<input type="hidden" name="template" value="true">
				</div>
				<div style="margin:10px 0 0;"><span id="templateNameError"></span></div>
			</div>
			
	</div>
</div>

<div id="multipleTransferPanel" class="rightPaneWrapper">
<div class="paneWrapper" role="form" aria-labelledby="transferDetails">
<div class="paneInnerWrapper">
<div class="header"><h2 id="transferDetails"><s:text name="jsp.transfer.details.label" /></h2></div>
<div class="paneContentWrapper">
	<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">

		<tr>
			<td colspan="3" align="center"><ul id="formerrors"></ul></td>
		</tr>


		<%--Template Name Section --%>
		<%--From Account Section --%>
		<tr>
			<td id="addTransfer_fromAccountLabelId" class="sectionsubhead ltrow2_color" width="48%">
				<label for="fromAccount"><s:text name="jsp.default_217" /></label> <span class="required" title="required">*</span>
			</td>
			<td width="1%"></td>
			<td id="addTransfer_toAccountLabelId" class="sectionsubhead ltrow2_color" width="48%"><label for="toAccount"><s:text name="jsp.default_424" /></label> <span class="required">*</span></td>
		</tr>
		<tr>
			<td class="columndata ltrow2_color" align="left"><ffi:setProperty name="TransferAccounts" property="Filter" value="All" /> <select class="txtbox"
				name="fromAccountID" id="FromAccount" size="1"
				onchange="onChangeCheck('from')" aria-labelledby="addTransfer_fromAccountLabelId" aria-required="true">
					<s:if test="newTransferTemplate.fromAccountID!=null">
								<option
									value="<s:property value='%{#transfer.fromAccountID}' />:<s:property value="#transfer.fromAccount.currencyCode" />"
									selected><s:property value="#transfer.fromAccount.accountDisplayText" />
								</option>
					</s:if>
			</select> </td>
			<td></td>
			<%--To Account Section --%>
			<td class="columndata ltrow2_color" align="left"><ffi:setProperty
					name="TransferAccounts" property="Filter" value="All" /> <select
				class="txtbox" name="toAccountID" id="ToAccount" size="1"
				onchange="onChangeCheck('to')" aria-labelledby="addTransfer_toAccountLabelId" aria-required="true">
					<s:if test="newTransferTemplate.toAccountID!=null">
						<option
							value="<s:property value='%{#transfer.toAccountID}' />:<s:property value="#transfer.toAccount.currencyCode" />"
							selected><s:property value="#transfer.toAccount.accountDisplayText" />
						</option>
					</s:if>
			</select>
		</td>
		</tr>
		<tr>
			<td><span id="fromAccountIDError"></span></td>
			<td></td>
			<td><span id="toAccountIDError"></span></td>
		</tr>
		<%--Amount Section --%>
		<tr>
			<td id="addTransfer_amountLabelId" class="sectionsubhead ltrow2_color">
				<span style="display:inline-block; width:120px;"><label for="Amount"><s:text name="jsp.default_43" /></label></span>
				<span id="CurrencySectionLabel" style="display: none"><span id="addTransfer_CurrencyLabel" class="sectionsubhead">
				<s:text name="jsp.default_125" /></span><span class="required" >*</span></span>	
			</td>
			<td></td>
			<td id="addTransfer_memoLabelId" class="sectionsubhead ltrow2_color"><label for="Memo"><s:text name="jsp.default_279" /></label></td>
		</tr>
		<tr>
			<td class="columndata ltrow2_color" align="left"><s:textfield
					name="amount" id="Amount1" size="12" maxlength="15" value="0.00"
					cssClass="txtbox ui-widget-content ui-corner-all" aria-labelledby="addTransfer_amountLabelId"/> <span
				id="CurrencyLabel" style="display: none">&nbsp;</span> <span
				id="CurrencySection" style="display: none">&nbsp;
					<select class="ui-widget-content ui-corner-all"
					name="userAssignedAmountFlagName" id="CurrencySelect">
						<option value="single" selected><s:text
								name="jsp.transfers_70" /></option>
				</select> <input type="hidden" id="OtherAmount" value="0.00">
			</span></td>
			<td></td>
			<td class="columndata ltrow2_color addTransfer_memo" align="left" colspan="2"><s:textfield
					name="memo" size="40" maxlength="100"
					cssClass="txtbox ui-widget-content ui-corner-all" value="%{#newTransferTemplate.memo}" aria-labelledby="addTransfer_memoLabelId"/></td>
		</tr>
		<tr>
			<td>
				<span id="amountError" style="width:160px"></span>
				<span id="toAmountError" style="width:160px"></span>
				<span id="userAssignedAmountError"></span>
			</td>
			<td></td>
			<td></td>
		</tr>
		<%--Memo Section --%>
		<%--Frequency Section --%>
		<tr>
			<td id="addTransfer_recuringLabelId" class="sectionsubhead ltrow2_color" colspan="3"><s:text
					name="jsp.transfers_65.1" /></td>
		</tr>
		<tr>
			<td class="columndata ltrow2_color" align="left" colspan="3"><s:select
					id="single_transfer_frequency"
					cssClass="ui-widget-content ui-corner-all"
					onchange="modifyFrequency();"
					headerValue="%{getText('jsp.default_296')}" headerKey="None"
					list="%{#request.frequencyList}" name="frequency" />  <script>
					$('#single_transfer_frequency').selectmenu({
						width : 110
					});
				</script>
				<input
				id="unlimitedTransfers" onclick="toggleRecurring();" type="radio"
				align="middle" name="openEnded" value="true" border="0"> <s:text
					name="jsp.default_446" /> <input id="noOfTransfers"
				onclick="toggleRecurring();" type="radio" align="middle"
				name="openEnded" value="false" border="0" checked> <s:text
					name="jsp.transfers_1" /> &nbsp; <input
				id="numberOfTransfersValue" type="text" name="numberTransfers"
				value="<ffi:getProperty name="newTransfer" property="NumberTransfers"/>"
				size="3" maxlength="3" style="width:50px"
				class="txtbox ui-widget-content ui-corner-all"> 	
			</td>
		</tr>
		<tr><td colspan="3"><span id="frequencyValueError"></span><span id="numberTransfersError"></span></td></tr>
		<tr><td colspan="3"></td></tr>
		<%--Transfer Count Section--%>
		<tr>
			<td id="addTransfer_requiredFieldLabelId" colspan="3" align="center">
				<br> <span class="required">* <s:text
						name="jsp.default_240" /></span>
			</td>
		</tr>
		<tr><td colspan="3" style="padding: 0; line-height: 5px;">&nbsp;</td></tr>
		<%--Transfer Template Buttons Section--%>
		<tr class="ltrow_color" height="8">
			<td class="columndata ltrow2_color" align="center" colspan="3">
				<sj:a id="cancelFormButtonOnInput" button="true" summaryDivId="summary" buttonType="cancel"
					onClickTopics="showSummary,cancelTemplatesForm">
					<s:text name="jsp.default_82" />
				</sj:a> <sj:a id="verifyTransferSubmit1" formIds="TransferNew"
					targets="verifyDiv" button="true" validate="true"
					validateFunction="customValidation"
					onBeforeTopics="beforeVerifyTransferForm"
					onCompleteTopics="verifyTransferFormComplete"
					onErrorTopics="errorVerifyTransferForm"
					onSuccessTopics="successVerifyTransferForm"
					onClick="onSubmitCheck(false)">
					<s:text name="jsp.default_395" />
				</sj:a>
			</td>
		</tr>
	</table>
</div>
</div>
</s:form>
</div></div>