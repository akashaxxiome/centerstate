<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
 
<ffi:help id="payments_externalaccounts" />
<div class="externallDialogHt" style="margin-bottom:40px;min-height:150px; ">
	<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
	<ffi:process name="BankIdentifierDisplayTextTask"/>
	<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
	<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>
	
	<br/>
	<ffi:setProperty name="tempURL" value="/pages/jsp/transfers/getExternalTransferAccountsAction.action" URLEncrypt="true"/>
    <s:url id="extTransferAccountsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="extTransferAccountsGridID"  
		caption=""  
		dataType="json"  
		href="%{extTransferAccountsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortable="true" 
		sortname="date"
		sortorder="desc"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onCompleteTopics="addGridControlsEvents,extTransferAccountsCompleteTopic"
		> 
		
		<sjg:gridColumn name="bankName" index="BankName" title="%{getText('jsp.default_63')}" sortable="true" width="80"/>
		<sjg:gridColumn name="routingNum" index="RoutingNum" title="%{#session.TempBankIdentifierDisplayText}" sortable="true" width="55"/>
		<sjg:gridColumn name="displayText" index="DisplayText" title="%{getText('jsp.default_16')}" sortable="true" width="70"/>
		<sjg:gridColumn name="type" index="Type" title="%{getText('jsp.default_444')}" sortable="true" width="50"/>
		<sjg:gridColumn name="currencyCode" index="CurrencyCode" title="%{getText('jsp.default_125')}" sortable="true" width="55"/>
		<sjg:gridColumn name="nickName" index="NickName" title="%{getText('jsp.default_294')}" sortable="true" width="90"/>
		
	</sjg:grid>
	
	<div  class="ui-widget-header customDialogFooter">
		<sj:a id="cancelFormButton" 
        button="true" 
		onClickTopics="closeDialog"
		><s:text name="jsp.default_83"/></sj:a>
		<br/>
	</div>
</div>	