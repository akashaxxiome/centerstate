<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
		
				<div id="quicksearchcriteria" class="quickSearchAreaCls">
				<s:form action="/pages/jsp/transfers/getTransfersAction_verify.action" method="post" id="QuickSearchTransfersFormID" name="QuickSearchTransfersForm" theme="simple">
					<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
					<table border="0" class="tableData">
						<tr>
						<td>
						<table border="0" width="100%">
						
						<tr >
							<td>
								<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.default.label.date.range"/></span>
								<input type="text" id="transferDateRangeBox" />
								
								<input title="%{getText('Choose_start_date')}"  value="<ffi:getProperty name='transferSearchCriteria' property='startDate' />" id="StartDateID" 
								name="startDate" type="hidden" />
								<input title="%{getText('Choose_to_date')}" value="<ffi:getProperty name='transferSearchCriteria' property='endDate' />" id="EndDateID"  
								name="endDate" type="hidden" /> <br />
							</td>
							<td nowrap="nowrap">
								<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.transfers_text_from_account"/></span>
								<select class="txtbox" name="fromAccountId" id="fromAccountId">
	                            </select>
							</td>
							<td nowrap="nowrap">
								<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.transfers_text_to_account"/></span>
								<select class="txtbox" name="toAccountId" id="toAccountId">
								</select>
							</td>
							<td>
								<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.transfers_text_amount"/></span>
								<input type="text" id="Amount" name="Amount" size="16" maxlength="20" value="<ffi:getProperty name='Amount'/>" class="ui-widget-content ui-corner-all">
								<span id="amountError"></span>
								<span id="toAmountError"></span>
							</td>
	
							<td>
								<span class="sectionsubhead dashboardLabelMargin" style="display:block;">&nbsp;</span>
								<sj:a targets="quick"
										id="quicksearchbutton"
										formIds="QuickSearchTransfersFormID"
										button="true" 
		                                validate="true"
		                                validateFunction="customValidation"
										onclick="removeValidationErrors();"
										onCompleteTopics="quickSearchTransfersComplete"
										><s:text name="jsp.default_6" />
								</sj:a>
							</td>
						</tr>
						<tr>
							<td colspan="5">
								<span id="startDateError"></span>
								<span id="endDateError"></span>
								<span id="dateRangeValueError"></span>						
							</td>
						</tr>
						</table>
						</td>
						</tr>
					</table>
				</s:form>
				</div>
			

<script>
	$(document).ready(function(){
			$("#dateRangeValue").selectmenu({width: 150});
			
			var value = {value:"All Accounts", label: "All Accounts"};
			$("#fromAccountId").lookupbox({
				"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
				"controlType":"server",
				"collectionKey":"1",
				"size":"35",
				"pairTo":"toAccountId",
				"module":"transferDashboard",
				"accounts":"from",
				"defaultOption":value
			});
			
			$("#toAccountId").lookupbox({
				"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
				"controlType":"server",
				"collectionKey":"1",
				"size":"35",
				"pairTo":"fromAccountId",
				"module":"transferDashboard",
				"accounts":"to",
				"defaultOption":value
			});
			
			var aConfig = {
				startDateBox:$("#StartDateID"),
				endDateBox:$("#EndDateID")
			};
			$("#transferDateRangeBox").extdaterangepicker(aConfig);
	
		// resets the selected index of the given dropdown to 0
		function clearDropDown() {		
			$("#dateRangeValue").selectmenu('value',"");		
		}
	
		// clears the 2 text fields
		function clearTextFields( field1, field2, formName ) {
			formName[field1].value = '';
			formName[field2].value = '';
			if ($("#dateRangeValue").val() == null || $("#dateRangeValue").val() == "") {
		
			}
		}
	
		// checks (selects) the given radio button
		//	radioIndex is the index of the radio button that you wish to check
		function setRadioButton( fieldName, radioElement, formName ) {
			formName[fieldName][radioElement].checked = true;
		}
	});
</script>
