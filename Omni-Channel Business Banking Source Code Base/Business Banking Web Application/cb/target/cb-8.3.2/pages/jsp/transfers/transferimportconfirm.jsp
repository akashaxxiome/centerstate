<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div align="left">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
	        <td>
			<s:form id="transferuploadconfirmform" name="transferuploadconfirmform" namespace="/pages/fileupload" action="transferFileUploadAction_continueProcess" method="post" theme="simple">
           			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<div id="importResultsDiv">
								<br/><br/>
								<span class="sectionhead">&gt; <s:text name="jsp.transfers_32"/></span><br/><br/>
								<span class="sectionhead"><s:text name="jsp.default_421"/>:</span>
								<br/><br/>
									<ffi:cinclude value1="${ImportErrors}" value2="" operator="notEquals">
										
											<s:include value="%{#session.PagesPath}/common/fileImportErrorsGrid.jsp"/>
									
									</ffi:cinclude>
									
									<br/>
									
									<div align="center" valign="bottom">
												<sj:a
												id="cancelImport"
												button="true" 
												summaryDivId="summary" buttonType="cancel"
												onClickTopics="cancelForm,cancelTransferFileImportForm,showSummary"
												><s:text name="jsp.default_82"/></sj:a>
											
											<ffi:cinclude value1="${ImportErrors.operationCanContinue}" value2="false" operator="notEquals">
												<sj:a
												id="continueImport" 
												targets="inputDiv"
												formIds="transferuploadconfirmform"
												button="true" 
												onCompleteTopics="completeConfirmUploadedTransfersProcess"
												onSuccessTopics="successConfirmUploadedTransfersProcess"
												onErrorTopics="errorConfirmUploadedTransfersProcess"
												onBeforeTopics="beforeConfirmUploadedTransfersProcess"
												><s:text name="jsp.default_111"/></sj:a>
											</ffi:cinclude>
									</div>
									<br/>
					</div>
			</s:form>
			</td>
		</tr>
	</table>
</div>
