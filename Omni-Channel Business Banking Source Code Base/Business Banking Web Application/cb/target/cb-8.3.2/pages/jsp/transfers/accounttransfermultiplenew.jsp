<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"
	import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<% boolean canImport = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
 		<% canImport = true; %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS_SWIFT_FILE_UPLOAD %>">
			 <% canImport = true; %>
</ffi:cinclude>

<%-- if Logged in user is consumer then dont show file import --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			 <% canImport = false; %>
</ffi:cinclude>

<s:if test="%{transferBatch.templateName!= ''}">
<% canImport = false; %>
</s:if>
<div id="multipleTransferPanel">
	<ffi:cinclude
		ifEntitled="<%=EntitlementsDefines.ENT_TRANSFERS_TEMPLATE%>">
		<div class="leftPaneWrapper" role="form" aria-labelledby="LoadTemplate/FileImport">
				<div id="templateForMultipleTransfer" class="templatePane leftPaneInnerWrapper">
					<% if (canImport) { %>
						<div class="header"><h2 id="LoadTemplate/FileImport"><s:text name="jsp.default.load_template_file_import" /></h2></div>
					<% } else {%>
						<div class="header"><s:text name="jsp.default_264" /></div>
					<% } %>
					<div id="templateLoadID" class="leftPaneInnerBox" class="leftPaneInnerBox">
					<label id="labelTemplateLoad" for="MultipleTransferTemplateList"></label>
						<s:include value="accounttransfermultipletempload.jsp" />
					</div>
					<% if (canImport) { %>
						<div style="width:100%; text-align:center">
							<span class="toUpperCase"><s:text name="jsp.default_306" /></span><br><br>
							<sj:a name="fileImportBtn" id="fileImportBtn" onclick="ns.common.setModuleType('transfer',function(){$('#transferFileImportLink').trigger('click');})" button="true" cssStyle="margin-left:10px; margin-bottom: 10px;">
								<s:text name="jsp.default_539.1" />
							</sj:a>
						</div>
					<% } %>
				</div>
			<%--Transfer Batch Total + Buttons Section --%>
				<div id="TotalSection" class="leftPaneInnerWrapper">
					<span id="TotalTitle">
						<s:text name="jsp.default_431" /> :
					</span>
					<span id="TotalValue">--------</span>
				</div>
		</div>
	</ffi:cinclude>
	<div id="MultipleTransferForm" class="rightPaneWrapper">
		<script>
			$.subscribe('onSubmitCheckTopics', function(event, data) {
				onSubmitCheck(false);
			});
		</script>
		<span id="PageHeading" style="display: none;"><h1 id="multipleTransferTemplate" class="portlet-title"><s:text
				name="jsp.transfers_14" /></h1></span> <span class="shortcutPathClass"
			style="display: none;">pmtTran_transfer,addMultipleTransferLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">Transfers</span>
		<s:include
			value="/pages/jsp/transfers/accounttransfermultiplenewform.jsp" />
	</div>
	<!-- MultipleTransferForm -->
</div>
<!-- MultipleTransferPanel -->
