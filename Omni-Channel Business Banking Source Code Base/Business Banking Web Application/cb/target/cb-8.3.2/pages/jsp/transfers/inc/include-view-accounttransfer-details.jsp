<%--
Name: 			include-view-accounttransfer-details.jsp

Flow(s):		View Single Transfer
				View Recurring Transfer
				View Single Transfer Template
				View Recurring Transfer Template
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page
	import="com.ffusion.beans.bcreport.ReportLogRecord,
				 com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.accounts.*"%>
<%@ page
	import="com.ffusion.util.enums.UserAssignedAmount, com.ffusion.beans.banking.TransferDefines"%>

<div class="approvalDialogHt2">
<%--Set the bean(s) that is to be used on the JSP --%>
<s:set var="transfer" value="#request.transfer" />

<%--Transfer Template Name(Secion will be visible for view Single Transfer Template) --%>
<s:if test="%{#transfer.templateName!= null && #transfer.templateName!=''}">
	<div class="templateConfirmationWrapper">
			<div class="confirmationDetails">
				<span id="viewTransfer_templateLabelId" class="sectionLabel"><s:text name="jsp.default_416" />: </span>
				<span id="viewTransfer_templateValueId"><s:property value="#transfer.templateName" /></span>
			</div>
	</div>
</s:if>
<s:if test="%{#request.istemplateflow!='true'}">
	<%--Display Transfer Tracking ID Section --%>
	<div class="confirmationDetails">
		<span id="viewTransfer_referenceLabelId"><s:text name="jsp.default_547" /></span>
		<span id="viewTransfer_referenceValueId"><s:property value="#transfer.trackingID" /></span>
	</div>
</s:if>
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /> :</div>
<div class="blockContent">
		<%-- <span id="viewTransfer_statusLabelId" class="sectionLabel"><s:text name="jsp.default_388" />: </span> --%>
		<s:if test="%{#transfer.StatusName!='Scheduled'}">
				<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
				<s:set var="TRS_INSUFFICIENT_FUNDS"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
				<s:set var="TRS_BPW_LIMITCHECK_FAILED"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
				<s:set var="TRS_FAILED_APPROVAL"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_APPROVAL==#transfer.status}" />
				
				<s:if test="%{#TRS_FAILED_TO_TRANSFER}">
						<s:set var="HighlightStatus" value="true" />
				</s:if>
				<s:elseif test="%{#TRS_INSUFFICIENT_FUNDS}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				<s:elseif test="%{#TRS_BPW_LIMITCHECK_FAILED}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				<s:elseif test="%{#TRS_FAILED_APPROVAL}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				
				<s:set var="TRS_SCHEDULED"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
					
				<s:set var="TRS_PENDING_APPROVAL"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
						
				<s:if test="%{#HighlightStatus==true}">
					<div class="blockRow failed">
						<span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-decline"></span>
							<s:if test="%{#transfer.backendErrorDesc=='' || #transfer.backendErrorDesc==null }">
								<s:property	value="%{#transfer.statusName}" />
								</s:if> 
								<s:else>
								<s:property	value="%{#transfer.statusName}" /> - <s:property value="%{#transfer.backendErrorDesc}"/>
								</s:else>
						</span>
					</div>
				</s:if> 
				<s:else>
					<s:if test="%{#TRS_PENDING_APPROVAL}">
						<div class="blockRow pending"><span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-pending"></span>
							<s:property value="%{#transfer.statusName}" />
						</span></div>
					</s:if>
					<s:else>
					<div class="blockRow completed"><span id="viewTransfer_statusValueId">
						<span class="sapUiIconCls icon-accept"></span>
						<s:property value="%{#transfer.statusName}" />
					</span></div>
					</s:else>
				</s:else>
		</s:if>
		<s:if test="%{#transfer.StatusName=='Scheduled'}">
			<div class="blockRow pending"><span id="viewTransfer_statusValueId">
				<span class="sapUiIconCls icon-future"></span>
				<s:property value="%{#transfer.statusName}" />
				</span></div>
		</s:if>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
<div class="blockContent">
	<div class="blockRow">
		<div class="inlineBlock" style="width:50%">
			<%--Transfer From Account Section --%>
			<span id="viewTransfer_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217" />: </span>
			<span id="viewTransfer_fromAccountValueId"><s:property value="#transfer.fromAccount.accountDisplayText"/></span>
		</div>
		<div class="inlineBlock">
			<%--Transfer Amount display.If MultiCurrency Transfer Amount and To Amount is displayed --%>
			<s:set var="DisplayEstimatedAmountKey" value="false" />
			<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
					<span id="viewTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43" />: </span>
					<span id="viewTransfer_amountValueId">
						<s:property	value="#transfer.amountValue.CurrencyStringNoSymbol" /> 
						<s:property value="#transfer.amountValue.CurrencyCode" /></span>
			</s:if>
			<s:else>
					<span id="viewTransfer_fromAmountLabelId" class="sectionLabel"><s:text name="jsp.default_489" /> : </span>
					<span id="viewTransfer_fromAmountValueId">
						<s:if test="%{#transfer.isAmountEstimated=='true'">
                           &#8776;<s:set
								var="DisplayEstimatedAmountKey" value="true" />
						</s:if> <s:property
							value="#transfer.amountValue.CurrencyStringNoSymbol" /> <s:property
							value="#transfer.amountValue.CurrencyCode" />
					</span>
			</s:else>
			
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width:50%">
			<%--Transfer TO Account Section --%>
			<span id="viewTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424" />: </span>
			<span id="viewTransfer_toAccountValueId"><s:property value="#transfer.toAccount.accountDisplayText"/></span>
		</div>
		<div class="inlineBlock">
			<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
			</s:if>
			<s:else>
				<span id="viewTransfer_toAmountLabelId" class="sectionLabel"><s:text name="jsp.default_512" />: </span>
					<span id="viewTransfer_toAmountValueId" class="columndata">
						<s:if test="%{#transfer.isToAmountEstimated=='true'">
                           &#8776;<s:set
								var="DisplayEstimatedAmountKey" value="true" />
						</s:if> <s:property
							value="#transfer.toAmountValue.CurrencyStringNoSymbol" /> <s:property
							value="#transfer.toAmountValue.CurrencyCode" />
					</span>
			</s:else>
		</div>
	</div>
	<s:if test="%{#request.istemplateflow!='true'}">
		<%--Display Transfer Date Section --%>
		<s:if test="%{#request.transfer.templateName== null || #request.transfer.templateName==''}">
				<div class="blockRow">
					<span id="viewTransfer_dateLabelId" class="sectionLabel"><s:text name="jsp.default_137" />: </span>
					<span id="viewTransfer_dateValueId"><s:property value="#transfer.date" /></span>
				</div>
			</s:if>
	</s:if>
	<div class="blockRow">
		<span id="viewTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" />: </span>
		<span id="viewTransfer_memoValueId"><s:property value="#transfer.memo" /></span>
	</div>
	<%--Check if Transfer/Template is Recurring and Display values --%>
	<s:if test="%{#transfer.templateName!= null && #transfer.templateName!=''}">
		<s:set var="isRecTemplate">true</s:set>
	</s:if>
	<s:set var="frequency" value="%{#transfer.frequency}" />
	<s:if test="%{#frequency!=null}">
	<s:if test="%{#attr.IsRecModel.equals('true') || #isRecTemplate=='true'}">
		<%--Display Transfer Frequency Section --%>
		<div class="blockRow">
		<div class="inlineBlock" style="width:50%">
			<span id="viewTransfer_frequencyLabelId" class="sectionLabel"><s:text name="jsp.default_351" />: <!--L10NEnd--></span>
			<span id="viewTransfer_frequencyValueId"><s:property value="#frequency" /></span>
		</div>
		<%--Display Transfer Counts Section --%>
		<div class="inlineBlock">
			<span id="viewTransfer_noOfTransfersLabelId" class="sectionLabel"><s:text name="jsp.default_443" />: </span>
			<span id="viewTransfer_noOfTransfersValueId">
				<s:set var="numberTransfers"
					value="%{#transfer.numberTransfers}" /> <s:if
				test="%{#transfer.numberTransfers==999}">
				<s:text name="jsp.default_446" />
				</s:if> <s:if test="%{#transfer.numberTransfers!=999}">
				<s:property value="#numberTransfers" />
				</s:if>
			</span>
		</div>
		</div>
		<div class="blockRow">
		<div class="inlineBlock" style="width:50%">
			<span id="viewTransfer_endDateLabelId" class="sectionLabel"><s:if test="%{#isRecTemplate=='true'}"><s:text name="jsp.default_137"/></s:if><s:else><s:text name="jsp.default.end.date"/></s:else>: <!--L10NEnd--></span>
			<span id="viewTransfer_endDateValueId">
			
			<s:if test="%{#transfer.numberTransfers==999}">
				<s:text name="jsp.default.not.applicable"/>
			</s:if> 
			<s:if test="%{#transfer.numberTransfers!=999}">
				<s:property value="#transfer.endDate" />
			</s:if>
			
			</span>
		</div>
		<s:if test="%{#isRecTemplate!='true'}">
		<div class="inlineBlock">
			<span id="viewTransfer_numberOfPendingTransfersLabelId" class="sectionLabel"><s:text name="jsp.default.future.transfers"/>: <!--L10NEnd--></span>
			<span id="viewTransfer_numberOfPendingTransfersValueId">
			
			<s:if test="%{#transfer.numberTransfers==999}">
				<s:text name="jsp.default.not.applicable"/>
			</s:if> 
			<s:if test="%{#transfer.numberTransfers!=999}">
				<s:property value="#transfer.numberOfPendingTransfers" />
			</s:if>
			
			</span>
		</div>
		</s:if>
		</div>
	</s:if>
	</s:if>
</div>	
<div class="estimatedAmountMessage">
	<s:if test="%{#DisplayEstimatedAmountKey=='true'}">
		<span id="viewTransfer_estimatedAmountLabelId">&#8776; <s:text name="jsp.default_241" /></span>
	</s:if>
</div>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
	operator="equals">
	<s:if test="%{isVirtualInstance !='true'}">
		<s:include value="include-view-transaction-history.jsp" />
	</s:if>
</ffi:cinclude>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
</div>