<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- set the BC property to false - this is required to determine which set --%>
<%-- of images/pages etc to use for the app --%>
<ffi:setProperty name="APPROVALS_BC" value="false"/>
	
<%-- set the PAYMENTS property to true in order to load style attributes for payments section --%>
<ffi:setProperty name="APPROVALS_CB_PAYMENTS" value="true"/>

<%-- include the common constants page --%>
<s:include value="/pages/jsp/common/include-view-transaction-details-constants.jsp"/>
