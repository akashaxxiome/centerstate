<%@ page import="com.ffusion.beans.banking.TransferDefines"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.banking.TransferStatus"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%--Set variables to be used on the JSP --%>
<s:set var="fromPortalPage" value="#parameters.fromPortalPage" />
<s:set var="isNewTransferFlow"
	value="%{#request.flowType=='NewTransfer'}" />
<s:if test="%{#isNewTransferFlow}">
	<s:set var="transfer" value="%{#session.newTransferTemplate}" />
	<s:set name="strutsActionName" value="%{'AddTransferTemplateAction'}" />
</s:if>
<s:else>
	<s:set var="transfer" value="#session.editTransferTemplate" />
	<s:set name="strutsActionName" value="%{'EditTransferTemplateAction'}" />
</s:else>
<s:set var="date" value="%{#transfer.date}" />


<%--Trnsfer Template Verify Form --%>
<ffi:help id="payments_transferTemplateEditVerify" />
<s:form id="TransferNewVerify" namespace="/pages/jsp/transfers" validate="false" action="%{#strutsActionName + '_execute'}" method="post" name="TransferNewVerify" theme="simple">
<%--Hidden variables --%>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
<div class="leftPaneWrapper" id="templateForSingleTransferVerify" role="form" aria-labelledby="templateSummary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id=templateSummary"><s:text name="jsp.default.templates.summary" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%" class="floatleft">
			<span id="verifyTransfer_templateLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_416" />:</span>
			<span id="verifyTransfer_templateValueId" class="inlineSection floatleft labelValue"><s:property value="#transfer.templateName" /></span>
		</div>
	</div>
</div>
</div>
<div class="confirmPageDetails">
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<s:set var="DisplayEstimatedAmountKey" value="false" />
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyTransfer_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217" />: </span>
				<span id="verifyTransfer_fromAccountValueId"><s:property value="#transfer.fromAccount.accountDisplayText"/></span>
			</div>
			<div class="inlineBlock">
				<%--Amount Section in case Single Currency Transfer--%>
				<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
					<span id="verifyTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43" />: </span>
					<span id="verifyTransfer_amountValueId">
						<s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" /> 
						<s:property value="#transfer.AmountValue.CurrencyCode" />
					</span>
				</s:if>
				<%--Amount Section in case Mutli Currency Transfer--%>
				<s:else>
						<span id="verifyTransferFrom_amountLabelId" class="sectionLabel"><s:text name="jsp.default_489" />: </span>
						<span id="verifyTransferFrom_amountValueId">
							<s:if test="%{#transfer.IsAmountEstimated}">
                                 &#8776;<s:set var="DisplayEstimatedAmountKey" value="true" />
							</s:if> 
							<s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" />
							<s:property value="#transfer.AmountValue.CurrencyCode" /></span>
				</s:else>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424" />: </span>
				<span id="verifyTransfer_toAccountValueId"><s:property value="#transfer.toAccount.accountDisplayText"/></span>
			</div>
			<div class="inlineBlock">
				<s:if test="%{#transfer.userAssignedAmountFlagName=='single'}">
				</s:if>
				<s:else>
						<span id="verifyTransferTo_amountLabelId" class="sectionLabel"><s:text name="jsp.default_512" />: </span>
						<span id="verifyTransferTo_amountValueId">
							<s:if test="%{#transfer.IsToAmountEstimated}">
		                                        &#8776;<s:set var="DisplayEstimatedAmountKey" value="true" />
							</s:if> 
							<s:property value="#transfer.toAmountValue.CurrencyStringNoSymbol" />
							<s:property value="#transfer.toAmountValue.CurrencyCode" /></span>
				</s:else>
			</div>
		</div>
	</div>
	<div class="blockHead"><s:text name="jsp.miscellaneous.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<%--Transfer Type Section(Internal to Internal, External to Internal,Internal to External) --%>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
			<s:set var="TRANSFER_BOOK" value="%{'INTERNAL'}" />
			<s:set var="TRANSFER_ITOI" value="%{'ITOI'}" />
			<%--Values to be in sync with com.ffusion.beans.banking.TransferDefines --%>
			<s:set var="TRANSFER_ITOE" value="%{'ITOE'}" />
			<s:set var="TRANSFER_ETOI" value="%{'ETOI'}" />
				<span id="verifyTransfer_typeLabelId"  class="sectionLabel"><s:text name="jsp.default_444" />: </span>
				<span id="verifyTransfer_typeValueId">
					<s:if test="%{#transfer.transferDestination==#TRANSFER_BOOK || #transfer.transferDestination==#TRANSFER_ITOI}">
						<s:text name="jsp.transfers_53" />
					</s:if> <s:if test="%{#transfer.transferDestination==#TRANSFER_ITOE}">
						<s:text name="jsp.transfers_52" />
					</s:if> 
					<s:if test="%{#transfer.transferDestination==#TRANSFER_ETOI}">
						<s:text name="jsp.transfers_44" />
					</s:if>
				</span>
			</ffi:cinclude>
		</div>
		<s:if test="%{memo!=''}">
		<div class="blockRow">
		<%--Transfer Memo Section --%>
		<s:set var="TransferMemo" value="%{#transfer.memo}" />
				<span id="verifyTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" />: </span>
				<span id="verifyTransfer_memoValueId"><s:property value="#transfer.memo" /></span>
		</div>
		</s:if>
	</div>
	<div  class="blockHead"><s:text name="jsp.recurrence.label" /></div>
	<div class="blockContent">
		<s:if test="%{#request.isRecurring='true'}">
			<div class="blockRow"><%--Rec Transfer Frequency + Transfer Count --%>
			<%--Transfer Frequency --%>
				<span id="verifyTransfer_recuringLabelId" class="sectionLabel"><s:text name="jsp.default_351" />: </span>
				<span id="verifyTransfer_recuringValueId"><s:property value="#transfer.frequency" /></span>
			</div>
		</s:if>
		<s:if test="%{#request.isRecurring='true'}">
		<div class="blockRow">
			<%--Transfer Count --%>
				<span id="verifyTransfer_noOfTransfersLabelId" class="sectionLabel"><s:text name="jsp.default_443" />: </span>
				<span id="verifyTransfer_noOfTransfersValueId">
					<s:if test="%{#transfer.NumberTransfers==999}">
						<s:text name="jsp.default_446" />
					</s:if><s:else>
						<s:property value="#transfer.numberTransfers" />
					</s:else>
				</span>
		</div>
		</s:if>
	</div>	
	
<%--Show message if the amount is estimated --%>
<s:if test="%{#DisplayEstimatedAmountKey=='true'}">
	<div class="mltiCurrenceyMessage">
		<span class="required">&#8776;
			<s:text name="jsp.default_241" />
		</span>
	</div>
</s:if>
<s:else>
</s:else>

<%--This condition check if the flow is Edit Transfer Template --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>">
	<s:if test="%{#transfer.iD!=null && #transfer.iD!=''}">
	<div  class="blockHead" id="verifyTransfer_TemplateLifecycleLabelId"><s:text name="jsp.default_414" /></div>
		<div class="blockContent">
			<s:if test="%{#DisplayEstimatedAmountKey=='true'}">
				
			</s:if>
			<div class="blockRow">
			<%-- Transfer Status Section --%>
				<span id="verifyTransfer_currnetStatusLabelId" class="sectionLabel"><s:text name="jsp.default_130" />: </span>
				<span class="columndata ltrow2_color"><s:property value="#transfer.statusName" /></span>
			</div>
			<div class="blockRow">
			<%--Transfer Template Comment Section --%>
				<span ><s:text name="jsp.default_534" />: </span>
				<span class="columndata"><s:property value="#transfer.templateComment" /></span>
			</div>
		</div>
	</s:if>
</ffi:cinclude>


<%--Verify Transfer Template Form Buttons --%>
<div class="btn-row">
	<s:if test="%{#request.fromPortalPage=='true'}">
		<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
		<sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel"
			onClickTopics="showSummary,cancelEditPendingTransferForm">
			<s:text name="jsp.default_82" />
		</sj:a>
		<sj:a id="backFormButton" button="true"
			onClickTopics="backPortalEditTransfer,hideOperationResult">
			<s:text name="jsp.default_57" />
		</sj:a>
		<sj:a id="sendSingleTransferSubmit" formIds="TransferNewVerify"
			targets="confirmDiv" button="true"
			onSuccessTopics="sendTransferPortalFormSuccess"
			onErrorTopics="errorSendTransferFormPortal">
			<s:text name="jsp.transfers_71" />
		</sj:a>
		<input type="hidden" name="fromPortalPage" value="true"></input>
	</s:if> 
	<s:else>
		<sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel"
			onClickTopics="showSummary,cancelTemplatesForm">
			<s:text name="jsp.default_82" />
		</sj:a>
		<sj:a id="backFormButton" button="true"
			onClickTopics="backToInput,hideOperationResult">
			<s:text name="jsp.default_57" />
		</sj:a>
		<sj:a id="saveSingleTransferTemplateSubmit"
			formIds="TransferNewVerify" targets="confirmDiv" button="true"
			onBeforeTopics="beforeSendTransferTemplate"
			onSuccessTopics="sendTransferTemplateSuccess"
			onErrorTopics="errorSendTransferTemplate"
			onCompleteTopics="sendTransferTemplateComplete">
			<s:text name="jsp.default_366" />
		</sj:a>
	</s:else>
</div>	
</div>
</s:form>
