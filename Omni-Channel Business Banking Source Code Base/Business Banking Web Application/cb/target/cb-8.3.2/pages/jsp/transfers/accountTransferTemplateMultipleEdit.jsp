<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page
	import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.banking.TransferBatch"%>

<ffi:help id="payments_accounttransfermultipletempedit" />
<s:set var="transferBatch" value="#session.editTransferTemplateBatch" />


<script>
		$.subscribe('onSubmitCheckTopics', function(event,data) {
			onSubmitCheck(false);
		});
</script>


<!-- Check AppType of user -->
<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />

<style type="text/css">
.columndataauto0 {
	padding: 3px
}

.columndataauto1 {
	padding: 3px
}

.columndataauto2 {
	padding: 3px;
	display: none
}

.columndataauto3 {
	padding: 3px;
	display: none
}

.columndataauto4 {
	padding: 3px;
	display: none
}
</style>


<script>
				var transferSize = <s:property value="%{#transferBatch.transferCount}"/>;
				if(transferSize < 2) transferSize = 2;
		</script>



<script>
			for(var i = 2; i < transferSize; i++){
				$(".columndataauto" + i).show();
			}
		</script>


<s:include value="/pages/jsp/transfers/inc/edit-transfer-template-js.jsp" />
<div id="multipleTemplatePanel" align="center">
	<div id="multipleTemplateForm" align="center">
		<span id="PageHeading" style="display: none;"><h1 id="editMultipleTransfersTemplate" class="portlet-title"><s:text name="jsp.transfers_101" /></h1></span>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td class="ltrow2_color">
		<div>
			<s:form namespace="/pages/jsp/transfers" validate="false" action="EditTransferTemplateBatchAction_verify" method="post" name="TransferMultipleEdit" id="TransferMultipleEdit" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
			<div class="leftPaneWrapper" role="form" aria-labelledby="saveTemplate">
				<div id="addTransfer_transferTemplateName" class="templatePane leftPaneInnerWrapper">
					<div class="header"><h2 id="saveTemplate"><s:text name="jsp.default_371" /></h2></div>
						<div id="" class="leftPaneInnerBox">
							<span style="margin:0 0 10px; display:block"><label for="templateName"><s:text name="jsp.default_416" /></label> <span class="required" title="required">*</span></span>
							<s:textfield name="editTransferTemplateBatch.templateName" id="templateName" size="32" maxlength="32" value="%{#session.editTransferTemplateBatch.templateName}"
										cssClass="ui-widget-content ui-corner-all" aria-labelledby="" aria-required="true"/>
							<div style="margin:10px 0 0;"><span id="TemplateNameError"></span></div>
						</div>
						
				</div>
				<%--Transfer Batch Total + Buttons Section --%>
				<div id="TotalSection" class="leftPaneInnerWrapper">
					<span id="TotalTitle">
						<s:text name="jsp.default_431" />:
					</span>
					<span id="TotalValue">--------</span>
				</div>
			</div>	
			<div id="multipleTransferPanel" class="rightPaneWrapper" align="center">
				<div class="paneWrapper" role="form" aria-labelledby="templateSummary">
				  <div class="paneInnerWrapper">
					<div class="header"><h2 id="templateSummary"><s:text name="jsp.default.templates.summary" /></h2></div>
					<div class="paneContentWrapper">
				<table border="0" cellspacing="0" cellpadding="3" width="100%">

				<tr>
					<td colspan="3" align="center"><span id="formerrors"></span></td>

				</tr>
			<s:set var="categoryCount" value="0" />
			<s:set var="transfers" value="#transferBatch.transfers" />
			<s:iterator value="#transfers" var="transfer">
				<tr  class="columndataauto<s:property value='#categoryCount' />" >
					<td colspan="3"><h3 class="transactionHeading">Template <s:property value="#categoryCount+1"/></h3></td>
				</tr>
				<tr>
					<td colspan="3"  align="center">
						<span id="transfers_<s:property value="#categoryCount" />_Error"></span>
					</td>
				</tr>
				<tr class="columndataauto<s:property value='#categoryCount' />">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				</tr>
				<tr class="columndataauto<s:property value='#categoryCount' />">
					<td class="sectionsubhead ltrow2_color" width="45%"><label for="FromAccount<s:property value="#categoryCount" />"><s:text name="jsp.default_217" /></label> <span class="required" title="required">*</span></td>
					<td class="sectionsubhead ltrow2_color" width="10%"></td>
					<td class="sectionsubhead ltrow2_color" width="45%"><label for="ToAccount<s:property value="#categoryCount" />"><s:text name="jsp.default_424" /></label> <span class="required" title="required">*</span></td>
				</tr>
				
					<input type="Hidden"
						name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].transferDestination"
						id="TransferDest<s:property value="#categoryCount" />"
						value="<s:property value='#transfer.transferDestination'/>">
					<tr>
						<td align="left" class="columndataauto<s:property value='#categoryCount' />" nowrap><s:if test="%{#transfer.fromAccountID!=null}">
							</s:if> <select class="txtbox" style="width: 30%"
							name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].fromAccountID"
							id="FromAccount<s:property value="#categoryCount" />"
							onChange="onAccountChangeCheck('from',<s:property value="#categoryCount" />);" aria-labelledby="FromAccount<s:property value="#categoryCount" />" aria-required="true">
								<s:if test="%{#transfer.fromAccountID!=null}">
									<option
								value="<s:property value='%{#transfer.fromAccountID}' />:<s:property value="#transfer.fromAccount.currencyCode" />"
								selected><s:property value="#transfer.fromAccount.accountDisplayText" />
							</option>
								</s:if>

						</select></td>
						<td
							class="columndataauto<s:property value='#categoryCount' />">&nbsp;</td>
						<td
							class="columndataauto<s:property value='#categoryCount' />" align="left" nowrap><s:if test="%{#transfer.toAccountID!=null}">
							</s:if> <select class="txtbox" style="width: 30%"
							name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].toAccountID"
							id="ToAccount<s:property value="#categoryCount" />"
							onChange="onAccountChangeCheck('from',<s:property value="#categoryCount" />);" aria-labelledby="ToAccount<s:property value="#categoryCount" />">
								<s:if test="%{#transfer.toAccountID!=null}">
									<option
								value="<s:property value='%{#transfer.toAccountID}' />:<s:property value="#transfer.toAccount.currencyCode" />"
								selected><s:property value="#transfer.toAccount.accountDisplayText" />
							</option>

								</s:if>
							</select></td>
						</tr>
						<tr class="columndataauto<s:property value='#categoryCount' />">
							<td><span id="transfers_<s:property value="#categoryCount" />_fromAccountIDError"></span></td>
							<td class="sectionsubhead ltrow2_color" width="10%"></td>
							<td><span id="transfers_<s:property value="#categoryCount" />_toAccountIDError"></span></td>
						</tr>	
						<tr class="columndataauto<s:property value='#categoryCount' />">
							<td class="sectionsubhead ltrow2_color">
								<span style="display:inline-block; width:135px;"><label for="Amount<s:property value='#categoryCount' />"><s:text name="jsp.default_43" /></label></span>
								<span id="CurrencyColumnHeader<s:property value='#categoryCount' />" style="display: none"><label for="CurrencySelect<s:property value='#categoryCount' />"><s:text name="jsp.default_125" /></label> 
								<span class="required">*</span></span>
							</td>
							<ffi:cinclude value1="${IsConsumerUser}" value2="true" operator="notEquals">
								<td class="sectionsubhead ltrow2_color" width="10%"></td>
								<td class="sectionsubhead ltrow2_color"><label for="status"><s:text name="jsp.default_388" /></label></td>
							</ffi:cinclude>	
						</tr>
						<tr>
							<td class="columndataauto<s:property value='#categoryCount' />" align="left" nowrap><input
							class="ui-widget-content ui-corner-all" type="text"
							name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].amount"
							size="10" maxlength="15" class="form_fields" style="width:120px"
							value="<s:if test="%{!#transfer.userAssignedAmountValue.isZero}"><s:property value='#transfer.userAssignedAmount'/></s:if>"
							id="Amount<s:property value='#categoryCount' />"
							onChange="calculateTotal();" aria-labelledby="Amount<s:property value='#categoryCount' />">&nbsp;
							<span
							id="CurrencyLabel<s:property value='#categoryCount' />"
							style="display: none">&nbsp;</span> <span
							id="CurrencySection<s:property value='#categoryCount' />"
							style="display: none"> <select class="txtbox"
								name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].userAssignedAmountFlagName"
								id="CurrencySelect<s:property value='#categoryCount' />"
								onChange="calculateTotal();" aria-labelledby="CurrencySelect<s:property value='#categoryCount' />">
									<option value="single" selected><s:text
											name="jsp.transfers_70" /></option>
							</select> <input type="hidden"
								name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].toAmount"
								id="OtherAmount<s:property value='#categoryCount' />"
								value="0.00">
							</span>	
							</td>
							<ffi:cinclude value1="${IsConsumerUser}" value2="true" operator="notEquals">
								<td class="columndataauto<s:property value='#categoryCount' />">&nbsp;</td>
								<td class="columndataauto<s:property value='#categoryCount' />" nowrap><s:if test="%{#transfer.statusName!=null}">
		                                  <s:property value="#transfer.statusName" /></s:if></td>
		                   </ffi:cinclude>               
                       </tr>
                       
                       <tr class="columndataauto<s:property value='#categoryCount' />">
							<td><span id="transfers_<s:property value="#categoryCount" />_amountError"></span>
								<span id="transfers_<s:property value="#categoryCount" />_toAmountError"></span>
								<span id="transfers_<s:property value="#categoryCount" />_userAssignedAmountError"></span>
							</td>
							<td><span id="transfers_<s:property value="#categoryCount" />_dateError"></span></td>
							<td>&nbsp;</td>
						</tr>
                       <tr class="columndataauto<s:property value='#categoryCount' />">
							<td class="sectionsubhead ltrow2_color"><label for="Memo<s:property value='#categoryCount' />"><s:text name="jsp.default_279" /></label></td>
							<td class="sectionsubhead ltrow2_color" width="10%"></td>
							<td class="sectionsubhead ltrow2_color"></td>
						</tr>
						
                       <tr>
                       <td
							class="columndataauto<s:property value='#categoryCount' />" align="left" valign="middle">
							<table border="0" cellpadding="0" cellspacing="0"
								width="100%">
								<tr>
									<td class="columndata ltrow2_color"><input
										class="ui-widget-content ui-corner-all" type="text"
										name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].memo"
										id="Memo<s:property value='#categoryCount' />" size="35"
										maxlength="100" class="form_fields"
										value="<s:property value='#transfer.memo'/>" aria-labelledby="Memo<s:property value='#categoryCount' />">&nbsp;&nbsp;&nbsp;&nbsp;
										<span id="Category<s:property value='#categoryCount' />"
										style="display: none"> <s:text
												name="jsp.default_88" />: <select class="txtbox"
											name="editTransferTemplateBatch.transfers[<s:property value="#categoryCount" />].transferCategory"
											id="CategorySelect<s:property value='#categoryCount' />"
											class="form_elements">
												<option value="0"><s:text
														name="jsp.default_445" /></option>
												<ffi:setProperty name="RegisterCategories"
													property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
												<ffi:setProperty name="RegisterCategories"
													property="FilterSortedBy" value="NAME" />
												<ffi:list collection="RegisterCategories"
													items="Category">
													<s:set var="Transfer" value="#transfer" scope="session" />

													<option
														value="<ffi:getProperty name='Category' property='Id'/>"
														<ffi:cinclude value1="${Transfer.REGISTER_CATEGORY_ID}" value2="${Category.Id}">selected</ffi:cinclude>><ffi:getProperty
															name="Category" property="Name" /></option>
													<ffi:setProperty name="RegisterCategoriesCopy"
														property="Filter"
														value="PARENT_CATEGORY=${Category.Id}" />
													<ffi:setProperty name="RegisterCategoriesCopy"
														property="FilterSortedBy" value="NAME" />
													<ffi:list collection="RegisterCategoriesCopy"
														items="Category1">

														<ffi:setProperty name="Compare" property="Value2"
															value="${Category1.Id}" />

														<option
															value="<ffi:getProperty name="Category1" property="Id"/>"
															<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;
															<ffi:getProperty name="Category" property="Name" />:
															<ffi:getProperty name="Category1" property="Name" /></option>
													</ffi:list>
												</ffi:list>
										</select>
									</span>
									</td>
								</tr>
							</table>
						</td>
						<td class="sectionsubhead ltrow2_color" width="10%"></td>
						<td class="columndataauto<s:property value='#categoryCount' />" align="right">

							<sj:a id="clearRow%{#categoryCount}" button="true" title="%{getText('Clear')}" onClick="return clearRow('%{#categoryCount}');">
									<s:text name="Clear" />
							</sj:a>	
								<ffi:cinclude value1="${IsConsumerUser}" value2="true" operator="notEquals">
								<s:if test="%{#transfer.trackingID!=null}">
									<ffi:setProperty name="nsTransferViewTemplateHistory"
										value="/cb/pages/jsp/transfers/viewTemplateHistoryAction_execute.action?srvrtId=${transfer.ID}&trackingId=${transfer.trackingID}&index=${categoryCount}&status=${transfer.statusName}"
										URLEncrypt="true" />
									<a class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'
										title='<s:text name="jsp.default_460" />' href='#'
										onClick="ns.transfer.viewTemplateHistory('<ffi:getProperty name="nsTransferViewTemplateHistory"/>')">
										<span class="ui-button-text"><s:text name="jsp.default_6" /></span>
									</a>

								</s:if>

								<s:else>
								</s:else>
							</ffi:cinclude>
						</td>
					</tr>
					<tr class="columndataauto<s:property value='#categoryCount' />">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<s:set var="categoryCount" value="%{#categoryCount+1}" />

				</s:iterator>
				<tr>
					<td colspan="3" align="right">
					<hr style="border:1px solid #b0b0b0; margin:10px 0" width="100%" height="1">

					<div class="deleteRow" style="display:inline-block"><sj:a id="deleteRowID"
							button="true">
							<s:text name="jsp.transfers_6" />
						</sj:a></div>
					<div class="addRow" style="display:inline-block"><sj:a id="addRowID"
							button="true">
							<s:text name="jsp.transfers_3" />
						</sj:a></div>
				</td>
				</tr>
				<tr class="ltrow_color">
					<td class="columndata ltrow2_color" align="center" colspan="8">
						<br> <sj:a id="cancelFormButtonOnInput" button="true" summaryDivId="summary" buttonType="cancel"
							onClickTopics="showSummary,cancelMultipleTemplatesForm">
							<s:text name="jsp.default_82" />
						</sj:a> <sj:a id="verifyMultipleTemplateSubmit"
							formIds="TransferMultipleEdit" targets="verifyDiv"
							button="true" validate="true"
							validateFunction="customValidation"
							onBeforeTopics="beforeVerifyTransferForm"
							onCompleteTopics="verifyTransferFormComplete"
							onClickTopics="onSubmitCheckTopics"
							onErrorTopics="errorVerifyTransferForm"
							onSuccessTopics="successVerifyTransferForm">
							<s:text name="jsp.default_395" />
						</sj:a>
					</td>
				</tr>
			</table></div></div></div>
			</div>
		</s:form>

	</div>
	</div> 
<sj:dialog id="viewMultiTransfersTemplateDialogID" cssClass="pmtTran_transferDialog" title="%{getText('jsp.transfers_88')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true"
showEffect="fold" hideEffect="clip" width="570">
</sj:dialog> 
<ffi:setProperty name="TransferAccounts" property="Filter" value="All" /> 
<ffi:removeProperty name="fromPortalPage" />
 <ffi:removeProperty name="IsConsumerUser" />