<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<!-- Check AppType of user -->
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="UserType" value="Corporate"/>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="UserType" value="Consumer"/>
</ffi:cinclude>

	<%-- Start: Clear session objects --%>
	<ffi:removeProperty name="LoadFromTransferTemplate"/>	
	<%-- End: Clear session objects --%>
	
	<div id="transferGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">
	    <div class="portlet-header">
			<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.transfers_112"/></h1>
		    <div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
							<s:text name="jsp.default_531"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
							<s:text name="jsp.default_530"/>
						</ffi:cinclude>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
						<s:iterator value="%{orderedTransferTabs}" var="tabName">
							<s:if test="%{#tabName == 'approving'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('approving')" >
									<s:text name="jsp.default_531"/>
								</span>
							</s:if>
							<s:elseif test="%{#tabName == 'pending'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('pending')">
									<s:text name="jsp.default_530"/>
								</span>
							</s:elseif>
							<s:elseif test="%{#tabName == 'completed'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('completed')">
									<s:text name="jsp.default_518"/>
								</span>
							</s:elseif>
						</s:iterator>
					</div>
				</div>
			</div>
	    </div>
	    
	    <% 
			String pendingApprovalGridSummaryCssCls =""; 
			String pendingGridSummaryCssCls ="hidden";
		%>
		
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<%
				pendingApprovalGridSummaryCssCls = "hidden";
				pendingGridSummaryCssCls = "";
			%>
		</ffi:cinclude>
	    
	    <div class="portlet-content">
	    	<div class="searchDashboardRenderCls">
				<s:include value="/pages/jsp/transfers/inc/transferSearchCriteria.jsp"/>
			</div>
			<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="approvingSummaryGrid" gridId="pendingApprovalTransfersID" class="gridSummaryContainerCls <%=pendingApprovalGridSummaryCssCls%>" >
					<s:action namespace="/pages/jsp/transfers" name="initTransfersSummaryGridAction_getPandingApprovalGrid" executeResult="true"/>
				</div>
				<div id="pendingSummaryGrid" gridId="pendingTransfersGridId" class="gridSummaryContainerCls <%=pendingGridSummaryCssCls%>" >
					<s:action namespace="/pages/jsp/transfers" name="initTransfersSummaryGridAction_getPandingGrid" executeResult="true"/>
				</div>
				<div id="completedSummaryGrid" gridId="completedTransfersGrid" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/transfers" name="initTransfersSummaryGridAction_getCompletedGrid" executeResult="true"/>
				</div>
			</div>
	    </div>
		
		<div id="transferSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('transferGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		
	</div>

	<div id="view_delete_transfer_dialog"></div>
	
	<s:url id="calendarURL" value="/pages/jsp/calendar/index.jsp" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		<s:param name="moduleName" value="%{'TransferCalendar'}"></s:param>
	</s:url>
	<sj:a
		id="openCalendar"
		href="%{calendarURL}"
		targets="summary"
		onClickTopics="closeTransferTabs" 
		onSuccessTopics="calendarLoadedSuccess"
		onCompleteTopics="onCompleteTabifyAndFocusCalendarTopic"
		cssClass="titleBarBtn hidden"
		>
		<s:text name="jsp.transfers_2"/><span class="ui-icon ui-icon-calendar floatleft"></span>
	</sj:a>
	    
		<%-- Set following hidden field to identify the module to which tab belong to, this is required in case of save/load tab settings --%>
		<input type="hidden" id="getTransID" value="<s:property value="%{moduleID}"/>" />
		<s:hidden name="transfersSummaryType" value="PendingTransfers" id="transfersSummaryTypeID"/>

<script>
	//Initialize portlet with settings & calendar icon
	ns.common.initializePortlet("transferGridTabs", false, true, function( ) {
	    $("#openCalendar").click();
	});
</script>
