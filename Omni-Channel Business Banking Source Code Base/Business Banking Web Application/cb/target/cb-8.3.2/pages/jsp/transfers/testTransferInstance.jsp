<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.ffusion.test.RecTransferInstanceTest, com.ffusion.beans.SecureUser, com.sap.banking.wires.test.RecWireTransferInstanceTest"%>
<%@ page import="java.io.*,java.util.*,javax.servlet.*,javax.servlet.http.*" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test Transfer Page</title>
<script type="text/javascript">

	function submitForm() {
		document.getElementById("testFormHidden01").value = "submitted";
		document.getElementById("testForm").submit();
		return true;
	}
</script>
</head>
<body>

<%!
	private String start(javax.servlet.http.HttpServletRequest request) {
		
		String result = "";
		try {
			//int instanceNum = 0;
			String type = request.getParameter("transTypeName");
			String recId = request.getParameter("recIdName");
			//String srvId = request.getParameter("srvIdName");
			//String instance = request.getParameter("instanceNumName");
			/* if (instance != null && !instance.isEmpty()) {
				instanceNum = Integer.parseInt(instance);
			} */
			String action = request.getParameter("actionTypeName");
			String amount = request.getParameter("amountName");
			String date = request.getParameter("dueDateName");
			
			System.out.println("Type: " + type);
			System.out.println("Instance ID: " + recId);
			//System.out.println("Actual ID: " + srvId);
			//System.out.println("Instance Num: " + instance);
			System.out.println("Action: " + action);
			System.out.println("Amount: " + amount);
			System.out.println("Date: " + date);

			if (type != null && type.equals("Select")) {
				result = "Please select Transfer Type.";
			} else if (recId == null || recId.trim().isEmpty()) {
				result = "Please provide Instance Id.";
			} /* else if ((srvId == null || srvId.trim().isEmpty())&& instanceNum == 0) {
				result = "Please provide either valid Instance Num or Actual Id.";
			} */
			SecureUser sUser = this.createSecureUser(request);
			//***modifyTransferInstance(SecureUser sUser, String instanceId, String id, String recID, int instanceNum, String amount, String date)
			//***cancelTransferInstance(SecureUser sUser, String instanceId, String id, String recID, int instanceNum)
			if (!result.isEmpty()) {
				return result;
			}
			
			if (action.equals("ModifyAction")) {
				if (type.equals("Payment")) {
					RecTransferInstanceTest.modifyPayment(sUser, recId, amount, date);
				} else if (type.equals("Transfer")) {
					RecTransferInstanceTest.modifyTransferInstance(sUser, recId, amount, date);
				} else if (type.equals("ACHBatchTransfer")) {
					RecTransferInstanceTest.modifyACHBatchInstance(sUser, recId, amount, date);
				} else if (type.equals("WireTransfer")) {
					RecWireTransferInstanceTest.modifyWireTransferInstance(sUser, recId, amount, date);
				}
			} else if (action.equals("CancelAction")) {
				if (type.equals("Payment")) {
					RecTransferInstanceTest.cancelPayment(sUser, recId);
				} else if (type.equals("Transfer")) {
					RecTransferInstanceTest.cancelTransferInstance(sUser, recId);
				} else if (type.equals("ACHBatchTransfer")) {
					RecTransferInstanceTest.cancelACHBatchInstance(sUser, recId);
				} else if (type.equals("WireTransfer")) {
					RecWireTransferInstanceTest.cancelWireTransferInstance(sUser, recId);
				}
			}
			//RecTransferInstanceTest.modifyACHBatchInstance(this.createSecureUser(request), "RECID=00000000000000000000000000003001,-3", null, "00000000000000000000000000003001", -3, "52", "10/14/2014");
			//RecTransferInstanceTest.cancelACHBatchInstance(this.createSecureUser(request), "RECID=00000000000000000000000000000001,ID=00000000000000000000000000012001","00000000000000000000000000012001", "00000000000000000000000000000001", 3);
			result = "Success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "Failure";
		}
		return result;
	}
%>
	
<%!
	private SecureUser createSecureUser(javax.servlet.http.HttpServletRequest request) {
		
		/*
		 * 	userName=jtech
			password=Pass1234
			bankID=1000
			profileID=0
			businessID=B67890
			appType=Business
		 */

		SecureUser secureUser = (SecureUser)request.getSession().getAttribute("SecureUser");
		return secureUser;

	}
%>
<%!
	private String post(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
		String hiddenParam = req.getParameter("testFormHidden01Name");
		System.out.println("sputlam - result 10: " + hiddenParam);
		String status = "";
		if (hiddenParam != null) {
			status = this.start(req);
			System.out.println("sputlam - result 11: " + status);
		}
		return status;
//		PrintWriter out = resp.getWriter();
//		out.println(status);
	}
%>

<form id="testForm" name="testFormName" action="testTransferInstance.jsp" method="post">
<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
	<input type="hidden" id="testFormHidden01" name="testFormHidden01Name" value=""/>
	<table width="100%" >
	<tr>
	<td width="20%">Transfer Type: </td>
	<td><select id="transType" name="transTypeName">
	<option value="Select" selected="selected">Select</option>
	<option value="ACHBatchTransfer">ACH Batch Transfer</option>
	<option value="Payment">Bill Payment</option>
	<option value="Transfer">External/Internal Transfer</option>
	<option value="WireTransfer">Wire Transfer</option>
	</select></td>
	</tr>
	<tr>
	<td width="20%">Instance Id: </td>
	<td><input type="text" id="recId" name="recIdName"  style="width:600px;" /></td>
	</tr>
	<!--<tr>
	<td width="20%">Instance Num: </td>
	<td><input type="text" id="instanceNum" name="instanceNumName" /></td>
	</tr>
	<tr>
	<td width="20%">Actual Id: </td>
	<td><input type="text" id="srvId" name="srvIdName" style="width:600px;"/></td>
	</tr>-->
	<tr>
	<td width="20%">Amount: </td>
	<td><input type="text" id="amount" name="amountName" /></td>
	</tr>
	<tr>
	<td width="20%">Due Date (mm/dd/yyyy): </td>
	<td><input type="text" id="dueDate" name="dueDateName" style="width:200px;"/></td>
	</tr>
	<tr>
	<td width="20%">Action Type: </td>
	<td><select id="actionType" name="actionTypeName">
	<option value="ModifyAction" selected="selected">Modify Transfer</option>
	<option value="CancelAction">Cancel Transfer</option>
	</select></td>
	</tr>
	</table>
	<br>
	<p>Note: For Cancel Action: Prepare 'Actual Id' using this format - <strong>RECID={recId},ID={srvId} or RECID={recId},instanceNum</strong> (for virtual instance cancellation).<br>
	Replace '{...}' from the database value</p>
	<br><br>
	<p>Result: <strong><%= this.post(request, response)%></strong></p><br>
	<input type="submit" name="submitButton" value="Submit" onclick="submitForm()"/>
</form>

</body>
</html>
