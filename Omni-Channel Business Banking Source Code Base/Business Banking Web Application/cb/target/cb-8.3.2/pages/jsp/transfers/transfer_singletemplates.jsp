<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_accounttransferSingleTemplates" />

	<ffi:setGridURL grid="GRID_transferSingletemplates" name="ViewURL" url="/cb/pages/jsp/transfers/viewTransferTemplateAction.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID"  />
	<ffi:setGridURL grid="GRID_transferSingletemplates" name="LoadURL" url="/cb/pages/jsp/transfers/AddTransferAction_initTemplateLoad.action?LoadFromTransferTemplate={0}:{1}:{2}&istemplateflow='false" parm0="ID" parm1="RecTransferID" parm2="TransferType" />
	<ffi:setGridURL grid="GRID_transferSingletemplates" name="EditURL" url="/cb/pages/jsp/transfers/EditTransferTemplateAction_input.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID" />
	<ffi:setGridURL grid="GRID_transferSingletemplates" name="DeleteURL" url="/cb/pages/jsp/transfers/deleteTransferTemplateAction_init.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID"  />

	<ffi:setProperty name="tempURL" value="/pages/jsp/transfers/getSingleTransferTemplates.action?GridURLs=GRID_transferSingletemplates" URLEncrypt="true"/>
    <s:url id="singleTransfersTemplatesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
			id="singleTransfersTemplatesGridID"
			caption=""
			sortable="true"
			dataType="json"
			href="%{singleTransfersTemplatesUrl}"
			pager="true"
			gridModel="gridModel"
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			sortname="TEMPLATE_NAME"
			sortorder="asc"
			onGridCompleteTopics="addGridControlsEvents,singleTemplatesGridCompleteEvents"
			>

			<sjg:gridColumn name="templateName" index="TEMPLATE_NAME" title="%{getText('jsp.default_283')}" sortable="true"/>
			<sjg:gridColumn name="type" index="TRANSFER_DESTINATION" title="%{getText('jsp.default_444')}" formatter="ns.transfer.customTypeColumnForSingleTemplate" sortable="true" width="90"/>

			<sjg:gridColumn name="combinedAccount" index="Accounts" title="Accounts" sortable="false" width="120" formatter="ns.transfer.formatCombinedAccount"/>
			<sjg:gridColumn name="fromAccount.accountDisplayText" index="FROMACCOUNTID" title="%{getText('jsp.default_217')}" sortable="true" width="150" hidden="true"/>
			<sjg:gridColumn name="toAccount.accountDisplayText" index="TOACCOUNTID" title="%{getText('jsp.default_424')}" sortable="true" width="150" hidden="true"/>

			<sjg:gridColumn name="amount" index="AMOUNT" title="%{getText('jsp.default_43')}" sortable="true" formatter="ns.transfer.formatCombinedAmount" width="70"/>
			<sjg:gridColumn name="statusName" index="STATUSNAME" title="%{getText('jsp.default_388')}" sortable="true" width="60"/>


			<sjg:gridColumn name="map.FrequencyValue" index="mapFrequencyValue" title="%{getText('jsp.default_215')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.transfer.formatSingleTransferTemplatesActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

			<sjg:gridColumn name="trackingID" index="trackingID" title="%{getText('jsp.default_435')}" width="80" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="fromAccount.displayText" index="fromAccountDisplayText" title="%{getText('jsp.transfers_48')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="fromAccount.currencyCode" index="fromAccountCurrencyCode" title="%{getText('jsp.transfers_49')}" hidden="true" hidedlg="true"/>

        <sjg:gridColumn name="approverName" index="approverName" title="" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="userName" index="userName" title="" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="rejectReason" index="rejectReason" title="" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="toAccount.displayText" index="toAccountDisplayText" title="%{getText('jsp.transfers_79')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="toAccount.currencyCode" index="toAccountCurrencyCode" title="%{getText('jsp.transfers_78')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="transferDestination" index="transferDestination" title="%{getText('jsp.transfers_85')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="transferType" index="transferType" title="%{getText('jsp.transfers_86')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="AMOUNT" title="%{getText('jsp.default_489')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="amountValue.currencyCode" index="amountValueCurrencyCode" title="%{getText('jsp.transfers_47')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canLoad" index="canLoad" title="%{getText('jsp.default_81')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canDelete" index="canDelete" title="%{getText('jsp.default_79')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canEdit" index="canEdit" title="%{getText('jsp.default_80')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="isAmountEstimated" index="isAmountEstimated" title="%{getText('jsp.transfers_54')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="isToAmountEstimated" index="isToAmountEstimated" title="%{getText('jsp.transfers_55')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="displayTextRoA" index="displayTextRoA" title="%{getText('jsp.default_359')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="resultOfApproval" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="status" index="status" title="%{getText('jsp.transfers_73')}" sortable="true" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.transfer.formatApproversInfo" />
			<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.LoadURL" index="LoadURL" title="%{getText('jsp.default_265')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>

	</sjg:grid>

	<script type="text/javascript">
		$("#singleTransfersTemplatesGridID").jqGrid('setColProp','ID',{title:false});
	</script>
