<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
 <%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="manage-external-wait.jsp"/>
<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="equals"> <%-- small business user --%>
	<ffi:object name="com.ffusion.tasks.exttransfers.GetExtTransferAccounts" id="GetExtTransferAccounts"  scope="session"/>
		<ffi:setProperty name="GetExtTransferAccounts" property="BusinessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetExtTransferAccounts" />
	<ffi:object name="com.ffusion.tasks.exttransfers.ModifyExtTransferAccounts" id="ModifyExtTransferAccounts"  scope="session"/>
		<ffi:setProperty name="ModifyExtTransferAccounts" property="Initialize" value="true"/>
		<ffi:setProperty name="ModifyExtTransferAccounts" property="CustID" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="ModifyExtTransferAccounts" property="SourceAccounts" value="BankingAccounts" />
</ffi:cinclude>
<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="notEquals"> <%-- consumer user --%>
	<ffi:object name="com.ffusion.tasks.exttransfers.GetExtTransferAccounts" id="GetExtTransferAccounts"  scope="session"/>
		<ffi:setProperty name="GetExtTransferAccounts" property="UserId" value="${SecureUser.ProfileID}"/>
	<ffi:process name="GetExtTransferAccounts" />

	<ffi:object name="com.ffusion.tasks.exttransfers.ModifyExtTransferAccounts" id="ModifyExtTransferAccounts"  scope="session"/>
		<ffi:setProperty name="ModifyExtTransferAccounts" property="Initialize" value="true"/>
		<ffi:setProperty name="ModifyExtTransferAccounts" property="CustID" value="${SecureUser.ProfileID}"/>
	<%-- check if this is a secondary user --%>
	<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
			<ffi:setProperty name="ModifyExtTransferAccounts" property="SourceAccounts" value="PrimaryUserAccounts" />
	</ffi:cinclude>

	<%-- only do this if this is a primary user --%>
	<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="equals">
		<ffi:setProperty name="ModifyExtTransferAccounts" property="SourceAccounts" value="BankingAccounts" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:process name="ModifyExtTransferAccounts" />
