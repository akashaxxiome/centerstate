<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.beans.banking.TransferStatus"%>
<%@ page import="java.lang.Integer"%>
<%@ page import="com.ffusion.beans.banking.TransferDefines"%>
<script type="text/javascript">
	function loadTemplate(){
		var id = $('#id').val();
		var recId = $('#recId').val();
		if($.trim(recId) ==""){
			recId = id;
		}
		var templateType = $('#transferType').val();
		var urlString = "/cb/pages/jsp/transfers/AddTransferAction_initTemplateLoad.action?LoadFromTransferTemplate="+id+":"+recId+":"+templateType;
		ns.transfer.loadSingleTransferTemplate(urlString);
	}
	
	$(document).ready(function(){
		// if status is not active disable load template
		var isValid = $('#isValid').val();
		if(isValid=='true'){
			$('#templateForMultipleTransfer').show();
			$('#disabledTemplateSave').hide();
		}else{
			$('#templateForMultipleTransfer').hide();
			$('#disabledTemplateSave').show();
		}
	});
	
</script>
<s:if test="%{#request.flowType=='NewTransfer'}">
	<s:set var="transferBatch" value="#session.transferTemplateBatch" />
</s:if>
<s:else>
	<s:set var="transferBatch" value="#session.editTransferTemplateBatch" />
</s:else>


<ffi:help id="payments_transferTemplateConfirm" />


<s:set var="hasMultiCurrency"
	value="%{''+#transferBatch.hasMultiCurrency}" />
<s:set var="batchTotalZero" value="#transferBatch.amountValue.isZero" />
<div class="leftPaneWrapper" role="form" aria-labelledby"templateSummary">

		<div class="leftPaneInnerWrapper">
			<div class="header"><h2 id="templateSummary"><s:text name="jsp.default.templates.summary" /></h2></div>
			<div class="summaryBlock"  style="padding: 15px 10px;">
				<div style="width:100%" class="floatleft">
					<span id="confirmMultipleTransfer_confirmTemplateTitleLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_416" />:</span>
					<span id="" class="inlineSection floatleft labelValue"><s:property value="%{#transferBatch.templateName}" /></span>
				</div>
			</div>
		</div>

		<s:if test="%{!#batchTotalZero}">
			<div class="leftPaneInnerWrapper totalAmountWrapper">				
				<div id="confirmMultipleTransfer_totalLabel" style="display:inline-block">
					<span id="TotalLabel"><s:text name="jsp.default_431" />:</span>
				</div>
				<div id="confirmMultipleTransfer_totalValue" style="display:inline-block">
					<span id="Total"> <s:property
							value="#transferBatch.amountValue.currencyStringNoSymbol" /> <s:property
							value="#transferBatch.amountValue.currencyCode" />
					</span>
				</div>
			</div>	
		</s:if>
</div>

<div class="confirmPageDetails">

	<s:set var="rowCount" value="0" />
	<s:iterator var="transfer" value="#transferBatch.transfers">
		<s:if test="%{#transfer.fromAccount!=null}">
			<s:if test="%{#transfer.statusName!='Cancelled'}">
				<h3 class="transactionHeading">Transfer<s:property value="#rowCount+1"/></h3>
				<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
				<div class="blockContent">
						
							<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
							<s:set var="TRS_INSUFFICIENT_FUNDS"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
							<s:set var="TRS_BPW_LIMITCHECK_FAILED"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
							<s:set var="TRS_SCHEDULED"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
							
							<s:set var="TRS_PENDING_APPROVAL"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
								
							<s:if test="%{#TRS_FAILED_TO_TRANSFER}">
								<s:set var="HighlightStatus" value="true" />
							</s:if> 
							<s:if test="%{#TRS_INSUFFICIENT_FUNDS}">
								<s:set var="HighlightStatus" value="true" />
							</s:if> 
							<s:if test="%{#TRS_BPW_LIMITCHECK_FAILED}">
								<s:set var="HighlightStatus" value="true" />
							</s:if>
							<s:if test="#HighlightStatus=='true'">
								<div class="blockRow failed"><span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
									<input type="hidden" id="isValid" value="false"/>
									<span class="sapUiIconCls icon-decline "></span> <span class="required"><s:property value="#transfer.statusName" /></span>
								</span><span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_92" /></span></div>
							</s:if>
							<s:else>
								<s:if test="%{#TRS_PENDING_APPROVAL}">
								<div class="blockRow pending"><span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
									<input type="hidden" id="isValid" value="false"/>
									<span class="sapUiIconCls icon-pending "></span>
									<s:property value="%{#transfer.statusName}" />
								</span><span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_92" /></span></div>
								</s:if>
								<s:else>
									<div class="blockRow completed"><span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
										<input type="hidden" id="isValid" value="true"/>
										<span class="sapUiIconCls icon-accept"></span>
										<s:property value="%{#transfer.statusName}" />
									</span><span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_92" /></span></div>
								</s:else>  
							</s:else>
				</div>
				<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
				<div class="blockContent">
					<div class="blockRow">
						<div class="inlineBlock" style="width: 50%">
							<span id="confirmMultipleTransfer_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217" />: </span>
							<span id="confirmMultipleTransfer_fromAccountValueId<s:property value='#rowCount'/>">
								<s:if
									test="%{#transfer.fromAccount!=null}">
									<s:property value="#transfer.fromAccount.accountDisplayText"/>
								</s:if>
							</span>
						</div>
						<div class="inlineBlock">
							<s:if test="%{#hasMultiCurrency=='false'}">
								<span id="confirmMultipleTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43" />: </span>
							</s:if>
							<s:if test="%{#hasMultiCurrency=='true'}">
								<span id="confirmMultipleTransfer_fromAmountLabelId" class="sectionLabel"><s:text name="jsp.default_489" />: </span>
							</s:if>
							<s:if test="%{#hasMultiCurrency=='false'}">
								<span id="confirmMultipleTransfer_amountValueId<s:property value='#rowCount'/>">
									<s:property value="#transfer.amountValue.currencyStringNoSymbol" /> 
									<s:property value="#transfer.amountValue.currencyCode" />&nbsp;&nbsp;&nbsp;&nbsp;
								</span>
							</s:if>
							<s:if test="%{#hasMultiCurrency=='true'}">
								<span id="confirmMultipleTransfer_fromAmountValueId<s:property value='#rowCount'/>">
									<s:if test="%{#transfer.isAmountEstimated}">&#8776;</s:if>
									<s:if test="%{#transfer.amountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
									<s:else>
										<s:property
											value="#transfer.amountValue.currencyStringNoSymbol" />
										<s:property value="#transfer.amountValue.currencyCode" />
                                    </s:else>
								</span>
							</s:if>
						</div>
					</div>
					<div class="blockRow">
						<div class="inlineBlock" style="width: 50%">
							<span id="confirmMultipleTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424" />: </span>
							<span id="confirmMultipleTransfer_toAccountValueId<s:property value='#rowCount'/>">
									<s:if test="%{#transfer.toAccount!=null}">
												<s:property value="#transfer.toAccount.accountDisplayText"/>
									</s:if>
								</span>
						</div>
						<div class="inlineBlock">
							<s:if test="%{#hasMultiCurrency=='true'}">
								<span id="confirmMultipleTransfer_toAmountLabelId" class="sectionLabel"><s:text name="jsp.default_512" />: </span>
								<s:if test="%{#hasMultiCurrency=='true'}">
								<span id="confirmMultipleTransfer_toAmountValueId<s:property value='#rowCount'/>">
									<s:if test="%{#transfer.isToAmountEstimated}">&#8776;</s:if>
									<s:if test="%{#transfer.toAmountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
									<s:else>
										<s:property value="#transfer.toAmountValue.currencyStringNoSymbol" />
										<s:property value="#transfer.toAmountValue.currencyCode" />
                                    </s:else>
								</span>
							</s:if>
							</s:if>
						</div>
					</div>
					<div class="blockRow">
						<s:if test="%{#hasMultiCurrency=='false'}">
								<span id="confirmMultipleTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" /></span>
							</s:if>
							<s:if test="%{#hasMultiCurrency=='true'}">
								<span id="confirmMultipleTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" /></span>
							</s:if>
							<span id="confirmMultipleTransfer_memoValueId<s:property value='#rowCount'/>"><s:property value="#transfer.memo" /></span>
					</div>
				</div>	
			</s:if>
		</s:if>
	<s:set var="rowCount" value="%{#rowCount + 1}" />
	</s:iterator>
<s:if test="%{#hasMultiCurrency=='true'}">
	<div class="mltiCurrenceyMessage">
		<span class="required">&#8776; <s:text name="jsp.default_241" /></span>
	</div>
</s:if>
<div class="btn-row" id="confirmMultipleTransfer_confirmDoneBtn">
	<sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,cancelTemplatesForm"> <s:text name="jsp.default_175" /></sj:a>
</div>
</div>
