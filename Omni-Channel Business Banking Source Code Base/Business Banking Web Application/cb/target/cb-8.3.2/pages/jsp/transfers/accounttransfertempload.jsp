<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script language="JavaScript" type="text/javascript">
<!--

$(function(){
	$("#SingleTransferTemplateList").lookupbox({
		"source":"/cb/pages/jsp/transfers/TransferTemplatesLookupBoxAction.action",
		"controlType":"server",
		"size":"30",
		"module":"singletransferTemplates",
	});

    var $textbox = $('#SingleTransferTemplateList').next('input');
	$textbox.attr('size', '20');
	
});

	var selectedvalue = $('#SingleTransferTemplateList').val();

	function enableLoadButton(selectedvalue)
	{
			$('#loadSingleTransferTemplateSubmit').trigger('click');
	}
	function checkTemplateType()
	{
	    var tempSel = document.forms.TemplateName.LoadFromTransferTemplate;
	    var selVal = tempSel.options[tempSel.selectedIndex].value;
	    if (selVal.length > 6 && selVal.substring(0,6) == "MULTI_")
	        document.forms.TemplateName.action = "accounttransfermultiplenew-wait.jsp";
	
	    document.forms.TemplateName.submit();
	}

// --></script>
        	    <ffi:removeProperty name="LoadTemplateFromTransfer"/>
        	    <s:url id="formUrl" value="/pages/jsp/transfers/accounttransferform.jsp"/>
        	           
                <s:form  id="LoadSingleTransferTemplateForm" name="LoadSingleTransferTemplateForm" method="post" action="/pages/jsp/transfers/AddTransferAction_initTemplateLoad.action" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

							<div class="selectBoxHolder">
								<select class="txtbox" id="SingleTransferTemplateList" name="LoadFromTransferTemplate"  onChange="enableLoadButton(this.value)">
								<option
									value="<s:property value="#session.newTransfer.ID" />"
									selected><s:property value="#session.newTransfer.templateName" />
								</option>
								
								</select>
							</div>

		               <sj:a 
							id="loadSingleTransferTemplateSubmit"
							targets="singleTransferForm"
							formIds="LoadSingleTransferTemplateForm"
		                    button="true" 
		                    onBeforeTopics="beforeLoadSingleTransferTemplate"
		                    onSuccessTopics="loadSingleTransferTemplateSuccess"
		                    onErrorTopics="errorLoadSingleTransferTemplate"
							templatetype="single"
		                    onCompleteTopics="loadSingleTransferTemplateComplete"
		                    cssStyle="display:none; margin-top:5px;"
		                	><s:text name="jsp.default_263" />
		                </sj:a> 
                </s:form>
