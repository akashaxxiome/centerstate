<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.ffusion.tasks.banking.Task"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


<script>
		$.subscribe('onSubmitCheckTopics', function(event,data) {
			onSubmitCheck(false);
		});
</script>
    <ffi:cinclude value1="${templateEdit}" value2="true">
		<span id="PageHeading" style="display:none;"><s:text name="jsp.transfers_15"/></span>
		<span class="shortcutPathClass" style="display:none;">pmtTran_transfer,templatesLink,addMultipleTemplateLink</span>
		<span class="shortcutEntitlementClass" style="display:none;">Transfers</span>
	</ffi:cinclude>
    <ffi:cinclude value1="${templateEdit}" value2="true" operator="notEquals">
		<span id="PageHeading" style="display:none;"><s:text name="jsp.transfers_14"/></span>
		<span class="shortcutPathClass" style="display:none;">pmtTran_transfer,transfersLink,addMultipleTransferLink</span>
		<span class="shortcutEntitlementClass" style="display:none;">Transfers</span>
	</ffi:cinclude>
	

	<%
		session.setAttribute("LoadTemplateFromTransfer", request.getParameter("LoadTemplateFromTransfer"));
		session.setAttribute("LoadFromTransferTemplate", request.getParameter("LoadFromTransferTemplate"));
		String templateEdit = request.getParameter("templateEdit");
		if( templateEdit != null && templateEdit.length() > 0 ){
			session.setAttribute("templateEdit", templateEdit);
		}
		String extAcctSelected = request.getParameter("ExtAcctSelected");
		if( extAcctSelected != null ){
			session.setAttribute("ExtAcctSelected", extAcctSelected);
		}
		
		String noCreate = request.getParameter("NoCreate");
		if( noCreate != null ){
			session.setAttribute("NoCreate", noCreate);
		}
	%>
	<ffi:cinclude value1="${NoCreate}" value2="" operator="equals">	
<ffi:cinclude value1="${LoadFromTransferTemplate}" value2="" operator="notEquals">
    <%-- Remove MULTI_ prefix from template ID --%>
    <ffi:object id="StringUtil" name="com.ffusion.beans.util.StringUtil" scope="request"/>
    <ffi:setProperty name="StringUtil" property="Value1" value="${LoadFromTransferTemplate}"/>
    <ffi:setProperty name="StringUtil" property="Range" value="6"/>

    <ffi:setProperty name="SetFundsTran" property="ID" value="${StringUtil.Substring}"/>
    <ffi:setProperty name="SetFundsTran" property="Name" value="TransferTemplates"/>
    <ffi:setProperty name="SetFundsTran" property="BeanSessionName" value="TransferBatch"/>
    <ffi:process name="SetFundsTran"/>

      <% String tTrackingId = ""; %>
       <% String tSrvrtId = ""; %>

     
    <ffi:setProperty name="TransferBatch" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:list collection="TransferBatch.Transfers" items="Transfer">
        <ffi:setProperty name="Transfer" property="Date" value="${GetCurrentDate.Date}"/>
        <ffi:cinclude value1="${Transfer.TransferDestination}" value2="<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>">
            <ffi:setProperty name="ExtAcctSelected" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${Transfer.TransferDestination}" value2="<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>">
            <ffi:setProperty name="ExtAcctSelected" value="true"/>
        </ffi:cinclude>
         <ffi:getProperty name="Transfer" property="ID" assignTo="tSrvrtId"/>
         <ffi:getProperty name="Transfer" property="TrackingID"  assignTo="tTrackingId" />        
         <ffi:cinclude value1="${templateEdit}" value2="true">
		<ffi:setProperty name="Transfer" property="TransferType" value="<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_TYPE_TEMPLATE %>"/>	                	
	 </ffi:cinclude>
    </ffi:list>
</ffi:cinclude>
	<ffi:object id="AddTransferBatch" name="com.ffusion.tasks.banking.AddTransferBatch" scope="session"/>


   <ffi:setProperty name="AddTransferBatch" property="Locale" value="${UserLocale.Locale}" />
   <ffi:setProperty name="AddTransferBatch" property="Initialize" value="true" />
   <ffi:setProperty name="AddTransferBatch" property="Validate" value="" />
   <ffi:setProperty name="AddTransferBatch" property="Process" value="false" />
   <ffi:setProperty name="AddTransferBatch" property="MinimumAmount" value="0.01" />
   <ffi:setProperty name="AddTransferBatch" property="DateFormat" value="${UserLocale.DateFormat}" />
   <ffi:setProperty name="AddTransferBatch" property="SubmitDate" value="${GetCurrentDate.Date}" />
   <ffi:setProperty name="AddTransferBatch" property="BatchType" value="<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_BATCH_TYPE_BATCH %>" />
   <ffi:cinclude value1="${LoadFromTransferTemplate}" value2="" operator="notEquals">
        <ffi:setProperty name="AddTransferBatch" property="UseTemplate" value="true" />
   </ffi:cinclude>
<ffi:process name="AddTransferBatch" />

</ffi:cinclude>

<%-- Remove session attribute added to identify File upload operation by user, as this is new multiple transfer request --%>
<ffi:removeProperty name="<%=Task.TRANSFER_FILE_UPLOAD%>"/>

<s:include value="/pages/jsp/transfers/accounttransfermultiplenewform.jsp" />