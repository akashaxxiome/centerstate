<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<HTML>
<HEAD>

<ffi:object id="AuditLogout" name="com.ffusion.tasks.user.AuditLogout" scope="session"/>
<ffi:setProperty name="AuditLogout" property="TimedOut" value="${TimedOut}"/>
<ffi:process name="AuditLogout"/>

<script type="text/javascript">
	if(window.parent.setPageUnloadHandledFlag){
		window.parent.setPageUnloadHandledFlag();
	}
</script>
	<s:if test="%{#session.SecureUser.Agent == null}">
		<s:if test="%{#parameters.TimedOut}">
			<s:set var="continueJsp" value="%{'/pages/jsp-ns/logout.jsp'}" scope="page"/>
			<s:url var="continueUrl" value="%{#attr.continueJsp}" escapeAmp="false">
				<s:param name="request_locale" value="#session.UserLocale.locale"/>
				<s:param name="themeName" value="#session.themeName"/>
				<s:param name="LoginAppType" value="#session.LoginAppType"/>
				<s:param name="timedout" value="true"/>
			</s:url>
		</s:if>
		<s:else>
			<s:set var="continueJsp" value="%{'/pages/jsp-ns/logout.jsp'}" scope="page"/>
			
			<s:url var="continueUrl" value="%{#attr.continueJsp}" escapeAmp="false">
				<s:param name="request_locale" value="#session.UserLocale.locale"/>
				<s:param name="themeName" value="#session.themeName"/>
				<s:param name="LoginAppType" value="#session.LoginAppType"/>
			</s:url>
		</s:else>
		
		<s:if test="%{#parameters.TimedOut}">
			<script>window.parent.location = '<s:property value="%{continueUrl}" escape="false"/>';</script>
		</s:if>
		<s:else>
			<script>window.parent.location.replace('<s:property value="%{continueUrl}" escape="false"/>');</script>
		</s:else>
		<% session.invalidate(); %>
	</s:if>
	<s:elseif test="%{#session.OBONewWindow}">
		<script type="text/javascript">window.parent.close();</script>
		<% session.invalidate(); %>
	</s:elseif>
	<s:else>
		<ffi:object id="RestoreSession" name="com.ffusion.tasks.obo.OBORestoreArchivedSession" scope="session"/>
		<ffi:process name="RestoreSession"/>
		<s:url var="continueUrl" value="%{#session.OBOReturnURL}" escapeAmp="false">
			<s:param name="request_locale" value="#parameter.request_locale"/>
		</s:url>
		<script>window.parent.location.replace('<s:property value="%{continueUrl}" escape="false"/>');</script>
	</s:else>
</HEAD>
</HTML>
