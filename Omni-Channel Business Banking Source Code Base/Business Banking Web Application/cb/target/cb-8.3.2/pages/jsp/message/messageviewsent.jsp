<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:help id="messages_messageviewsent"/>
<ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper">
			<div class="header">Message Summary</div>
			<div class="leftPaneInnerBox summaryBlock">
				<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.message_9"/></span>
					<span class="inlineSection floatleft labelValue" id="fromInViewMsg"><ffi:getProperty name="Message" property="ToName"/></span>
				</div>
				<div class="marginTop10 clearBoth floatleft" style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.message_16"/></span>
					<span class="inlineSection floatleft labelValue" id="ReceivedInViewMsg"><ffi:getProperty name="Message" property="DateValue"/></span>
				</div>
			</div>
		</div>
</div>
<div class="rightPaneWrapper w71">
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><s:text name="jsp.default_516"/> <ffi:getProperty name="Message" property="Subject" encode="true"/></div>
   		<div class="paneContentWrapper">
   			<ffi:list collection="Message.MemoLines" items="line">
				<textarea class="testClass2" style="display: none;">
						<ffi:getProperty name="line" encodeLeadingSpaces="true" encode="true"/><br>
				</textarea>
			</ffi:list>
			<span id="dataSpan2"></span> 	
   		</div>
   		<div class="btn-row">
   			<sj:a   button="true" onClickTopics="cancelForm"><s:text name="jsp.default_175"/></sj:a>
   		</div>
 </div>
</div>  
</div>
			
<script>
	$(document).ready(function() {
		var gmMsgStr = "";
		$(".testClass2").each(function(){
			gmMsgStr += $(this).text()+"<br>";
		});
		$("#dataSpan2").html(gmMsgStr);
	})
</script>	