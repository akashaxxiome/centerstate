<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
    $(document).ready(function () {	    	
		$("#inboxMessagesGridId").data("supportSearch",true);

		if($("#inboxMessagesGridId tbody").data("ui-jqgrid") != undefined){
    		$("#inboxMessagesGridId tbody").sortable("destroy");
		}
		
		$.subscribe('deleteAllReceivedCheckboxes', function(event,data) {
			$("#jqgh_inboxMessagesGridId_Delete").removeClass( "ui-jqgrid-sortable" ).addClass( "sapUiIconCls icon-delete" );
			$("#jqgh_inboxMessagesGridId_Delete").css({"cursor" : "pointer"});
			$("#inboxMessagesGridId tbody > tr > td:first-child input").css({ "margin-left" : "14px" });
			//$('#jqgh_inboxMessagesGridId_cb').css({"padding-left":"0", "float":"left"});
		});

    });
</script>
<div id="Received_Messages">
<ffi:help id="messages_msg_inbox" />
<ffi:setProperty name="MessageBox" value="Inbox"/>

	<ffi:setGridURL grid="GRID_msginbox" name="ViewURL" url="/cb/pages/jsp/message/MessageViewAction_received.action?MessageID={0}" parm0="ID"/>
	
    <form id="frmDelMsgFromInbox" action="/pages/jsp/message/messagesdelete.jsp" method="post" name="frmDelMsgFromInbox">    
		<input type="hidden" name="selectedMsgIDs" id="selectedInboxMsgIDs" />
		<input type="hidden" name="messageIDs" id="allSelectedInboxMsgIDs" />
		<input type="hidden" id="multicheck" value="false"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/message/GetMessagesAction.action?msgFilter=InboxFilter&GridURLs=GRID_msginbox&receivedItems=true&messageBody=false" URLEncrypt="true"/>
    <s:url id="inboxMessagesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
    
    <%--Skip call to load grid data when this grid is not visible --%>
	<s:if test="%{#parameters.visibleGrid[0] == 'inbox'}">
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:if>
	 <s:if test="%{#parameters.visibleGrid[0] == 'sentbox'}">
		<ffi:setProperty name="gridDataType" value="local"/>
	</s:if>
	<s:else>
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:else>
	
    
	<sjg:grid  
		id="inboxMessagesGridId"  
		dataType="%{#session.gridDataType}"  
		sortable="true"  
		href="%{inboxMessagesUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
        multiselect="true"
        multiselectWidth="30"
		navigatorView="false"
		sortname="receiveddate"
		sortorder="desc"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		reloadTopics="reloadInboxMessages"
		onPagingTopics="inboxMessagesGridPagingEvents"
		onCompleteTopics="inboxMessagesLoadCompleteEvents"
		onGridCompleteTopics="addGridControlsEvents,inboxMessagesGridCompleteEvents,deleteAllReceivedCheckboxes"
		> 
		
		<sjg:gridColumn 
			title="" 
			name="Delete"
			sortable="false" 
			width="35" 
			hidedlg="true" />
			
		<sjg:gridColumn 
			name="messageIds" 
			title="messageIds" 
			sortable="false" 
			search="false" 
			width="15" 
			cssClass="datagrid_textColumn"
			formatter="ns.message.deleteAllIds" 
			hidedlg="true"
			hidden="true"/>
			
		<sjg:gridColumn 
			name="fromName" 
			index="from" 
			title="%{getText('jsp.default_216')}" 
			sortable="true" 
			formatter="ns.message.formatFromName" 
			width="270"
			cssClass="datagrid_textColumn msgFromImg"/>
			
		<sjg:gridColumn 
			name="subject" 
			index="subject" 
			title="%{getText('jsp.default_394')}" 
			sortable="true" 
			formatter="ns.message.formatSubjectLink1" 
			width="360"
			cssClass="datagrid_textColumn"/>
			
		<sjg:gridColumn 
			name="caseNumDesc" 
			index="casenum" 
			title="%{getText('jsp.message_7')}" 
			sortable="true" 
			search="true" 
			hidden="false" 
			width="150"
			cssClass="datagrid_numberColumn"/>
		
		<sjg:gridColumn 
			name="date" 
			index="receiveddate" 
			title="%{getText('jsp.default_340')}" 
			sortable="true" 
			hidden="false" 
			width="150"
			cssClass="datagrid_dateColumn"/>

		<sjg:gridColumn 
			name="readDate" 
			index="readdate" 
			title="%{getText('jsp.default_337')}" 
			sortable="false" 
			hidden="true" 
			hidedlg="true"
			width="150"
			cssClass="datagrid_dateColumn"/>

		<sjg:gridColumn 
			name="ID" 
			index="id" 
			title="" 
			hidedlg="true" 
			search="false"  
			sortable="false" 
			hidden="true" 
			formatter="ns.message.formatDeleteLinks" 
			width="150"
			cssClass="__gridActionColumn"/>


		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
		
	</sjg:grid>
	
	</form>
	</div>
	<ffi:removeProperty name="gridDataType"/>    
