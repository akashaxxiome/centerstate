<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- <s:url id="inbox_messages_url" value="/pages/jsp/message/msg_inbox.jsp"/>	
<s:url id="sent_messages_url" value="/pages/jsp/message/msg_sent.jsp"/>	
 --%>

<%-- 
<sj:tabbedpanel id="messageTabs" collapsible="false" cssClass="portlet ui-jqgrid">
    <sj:tab id="Inbox" href="%{inbox_messages_url}" label="%{getText('jsp.message_15')}"/>
    <sj:tab id="Sentbox" href="%{sent_messages_url}" label="%{getText('jsp.message_20')}"/>
</sj:tabbedpanel> --%>

<div id="messageTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;">
	    <div class="portlet-header">
	    	<div class="searchHeaderCls">
				<div class="summaryGridTitleHolder">
				
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:if test="%{#parameters.visibleGrid[0] == 'sentbox'}">
									<s:text name="jsp.message_20"/>
								</span>
						</s:if>
						<s:else>
								<s:text name="jsp.message_15"/>
						</s:else>
					</span>
					
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
								<span class="gridTabDropdownItem" id='inbox' onclick="updateGridAndReload('inbox')" >
									<s:text name="jsp.message_15"/>
								</span>
								<span class="gridTabDropdownItem" id='sentbox' onclick="updateGridAndReload('sentbox')">
									<s:text name="jsp.message_20"/>
								</span>
					</div>
				</div>
			</div>
	    </div>
	    
<% 
	String inboxSummaryCssClass ="visible"; 
	String sentSummaryCssClass ="hidden";
%>

<s:if test="%{#parameters.visibleGrid[0] == 'sentbox'}">
	<%
		inboxSummaryCssClass = "hidden";
		sentSummaryCssClass = "visible";
	%>
</s:if>
	    
	    <div class="portlet-content">
		    	<div id="gridContainer" class="summaryGridHolderDivCls">
					<div id="inboxSummaryGrid" gridId="inboxMessagesGridId" class="gridSummaryContainerCls <%=inboxSummaryCssClass%>" >
						<s:include value="/pages/jsp/message/msg_inbox.jsp"/>
					</div>
					<div id="sentboxSummaryGrid" gridId="sentMessagesGridId" class="gridSummaryContainerCls <%=sentSummaryCssClass%>" >
						<s:include value="/pages/jsp/message/msg_sent.jsp"/>
					</div>
				</div>
			</div>
	    	<div id="messageTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       		<ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('messageTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	      		</ul>
			</div>
</div>

	<script>

	//Initialize portlet with settings & calendar icon
	ns.common.initializePortlet("messageTabs");

	function updateGridAndReload(id){
		if(id=='inbox'){
			ns.message.mailbox = 'Inbox';
		}else{
			ns.message.mailbox = 'Sentbox';
		}
		ns.common.showGridSummary(id);
	}
	
	</script>
