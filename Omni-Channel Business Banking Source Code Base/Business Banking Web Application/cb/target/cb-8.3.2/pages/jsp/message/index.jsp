<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>


<script type="text/javascript"
	src="<s:url value='/web/js/message/message%{#session.minVersion}.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/web/js/message/message_grid%{#session.minVersion}.js'/>"></script>

<link type="text/css" rel="stylesheet" media="all"
	href="<s:url value='/web/css/message/message%{#session.minVersion}.css'/>"></link>

<div id="desktop" align="center">
	<div id="operationresult">
		<div id="resultmessage">
			<s:text name="jsp.default_498" />
		</div>
	</div>
	<!-- result -->

	<div id="appdashboard">
		<s:include value="msg_dashboard.jsp" />
	</div>
	<div id="details" style="display: none;">
		<s:include value="/pages/jsp/message/msg_details.jsp" />
	</div>
	
	<div id="summary">

		<ffi:cinclude value1="${dashboardElementId}" value2=""
			operator="equals">
			<s:include value="/pages/jsp/message/msg_summary.jsp" />
		</ffi:cinclude>

	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	ns.message.showMessageDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
});
</script>
