<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>


<%-- we do not show alerts message (type value 8) and
     Employee to employee message (typevalue 3 ) since this is businses central only )
--%>

		<div align="left">


<ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateTimeFormat}" />

				<table width="98%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground tableData">
					<tr>
						<td width="8%" height="17" align="right" valign="baseline" nowrap class="greenClayBackground"><span class="sectionsubhead"><s:text name="jsp.message_9"/></span></td>
						<td valign="baseline" nowrap ><span id="fromInViewMsg" class="columndata"><ffi:getProperty name="Message" property="FromName"/></span></td>
					</tr>
					<tr>
						<td width="8%" height="17" align="right" valign="baseline" nowrap class="greenClayBackground"><span class="sectionsubhead"><s:text name="jsp.default_516"/></span></td>
						<td valign="baseline"><span id="subjectInViewMsg" class="columndata"><ffi:getProperty name="Message" property="Subject"/></span></td>
					</tr>
					<tr>
						<td width="8%" height="17" align="right" valign="baseline" nowrap class="greenClayBackground"><span class="sectionsubhead"><s:text name="jsp.message_16"/></span></td>
						<td valign="baseline" nowrap><span id="ReceivedInViewMsg" class="columndata"><ffi:getProperty name="Message" property="DateValue"/></span></td>
					</tr>

					<tr id="dataTR">
						<td class="tbrd_t columndata" colspan="2">
							<ffi:list collection="Message.MemoLines" items="line">
								<textarea class="testClass3" style="display: none;">
									<ffi:getProperty name="line" encodeLeadingSpaces="true"/>
								</textarea>                                                
							</ffi:list>
							<span id="dataSpan3"></span> 								
						</td>
					</tr>
					<tr>
						<td colspan="2" height="20">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" align="center" nowrap class="ui-widget-header customDialogFooter">
<ffi:cinclude value1="${Message.TypeValue}" value2="<%= Integer.toString(com.ffusion.efs.adapters.profile.constants.ProfileDefines.MESSAGE_TYPE_GLOBAL_MESSAGE)%>" operator="notEquals" >

							<sj:a   button="true" 
									onClickTopics="closeDialog" onclick="ns.home.loadAllMessages(5),ns.home.loadUnReadMessages(5)"
					        ><s:text name="jsp.default_175"/></sj:a>

						    <s:url id="replyMessageUrl" value="/pages/jsp/message/SendReplyAction_initNotificationReply.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							    <s:param name="MessageID" value="%{#attr.Message.ID}"></s:param>		
							</s:url>
				            <sj:a
								id="loadQuickReplySubmit"
								href="%{replyMessageUrl}"
	                            targets="quickViewMsgDlgBand2" 
	                            button="true" 
	                            onBeforeTopics="beforeLoadingQuickReply"
	                            onCompleteTopics="loadingQuickReplyComplete"
								onErrorTopics="errorLoadingQuickReply"
	                            onSuccessTopics="successLoadingQuickReply"
	                        ><s:text name="jsp.message_17"/></sj:a>

						    <s:url id="deleteMessageUrl" value="/pages/jsp/message/DeleteMessagesAction_confirm.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
							    <s:param name="selectedMsgIDs" value="%{#attr.Message.ID}"></s:param>		
							    <s:param name="quickDelete" value="%{'true'}"></s:param>		
							</s:url>
				            <sj:a
								id="quickDeleteMessageConfirmSubmit"
								href="%{deleteMessageUrl}"
	                            targets="quickViewMsgDlgBand2" 
	                            button="true" 
	                            onBeforeTopics="beforeQuickDeletingMessageConfirm"
	                            onCompleteTopics="quickdeletingMessageConfirmComplete"
								onErrorTopics="errorQuickDeletingMessageConfirm"
	                            onSuccessTopics="successQuickDeletingMessageConfirm"
	                        ><s:text name="jsp.default_162"/></sj:a>


						</td>
</ffi:cinclude>
<ffi:cinclude value1="${Message.TypeValue}" value2="<%= Integer.toString(com.ffusion.efs.adapters.profile.constants.ProfileDefines.MESSAGE_TYPE_GLOBAL_MESSAGE)%>" operator="equals" >
							<sj:a   button="true" 
									onClickTopics="closeDialog" onclick="ns.home.loadAllMessages(5),ns.home.loadUnReadMessages(5)"
					        ><s:text name="jsp.default_175"/></sj:a>
						    <s:url id="deleteMessageUrl" value="/pages/jsp/message/DeleteMessagesAction_confirm.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
							    <s:param name="selectedMsgIDs" value="%{#attr.Message.ID}"></s:param>		
							    <s:param name="quickDelete" value="%{'true'}"></s:param>		
							</s:url>
				            <sj:a
								id="quickDeleteMessageConfirmSubmit"
								href="%{deleteMessageUrl}"
	                            targets="quickViewMsgDlgBand2" 
	                            button="true" 
	                            onBeforeTopics="beforeQuickDeletingMessageConfirm"
	                            onCompleteTopics="quickdeletingMessageConfirmComplete"
								onErrorTopics="errorQuickDeletingMessageConfirm"
	                            onSuccessTopics="successQuickDeletingMessageConfirm"
	                        ><s:text name="jsp.default_162"/></sj:a>

						</td>
</ffi:cinclude>
					</tr>
				</table>
			<br>
		</div>

<script>
	$(document).ready(function() {
		var gmMsgStr = "";
		$(".testClass3").each(function(){
			gmMsgStr += $(this).text()+"<br>";
		});
		$("#dataSpan3").html(gmMsgStr);
	})
</script>		