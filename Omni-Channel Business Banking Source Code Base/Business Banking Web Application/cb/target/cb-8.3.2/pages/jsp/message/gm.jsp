<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.struts.ConstantDefine" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<% String msgSize = ""; %>
<ffi:getProperty name="globalMsgs" property="Size" assignTo="msgSize"/>

<ffi:cinclude value1="<%= msgSize %>" value2="0" operator="equals">
	<%request.setAttribute(ConstantDefine.SERVER_EXCEPTION, String.valueOf(ConstantDefine.NO_GLOBAL_MSG_AVAILABLE)); %>
	<script type="text/javascript">typeof jQuery != 'undefined'? $("#globalmessage").hide(): '';</script>
</ffi:cinclude>

<ffi:cinclude value1="<%= msgSize %>" value2="0" operator="notEquals">
<div class="marginTop10">
		<input id="showGlobalMessages" name="showGlobalMessages" type="hidden" value="<s:property value='#request.showGlobalMessages' />">
	    <div id="RTE">
	    	<ffi:list collection="globalMsgs" items="message">
				<textarea class="testCls" data-msgPriority="<ffi:getProperty name="message" property="Priority"/>" style="display: none;">
					<ffi:getProperty name="message" property="MsgText"/>
				</textarea>
				<span id="dataIdSpan" class="dataId"></span>
			</ffi:list>
		</div>
	    
</div>
</ffi:cinclude>

<script>
	$(document).ready(function() {
		getGlobalMessageTextSpan = function(gmText,priority ){
			if(gmText!=''){
				return '<span class="sapUiIconCls icon-alert floatleft" style="color:'+priority +'"></span><span class="globalMsgTxtHolder">'+ gmText +'</span><br/>';
			}
		};
		
		$("#dataIdSpan").html('');
		
		var globalMsgPriorityColorAry = new Array("#D34600", "#00AD1A", "#FFA100");
		
		$(".testCls").each(function(){
			var textMsg = $(this).text();
			var priorityStr = $(this).attr("data-msgPriority");
			var priorityInt = globalMsgPriorityColorAry[parseInt(priorityStr)-1];
			$("#dataIdSpan").append(getGlobalMessageTextSpan(textMsg,priorityInt));
		});
		
		
	});
</script>

<ffi:removeProperty name="IconWidthPct" />
<ffi:removeProperty name="MessageWidthPct" />
