<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<div id="newMsgPortlet" >
	<div class="portlet-header">
	   	  <span class="portlet-title"><s:text name="jsp.message_30"/></span>
	</div>
	<div class="portlet-content">
		<div id="newMessageFormDiv" ></div>
	</div>
	<div id="newMsgPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	       		  <li><a href='#' onclick="ns.common.bookmarkThis('newMsgPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
	              <li><a href='#' onclick="ns.common.showDetailsHelp('newMsgPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
	</div>
</div>

<div id="actionMsgPortlet" style="padding: 5px;">
	<div class="portlet-header">
	   	  <span class="portlet-title"><s:text name="jsp.message_31"/></span>
	</div>
	<div class="portlet-content">
       <div id="messageView"></div>
       <div id="messageReplyWrapper"></div>
    </div>
    <div id="viewMsgPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('actionMsgPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
	</div>
</div>

<%-- <div id="replyMsgPortlet">
	<div class="portlet-header">
	   	  <span class="portlet-title"><s:text name="jsp.message_32"/></span>
	</div>
	<div class="portlet-content">
       <div id="messageReply"></div>
    </div>
    <div id="replyMsgPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	       		  <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
	              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
	</div>
</div> --%>


<script>
//Initialize portlet with settings icon
ns.common.initializePortlet("newMsgPortlet");

ns.common.initializePortlet("actionMsgPortlet");

//ns.common.initializePortlet("replyMsgPortlet");


//ns.common.initializePortlet("messageView");

//ns.common.initializePortlet("messageReply");
/* $('#viewMsgPortlet').portlet({
	title: js_view_message,
	close: true,
	closeCallback: function(){
		$('#details').slideUp();
	},
	helpCallback: function(){
		var helpFile = null;
		
		if( $('#messageReply_pane').css('display').toLowerCase() == "block" ) {
			helpFile = $('#messageReply').find('.moduleHelpClass').html();
		} else {
			helpFile = $('#messageView').find('.moduleHelpClass').html();
		}
		
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	},
	generateDOM: true
}); 

$('#messageView').pane({
	title: js_view_message,
	showHeader: false
});

$('#messageReply').pane({
	title: js_reply_message
});
*/

</script>
    