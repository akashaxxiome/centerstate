<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:setProperty name='PageHeading' value='Controlled Disbursement'/>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="PortalFlag" value="false"/>
<% session.removeAttribute("Presentment"); %>

<ffi:setProperty name="TransferHomeURL" value="${SecurePath}cash/cashdisbursement.jsp"/>


	<div id="Disbursementdesktop" align="center">
			<script type="text/javascript" src="<s:url value='/web/js/cash/cash_grid%{#session.minVersion}.js'/>"></script>
			<ffi:setProperty name="subMenuSelected" value="controlled disbursement"/>

			<ffi:setProperty name="SubmitURL" value="${SecurePath}cash/cashdisbursement.jsp"/>
			<ffi:setProperty name="DateVariable" value="DisbursementCriteriaDate"/>
			<ffi:setProperty name="DataClassificationVariable" value="DisbursementDataClassification"/>
			
			<%-- Determine if entitled to see anything at all - check if any of the 4 IR permissions is set. --%>
			<ffi:removeProperty name="IRentitled"/>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
					<ffi:setProperty name="IRentitled" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL%>">
					<ffi:setProperty name="IRentitled" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
					<ffi:setProperty name="IRentitled" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
					<ffi:setProperty name="IRentitled" value="true"/>
			</ffi:cinclude>
			<%-- Display graceful message if not entitled to any of the 4 information reporting entitlements --%>
			<ffi:cinclude value1="${IRentitled}" value2="true" operator="notEquals">
			<table width="750" border="0" cellspacing="0" cellpadding="0" style="display:none">
				<tbody>
					<tr height="13">
						<td colspan="6" align="left" width="750" height="14" background="/cb/web/multilang/grafx/cash/sechdr_blank.gif"><span class="sectiontitle sectionsubhead">&nbsp; > <s:text name="jsp.cash_32"/></span></td>
					</tr>
					<tr>
						<td width="1" class="tbrd_ltb">&nbsp;</td>
						<td align="center" class="tbrd_tb" colspan="4" nowrap>
							<span align="center" class="columndata">
								<s:text name="jsp.cash_103"/>
							</span>
						</td>
						<td width="1" class="tbrd_trb"> &nbsp; </td>
					</tr>
					<tr>
						<td colspan="6"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" alt="" width="750" height="11" border="0"></td>
					</tr>
				</tbody>
			</table>
			</ffi:cinclude>

		<div id = "Disbursement">
		
				
						
						<ffi:cinclude value1="${IRentitled}" value2="true" operator="equals">
						
							<div id="appdashboard">
								<s:include value="/pages/jsp/cash/cashDisbursementDashboard.jsp"/> 
							</div>
						
							
							<div class="marginTop10">
							<div id="cashDibursementGridTabs" class="portlet gridPannelSupportCls">
									<div class="portlet-header">
									    <div class="searchHeaderCls">
											<span class="searchPanelToggleAreaCls" onclick="$('#disbursementQuicksearchCriteria').slideToggle();">
												<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
											</span>
											<div class="summaryGridTitleHolder">
												<s:if test="%{#parameters.dashboardElementId[0] == 'disbursementAccount'}">
													<span id="selectedGridTab" class="selectedTabLbl">
														<s:text name="jsp.cash.disbursement.by.account"/>
													</span>
												</s:if>
												<s:else>
													<span id="selectedGridTab" class="selectedTabLbl">
														<s:text name="jsp.cash.disbursement.by.presentment"/>
													</span>
												</s:else>
												<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
												
												<div class="gridTabDropdownHolder">
													<span class="gridTabDropdownItem" id='disbursementPresentment' onclick="setDisbursementType('disbursementPresentment')" >
														<s:text name="jsp.cash.disbursement.by.presentment"/>
													</span>
													<span class="gridTabDropdownItem" id='disbursementAccount' onclick="setDisbursementType('disbursementAccount')">
														<s:text name="jsp.cash.disbursement.by.account"/>
													</span>
												</div>
											</div>
										</div>
								    </div> <%-- portlet-header --%>
								    
								   	<div class="portlet-content">
	    								<div id="disbursementQuicksearchCriteria" class="searchDashboardRenderCls quickSearchAreaCls">
	    									<s:include value="/pages/jsp/cash/inc/specifycriteriaforDisbursement.jsp"/>
	    								</div>
  										<div id="gridContainer" class="summaryGridHolderDivCls">
  										<input id="tabType" type="hidden" value="<s:property value='#parameters.dashboardElementId' />"/>
											<div id="disbursementPresentmentSummaryGrid" gridId="cashpresentSummaryID" class="gridSummaryContainerCls hidden" >
												<div id="cashPresentment" class="cashDisbursementAppdashboardType">		
													<s:include value="/pages/jsp/cash/inc/presentmentdisbursements.jsp">
													</s:include> 
												</div>
											</div>
											<div id="disbursementAccountSummaryGrid" gridId="cashaccdisbursementID" class="gridSummaryContainerCls hidden" >
												<div id="cashAccDisbursement" class="cashDisbursementAppdashboardType marginBottom10">
													<s:include value="/pages/jsp/cash/inc/accountdisbursements.jsp">
													</s:include>	
												</div> 
											</div>
										</div>
	    							</div><%-- portlet-content --%>
	    							<div id="cashDibursementDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
								       <ul class="portletHeaderMenu" style="background:#fff">
								              <li><a href='#' onclick="ns.common.showDetailsHelp('cashDibursementGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
								       </ul>
									</div>
							
							<div id="cashDisbursementAppdashboard" class="marginTop10">
							</div>
							
							</div>
							</div>
							
						</ffi:cinclude>	
			
				<div id="cashDisbursedetail" style="display:none;">
				</div>
				<div id="cashDisbursedetailForAcc" style="display:none;">
				</div>
				<div id="summary">
				</div>
					
		</div>	
		
		<script type="text/javascript">
			ns.common.initializePortlet("cashDibursementGridTabs"); 
			
			// all div's are hidden, only required as shown
			var tabType = $('#tabType').val();

			if(tabType == 'disbursementAccount'){
				$('#disbursementTypeID').val('disbursementAccount');
				$('#disbursementAccountSummaryGrid').show();
				$('#disbursementPresentmentSummaryGrid').hide();
			}else{
				$('#disbursementTypeID').val('disbursementPresentment');
				$('#disbursementAccountSummaryGrid').hide();
				$('#disbursementPresentmentSummaryGrid').show();
			}
			//display the grid for selected tab option 
			$(".gridTabDropdownItem").click(function(){
				var id = $(this).attr('id');
				if(id == "disbursementPresentment")
				{
					$("#cashPresentment").show();
					$("#cashAccDisbursement").hide();
				}
				else
				{
					$("#cashPresentment").hide();
					$("#cashAccDisbursement").show();
				}
				//onchange of the gridTabDropdownItem, hide the details divs 
				$("#cashDisbursedetail").hide();
				$("#cashDisbursedetailForAcc").hide();
				
			});
			
			function setDisbursementType(disbursementType) {
				 $('#disbursementTypeID').val(disbursementType);
				 ns.common.showGridSummary(disbursementType);
			}
			
		</script>

		<ffi:removeProperty name="DateVariable"/>
		<ffi:removeProperty name="DataClassificationVariable"/>
		<ffi:removeProperty name="PortalFlag"/>
		<ffi:removeProperty name="fromPortalPage"/>

	</div>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashdisbursement.jsp" URLEncrypt="true"/>