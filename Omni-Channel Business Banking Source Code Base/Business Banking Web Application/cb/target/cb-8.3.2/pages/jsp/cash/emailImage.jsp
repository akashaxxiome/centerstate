<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<script>
function changeActionName(){

	document.emailFormName.action = "/cb/pages/jsp/cash/EmailImagesAction_cancelEmail.action";

}

function changeActionNameBack(){

	document.emailFormName.action = "/cb/pages/jsp/cash/EmailImagesAction_execute.action";

}

</script>

<body">

	<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td colspan="2">

						<s:form action="/pages/jsp/cash/EmailImagesAction_execute.action" method="post" name="emailFormName"  id="emailFormID" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<table width="500" border="0" cellspacing="6" cellpadding="0">

								<tr>
									<td class="tbrd_b" colspan="3" align="left" nowrap><span class="sectionsubhead"><s:text name="jsp.default_184"/></span></td>
									<td class="tbrd_b" align="left" nowrap></td>
								</tr>

								<tr>
									<td colspan="4">
										<div align="left">
											<span class="sectionsubhead"><s:text name="jsp.default_299"/></span>
										</div>
									</td>
								</tr>

								<tr>
									<td align="left" nowrap>
										<div align="left">
											<span class="sectionsubhead"><s:text name="jsp.default_220"/>:</span></div>
									</td>
									<td colspan="2"><input class="ui-widget-content ui-corner-all" type="text" name="fromAddress" value="<ffi:getProperty name="EmailImages" property="FromAddress" />" disabled size="40" border="0" id="fromAddressID">&nbsp;<span id="fromAddressIDError"></span>
									<input type="hidden" name="fromAddress" value="<ffi:getProperty name="EmailImages" property="FromAddress" />"></td>
									<td></td>
								</tr>

								<tr>
									<td align="left" nowrap>
										<div align="left">
											<span class="sectionsubhead"><s:text name="jsp.default_426"/>:</span></div>
									</td>
									<td colspan="2"><input class="ui-widget-content ui-corner-all" type="text" name="toAddress" size="40" border="0" id="toAddressID">&nbsp;<span id="toAddressIDError"></span></td>
									<td></td>
								</tr>

								<tr>
									<td align="left" nowrap>
										<div align="left">
											<span class="sectionsubhead"><s:text name="jsp.default_394"/>:</span></div>
									</td>
									<td colspan="2"><input class="ui-widget-content ui-corner-all" type="text" name="subject" size="40" border="0"></td>
									<td></td>
								</tr>

								<tr>
									<td colspan="3"><textarea class="ui-widget-content ui-corner-all" name="message" cols="72" rows="5" border="0"></textarea></td>
									<td></td>
								</tr>
								
								<tr>
									<td align="left" nowrap></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="3" >
										<div align="center">
											<%-- input class="submitbutton" type="button" value="CANCEL" onclick="javascript:window.history.back();" --%>
											
											<sj:a 
												id="cancelEmailID"
												formIds="emailFormID"
												targets="searchImageDialog" 
												button="true" 
												onclick="changeActionName()"
												><s:text name="jsp.default_82"/></sj:a>
		
											<%-- input class="submitbutton" type="submit" value="SUBMIT" --%>
											
											<sj:a 
												id="emailSubmit"
												formIds="emailFormID"
												targets="searchImageDialog" 
												button="true" 
												validate="true" 
				                                validateFunction="customValidation"
				                                onclick="changeActionNameBack()"
				                                onBeforeTopics="customMappingBefore"
												><s:text name="jsp.default_395"/></sj:a>
		
										</div>
									</td>
									<td></td>
								</tr>
							</table>
						</s:form>
					</td>
				</tr>
	
			</table>
		</div>
</body>

<ffi:setProperty name="EmailImages" property="Validate" value="FROMADDRESS,TOADDRESS" />
<ffi:setProperty name="EmailImages" property="VerifyFormat" value="FROMADDRESS,TOADDRESS" />
