<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@page import="com.ffusion.beans.lockbox.LockboxSummaries" %>
<%@page import="com.ffusion.beans.lockbox.LockboxSummary" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<%
	session.setAttribute("LockboxCriteriaDate", request.getParameter("LockboxCriteriaDate"));
	session.setAttribute("LockboxDataClassification", request.getParameter("LockboxDataClassification"));
%>
	
<ffi:cinclude value1="" value2="${LockboxDataClassification}" operator="equals">
	<ffi:setProperty name="LockboxDataClassification" value="P"/>
</ffi:cinclude>


<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="LockboxDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="" value2="${LockboxSummaries}" operator="equals">
	<ffi:object id="LockboxSummaries" name="com.ffusion.tasks.lockbox.LockboxSummariesTask" scope="session"/>
</ffi:cinclude>
<ffi:setProperty name="LockboxSummaries" property="DateFormat" value="${DateFormat}"/>
<ffi:setProperty name="LockboxSummaries" property="StartDate" value="${LockboxCriteriaDate}"/>
<ffi:setProperty name="LockboxSummaries" property="EndDate" value="${LockboxCriteriaDate}"/>
<ffi:setProperty name="LockboxSummaries" property="DataClassification" value="${LockboxDataClassification}"/>

<ffi:process name="LockboxSummaries"/>


				<%
							session.setAttribute("FFILockboxSummaries", session.getAttribute("LockboxSummaries"));
				%>

<%-- need to set the start date for display.  if it was not set during task processing, set it to the current date. --%>
<ffi:cinclude value1="${LockboxSummaries.StartDate}" value2="" operator="equals">
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
	<ffi:process name="GetCurrentDate"/>

	
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${DateFormat}" />
	<ffi:setProperty name="LockboxSummaries" property="StartDate" value="${GetCurrentDate.Date}"/>
</ffi:cinclude>


<table width="750" border="0" cellspacing="0" cellpadding="0" style="display:none">
	<tr>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="equals">
			<td colspan="6" align="left" width="750" height="14" background="/cb/web/multilang/grafx/sechdr_blank.gif"><span class="sectiontitle sectionsubhead">&nbsp; > <s:text name="jsp.cash_64"/><ffi:getProperty name="LockboxSummaries" property="StartDate"/></span></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="notEquals">
			<td colspan="6" align="left" width="750" height="14" background="/cb/web/multilang/grafx/cash/sechdr_blank.gif"><span class="sectiontitle sectionsubhead">&nbsp; > <s:text name="jsp.cash_63"/> <ffi:getProperty name="LockboxSummaries" property="StartDate"/></span></td>
		</ffi:cinclude>
	</tr>
	<tr>
		<td class="tbrd_ltb" align="left">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><span class="sectionsubhead"><s:text name="jsp.default_15"/></span></td>
					<td>
					<%-- If sorted by this column in descending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="LOCKBOX_ACCT" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=LOCKBOX_ACCT,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="LOCKBOX_ACCT,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=LOCKBOX_ACCT"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="LOCKBOX_ACCT" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="LOCKBOX_ACCT,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=LOCKBOX_ACCT"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
				<tr>
					<td><span class="sectionsubhead"><s:text name="jsp.default_293"/></span></td>
					<td>
					<%-- If sorted by this column in descending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NICKNAME" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=NICKNAME,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NICKNAME,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=NICKNAME"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NICKNAME" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NICKNAME,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=NICKNAME"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>
		<td class="tbrd_tb" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><span class="sectionsubhead"><s:text name="jsp.default_169"/></span></td>
					<td>
					<%-- If sorted by this column in descending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TOTAL_CREDITS" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=TOTAL_CREDITS,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TOTAL_CREDITS,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=TOTAL_CREDITS"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TOTAL_CREDITS" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TOTAL_CREDITS,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=TOTAL_CREDITS"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>
		<td class="tbrd_tb" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><span class="sectionsubhead"><s:text name="jsp.default_123"/></span></td>
					<td>
					<%-- If sorted by this column in descending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NUM_CREDITS" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=NUM_CREDITS,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NUM_CREDITS,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=NUM_CREDITS"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NUM_CREDITS" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="NUM_CREDITS,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=NUM_CREDITS"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>
		<td class="tbrd_tb" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><span class="sectionsubhead"><s:text name="jsp.cash_57"/></span></td>
					<td>
					<%-- If sorted by this column in descending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="IMMEDIATE_FLOAT" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=IMMEDIATE_FLOAT,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="IMMEDIATE_FLOAT,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=IMMEDIATE_FLOAT"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="IMMEDIATE_FLOAT" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="IMMEDIATE_FLOAT,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=IMMEDIATE_FLOAT"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>
		<td class="tbrd_tb" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_3"/></span></td>
					<td>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="ONE_DAY_FLOAT" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=ONE_DAY_FLOAT,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="ONE_DAY_FLOAT,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=ONE_DAY_FLOAT"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="ONE_DAY_FLOAT" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="ONE_DAY_FLOAT,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=ONE_DAY_FLOAT"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>
		<td class="tbrd_trb" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_5"/></span></td>
					<td>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TWO_DAY_FLOAT" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=TWO_DAY_FLOAT,REVERSE"><img src="/cb/web/multilang/grafx/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else if sorted by this column in ascending order --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TWO_DAY_FLOAT,REVERSE" operator="equals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=TWO_DAY_FLOAT"><img src="/cb/web/multilang/grafx/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>

					<%-- Else ( not sorted by this column ) --%>
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TWO_DAY_FLOAT" operator="notEquals">
					<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="TWO_DAY_FLOAT,REVERSE" operator="notEquals">
						<ffi:link url="?LockboxCriteriaDate=${LockboxCriteriaDate}&LockboxDataClassification=${LockboxDataClassification}&LockboxAvailabilityListSortCriteria=TWO_DAY_FLOAT"><img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0"></ffi:link>
					</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="1">
		<td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="110" border="0"></td>
		<td align="right" height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="70" border="0"></td>
		<td align="center" height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="70" border="0"></td>
		<td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="85" border="0"></td>
		<td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="75" border="0"></td>
		<td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="75" border="0"></td>
	</tr>

	<%-- display a message if there are no records for today --%>
	<ffi:cinclude value1="0" value2="${LOCKBOX_SUMMARIES.Size}" operator="equals">
		<tr> <td class="columndata" height="20" colspan="6" align="center"><s:text name="jsp.cash_108"/></td> </tr>
	</ffi:cinclude>

	<%-- sort the list --%>
	<ffi:cinclude value1="${LockboxAvailabilityListSortCriteria}" value2="" operator="notEquals">
		<ffi:setProperty name="LOCKBOX_SUMMARIES" property="SortedBy" value="${LockboxAvailabilityListSortCriteria}"/>
	</ffi:cinclude>

	<%-- In case of Accounts we have tasks that filter the accounts based on what the user is entitled to. 
       	     For lockbox and disbursements we don't have such a task, and since there are only three jsp's that
	     need this customization, it is done as a scriptlet (instead of a task). --%>
	<% boolean toggle = false; %>
	<% int i=0; %>
	<% LockboxSummaries lss = (LockboxSummaries)(session.getAttribute("LOCKBOX_SUMMARIES")); %>
	<ffi:setProperty name="noEntitledLockboxes" value="true"/>
	<ffi:list collection="LOCKBOX_SUMMARIES" items="LOCKBOX_SUMMARY1">
		<%
			LockboxSummary ls = (LockboxSummary)(lss).get(i);
			String accountID = ""; 
			if( ls!=null) {	
				accountID = com.sap.banking.web.util.entitlements.EntitlementsUtil.getEntitlementObjectId(ls.getLockboxAccount());
			}
			i++;
		%>
		<ffi:setProperty name="showAccount" value=""/>
		<ffi:setProperty name="showDetailLink" value=""/>
		<ffi:cinclude value1="${LockboxDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>" operator="equals">
			<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
				<ffi:setProperty name="showAccount" value="true"/>	
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
				<ffi:setProperty name="showAccount" value="true"/>	
				<ffi:setProperty name="showDetailLink" value="true"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${LockboxDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
			<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
				<ffi:setProperty name="showAccount" value="true"/>	
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
				<ffi:setProperty name="showAccount" value="true"/>	
				<ffi:setProperty name="showDetailLink" value="true"/>
			</ffi:cinclude>
		</ffi:cinclude>

		<%-- show only if entitled for this account --%>
		<ffi:cinclude value1="${showAccount}" value2="true" operator="equals">
			<ffi:setProperty name="noEntitledLockboxes" value="false"/>
		<%	toggle = !toggle;
			if (toggle) { 	%>
				<tr class="columndata_grey">
		<% 	} else { 	%>
				<tr class="columndata">
		<% 	} 		%>
			<TD align="left">
			<ffi:cinclude value1="${showDetailLink}" value2="true" operator="equals">
				<ffi:link url="${SecurePath}cash/cashlockboxdeposits.jsp?AccountID=${LOCKBOX_SUMMARY1.LockboxAccountID}&lockboxtransactionsReload=true&DataClassification=${LockboxDataClassification}">
					<ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccount.RoutingNumber"/> : <ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccountID"/>
					<ffi:cinclude value1="${LOCKBOX_SUMMARY1.LockboxAccountNickname}" value2="" operator="notEquals" >
						 - <ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccountNickname"/>
			 		</ffi:cinclude>	
					 - <ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccount.CurrencyType"/>
				</ffi:link>
			</ffi:cinclude>
			<ffi:cinclude value1="${showDetailLink}" value2="true" operator="notEquals">
				<ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccount.RoutingNumber"/> : <ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccountID"/>
				<ffi:cinclude value1="${LOCKBOX_SUMMARY1.LockboxAccountNickname}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccountNickname"/>
		 		</ffi:cinclude>	
				 - <ffi:getProperty name="LOCKBOX_SUMMARY1" property="LockboxAccount.CurrencyType"/>
			</ffi:cinclude>
			</TD>
			<TD align="right">
			<ffi:getProperty name="LOCKBOX_SUMMARY1" property="TotalLockboxCredits.CurrencyStringNoSymbol"/>
			</TD>
			<TD align="right">
			<ffi:cinclude value1="${LOCKBOX_SUMMARY1.TotalNumLockboxCredits}" value2="-1" operator="notEquals">
			    <ffi:getProperty name="LOCKBOX_SUMMARY1" property="TotalNumLockboxCredits"/>
			</ffi:cinclude>
			</TD>
			<TD align="right">
			<ffi:getProperty name="LOCKBOX_SUMMARY1" property="ImmediateFloat.CurrencyStringNoSymbol"/>
			</TD>
			<TD align="right">
			<ffi:getProperty name="LOCKBOX_SUMMARY1" property="OneDayFloat.CurrencyStringNoSymbol"/>
			</TD>
			<TD align="right">
			<ffi:getProperty name="LOCKBOX_SUMMARY1" property="TwoDayFloat.CurrencyStringNoSymbol"/>
			</TD>
			</TR>
		</ffi:cinclude>
	</ffi:list>
		<ffi:cinclude value1="0" value2="${LOCKBOX_SUMMARIES.Size}" operator="notEquals">
		<ffi:cinclude value1="${noEntitledLockboxes}" value2="true">
			<tr> <td class="columndata" height="20" colspan="6" align="center"><s:text name="jsp.cash_108"/></td> </tr>
		</ffi:cinclude>
	</ffi:cinclude>

	<tr>
		<td class="tbrd_ltb"><br>
		</td>
		<td class="tbrd_tb"><br>
		</td>
		<td class="tbrd_tb"><br>
		</td>
		<td class="tbrd_tb"><br>
		</td>
		<td class="tbrd_tb"><br>
		</td>
		<td class="tbrd_trb"><br>
		</td>
	</tr>
	<tr>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="equals">
			<td colspan="6"><img src="/cb/web/multilang/grafx/sechdr_btm.gif" alt="" width="750" height="11" border="0"></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="notEquals">
			<td colspan="6"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" alt="" width="750" height="11" border="0"></td>
		</ffi:cinclude>
	</tr>
</table>

<ffi:setProperty name="LockboxDate" value="${LockboxSummaries.StartDate}"/>		

<s:include value="/pages/jsp/cash/cashlockbox_grid.jsp"/>
	
 <script>
$(function(){
	var title = js_loackbox_deposit_summary_portlet_title;
	var startDate = '<ffi:getProperty name="LockboxSummaries" property="StartDate"/>';				
	ns.common.updatePortletTitle("cashLockboxList", title +" "+ startDate);
});

</script>



 <ffi:removeProperty name="LockboxAvailabilityListSortCriteria"/> 
<ffi:removeProperty name="DateFormat"/>
