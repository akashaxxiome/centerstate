<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:setProperty name='PageText' value='' />
<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>' />
</ffi:cinclude>
<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}cash/cashppupload.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_uploadrecord.gif" alt="" width="150" height="16" border="0" hspace="6"></a><a href="${SecurePath}cash/cashppaybuild.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_buildmanualfile.gif" alt="" width="120" height="16" border="0"></a>' />
<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashppay.jsp?UseLastRequest=TRUE" URLEncrypt="true" />

<%-- 
	In case of file upload this file will be loaded in Iframe used to upload file.
	In that case it will show javascript error as JQuery won't be available.
	Added following functionality to make sure error won't be shown.
 --%>
<s:include value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><br>
		</td>
		<td align="center" class="ltrow2_color">
		<table width="98%" border="0" cellspacing="0" cellpadding="3"
			class="ltrow2_color">
			<tr>
				<td class="columndata" align="center" colspan="3"><s:text name="jsp.cash_119"/></td>
			</tr>
			<tr height="10px"></tr>
			<tr>
				<td colspan="3" align="center" nowrap>
				 <sj:a 
					button="true" 
					onClickTopics="closeDialog"
					><s:text name="jsp.default_175"/></sj:a>
				</td>
			</tr>
		</table>
		</td>
		<td>
		</td>
	</tr>
</table>
</div>
