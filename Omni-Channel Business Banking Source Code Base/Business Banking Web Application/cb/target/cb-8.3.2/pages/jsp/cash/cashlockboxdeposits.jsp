<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<%	
	session.setAttribute("AccountID", request.getParameter("AccountID"));
	session.setAttribute("DataClassification", request.getParameter("DataClassification"));
	session.setAttribute("lockboxtransactionsReload", request.getParameter("lockboxtransactionsReload"));
	session.setAttribute("LockboxCriteriaDate", request.getParameter("LockboxCriteriaDate"));
%>	

<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:cinclude value1="${lockboxtransactionsReload}" value2="false" operator="notEquals">
    <ffi:object id="GetLockboxTransactions" name="com.ffusion.tasks.lockbox.GetPagedTransactions" scope="session"/>
    <ffi:setProperty name="GetLockboxTransactions" property="DateFormatParam" value="${DateFormat}"/>
    <ffi:setProperty name="GetLockboxTransactions" property="StartDate" value="${LockboxCriteriaDate}"/>
    <ffi:setProperty name="GetLockboxTransactions" property="EndDate" value="${LockboxCriteriaDate}"/>
    <ffi:setProperty name="GetLockboxTransactions" property="Page" value="first"/>
    <ffi:setProperty name="GetLockboxTransactions" property="DataClassification" value="${DataClassification}"/>
    <ffi:setProperty name="GetLockboxTransactions" property="AccountID" value="${AccountID}"/>
    <ffi:setProperty name="GetLockboxTransactions" property="PageSize" value="10"/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetLockboxTransactions" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetLockboxTransactions" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetLockboxTransactions" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <ffi:setProperty name="GetLockboxTransactions" property="SortCriteriaOrdinal" value="1"/>
    <ffi:setProperty name="GetLockboxTransactions" property="SortCriteriaName" value="<%= com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_LOCKBOX_NUMBER %>"/>
    <ffi:setProperty name="GetLockboxTransactions" property="SortCriteriaAsc" value="True"/>
    <ffi:setProperty name="lockboxtransactionsReload" value="false"/>
</ffi:cinclude>
<ffi:process name="GetLockboxTransactions"/>

				<%
							session.setAttribute("FFIGetLockboxTransactions", session.getAttribute("GetLockboxTransactions"));
				%>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashlockboxdeposits.jsp" URLEncrypt="true"/>
		<div align="center">



  <table width="750" border="0" cellspacing="0" cellpadding="0" style="display:none">
    <tr>
      <td colspan="7" width="750" background="/cb/web/multilang/grafx/cash/sechdr_blank.gif" class="sectiontitle">
      	&nbsp;&nbsp; &gt; <s:text name="jsp.cash_62"/> 
	<ffi:setProperty name="BankingAccounts" property="Filter" value="ID=${GetLockboxTransactions.AccountID}"/>
	<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
		<ffi:list collection="BankingAccounts" items="acct" startIndex="1" endIndex="1">
			<s:text name="jsp.default_15"/> <ffi:getProperty name="acct" property="RoutingNum"/> :
			<ffi:getProperty name="acct" property="DisplayText"/> -
			<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
				<ffi:getProperty name="acct" property="NickName"/> -
	 		</ffi:cinclude>	
			<ffi:getProperty name="acct" property="CurrencyCode"/> <s:text name="jsp.cash_74"/> 
		</ffi:list>
	</ffi:cinclude>
	<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	<% session.removeAttribute("AccountID"); %>
	<ffi:getProperty name="GetLockboxTransactions" property="StartDate"/>
      </td>
    </tr>
    <tr>
      <td class="tbrd_ltb"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_LOCKBOX_NUMBER %>"/>
            <td><span class="sectionsubhead" align="left"><s:text name="jsp.cash_67"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table>
      </td>
      <td class="tbrd_tb" align="right"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_AMOUNT %>"/>
            <td><span class="sectionsubhead" align="left"><s:text name="jsp.default_43"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table>
      </td>
      <td class="tbrd_tb" align="right"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_IMMEDIATE_FLOAT %>"/>
            <td><span class="sectionsubhead"><s:text name="jsp.cash_57"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table></td>
      <td class="tbrd_tb" align="right"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_ONE_DAY_FLOAT %>"/>
            <td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_4"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table></td>
      <td class="tbrd_tb" align="right"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_TWO_DAY_FLOAT %>"/>
            <td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_6"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table></td>
      <td class="tbrd_tb" align="right"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_NUM_REJECTED_CHECKS %>"/>
            <td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_94"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table></td>
      <td class="tbrd_trb" align="right"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
	    <ffi:setProperty name="GetLockboxTransactions" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_AMT_REJECTED %>"/>
            <td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_93"/></span></td>
	    <td><ffi:link url="?DataClassification=${DataClassification}&GetLockboxTransactions.Page=first&GetLockboxTransactions.SortedBy=${GetLockboxTransactions.ToggleSortedBy}"><ffi:getProperty name="GetLockboxTransactions" property="SortImage" encode="false"/></ffi:link></td>
          </tr>
        </table></td>
    </tr>
    <tr height="1">
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="110" border="0"></td>
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="110" border="0"></td>
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="110" border="0"></td>
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="110" border="0"></td>
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="110" border="0"></td>
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="100" border="0"></td>
      <td height="1"><img src="/cb/web/multilang/grafx/cash/spacer.gif" height="1" width="100" border="0"></td>
    </tr>
	<ffi:cinclude value1="0" value2="${LOCKBOX_TRANSACTIONS.Size}" operator="equals">
		<tr>
			<td class="promptfont" height="20" colspan="7" align="center"><s:text name="jsp.cash_131" /></td>
		</tr>
	</ffi:cinclude>

	<%-- sort the list --%>
	<ffi:cinclude value1="${LockboxDepositAvaliabilityListSortCriteria}" value2="" operator="notEquals">
		<ffi:setProperty name="LOCKBOX_TRANSACTIONS" property="SortedBy" value="${LockboxDepositAvaliabilityListSortCriteria}"/>
	</ffi:cinclude>

        <ffi:cinclude value1="0" value2="${LOCKBOX_TRANSACTIONS.Size}" operator="notEquals">

	<% boolean toggle = false; %>
	<ffi:list collection="LOCKBOX_TRANSACTIONS" items="LOCKBOX_TRANSACTIONS_1">
		<%-- Using different style on different lines --%>
		<%	toggle = !toggle;
			if (toggle) { 	%>
				<tr class="columndata_grey">
		<% 	} else { 	%>
				<tr class="columndata">
		<% 	} 		%>
			<ffi:setProperty name="nextIndex" value="${LOCKBOX_TRANSACTIONS_1.TransactionIndex}"/>

      		<td align="left">
				<ffi:link url="cashlockboxdetail.jsp?AccountID=${LOCKBOX_TRANSACTIONS_1.AccountID}&LockboxNumber=${LOCKBOX_TRANSACTIONS_1.LockboxNumber}&lockboxcredititems-reload=true&DataClassification=${DataClassification}">
					<ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="LockboxNumber"/>
				</ffi:link>
			</td>
      		<td align="right"><ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="Amount.CurrencyStringNoSymbol"/></td>
     		<td align="right"><ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="ImmediateFloat.CurrencyStringNoSymbol"/></td>
			<%-- <td align="right"><ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="OneDayFloat.CurrencyStringNoSymbol"/></td>  --%>
			<td align="right"><ffi:getProperty name="GetLockboxTransactions" property="StartDate"/></td>
      		<td align="right"><ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="TwoDayFloat.CurrencyStringNoSymbol"/></td>
      		<td align="right"> <div align="right"/><ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="NumRejectedChecks"/></div></td>
			<td align="right"> <div align="right"/><ffi:getProperty name="LOCKBOX_TRANSACTIONS_1" property="RejectedAmount.CurrencyStringNoSymbol"/></div></td>
		</tr>
	</ffi:list>
       	</ffi:cinclude>
        <tr>
	<ffi:cinclude value1="0" value2="${LOCKBOX_TRANSACTIONS.Size}" operator="notEquals">
       	  <td class="tbrd_ltb" colspan="4" valign="middle">&nbsp;
	    <span class="columndata">&nbsp;&nbsp;&nbsp;&nbsp;
	      <ffi:cinclude value1="${GetLockboxTransactions.IsFirstPage}" value2="false" operator="equals">
		<ffi:link url='?DataClassification=${DataClassification}&GetLockboxTransactions.Page=previous'><img src="/cb/web/multilang/grafx/buttons_previous.gif" alt="" height="16" border="0" hspace="6"></ffi:link>
	      </ffi:cinclude>
	      <ffi:cinclude value1="${GetLockboxTransactions.IsLastPage}" value2="false" operator="equals">
		<ffi:link url='?DataClassification=${DataClassification}&GetLockboxTransactions.Page=next'><img src="/cb/web/multilang/grafx/button_next.gif" alt="" height="16" border="0"></ffi:link>
	      </ffi:cinclude>
	    </span>
	  </td>
	</ffi:cinclude>
	<ffi:cinclude value1="0" value2="${LOCKBOX_TRANSACTIONS.Size}" operator="equals">
                <td class="tbrd_ltb" colspan="4"><br></td>
	</ffi:cinclude>
                <td class="tbrd_tb"><br></td>
                <td class="tbrd_tb"><br></td>
                <td class="tbrd_trb"><br></td>
        </tr>
    <tr>
      <td colspan="7"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" height="11" width="750"></td>
    </tr>
  </table>
  <s:include value="/pages/jsp/cash/cashlockboxdeposit_grid.jsp"/> 
  	</div>
  <script>
  $(document).ready(function(){
	  $('#lockboxDepositIcon').portlet({
			generateDOM: true,
			helpCallback: function(){
				var helpFile = $('#lockboxDepositIcon').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
		});
	  
	  
			var title = js_loackbox_account_deposit_summary_portlet_title;
			var routingNum = '<ffi:getProperty name="acct" property="RoutingNum"/>';
			var displayText = '<ffi:getProperty name="acct" property="DisplayText"/>';
			var nickName = '<ffi:getProperty name="acct" property="NickName"/>';
			var currencyCode = '<ffi:getProperty name="acct" property="CurrencyCode"/>';
			var startDate = '<ffi:getProperty name="LockboxDate"/>';				
			$("#lockboxDepositIcon").portlet('title', title +" "+ routingNum + ":" + displayText + "-" + nickName + "-" + currencyCode + ' <s:text name="jsp.cash_130" /> ' + startDate);
		
	  
  });
	
	</script>
				
  				
   

			
		
	
	


<ffi:removeProperty name="TempPreviousPage"/>
<ffi:removeProperty name="TempNextPage"/>
<ffi:removeProperty name="DataClassification"/>
