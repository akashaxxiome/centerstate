<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:removeProperty name="CurrentSelectedTotalRows"/>
	<div id="disbursementDetailIcon" class="portlet">
	<ffi:help id="cash_cashdisbursedetail" />
	<ffi:setGridURL grid="GRID_disburseDetails" name="ViewURL" url="/cb/pages/jsp/cash/cashdisburseitem.jsp?BackToURL=/cb/pages/jsp/cash/cashdisbursement.jsp&AccountID={0}&Presentment={1}&DisbursementDataClassification={2}&DisbursementCriteriaDate={3}&DisbursementTransactionIndex={4}" parm0="accountID" parm1="searchCriteriaValue" parm2="dataClassification" parm3="date" parm4="tranIndex"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/cash/GetDisburseDetailAction.action?collectionName=DisbursementItems&GridURLs=GRID_disburseDetails" URLEncrypt="true"/>
    <s:url id="cashdisbursedetailURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="cashdisbursedetailID"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{cashdisbursedetailURL}"  
		pager="true" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"		
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,cashdisbursedetailEvent"
		> 
		
		<sjg:gridColumn name="checkDate" index="checkDate" title="%{getText('jsp.default_137')}" sortable="true" width="100" hidden="true" hidedlg="true" cssClass="datagrid_dateColumn"/>
		<sjg:gridColumn name="map.getDateString" index="getDateString" title="%{getText('jsp.default_137')}" sortable="true" hidden="" cssClass="datagrid_dateColumn"/>
	    <sjg:gridColumn name="payee" index="payee" title="%{getText('jsp.default_313')}" sortable="true" width="100" cssClass="datagrid_actionColumn"/>
	    <sjg:gridColumn name="checkNumber" index="checkNumber" title="%{getText('jsp.default_91')}" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
	    <sjg:gridColumn name="checkReferenceNumber" index="checkReferenceNumber" title="%{getText('jsp.cash_114')}" sortable="true" width="90" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="memo" index="memo" title="%{getText('jsp.default_279')}" sortable="true" width="200" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="checkAmount.currencyStringNoSymbol" index="checkAmount.currencyStringNoSymbol" formatter="ns.cash.checkAmountFormatter" title="%{getText('jsp.default_43')}" sortable="true" width="90" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.tranIndex" index="transactionIndex" title="%{getText('jsp.cash_116')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		
		<sjg:gridColumn name="map.searchCriteriaValue" index="searchCriteriaValue" title="%{getText('jsp.cash_98')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.accountID" index="accountID" title="%{getText('jsp.cash_10')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.dataClassification" index="dataClassification" title="%{getText('jsp.cash_47')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.date" index="date" title="%{getText('jsp.default_137')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.objBackURL" index="objBackURL" title="%{getText('jsp.cash_73')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		
	</sjg:grid>
	</div>
	<br>
	<div align="center">
		<sj:a id="backFormButtonForDetail" 
				button="true" 
				onClickTopics="GoBackToCashOption"
		><s:text name="jsp.default_57"/></sj:a>
	</div>
	
	
