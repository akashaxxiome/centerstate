<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%--
  --  The view button for consolidated balance and account balance
 --%>
<script language="javascript">
$(function(){
	$("#reportOn").selectmenu({width: 140});
   });
//Refresh the summary for view button for consolidated balance and account balance
function reloadView(){
	var flag ="<ffi:getProperty name="flag"/>";
	if(flag=="consolidate"){
		var urlString ="/cb/pages/jsp/account/consolidatedbalance.jsp";
		$.ajax({
			url: urlString,	
			type: "POST",
			data: $("#CriteriaFormID").serialize(),
			success: function(data){
				$('#querybord').html(data);
				$('#consolidatedBalanceSummary').html('');
				$.publish("common.topics.tabifyDesktop");					
				$("#reloadViewButton").focus();
			}
		});
		}
	
	 if(flag=="accountbygroup"){
            accountbygroupURL = "<ffi:urlEncrypt url='/cb/pages/jsp/accountsbygroup.jsp?AccountGroupID=${AccountGroupID}'/>";
		    
			$.ajax({
				url: accountbygroupURL,	
				type: "POST",
				data: $("#CriteriaFormID").serialize(),
				success: function(data){
					$('#querybord').html(data);
					$('#consolidatedBalanceSummary').html('');
					$.publish("common.topics.tabifyDesktop");					
					$("#reloadViewButton").focus();
				}
			});
			}
	 if(flag=="accountbalance"){
			accountbalanceURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accountbalance.jsp?changeAccountDisplayCurrency=true'/>";
		    $.ajax({
				url: accountbalanceURL,	
				type: "POST",
				data: $("#CriteriaFormID").serialize(),
				success: function(data){
					$('#accountBalanceDashbord').html(data);
					$.publish("common.topics.tabifyDesktop");					
					$("#reloadViewButton").focus();
			}
			});
		 }
}
</script>
<% String DataClassification = "";  String DateString= ""; String AccountDisplayCurrencyCodeString= "";%>

<%
/** Getting "DataClassificationVariable" from request else from session. */
String reportOnValue = (String)request.getParameter("DataClassificationVariable");
session.setAttribute("DataClassification", reportOnValue != null && !reportOnValue.isEmpty() ? reportOnValue : session.getAttribute("DataClassificationVariable"));
%>

<ffi:getProperty name="AccountDisplayCurrencyCode" assignTo="AccountDisplayCurrencyCodeString"/>
<%-- If this is the first visit to the page, --%>
<ffi:cinclude value1="${DataClassification}" value2="" operator="equals">
	<%-- we set DataClassification to its default value - Previous Day --%>
	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
	<%-- UNLESS we are doing this for Controlled Disubursement, where we would like default Intra Day --%>
	<ffi:cinclude value1="${DataClassificationVariable}" value2="DisbursementDataClassification" operator="equals">
		<%-- but first we'll check if we are even entitled to Intra day! --%>
		<ffi:setProperty name="intraEntitled" value="false"/>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<%-- and if we are, we will set the default to Intra Day --%>
		<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
	        	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:setProperty name="${DataClassificationVariable}" value="${DataClassification}"/>
</ffi:cinclude>

<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="notEquals">
	<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
	<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	<ffi:cinclude value1="" value2="${AccountGroupID}" operator="notEquals">
		<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP=${AccountGroupID}" />
	</ffi:cinclude>
	<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="${AccountsCollectionName}"/>
	<ffi:process name="CheckPerAccountReportingEntitlements"/>
</ffi:cinclude>
<!-- <center> -->
    <s:form method="post" id="CriteriaFormID" name="CriteriaForm" theme="simple">
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <input type="hidden" name="AccountDisplayCurrencyCode" value="<%= AccountDisplayCurrencyCodeString==null?"" : com.ffusion.util.HTMLUtil.encode(AccountDisplayCurrencyCodeString) %>"/>
	<div id="reportOnDivID">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>

			<td class="columndata ltrow2_color" colspan="6" align="center">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.default_354"/></span>
					<div style="margin-bottom:5px"></div>
					<!-- <td nowrap> 
						<table>
							<tr>
								<td>  -->
									<select class="txtbox" id="reportOn" name="DataClassificationVariable">
 										<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="equals">								
										<%-- we must determine which data classification the user is entitled to. AccountsCollectionName will be blank for
									       	     Lockbox and Disbursement. --%>
										     	<ffi:removeProperty name="previousEntitled"/>
										     	<ffi:removeProperty name="intraEntitled"/>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
												<ffi:setProperty name="previousEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
												<ffi:setProperty name="previousEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${previousEntitled}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
											</ffi:cinclude>

											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
												<ffi:setProperty name="intraEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
												<ffi:setProperty name="intraEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
											</ffi:cinclude>
 										</ffi:cinclude>
 										<ffi:cinclude  value1="${AccountsCollectionName}" value2="" operator="notEquals">
 								 		<%-- we must determine which data classifications to place in this drop down, based on the entiltment checks on the accounts collection AND the data type of this information --%>
 										<ffi:cinclude value1="${DataType}" value2="Summary" operator="equals">
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
 											</ffi:cinclude>
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
 											</ffi:cinclude>
 										</ffi:cinclude>
 										<ffi:cinclude value1="${DataType}" value2="Detail" operator="equals">
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
 											</ffi:cinclude>
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
 											</ffi:cinclude>
 										</ffi:cinclude>
 										<ffi:cinclude value1="${DataType}" value2="" operator="equals">
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryPrev}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
 											</ffi:cinclude>
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryIntra}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
 											</ffi:cinclude>
 										</ffi:cinclude>
 										</ffi:cinclude> <%-- End if accounts collection name is empty --%>
									</select>
								</td>	
								<!-- </td>
							</tr>
						</table>
						
					</td> -->
                        <td align="center" nowrap>
                            <br><sj:a id="reloadViewButton"								
                                  button="true"
                                  onclick="reloadView()"
								  cssStyle="margin-left: 10px; margin-top:5px;"><s:text name="jsp.default_459"/></sj:a>
                             </td>		
				</tr>
			</table>
			</td>
			<td></td>
		</tr>
		
	</table>
        </s:form>
	</div>
<!-- 	</center> -->
<ffi:removeProperty name="DataClassification"/>
<ffi:removeProperty name="DateString"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>