<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_211')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:help id="cash_cashppayupload" className="moduleHelpClass"/>
<span class="shortcutPathClass" style="display:none;">cashMgmt_ppay,ppayuploadLink</span>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
	<ffi:setProperty name='Temp1' value='${SecurePath}fileuploadcustom.jsp?Section=PPAY' URLEncrypt="true"/>
	<ffi:setProperty name='PageText' value='<a href="${Temp1}"><img src="/cb/pages/${ImgExt}grafx/account/i_custommappings.gif" alt="" width="91" height="16" border="0"></a>'/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
	<ffi:setProperty name='PageText' value=''/>
</ffi:cinclude>

<ffi:object id="FileUpload" name="com.ffusion.tasks.fileImport.FileUploadTask" scope="session"/>
<ffi:setProperty name="FileUpload" property="FileType" value="Positive Pay Check Record" />

<ffi:object id="CheckPPayImport" name="com.ffusion.tasks.fileImport.CheckPPayImportTask" scope="session"/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
		<ffi:object id="GetMappingDefinitions" name="com.ffusion.tasks.fileImport.GetMappingDefinitionsTask" scope="session"/>
		<ffi:process name="GetMappingDefinitions"/>
		<ffi:removeProperty name="GetMappingDefinitions"/>
</ffi:cinclude>

<ffi:object id="SetMappingDefinition" name="com.ffusion.tasks.fileImport.SetMappingDefinitionTask" scope="session"/>
<% session.setAttribute("FFISetMappingDefinition", session.getAttribute("SetMappingDefinition")); %>
<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>
<ffi:setProperty name="CheckPPayImport" property="ImportErrorsURL" value="confirm"/>
<%-- 
<ffi:setProperty name="FileUpload" property="SuccessURL" value="${SecureServletPath}CheckPPayImport"/>
<ffi:setProperty name="CheckPPayImport" property="SuccessURL" value="${SecurePath}cash/cashppuploadsuccess.jsp"/>
<ffi:setProperty name="CheckPPayImport" property="ImportErrorsURL" value="${SecurePath}cash/cashppuploadfailure.jsp"/>
--%>

<script type="text/javascript"><!--


function updateCustomMapping(fileType, customFileType)
{
	if ( fileType==null || fileType.value==null ) {
		return true;
	}

	if ( fileType.value == 'Custom')
	{    
		$("#cashSetMappingDefinitionID").selectmenu('enable');
	}
	else
	{   
		$("#cashSetMappingDefinitionID").val('0');
		$("#cashSetMappingDefinitionID").selectmenu('destroy').selectmenu({escapeHtml:'true', width: 220});
		$("#cashSetMappingDefinitionID").selectmenu('disable');
	}
}

function verifyMappingSelected( fileType, customFileType ) {

	return true;
}     
		ns.cash.refreshDashboardFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/ppay_mapping_dashboard.jsp'/>";
		ns.cash.originalDashboardFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/positivepay/positivepay_dashboard.jsp'/>"; 
					
		$(function(){
			//ns.cash.refreshDashboardForm(ns.cash.refreshDashboardFormURL);
			$("#cashfileTypeID").selectmenu({width: 220});
			$("#cashSetMappingDefinitionID").selectmenu({escapeHtml:'true', width: 220});
		
		});
		
		$(document).ready(function(){
			updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID']);
		});

      $.subscribe('openCashFileUploadTopics', function(event,data){
  		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "true";
		ns.common.openDialog("fileImport");	
  	});
      
// --></script>
		<span id="PageHeading" style="display:none;"><%="" + session.getAttribute("PageHeading")%></span>
		<div align="center">
			<form name="FileType" id="CashFileTypeID" action="/cb/pages/fileupload/cashFileUploadAction_verify.action" method="post" target="FileUpload">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" >
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="columndata" width="480" colspan="3"><s:text name="jsp.default_524"/><br>
									<br><s:text name="jsp.default_526"/><br><br></td>
								<%-- <td class="columndata" nowrap width="30"></td> 
	
							<td class="columndata" valign="top" align="left">
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
								<select id="cashfileTypeID" name="fileType" class="txtbox" onChange="updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
									<option value="Positive Pay Check Record"><s:text name="jsp.cash_83"/></option>
									<option value="Custom"><s:text name="jsp.default_132"/></option>
								</select>
							</ffi:cinclude>
							<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
								<s:text name="jsp.cash_83"/>
							</ffi:cinclude>
							</td> --%>
							</tr>
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
							<tr>
								<td>
									<span class="sectionsubhead"><s:text name="jsp.default_14"/></span>
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
									<select id="cashfileTypeID" name="fileType" class="txtbox" onChange="updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
										<option value="Positive Pay Check Record"><s:text name="jsp.cash_83"/></option>
										<option value="Custom"><s:text name="jsp.default_132"/></option>
									</select>
									</ffi:cinclude>
									<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
										<s:text name="jsp.cash_83"/>
									</ffi:cinclude>
									<span class="sectionsubhead marginleft10"><s:text name="jsp.default_135"/></span>
									<select id="cashSetMappingDefinitionID" name="SetMappingDefinition.ID" class="txtbox" onChange="updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
										<option value="0" selected >- <s:text name="jsp.default_377"/> -</option>
										<ffi:list collection="MappingDefinitions" items="MappingDefinition">
											<ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
												<ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
												<ffi:process name="GetOutputFormat"/>
												<ffi:setProperty name="OutputFormat" property="CurrentCategory" value="PPAY"/>
	
												<ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
													<option value="<ffi:getProperty name='MappingDefinition' property='MappingID'/>"><ffi:getProperty name="MappingDefinition" property="Name"/></option>
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:list>
									</select>
									<br/>
	                                      <span id="mappingTypeError"></span>
	                                     </td>
								</tr>
							</ffi:cinclude>
							<tr>
								<td colspan="3" align="center" style="padding-top: 10px;" nowrap>
								<sj:a 
								    id="btnCancelUploadForm"
									button="true"
									onClickTopics="onUploadCancelButton"
									><s:text name="jsp.default_82"/></sj:a>
								<sj:a 
								    id="cashFileUploadID"
									formIds="CashFileTypeID" 
									button="true"
									targets="openFileImportDialogID" 
									validate="true"
							        validateFunction="customValidation"
									onclick="removeValidationErrors();"
									onErrorTopics="errorOpenFileUploadTopics"
									onSuccessTopics="openCashFileUploadTopics"
									><s:text name="jsp.cash_2"/></sj:a>
								</td>
							</tr>
						</table>
					</td>
					
				</tr>
			
			</table>
		</form>
	</div>
<ffi:removeProperty name="tmp_url"/>
<ffi:removeProperty name="GetOutputFormat"/>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />
