<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!-- <div class="innerPortletsContainer"  class="clearBoth"> -->
<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:process name="GetCurrentDate"/>

<div id="cashflowSummary"style="display:none;">
	<div class="portlet-header">
		<div class="searchHeaderCls">
			<span class="searchPanelToggleAreaCls" onclick="$('.acntDashboard_masterItemHolders').slideToggle();">
				<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
			</span>
			<div class="summaryGridTitleHolder marginRight10">
				<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_531"/>
				</span>
			</div>
		</div>
	</div>
	<div class="portlet-content">
		<s:include value="/pages/jsp/cash/inc/cashflowSearchCriteria.jsp"/>
		<div id="cashflowSummaryGrid" class="clearBoth">
   	 	</div>
	</div>
    <div id="cashflowDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('cashflowSummary')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>	
</div>
<!-- </div> -->


<script>

 $(document).ready(function(){
    ns.common.initializePortlet("cashflowSummary");
	var title = js_cashflow_summary_portlet_title;
	ns.common.updatePortletTitle("cashflowSummary",title + ' <ffi:getProperty name="GetCurrentDate" property="Date"/>');
});	

</script>