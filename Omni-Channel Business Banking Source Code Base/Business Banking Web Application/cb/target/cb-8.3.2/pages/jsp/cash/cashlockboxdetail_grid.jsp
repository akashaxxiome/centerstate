<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:help id="cash_cashlockboxdetail" />
	<ffi:setProperty name="tempURL" value="/pages/jsp/cash/GetLockboxDetailAction.action?collectionName=LOCKBOX_CREDIT_ITEMS" URLEncrypt="true"/>
    <s:url id="cashlockboxdetailURL" value="%{#session.tempURL}"/>
	<sjg:grid  
		id="cashlockboxdetailID"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{cashlockboxdetailURL}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,cashlockboxdetailEvent"
		> 
		
		<sjg:gridColumn name="payer" index="payer" title="%{getText('jsp.cash_77')}" sortable="true" width="80" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="checkAmount.currencyStringNoSymbol_1" index="checkAmount.currencyStringNoSymbol" title="%{getText('jsp.cash_26')}" sortable="true" width="80" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}" sortable="true" width="80" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="couponAccountNumber" index="couponAccountNumber" title="%{getText('jsp.cash_34')}" sortable="true" width="80" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="couponAmount1.currencyStringNoSymbol" index="couponAmount1.currencyStringNoSymbol" title="%{getText('jsp.cash_35')}" sortable="true" width="80" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="couponAmount2.currencyStringNoSymbol" index="couponAmount2.currencyStringNoSymbol" title="%{getText('jsp.cash_36')}" sortable="true" width="80" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="couponDate1String" index="couponDate1String" title="%{getText('jsp.default_116')}" sortable="true" width="80" cssClass="datagrid_dateColumn"/>
		<sjg:gridColumn name="couponDate2String" index="couponDate2String" title="%{getText('jsp.default_117')}" sortable="true" width="80" cssClass="datagrid_dateColumn"/>
		<sjg:gridColumn name="couponReferenceNumber" index="couponReferenceNumber" title="%{getText('jsp.cash_89')}" sortable="true" width="80" cssClass="datagrid_numberColumn"/>
		
		<sjg:gridColumn name="map.accountID" index="accountID" title="%{getText('jsp.cash_10')}" sortable="true" width="80" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.dataClassification" index="dataClassification" title="%{getText('jsp.cash_47')}" sortable="true" width="80" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
	
	</sjg:grid>
