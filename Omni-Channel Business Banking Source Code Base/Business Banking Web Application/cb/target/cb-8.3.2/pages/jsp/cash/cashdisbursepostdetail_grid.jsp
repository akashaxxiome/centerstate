<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<div id="disbursementPostDetailIcon">
	<ffi:help id="cash_cashdisbursepostdetail" />

	<ffi:setGridURL grid="GRID_cashPostDetail" name="ViewURL" url="/cb/pages/jsp/cash/cashdisbursedetail.jsp?Presentment={0}&AccountID={1}&Date={2}&DataClassification={3}&disbursementtransactions-reload=true&BackToURL=/cb/pages/jsp/cash/cashdisbursepostdetail.jsp" parm0="presentment" parm1="Account.AccountID" parm2="date" parm3="dataClassification"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/cash/GetDisbursePostDetailAction.action?collectionName=DisbursementSummariesForPresentment&GridURLs=GRID_cashPostDetail" URLEncrypt="true"/>
    <s:url id="cashdisbursepostdetailURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="cashdisbursepostdetailID"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{cashdisbursepostdetailURL}"  
		pager="true"   
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,cashdisbursepostdetailEvent"
		> 
		
		<sjg:gridColumn name="accountNickName" index="accountNickNameForDeposit" title="%{getText('jsp.default_15')}" sortable="true" width="350" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="currentBalance.currencyStringNoSymbol" index="currentBalance.currencyStringNoSymbolForDeposit" title="%{getText('jsp.default_60')}" sortable="true" width="150" cssClass="datagrid_amountColumn"/>
	    <sjg:gridColumn name="numItemsPending" index="numItemsPendingForDeposit" title="%{getText('jsp.cash_60')}" sortable="true" width="150" cssClass="datagrid_actionColumn"/>
	    <sjg:gridColumn name="totalDebits.currencyStringNoSymbol" index="totalDebits.currencyStringNoSymbolForDeposit" title="%{getText('jsp.default_43')}" sortable="true" width="90" cssClass="datagrid_textColumn"/>
		
		
		<sjg:gridColumn name="map.rountingNumber" index="rountingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_8')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="map.accountName" index="accountName" title="%{getText('jsp.cash_12')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_43')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.presentment" index="presentment" title="%{getText('jsp.default_328')}" sortable="true" width="150" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="account.accountID" index="accountID" title="%{getText('jsp.cash_10')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="map.date" index="date" title="%{getText('jsp.default_137')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="map.dataClassification" index="dataClassification" title="%{getText('jsp.cash_47')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
  
	</sjg:grid>
	</div>
	<br>
	<div align="center">
	<sj:a id="backFormButtonForCashPost" 
			button="true" 
			onClickTopics="GoBackToPresentment"
	><s:text name="jsp.default_57"/></sj:a> 
	</div>	
