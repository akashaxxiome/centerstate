<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
    
    
    <div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-transfer"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_263" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/cashflowSubmenuDropdown.jsp" />
    	</div>
	</div>
    
    
    
    <%-- <div id="appdashboard-left" >
		<div style="float:left;">
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
					<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
						<s:param name="SetMappingDefinitionID" value="0"/>
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
					</s:url>
					<sj:a
						id="addCustomMappingsLink"
						href="%{addCustomMappingUrl}"
						targets="inputCustomMappingDiv"
						title="%{getText('jsp.default_135')}"
						indicator="indicator"
						button="true"
						buttonIcon="ui-icon-plus"
						onClickTopics="beforeCustomMappingsOnClickTopics" 
						onCompleteTopics="addCustomMappingsOnCompleteTopics" 
						onErrorTopics="errorCustomMappingsOnErrorTopics"
						style="display:none;"
					>
						<s:text name="jsp.default_34" />
					</sj:a> 
				</ffi:cinclude>
			</ffi:cinclude>
		</div>
	</div>
	 --%>
	<%-- Following three dialogs are used for view, delete and inquiry transfer --%>
	
	<sj:dialog id="InquiryTransferDialogID" cssClass="cashMgmtDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>

<script>
	$("#ppayFileUploadSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>
		
	