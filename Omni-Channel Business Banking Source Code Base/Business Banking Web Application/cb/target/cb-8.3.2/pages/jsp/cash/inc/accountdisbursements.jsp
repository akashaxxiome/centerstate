<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@page import="com.ffusion.beans.disbursement.DisbursementSummaries" %>
<%@page import="com.ffusion.beans.disbursement.DisbursementSummary" %>

<%
	String DisbursementCriteriaDate = request.getParameter("DisbursementCriteriaDate");
	String DisbursementDataClassification = request.getParameter("DisbursementDataClassification");
	
	session.setAttribute("DisbursementCriteriaDate", request.getParameter("DisbursementCriteriaDate"));
	session.setAttribute("DisbursementDataClassification", request.getParameter("DisbursementDataClassification"));
%>

<ffi:cinclude value1="" value2="${DisbursementDataClassification}" operator="equals">
	<ffi:setProperty name="DisbursementDataClassification" value="I"/>
</ffi:cinclude>	

<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="DisbursementDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
	<ffi:setProperty name="DisbursementDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
</ffi:cinclude>

<ffi:object id="GetDisbursementSummariesForAccount" name="com.ffusion.tasks.disbursement.GetSummaries" scope="session"/>
<ffi:setProperty name="GetDisbursementSummariesForAccount" property="DateFormat" value="${DateFormat}"/>
<ffi:setProperty name="GetDisbursementSummariesForAccount" property="StartDate" value="${DisbursementCriteriaDate}"/>
<ffi:setProperty name="GetDisbursementSummariesForAccount" property="EndDate" value="${DisbursementCriteriaDate}"/>
<ffi:setProperty name="GetDisbursementSummariesForAccount" property="DataClassification" value="${DisbursementDataClassification}"/>
<ffi:setProperty name="GetDisbursementSummariesForAccount" property="BatchSize" value="50"/>

<ffi:process name="GetDisbursementSummariesForAccount"/>

				<%
                  session.setAttribute("FFIGetDisbursementSummariesForAccount", session.getAttribute("GetDisbursementSummariesForAccount"));
                 %>

<%-- need to set the start date for display.  if it was not set during task processing, set it to the current date. --%>
<ffi:cinclude value1="${GetDisbursementSummariesForAccount.StartDate}" value2="" operator="equals">
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
	<ffi:process name="GetCurrentDate"/>
				<%
                  session.setAttribute("FFIGetCurrentDate", session.getAttribute("GetCurrentDate"));
                 %>
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${DateFormat}" />
	<ffi:setProperty name="GetDisbursementSummariesForAccount" property="StartDate" value="${GetCurrentDate.Date}"/>
</ffi:cinclude>
	
<ffi:setProperty name="DisbursementSummaries" property="SortedBy" value="${acctDisbursementSortCriteria}"/>


<table width="750" border="0" cellspacing="0" cellpadding="0" style="display:none">
	<tr>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="equals">
			<td colspan="7" align="left" width="750" height="14" background="/cb/web/multilang/grafx/sechdr_blank.gif"><span class="sectiontitle sectionsubhead" >&nbsp; > <s:text name="jsp.cash_ControlledDisbursementsByAccount" /></span></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="notEquals">
			<td colspan="7" align="left" width="750" height="14" background="/cb/web/multilang/grafx/cash/sechdr_blank.gif"><span class="sectiontitle sectionsubhead" >&nbsp; > <s:text name="jsp.cash_128" /><ffi:getProperty name="GetDisbursementSummariesForAccount" property="StartDate"/></span></td>
		</ffi:cinclude>
	</tr>
	<tr>
		<td class="tbrd_lt">&nbsp;</td>
		<td class="tbrd_t" align="right" width="65">&nbsp;</td>
		<td class="tbrd_t" colspan="3" align="center" nowrap><span class="sectionsubhead_dim">&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;<s:text name="jsp.default_428" />&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;&#150;</span></td>
		<td class="tbrd_t" align="center" nowrap><span class="sectionsubhead_dim">&#150;&#150;&#150;&#150;<s:text name="jsp.default_OneDay" />&#150;&#150;&#150;&#150;</span></td>
		<td class="tbrd_tr" align="center" nowrap><span class="sectionsubhead_dim">&#150;&#150;&#150;&#150;<s:text name="jsp.default_TwoDay" />&#150;&#150;&#150;&#150;</span></td>
	</tr>

	<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
	<ffi:setProperty name="SortImage" property="Collection" value="DisbursementSummaries"/>
	<ffi:process name="SortImage"/>
	<ffi:setProperty name="SortImage" property="Compare" value="ACCOUNTID"/>

	<tr>
		<td class="tbrd_lb">
			<table border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.account_7" /></span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ACCOUNTID" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=ACCOUNTID,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ACCOUNTID,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=ACCOUNTID"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ACCOUNTID,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ACCOUNTID" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=ACCOUNTID"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>						
					</td>
				</tr>
				<tr>
		            <ffi:setProperty name="SortImage" property="Compare" value="NICKNAME"/>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.default_293" /></span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NICKNAME" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=NICKNAME,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NICKNAME,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=NICKNAME"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NICKNAME,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NICKNAME" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=NICKNAME"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>

		<ffi:setProperty name="SortImage" property="Compare" value="CURRENTBALANCE"/>
		<td class="tbrd_b" align="right" width="65">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.default_60" /></span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="CURRENTBALANCE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=CURRENTBALANCE,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="CURRENTBALANCE,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=CURRENTBALANCE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="CURRENTBALANCE,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="CURRENTBALANCE" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=CURRENTBALANCE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>

		<ffi:setProperty name="SortImage" property="Compare" value="NUMITEMSPENDING"/>

		<td class="tbrd_b" align="center" width="65">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.cash_59" /></span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NUMITEMSPENDING" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=NUMITEMSPENDING,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NUMITEMSPENDING,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=NUMITEMSPENDING"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NUMITEMSPENDING,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="NUMITEMSPENDING" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=NUMITEMSPENDING"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>

		<ffi:setProperty name="SortImage" property="Compare" value="TOTALDEBITS"/>

		<td class="tbrd_b" align="right" width="65">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.billpay_amount" /></span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TOTALDEBITS" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=TOTALDEBITS,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>	
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TOTALDEBITS,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=TOTALDEBITS"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>	
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TOTALDEBITS,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TOTALDEBITS" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=TOTALDEBITS"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>	
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>

		<ffi:setProperty name="SortImage" property="Compare" value="IMMEDIATEFUNDSNEEDED"/>

		<td class="tbrd_b" align="right" width="65">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.cash_FundsNeeded" /></span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="IMMEDIATEFUNDSNEEDED" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=IMMEDIATEFUNDSNEEDED,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="IMMEDIATEFUNDSNEEDED,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=IMMEDIATEFUNDSNEEDED"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="IMMEDIATEFUNDSNEEDED,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="IMMEDIATEFUNDSNEEDED" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=IMMEDIATEFUNDSNEEDED"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
				</tr>
			</table>
		</td>

		<ffi:setProperty name="SortImage" property="Compare" value="ONEDAYFUNDSNEEDED"/>

		<td class="tbrd_b" align="right" width="65">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.cash_FundsNeeded" />*</span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ONEDAYFUNDSNEEDED" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=ONEDAYFUNDSNEEDED,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ONEDAYFUNDSNEEDED,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=ONEDAYFUNDSNEEDED"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>	
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ONEDAYFUNDSNEEDED,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="ONEDAYFUNDSNEEDED" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=ONEDAYFUNDSNEEDED"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>								
					</td>
				</tr>
			</table>
		</td>

		<ffi:setProperty name="SortImage" property="Compare" value="TWODAYFUNDSNEEDED"/>

		<td class="tbrd_br" align="right" width="65">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.cash_FundsNeeded" />*</span></td>
					<td>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TWODAYFUNDSNEEDED" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=TWODAYFUNDSNEEDED,REVERSE"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TWODAYFUNDSNEEDED,REVERSE" operator="equals">
							<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=TWODAYFUNDSNEEDED"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TWODAYFUNDSNEEDED,REVERSE" operator="notEquals">
							<ffi:cinclude value1="${acctDisbursementSortCriteria}" value2="TWODAYFUNDSNEEDED" operator="notEquals">
								<ffi:link url="?DisbursementCriteriaDate=${DisbursementCriteriaDate}&DisbursementDataClassification=${DisbursementDataClassification}&acctDisbursementSortCriteria=TWODAYFUNDSNEEDED"><ffi:getProperty name="SortImage" encode="false"/></ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>

					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="1">
		<td align="center" height="1" colspan="7"><img src="/cb/web/multilang/grafx/account/spacer.gif" height="1" width="750" border="0"></td>
	</tr>
	<%-- In case of Accounts we have tasks that filter the accounts based on what the user is entitled to. 
       	     For lockbox and disbursements we don't have such a task, and since there are only three jsp's that
	     need this customization, it is done as a scriptlet (instead of a task). --%>
	<% 	boolean toggle = false; %>
	<% int i=0; %>
	<% DisbursementSummaries dss = (DisbursementSummaries)(session.getAttribute("DisbursementSummaries")); %>

		<%-- A flag to see if there's anything in the list of accounts that we are entitled to --%>
		<ffi:setProperty name="emptyList" value="true"/>
		<ffi:list collection="DisbursementSummaries" items="Summary">
			<%
				DisbursementSummary ds = (DisbursementSummary)(dss).get(i);
				String accountID = ""; 
				if( ds!=null) {	
					accountID = com.sap.banking.web.util.entitlements.EntitlementsUtil.getEntitlementObjectId(ds.getAccount());
				}
				i++;
			%>
			<ffi:setProperty name="showAccount" value=""/>
			<ffi:setProperty name="showDetailLink" value=""/>
			<ffi:cinclude value1="${DisbursementDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>" operator="equals">
				<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
					<ffi:setProperty name="showAccount" value="true"/>	
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
					<ffi:setProperty name="showAccount" value="true"/>	
					<ffi:setProperty name="showDetailLink" value="true"/>	
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${DisbursementDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
				<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
					<ffi:setProperty name="showAccount" value="true"/>	
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>" objectType="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="<%=accountID %>">
					<ffi:setProperty name="showAccount" value="true"/>	
					<ffi:setProperty name="showDetailLink" value="true"/>	
				</ffi:cinclude>
			</ffi:cinclude>
	
			<%-- show only if entitled for this account --%>
			<ffi:cinclude value1="${showAccount}" value2="true" operator="equals">
				<%-- We found at least one account to display --%>
				<ffi:setProperty name="emptyList" value="false"/>
				<%	toggle = !toggle;
					if (toggle) { 	%>
						<tr class="ltrow2_color">
				<% 	} else { 	%>
						<tr>
				<% 	} 		%>
	
					<td class="columndata"><ffi:getProperty name="Summary" property="Account.RoutingNumber"/> : 
					
					<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="session"/>
					<ffi:setProperty name="AccountDisplayTextTask" property="AccountID" value="${Summary.Account.AccountID}"/>
					<ffi:process name="AccountDisplayTextTask"/>
					
				<%
                  session.setAttribute("FFIAccountDisplayTextTask", session.getAttribute("AccountDisplayTextTask"));
                 %>
					
					<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
					<ffi:removeProperty name="AccountDisplayTextTask"/>
						<ffi:cinclude value1="${Summary.Account.AccountName}" value2="" operator="notEquals" >
							 - <ffi:getProperty name="Summary" property="Account.AccountName"/>
				 		</ffi:cinclude>	
						 - <ffi:getProperty name="Summary" property="Account.CurrencyType"/>
					</td>
					<td class="columndata" align="right" width="65"><ffi:getProperty name="Summary" property="CurrentBalance.CurrencyStringNoSymbol"/></td>
					<td class="columndata" align="center" width="65">
						<ffi:cinclude value1="${Summary.NumItemsPending}" value2="-1" operator="notEquals">
							<ffi:cinclude value1="${showDetailLink}" value2="true" operator="equals">
								<ffi:link url="${SecurePath}cash/cashdisbursedetail.jsp?AccountID=${Summary.Account.AccountID}&Date=${GetDisbursementSummariesForAccount.StartDate}&DataClassification=${DisbursementDataClassification}&disbursementtransactions-reload=true">
									<ffi:getProperty name="Summary" property="NumItemsPending"/>
								</ffi:link>
							</ffi:cinclude>
							<ffi:cinclude value1="${showDetailLink}" value2="true" operator="notEquals">
								<ffi:getProperty name="Summary" property="NumItemsPending"/>
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
					<td class="columndata" align="right" width="65"><ffi:getProperty name="Summary" property="TotalDebits.CurrencyStringNoSymbol"/></td>
					<td class="columndata" align="right" width="65"><ffi:getProperty name="Summary" property="ImmediateFundsNeeded.CurrencyStringNoSymbol"/></td>
					<td class="columndata" align="right" width="65"><span class="columndata"><ffi:getProperty name="Summary" property="OneDayFundsNeeded.CurrencyStringNoSymbol"/></span></td>
					<td class="columndata" align="right" width="65"><ffi:getProperty name="Summary" property="TwoDayFundsNeeded.CurrencyStringNoSymbol"/></td>
				</tr>
			</ffi:cinclude>
		</ffi:list>
		<%-- If not displayed any accounts, display a message --%>
		<ffi:cinclude value1="${emptyList}" value2="true" operator="equals">
	        	<tr class="ltrow2_color">
	                	<td class="columndata" align="center" colspan="9"><s:text name="jsp.cash_109" /></td>
	                </tr>
        	</ffi:cinclude>
	<tr>
		<td class="tbrd_ltb" colspan="5"><span class="columndata">* <s:text name="jsp.home_202" /></span></td>
		<td class="tbrd_tb">&nbsp;</td>
		<td class="tbrd_trb">&nbsp;</td>
	</tr>
	<tr>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="equals">
			<td colspan="7"><img src="/cb/web/multilang/grafx/sechdr_btm.gif" alt="" width="750" height="11" border="0"></td>
		</ffi:cinclude>
		<ffi:cinclude value1="${PortalFlag}" value2="true" operator="notEquals">
			<td colspan="7"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" alt="" width="750" height="11" border="0"></td>
		</ffi:cinclude>
	</tr>
</table>
<ffi:setProperty name="DisburseDate" value="${GetDisbursementSummariesForAccount.StartDate}"/>
<s:include value="/pages/jsp/cash/cashaccdisbursement_grid.jsp"/> 
<script>

	//ns.common.initializePortlet("AccountDisbursementIcon",true); 
	$(function(){
		var title = js_disbursement_portlet_title;
		var startDate = '<ffi:getProperty name="GetDisbursementSummariesForAccount" property="StartDate"/>';				
		//$("#AccountDisbursementIcon").portlet('title', title +" "+ startDate);
	});
</script>
<ffi:removeProperty name="BackToURL"/>
<ffi:removeProperty name="DateFormat"/>
<ffi:removeProperty name="acctDisbursementSortCriteria"/>
<ffi:removeProperty name="SortImage"/>
