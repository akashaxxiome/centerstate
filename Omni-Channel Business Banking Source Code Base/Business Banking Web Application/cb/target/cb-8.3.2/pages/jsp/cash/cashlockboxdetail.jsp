<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%	
	session.setAttribute("AccountID", request.getParameter("AccountID"));
	session.setAttribute("LockboxNumber", request.getParameter("LockboxNumber"));
	session.setAttribute("LockboxDataClassification", request.getParameter("LockboxDataClassification"));
	session.setAttribute("lockboxcredititems-reload", request.getParameter("lockboxcredititems-reload"));
	session.setAttribute("LockboxCriteriaDate", request.getParameter("LockboxCriteriaDate"));
%>	


<%-- ADDED BY TCG FOR CHECKIMAGING --%>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>
<ffi:setProperty name="SearchImage" property="SuccessURL" value="${SecurePath}cash/checkcouponimage.jsp" />

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="get-image-flag" value="true"/>

<ffi:object id="LockboxCreditItemDetail" name="com.ffusion.tasks.lockbox.LockboxCreditItemDetailTask" scope="session"/>

<ffi:cinclude value1="${lockboxcredititemsReload}" value2="false" operator="notEquals">
    <ffi:object id="GetLockboxCreditItems" name="com.ffusion.tasks.lockbox.GetPagedCreditItems" scope="session"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="DateFormat" value="${DateFormat}"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="StartDate" value="${LockboxCriteriaDate}"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="EndDate" value="${LockboxCriteriaDate}"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="Page" value="first"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="DataClassification" value="${LockboxDataClassification}"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="AccountID" value="${AccountID}"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="LockboxNumber" value="${LockboxNumber}"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="PageSize" value="20"/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetLockboxCreditItems" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetLockboxCreditItems" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetLockboxCreditItems" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <ffi:setProperty name="GetLockboxCreditItems" property="SortCriteriaOrdinal" value="1"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="SortCriteriaName" value="<%= com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_PAYER %>"/>
    <ffi:setProperty name="GetLockboxCreditItems" property="SortCriteriaAsc" value="True"/>
    <ffi:setProperty name="lockboxcredititems-reload" value="false"/>
</ffi:cinclude>
<ffi:process name="GetLockboxCreditItems"/>

				<%
							session.setAttribute("FFIGetLockboxCreditItems", session.getAttribute("GetLockboxCreditItems"));
				%>

<%-- get entitlement object id --%>
<%
	String objectID = null;
%>
<%-- Get entitlement object ID --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetEntitlementObjectID"/>
<ffi:setProperty name="GetEntitlementObjectID" property="ObjectType" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="${GetLockboxCreditItems.Account.AccountID}" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_BANK_ID %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="${GetLockboxCreditItems.Account.BankID}" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="${GetLockboxCreditItems.Account.RoutingNumber}" />
<ffi:process name="GetEntitlementObjectID" />
<ffi:getProperty name="GetEntitlementObjectID" property="ObjectID" assignTo="objectID" />

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashlockboxdetail.jsp" URLEncrypt="true"/>

			<table width="750" border="0" cellspacing="0" cellpadding="0" style="display:none">
				<tr valign="bottom">
					<td colspan="9" width="750" background="/cb/web/multilang/grafx/cash/sechdr_blank.gif" class="sectiontitle">
					  &nbsp;&nbsp; &gt; <s:text name="jsp.cash_65"/>
						<ffi:setProperty name="BankingAccounts" property="Filter" value="ID=${GetLockboxCreditItems.AccountID}"/>
						<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
								<ffi:list collection="BankingAccounts" items="acct" startIndex="1" endIndex="1">
										<s:text name="jsp.default_15"/> <ffi:getProperty name="acct" property="RoutingNum"/> : 
										<ffi:getProperty name="acct" property="DisplayText"/> - 
										<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
											<ffi:getProperty name="acct" property="NickName"/> -
								 		</ffi:cinclude>
										<ffi:getProperty name="acct" property="CurrencyCode"/> <s:text name="jsp.cash_74"/>
								</ffi:list>
						</ffi:cinclude>
						<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
						<ffi:getProperty name="GetLockboxCreditItems" property="StartDate"/>
					</td>
				</tr>
				<tr valign="bottom">
					<td class="tbrd_ltb" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_PAYER %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_77"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="right" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_AMOUNT %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_29"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="center" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_CHECK_NUMBER %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.default_92"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="center" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_COUPON_ACCT_NUM %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_37"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="right" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_COUPON_AMOUNT1 %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_38"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="right" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_COUPON_AMOUNT2 %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_39"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="center" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_COUPON_DATE1 %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_40"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" align="center" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_COUPON_DATE2 %>"/>
								<td><span class="sectionsubheadb"><s:text name="jsp.cash_41"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_trb" align="right" nowrap>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <ffi:setProperty name="GetLockboxCreditItems" property="CompareSortCriterion" value="<%=  com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_COUPON_REF_NUM %>"/>
								<td nowrap><span class="sectionsubheadb"><s:text name="jsp.default_529"/></span></font></td>
							  <td><ffi:link url="?AccountID=${AccountID}&GetLockboxCreditItems.Page=first&GetLockboxCreditItems.SortedBy=${GetLockboxCreditItems.ToggleSortedBy}"><ffi:getProperty name="GetLockboxCreditItems" property="SortImage" encode="false"/></ffi:link></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr valign="middle">
					<td nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="70" height="1" border="0"></td>
					<td nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="65" height="1" border="0"></td>
					<td align="center" nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="80" height="1" border="0"></td>
					<td nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="55" height="1" border="0"></td>
					<td nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="75" height="1" border="0"></td>
					<td nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="75" height="1" border="0"></td>
					<td align="center" nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="80" height="1" border="0"></td>
					<td align="center" nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="70" height="1" border="0"></td>
					<td nowrap class="ltrow2_color"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="65" height="1" border="0"></td>
				</tr>
	<ffi:cinclude value1="0" value2="${LOCKBOX_CREDIT_ITEMS.Size}" operator="equals">
		<tr>
			<td class="columndata" height="20" colspan="9" align="center"><s:text name="jsp.cash_102"/></td>
		</tr>
	</ffi:cinclude>


	<%-- sort the list --%>
	<ffi:cinclude value1="${LockboxDetailListSortCriteria}" value2="" operator="notEquals">
		<ffi:setProperty name="LOCKBOX_CREDIT_ITEMS" property="SortedBy" value="${LockboxDetailListSortCriteria}"/>
	</ffi:cinclude>
	<%
	boolean imagingEntitled = false;
	%>
	<%-- do this so we don't repeat check entitlements inside the loop --%>
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHECK_IMAGING %>" objectType="Account" objectId="<%= objectID %>" >
		<% imagingEntitled = true; %>
	</ffi:cinclude>

	<% boolean toggle = false; %>
	<ffi:list collection="LOCKBOX_CREDIT_ITEMS" items="LOCKBOX_CREDIT_ITEMS_1">
		<%-- Using different style on different lines --%>
		<%	toggle = !toggle;
			if (toggle) { 	%>
				<tr class="columndata_grey">
		<% 	} else { 	%>
				<tr class="columndata">
		<% 	} 		%>
		<td nowrap>
			<ffi:link url="cashlockboxrecord.jsp?LockboxCreditItemDetail.ItemIndex=${LOCKBOX_CREDIT_ITEMS_1.ItemIndex}">
				<ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="Payer"/>
			</ffi:link>
		</td>
		<td  nowrap align="right"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CheckAmount.CurrencyStringNoSymbol" default="&nbsp;"/></td>
		<td  nowrap align="center"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CheckNumber"/></td>
		<td  nowrap align="center"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CouponAccountNumber"/></td>
		<td  nowrap align="right"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CouponAmount1.CurrencyStringNoSymbol" default="&nbsp;"/></td>
		<td  nowrap align="right"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CouponAmount2.CurrencyStringNoSymbol" default="&nbsp;"/></td>
		<td  nowrap align="center"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CouponDate1String"/></td>
		<td  nowrap align="center"><ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CouponDate2String"/></td> 
		<td  nowrap align="center">
			<%
			if( imagingEntitled ) {
			%>
				<ffi:setProperty name='Temp1' value='${SecureServletPath}SearchImage?MODULE=LOCKBOX&SearchImage.ItemID=${LOCKBOX_CREDIT_ITEMS_1.ItemID}' URLEncrypt="true"/>
				<a onclick="CSOpenImageWindow('<ffi:urlEncrypt url="${Temp1}" />');" href="#" csclick="<ffi:getProperty name='LOCKBOX_CREDIT_ITEMS_1' property='ItemID'/>" >
					<ffi:getProperty name="LOCKBOX_CREDIT_ITEMS_1" property="CouponReferenceNumber"/>
				</a>
			<%
			}
			%>
		</td>
		</tr>
	</ffi:list>
        <tr>
          <td class="tbrd_ltb" colspan="7" valign="middle">&nbsp;<span class="columndata">&nbsp;&nbsp;&nbsp;&nbsp;
						<ffi:cinclude value1="${GetLockboxCreditItems.IsFirstPage}" value2="false" operator="equals">
								<ffi:link url='?GetLockboxCreditItems.Page=previous&AccountID=${AccountID}&DataClassification=${DataClassification}'>
									<img src="/cb/web/multilang/grafx/buttons_previous.gif" alt="" width="68" height="16" border="0" hspace="6">
								</ffi:link>
						</ffi:cinclude>
						<ffi:cinclude value1="${GetLockboxCreditItems.IsLastPage}" value2="false" operator="equals">
								<ffi:link url='?GetLockboxCreditItems.Page=next&AccountID=${AccountID}&DataClassification=${DataClassification}'>
									<img src="/cb/web/multilang/grafx/button_next.gif" alt="" width="68" height="16" border="0">
								</ffi:link>
						</ffi:cinclude>
          </span></td>
          <td class="tbrd_tb" width="142"><br></td>
          <td class="tbrd_trb"><br></td>
        </tr>
				<tr>
					<td colspan="9"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" height="11" width="750"></td>
				</tr>
			</table>

	<table width="750" border="0" cellspacing="0" cellpadding="0" style="display:none">
		<tr>
		<br>
			<td colspan="7" align="center">
				<input class="submitbutton" type="button" value="<s:text name="jsp.default_57"/>" onClick="location.replace('<ffi:urlEncrypt url="${SecurePath}cash/cashlockboxdeposits.jsp?AccountID=${AccountID}&lockboxtransactionsReload=true&DataClassification${DataClassification}"/>')">
			</td>
		</tr>
	</table>
	
		<div id="lockboxDetailGrid">

	</div>	
	
	
	<script>
	$(document).ready(function(){
		$('#lockboxDetailGrid').portlet({
			generateDOM: true,
			helpCallback: function(){
				var helpFile = $('#lockboxDetailGrid').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
		});	
		
		ns.cash.refreshCashLockboxDetailGridURL = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/cashlockboxdetail_grid.jsp'/>";
		ns.cash.refreshCashLockboxDetailGrid(ns.cash.refreshCashLockboxDetailGridURL);
		var title = js_lockbox_detail;
		var routingNum = "<ffi:getProperty name="acct" property="RoutingNum"/>";
		var displayText = "<ffi:getProperty name="acct" property="DisplayText"/>";
		var nickName = "<ffi:getProperty name="acct" property="NickName"/>";
		var currencyCode = "<ffi:getProperty name="acct" property="CurrencyCode"/>";
		var startDate = "<ffi:getProperty name="LockboxDate"/>";	
		$("#lockboxDetailGrid").portlet('title', + " "+ routingNum + ":" + displayText + "-" + nickName + "-" + currencyCode + " on " + startDate);
	});
	
</script>
	
	<br>
	<div align="center">
	<sj:a id="backFormButtonForLockDetail" 
			button="true" 
			onClickTopics="GoBackToLockPost"
	><s:text name="jsp.default_57"/></sj:a> 
	</div>

<ffi:removeProperty name="TempPreviousPage"/>
<ffi:removeProperty name="TempNextPage"/>
<ffi:removeProperty name="GetEntitlementObjectID"/>
<ffi:removeProperty name="AccountID"/>
