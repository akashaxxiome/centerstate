<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<div id="lockboxIcon">

	<ffi:help id="cash_cashlockbox" />
	<ffi:setGridURL grid="GRID_lockboxGrid" name="ViewURL" url="/cb/pages/jsp/cash/cashlockboxdeposits.jsp?AccountID={0}&lockboxtransactionsReload=true&DataClassification={1}" parm0="LockboxAccountID" parm1="dataClassification"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/cash/GetLockboxAction.action?collectionName=LOCKBOX_SUMMARIES&GridURLs=GRID_lockboxGrid" URLEncrypt="true"/>
    <s:url id="cashlockboxsummaryURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="cashlockboxsummaryID"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{cashlockboxsummaryURL}"  
		pager="true"
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		gridModel="gridModel" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		reloadTopics="reloadInboxMessages"
		sortname="name" 
		onGridCompleteTopics="cashlockboxsummaryEvent,addGridControlsEvents"
		> 
		<sjg:gridColumn name="accountDisplayText" index="name" title="%{getText('jsp.default_15')}" sortable="true" width="250" formatter="ns.cash.customToCashLockboxColumn" cssClass="datagrid_actionColumn"/>
	    <sjg:gridColumn name="lockboxAccount.routingNumber" index="routingNumber" title="%{getText('jsp.cash_96')}" sortable="true" width="150" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
	    <sjg:gridColumn name="lockboxAccountID" index="lockboxAccountID" title="%{getText('jsp.cash_68')}" sortable="true" width="150" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="lockboxAccountNickname" index="lockboxAccountNickname" title="%{getText('jsp.cash_69')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="lockboxAccount.currencyType" index="currencyType" title="%{getText('jsp.cash_43')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="totalLockboxCredits.currencyStringNoSymbol" index="totalLockboxCredits.currencyStringNoSymbol" title="%{getText('jsp.default_169')}" sortable="true" width="90" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="totalNumLockboxCredits" index="totalNumLockboxCredits" title="%{getText('jsp.default_123')}" sortable="true" width="50" cssClass="datagrid_amountColumn"/>
		<sjg:gridColumn name="immediateFloat.currencyStringNoSymbol" index="immediateFloat.currencyStringNoSymbol" title="%{getText('jsp.default_234')}" sortable="true" width="110" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="oneDayFloat.currencyStringNoSymbol" index="oneDayFloat.currencyStringNoSymbol" title="%{getText('jsp.default_10')}" sortable="true" width="90" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="twoDayFloat.currencyStringNoSymbol" index="twoDayFloat.currencyStringNoSymbol" title="%{getText('jsp.default_12')}" sortable="true" width="90" hidden="" cssClass="datagrid_textColumn"/>
		
		<sjg:gridColumn name="map.dataClassification" index="map.dataClassification" title="%{getText('jsp.cash_70')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.date" index="map.date" title="%{getText('jsp.cash_71')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="lockboxAccount.displayText" index="displayText" title="%{getText('jsp.cash_53')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<%--for double sorting --%>
		<%-- <sjg:gridColumn name="name" index="name" title="%{getText('jsp.default_15')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/> --%>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		
	</sjg:grid>	
</div>


