<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="cash_cashppaybuild" />
<span class="shortcutPathClass" style="display:none;">cashMgmt_ppay,ppaybuildLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Positive Pay</span>

<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='Temp1' value='${SecurePath}cash/cashppaybuild.jsp?buildFromBlank=true' URLEncrypt="true"/>
<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}cash/cashppupload.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_uploadrecord.gif" alt="" width="150" height="16" border="0" hspace="6"></a><a href="${Temp1}"><img src="/cb/pages/${ImgExt}grafx/cash/button_buildmanualfile_dim.gif" alt="" width="120" height="16" border="0"></a>'/>

<ffi:cinclude value1="${ppayBuildTouched}" value2="" operator="equals">
    <ffi:setProperty name="ppayBuildTouched" value="false"/>
    <ffi:object id="CreateCheckRecords" name="com.ffusion.tasks.positivepay.CreateCheckRecords" scope="session"/>
    <%-- ffi:setProperty name="CreateCheckRecords" property="invalidInformationEnteredURL" value="invalid-input.jsp"/>
    <ffi:setProperty name="CreateCheckRecords" property="AccountSeparator" value="|"/>
    <ffi:setProperty name="UserResource" property="ResourceID" value="<%= com.ffusion.beans.user.UserLocale.DATEFORMAT %>"/>
    <ffi:setProperty name="CreateCheckRecords" property="CheckDateEmptyString" value="${UserResource.Resource}"/ --%>

				 <%
					session.setAttribute("FFICreateCheckRecords", session.getAttribute("CreateCheckRecords"));
                 %>
</ffi:cinclude>

<%-- When coming to this page upon clicking build manual file --%>
<ffi:cinclude value1="${buildFromBlank}" value2="true" operator="equals">
    <%-- Sets the number of rows to be displayed --%>
    <ffi:object id="CreateEmptyCheckRecords" name="com.ffusion.tasks.positivepay.CreateEmptyCheckRecords" scope="session"/>
    <ffi:setProperty name="CreateEmptyCheckRecords" property="NumberOfRecords" value="15"/>
    <ffi:process name="CreateEmptyCheckRecords"/>
    <ffi:removeProperty name="CreateEmptyCheckRecords"/>
</ffi:cinclude>
<ffi:object id="Counter" name="com.ffusion.beans.util.IntegerMath" scope="session"/>


<script type="text/javascript"><!--

ns.common.updatePortletTitle("ppay_workflowID",js_ppay_build_portlet_title,false);

$(function(){
	for(var i = 0; i < 15; i++){
		//$("#accountID" + i).combobox({'size':'50'});
		$("#accountID" + i).selectmenu({width: 390});
   	}
});


CSStopExecution=false;
CSAct = new Object;
function CSClickReturn () {
    var bAgent = window.navigator.userAgent;
    var bAppName = window.navigator.appName;
    if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
        return true; // dont follow link
    else return false; // dont follow link
}
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
    var result;
    for (var i=0;i<array.length;i++) {
        if(CSStopExecution) return false;
        var aa = fct[array[i]];
        if (aa == null) return false;
        var ta = new Array;
        for(var j=1;j<aa.length;j++) {
            if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
                if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
                else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
                else ta[j]=aa[j];}
            } else ta[j]=aa[j];
        }
        result=aa[0](ta);
    }
    return result;
}
function CSGotoLink(action) {
    if (action[2].length) {
        var hasFrame=false;
        for(i=0;i<parent.frames.length;i++) { if (parent.frames[i].name==action[2]) { hasFrame=true; break;}}
        if (hasFrame==true)
            parent.frames[action[2]].location = action[1];
        else
            window.open (action[1],action[2],"");
    }
    else location = action[1];
}
function CSOpenWindow(action) {
	var wf = "width=" + action[3];
        wf = wf + ",height=" + action[4];
        wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
        wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
        wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
        wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
        wf = wf + ",directories=" + (action[9] ? "yes" : "no");
        wf = wf + ",location=" + (action[10] ? "yes" : "no");
        wf = wf + ",status=" + (action[11] ? "yes" : "no");
        window.open(action[1], action[2], wf);
}
CSAct[/*CMP*/ 'B9E9A203159'] = new Array(CSGotoLink,/*URL*/ 'cashppaybuildconfirm.jsp','');
CSAct[/*CMP*/ 'B9ED916921'] = new Array(CSGotoLink,/*URL*/ 'cashppay.jsp','');


<ffi:setProperty name="Counter" property="Value1" value="1"/>
<ffi:setProperty name="Counter" property="Value2" value="0"/>

<%--Make a few arrays for opening the calendar window--%>
<%--The number of arrays is the same as the number of items in the PPayCheckRecords--%>
<ffi:list collection="PPayCheckRecords" items="CheckRecord1">
    CSAct[/*CMP*/ 'B9E611E8165' + '<ffi:getProperty name="Counter" property="Value2"/>'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="{SecurePath}calendar.jsp?calDiretion=back&calForm=BuildForm&calTarget=checkDate${Counter.Value2}"/>','',350,300,false,false,false,false,false,false,false);
    <ffi:setProperty name="Counter" property="Value2" value="${Counter.Add}" />
</ffi:list>


<%-- include javascripts for calculation of totals --%>
<%-- Change 2.8 in the PositivePayDocumentDDD.doc in WARP folder --%>


var CSRowNumber = <ffi:getProperty name="PPayCheckRecords" property="size"/>;
<ffi:object id="GetCurrencyNumDecimals" name="com.ffusion.tasks.fx.GetCurrencyNumDecimals" scope="session"/>
<ffi:process name="GetCurrencyNumDecimals"/>

var CSDecimalPlace = <ffi:getProperty name="GetCurrencyNumDecimals" property="NumDecimals" /> ;<%-- This variable shoud be later populated with the help of the user session locale  --%>
var CSDecimalDummy = Math.pow(10,CSDecimalPlace);<%-- This variable is used for javascript rounding --%>


<%-- This is a utility adds trailing zeros after the decimal place to ensure proper alignment --%>
function CSFormatDecimalPlaces(pNumber,pDecimalPosition){

	var   sAmount = pNumber.toString();
	var   iNumberOfZeros = 0;
	var   iDotPostition = sAmount.indexOf('.');

	if ( iDotPostition == -1){
		 sAmount= sAmount+".";
		 iNumberOfZeros = pDecimalPosition;
	}else{
		iNumberOfZeros = CSDecimalPlace - sAmount.length + iDotPostition + 1 ;
		if (iNumberOfZeros >= CSDecimalPlace){
			iNumberOfZeros = 0;
		}
	}


	for (i=1;i<= iNumberOfZeros;i++){
		sAmount = sAmount+"0";
	}
	return sAmount;

}

function CSCalculateSum(){
	var amountSum = 0 ;

	for(i=0;i< CSRowNumber;i++){
		var amount = eval("document.BuildForm.amount"+i+".value");
		if(isNaN(amount)){
		  continue;
		}
		var fAmount =  parseFloat(amount);
		if(fAmount > 0 ){
			amountSum  = amountSum + fAmount;
			amountSum  = Math.round(amountSum*CSDecimalDummy)/CSDecimalDummy;
		}
	}

	document.getElementById('amountTotal').innerHTML= CSFormatDecimalPlaces(amountSum,CSDecimalPlace);
	return true;
}


function CSValidateAmount(object){


	if(!(object.value =="" ||object.value ==null)){
			object.value = Math.round(object.value*CSDecimalDummy)/CSDecimalDummy ;
			object.value = CSFormatDecimalPlaces(object.value ,CSDecimalPlace);
	}

	CSCalculateSum();
}

<%-- JS Code added for Total  field ends here --%>

// --></script>

<BODY onload="preloadImages();CSCalculateSum();">
        <div align="center">


            <s:form namespace="/pages/jsp/cash" action="BuildManualFileAction_execute" method="post" name="BuildForm" id="BuildFormID" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                <div align="center">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <tr><td><span id="buildFormError"></span></td></tr>
                        <tr>

                            <td align="center" class="ltrow2_color">
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="ltrow2_color">
                                    <tr valign="middle">
                                        <td class="tbrd_b" nowrap colspan="6"><span class="sectionhead"><s:text name="jsp.cash_22"/> </span></td>
                                    </tr>
									<br>
                                    <tr valign="middle">
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.default_137"/><span class="required">*</span></td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.default_15"/><span class="required">*</span></td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.default_92"/><span class="required">*</span></td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.default_43"/><span class="required">*</span></td>
                                        <td nowrap class="sectionsubhead"><s:text name="jsp.cash_14"/></td>
                                        <td align="center" nowrap class="sectionsubhead"><s:text name="jsp.cash_122"/></td>
                                    </tr>


                                    <ffi:setProperty name="Counter" property="Value1" value="1"/>
                                    <ffi:setProperty name="Counter" property="Value2" value="0"/>


                                    <%--Populates the CheckRecord fields with gotten info. --%>
                                    <%--This allows us to use this page both for entry, and editing --%>
                                    <ffi:list collection="PPayCheckRecords" items="CheckRecord1">
                                        <tr valign="middle">
					    <td nowrap>
                                           	 <table>
					    		<tr>
					   			 <%--For the date, either output the date if non-empty, or output the default date format set at top of page --%>
                                           			 <td nowrap class="ltrow2_color">

                                           			 <%-- input type="text" name="checkDate<ffi:getProperty name="Counter" property="Value2"/>" size="12" maxlength="10"
                                               				 value=<ffi:cinclude value1="${CheckRecord1.CheckDate}" value2="" operator="equals">"<ffi:getProperty name="CreateCheckRecords" property="CheckDateEmptyString"/>"</ffi:cinclude>
                                                      			 <ffi:cinclude value1="${CheckRecord1.CheckDate}" value2="" operator="notEquals">"<ffi:getProperty name="CheckRecord1" property="CheckDate"/>"</ffi:cinclude> onFocus="select()" class="txtbox" --%>


                                                      			 <sj:datepicker value="%{#session.CheckRecord1.CheckDate}" name="checkDate%{#attr.Counter.Value2}" id="checkDateID%{#attr.Counter.Value2}" displayFormat="mm/dd/yy" label="%{getText('jsp.default_137')}" maxlength="10" buttonImageOnly="true" size="10" cssClass="ui-widget-content ui-corner-all"/>


                                           			 </td>
							</tr>
					   	 </table>
					    </td>
 					    <%--For the drop down, list each account. If no account previously selected, select Choose Account. --%>

                                            <%--Otherwise, select the chosen account, and check to ensure that account isn't listed twice in the list --%>
                                            <td nowrap class="ltrow2_color"><select name="account<ffi:getProperty name="Counter" property="Value2"/>" id="accountID<ffi:getProperty name="Counter" property="Value2"/>" class="txtbox">
                                               <option value="" <ffi:cinclude value1="${CheckRecord1.AccountID}" value2="" operator="equals">selected</ffi:cinclude>><s:text name="jsp.cash_30"/></option>
                                               <ffi:setProperty name="GetAccountData" property="AccountID" value="${CheckRecord1.AccountID}"/>
                                               <ffi:setProperty name="GetAccountData" property="BankID" value="${CheckRecord1.BankID}"/>
                                               <ffi:process name="GetAccountData"/>
                                               <ffi:cinclude value1="${CheckRecord1.AccountID}" value2="" operator="notEquals">
                                                   <option selected value="<ffi:getProperty name="CheckRecord1" property="AccountID"/>|<ffi:getProperty name="CheckRecord1" property="BankID"/>|<ffi:getProperty name="GetAccountData" property="CurrencyType"/>|<ffi:getProperty name="GetAccountData" property="RoutingNumber"/>"><ffi:getProperty name="GetAccountData" property="RoutingNumber"/> : <ffi:getProperty name="CheckRecord1" property="AccountID"/>
														<ffi:cinclude value1="${GetAccountData.NickName}" value2="" operator="notEquals" >
															 - <ffi:getProperty name="GetAccountData" property="NickName"/>
												 		</ffi:cinclude>
														 - <ffi:getProperty name="GetAccountData" property="CurrencyType"/>
													</option>
                                               </ffi:cinclude>
                                               <ffi:list collection="PPaySummaries" items="Summary3">
                                                   <ffi:cinclude value1="${CheckRecord1.AccountID}|${CheckRecord1.BankID}" value2="${Summary3.PPayAccount.AccountID}|${Summary3.PPayAccount.BankID}" operator="notEquals">
                                                       <option value="<ffi:getProperty name="Summary3" property="PPayAccount.AccountID"/>|<ffi:getProperty name="Summary3" property="PPayAccount.BankID"/>|<ffi:getProperty name="Summary3" property="PPayAccount.CurrencyType"/>|<ffi:getProperty name="Summary3" property="PPayAccount.RoutingNumber"/>"><ffi:getProperty name="Summary3" property="PPayAccount.RoutingNumber"/> :

                                                       								<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="session"/>
														<ffi:setProperty name="AccountDisplayTextTask" property="AccountID" value="${Summary3.PPayAccount.AccountID}"/>
														<ffi:process name="AccountDisplayTextTask"/>
														<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
														<ffi:removeProperty name="AccountDisplayTextTask"/>
														<ffi:cinclude value1="${Summary3.PPayAccount.NickName}" value2="" operator="notEquals" >
															 - <ffi:getProperty name="Summary3" property="PPayAccount.NickName"/>
												 		</ffi:cinclude>
														 - <ffi:getProperty name="Summary3" property="PPayAccount.CurrencyType"/></option>
                                                   </ffi:cinclude>
                                               </ffi:list>
                                                </select>
                                            </td>
                                            <td nowrap class="ltrow2_color"><input type="text" name="checkNumber<ffi:getProperty name="Counter" property="Value2"/>" id="checkNumberID<ffi:getProperty name="Counter" property="Value2"/>" size="8" maxlength="20" class="ui-widget-content ui-corner-all" value="<ffi:getProperty name="CheckRecord1" property="CheckNumber"/>"></td>
                                            <td nowrap class="ltrow2_color"><input type="text" style="text-align:right" name="amount<ffi:getProperty name="Counter" property="Value2"/>" id="amountID<ffi:getProperty name="Counter" property="Value2"/>" size="8" maxlength="31" class="ui-widget-content ui-corner-all" value="<ffi:getProperty name="CheckRecord1" property="Amount.CurrencyStringNoSymbolNoComma"/>" onblur='CSValidateAmount(this)'></td>
                                            <td nowrap class="ltrow2_color"><input type="text" name="additionalData<ffi:getProperty name="Counter" property="Value2"/>" id="additionalDataID<ffi:getProperty name="Counter" property="Value2"/>" size="16" maxlength="32" class="ui-widget-content ui-corner-all" value="<ffi:getProperty name="CheckRecord1" property="AdditionalData"/>"></td>
                                            <td align="center" nowrap ><input type="checkbox" name="void<ffi:getProperty name="Counter" property="Value2"/>" value="V" <ffi:cinclude value1="${CheckRecord1.VoidCheck}" value2="true" operator="equals">checked</ffi:cinclude>></td>
                                        </tr>


                                       <tr>
											<td ><span id="checkDateID<ffi:getProperty name="Counter" property="Value2"/>Error"></span></td>
											<td ><span id="accountID<ffi:getProperty name="Counter" property="Value2"/>Error"></span></td>
											<td ><span id="checkNumberID<ffi:getProperty name="Counter" property="Value2"/>Error"></span></td>
											<td ><span id="amountID<ffi:getProperty name="Counter" property="Value2"/>Error"></span></td>
											<td ><span id="additionalDataID<ffi:getProperty name="Counter" property="Value2"/>Error"></span></td>
											<td ></td>
										</tr>

										<ffi:setProperty name="Counter" property="Value2" value="${Counter.Add}" />

                                    </ffi:list>


                                    <tr valign="middle">
										<td nowrap class="sectionsubhead">&nbsp;</td>
										<td nowrap class="sectionsubhead" colspan="2" align="right"><s:text name="jsp.default_432"/></td>
										<td align="center" nowrap class="sectionsubhead"><p style="text-align:center" id="amountTotal">&nbsp;</p></td>
										<td nowrap class="sectionsubhead">&nbsp;</td>
										<td nowrap class="sectionsubhead">&nbsp;</td>
                                    </tr>
				    <tr>
				 	<td colspan="6"><br></td>
				    </tr>
				    <tr>
					<td align="center" colspan="6"><span class="required">* <s:text name="jsp.default_240"/></span></td>
				    </tr>
                                    <tr>
                                        <td colspan="6" align="center"><br>
                                            <%-- input onclick="CSAction(new Array(/*CMP*/'B9ED916921'));" type="button" name="Button2" class="submitbutton" value="CANCEL" csclick="B9ED916921">&nbsp;&nbsp;<input class="submitbutton" type="submit" name="submitButtonName" value="DONE" --%>

                                            <s:url id="buildCancelUrl" value="/pages/jsp/cash/cashppay.jsp" escapeAmp="false">
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
													<s:param name="positivepayReload" value="true"/>
											</s:url>
											<sj:a
												id="cancelBuildFormID"
												href="%{buildCancelUrl}"
												targets="notes"
												indicator="indicator"
												button="true"
												><s:text name="jsp.default_82"/></sj:a>

                                            <sj:a
												id="buildManualFileID"
												formIds="BuildFormID"
												targets="secondDiv"
												button="true"
												validate="true"
												validateFunction="customValidation"
												onBeforeTopics="buildFormBefore"
												onCompleteTopics="buildFormComplete"
												><s:text name="jsp.default_175"/></sj:a>
                                            </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>

                    </table>
                </div>
            </s:form>
            <br>
        </div>
    </BODY>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashppaybuild.jsp?buildFromBlank=false" URLEncrypt="true"/>

<ffi:removeProperty name="GetCurrencyNumDecimals"/>
