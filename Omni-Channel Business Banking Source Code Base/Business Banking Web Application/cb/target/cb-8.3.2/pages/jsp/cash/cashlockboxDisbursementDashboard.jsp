<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


	<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-lockBox"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_113" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/cashflowSubmenuDropdown.jsp" />
    	</div>
    	<div id="dbCashLockboxSummary" class="dashboardSummaryHolderDiv">
    		<sj:a
				id="cashLockBoxSummaryBtn"
				indicator="indicator"
				cssClass="summaryLabelCls"
				button="true"
				onclick="ns.shortcut.goToMenu('cashMgmt_lockbox');"
				title="%{getText('jsp.transfers.transfer_summary')}"
			><s:text name="jsp.transfers.transfer_summary"/></sj:a>
    	
<%--     	
	        <sj:a id="lockboxReportingButton"
	   		  button="true"
			  cssStyle="margin-right: 5px;"
			  onclick="ns.shortcut.goToFavorites('reporting_cashmgt');"
			  ><s:text name="jsp.default_355"/></sj:a>
			   --%>
		</div>
	</div>
<script>
	$("#cashLockBoxSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>