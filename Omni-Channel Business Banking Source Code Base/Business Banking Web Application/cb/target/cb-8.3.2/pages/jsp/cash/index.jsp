<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:setProperty name="PortalFlag" value="false"/>
<script type="text/javascript" src="<s:url value='/web/js/cash/cash%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/cash/cash_grid%{#session.minVersion}.js'/>"></script>


<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="false"/>
</ffi:cinclude>


		<div id="desktop" align="center">

		<div id="operationresult">
			<div id="resultmessage"><s:text name="jsp.default_498"/></div>
		</div><!-- result -->

		<div id="appdashboard">
			<s:include value="/pages/jsp/cash/inc/cashflow.jsp"/>
 	    </div>


<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>

		<div id="summary">
			<s:include value="/pages/jsp/cash/cashflow_summary.jsp"/>
		</div>

	<script type="text/javascript">
	$(document).ready(function(){
		if(ns.cash.appdashboardflag){
			//Accounts is zero,no dashbpard displayed 
			$('#appdashboard').html('');
		}
	});		
	</script>

<ffi:removeProperty name="AccountsCollectionName"/>
<ffi:removeProperty name="DataType"/>
<ffi:removeProperty name="CashFlowCriteriaDate"/>
<ffi:removeProperty name="CashFlowDataClassification"/>
<ffi:removeProperty name="CashDisplayCurrencyCode"/>
<ffi:removeProperty name="DateVariable"/>
<ffi:removeProperty name="DataClassificationVariable"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>
<ffi:removeProperty name="PortalFlag"/>

