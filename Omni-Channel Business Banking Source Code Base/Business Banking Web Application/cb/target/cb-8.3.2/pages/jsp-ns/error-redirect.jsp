<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 

<script type="text/javascript">
<!--
function setLocation()
{
	location.replace("<ffi:getProperty name="ServletPathNoNs"/><ffi:getProperty name="ERROR_REDIRECT_URL"/>");
}
if (typeof jQuery == 'undefined') { 
	window.onload=setLocation;
}
//-->
</script>

<s:url id="ajax" value="/servlet/cb/%{#attr.ERROR_REDIRECT_URL}" >
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<sj:div href="%{ajax}" indicator="indicator"></sj:div>