<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<div class="ui-widget">
	<div style="padding: 0pt 0.7em;text-align:left" class="ui-state-error ui-corner-all">
		<p>
			<span style="float: left; margin-right: 0.3em;"></span>
			<strong><s:text name="jsp.login_5"/></strong>
			<ffi:setProperty name="PageHeading" value="ERROR" />

			<% String taskErrorCode;
				String tempError;
				String tempBankID;
			%>

			<s:text name="jsp.concurrent_login_error"/>
			
			
		</p>
	</div>
		<br/>
		<center>
			<sj:a id='processErrorBackButtonID' button='true' tabindex='3' onClickTopics="rclBackButtonHandleTopic"><s:text name='jsp.default_57'/></sj:a>
		</center>
</div>
		
<script type="text/javascript">
	$.subscribe("rclBackButtonHandleTopic", function(e,d){
		var appType = "<ffi:getProperty name="appType" />";
		var userLocale = '<ffi:getProperty name="UserLocale" property="Locale"/>';
		if(appType == 'Business') {
			window.location.href = "./login-corp.jsp?request_locale="+userLocale;
		} else {
			window.location.href = "./login-cons.jsp?request_locale="+userLocale;
		}
	});
	$(document).ready(function() {
		if(loginController){
			loginController.changeLoginPanelTitle(js_login_concurrent_session_pane_title)
		}
	});
</script>
