<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
	Displays an error when a user's password cannot be reset because information is missing from the database. See the PasswordRecoveryDDD.doc design documentation file for details.
--%>
<div align="center">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left">
				<s:form action="userName.action"  method="post" name="ValidateForm" id="validateForm">
					<div align="center">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<table style="lin-height:normal">
							<tr>
								<td class="sectionsubhead ui-state-error ui-corner-all">
									<span style="float: left;"></span>
									<s:text name="jsp.login_4"/>
								</td>
							</tr>
							<tr>
								<td class="errortext"><s:text name="jsp.login_108"/></td>
							</tr>
							<tr>
								<td><br/></td>
							</tr>
						</table>
					</div>
					<div align="center" >
							<sj:a id="contNo2FAButtonID" formIds="validateForm" button="true" tabindex="1" targets="signin_menu" ><s:text name="jsp.default_111"/></sj:a>
					</div>
					<br>
					<div class="sectionsubhead" align="center">
					<s:text name="jsp.login_35"/>&nbsp;<ffi:getProperty name="AssistancePhoneNumber" />&nbsp;<s:text name="jsp.login_12"/>
					</div>

				</s:form>
			</td>
		</tr>
	</table>
</div>
