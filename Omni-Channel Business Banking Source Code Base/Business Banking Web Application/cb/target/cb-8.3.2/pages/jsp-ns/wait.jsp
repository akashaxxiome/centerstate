<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_335')}" scope="request" /><ffi:setProperty name="PageHeading" value="${tmpI18nStr}"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

        <center>
           <ffi:setProperty name="showDate" value="false"/>
           <s:set var="tmpI18nStr" value="%{getText('jsp.default_335')}" scope="request" /><ffi:setProperty name="navTitle" value="${tmpI18nStr}"/>
           <ffi:include page="${PathExtNs}blank-nav.jsp" />
           <ffi:removeProperty name="navTitle"/>
           <ffi:removeProperty name="showDate"/>
        </center>

        <%-- ================ MAIN CONTENT START ================ --%>
        <br>
        <center>
            <span class="sectionhead"><s:text name="jsp.login_77"/></span>
        </center>
        <%-- ================= MAIN CONTENT END ================= --%>

        <ffi:include page="${PathExtNs}footer.jsp" />

<script type="text/javascript">
    location.replace("<ffi:urlEncrypt url="${ServletPath}${target}"/>");
</script>
