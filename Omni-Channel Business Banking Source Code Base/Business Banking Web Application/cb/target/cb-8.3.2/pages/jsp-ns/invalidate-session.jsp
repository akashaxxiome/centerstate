<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<%
	String reqLocale = "";
	String theme = "";
	String loginAppType = "";
	String noAuthenticateURL = "/cb/pages/jsp-ns/no-authenticate.jsp";
	String params = "";
	
	if (request.getParameter("request_locale") != null) {
	 	reqLocale = "request_locale=" + (String)request.getParameter("request_locale");
	 	params += "&" + reqLocale;
   	}
	if(session.getAttribute("themeName") != null) {
 		theme = "themeName=" + (String)session.getAttribute("themeName");
 		params += "&" + theme;
	}
	if(session.getAttribute("LoginAppType") != null) {
 		loginAppType = "LoginAppType=" + (String)session.getAttribute("LoginAppType");
 		params+= "&" + loginAppType;
	}
	params = params.replaceFirst("&","?");
 	noAuthenticateURL+=params;
%>
<ffi:setProperty name="NoAuthenticateURL" value="<%=noAuthenticateURL%>" URLEncrypt="true" />
<script type="text/javascript">
	location.replace('<ffi:getProperty name="NoAuthenticateURL" encode="false" />');
</script>
<ffi:removeProperty name="NoAuthenticateURL" />
<% session.invalidate(); %>
</head>
<body>
</body>
</html>