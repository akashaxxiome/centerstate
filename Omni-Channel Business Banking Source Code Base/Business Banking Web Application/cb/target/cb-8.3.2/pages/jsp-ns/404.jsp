<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String noAuthenticateURL = "/cb/pages/jsp-ns/no-authenticate.jsp";
	String params = "";
	if(session.getAttribute("themeName") != null) {
		String theme = "themeName=" + (String)session.getAttribute("themeName");
		params += "&" + theme;
	}
	if(session.getAttribute("LoginAppType") != null) {
		String loginAppType = "LoginAppType=" + (String)session.getAttribute("LoginAppType");
		params+= "&" + loginAppType;
	}
	params = params.replaceFirst("&","?");
	noAuthenticateURL+=params;
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
	<head>
	    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8"/>
	    <ffi:setProperty name="NoAuthenticateURL" value="<%=noAuthenticateURL%>" URLEncrypt="true"/>
	    <META HTTP-EQUIV="Refresh" CONTENT="5;URL='<ffi:getProperty name="NoAuthenticateURL"/>'">
		<ffi:removeProperty name="NoAuthenticateURL"/>
		<title><s:text name="jsp.login_27"/></title>
		
		<style type="text/css">
	
				body {color: #fff; margin: 0;	padding: 0; }
				.masterDiv { width: 100%; height: 100%; overflow: hidden; }
				.headerDiv { background-color: #6699cc; width: 100%; }
				.errorDiv { background-color: #00364E; width: 100%;	height: 100%; position: fixed; }
				.errorLblHolder { margin: 15%; }
				#sapJspNsGoldBar {border:1px solid red;}
		</style>
		<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ubuntu-font-family-0.80/ubuntuFontLib%{#session.minVersion}.css'/>"/>
	</head>
	<body>
	    <div class="masterDiv">
			<div class="headerDiv">
				<img src="<s:url value="/web/grafx/banklogos/Online_Banking_Logo_747075.png"/>"/>
			</div>
			<div id="sapJspNsGoldBar" class="ui-state-hover"></div>
			<div class="errorDiv">
				<div class="errorLblHolder">
					<h1><s:text name="jsp.login_27"/></h1>
					<p><s:text name="jsp.login_88"/></p>
					<p><s:text name="jsp.login_88_1"/></p>
					<p><s:text name="jsp.login_88_2"/></p>
				</div>
			</div>
		</div>
	</body>
</html>

<% session.invalidate(); %>