<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- <s:include value="include-theme.jsp"/> --%>
<ffi:cinclude value1="${BackAction}" value2="" operator="equals" >
	<s:include value="invalid-input-ajax.jsp"></s:include>
</ffi:cinclude>
<ffi:cinclude value1="${BackAction}" value2="" operator="notEquals" >
				<div class="ui-widget">
					<div style="padding: 0pt 0.7em;text-align:left" class="ui-state-error ui-corner-all">
						<p>
							<span style="float: left; margin-right: 0.3em;"></span> 
							<strong><s:text name="jsp.login_5"/></strong> 
							<ffi:setProperty name="PageHeading" value="ERROR" /> 
							<% String taskErrorCode; %> 
						 	<ffi:getProperty name="CurrentTask" property="Error" assignTo="taskErrorCode" /> 
						 	<%
						 	boolean useAdditionalErrorMessage = com.ffusion.util.CommBankIdentifier
						 			.isValidAdditionalServiceCode(taskErrorCode);
						 	%> 
						 	<%--
							<div align="center">
							<ffi:include page="${PathExt}inc/nav_header.jsp" />
							--%> 
							<ffi:cinclude value1="${CurrentTask.Error}" value2="<%= String.valueOf( com.ffusion.csil.CSILException.ERROR_EXCEED_LIMIT ) %>" operator="equals">
								<script type="text/javascript">
									window.location.href = "<ffi:getProperty name='SecurePath'/> approvals/exceedlimitsnoapproval.jsp";
								</script>
							</ffi:cinclude> 
							<%-- ================ MAIN CONTENT START ================ --%> 
							<ffi:setProperty name="${touchedvar}" value="false" /> 
							<%-- ERROR MESSAGE BEGIN --%> 
							<%
						 		if (useAdditionalErrorMessage) {
						 	%> 
						 		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="ErrorA${CurrentTask.Error}_descr" /> 
						 	<%
						 		} else {
						 	%> 
						 		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${CurrentTask.Error}_descr" /> 
						 	<%
						 		}
						 	%> 
						 	<ffi:cinclude value1="${ErrorsResource.Resource}" value2="" operator="equals" >
								<s:text name="jsp.default_114" />
							</ffi:cinclude>
							<ffi:cinclude value1="${ErrorsResource.Resource}" value2="" operator="notEquals" >
							                                <ffi:getProperty name="ErrorsResource" property="Resource"/>
							</ffi:cinclude>
							<%-- SE_WHY will have a BPW specific error, text gotten from BPW --%> 
							<%
						 		if (session.getAttribute("SE_WHY") != null) {
						 	%> 
						 		<ffi:getProperty name="SE_WHY" /> 
						 	<%
							 		session.removeAttribute("SE_WHY");
							 	}
							 %>
						</p>
					</div>
				</div>
				<br/>

				<center>
					<s:url id="ajax" namespace="/pages/jsp-ns" action="%{#session.BackAction}">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url> 
					<sj:a id="invalidInputBackButtonID" href="%{ajax}" targets="signin_menu" button="true" tabindex="3"><s:text name="jsp.default_57"/></sj:a>
				</center>
</ffi:cinclude>