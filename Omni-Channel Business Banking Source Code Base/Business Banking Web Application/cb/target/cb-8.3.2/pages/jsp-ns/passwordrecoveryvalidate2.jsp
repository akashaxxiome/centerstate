<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="java.util.Locale" %>
<%@page import="com.ffusion.util.UserLocaleConsts" %>
<%@page import="com.ffusion.beans.user.UserLocale" %>

<%--
	Requests information necessary to validate ownership of a user's login account.
	See the PasswordRecoveryDDD.doc design documentation file for details.
	Note: form field names must match the values in com.ffusion.passwordrecovery.ParameterNames
--%>
<script type="text/javascript">
	$("#passwordAnswer2").trigger('focus');
	<s:if test="%{#session.isMSIE}">document.getElementById("passwordAnswer2").focus();</s:if>
	$(document).ready(function()
	{
		if (loginController) {
			loginController.changeLoginPanelTitle(js_password_recovery)
		}

		// Triggers "click" event when the CONTINUE button has got focus and "Enter" key is hit on the keyboard
		$("#contPasswordRecoveryValidate").keypress(function(event)
		{
			if (event.which == 13) {
				event.preventDefault();
				$('#contPasswordRecoveryValidate').trigger('click');
			}
		});
	});
</script>
<style type="text/css">
/*background-image: url("/cb/web/multilang/grafx/loginPageBg.png");
    background-repeat: repeat-x;
    background-size: 100% 100%;*/
.corpLoginPageStyle, .consLoginPageStyle {
    background: #e1e1e1;
}
</style>
<%--=================== Securing the Second Password Answert page - Part 1 Start  =====================--%>
<%
	if (!(session.getAttribute("UserSecurityKey") instanceof com.ffusion.tasks.util.ImmutableString)) {
%>
<script>
	window.location = "/cb/pages/jsp-ns/invalidate-session.jsp";
</script>
<%
	}
%>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="notEquals">
	<script>
		window.location = "/cb/pages/jsp-ns/invalidate-session.jsp";
	</script>
</ffi:cinclude>

<%-------------------- Get the appropriate resources files containing the PasswordQuestions -----------------%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResource" scope="session"/>
<ffi:setProperty name="UserResource" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResource"/>

<ffi:object name="com.ffusion.tasks.util.ResourceList" id="UserResourceList" scope="session"/>
<ffi:setProperty name="UserResourceList" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResourceList"/>

<%
	if (session.getAttribute("UserSecurityKey") instanceof com.ffusion.tasks.util.ImmutableString) {
		//This code is needed to switch locale to get english value for chinese text of password questions
		Locale englishLocale = new Locale("en", "US");
		com.ffusion.tasks.util.Resource userResourceTemp = (com.ffusion.tasks.util.Resource)session.getAttribute("UserResource");
		UserLocale userLocale = (UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE);
		Locale originalLocale = userLocale.getLocale();

%>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="equals">
	<%--=================== Securing the Second Password Answer page - Part 1 End =====================--%>

	<%-- Clean up from the previous password recovery step --%>
	<ffi:removeProperty name="<%= com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER %>"/>

	<s:form id="credentialCheck2" namespace="/pages/jsp-ns" action="credentialCheck2" method="post"
			name="ValidateForm" theme="simple" onsubmit="$('#contPasswordRecoveryValidate').trigger('click');return false">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

		<div align="center">
			<s:include value="inc/action-error.jsp"/>
			<p class="instructions">
				<s:text name="jsp.login_73"/>
			</p>
			<p>
				<label for="<%=com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER2%>">
					<span class="sectionhead">
						<ffi:setProperty name="UserResourceList" property="ResourceID" value="PWD_QUESTION_LIST"/>
						<ffi:getProperty name="UserResourceList" property="Resource"/>

						<ffi:list collection="UserResourceList" items="item">
							<ffi:setProperty name="UserResource" property="ResourceID" value="PWD_QUESTION.${item}"/>
							<%
								userResourceTemp.setLocale(englishLocale); //switch locale to get english value
							%>
							<ffi:cinclude value1="${UserResource.Resource}" value2="${User.passwordClue2}" operator="equals">
								<%
									userResourceTemp.setLocale(originalLocale); //switch locale to get user locale value
								%>
								<ffi:getProperty name="UserResource" property="Resource"/>
							</ffi:cinclude>
						</ffi:list>
					</span>
				</label>
				<input type="password" class="ui-widget-content ui-corner-all" tabindex="1" name="<%=com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER2%>" id="passwordAnswer2" size="35" maxlength="50"/>

			<div id="prErrorDiv" style="text-align:center;clear:left;">
				<span id="<%=com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER2%>Error"></span>
			</div>
			</p>
			<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<div align="center">
				<sj:a id="pwdRVCancelButton1ID" href="%{ajax}" targets="signin_menu" button="true" tabindex="3" cssClass="buttonSizeReset"><s:text name="jsp.default_82"/></sj:a>
				<sj:a id="contPasswordRecoveryValidate"
					  formIds="credentialCheck2" button="true" tabindex="2"
					  validate="true"
					  validateFunction="customValidation"
					  targets="signin_menu" cssClass="buttonSizeReset"><s:text name="jsp.default_111"/></sj:a>
			</div>
			<br>
			<br>

			<p class="instructions">
				<s:text name="jsp.login_35"/>&nbsp;<ffi:getProperty name="AssistancePhoneNumber"/>&nbsp;<s:text name="jsp.login_12"/>
			</p>
			<br/>
		</div>
	</s:form>

	<ffi:setProperty name="BackURL" value="/cb/servlet/cb/${PathExtNs}passwordrecoveryvalidate2.jsp?verified=${UserSecurityKey.ImmutableString}" URLEncrypt="true"/>

	<%--=================== Securing the Second Password Answer page - Part 2 Start ====================--%>
</ffi:cinclude>
<%
	}
%>
<ffi:removeProperty name="verified"/>
<%--=================== Securing the Second Password Answer page - Part 2 End =====================--%>
