<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:setProperty name='PageHeading' value='Log In'/>

<ffi:task errorURL="${PathExtNs}invalid-input.jsp" />
<ffi:task serviceErrorURL="${PathExtNs}process-error-ajax.jsp" />
<ffi:task errorRedirectURL="${PathExtNs}error-redirect.jsp" />
<ffi:task taskRedirectURL="${PathExtNs}task-redirect-ajax.jsp" />

<ffi:object id="InvalidateSession" name="com.ffusion.tasks.util.InvalidateSession" scope="session"/>

<ffi:object id="Compare" name="com.ffusion.beans.util.StringUtil" scope="session"/>
<ffi:object id="StringUtil" name="com.ffusion.beans.util.StringUtil" scope="session"/>
<ffi:object id="FloatMath" name="com.ffusion.beans.util.FloatMath" scope="session"/>
<ffi:object id="Math" name="com.ffusion.beans.util.IntegerMath" scope="session"/>



<ffi:object id="ServiceErrorsResource" name="com.ffusion.tasks.util.ServiceErrorsResource" scope="session"/>
   
<ffi:process name="ServiceErrorsResource"/>

<ffi:object id="ErrorsResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="ErrorsResource" property="ResourceFilename" value="com.ffusion.tasksresources.errors" />
<ffi:process name="ErrorsResource"/>


<ffi:object name="com.ffusion.tasks.util.Resource" id="MessagesResources" scope="session"/>
    <ffi:setProperty name="MessagesResources" property="ResourceFilename" value="com.ffusion.beansresources.messages.resources" />
<ffi:process name="MessagesResources"/>

<% String localeName; %>

<ffi:getProperty name="UserLocale" property="Locale" assignTo="localeName"/>


	<%-- To get to non secure 404 page, PathExt must point to jsp-ns folder.
		This is changed back to the jsp folder in login-init.jsp
	--%>
	<ffi:setProperty name="PathExt" value="jsp-ns/" />
	<ffi:setProperty name="PathExtNs" value="jsp-ns/" />
	<ffi:setProperty name="ComPathExt" value="jsp/" />
	<ffi:setProperty name="ComImgExt" value="" />
	<ffi:setProperty name="ImgExt" value="" />
	<ffi:setProperty name="HtmlPath" value="/cb/web/html/"/>
    <ffi:setProperty name="HelpPath" value="/cb/web/help/"/>



<ffi:setProperty name="InvalidateSession" property="SuccessURL" value="${HtmlPath}logout.html" />

<ffi:setProperty name="BankId" value="1000"/>

<ffi:setProperty name="ServletPath" value="/pages/"/>
<ffi:setProperty name="SecurePath" value="/pages/"/>

<ffi:setProperty name="product" value="FusionBank"/>
<ffi:setProperty name="AssistancePhoneNumber" value="1-800-888-8888"/>

<%-- ffi:setProperty name="BackURL" value="${ServletPath}InvalidateSession"/ --%>
<s:if test="%{#session.LoginAppType.equals('CB')}">
<ffi:setProperty name="BackURL" value="<s:url value='/pages'/>/${PathExtNs}login-corp.jsp?locale=${UserLocale.Locale}" URLEncrypt="true"/>
</s:if>
<s:else>
<ffi:setProperty name="BackURL" value="<s:url value='/pages'/>/${PathExtNs}login-cons.jsp?locale=${UserLocale.Locale}" URLEncrypt="true"/>
</s:else>

<ffi:object id="AuthenticationResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="AuthenticationResource" property="ResourceFilename" value="com.ffusion.beansresources.authentication.resources"/>
<ffi:process name="AuthenticationResource"/>

<ffi:setProperty name="AuthResourceID" value="<%= com.ffusion.beans.authentication.AuthConsts.CREDENTIAL_FIELD_NAME_PREFIX %>"/>

<ffi:object id="CredentialIndex" name="com.ffusion.beans.util.IntegerMath" scope="session"/>
    <ffi:setProperty name="CredentialIndex" property="Value1" value="0"/>
    <ffi:setProperty name="CredentialIndex" property="Value2" value="1"/>

<%-- Get the security password generated here, used to secure the password recovery sequence.  --%>
<ffi:object id="GetSecurityKey" name="com.ffusion.tasks.util.GetSecurityKey" scope="session"/>
	<ffi:setProperty name="GetSecurityKey" property="KeyLength" value="20"/>
<ffi:process name="GetSecurityKey"/>
<ffi:removeProperty name="GetSecurityKey"/>
