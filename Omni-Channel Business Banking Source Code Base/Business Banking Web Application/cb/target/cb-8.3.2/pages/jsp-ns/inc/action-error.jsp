<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="hasActionErrors()">
	<div id="formerrors">
	<style>
		ul{list-style-type:none;}
	</style>
	<div class="ui-widget" align="center">
		<div class="ui-state-error ui-corner-all">
			<s:actionerror/>
		</div>
	</div>
	</div>
</s:if>
