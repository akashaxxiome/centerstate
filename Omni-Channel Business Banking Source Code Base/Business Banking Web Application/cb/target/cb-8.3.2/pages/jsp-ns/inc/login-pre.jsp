<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var="tmpI18nStr" value="%{getText('jsp.login_44')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:object id="InvalidateSession" name="com.ffusion.tasks.util.InvalidateSession" scope="session"/>
<ffi:object id="Compare" name="com.ffusion.beans.util.StringUtil" scope="session"/>
<ffi:object id="StringUtil" name="com.ffusion.beans.util.StringUtil" scope="session"/>
<ffi:object id="FloatMath" name="com.ffusion.beans.util.FloatMath" scope="session"/>
<ffi:object id="Math" name="com.ffusion.beans.util.IntegerMath" scope="session"/>

<ffi:object id="ServiceErrorsResource" name="com.ffusion.tasks.util.ServiceErrorsResource" scope="session"/>
<ffi:process name="ServiceErrorsResource"/>

<ffi:object id="ErrorsResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="ErrorsResource" property="ResourceFilename" value="com.ffusion.tasksresources.errors" />
<ffi:process name="ErrorsResource"/>

<% String localeName; 
String ua = request.getHeader( "User-Agent" );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
session.setAttribute("isMSIE",Boolean.valueOf(isMSIE)); %>

<ffi:getProperty name="UserLocale" property="Locale" assignTo="localeName"/>

	<%-- To get to non secure 404 page, PathExt must point to jsp-ns folder.
		This is changed back to the jsp folder in login-init.jsp
	--%>
  	<ffi:setProperty name="PathExt" value="jsp-ns/" />
	<ffi:setProperty name="PathExtNs" value="jsp-ns/" />
	<ffi:setProperty name="ComPathExt" value="jsp/" />
	<ffi:setProperty name="ComImgExt" value="" />
	<ffi:setProperty name="ImgExt" value="" />
	<ffi:setProperty name="HtmlPath" value="/cb/pages/html/" />
    <ffi:setProperty name="HelpPath" value="/cb/web/help/" />


<ffi:setProperty name="ServletPath" value="/cb/servlet/cb/"/>
<ffi:setProperty name="SecurePath"  value="/cb/servlet/cb/"/>
<ffi:setProperty name="ServletPathNoNs" value="/cb/servlet/cb/"/>

<ffi:setProperty name="InvalidateSession" property="SuccessURL" value="${HtmlPath}logout.html" />

<ffi:setProperty name="BankId" value="1000"/>

<s:set var="tmpI18nStr" value="%{getText('jsp.login_33')}" scope="request" /><ffi:setProperty name="product" value="${tmpI18nStr}"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.login_3')}" scope="request" /><ffi:setProperty name="AssistancePhoneNumber" value="${tmpI18nStr}"/>

<ffi:object name="com.ffusion.tasks.util.Resource" id="MessagesResources" scope="session"/>
    <ffi:setProperty name="MessagesResources" property="ResourceFilename" value="com.ffusion.beansresources.messages.resources" />
<ffi:process name="MessagesResources"/>

<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
<ffi:process name="GetLanguageList" />