<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div class="headerContainer">
	<span>
		<span class="topBarBankLogo">
	</span>

<div class="dateLocale">
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
<ffi:process name="GetCurrentDate"/>			
<%-- Format and display the current date/time --%>
<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="EEEEEEEEE, ${UserLocale.DateTimeFormat}"/>
<ffi:getProperty name="GetCurrentDate" property="Date" />

<ffi:cinclude value1="${showDate}" value2="true" operator="equals">
	<span id="date"></span>
</ffi:cinclude>
<ffi:cinclude value1="${showDate}" value2="true" operator="notEquals">
	&nbsp;
</ffi:cinclude>
<%-- 

<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="notEquals">
	<select id="languagesDropdown" tabindex="0"  name="localeName">
		<ffi:setProperty name="GetLanguageList" property="LanguagesList.Locale" value="${UserLocale.Locale}" />
		<ffi:list collection="GetLanguageList.LanguagesList" items="languageDf">
			<option
			 class="<ffi:getProperty name="languageDf" property="Language" />"
			 value="<ffi:getProperty name="languageDf" property="Language" />"
			 <ffi:cinclude value1="${UserLocale.Locale}" value2="${languageDf.Language}" operator="equals"
			  >selected</ffi:cinclude>
			  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="languageDf" property="DisplayName" />
			</option>
		</ffi:list>
	</select> 
</ffi:cinclude> --%>
</div>
<!-- <div id="sapJspNsGoldBar" class="ui-state-hover"></div> -->
</div>