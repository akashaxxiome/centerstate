<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<html>
    <head>
    	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <title><s:text name="jsp.login_29"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<s:include value="include-theme.jsp" />	
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/FF.min.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login.min.css'/>"/>
        <link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/showLoading.min.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/ui.selectmenu.min.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/home/jquery/layout.min.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login_custom.css'/>" />
		<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ubuntu-font-family-0.80/ubuntuFontLib%{#session.minVersion}.css'/>"/>
		<script type="text/javascript" src="<s:url value='/web/js/namespace.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/jquery.i18n.properties.min.js'/> "></script>
		
		<!-- Include Migrate Plugin for jQuery Upgrade -->
		<script type="text/javascript" src="<s:url value='/web/jquery/home/jquery-migrate-1.2.1.min.js'/>"></script>
		
		<script>
		
		$(document).ready(function() {
			jQuery.i18n.properties({
				name: 'LoginJavaScript',
				path:'<s:url value="/web/js/i18n/" />',
				mode:'both',
				language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
				cache:true,
				callback: function(){
				}
			});
		});
		</script>
		
		<style>
			/*to remove the gray bar from the top*/
			.ui-layout-resizer {
				position: static !important;
			}
			
			.ui-layout-north, .ui-layout-center, .ui-layout-south {
				border: none !important;
				padding: 0px !important;
			}
		</style>
    </head>
    <body class="corpLoginPageStyle">
		<div class="ui-layout-north headePane"><s:include value="simple-header.jsp"/></div>
		<div class="ui-layout-center centerLayoutPane" tabindex="1"><s:include value="logout-desktop.jsp"/></div>
		<div class="ui-layout-south footerPane">
			<s:include value="footer.jsp" />
			<s:url id="ajax" value="inc/security_info.jsp" />
				<s:set var="tmpI18nStr" value="%{getText('jsp.default_102')}" />
				<sj:dialog id="security" position="['center',130]" cssStyle="display:none;"
						   hideEffect="fade" title="%{getText('jsp.login_83')}" autoOpen="false"
						   showEffect="fade" width="700" modal="true" resizable="false"
						   buttons="{'%{tmpI18nStr}': function() { $(this).dialog('close'); } }"
				>
					<s:include value="%{ajax}" />
			</sj:dialog>
		</div>      
	</div>

    </body>
	<script type="text/javascript" src="<s:url value='/web/js/jsp-ns/jquery.showLoading.min.js'/>" ></script>
	<script type="text/javascript" src="<s:url value='/web/js/jquery.ui.pane.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/jsp-ns/logout.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/jquery/home/layout.latest.min.js'/>"></script>
	<%-- <script>
		ns.logout.bankTitle = '<s:text name="login.common.title"/>';
		js_tooltip_security='<s:text name="jsp.login_18"/>';
		
	</script> --%>
	<script>
		$(document).ready(function(){
			$(".instructions").css('line-height', $("#loginPageHolderDiv").outerHeight()+"px");
		})
	</script>
</html>
<%
	// last thing on the page must be session.invalidate since a new session was created by requesting this page
	session.invalidate();
%>
