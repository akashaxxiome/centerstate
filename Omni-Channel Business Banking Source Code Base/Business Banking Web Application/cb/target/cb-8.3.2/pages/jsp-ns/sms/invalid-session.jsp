<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<f:view locale="#{JSFUtil.userLocale}">
<f:loadBundle var="appBundle" basename="templates_#{JSFUtil.applicationName}"/>
<html>
<head>
	<meta name="viewport" content="width=250, user-scalable=no" />
	<title><h:outputText value="#{appBundle.title_error}"/></title>
	 <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/wap.css" />
	 <script type="text/javascript" src="<%= request.getContextPath() %>/jsp/js/exitFrames.js"></script>
</head>
<body>
<div align="center">
	<h:outputText value="#{appBundle.msg_noSession}" styleClass="msg"/>
	<jsp:include page="/ns/inc/footer.jsp"/>
</div>	
</body>
</html>
</f:view>