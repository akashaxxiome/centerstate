<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<ffi:setProperty name="BACKURL" value="ns/login.jsp" />
<ffi:removeProperty name="UserAction"/>
<%
	
	String locale = request.getParameter("LOC");
	String actionId=request.getParameter("ID");
	
	if (actionId != null && actionId.length() > 0) {
		session.setAttribute("ACTIONID", actionId);
	}
	if (locale != null && locale.length() > 0) {
			com.sybase.mbanking.common.faces.JSFUtil.setLocale(locale);
	}

%>
<f:view>
<f:loadBundle var="appBundle" basename="templates_#{JSFUtil.applicationName}"/>
<html>
<head>
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name="viewport" content="width=250, user-scalable=no" />
	<title><h:outputText value="#{appBundle.title_login}"/></title>
	 <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/wap.css" />
	<script type="text/javascript" src="<%= request.getContextPath() %>/jsp/js/exitFrames.js"></script>
</head>
<body onload='document.forms["frmLogin"]["frmLogin:userName"].focus()'>
<div align="center">
	<h:form id="frmLogin">
		<t:inputHidden id="CSRF_TOKEN" value="#{CSRF_TOKEN}"/>	<%-- required for cross-site request forgery handling --%>
		<div><h:graphicImage id="logo" value="/grafx/mBankingLogo.gif" alt="#{appBundle.alt_FFI}"/></div>
		<div><h:outputText value="#{appBundle.inst_enterLoginID}" styleClass="inst"/></div>
		<div><t:inputText id="userName" value="#{UserAction.user.userId}" autocomplete="off" styleClass="input_txt" required="true" size="12" maxlength="20"/></div>
		<div><h:message for="userName" styleClass="error"/></div>
		<div><h:outputText value="#{appBundle.inst_enterBusinessID}" styleClass="inst" rendered="#{UserAction.businessIdRequired == true}"/></div>
		<div><t:inputText id="businessCIF" value="#{UserAction.user.groupId}" autocomplete="off" styleClass="input_txt" required="true" size="12" maxlength="32" rendered="#{UserAction.businessIdRequired == true}"/></div>
		<div><h:message for="businessCIF" styleClass="error"/></div>
		<div class="vspc"><h:commandButton id="btnLogin" value="#{appBundle.btn_enter}" action="#{UserAction.getMultifactorSecurityInfo}"  styleClass="btn"/></div>
	</h:form>
	<f:verbatim><br></f:verbatim>
   	<jsp:include page="/ns/inc/footer.jsp"/>
</div>   	
</body>
</html>
</f:view>