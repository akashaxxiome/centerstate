<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <%-- 1st column --%>
        <td align="right" valign="middle" class="columndata">
            <ffi:cinclude value1="${showDate}" value2="true" operator="equals">
                <ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
                <ffi:process name="GetCurrentDate"/>
                <%-- Format and display the current date/time --%>
                <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="EEEEEEEEE, ${UserLocale.DateTimeFormat}"/>
                <span id="date" style="font-weight: bold; font-size: 1.05em;"><ffi:getProperty name="GetCurrentDate" property="Date"/></span>
            </ffi:cinclude>
            <ffi:cinclude value1="${showDate}" value2="true" operator="notEquals">
                &nbsp;
            </ffi:cinclude>
        </td>
    </tr>
</table>

