<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	$(document).ready(function() {
		/* $("#signin_menu").pane('title',js_login_error); */
		$("#processErrorBackButtonID").trigger('focus') ;
});
</script>

<s:include value="inc/action-error.jsp"/>
<s:url var="backUrl" namespace="/pages/jsp-ns" action="userName">
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>
<div style="margin:.7em 0 .3em 0; text-align:center;">
	<sj:a id="processErrorBackButtonID" href="%{backUrl}" targets="signin_menu" button="true"><s:text name="jsp.default_57"/></sj:a>
</div>
