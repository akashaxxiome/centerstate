<%@ page import="java.util.Locale" %>
<%@ page import="com.ffusion.util.UserLocaleConsts" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%-- ------------------ Get the appropriate resources files containing the PasswordQuestions --------------- --%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResource" scope="session"/>
<ffi:setProperty name="UserResource" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResource"/>

<ffi:object name="com.ffusion.tasks.util.ResourceList" id="UserResourceList" scope="session"/>
<ffi:setProperty name="UserResourceList" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResourceList"/>
<%
                //This code is needed to switch locale to get english value for chinese text of password questions
                Locale englishLocale = new Locale("en", "US");
                com.ffusion.tasks.util.Resource userResourceTemp = (com.ffusion.tasks.util.Resource)session.getAttribute("UserResource");
                UserLocale userLocale = (UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE);
                Locale originalLocale = userLocale.getLocale();
%>
<%-- ----------------------------------------------------------------------------------------------------- --%>

<%-- Assuming that authentication has occured upto this point, there should be a User bean in session for us to use --%>
<ffi:object id="FFIModifyUser" name="com.ffusion.tasks.user.EditUser" scope="session"/>
	<ffi:setProperty name="FFIModifyUser" property="Init" value="true"/>
	<ffi:setProperty name="FFIModifyUser" property="Process" value="false"/>
	<ffi:process name="FFIModifyUser"/>
<%-- ----------------------------------------------------------------------------------------------------- --%>

<ffi:setProperty name="BackAction" value="changeAnswerForm"/>
<div align="center" style="line-height:normal !important">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>

				<s:form action="updatePasswordQA" id="pwdqaChangeForm" name="pwdqaChangeForm" namespace="/pages/jsp-ns" method="post">
					<div align="center">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<table border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td colspan="3"><span class="instructions"><s:text name="jsp.login_39"/></span></td>
							</tr>
							<tr align="center">
								<td nowrap class="sectionhead" align="right" valign="baseline">
									<s:text name="jsp.login_63"/><span class="required">*</span></td>
								<td colspan="2" class="sectionhead" valign="baseline" align="left" nowrap>
									<select style="width:300" name="PasswordClue" id="PasswordClue" tabIndex="1">
										<option value=""><s:text name="jsp.login_84"/></option>
										<ffi:setProperty name="UserResourceList" property="ResourceID" value="PWD_QUESTION_LIST"/>
										<ffi:getProperty name="UserResourceList" property="Resource"/>
										<ffi:list collection="UserResourceList" items="item">
											<ffi:setProperty name="UserResource" property="ResourceID" value="PWD_QUESTION.${item}"/>
											<%        userResourceTemp.setLocale(englishLocale); //switch locale to get english value
																									%>
											<ffi:cinclude value1="${UserResource.Resource}" value2="${FFIModifyUser.PasswordClue}" operator="equals">
												<option value="<ffi:getProperty name="UserResource" property="Resource"/>" selected ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${UserResource.Resource}" value2="${FFIModifyUser.PasswordClue}" operator="notEquals">
												<option value="<ffi:getProperty name="UserResource" property="Resource"/>" ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
											</ffi:cinclude>
										</ffi:list>
									</select>

									<div id="PasswordClueError"></div>
								</td>
							</tr>
							<tr>
								<td nowrap class="sectionhead" align="right" valign="baseline">
									<s:text name="jsp.login_60"/><span class="required">*</span></td>
								<td colspan="2" class="sectionhead" valign="baseline" align="left">

									<input class="ui-widget-content ui-corner-all" tabIndex="2" type="password" name="NewPasswordReminder" size="36" maxlength="50" border="0">
									<br/>

									<div id="NewPasswordReminderError"></div>
								</td>
							</tr>
							<tr>
								<td nowrap class="sectionhead" align="right" valign="baseline">
									<s:text name="jsp.login_20"/><span class="required">*</span></td>
								<td colspan="2" class="sectionhead" valign="baseline" align="left">
									<input class="ui-widget-content ui-corner-all" tabIndex="3" type="password" name="ConfirmPasswordReminder" size="36" maxlength="50" border="0">

									<div id="ConfirmPasswordReminderError"></div>
								</td>
							</tr>
							<tr>
								<td nowrap class="sectionhead" align="right" valign="baseline" style="padding-top: 25px">
									<s:text name="jsp.login_64"/><span class="required">*</span></td>
								<td colspan="2" class="sectionhead" valign="baseline" align="left" nowrap>
									<select style="width:300" name="PasswordClue2" id="PasswordClue2" tabIndex="4">
										<option value=""><s:text name="jsp.login_84"/></option>
										<ffi:setProperty name="UserResourceList" property="ResourceID" value="PWD_QUESTION_LIST"/>
										<ffi:getProperty name="UserResourceList" property="Resource"/>
										<ffi:list collection="UserResourceList" items="item">
											<ffi:setProperty name="UserResource" property="ResourceID" value="PWD_QUESTION.${item}"/>
											<%        userResourceTemp.setLocale(englishLocale); //switch locale to get english value
																									%>
											<ffi:cinclude value1="${UserResource.Resource}" value2="${FFIModifyUser.PasswordClue2}" operator="equals">
												<option value="<ffi:getProperty name="UserResource" property="Resource"/>" selected ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${UserResource.Resource}" value2="${FFIModifyUser.PasswordClue2}" operator="notEquals">
												<option value="<ffi:getProperty name="UserResource" property="Resource"/>" ><%userResourceTemp.setLocale(originalLocale); %><ffi:getProperty name="UserResource" property="Resource"/></option>
											</ffi:cinclude>
										</ffi:list>
									</select>

									<div id="PasswordClue2Error"></div>
								</td>
							</tr>
							<tr>
								<td nowrap class="sectionhead" align="right" valign="baseline">
									<s:text name="jsp.login_61"/><span class="required">*</span></td>
								<td colspan="2" class="sectionhead" valign="baseline" align="left">
									<input class="ui-widget-content ui-corner-all" tabIndex="5" type="password" name="NewPasswordReminder2" size="36" maxlength="50" border="0">

									<div id="NewPasswordReminder2Error"></div>
								</td>
							</tr>
							<tr>
								<td nowrap class="sectionhead" align="right" valign="baseline">
									<s:text name="jsp.login_21"/><span class="required">*</span></td>
								<td colspan="2" class="sectionhead" valign="baseline" align="left">
									<input class="ui-widget-content ui-corner-all" tabIndex="6" type="password" name="ConfirmPasswordReminder2" size="36" maxlength="50" border="0">

									<div id="ConfirmPasswordReminder2Error"></div>
								</td>
							</tr>
							<tr>
								<td colspan="3" class="columndata" align="center" valign="top"><span class="required">*&nbsp;<s:text name="jsp.default_240"/></span>
								</td>
							</tr>
							<tr>
								<td class="sectionhead" align="center" colspan="3">
									<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
									</s:url>
									<sj:a id="mustanswerPwdQesCancelButtonID" href="%{ajax}" targets="signin_menu" button="true"><s:text name="jsp.default_82"/></sj:a>
									<sj:a id="mustanswerPwdQesSaveButtonID"
										  formIds="pwdqaChangeForm"
										  targets="signin_menu"
										  validate="true"
										  validateFunction="customValidation"
										  onClickTopics="onClickSubmitButton"
										  tabIndex="7"
												button="true"><s:text name="jsp.default_366"/></sj:a>
		    							</td>
					    			</tr>
								</table>
								<br>
							</div>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
	
<ffi:removeProperty name="UserResource"/>
<ffi:removeProperty name="UserResourceList"/>
<script type="text/javascript">
	$(document).ready(function()
	{

		$("#PasswordClue").selectmenu({width:300});
		$("#PasswordClue2").selectmenu({width:300});
		$('input[type="password"]').width(287);
		if (loginController) {
			loginController.changeLoginPanelTitle(js_reset_SQA);
		}

		//Triggers "click" event when the SAVE button has got focus
		//and "Enter" key is hit on the keyboard
		$("#mustanswerPwdQesSaveButtonID").keypress(function(event)
		{
			if (event.which == 13) {
				event.preventDefault();
				$('#mustanswerPwdQesSaveButtonID').trigger('click');
			}
		});

	});
</script>
