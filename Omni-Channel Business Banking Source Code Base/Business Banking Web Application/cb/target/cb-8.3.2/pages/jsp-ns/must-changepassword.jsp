<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8"  import="com.ffusion.beans.SecureUser"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<style>
#dataIdSpan span[class~="sapUiIconCls"]{clear: both; display: inline;}
#dataIdSpan span{float:left}
</style>
<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/passwordmeter.css'/>"/>

<script type="text/javascript">
	//$('#PRNewPassword').passwordStrength({targetDiv: '#password-strength-indicator', classes : Array('weak', 'medium', 'strong')});
	$(document).ready(function()
	{
		/* $("#signin_menu").pane({
			title: js_change_password
		}); */
		
		$("#confirmCurrentPassword").trigger('focus');
	});
</script>
<style>
input[type="text"],input[type="password"] {
 width:161px;
 /* opacity: 0.3; */
 background-color: transparent;
 }
</style>

<s:include value="/pages/jsp/user/password-indicator-js.jsp" />
<s:include value="/pages/jsp/user/corpadmin_js.jsp" />

<ffi:setProperty name="BackAction" value="changePasswordForm"/>
<s:if test="%{#session.SecureUser.get('AUTHENTICATE') != null && 
	( #session.SecureUser.get('AUTHENTICATE') == @com.ffusion.csil.CSILException@WARNING_PASSWORD_ABOUT_TO_EXPIRE ||
	  #session.SecureUser.get('AUTHENTICATE') == @com.ffusion.csil.CSILException@ERROR_PASSWORD_CHANGE_REQUIRED ||
	  #session.SecureUser.get('AUTHENTICATE') == @com.ffusion.csil.CSILException@ERROR_PASSWORD_CHANGE_REQUIRED_NEW_REQ)}">

<div class="passwordChangeContent">
<table border="0" cellspacing="0" cellpadding="3" style="line-height: normal;" align="center">
	<tr align="center">
		<%-- check the SecureUser.AUTHENTICATE flags --%>
		<s:if test="%{#session.SecureUser.agent == null && #session.SecureUser.get('AUTHENTICATE') == @com.ffusion.csil.CSILException@WARNING_PASSWORD_ABOUT_TO_EXPIRE}">
			<td class="instructions"><s:text name="jsp.login_114"/></td>
		</s:if>
		<s:else>
			<%-- If the SecureUser is forced to change their password because of new password strength rule, display a different message. --%>
			<s:if test="%{#session.SecureUser.get('AUTHENTICATE') == @com.ffusion.csil.CSILException@ERROR_PASSWORD_CHANGE_REQUIRED_NEW_REQ}">
				<td class="instructions"><s:text name="jsp.login_109"/></td>
			</s:if>
			<s:else>
				<td class="instructions"><s:text name="jsp.login_104"/></td>
			</s:else>
		</s:else>
	</tr>
	<tr><td><s:include value="inc/action-error.jsp"/></td></tr>
	<tr>
		<td>
			<s:form namespace="/pages/jsp-ns" action="changePassword" method="post" name="pswdChangeForm" id="pswdChangeFormID">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<div align="center">
					<table border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td nowrap align="right" valign="baseline">
								<span class="sectionhead"><s:text name="jsp.login_24"/>:</span><span class="required">*</span>
							</td>
							<td valign="baseline" class="sectionsubhead">
								<input class="ui-widget-content ui-corner-all" type="password" name="changePassword.ConfirmCurrentPassword" maxlength="15" id="confirmCurrentPassword">
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<span id="confirmCurrentPasswordError"></span>
							</td>
						</tr>
	
						<tr>
							<td nowrap align="right" valign="baseline">
								<span class="sectionhead"><s:text name="jsp.login_54"/>:</span><span class="required">*</span>
							</td>
							<td valign="baseline" class="sectionsubhead">
								<div style="width:166px; height:30px;">
								<input class="ui-widget-content ui-corner-all inputPassword"  type="password" id="PRNewPassword" name="changePassword.NewPassword" maxlength="15">
								</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap align="right" valign="baseline">
								<span class="sectionhead"><s:text name="jsp.login_19"/>:</span><span class="required">*</span>
							</td>
							<td nowrap valign="baseline" class="sectionsubhead">
								<input class="ui-widget-content ui-corner-all" type="password" name="changePassword.ConfirmNewPassword" maxlength="15" id="PRConfirmNewPass">
							</td>

						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="2"><span id="newPasswordError"></span></td>
						</tr>
						<%-- <tr>
							<td nowrap align="right" valign="middle">
								<span class="sectionhead"><s:text name="jsp.login_66"/></span>
							</td>
							<td valign="top">
								<div id="password-strength-indicator">
									<div id="iWeak" class="passWeek"><s:text name="jsp.login_98"/></div>
									<div id="iMedium" class="passMedium"><s:text name="jsp.login_50"/></div>
									<div id="iStrong" class="passStrong"><s:text name="jsp.login_85"/></div>
									<ul class="weak">
										<li id="iWeak"><s:text name="jsp.login_98"/></li>
										<li id="iMedium"><s:text name="jsp.login_50"/></li>
										<li id="iStrong"><s:text name="jsp.login_85"/></li>
									</ul>
								</div>
							</td>
						</tr> --%>
						<tr>
							<td>&nbsp;</td>
							<td class="instructions" style="padding-left:2px !important;">
								<ffi:setProperty name="useBCsettings" value="false"/>
								<ffi:setProperty name="changeOwnPassword" value="true"/>
								<s:include value="/pages/jsp-ns/common/password-strength-text.jsp"/>
								<ffi:removeProperty name="changeOwnPassword"/>
								<ffi:removeProperty name="useBCsettings"/>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2">
								<span class="required">*&nbsp;<s:text name="jsp.default_240"/></span></td>
						</tr>
						<tr>
							<td colspan="2" class="columndata" align="center" valign="top"><br>

								<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
								<sj:a id="mustChangePwdCancelButtonID" href="%{ajax}" targets="signin_menu" button="true" tabindex="2"><s:text name="jsp.default_82"/></sj:a>
								<sj:a id="mustChangePwdSaveButtonID" targets="signin_menu" formIds="pswdChangeFormID"
									  button="true" onClickTopics="removeValidationErrors"
									  validate="true" validateFunction="customValidation"><s:text name="jsp.default_366"/></sj:a>
								<s:if test="%{#session.SecureUser.agent == null && #session.SecureUser.get('AUTHENTICATE').intValue() == @com.ffusion.csil.CSILException@WARNING_PASSWORD_ABOUT_TO_EXPIRE}">
									<s:url id="ajax1" namespace="/pages/jsp-ns" action="loginUser.action">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url>
									<sj:a id="mustChangePwdSkipButtonID" href="%{ajax1}" targets="signin_menu" button="true"><s:text name="jsp.default_Skip"/></sj:a>
								</s:if>
							</td>

						</tr>
					</table>
					<br>
				</div>
			</s:form>
		</td>
	</tr>
</table>

<div id="globalmessage" class="loginGlobalMessage">
	<div class="loginMessagePanel">

		<s:text name="jsp.login_51"/>
		<ffi:setProperty name='msgType' value='<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>'/>
		<ffi:setProperty name='BackGround' value="greenClayBackground"/>
		<s:action namespace="/pages/common" name="GlobalMessageAction_getLoginGlobalMessages" executeResult="true">			
				<s:param name="msgType" value="@com.ffusion.beans.messages.GlobalMessageConsts@MSG_TYPE_LOGIN"></s:param>
		</s:action>
	</div>
</div>
</div>
<script type="text/javascript">
		$(document).ready(function() {
			$("#PRNewPassword").strength({
			});
			
		});
		
		
</script>


</s:if>
<s:else>
<!-- Invalidate session as somebody must have landed on this page directly and not through application. -->
	<script type="text/javascript">
		window.location = "/cb/pages/jsp-ns/invalidate-session.jsp"; 
	</script>
</s:else>
