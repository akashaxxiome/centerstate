<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<%-- <link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/passwordmeter.css'/>"/> --%>
<ffi:setProperty name="PageHeading" value="Reset Password"/>
<script type="text/javascript">
	/* $("#PRNewPassword").trigger('focus');
	<s:if test="%{#session.isMSIE}">document.getElementById("PRNewPassword").focus();
	</s:if>
	$(document).ready(function()
	{
		if (loginController) {

			loginController.changeLoginPanelTitle(js_reset_password)
		}
		$("#password-strength-indicator").removeClass();

		// Triggers "click" event when the CONTINUE button has got focus
		// and "Enter" key is hit on the keyboard
		$("#contPasswordRecoveryValidate").keypress(function(event)
		{
			if (event.which == 13) {
				event.preventDefault();
				$('#contPasswordRecoveryValidate').trigger('click');
			}
		});

	}); */
</script>
<style type="text/css">
/*background-image: url("/cb/web/multilang/grafx/loginPageBg.png");
    background-repeat: repeat-x;
    background-size: 100% 100%;	*/
.corpLoginPageStyle, .consLoginPageStyle {
	background: #e1e1e1;
}



/* input[type="password"]
{
  background:transparent;
} */
</style>

<s:include value="/pages/jsp/user/password-indicator-js.jsp" />
<s:include value="/pages/jsp/user/corpadmin_js.jsp" />
<%--
	Requests information necessary to reset the password for a user's login account.
	See the PasswordRecoveryDDD.doc design documentation file for details.

	Note: form field names must match the values in com.ffusion.passwordrecovery.ParameterNames
--%>

<%--=================== Securing the Password Reset page - Part 1 Start  =====================--%>
<%
	if (!(session.getAttribute("UserSecurityKey") instanceof com.ffusion.tasks.util.ImmutableString)) {
%>
<script>
	window.location = "/cb/pages/jsp-ns/invalidate-session.jsp";
</script>
<%
	}
%>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="notEquals">
	<script>
		window.location = "/cb/pages/jsp-ns/invalidate-session.jsp";
	</script>
</ffi:cinclude>
<%
	if (session.getAttribute("UserSecurityKey") instanceof com.ffusion.tasks.util.ImmutableString) {
%>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="equals">
	<script type="text/javascript">
		//$('#PRNewPassword').passwordStrength({targetDiv: '#password-strength-indicator', classes : Array('weak', 'medium', 'strong')});
	</script>
	<%--=================== Securing the Password Reset page - Part 1 End =====================--%>

	<s:form namespace="/pages/jsp-ns" id="passwordRecoveryReset" action="passwordRecoveryReset" method="post" name="ResetForm"
			theme="simple" onsubmit="$('#contPasswordRecoveryReset').trigger('click');return false">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

		<div align="center">
			<table border="0" cellspacing="0" cellpadding="3">
				<tr align="center">
					<td class="instructions" colspan="2"><s:text name="jsp.login_75"/></td>
				</tr>
				<tr><td colspan="2"><s:include value="inc/action-error.jsp"/></td></tr>
				<tr>
					<td nowrap align="right" width="40%">
						<span class="sectionhead"><s:text name="jsp.login_54"/></span>
					</td>
					<td align="left"  width="60%">
						<div style="width:200px;">
						<input id="PRNewPassword" name="PRNewPassword" type="password"  style="opacity: 0.4; width:197px;"  maxlength="15" class="ui-widget-content ui-corner-all inputPassword" tabindex="1"  onchange="evalPswd(this.value);" onkeyup="evalPswd(this.value);"/>
						</div>
						<span id="newPasswordError"></span>
					</td>
				</tr>
				<tr>
					<td nowrap align="right">
						<span class="sectionhead"><s:text name="jsp.default_109"/></span>
					</td>
					<td align="left">
						<s:password id="confirmPwdInputID" name="ConfirmPassword"  style="opacity: 0.4; width:197px;" maxlength="15" cssClass="ui-widget-content ui-corner-all" tabindex="2"/>
						<span id="confirmPasswordError"></span>
					</td>
				</tr>
				<%-- <tr>
					<td nowrap align="right">
						<span class="sectionhead"><s:text name="jsp.login_66"/></span>
					</td>
					<td>
						<div id="password-strength-indicator">
							<div id="iWeak" class="passWeek"><s:text name="jsp.login_98"/></div>
							<div id="iMedium" class="passMedium"><s:text name="jsp.login_50"/></div>
							<div id="iStrong" class="passStrong"><s:text name="jsp.login_85"/></div>
							<ul class="weak">
								<li id="iWeak"><s:text name="jsp.login_98"/></li>
								<li id="iMedium"><s:text name="jsp.login_50"/></li>
								<li id="iStrong"><s:text name="jsp.login_85"/></li>
							</ul>
						</div>
					</td>
				</tr> --%>
				<tr>
					<td colspan="2" class="instructions">
						<span style="width:100%; display:inline-block"><s:include value="/pages/jsp-ns/common/password-strength-text.jsp"/></span>
					</td>
				</tr>
			</table>
		</div>
		<br>

		<div align="center">
			<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="resetPwdCancelButtonID" href="%{ajax}" targets="signin_menu" button="true" tabindex="4" cssClass="buttonSizeReset"><s:text name="jsp.default_82"/></sj:a>
			<sj:a id="contPasswordRecoveryValidate" button="true" tabindex="3" targets="signin_menu"
				  formIds="passwordRecoveryReset" validate="true" validateFunction="customValidation" onBeforeTopics="removeValidationErrors" cssClass="buttonSizeReset">
				<s:text name="jsp.default_111"/></sj:a>
		</div>
		<br>

		<div class="instructions" colspan="3" nowrap align="center">
			<s:text name="jsp.login_35"/>&nbsp;<ffi:getProperty name="AssistancePhoneNumber"/>&nbsp;<s:text name="jsp.login_12"/></div>
	</s:form>


	<%--=================== Securing the Password Reset page - Part 2 Start ====================--%>
</ffi:cinclude>
<%
	}
%>
<script type="text/javascript">

$("#PRNewPassword").strength({
});
</script>
<ffi:removeProperty name="verified"/>
<%--=================== Securing the Password Reset page - Part 2 End =====================--%>
