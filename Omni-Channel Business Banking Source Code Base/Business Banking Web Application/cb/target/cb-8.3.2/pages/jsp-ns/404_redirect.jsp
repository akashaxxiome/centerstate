<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- This page is loaded by the application server from the web.xml 404 error tag. It will not work if there is any struts2 stuff on it. -->
<!-- So all it does is does a client side redirect to the real 404 page which has the struts tags to get the localized text. -->
<html>
<head>
	<script type="text/javascript">
		window.location.href = "/cb/pages/jsp-ns/404.jsp";
	</script></head>
<body>
</body>
</html>
