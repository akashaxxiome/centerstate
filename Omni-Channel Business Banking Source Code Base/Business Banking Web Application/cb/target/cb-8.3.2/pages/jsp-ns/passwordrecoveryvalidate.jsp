<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="java.util.Locale" %>
<%@page import="com.ffusion.util.UserLocaleConsts" %>
<%@page import="com.ffusion.beans.user.UserLocale" %>

<script type="text/javascript">
	$("#passwordAnswer").trigger('focus');
	<s:if test="%{#session.isMSIE}">document.getElementById("passwordAnswer").focus();</s:if>
	$(document).ready(function()
	{
		if (loginController) {
			loginController.changeLoginPanelTitle(js_password_recovery)
		}

		// Triggers "click" event when the CONTINUE button has got focus
		// and "Enter" key is hit on the keyboard
		$("#contPasswordRecoveryValidate").keypress(function(event)
		{
			if (event.which == 13) {
				event.preventDefault();
				$('#contPasswordRecoveryValidate').trigger('click');
			}
		});
	});
</script>
<style type="text/css">
/*background-image: url("/cb/web/multilang/grafx/loginPageBg.png");
    background-repeat: repeat-x;
    background-size: 100% 100%;*/
.corpLoginPageStyle, .consLoginPageStyle {
    background: #e1e1e1;	
}
</style>
<%--
	Requests information necessary to validate ownership of a user's login account.
	See the PasswordRecoveryDDD.doc design documentation file for details.
	Note: form field names must match the values in com.ffusion.passwordrecovery.ParameterNames
--%>

<%-------------------- Get the appropriate resources files containing the PasswordQuestions -----------------%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResource" scope="session"/>
<ffi:setProperty name="UserResource" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResource"/>

<ffi:object name="com.ffusion.tasks.util.ResourceList" id="UserResourceList" scope="session"/>
<ffi:setProperty name="UserResourceList" property="ResourceFilename" value="com.ffusion.beansresources.user.resources"/>
<ffi:process name="UserResourceList"/>

<%
	//This code is needed to switch locale to get english value for chinese text of password questions
	Locale englishLocale = new Locale("en", "US");
	com.ffusion.tasks.util.Resource userResourceTemp = (com.ffusion.tasks.util.Resource)session.getAttribute("UserResource");
	UserLocale userLocale = (UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE);
	Locale originalLocale = userLocale.getLocale();
%>
<div id="validateOwenerShipStep1">
	<s:form id="credentialCheck" namespace="/pages/jsp-ns" action="credentialCheck" method="post" name="ValidateForm" theme="simple"
			onsubmit="$('#contPasswordRecoveryValidate').trigger('click');return false">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

		<div align="center">
			<s:include value="inc/action-error.jsp"/>
			<p class="instructions">
				<s:text name="jsp.login_74"/>
			</p>

			<p>
				<label for="<%=com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER%>">
						<span class="sectionhead">
							<ffi:setProperty name="UserResourceList" property="ResourceID" value="PWD_QUESTION_LIST"/>
							<ffi:getProperty name="UserResourceList" property="Resource"/>
							
							<ffi:list collection="UserResourceList" items="item">
								<ffi:setProperty name="UserResource" property="ResourceID" value="PWD_QUESTION.${item}"/>
								<%
									userResourceTemp.setLocale(englishLocale); //switch locale to get english value
								%>
								<ffi:cinclude value1="${UserResource.Resource}" value2="${User.passwordClue}" operator="equals">
									<%
										userResourceTemp.setLocale(originalLocale); //switch locale to get user locale value
									%>
									<ffi:getProperty name="UserResource" property="Resource"/>
								</ffi:cinclude>
							</ffi:list>
						</span>
				</label>

				<input type="password" name="<%=com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER%>" size="35" maxlength="50" class="ui-widget-content ui-corner-all" tabindex="1" id="passwordAnswer"/>
			<div id="prErrorDiv" style="text-align:center;clear:left;">
				<span id="<%=com.ffusion.passwordrecovery.ParameterNames.PASSWORD_ANSWER%>Error"></span>
			</div>
			</p>
			<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<p>
				<sj:a id="pwdRVCancelButton1ID" href="%{ajax}" targets="signin_menu" button="true" tabindex="3" cssClass="buttonSizeReset"><s:text name="jsp.default_82"/></sj:a>
				<sj:a id="contPasswordRecoveryValidate"
					  formIds="credentialCheck" button="true" tabindex="2"
					  validate="true"
					  validateFunction="customValidation"
					  targets="signin_menu" cssClass="buttonSizeReset"><s:text name="jsp.default_111"/></sj:a>
			</p>
			<p class="instructions">
				<s:text name="jsp.login_35"/>&nbsp;<ffi:getProperty name="AssistancePhoneNumber"/>&nbsp;<s:text name="jsp.login_12"/>
			</p>
		</div>
	</s:form>
</div>
