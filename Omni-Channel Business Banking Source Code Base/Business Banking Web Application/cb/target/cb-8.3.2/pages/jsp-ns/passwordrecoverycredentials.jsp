<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%-- see if there are any multifactor credentials to handle --%>
<s:set var="mfCredentials" value="0" scope="request"/>
<s:iterator value="#session.SecureUser.multifactorInfo" var="next" status="status">
	<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_QUESTION || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_TOKEN || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_SCRATCH_CARD">
		<s:set var="mfCredentials" value="1" scope="request"/>
	</s:if>
</s:iterator>

<%-- If there are no credentials,  credentials collection is null, or empty, forward to the wait.jsp page --%>
<s:if test="#request.mfCredentials == 0">"
	<br>
	<center>
		<span class="sectionhead"><s:text name="jsp.login_77"/></span>
	</center>
	<s:action namespace="/pages/jsp-ns" name="authenticate"/>
	<script type="text/javascript">
		window.location = "/cb/pages/jsp-ns/login-init.jsp";
	</script>
</s:if>
<s:else>
	<s:form id="resetForm" name="ResetForm" method="post" action="authenticate" namespace="/pages/jsp-ns" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<table width=85% cellpadding="0" cellspacing="0" border="0" align="center">
			<tr>
				<td><br></td>
			</tr>
			<tr>
				<td colspan="3" width="100%" align="center" class="instructions" style="padding-left:15px">
					<br><s:text name="login.resetPassword.securityForm.instruction"/></td>
			</tr>
			<tr>
				<td>&nbsp;
				</td>
			</tr>
			<s:iterator value="#session.SecureUser.multifactorInfo" var="next" status="status">
				<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_QUESTION || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_TOKEN || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_SCRATCH_CARD">
					<s:set var="tempType" value="#next.type" scope="session"/>
					<tr>
						<td style="word-wrap:break-word;" class="sectionhead">
							<label for="secureUser.MultifactorInfo[<s:property value='#status.index'/>].response"><s:text name="%{@com.ffusion.beans.authentication.AuthConsts@CREDENTIAL_FIELD_NAME_PREFIX}%{#next.type}"/>:</label><span class="required">*</span>
							<s:property value="#next.challenge" escape="false"/>
						</td>
						<td class="sectionhead">
						<input class="credentialUserInput ui-widget-content ui-corner-all" id="secureUser.MultifactorInfo[<s:property value="#status.index"/>].response" name="secureUser.MultifactorInfo[<s:property value="#status.index"/>].response" size="20" maxlength="50" required="true" tabindex="2" type="password" />
					</tr>
				</s:if>
			</s:iterator>
			<tr>
				<td><span id="secureUser.multifactorInfoError"></span></td>
			</tr>
			<tr>
				<td colspan="4" class="required" align="center">* <s:text name="jsp.default_240"/><br>
				</td>
			</tr>

			<tr>
				<td colspan="3" class="filter_table" align="center" valign="top"><br>
					<sj:a id="enterResetFormButton"
						  formIds="resetForm"
						  button="true"
						  targets="signin_menu"
						  onBeforeTopics="removeValidationErrors"
						  validate="true"
						  validateFunction="customValidation"
						  indicator="indicator"
						  effect="highlight"
						  effectOptions="{ color : '#E8F4FD' }"
							><s:text name="jsp.login_47"/></sj:a>
				</td>
			</tr>
			<tr>
				<td class="instructions" align="center" colspan="100%"><br><s:text name="jsp.login_56"/></td>
			</tr>
		</table>
	</s:form>
</s:else>
