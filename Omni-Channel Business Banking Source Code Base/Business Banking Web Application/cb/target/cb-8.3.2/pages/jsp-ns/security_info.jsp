<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<center>
    <table cellspacing="0" border="0" align="center" width="100%">
	<tr>
	    <td width="32"><img src="/cb/web/multilang/grafx/arrowhead.gif" alt=""></td>
	    <td class="pagehead"><s:text name="jsp.login_83"/></td>
	</tr>
    </table>
    <p>
    <table width="500" border="0" cellspacing="0" cellpadding="0">
	<tr>
	    <td><img src="/cb/web/multilang/grafx/corner_al.gif" alt="" height="7" width="11" border="0"></td>
	    <td align="right" class="greenClayBackground">
		<div align="center">
		    <img src="/cb/web/multilang/grafx/spacer.gif" height="7" width="478" border="0"></div>
	    </td>
	    <td align="right" ><img src="/cb/web/multilang/grafx/corner_ar.gif" alt="" height="7" width="11" border="0"></td>
	</tr>
	<tr>
	    <td background="/cb/web/multilang/grafx/corner_cellbg.gif"><br><br></td>
	    <td align="left" class="greenClayBackground">
		<div align="center">
		    <table width="95%" border="0" cellspacing="0" cellpadding"0" align="center">
			<tr>
			    <td class="tbrd_b">
				<span class="sectionhead">&gt; <s:text name="jsp.login_100"/></span>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_59"/>
			    </td>
			</tr>
			<tr class="sectionsubhead"><td>&nbsp;</td></tr>
			<tr>
			    <td class="sectionsubhead" align="left"><s:text name="jsp.login_86"/></td>
			</tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_41"/>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="sectionsubhead" align="left"><s:text name="jsp.login_81"/></td>
			</tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_58"/>			    
				<p><s:text name="jsp.login_102"/>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="sectionsubhead" align="left"><s:text name="jsp.login_62"/></td>
			</tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_91"/>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="sectionsubhead" align="left"><s:text name="jsp.login_13"/></td>
			</tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_105"/>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="sectionsubhead" align="left"><s:text name="jsp.login_78"/></td>
			</tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_38"/>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="sectionsubhead" align="left"><s:text name="jsp.login_32"/></td>
			</tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_37"/>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="tbrd_b">
				<span class="sectionhead">&gt; <s:text name="jsp.login_101"/></span>
			    </td>
			</tr>
			<tr><td class="sectionsubhead">&nbsp;</td></tr>
			<tr>
			    <td class="columndata">
				<s:text name="jsp.login_8"/>
				
				<p><s:text name="jsp.login_6"/>
				
				<p><s:text name="jsp.login_10"/>
				
				<p><s:text name="jsp.login_52"/>
				
				<p><center><s:text name="jsp.login_2"/></center>
				
				<p><s:text name="jsp.login_15"/>
				
				<p><s:text name="jsp.login_14"/>
			    </td>
			</tr>

		    </table>
    
		    <br>
		</div>
	    </td>
	    <td align="right" background="/cb/web/multilang/grafx/corner_cellbg.gif"><br>
	    </td>
	</tr>
	<tr>
	    <td><img src="/cb/web/multilang/grafx/corner_bl.gif" alt="" height="7" width="11" border="0"></td>
	    <td class="greenClayBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="7" border="0"></td>
	    <td><img src="/cb/web/multilang/grafx/corner_br.gif" alt="" height="7" width="11" border="0"></td>
	</tr>
    </table>
    <div align="center">
	<p class="columndata">
	    <input class="submitbutton" type="button" name="Close" value="<s:text name="jsp.default_102"/>" onclick="window.close()">
	</p>
    </div>

</center>



