<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<style type="text/css">
/*
	Login page background and branding
*/
/*background-image: url("/cb/web/multilang/grafx/loginPageBg.png");
    background-repeat: repeat-x;
    background-size: 100% 100%;	*/
.corpLoginPageStyle, .consLoginPageStyle {
    background: #e1e1e1;
}
.headePane{background: none !important;}
.centerLayoutPane{background: none !important;}
.footerPane{overflow:hidden !important;}
.instructions{color: rgba(0,156,120,1) !important; font-size:18px !important;} 
.instructions{margin: 0 auto; padding: 0; text-align: center;}
.desktop{margin: 0; position: relative; overflow: hidden;}
.passwordEntryPageStyle{background-color: rgba(255, 255, 255, 0.6); border-radius: 5px; height: 80%; margin: 0 auto;  width: 80%; position: relative;}
.logoutPage{background-color: none; border-radius: 5px; height: 80%; margin: 0 auto;  width: 80%; position: relative;}

#backRef {
	color: #333A9B;
}
</style>
<div class="pageWrapper" style="position: relative; overflow: hidden; width: 80%; height: 100%; margin: 0 auto;">
	<div class="logoutPage" id="loginPageHolderDiv">
		<div class="desktop" id="logoutDesktop">
			<!-- <div id="logoutPanel" class="logoutPanel" style="text-align:center;"> -->
				<div class="instructions" tabindex="2">
					<s:if test="%{#parameters.timedout}">
					 	<s:text name="jsp.login_115"/>
					</s:if>
					<s:else>
						<s:text name="jsp.login_103"/>
					</s:else>
					<s:text name="jsp.login_68"/>
					<s:if test='%{"CB".equals(#parameters.LoginAppType[0])}'>
						<s:url var="loginUrl" value="/pages/jsp-ns/login-corp.jsp" escapeAmp="false">
							<s:param name="request_locale" value="#parameters.request_locale"/>
						</s:url>
						<span onfocus="setFocus()" tabindex="3"><a id="backRef" href="<s:property value='%{loginUrl}'/>" tabindex="3"><s:text name="jsp.login_34"/></a></span>
					</s:if>
					<s:else>
						<s:url var="loginUrl" value="/pages/jsp-ns/login-cons.jsp" escapeAmp="false">
							<s:param name="request_locale" value="#parameters.request_locale"/>
						</s:url>
						<span onfocus="setFocus()" tabindex="3"><a id="backRef" href="<s:property value='%{loginUrl}'/>" tabindex="3"><s:text name="jsp.login_34"/></a></span>
					</s:else>
					<s:text name="jsp.login_90"/>
				</div>
			<!-- </div> -->
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		setTimeout("$('#backRef').focus();", 500);
	})
	
	function setFocus() {
		$('#backRef').focus();
	}
</script>