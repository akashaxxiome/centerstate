<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<HTML>
<HEAD>
	<ffi:object id="AuditLogout" name="com.ffusion.tasks.user.AuditLogout" scope="session"/>
	<ffi:setProperty name="AuditLogout" property="TimedOut" value="${TimedOut}"/>
	<ffi:process name="AuditLogout"/>

	<s:if test="%{#session.SecureUser.Agent == null}">
		<s:if test="%{#parameters.TimedOut}">
			<s:set var="continueJsp" value="%{'/pages/jsp-ns/no-authenticate.jsp'}" scope="page"/>
		</s:if>
		<s:else>
			<s:set var="continueJsp" value="%{'/pages/jsp-ns/logout.jsp'}" scope="page"/>
		</s:else>

		<s:url var="continueUrl" value="%{#attr.continueJsp}" escapeAmp="false">
			<s:param name="request_locale" value="#parameters.request_locale"/>
			<s:param name="themeName" value="#session.themeName"/>
			<s:param name="LoginAppType" value="#session.LoginAppType"/>
		</s:url>

		<script>location.replace('<s:property value="%{continueUrl}" escape="false"/>');</script>
		<% session.invalidate(); %>
	</s:if>
	<s:elseif test="%{#session.OBONewWindow}">
		<script>window.close();</script>
		<% session.invalidate(); %>
	</s:elseif>
	<s:else>
		<ffi:object id="RestoreSession" name="com.ffusion.tasks.obo.OBORestoreArchivedSession" scope="session"/>
		<ffi:process name="RestoreSession"/>
		<s:url var="continueUrl" value="%{#session.OBOReturnURL}" escapeAmp="false">
			<s:param name="request_locale" value="#parameter.request_locale"/>
		</s:url>
		<script>location.replace('<s:property value="%{continueUrl}" escape="false"/>');</script>
	</s:else>
</HEAD>
</HTML>