<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div >			
			<s:set var="tmpI18nStr" value="%{getText('jsp.login_40')}" scope="request" /><ffi:setProperty name="navTitle" value="${tmpI18nStr}"/>
			<ffi:setProperty name="showDate" value="true"/>
			<ffi:removeProperty name="navTitle"/>
			<ffi:removeProperty name="showDate"/>
	
<form action='<ffi:getProperty name="PathExtNs"/>invalidate-session.jsp' method='post' name='pswdChangeForm'>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<div align="center">
				<div class="ui-state-error ui-corner-all">
					<p>
						<span style="float: left;"></span> 
						<s:text name="jsp.login_40"/>:<strong><s:text name="jsp.login_106"/></strong>
					</p>
				</div>
			
				<div class="instructions">
				<s:text name="jsp.login_69"/>&nbsp;<ffi:getProperty name="AssistancePhoneNumber" />&nbsp;<s:text name="jsp.login_12"/>
				</div>
				<div align="center">
					<a 
					id="backButton"
					button="true" 
					 href="/cb/pages/jsp-ns/invalidate-session.jsp"
					 class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
					 style="padding:2px 2px 2px 2px;"
					><s:text name="jsp.default_303"/></a>
				</div>
	</div>
</form>
 
<ffi:setProperty name='currentMsgType' value='<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>' />
<ffi:setProperty name='BackGround' value="greenClayBackground" /> 
<div id="globalmessage" class="loginGlobalMessage">
	 <div id="loginMessagePanel">
					<ffi:setProperty name='msgType' value='<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>' />
					<ffi:setProperty name='BackGround' value="greenClayBackground" />
			<s:action namespace="/pages/common" name="GlobalMessageAction_getLoginGlobalMessages" executeResult="true">			
				<s:param name="msgType" value="@com.ffusion.beans.messages.GlobalMessageConsts@MSG_TYPE_LOGIN"></s:param>
			</s:action>
	 </div>	
</div>
</div>
<ffi:setProperty name="BackURL" value="/cb/servlet/cb/${PathExtNs}must-changepassword.jsp"/>
<script type="text/javascript">	
	
	$(document).ready(function() {
		if(loginController){
			loginController.changeLoginPanelTitle(js_inactive_account);
		}
	});
</script>
