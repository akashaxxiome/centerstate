/**
 * uiroller is responsible for switching the main page of a certain module (e.g. Transfer)
 * among different modes. Specifically, when switching from one mode to another, it turn on 
 * the UI widgets defined by the new mode and turn off others and trigger certain events if 
 * necessary.
 */
ns.common.UIRoller = function(){
	
	//alert('uiroller created');

	
	//Used to hold a list of mode definitions
	//A mode is defined by a mode name (key) and an object containing components which should be
	//displayed (opened) for the mode. 
	//Mode properties is an object that consists of the IDs of the widgets that should be turned 
	//on in the mode and the callbacks to turn them off if necessary.
	var modes = new Object();
	
	//Used to save all the UI components defined by all UI modes registered.
	//Each time a UI mode is registered, the UI components defined by the UI mode will be added 
	//to this object. 
	//Different UI modes main the same UI component. In this case, saved UI components always get
	//overwritten by those defined in UI component added later.
	var allComponents = new Object();


	var modeHistory = [];
	var modeIndex = -1;
	
	//The name of current mode
	var currentMName;
	
	/**
	 * Default handler for closing a UI component
	 */
	var defaultClose = function(compId) {
		var e = document.getElementById(compId);
		if (e) {
			var compProps = allComponents[compId];
			compProps["display"] = e.style.display; 
			e.style.display = "none";
		}
	}
	
	/**
	 * Default handler for opening (showing) a UI component
	 */
	var defaultOpen = function(compId){
		var e = document.getElementById(compId);
		if (e) {
			var compProps = allComponents[compId];
			var oldDisplay = compProps["display"];
			if(oldDisplay) {
				e.style.display = oldDisplay;
			} else {
				e.style.display = "";
			}
		}
	} 

	/**
	 * Default handler to check if a UI component is showing
	 */
	var defaultIsOpen = function(compId){

		var e = document.getElementById(compId);
		if(!e)
			return false;
		if(e.style.display.toLowerCase() != 'none' && e.style.visibility.toLowerCase() != 'hidden'){
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Add a component to the component list that uiroller will manage. Components can be
	 * added with a mode, but as uiroller isn't aware of them before adding the mode, it 
	 * can not close them. So it's user's responsibility to make sure they are close if 
	 * necessary before uiroller knows them.
	 * compId is the DOM element Id corresponding to the component. 
	 * compProps is an object containing necessary callbacks/properties for the component.
	 */
	this.addComponent = function(compId, compProps){
		
		if(!compProps)
			compProps = new Object();
	
		saveComponent(compId, compProps);
	}
	
	
	var saveComponent = function(compId, compProps){
		var savedComp = allComponents[compId]; 
		var newComp = compProps;
		if(savedComp){ //The UI component may be already added with other UI modes or individually
			if (newComp["openCallback"]) {
				savedComp["openCallback"] = newComp["openCallback"];
			} else {
				newComp["openCallback"] = savedComp["openCallback"];
			};
			if (newComp["closeCallback"]) {
				savedComp["closeCallback"] = newComp["closeCallback"];
			} else {
				newComp["closeCallback"] = savedComp["closeCallback"];
			};
			if (newComp["isOpenCallback"]) {
				savedComp["isOpenCallback"] = newComp["isOpenCallback"];
			} else {
				newComp["isOpenCallback"] = savedComp["isOpenCallback"];
			};

		} else {
			if (!newComp["openCallback"]) {
				newComp["openCallback"] = defaultOpen;
			}
			if (!newComp["closeCallback"]) {
				newComp["closeCallback"] = defaultClose;
			}
			if (!newComp["isOpenCallback"]) {
				newComp["isOpenCallback"] = defaultIsOpen;
			}
			allComponents[compId] = newComp; 
		}
		
	}
	
	
	
	//register a new mode
	//If no openCallback, closeCallback, or isOpenCallback are defined for a UI component, UIRoller
	//will assign default handlers to it. 
	//In addition, an UI component may be added to different UI modes with different callbacks each
	//time. Though this should not happen, the new callbacks will override old ones.
	this.addMode = function(uiMode) {
		var modeName = uiMode.getName();
		var modeComponents = uiMode.getComponents();
		//alert('add mode: ' + modeName);
		modes[modeName] = uiMode;
		
		//Save the UI components to a list which contains components defined by all modes.
		for(var compId in modeComponents ) {
			var newComp = modeComponents[compId];
			saveComponent(compId, newComp);
		}
	};
	
	
	this.removeMode = function(modeName) {
		delete modes[modeName];
	};
	
	this.displayModes = function() {
		
		for(var modeName in modes) {
			alert(modeName);
		    //if(obj.hasOwnProperty(prop))
		    //    doSomethingWith(obj[prop]);
		}
	};
	
	
	var switchMode = function(modeName) {
		var uiMode = modes[modeName];
		if(!uiMode) {
			alert('Invalid mode name.');
			return;
		}
		
		var beforeCallback = uiMode.getOnBefore();
		var completeCallback = uiMode.getOnComplete();

		//if there will be an ajax request call onComplete upon the request completes
		if(uiMode.getAjax() && completeCallback)
			ns.common.setAjaxOptions(completeCallback);
		if(beforeCallback)
			beforeCallback();
		
		
		var modeComponents = uiMode.getComponents();
		
		for(var compId in allComponents){
			if(modeComponents[compId]) 
				continue; //don't close components included in current mode to open
			var savedComp = allComponents[compId];
			if(savedComp["isOpenCallback"](compId)) {
				//alert("about to close: " + compId);
				savedComp["closeCallback"](compId);
			}
		}
			
		for (var compId in modeComponents) {
			var modeComp = modeComponents[compId];
			if(modeComp["isOpenCallback"](compId)) 
				continue; //don't open components which are already open
			//alert("about to open: " + compId);
			modeComp["openCallback"](compId);
		}
		
		//Call onComplete here only when there will not be an ajax request
		if(!uiMode.getAjax() && completeCallback)
			completeCallback();
		
		currentMName = modeName;
	}
	
	/**
	 * Switch to the specified UI mode. Display (open) the UI components for the UI 
	 * mode and hide (close) all others.
	 * The onBefore and onComplete callbacks of the specified UI mode will be called
	 * respectively.
	 */
	this.gotoMode = function(modeName) {
		
		switchMode(modeName);
		modeHistory.push(modeName);
		modeIndex = modeHistory.length - 1;
	};
	

	/**
	 * Return back to the previous mode in mode history
	 */	
	this.back = function() {
		var modeNum = modeHistory.length;
		if(modeNum == 0){
			alert('no mode history');
			return;
		}
		if(modeIndex == 0){
			alert('can not back');
			return;
		}
		if(modeIndex == -1){
			modeIndex = modeNum - 1;
		}
		modeIndex--;
		alert("back to: " + modeHistory[modeIndex]);
		switchMode(modeHistory[modeIndex]);
		
	}
	
	/**
	 * Go to the next mode in mode history
	 */
	this.next = function() {
		var modeNum = modeHistory.length;
		if(modeNum == 0){
			alert('no mode history');
			return;
		}
		if(modeIndex == -1){
			modeIndex = modeNum - 1;
		}
		if(modeIndex == modeNum - 1){
			alert('can not next');
			return;
		}

		modeIndex++;
		alert("next to: " + modeHistory[modeIndex]);
		switchMode(modeHistory[modeIndex]);
	}
	
	/**
	 * Get an Array of mode names which defines the path from root to the current mode.
	 * Each mode except root mode has a parent mode where it's triggered.
	 */
	this.getModePath = function() {
		var path = [];
		var curMode = modes[currentMName];
		while(curMode.getParent()){
			path.push(curMode.getName());
			curMode = modes[curMode.getParent()];
		}
		path.push(curMode.getName());
		return path;
	}
	
	
};

/**
 * UIMode defines which UI components should be showed related to certain functionality on a page. 
 * Specifically, It's used to control the main page of a module (i.e. Transfer).
 */
ns.common.UIMode = function(modeName, parentModeName) {
	
	//alert("creating UIMode: " + modeName);
	
	var name = modeName;
	var components = new Object();
	
	var onBefore; 	//A callback called before switching to this mode
	var onComplete; //A callback called after switching to this mode
	
	var ajax;  //If an Ajax request will be sent after switching to this mode.
			   //If so, the onComplete callback should be triggered when the Ajax request complete
	
	var parentMName = parentModeName; //The mode name of the parent mode where this mode is triggered
									  //May be used to build a mode path
	
	var displayName;
	
	this.setName = function(mname) {
		name = mname;
	}
	
	this.getName = function() {
		return name;
	} 
	
	
	this.setParent = function(parentModeName2) {
		parentMName = parentModeName2;
	}
	
	this.getParent = function() {
		return parentMName;
	}
	
	this.setDisplayName = function(dname) {
		displayName = dname;
	}
	
	this.getDisplayName = function() {
		return displayName;
	}
	
	this.setOnBefore = function(beforeHandler) {
		onBefore = beforeHandler;
	}
	
	this.getOnBefore = function() {
		return onBefore;
	}	
	
	this.setOnComplete = function(completeHandler) {
		onComplete = completeHandler;
	}
	
	this.getOnComplete = function() {
		return onComplete;
	}
	
	this.setAjax = function(isAjax) {
		ajax = isAjax;
	}
	
	this.getAjax = function() {
		return ajax;
	}
	
	/**
	 * Add a component which should be displayed (opened) in this mode.
	 * compId is the DOM element Id corresponding to the component. 
	 * compProps is an object containing necessary callbacks/properties for the component.
	 */
	this.addComponent = function(compId, compProps) {
		
		if(!compProps)
			compProps = new Object();
	
		components[compId] = compProps;
	}
	
	this.removeComponent = function(compId) {
		delete components[compId];
	}
	
	this.getComponent = function(compId) {
		return components[compId];
	}
	
	
	this.getComponents = function() {
		return components;
	}
	
	this.copyTo = function(newMName) {
		var newMode = new ns.common.UIMode(newMName);
		for (var compId in components) {
			newMode.addComponent(compId, components[compId]);
		}
		return newMode;
	}
};

/**
 * Tweak ajax options to execute mode onComplete callback upon ajax request 
 * (if any) completes
 */
ns.common.setAjaxOptions = function(modeOnComplete){

	var oneOffComplete = function(data, textStatus, xmlHttpRequest) {
		$.ajaxSettings.complete = ns.common.ajaxComplete;
		var completeCallback = modeOnComplete;
		modeOnComplete = undefined; //avoid duplicate calls as this is a closure
		if (completeCallback)
			completeCallback();
	};

	$.ajaxSettings.complete = oneOffComplete;
	oneOffComplete = undefined;
};

