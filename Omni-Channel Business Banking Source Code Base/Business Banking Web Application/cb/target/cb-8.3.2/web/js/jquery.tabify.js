var tabify = (function($){
	//defualt settings of the plugin 
	var settings = {
		baseIndex:9000
	}
	 
	$.fn.tabify=function(options){
		//override the default settings with options passed
		if(options){
			settings = $.extend(settings,options);
		}

		var children = this.children();
		if (typeof children == "undefined" || children.size() === 0) {
			return this;
		};
		
		var _setTabIndex = function(e){
			//( !e.parent().is(":hidden")  && !e.parent().hasClass("ui-state-disabled")  &&  !e.is(":hidden")
			if(e.is("input") || e.is("textarea") || e.is("select") || e.is("a") ||  e.is("button") || e.hasClass("ui-datepicker-trigger")){				
				$(e).attr("tabIndex",settings.baseIndex ++);				
				//console.info("setting tab for  " + $(e).attr("name")  + ", and id " +  $(e).attr("id")  + "  tabid:" + settings.baseIndex);
			}
		};

		var _needToTabifyChildren = function(e){
			var childrens = e.children().size();
			var needToTabifyChildren = false;
			if(childrens>0){
				needToTabifyChildren = true;
				if(e.is("select") || e.is("button")){// exclude select menu options from getting tabified
					needToTabifyChildren = false;
				}
			}
			return needToTabifyChildren;
		};

		for (var i = 0; i < children.length; i++) {
			var child = $(children[i]); 
			if(_needToTabifyChildren(child)){					
				if(child.is("a") ){
				_setTabIndex(child);
				}else{
					$(child).tabify(settings.baseIndex);
				}					
			}else{
				_setTabIndex(child);
			}
		};
		return this; //to maintain chainability we should return this object as it it
	};	
})(jQuery);