var termsController = (function(){
	function _initView(){
		_initLayout();
		$("#termsAndConditions").pane({
			"title":js_terms_and_conditions_header,
			"minmax":false,
			"close":false
		});			
	};
	
	function _initLayout(){
		$('body').layout({ 
			applyDefaultStyles: true,
			north:{
				size:100,
				resizable :false,
				closable :false
			},
			south:{
				size:50,
				resizable :false,
				closable :false
			}				
		});
	};

	
	function _registerEvents(){
		
		$.subscribe('acceptTerms',function() {
			termsController.accept();
		});	
		
		$.subscribe('declineTerms',function() {
			termsController.decline();
		});
		
		$.subscribe('invalidateTerms',function() {
			termsController.invalidate();
		});
		
		$.subscribe('backToTermsAndCondition',function() {
			termsController.back();
		});

	};
	
	function _accept(){
		//show loading sign
		$('body').showLoading();
		//location.replace(acceptTC);
		$.ajax({
			url: acceptTC,
			type: 'GET',
			cache: false,
			dataType: 'HTML',
			success:function(data){
				document.forms[0].submit();
			}
		});
	}

	function _decline(){
		if(declineTC){
			document.location= declineTC;
		}		
	}

	function _invalidate(){
		if(invalidateSessionTc){
			document.location= invalidateSessionTc;
		}
	}
	
	function _back(){
		if(backTC){
			document.location= backTC;
		}		
	}
	
	function logIntoApplication(){
		$.ajax({
			url: '/cb/pages/jsp-ns/loginUserRedirect.action',
			type: 'POST',
			cache: false,
			dataType: 'HTML'
		});
	}

	return {
		init:function(){
			_initView();
			_registerEvents();			
		},
		accept:_accept,
		decline:_decline,
		invalidate:_invalidate,
		back:_back
};
	
}());

$(document).ready(function() {
	termsController.init();	
});