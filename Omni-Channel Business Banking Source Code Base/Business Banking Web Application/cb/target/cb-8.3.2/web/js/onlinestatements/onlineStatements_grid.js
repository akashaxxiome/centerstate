	//formatter for online statement transaction list action
	ns.onlineStatements.formatActionLinks = function(cellvalue, options, rowObject) { 
		//cellvalue is the ID of the transfer.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.onlineStatements.viewTransactionDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		return view;
	};
	
	//use to view transaction details in transaction list
	ns.onlineStatements.viewTransactionDetails = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewOnlineStatementsTransactionDetailDialogID').html(data).dialog('open');
			}
		});
	};
	
	// Function to decide whether to show link or plain text depending on typeValue of transaction
	ns.onlineStatements.formatDescription= function (cellvalue, options, rowObject){
		var description = rowObject["description"];
				
		// If Type value matches with constant defined for Check transaction, then show link
		if(rowObject["typeValue"] == ns.onlineStatements.typeCheckValue){
			description = "<a href=\"#\" onClick=\"ns.onlineStatements.viewCheckImage('"+rowObject.map.LinkURL+"')\" class=\"anchorText\" >" + description + "</a>";
		}
		return description;
	};
	
	// Open modal dialog to show Check image.
	ns.onlineStatements.viewCheckImage = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				// Refer dialog defined in home/index.jsp page
				$('#checkImageDialogID').html(data).dialog('open');
			}
		});
	};
	
	ns.onlineStatements.setup = function(){
		var config = ns.common.dialogs["checkImage"];
		config.autoOpen = "false";
		ns.common.openDialog("checkImage");	
	}	
	
	$(document).ready(function() {
		ns.onlineStatements.setup();
	});
	