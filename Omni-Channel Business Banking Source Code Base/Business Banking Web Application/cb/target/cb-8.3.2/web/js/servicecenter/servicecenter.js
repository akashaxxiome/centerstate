ns.servicecenter.setup = function() {

	
	ns.common.refreshDashboard('dbOnlineCustomerSummary');
	
	$.subscribe('beforeLoadServicesForm', function(event, data) {
		// $.log('About to load form! ');
		ns.common.hideStatus();
	});

	/*
	 * Load Services form (
	 */
	$.subscribe('loadServicesFormComplete', function(event, data) {
		//ns.servicecenter.foldPortlet();

		/*
		 * Set portlet title (defined in each page loaded to inputDiv by Id
		 * "PageHeading"
		 */
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		$('#summary').hide();
		$('#details').show();
		$('#details').slideDown();
		
		ns.servicecenter.clearAddAccountsForm();
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#details").setFocus();
		}
	});
	
	$.subscribe('onCustServicesLoadComplete', function(event, data) {
		$('#details').hide();
		$('#summary').show();
	});

	$.subscribe('errorLoadServicesForm', function(event, data) {
		// $.log('Form Loading error! ');
	});

	$.subscribe('cancelServicesForm', function(event, data) {
		// $.log('About to cancel Form! ');
		ns.servicecenter.closeServicesTabs();
		ns.common.hideStatus();
		ns.servicecenter.gotoBackURL();
		ns.servicecenter.clearAddAccountsForm();
	});

	/** Bill Pay Accounts */
	/*
	 * Add Online Accounts, Page (Confirm)
	 */
	$.subscribe('beforeConfirmAddBillPayAccountsForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmAddBillPayAccountsFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmAddBillPayAccountsForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmAddBillPayAccountsForm', function(event, data) {
		// $.log('Form success! ');
		$.publish('refreshMessageSmallPanel');
		ns.servicecenter.closeServicesTabs();
		var statusMessage = $('#operationresult').text();
		ns.servicecenter.gotoBackURL();
		ns.common.showStatus(statusMessage);
	});
	/** Bill Pay Accounts */

	/** Online Accounts */
	/*
	 * Add Online Accounts, Page (Confirm)
	 */
	$.subscribe('beforeConfirmAddOnlineAccountsForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmAddOnlineAccountsFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmAddOnlineAccountsForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmAddOnlineAccountsForm', function(event, data) {
		// $.log('Form success! ');
		$.publish('refreshMessageSmallPanel');
		ns.servicecenter.closeServicesTabs();
		var statusMessage = $('#operationresult').text();
		
		//QTS:752466
		ns.userPreference.tabToBeSelected = 1;
		if($('#backLinkURL').length) {
			$('li[menuId=home_prefs] a').click();
		}
		ns.common.showStatus(statusMessage);
	});
	/** Online Accounts */

	/** Order Cleared Checks */
	/*
	 * Add Online Accounts, Page (Confirm)
	 */
	$.subscribe('beforeConfirmOrderClearedChecksForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmOrderClearedChecksFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmOrderClearedChecksForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmOrderClearedChecksForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Order Cleared Checks */

	/** Order Statement Copies */
	/*
	 * Order Statement Copies, Page (Confirm)
	 */
	$.subscribe('beforeConfirmOrderStatementForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmOrderStatementFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmOrderStatementForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmOrderStatementForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Order Statement Copies */

	/** Order Deposit Envelopes */
	/*
	 * Order Deposit Envelopes, Page (Confirm)
	 */
	$.subscribe('beforeConfirmOrderEnvelopesForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmOrderEnvelopesFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmOrderEnvelopesForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmOrderEnvelopesForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Order Deposit Envelopes */

	/** General Questions or Comments */
	/*
	 * General Questions or Comments, Page (Confirm)
	 */
	$.subscribe('beforeConfirmGeneralQuestionsForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmGeneralQuestionsFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmGeneralQuestionsForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmGeneralQuestionsForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** General Questions or Comments */

	/** Bill Payment Inquiry */
	/*
	 * Bill Payment Inquiry, Page (Confirm)
	 */
	$.subscribe('beforeConfirmBillPaymentInquiryForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmBillPaymentInquiryFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmBillPaymentInquiryForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmBillPaymentInquiryForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Bill Payment Inquiry */

	/** Transfer Inquiry */
	/*
	 * Transfer Inquiry, Page (Confirm)
	 */
	$.subscribe('beforeConfirmTransferInquiryForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmTransferInquiryFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmTransferInquiryForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmTransferInquiryForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Transfer Inquiry */

	/** Bank Fee Inquiry */
	/*
	 * Bank Fee Inquiry, Page (Confirm)
	 */
	$.subscribe('beforeConfirmBankFeeInquiryForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmBankFeeInquiryFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmBankFeeInquiryForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmBankFeeInquiryForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Bank Fee Inquiry */

	/** Account History Inquiry */
	/*
	 * Account History Inquiry, Page (Confirm)
	 */
	$.subscribe('beforeConfirmAccountHistoryInquiryForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmAccountHistoryInquiryFormComplete', function(event, data) {
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmAccountHistoryInquiryForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmAccountHistoryInquiryForm', function(event, data) {
		// $.log('Form success! ');
		ns.servicecenter.closeServicesTabs();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);

		$.publish('refreshMessageSmallPanel');

	});
	/** Account History Inquiry */

	/** Session Receipt */
	/*
	 * Session Receipt, Page (Confirm)
	 */
	$.subscribe('beforeSessionReceiptFilterForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmSessionReceiptFilterComplete', function(event, data) {
		// $.log('Form Verified! ');
		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setFocus();
		}
	});

	$.subscribe('errorConfirmSessionReceiptFilterForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmSessionReceiptFilterForm', function(event, data) {
		// $.log('Form success! ');
		$('#sessionReceiptResultPortID').show();
	});
	/** Session Receipt */
	
	$.subscribe('services.clearMessageBoxArea', function(event,data) {		
		$("#subject").val('');
		$("#sendMemo").val('');
		CKEDITOR.instances.sendMemo.setData('');
	});
};

/* Go back to backLinkURL location */
ns.servicecenter.gotoBackURL = function() {
	if($('#backLinkURL').length) {
		var backURL = $('#backLinkURL').val();
		ns.shortcut.goToFavorites(backURL);
	}
};

ns.servicecenter.foldPortlet = function() {
	$('#onlineCustomerServicesTab').portlet('fold');
	$('#documentCenterTab').portlet('fold');
	$('#contactInfoTab').portlet('fold');
};

ns.servicecenter.expandPortlet = function() {
	$('#onlineCustomerServicesTab').portlet('expand');
	$('#documentCenterTab').portlet('expand');
	$('#contactInfoTab').portlet('expand');
};

/**
 * Close Details island and expand Summary island
 */
ns.servicecenter.closeServicesTabs = function() {
	//ns.servicecenter.expandPortlet();
	$('#details').slideUp();
	$('#details').hide();
	$('#summary').show();
};

ns.servicecenter.editUserProfile = function(inputPageURL, inputLink) {
	$.ajax({
		url: inputPageURL,
		success: function(success) {
			//Set tabs to be selected
			ns.userPreference.tabToBeSelected = 0;
			ns.shortcut.goToFavorites(inputLink);
			$("#preferencesTabs").tabs( "option", "selected", 0);
      }
    });
};

ns.servicecenter.cancelAddAccounts = function(backURL) {
	if(backURL != null) {
		ns.shortcut.goToFavorites(backURL);
	} else {
		ns.servicecenter.closeServicesTabs();
		ns.common.hideStatus();
	}
};

ns.servicecenter.clearAddAccountsForm = function(){
	$("#accountType-1").selectmenu("value","-1");
	$("#accountType-2").selectmenu("value","-1");
	$("#accountType-3").selectmenu("value","-1");
	$("input[name=ContactPhoneNumber]").val("");
	$("#accountNumber-1").val("");
	$("#accountNumber-2").val("");
	$("#accountNumber-3").val("");
	$("textarea[name=Question]").val("");	
	$("#accountID").combobox("clear");
	$("#accountNumberId").combobox("clear");
};

ns.servicecenter.disableUsers = function(disable){
	if(disable == true){
		//$("#sessionReceiptFilterForm input[type=checkbox][user=primary]").attr("checked",true);
		$("#userList").extmultiselect('disable','true');
	}
	else{
		$("#userList").extmultiselect('enable','true');
	}
	//$("#sessionReceiptFilterForm input[type=checkbox]").attr("disabled",disable);
};

ns.servicecenter.disableUsersOnCriteriaChange = function(){
	var dateRangeValue = $("#dateRangeDropDown").val();
	if(dateRangeValue == ns.servicecenter.criteriaCurrentSession){
		ns.servicecenter.disableUsers(true);	
	}else{
		ns.servicecenter.disableUsers(false);	
	}
};


ns.servicecenter.initSecondaryUsersEvents = function(){
	$("#sessionReceiptFilterForm input[type=radio]").on("click",function(e){
		var radioValue = $(this).val();
		if(radioValue == ns.servicecenter.absoluteRange){
			ns.servicecenter.disableUsers(false);
		}else{
			ns.servicecenter.disableUsersOnCriteriaChange();	
		}			
	});

	$("#dateRangeDropDown").on("change",function(e){
		var dateRangeValue = $(this).val();
		if(dateRangeValue == ns.servicecenter.criteriaCurrentSession){
			ns.servicecenter.disableUsers(true);	
		}else{
			ns.servicecenter.disableUsers(false);	
		}
	});

	$("#sessionReceiptFilterForm input[daterangetype=absolute]").on("focus",function(){
		ns.servicecenter.disableUsers(false);	
	});
};

$.subscribe('contactInfoLoadFunction', function(event,data){
	$("#details").hide();
	$("#summary").show();	
});

//Show the active dashboard based on the user selected summary - FOR ServiceCenter & FAQ
ns.servicecenter.showServiceCenterDashboard = function(serviceCenterUserActionType, serviceCenterDashboardElementId){

	if(serviceCenterUserActionType=="ServiceCenter"){
		//Simulate dashboard item click event if any additional action is required
		if(serviceCenterDashboardElementId && serviceCenterDashboardElementId !='' && $('#'+serviceCenterDashboardElementId).length > 0){

			//Timeout is required to display spinning wheel for the dashboard button click
			setTimeout(function(){$("#"+serviceCenterDashboardElementId).trigger("click");}, 10);
		}
	}else if(serviceCenterUserActionType=="FAQServiceCenter"){
		if(serviceCenterDashboardElementId && serviceCenterDashboardElementId !='' && $('#'+serviceCenterDashboardElementId).length > 0){

			//Timeout is required to display spinning wheel for the dashboard button click		
			setTimeout(function(){$("#"+serviceCenterDashboardElementId).trigger("click");}, 10);
		}
	}
};

ns.servicecenter.criteriaCurrentSession = "";
ns.servicecenter.absoluteRange = "";


$(document).ready(function() {
	// jQuery.LINT.level = 1;
	$.debug(false);
	ns.servicecenter.setup();
});