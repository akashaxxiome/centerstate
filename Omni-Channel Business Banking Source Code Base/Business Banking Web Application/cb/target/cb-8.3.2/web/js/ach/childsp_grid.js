	$.subscribe('childspTypeGridCompleteEvents', function(event,data) {
		ns.common.resizeWidthOfGrids();
	});
	
	ns.childsp.handleGridEventsAndSetWidth = function (gridID,tabsID){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	};

	ns.childsp.formatchildspTypeActionLinks = function (cellvalue, options, rowObject) {
		//var isBusiness = "false";
		//if(rowObject["map"].BusinessID != undefined) {
		//	isBusiness = "true";
		//}
		var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.childsp.deleteChildspType(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		return del;
	};

	ns.childsp.formatchildspTypeNameLinks = function (cellvalue, options, rowObject) {
		//var name = rowObject["state"] + " " + rowObject["name"];
		var name = rowObject["name"];
		if(rowObject["map"].BusinessID != undefined) {
			name = name + "(Business)";
		}
		return name;
	}
	
	ns.childsp.deleteChildspType = function ( urlString ){
		
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteChildspTypeDialogID').html(data).dialog('open');
			}
		});
	};
	
	$.subscribe('cancelChildspTypeSuccessTopics', function(event,data) {
		$('#childspTypesGridId').trigger("reloadGrid");
		if(data.innerHTML != "") {
			ns.common.showStatus();
		} else {
			ns.common.showStatus();
			ns.common.showStatus($("#deleteTypeMessageId").val());
		}
	});
	
	$.subscribe('cancelChildspTypeCompleteTopics', function(event,data) {
		ns.common.closeDialog();
	});

	$.subscribe('cancelChildspTypeErrorTopics', function(event,data) {
		ns.common.showError();
	});
