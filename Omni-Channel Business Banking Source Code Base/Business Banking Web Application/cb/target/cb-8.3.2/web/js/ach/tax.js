ns.tax.setup = function (){

	//ns.tax.gotoTaxBatches();

	/*
	 * Control dashboard button display 
	 */
	$.subscribe('taxTemplatesSummaryLoadedSuccess', function(event,data){
		$('#details').hide();
		ns.tax.gotoTemplates();
	});
	
    $.subscribe('taxTypesSummaryLoadedSuccess', function(event,data){
    	$('#details').hide();
        ns.tax.gotoTaxType();
    });

	$.subscribe('taxSummaryLoadedSuccess', function(event,data){
		$('#details').hide();
		ns.tax.gotoTaxBatches();
	});
	
    $.subscribe('closeACHEntryImportAddenda', function(event, data) {
        $('#importEntryAddendaDialogID').dialog('close');
    });
    
	
	$.subscribe('loadSingleTaxBatch', function(event, data) {
		ns.common.selectDashboardItem("addSingleTaxBatchLink");
	});
	
	$.subscribe('loadMultipleTaxBatch', function(event, data) {
		ns.common.selectDashboardItem("addMultipleTaxBatchLink");
	});
	
	$.subscribe('loadAddTaxPayee', function(event, data) {	
		ns.common.refreshDashboard('dbPayeeSummary');
		ns.common.selectDashboardItem("addTaxPayeeLink");
	});
	
	$.subscribe('loadAddFederalPayee', function(event, data) {	
		ns.common.refreshDashboard('dbPayeeSummary');
		ns.common.selectDashboardItem("addFederalPayeeLink");
	});

    $.subscribe('beforeACHEntryImportAddendaForm', function(event, data) {
        // $.log('Form Loading error! ');
        // clear everything from the VIEW Dialog ID to not cause conflicting DIV
        // names

    });
    $.subscribe('errorACHEntryImportAddendaForm', function(event, data) {
        // $.log('Form Loading error! ');
    });
    $.subscribe('successACHEntryImportAddendaForm',
        function(event, data) {
            // submit should have happened.
            $('#importEntryAddendaDialogID').dialog('close');

            var urlString = "/cb/pages/jsp/ach/ACHEntryImportAddendaAction_executeAddendaString.action";
            $.ajax({
                url : urlString,
                contentType : "application/json;charset=utf-8",
                dataType : "json",
                async : false,
                success : function(data) {
                    $("#ACHADDENDA").val(data.addendaString);
                },
                error : function(msg) {
                }
            });
    });

	$.subscribe('successSubmitTaxBatchesForPaymentForm', function(event,data) {
		var resStatus = event.originalEvent.request.status;
	    if(resStatus != 700) //700 indicates interval validation error
        {
            $('#verifyDiv').empty();        // clear out form data from verify page
            $.ajax({
                url: "/cb/pages/jsp/ach/achmultibatchaddeditconfirm.jsp?confirm=true",
                success: function(data) {
                    $('#confirmDiv').html(data);
                }
            });
            ns.ach.gotoStep(3);
            ns.common.showStatus();
        }
    });
	
	//$.publish('TaxSummaryLoadedSuccess');
	
	/*
	 *  Load Tax form (
	 */
	$.subscribe('beforeLoadTaxForm', function(event,data) {
		//$.log('About to load form! ');
		$('#details').hide();
		$('#quicksearchcriteria').hide();

        // UPLOAD might change the names of the tabs, so change them back.
        var heading = $('#normalInputTab').html();
        var $title = $('#inputTab a');
        $title.attr('title',heading);
        $title = $('#inputTab a span');
        $title.html(heading);

        heading = $('#normalVerifyTab').html();
        $title = $('#verifyTab a');
        $title.attr('title',heading);
        $title = $('#verifyTab a span');
        $title.html(heading);

        $('#inputDiv').html("");            // before we load, clear previous contents
        $('#confirmTab').show();
	});
	
	$.subscribe('successLoadTaxForm', function(event,data) {
	});
	
	$.subscribe('loadTaxFormComplete', function(event,data) {
		//$.log('Form Loaded! ');

		//set portlet title (defined in etax page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.tax.gotoStep(1);
		$('#details').slideDown();
	
	});

	$.subscribe('errorLoadTaxForm', function(event,data) {
		//$.log('Form Loading error! ');
		
	});
	
	$.subscribe('quickSearchTaxTemplatesComplete', function(event,data) {
        ns.common.setFirstGridPage('#singleACHTemplatesGridID');//set grid page param to 1 to reload grid from first
        ns.common.setFirstGridPage('#multipleACHTemplatesGridID');//set grid page param to 1 to reload grid from first
		$.publish("reloadSelectedGridTab");
		$('#details').hide();
		$('#ACHTemplateGridTabs').portlet('expand');
	});
	
	$.subscribe('cancelAddTaxForm', function(event, data) {
		// $.log('About to cancel Form! ');
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});
	
};

	ns.tax.gotoTemplates = function (){
		//ns.common.refreshDashboard('dbTemplateSummary');
		ns.common.showSummaryOnSuccess("summary","done");
		$('#summary').show();
		/*
		$('#addSingleTemplateLink').removeAttr('style');
		$('#addMultipleTemplateLink').removeAttr('style');
		$('#addSingleTaxBatchLink').attr('style','display:none');
		$('#addMultipleTaxBatchLink').attr('style','display:none');
		$('#loadTemplateLink').attr('style','display:none');
		$('#TaxtemplatesLink').attr('style','display:none');
		$('#details').hide();
		$('#quickSearchLink').show();
		$('#quicksearchcriteria').hide();
		// switch quick search creteria board
		$('#achsearchcriteria').attr('style','display:none');
		$('#templatessearchcriteria').attr('style','display:block');
		
		$('#loadTemplateId').hide();
	    $('#addTaxPayeeLink').attr('style','display:none');
	    $('#addFederalPayeeLink').attr('style','display:none');
	
	    $('#TaxBatchesLink').removeAttr('style');
	    $('#TaxPayeesLink').removeAttr('style');
	    $('#TaxUploadLink').removeAttr('style');
	    
	    $('#inputTab').hide();
	    $('#verifyTab').hide();
	    $('#confirmTab').hide();*/
	};
	
	
		//Show the active dashboard based on the user selected summary
	ns.tax.taxDashboard = function(achUserActionType,achDashboardElementId){
	if(achUserActionType == 'TaxBatch'){
		ns.common.refreshDashboard('dbACHSummary');	
	
	}else if(achUserActionType == 'TaxBatchTemplate'){
	
		ns.common.refreshDashboard('dbTemplateSummary');
		
	}
	else if(achUserActionType == 'TaxType'){
		ns.common.refreshDashboard('dbPayeeSummary');
		
	}
	
	else{//In case of normal Transfer top menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbACHSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(achDashboardElementId && achDashboardElementId !='' && $('#'+achDashboardElementId).length > 0){
		//$("#"+transferDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+achDashboardElementId).trigger("click");}, 10);
	}
	}	;

	
	ns.tax.gotoTaxBatches = function (){
		//ns.common.refreshDashboard('dbACHSummary');
		ns.common.showSummaryOnSuccess("summary","done");
		$('#summary').show();
		/*
		$('#addSingleTemplateLink').attr('style','display:none');
		$('#addMultipleTemplateLink').attr('style','display:none');
		$('#TaxBatchesLink').attr('style','display:none');
		$('#goBackSummaryLink').attr('style','display:none');
	    $('#addTaxPayeeLink').attr('style','display:none');
	    $('#addFederalPayeeLink').attr('style','display:none');
	
		$('#addSingleTaxBatchLink').removeAttr('style');
		$('#addMultipleTaxBatchLink').removeAttr('style');
		$('#loadTemplateLink').removeAttr('style');
		$('#details').hide();	
		$('#quickSearchLink').show();
		$('#quicksearchcriteria').hide();
		// switch quick search creteria board
		$('#achsearchcriteria').attr('style','display:block');
		$('#templatessearchcriteria').attr('style','display:none');
		
	
	    $('#TaxtemplatesLink').removeAttr('style');
	    $('#TaxPayeesLink').removeAttr('style');
	    $('#TaxUploadLink').removeAttr('style');
	    
	    $('#inputTab').show();
	    $('#verifyTab').show();
	    $('#confirmTab').show();*/
	};
	
	ns.tax.gotoTaxType = function (){
		ns.common.showSummaryOnSuccess("summary","done");
		  $('#summary').show();
		/*
		$('#addSingleTemplateLink').attr('style','display:none');
		$('#addMultipleTemplateLink').attr('style','display:none');
		$('#goBackSummaryLink').attr('style','display:none');
		$('#addSingleTaxBatchLink').attr('style','display:none');
		$('#addMultipleTaxBatchLink').attr('style','display:none');
		$('#loadTemplateLink').attr('style','display:none');
		$('#details').hide();
		$('#quickSearchLink').hide();
		$('#quicksearchcriteria').hide();
		// switch quick search creteria board
		$('#achsearchcriteria').attr('style','display:block');
		$('#templatessearchcriteria').attr('style','display:none');
				
		$('#loadTemplateId').hide();
	
	    $('#TaxtemplatesLink').removeAttr('style');
	    $('#TaxBatchesLink').removeAttr('style');
	    $('#TaxUploadLink').removeAttr('style');
	    $('#addTaxPayeeLink').removeAttr('style');
	    $('#addFederalPayeeLink').removeAttr('style');
	    $('#TaxPayeesLink').attr('style','display:none');
	    $('#inputDiv').show();
	    $('#achAddEditEntrySection').empty();
	    
	    $('#inputTab').hide();
	    $('#verifyTab').hide();
	    $('#confirmTab').hide();
	    */
	};
	
	ns.tax.gotoStep = function (stepnum){
	//    alert("GOTO Step "+stepnum);
		var tabindex = stepnum - 1;
		$('#TaxTransactionWizardTabs').tabs( 'enable', tabindex );
		$('#TaxTransactionWizardTabs').tabs( 'option','active', tabindex );
		$('#TaxTransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
		$('#TaxTransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );
	//	$('#TransactionWizardTabs').tabs( "option", "disabled", [0,2] );
		
	};
	
	/**
	 * Close Details island and expand Summary island
	 */
	ns.tax.closeTaxTabs = function (){
		ns.ach.expandPortlet('summary');
		$('#details').slideUp();
	};

    ns.tax.statusMessage = null;        // used in tax_grid.js to show status message

	ns.tax.fedTaxAdd = function() {
		var urlString = '/cb/pages/jsp/ach/addChildTaxTypeAction.action';
		$.ajax({
			   url: urlString,
			   type: "POST",
			   data: $("#FedTaxId").serialize(),
			   success: function(data){
                   ns.tax.statusMessage = data;
                   ns.common.showSummaryOnSuccess('summary','done');
                   ns.tax.closeTaxTabs();               
		   }
		});
	};
	
	ns.tax.stateTaxAdd = function() {
		var urlString = '/cb/pages/jsp/ach/addChildTaxTypeAction.action';
		$.ajax({
			   url: urlString,
			   type: "POST",
			   data: $("#StateTaxId").serialize(),
			   success: function(data){
                    ns.tax.statusMessage = data;
                    ns.common.showSummaryOnSuccess('summary','done');
                    ns.tax.closeTaxTabs();
             
		   }
		});
	};
	
	$(document).ready(function(){
	//	jQuery.LINT.level = 1;
		$.debug(false);
		ns.tax.setup();
	});

