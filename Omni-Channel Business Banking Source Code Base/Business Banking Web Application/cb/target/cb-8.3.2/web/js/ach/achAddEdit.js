/**
 * JS File especially kept to have all javascript functions required on Add and
 * Edit JSP page.
 */
ns.ach.changeACHClassCode = function() {
	// we need to serialize the data so it doesn't get lost before changing
	// class code
	var selectedSECCode  = $("#standardEntryClassCodeId").val();
	
	var strutsActionName = $("#strutsActionName").val()
			+ '_changeACHClassCode.action';
	$.ajax({
		type : "POST",
		url : "/cb/pages/jsp/ach/" + strutsActionName,
		data : $("#AddBatchNew").serialize(),
		success : function(data) {
			$('#addEditVariableSection').html(data);
			// Clear payment Type
			changePaymentType('true');
			ns.ach.modifyFrequency($("#fromTemplate").val(), $("#isEdit").val());
			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}	
		}
	});
};

function enableDisableImportAddEntryBtns(selectedValue){
	if (selectedValue == '' ||selectedValue == '-1') {
		//Disable controls
		$("#IMPORTENTRIES").button({ disabled: true });
		$("#addEntryButtonId").button({ disabled: true });
	} else {
		//Enable only one button.
		//Disable controls
		$("#IMPORTENTRIES").button({ disabled: false });
		$("#addEntryButtonId").button({ disabled: false });
	}	
}


ns.ach.changeACHCompany = function(companyID) {
	var achType = $("#achType").val();
	var strutsActionName = $("#strutsActionName").val()
			+ '_changeACHCompany.action';
	
	$.ajax({
		type : "POST",
		url : "/cb/pages/jsp/ach/" + strutsActionName,
		data : $("#AddBatchNew").serialize(),
		success : function(data) {
			$('#inputDiv').html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
			
			//Disable controls always on change for modules other than Tax.
			if(achType != 'TaxPayment'){
				$("#IMPORTENTRIES").button({ disabled: true });
				$("#addEntryButtonId").button({ disabled: true }); 
			} else {
				//For Tax module disable controls only when company ID is not selected.
				if(companyID == "" || companyID == "-1"){
					$("#IMPORTENTRIES").button({ disabled: true });
					$("#addEntryButtonId").button({ disabled: true });
					$("#setACHBatchTypeId").button({ disabled: true });
				} else {
					$("#IMPORTENTRIES").button({ disabled: false });
					$("#addEntryButtonId").button({ disabled: false });
					$("#setACHBatchTypeId").button({ disabled: false });
				}
			}
		}
	});
};

// Changed.
function addACHEntry(achType) {
	var strutsActionName = $("#strutsActionName").val();
	var addEditEntryUrl = "";

	if (achType != "ACHBatch")
		addEditEntryUrl = '/cb/pages/jsp/ach/' + strutsActionName
				+ '_beforeACHEntryOperation.action';
	else
		addEditEntryUrl = "/cb/pages/jsp/ach/addACHEntryAction_init.action";

	var defaultTaxFormID = $("#defaultTaxFormID");
	if (defaultTaxFormID != 'undefined' && defaultTaxFormID != undefined) {
		if ($("#defaultTaxFormID").val() != undefined) {
			addEditEntryUrl = addEditEntryUrl + "?defaultTaxFormID="
					+ $("#defaultTaxFormID").val();
		}
	}
	ns.ach.addEditEntry('AddBatchNew', addEditEntryUrl);
}

function setAllAmounts() {
	var urlStr = "/cb/pages/jsp/ach/setallamounts.jsp";
	$.ajax({
		type : "GET",
		url : urlStr,
		success : function(data) {
			$('#setAllAmountDialogID').html(data).dialog("open");
			$('#setAllAmountDialogID').css('height','auto');
		}
	});
}


/* Change ACH Batch type to 'ACH' for Child support and Tax batch */
function openACHBatchTypeDialog() {
	$.get("/cb/pages/jsp/ach/achchangebatchtype.jsp", function(data) {
		$('#setACHBatchTypeDialogID').html(data).dialog('open');
	});
};

function setACHBatchType() {
	var strutsActionName = $("#strutsActionName").val();
	var urlString = "/cb/pages/jsp/ach/" + strutsActionName + "_changeBatchType.action";
	$.ajax({
		url : urlString,
		type : "POST",
		data : $("#AddBatchNew").serialize(),
		success : function(data) {
			$('#inputDiv').html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
	
};

function changeChildspType() {
    // in order to ADD an entry, the ACH Company ID needs to be set and for Child Support,
    // the Default Tax Form ID also needs to be set.
	var companyID = $("#AddEditACHBatchCompanyID").val();
	if($("#defaultTaxFormID").val() == '' || $("#defaultTaxFormID").val() == '-1' || companyID == "" || companyID == "-1"){
		$("#setACHBatchTypeId").button({ disabled: true });
		$("#addEntryButtonId").button({ disabled: true });
	} else {
		$("#setACHBatchTypeId").button({ disabled: false });
		$("#addEntryButtonId").button({ disabled: false });
	}

	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		$("#inputDiv").setFocus();
	}
}

function changePaymentType(clearValue) {
	if (clearValue == 'true') {
		if ($("#coEntryDesc").val() == 'HCCLAIMPMT') {
			$("#coEntryDesc").val("");
		}
	}

	if ($("#paymentTypeID").length > 0) {
		if ($("#paymentTypeID").val() == '-1'
				|| (typeof $("#paymentTypeID").val() == 'undefined')) {
			$("#coEntryDesc").removeClass("ui-state-disabled");
            if (clearValue == 'true')
                $("#coEntryDesc").val("");
			if (document.getElementById("coEntryDesc") != null)
				document.getElementById("coEntryDesc").disabled = false;
			if (document.getElementById("coEntryDescHidden") != null)
				document.getElementById("coEntryDescHidden").disabled = true;
		} else if ($("#paymentTypeID").val() == 'Healthcare') {
			$("#coEntryDesc").addClass("ui-state-disabled");
			$("#coEntryDesc").val("HCCLAIMPMT");
			if (document.getElementById("coEntryDesc") != null)
				document.getElementById("coEntryDesc").disabled = true;
			if (document.getElementById("coEntryDescHidden") != null)
				document.getElementById("coEntryDescHidden").disabled = false;
			$("#coEntryDescHidden").val("HCCLAIMPMT");
		}
	}
}

function tempHistoryDisplay(show) {
	if (show) {
		document.getElementById("tempHistory").style.display = "inline";
		document.getElementById("viewTempHistory").style.display = "none";
		document.getElementById("hideTempHistory").style.display = "inline";
		$("#hideTempHistory").focus();
	} else {
		document.getElementById("tempHistory").style.display = "none";
		document.getElementById("viewTempHistory").style.display = "inline";
		document.getElementById("hideTempHistory").style.display = "none";
		$("#viewTempHistory").focus();
	}
}

function toggleRecurring() {
	if (document.AddBatchForm.OpenEnded != null) {
		if (document.AddBatchForm.OpenEnded[1].checked == true) {
			if (document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'])
				document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].disabled = false;
		} else {
			if (document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'])
				document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].disabled = true;
		}
	}
}

function holdAll(holdCheckBox) {
	var flag;
	if (holdCheckBox.checked) {
		flag = "true";
	} else {
		flag = "false";
	}
	var type = $("#achType").val();
	var action = "/cb/pages/jsp/ach/holdAllEntries.action?holdAll=" + flag + "&achType="+type;
	

	$.ajax({
		type : "POST",
		url : action,
		data : $("#AddBatchNew").serialize(),
		// data: {holdAllEntries: flag,CSRF_TOKEN:
		// ns.home.getSessionTokenVarForGlobalMessage},
		async : false,
		success : function(data) {
			$('#achEntryDetails').html(data);
		}
			
		});
	return false;
}

function prepareFooterDisplayData(list,updatedValue){
	for(var i=0;i<list.length;i++){
		if(list[i].className!= undefined && list[i].className!='' && list[i].className.indexOf('updatedValue')>-1 ){
			if(list[i].nodeName == 'INPUT'){
				list[i].value=updatedValue;
			}else{
				$(list[i]).text(updatedValue);
			}
		}
	}
	return list;
}
function changeHold(holdCheckBox, ActiveID) {
	var crdr = document.getElementById("crdr" + ActiveID).value;
	var frm = document.AddBatchForm;
	var tag = null;
	var credit =null;
	if (crdr.indexOf("Credit") >= 0) {
		tag = frm['TotalNumberCredits'];
		credit = true;
	} else {
		tag = frm['TotalNumberDebits'];
		credit = false;
	}
	var value = tag.value;
	var total = parseInt(value);
	var amtStr = "";
	if (holdCheckBox.checked == true) {
		document.getElementById("active" + ActiveID).value = "false";
		total -= 1; // subtract out previous count
		amtStr = "" + total;
		tag.value = amtStr;
	} else {
		document.getElementById("active" + ActiveID).value = "true";
		total += 1; // add in previous count
		amtStr = "" + total;
		tag.value = amtStr;
	}
	// The name achPayee.nameSecure & amount are not random names they should be same as used in ach_grid.js's "prepareFooterOnLoad" method  
	if(credit){
		// below condition is added to handle UI case so that the footer looks align
		// for XCK and other ACH type where Participant Name doesn't suit alignment we use itemResearchNumber
		var achPayeeAccountNumberSecure = $("#achEntriesGridId").jqGrid('getGridParam','colNames').indexOf('Participant Name');
		var achPayeeAccountNumberSecure2 = $("#achEntriesGridId").jqGrid('getGridParam','colNames').indexOf('Receiver Name');
		if(achPayeeAccountNumberSecure!='-1' || achPayeeAccountNumberSecure2 !='-1')
		{
			var noOfCredits = $.parseHTML($('#achEntriesGridId').jqGrid('footerData','get')['achPayee.nameSecure']);
			$('#achEntriesGridId').jqGrid('footerData','set',{'achPayee.nameSecure': prepareFooterDisplayData(noOfCredits,amtStr)},false);
		}else{
			var noOfCredits = $.parseHTML($('#achEntriesGridId').jqGrid('footerData','get')['itemResearchNumber']);
			$('#achEntriesGridId').jqGrid('footerData','set',{'itemResearchNumber': prepareFooterDisplayData(noOfCredits,amtStr)},false);
		}
	}else{
		var amount = $.parseHTML($('#achEntriesGridId').jqGrid('footerData','get')['amount']);
		$('#achEntriesGridId').jqGrid('footerData','set',{'amount': prepareFooterDisplayData(amount,amtStr)},false);
	}
	ns.ach.totalThisAmount(ActiveID);
}

ns.ach.modifyFrequency = function(fromTemplate, isEdit) {

	if(typeof isEdit === "undefined")
	isEdit=false;
	
	if (fromTemplate==='false')
	fromTemplate=false;
	
	if (fromTemplate==='true')
	fromTemplate=true;

	if (!fromTemplate) {
		if (!isEdit) {

			if (document.forms['AddBatchForm']['AddEditACHBatch.frequency']) {//check if form has frequency parameter
				if (document.forms['AddBatchForm']['AddEditACHBatch.frequency'].selectedIndex == 0) {

					if (document.AddBatchForm.OpenEnded != null)
						document.AddBatchForm.OpenEnded[0].disabled = true;
						
					if (document.AddBatchForm.OpenEnded != null)
						document.AddBatchForm.OpenEnded[1].disabled = true;
						
					if (document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'])
					{
						document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].disabled = true;
						document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].value = "1";
					}
					
				} else {
					if (document.AddBatchForm.OpenEnded != null)
					{
						document.AddBatchForm.OpenEnded[0].disabled = false;
						document.AddBatchForm.OpenEnded[1].disabled = false;
					}
						
					if (document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'])
						document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].disabled = false;

					if (document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].value == "0"
							|| document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].value == "1")
					{
						document.forms['AddBatchForm']['AddEditACHBatch.NumberPayments'].value = "2";
					}
					toggleRecurring();
				}
			}
		} else {
			if (document.forms['AddBatchForm']['AddEditACHBatch.frequency']) {//check if form has frequency parameter
				if (document.AddBatchForm.OpenEnded != null)
					document.AddBatchForm.OpenEnded[0].disabled = false;
				if (document.AddBatchForm.OpenEnded != null)
					document.AddBatchForm.OpenEnded[1].disabled = false;
				if (document.AddBatchForm.numberPayments != null)
					document.AddBatchForm.numberPayments.disabled = false;
				toggleRecurring();
			}
		}
	}
};

ns.ach.totalThisAmount = function(ID) {
	var creditAmtTotal = 0.0;
	var debitAmtTotal = 0.0;
	var frm = document.AddBatchForm;
	var tag = null;
	var newAmount = document.getElementById("amount" + ID).value;
	while (newAmount.indexOf(',') > -1) {
		newAmount = newAmount.substr(0, newAmount.indexOf(','))
				+ newAmount.substr(newAmount.indexOf(',') + 1);
	}
	var crdr = document.getElementById("crdr" + ID).value;
	var prevAmount = crdr.substr(0, crdr.indexOf(' '));
	while (prevAmount.indexOf(',') > -1) {
		prevAmount = prevAmount.substr(0, prevAmount.indexOf(','))
				+ prevAmount.substr(prevAmount.indexOf(',') + 1);
	}
	if (crdr.indexOf("Credit") >= 0) {
		crdr = "Credit";
		tag = frm['AddEditACHBatch.TotalCreditAmount'];
	} else {
		crdr = "Debit";
		tag = frm['AddEditACHBatch.TotalDebitAmount'];
	}

	// task validation will catch an invalid amount
	// if (!checkACHAmount(
	// frm["AddEditACHBatch.CurrentEntry="+ID+"&AddEditACHBatch.EntryAmount"]))
	// {
	// newAmount = "0.00";
	// }
	if (document.getElementById("active" + ID).value == "false") {
		newAmount = "0.00"; // if inactive, set amount to "0.00" so it
		// doesn't get added in.
	}
	document.getElementById("crdr" + ID).value = newAmount + ' ' + crdr;
	var amt = parseFloat(prevAmount);
	if (isNaN(amt))
		amt = parseFloat("0.00");
	var newamt = parseFloat(newAmount);
	if (isNaN(newamt))
		newamt = parseFloat("0.00");

	var value = tag.value;
	while (value.indexOf(',') > -1) {
		value = value.substr(0, value.indexOf(','))
				+ value.substr(value.indexOf(',') + 1);
	}
	var amtTotal = parseFloat(value);
	amtTotal -= amt; // subtract out previous amount
	amtTotal += newamt; // add in new amount

	amtTotal = Math.round(amtTotal * Math.pow(10, 2)) / Math.pow(10, 2);
	var amtStr;
	if (amtTotal < 0) {
		amtStr = "0.00";
	} else {
		amtStr = "" + amtTotal;
	}
	// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator
	// =
	// ","
	if (amtStr.indexOf('.') == -1) {
		amtStr = amtStr + ".0"
	} // Add .0 if a
	// decimal point
	// doesn't exist
	temp = (amtStr.length - amtStr.indexOf('.')); // Find out if the # ends in
	// a
	// tenth (ie 145.5)
	if (temp <= 2) {
		amtStr = amtStr + "0";
	}
	; // if it ends in a tenth, add an
	// extra zero to the string
	// add in commas again
	var x = amtStr.indexOf('.') - 3;
	while (x > 0) {
		amtStr = amtStr.substr(0, x) + ',' + amtStr.substr(x, amtStr.length);
		x = amtStr.indexOf(',') - 3;
	}
	tag.value = amtStr;
	// The name achPayee.userAccountNumber & amountIsDebit are not random names they should be same as used in ach_grid.js's "prepareFooterOnLoad" method
	if(crdr=='Credit'){
		var userAccountNumber = $.parseHTML($('#achEntriesGridId').jqGrid('footerData','get')['achPayee.accountNumberSecure']);
		$('#achEntriesGridId').jqGrid('footerData','set',{'achPayee.accountNumberSecure': prepareFooterDisplayData(userAccountNumber,amtStr)},false);
	}
	else{
		var amountIsDebit = $.parseHTML($('#achEntriesGridId').jqGrid('footerData','get')['amountIsDebit']);
		$('#achEntriesGridId').jqGrid('footerData','set',{'amountIsDebit': prepareFooterDisplayData(amountIsDebit,amtStr)},false);
	}
};


$(document).ready(function(){
	ns.ach.modifyFrequency($("#fromTemplate").val(), $("#isEdit").val());
    $("#AddEditACHBatchCompanyID").selectmenu({width: 370});
	$("#standardEntryClassCodeId").selectmenu({width: 370});
	$("#defaultTaxFormID").selectmenu({width: 370});
	ns.common.addHoverEffect("setACHBatchTypeId");
	ns.common.addHoverEffect("setAllMountsButtonId");
	ns.common.addHoverEffect("importAmmounts");
	ns.common.addHoverEffect("addEntryButtonId");
	ns.common.addHoverEffect("IMPORTENTRIES");

    ns.ach.setInputTabText('#normalInputTab');
    ns.ach.setVerifyTabText('#normalVerifyTab');
    ns.ach.setConfirmTabText('#normalConfirmTab');
    ns.ach.showInputDivSteps();
});
