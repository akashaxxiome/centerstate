ns.childsp.setup = function (){

	//ns.childsp.gotoChildspBatches();

	/*
	 * Control dashboard button display 
	 */
	$.subscribe('childspTemplatesSummaryLoadedSuccess', function(event,data){
		$('#details').hide();
		ns.childsp.gotoTemplates();
	});
	
    $.subscribe('childspTypesSummaryLoadedSuccess', function(event,data){
    	$('#details').hide();
        ns.childsp.gotoChildspType();
    });

	$.subscribe('childspSummaryLoadedSuccess', function(event,data){
		$('#details').hide();
		ns.childsp.gotoChildspBatches();
	});
	
	$.subscribe('successSubmitChildBatchesForPaymentForm', function(event,data) {
		var resStatus = event.originalEvent.request.status;
	    if(resStatus != 700) //700 indicates interval validation error
        {
            $('#verifyDiv').empty();        // clear out form data from verify page
            $.ajax({
                url: "/cb/pages/jsp/ach/achmultibatchaddeditconfirm.jsp?confirm=true",
                success: function(data) {
                    $('#confirmDiv').html(data);
                }
            });
            ns.ach.gotoStep(3);
            ns.common.showStatus();
        }
    });
	
    $.subscribe('closeACHEntryImportAddenda', function(event, data) {
        $('#importEntryAddendaDialogID').dialog('close');
    });

    $.subscribe('beforeACHEntryImportAddendaForm', function(event, data) {
        // $.log('Form Loading error! ');
        // clear everything from the VIEW Dialog ID to not cause conflicting DIV
        // names

    });
    $.subscribe('errorACHEntryImportAddendaForm', function(event, data) {
        // $.log('Form Loading error! ');
    });
    $.subscribe(
                    'successACHEntryImportAddendaForm',
                    function(event, data) {
                        // submit should have happened.
                        $('#importEntryAddendaDialogID').dialog('close');

                        var urlString = "/cb/pages/jsp/ach/ACHEntryImportAddendaAction_executeAddendaString.action";
                        $.ajax({
                            url : urlString,
                            contentType : "application/json;charset=utf-8",
                            dataType : "json",
                            async : false,
                            success : function(data) {
                                $("#ACHADDENDA").val(data.addendaString);
                            },
                            error : function(msg) {
                            }
                        });
                    });
    
	$.subscribe('cancelAddChildSupportTypeForm', function(event, data) {
		// $.log('About to cancel Form! ');
		ns.common.refreshDashboard('showdbPayeeSummary',true);
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});
    
    
    };
	
		//Show the active dashboard based on the user selected summary
ns.childsp.showChildSPDashboard = function(achUserActionType,achDashboardElementId){
	if(achUserActionType == 'ChildSPBatch'){
		ns.common.refreshDashboard('dbACHSummary');	
	
	}else if(achUserActionType == 'ChildSPBatchTemplate'){
	
		ns.common.refreshDashboard('dbTemplateSummary');
		
	}
	else if(achUserActionType == 'ChildSPPayee'){
		ns.common.refreshDashboard('dbPayeeSummary');
		
	}
	
	else{//In case of normal Transfer top menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbACHSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(achDashboardElementId && achDashboardElementId !='' && $('#'+achDashboardElementId).length > 0){
		//$("#"+transferDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+achDashboardElementId).trigger("click");}, 10);
	}
};

	/*
	 * Verify transfer form
	 */
	$.subscribe('beforeVerifyChildspForm', function(event,data) {
		//$.log('About to verify form! ');
		removeValidationErrors();
		
			
	});
	
	$.subscribe('clickVerifyChildspForm', function(event,data) {
		//$.log('About to verify form! ');
		if($("#DefaultTaxFormID").val()== "") {
			$("#defaultTaxFormIDError").html("Child Support Type must be specified.");
		}else {
			$("#defaultTaxFormIDError").html("");
		} 
			
	});
	
	$.subscribe('quickSearchChildspTemplatesComplete', function(event,data) {
        ns.common.setFirstGridPage('#singleACHTemplatesGridID');//set grid page param to 1 to reload grid from first
        ns.common.setFirstGridPage('#multipleACHTemplatesGridID');//set grid page param to 1 to reload grid from first
		$.publish("reloadSelectedGridTab");
		$('#details').hide();
		$('#ACHTemplateGridTabs').portlet('expand');	
	});
	
	$.subscribe('verifyChildspFormComplete', function(event,data) {
		//$.log('Form Verified! ');
		ns.childsp.gotoStep(2);
		
	});
	
	$.subscribe('recheckChildspFormComplete', function(event,data) {
		ns.childsp.sucessChildspEntry();		
	});

	$.subscribe('errorVerifyChildspForm', function(event,data) {
		//$.log('Form Verifying error! ');
		
	});
	
	$.subscribe('successVerifyChildspForm', function(event,data) {
		//$.log('Form Verifying success! ');
		
	});
	
    
    $.subscribe('clickAddChildspTempForm', function(event,data) {
    	$("#AddBatchNew").attr("action","/cb/pages/jsp/childsp/editChildspBatchAction_addEditTemplate.action");
    });
    
    $.subscribe('completeAddChildspTempForm', function(event,data) {
    	ns.childsp.closeChildspTabs();
        ns.common.hideStatus();
    });
    
    $.subscribe('successAddChildspTempForm', function(event,data) {
    	ns.ach.reloadACHTemplateSummaryGrid();
    });
	
	
	/*
	 * Send Childsp form
	 */
	$.subscribe('beforeSendChildspForm', function(event,data) {
		//$.log('About to send Childsp Batch! ');
			
	});
	
	$.subscribe('sendChildspFormComplete', function(event,data) {
		//$.log('sending Childsp Batch complete! ');
			
	});

	$.subscribe('sendChildspFormSuccess', function(event,data) {
		//$.log('sending Childsp succeeded! ');
			
		ns.childsp.gotoStep(3);
		ns.common.showStatus($('#childspBatchResultMessage').html());
		ns.ach.reloadACHSummaryGrid();

	});

	$.subscribe('errorSendChildspForm', function(event,data) {
		//$.log('Childsp error! ');
		
	});
	
	$.subscribe('beforeChildspTypeForm', function(event,data) {
		//$.log('Childsp error! ');
	});
	$.subscribe('successChildspTypeForm', function(event,data) {
		$('#childspTypesGridId').trigger("reloadGrid");
	});
	
	$.subscribe('completeChildspTypeForm', function(event,data) {
		ns.childsp.closeChildspTabs();
		ns.common.hideStatus();
	});


	$.subscribe('quickSearchChildspComplete', function(event,data) {
		$('#pendingApprovalChildspId').trigger("reloadGrid");
		$('#pendingChildspGridId').trigger("reloadGrid");
		$('#completedChildspGridId').trigger("reloadGrid");
	});
	
	
    $.subscribe('addEntryChildspFormComplete', function(event,data) {
        $.ajax({
            url: "/cb/pages/jsp/childsp/childspbatchaddeditentry.jsp",
            data: "SubmitEntry=true",
            success: function(data) {
        		$('#addEditEntryDialogID').html(data);
            }
        });
	});

	ns.childsp.sucessChildspEntry = function() {
	    $.ajax({
	        url: "/cb/pages/jsp/childsp/childspbatchaddedit.jsp?Initialize=true",
	        success: function(data) {
	    		if($("#templateContent").attr("style") == "display: none;") {
	    			$('#inputDiv').html(data);
	    		} else {
	    			$('#templateContent').html(data);
	    		}
	        }
	    });
	};
	
	ns.childsp.stateTaxAdd = function() {
		var urlString = '/cb/pages/jsp/ach/addChildTaxTypeAction.action';
		$.ajax({
			   type: "POST",
			   url: urlString,
			   data: $("#StateTaxId").serialize(),
			   async: false,
			   success: function(data){
		   		    ns.tax.statusMessage = data;
                    ns.common.showSummaryOnSuccess('summary','done');
					ns.childsp.closeChildspTabs();
		   }
		});
	};

	ns.childsp.gotoTemplates = function (){
		ns.common.showSummaryOnSuccess("summary","done");
		$('#summary').show();
/*		$('#addSingleTemplateLink').removeAttr('style');
		$('#addMultipleTemplateLink').removeAttr('style');
		$('#addSingleChildspBatchLink').attr('style','display:none');
		$('#addMultipleChildSupportBatchLink').attr('style','display:none');
		$('#loadTemplateLink').attr('style','display:none');
		$('#ChildsptemplatesLink').attr('style','display:none');
		$('#details').hide();
		
		$('#quickSearchLink').show();
		$('#quicksearchcriteria').hide();
		// switch quick search creteria board
		$('#achsearchcriteria').attr('style','display:none');
		$('#templatessearchcriteria').attr('style','display:block');
		
		$('#loadTemplateId').hide();
	    $('#addChildspPayeeLink').attr('style','display:none');
	
	    $('#ChildspBatchesLink').removeAttr('style');
	    $('#ChildspPayeesLink').removeAttr('style');
	    $('#ChildspUploadLink').removeAttr('style');
	    
	    $('#inputTab').hide();
	    $('#verifyTab').hide();
	    $('#confirmTab').hide();*/
	};
	
	ns.childsp.gotoChildspBatches = function (){
		//ns.common.refreshDashboard('dbACHSummary');
		ns.common.showSummaryOnSuccess("summary","done");
		$('#summary').show();
		/*
		$('#addSingleTemplateLink').attr('style','display:none');
		$('#addMultipleTemplateLink').attr('style','display:none');
		$('#ChildspBatchesLink').attr('style','display:none');
		$('#goBackSummaryLink').attr('style','display:none');
	    $('#addChildspPayeeLink').attr('style','display:none');
	    
	
		$('#addSingleChildspBatchLink').removeAttr('style');
		$('#addMultipleChildSupportBatchLink').removeAttr('style');
		$('#loadTemplateLink').removeAttr('style');
		$('#details').hide();	

		$('#quickSearchLink').show();
		$('#quicksearchcriteria').hide();
		// switch quick search creteria board
		$('#achsearchcriteria').attr('style','display:block');
		$('#templatessearchcriteria').attr('style','display:none');
		
	
	    $('#ChildsptemplatesLink').removeAttr('style');
	    $('#ChildspPayeesLink').removeAttr('style');
	    $('#ChildspUploadLink').removeAttr('style');
	    
	    $('#inputTab').show();
	    $('#verifyTab').show();
	    $('#confirmTab').show();
	    */
	};
	
	ns.childsp.gotoChildspType = function (){
		ns.common.refreshDashboard('dbPayeeSummary');
			$('#summary').show();
		
/*		$('#addSingleTemplateLink').attr('style','display:none');
		$('#addMultipleTemplateLink').attr('style','display:none');
		$('#goBackSummaryLink').attr('style','display:none');
		$('#addSingleChildspBatchLink').attr('style','display:none');
		$('#addMultipleChildSupportBatchLink').attr('style','display:none');
		$('#loadTemplateLink').attr('style','display:none');
		$('#details').hide();
		
		$('#quickSearchLink').hide();
		$('#quicksearchcriteria').hide();
		
		$('#loadTemplateId').hide();
	
	    $('#ChildsptemplatesLink').removeAttr('style');
	    $('#ChildspBatchesLink').removeAttr('style');
	    $('#ChildspUploadLink').removeAttr('style');
	    $('#addChildspPayeeLink').removeAttr('style');
	    $('#ChildspPayeesLink').attr('style','display:none');
	    
	    $('#inputTab').hide();
	    $('#verifyTab').hide();
	    $('#confirmTab').hide();*/
	};
	
	ns.childsp.gotoStep = function (stepnum){
	//    alert("GOTO Step "+stepnum);
		var tabindex = stepnum - 1;
		$('#ChildspTransactionWizardTabs').tabs( 'enable', tabindex );
		$('#ChildspTransactionWizardTabs').tabs( 'option','active', tabindex );
		$('#ChildspTransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
		$('#ChildspTransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );
	//	$('#TransactionWizardTabs').tabs( "option", "disabled", [0,2] );
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#details").setInitialFocus();
		}
	};
	
	/**
	 * Close Details island and expand Summary island
	 */
	ns.childsp.closeChildspTabs = function (){
		ns.ach.expandPortlet('summary');
		$('#details').slideUp();
	};
	
	/**
	 * Revert Child Support Entry.
	 */
	ns.childsp.revertChildSupportEntry = function() {
		var urlString = '/cb/pages/jsp/ach/achbatchtaxpayentry.jsp';
		$.ajax({
			   type: "POST",
			   url: urlString,
			   data: $("#TaxPayFormId").serialize(),
			   async: false,
			   success: function(data){
		   			$("#addEditACHEntryDialogID").html(data);
		   }
		});
	};
	
	$.subscribe('loadSingleChildspBatch', function(event, data) {
		$("#dbACHSummary a span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
		$("#addSingleChildspBatchLink span").addClass("dashboardSelectedItem");
	});
	
	$.subscribe('loadMultipleChildspBatch', function(event, data) {
		$("#dbACHSummary a span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
		$("#addMultipleChildSupportBatchLink span").addClass("dashboardSelectedItem");
	});
	
	$.subscribe('beforeAddChildspPayee', function(event,data) {
		ns.common.refreshDashboard('dbPayeeSummary');
		ns.common.selectDashboardItem("addChildspPayeeLink");
	});
	
	$(document).ready(function(){
	//	jQuery.LINT.level = 1;
		$.debug(false);
		ns.childsp.setup();
	});
	
