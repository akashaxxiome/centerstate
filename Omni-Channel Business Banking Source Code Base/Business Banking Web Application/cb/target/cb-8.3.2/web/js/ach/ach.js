ns.ach.setup = function() {

	ns.ach.currentTemplateSearchCriteria = "templatessearchcriteria";
	//ns.ach.gotoACHBatches();

	/*
	 * Control dashboard button display
	 */
	$.subscribe('ACHtemplatesSummaryLoadedSuccess', function(event, data) {
		$('#details').hide();
		ns.ach.gotoTemplates();
	});

	$.subscribe('payeesSummaryLoadedSuccess', function(event, data) {
		$('#details').hide();
		ns.ach.gotoACHPayees();
	});

	$.subscribe('ACHSummaryLoadedSuccess', function(event, data) {
		$('#details').hide();
		ns.ach.gotoACHBatches();
	});
	
	$.subscribe('ACHGridTabsShow', function(event, data) {
		$('#ACHGridTabs').show();
	});
	
	$.subscribe('ACHTemplateGridTabsShow', function(event, data) {
		$('#ACHTemplateGridTabs').show();
	});
	
	
	

	/*
	 * Listens to the Done button click event on report for Import ACH Entries
	 * and clicks on the Cancel button on Import Entries UI form.
	 */
	$.subscribe('ach.topics.closeImportACHEntries', function(event, data) {
		/* Click on Cancel button */
		$('#cancelFileImportACHTypeID').click();
	});
	
	ns.ach.showTemplateSearchCriteria = function (){
		$("#"+ns.ach.currentTemplateSearchCriteria).slideToggle();
	};
	
	ns.ach.setCurrentTemplateSearchCriteria = function (currentTemplateSearchCriteria,lastTemplateSearchCriteria){
		ns.ach.currentTemplateSearchCriteria = currentTemplateSearchCriteria;
		$("#"+currentTemplateSearchCriteria).hide();
		$("#"+lastTemplateSearchCriteria).hide();
	};

	// $.publish('ACHSummaryLoadedSuccess');

	/*
	 * Load ACH form (
	 */
	$.subscribe('beforeLoadACHForm', function(event, data) {
		// $.log('About to load form! ');
		$('#details').slideUp();
		$('#quicksearchcriteria').hide();
		$('#fileupload').hide();
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
		$('#addCustomMappingsLink').hide();
		$('#loadTemplateId').hide();
		$('#summary').show();
		$("#detailsPortlet").show();
		if($('#achCalendar').length > 0){
			$('#summary').empty();
		}
	});
	
	$.subscribe('addACHPayeeLoadForm', function(event, data) {
		ns.common.refreshDashboard('dbPayeeSummary');
		ns.common.selectDashboardItem("addACHPayeeLink");
	});
	
	$.subscribe('loadSingleACHBatch', function(event, data) {
		ns.common.selectDashboardItem("addSingleACHBatchLink");
		
	});
	
	$.subscribe('loadMultipleACHBatch', function(event, data) {
		ns.common.selectDashboardItem("addMultipleACHBatchLink");
		
	});
	
	$.subscribe('loadACHSingleBatchTemplate', function(event, data) {
		ns.common.refreshDashboard('dbTemplateSummary');
		ns.common.selectDashboardItem("addSingleTemplateLink");
		
	});
	$.subscribe('loadACHBatchTemplate', function(event, data) {
		ns.common.refreshDashboard('dbTemplateSummary');
		ns.common.selectDashboardItem("addMultipleTemplateLink");
		
	});
	$.subscribe('loadACHFormComplete', function(event, data) {
		// $.log('Form Loaded! ');
		ns.ach.foldPortlet('summary');

		// set portlet title (defined in each page loaded to inputDiv by Id
		// "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		$('#achAddEditEntrySection').hide();
		$("#ACHTransactionWizardTabs").show();
		$('#inputDiv').show();
		
		ns.ach.gotoStep(1);
		$('#details').slideDown();

		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
			$("#ACHTransactionWizardTabs").setFocusOnTab();
		}
	});
	
	$.subscribe('hideWizardFormOnComplete', function(event, data) {
		$('#detailsPortletWizardStatusId').hide();
	});
	
	$.subscribe('showWizardFormOnComplete', function(event, data) {
		$('#detailsPortletWizardStatusId').show();
	});
	
	$.subscribe('loadACHFormCompletePayee', function(event, data) {
		// $.log('Form Loaded! ');
		ns.ach.foldPortlet('summary');

		// set portlet title (defined in each page loaded to inputDiv by Id
		// "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		$('#achAddEditEntrySection').hide();
		$("#ACHTransactionWizardTabs").show();
		$('#inputDiv').show();
		
		ns.ach.gotoStep(1);
		$('#details').slideDown();

		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
			$("#ACHTransactionWizardTabs").setFocusOnTab();
		}
		
		//No need of third step i.e. confirmation for payee
		if($("#confirmTab").length > 0){
			$("#confirmTab").hide();
		}
	});
	
	$.subscribe('loadACHFormCompleteForTax', function(event, data) {
		// $.log('Form Loaded! ');
		ns.ach.foldPortlet('summary');

		// set portlet title (defined in each page loaded to inputDiv by Id
		// "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		$('#achAddEditEntrySection').hide();
		$("#ACHTransactionWizardTabs").show();
		$('#inputDiv').show();

		ns.ach.gotoStep(1);
		$('#details').slideDown();

		$.publish("common.topics.tabifyNotes");
		if (accessibility) {
			$("#details").setFocus();
		}
	});
	
	$.subscribe('loadSummaryPg', function(event, data) {
		ns.common.showSummaryOnSuccess('summary','done');
	});

	$.subscribe('loadManageNewPayeeFormComplete', function(event, data) {

		ns.ach.showInputDivSteps();

		ns.ach.gotoStep(1);
		// $('#details').slideDown();
        $('#inputDiv').hide();
        $('#achAddEditEntrySection').show();

	});

	$.subscribe('errorLoadACHForm', function(event, data) {
		// $.log('Form Loading error! ');

	});

	$.subscribe('beforeDeleteACHBatchLoadForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names

		$.log('About to load Delete ACH Batch form!');
		$('#details').slideUp();
		$('#quicksearchcriteria').hide();
		ns.ach.setInputTabText('#deleteInputTab');

		$('#verifyTab').hide();
		$('#confirmTab').hide();
		$('#inputDiv').html(""); // before we load, clear previous contents
	});

	$.subscribe('beforeViewACHBatchLoadForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names
		$.log('About to load View ACH Batch form!');
		$('#details').slideUp();
		$('#quicksearchcriteria').hide();
		ns.ach.setInputTabText('#viewInputTab');

		$('#verifyTab').hide();
		$('#confirmTab').hide();
		$('#inputDiv').html(""); // before we load, clear previous contents

	});

	/*
	 * Upload ACH form
	 */
	$.subscribe('beforeUploadACHForm', function(event, data) {
		$.log('About to load ACH Upload form!');
		$('#details').slideUp();
		$('#quicksearchcriteria').hide();
		ns.ach.setInputTabText('#uploadInputTab');
		// the REPORT doesn't appear in a tab any more, so we don't set the
		// verify Tab here
		// ns.ach.setVerifyTabText('#uploadVerifyTab');
		ns.ach.setVerifyTabText('');
		ns.ach.setConfirmTabText('');

		$('#inputDiv').html(""); // before we load, clear previous contents
		$('#verifyTab').hide();
		$('#confirmTab').hide();

		// load the import dialog if not loaded
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "false";
		ns.common.openDialog("fileImport");

		var configImportResults = ns.common.dialogs["fileImportResults"];
		configImportResults.autoOpen = "false";
		ns.common.openDialog("fileImportResults");

	});
	
	$.subscribe('loadFileUploadACHForm', function(event, data) {	
		ns.common.refreshDashboard('dbFileUploadSummary');
		ns.common.selectDashboardItem("ACHUploadLink");
	});

	$.subscribe('uploadACHFormComplete', function(event, data) {
		$.log('ACH Upload Form Loaded! ');
		ns.ach.foldPortlet('summary');

		// set portlet title (defined in each page loaded to inputDiv by Id
		// "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		ns.ach.gotoStep(1);
		
		$('#achAddEditEntrySection').hide();
		$("#ACHTransactionWizardTabs").show();
		$('#inputDiv').show();
		
		$('#details').slideDown();
		$("#detailsPortlet").show();
		$("#customMappingDetails").hide();
		$("#mappingDiv").hide();
		
		
		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
			$("#inputDiv").setFocus();
		}
	});

	$.subscribe('closeUploadACHForm', function(event, data) {

		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});

	$.subscribe('errorUploadACHForm', function(event, data) {
		$.log('Form Loading error! ');
		ns.common.showStatus();
	});

	$.subscribe('successUploadACHForm', function(event, data) {
		$.log('Form Verifying success! ');
		var resStatus = $('#inputDiv').html();
		$('#verifyDiv').html(resStatus);
		ns.ach.gotoStep(2);
		$('#details').slideDown();
	});

	/**
	 * ACH Entry Import
	 */
	$.subscribe('beforeACHImportEntriesForm', function(event, data) {
		ns.common.hideStatus();
	});

	$.subscribe('errorImportEntriesACHForm', function(event, data) {
		$.log('Form Loading error! ');
		ns.common.showStatus();
	});

	$.subscribe('beforeImportEntriesACHForm', function(event, data) {
		// upload button pressed
	});

	$.subscribe('closeImportACHEntryForm', function(event, data) {
		//Hide Import section and show ACHBatch input section.
		$('#achBatchImportDiv').hide();
		$('#achBatchInputDiv').show();
		$('#achBatchImportDiv').empty();
		
		//refresh entry grid and re-calculate total.
		//ns.ach.refreshACHEntries();
		
		ns.ach.importACHBatch();
		
		//$.publish("common.topics.tabifyDesktop");
		//if (accessibility) {
		//	$("#inputDiv").setFocus();
		//}
	});

	$.subscribe('closeImportACHEntryBackToMultiACHTemplate', function(event,data) {

		$.ajax({
			url : "/cb/pages/jsp/ach/addMultiACHTemplateAction_initImport.action?Initialize=true",
			data : {
				Initialize : 'true',
				CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
			},
			success : function(data) {
				$('#inputDiv').html(data);
				ns.ach.showInputDivSteps();
				ns.common.refreshDashboard('dbTemplateSummary');
				ns.common.selectDashboardItem("addMultipleTemplateLink");
			}
		});
	});

	/*
	 * Verify transfer form
	 */
	$.subscribe('beforeVerifyACHForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();

	});

	$.subscribe('verifyACHFormComplete', function(event, data) {
		// $.log('Form Verified! ');
		ns.ach.gotoStep(2);

	});

	$.subscribe('errorVerifyACHForm', function(event, data) {
		// $.log('Form Verifying error! ');

	});

	$.subscribe('successVerifyACHForm', function(event, data) {
		// $.log('Form Verifying success! ');

	});

	$.subscribe('clickVerifyACHForm', function(event, data) {
		// $.log('About to verify form! ');
		if ($("#DefaultTaxFormID").val() == "") {
			$("#defaultTaxFormIDError").html(
					"Child Support Type must be specified.");
		} else {
			$("#defaultTaxFormIDError").html("");
		}

	});

	$.subscribe('recheckACHFormComplete', function(event, data) {
		ns.ach.refreshACHEntries();
	});

	$.subscribe('showErrorRowsACHFormComplete', function(event, data) {
		$.ajax({
			url : ns.ach.showErrorEntriesURL,
			success : function(data) {
				$('#inputDiv').html(data);
			}
		});
	});

	/*
	 * Send ACH form
	 */
	$.subscribe('beforeSendACHForm', function(event, data) {
		// $.log('About to send ACH Batch! ');

	});

	$.subscribe('sendACHFormComplete', function(event, data) {
		// $.log('sending ACH Batch complete! ');

	});

	$.subscribe('sendACHFormSuccess', function(event, data) {
		// $.log('sending ACH succeeded! ');		
		ns.ach.gotoStep(3);
		ns.common.showStatus($('#achBatchResultMessage').html());
		$.publish("reloadSelectedGridTab");
	});
	
	$.subscribe('sendACHTemplateFormSuccess', function(event, data) {
		// $.log('sending ACH succeeded! ');
		ns.ach.gotoStep(3);
		ns.common.showStatus($('#achBatchResultMessage').html());
		ns.ach.reloadSingleACHTemplatesSummaryGrid();
	});
	
	

	$.subscribe('errorSendACHForm', function(event, data) {
		var resStatus = event.originalEvent.request.status;
		if (resStatus === 700) // 700 indicates interval validation error
			ns.common.openDialog("authenticationInfo");
	});

	/*
	 * Send ACH form in Multiple Batches Template
	 */
	$.subscribe('beforeSendACHFormInMBT', function(event, data) {
		// $.log('About to send ACH Batch! ');

	});

	$.subscribe('sendACHFormCompleteInMBT', function(event, data) {
		// $.log('sending ACH Batch complete! ');

	});

	$.subscribe('sendACHFormSuccessInMBT', function(event, data) {
		// $.log('sending ACH succeeded! ');
		ns.ach.gotoStep(1);
		

	});

	$.subscribe('errorSendACHFormInMBT', function(event, data) {
	});

	/*
	 * Save single ACH template
	 */
	$.subscribe('beforeSaveSingleACHTemplate', function(event, data) {
		// $.log('beforeSaveSingleACHTemplate');
		removeValidationErrors();
	});

	$.subscribe('errorSaveSingleACHTemplate', function(event, data) {
		// $.log('errorSaveSingleACHTemplate');
	});

	$.subscribe('saveSingleACHTemplateComplete', function(event, data) {
		// $.log('saveSingleACHTemplateComplete');
	});

	$.subscribe('saveSingleACHTemplateSuccess', function(event, data) {
		// $.log('saveSingleACHTemplateSuccess');
		ns.ach.reloadACHSummaryGrid();
		ns.common.showStatus();
	});

	$.subscribe('quickSearchACHComplete', function(event, data) {
		// make sure the summary is displayed and custom mapping junk is hidden
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
		$('#addCustomMappingsLink').hide();
		$('#summary').show();
		ns.ach.expandPortlet('summary');

		ns.common.setFirstGridPage('#pendingApprovalACHID');// set grid page
		// param to 1 to
		// reload grid from
		// first
		ns.common.setFirstGridPage('#pendingACHGridID');// set grid page param
		// to 1 to reload grid
		// from first
		ns.common.setFirstGridPage('#completedACHGridID');// set grid page
		// param to 1 to
		// reload grid from
		// first

		$.publish("reloadSelectedGridTab");

		/*
		 * $('#pendingApprovalACHID').trigger("reloadGrid");
		 * $('#pendingACHGridID').trigger("reloadGrid");
		 * $('#completedACHGridID').trigger("reloadGrid");
		 */
		$('#details').hide();
		$('#ACHGridTabs').portlet('expand');
	});
	
	// Reload current active grid from first page
	$.subscribe('reloadSelectedACHTemplateGridTab', function(event,data) {
		$(".gridSummaryContainerCls").each(function(i){
			if($(this).is(":visible")){
		        var gridId = $(this).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
		        ns.common.setFirstGridPage('#'+gridId);
		        $('#'+gridId).trigger("reloadGrid");
			}
	    });
	});	
	
	$.subscribe('quickSearchACHTemplatesComplete', function(event, data) {
		// make sure the summary is displayed and custom mapping junk is hidden
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
		$('#addCustomMappingsLink').hide();
		$('#summary').show();
		ns.ach.expandPortlet('summary');

		$.publish("reloadSelectedGridTab");
		
		$('#details').hide();
		$('#ACHTemplateGridTabs').portlet('expand');
	});

	$.subscribe('cancelACHForm', function(event, data) {
		// $.log('About to cancel Form! ');
		// below check is added in case when some thing else other that the grids are loaded in the summary div then just 
		// the toggle of show hide won't work but u have to reload the whole dashboard to get the desired data
		//if($('#ACHGridTabs').size()>0){
		//	ns.common.refreshDashboard('showdbACHSummary',true);
		//}else{
		//	ns.common.refreshDashboard('showdbACHSummary',true);
		//}
		$('#summary').show();
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});
	
	$.subscribe('cancelACHPayeeForm', function(event, data) {
		// $.log('About to cancel Form! ');
		// below check is added in case when some thing else other that the grids are loaded in the summary div then just 
		// the toggle of show hide won't work but u have to reload the whole dashboard to get the desired data
	//	if($('#ACHGridTabs').size()>0){
	//		ns.common.refreshDashboard('showdbPayeeSummary',true);
	//	}else{
	//		ns.common.refreshDashboard('showdbPayeeSummary',true);
	//	}
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});
	
	$.subscribe('cancelACHTemplateForm', function(event, data) {
		// $.log('About to cancel Form! ');
		//ns.common.refreshDashboard('showdbTemplateSummary', true);
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});
	
	$.subscribe('cancelACHMultipleTemplateForm', function(event, data) {
		// $.log('About to cancel Form! ');
		//ns.common.refreshDashboard('showdbTemplateSummary', true);
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});
	

	$.subscribe('ach_backToInput', function(event, data) {

		// $.log('About to back to input! ');
		ns.ach.gotoStep(1);
	});

    $.subscribe('ach_backToACHEntry', function(event, data) {
        ns.ach.gotoStep(1);
        $('#inputDiv').hide();
        ns.ach.hideInputDivSteps();
        $('#verifyDiv').empty();
        $('#confirmDiv').empty();
        $("#achAddEditEntrySection").show();
    });

	$.subscribe('calendarLoadedSuccess', function(event, data) {
		//$('#quickSearchLink').hide();
		//$('#goBackSummaryLink').show();
	});
	$.subscribe('calendarToSummaryLoadedSuccess', function(event, data) {
		$('#goBackSummaryLink').hide();
		$('#quickSearchLink').show();

	});

	$.subscribe('ach_closeSelectedBankFormClickTopics', function(event, data) {
		$('#ACHsearchDestinationBankDialogID').dialog('close');
	});

	// Close the search bank window, used by achbanklistselect.jsp.
	$.subscribe('ach_closeSearchBankListOnClickTopics', function(event, data) {
		$('#ACHsearchDestinationBankDialogID').dialog('close');
	});

	$.subscribe('ach_selectDestBankTopics', function(event, data) {
		ns.ach.selectDestBank("");
	});
	$.subscribe('achPayee_selectDestBankTopics', function(event, data) {
		ns.ach.selectDestBank("#ACHPayeeForm");
	});
	$.subscribe('achEntry_selectDestBankTopics', function(event, data) {
		ns.ach.selectDestBank("#ACHEntryForm");
	});
	// Research bank list, used by achbanklistselect.jsp.
	$.subscribe('ach_researchBankListOnClickTopics', function(event, data) {
		$.ajax({
			type : "POST",
			url : ns.ach.researchBankListURL,
			data : $("#ach_researchACHBankListFormID").serialize(),
			success : function(data) {
				$('#ACHsearchDestinationBankDialogID').html(data);
			}
		});
	});

	$.subscribe('ach_researchBankListWithResultOnClickTopics', function(event,
			data) {
		$.ajax({
			type : "POST",
			url : ns.ach.researchBankListNewURL,
			data : $("#ach_insertSelectedDestinationBankFormID").serialize(),
			success : function(data) {
				$('#ACHsearchDestinationBankDialogID').html(data);
			}
		});
	});

	$.subscribe('achBatch_setBatchType', function(event, data) {
		// ${SecurePath}payments/achbatchaddedit.jsp?SetACHBatchType=true
		// ns.ach.selectDestBank();
	});
	$.subscribe('achBatch_setAllAmounts', function(event, data) {
		$('#fileupload').hide();
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
		$('#addCustomMappingsLink').hide();
		ns.ach.setAllAmountsForBatch();
	});

	// Insert the selected destination bank, used by "achbanklistitem.jsp"
	$.subscribe('ach_insertSelectedACHBankFormClickTopics', function(event,
			data) {

		var fields = $("#ach_insertSelectedDestinationBankFormID")
				.serializeArray();

		jQuery.each(fields, function(i, field) {
			if (field.name == "bankName") {
				$("#BANKNAME").val(field.value);
				$("#BANKNAMEhidden").val(field.value);
			}
			if (field.name == "rtNum") {
				$("#ROUTINGNUMBER").val(field.value);
			}
		});
		$('#ACHsearchDestinationBankDialogID').dialog('close');

		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
			$("#inputDiv").setFocus();
		}
	});

	$.subscribe('ach_removeAddEditACHTemplateObject', function(event, data) {
		ns.ach.removeAddEditACHTemplateObject();
	});

};

ns.ach.importAmounts = function(urlString) {
	// we need to serialize the data so it doesn't get lost before importing
	// entries
	var strutsActionName = "/cb/pages/jsp/ach/" + $("#strutsActionName").val()
	+ '_saveUserInputBeforeImportAmout.action';
    ns.ach.doPartialImport = true;

	$.ajax({
		type : "POST",
		url : strutsActionName,
		data : $("#AddBatchNew").serialize(),
		success : function(data) {
			ns.ach.showImportSection(data);
		}
	});
};

ns.ach.importEntries = function(urlString) {
	// we need to serialize the data so it doesn't get lost before importing
	// entries
	var strutsActionName = "/cb/pages/jsp/ach/" + $("#strutsActionName").val()
	+ '_saveUserInputBeforeImportEntries.action';
    ns.ach.doPartialImport = false;

	$.ajax({
		type : "POST",
		url : strutsActionName,
		data : $("#AddBatchNew").serialize(),
		success : function(data) {
			ns.ach.showImportSection(data);
		}
	});
};

/**
 * Updates Import section where Import Amount or Import entries section could
 * be shown.
 */
ns.ach.showImportSection = function(data){
	$('#achBatchImportDiv').html(data);
	$('#achBatchInputDiv').hide();
	$('#achBatchImportDiv').show();
	//$('#customMappingsLink').removeAttr('style');
	$('#customMappingsLink').show();
	ns.ach.gotofileimport();
};

ns.ach.hideInputDivSteps = function() {
	$("#ACHTransactionWizardTabs").hide();
	$('#inputTab').hide();
	$('#verifyTab').hide();
	$('#confirmTab').hide();
};

ns.ach.showInputDivSteps = function() {
	$("#ACHTransactionWizardTabs").show();
	$('#inputTab').show();
	// don't show empty tabs
	if ($('#verifyTab a span').html() != "")
		$('#verifyTab').show();
	else
		$('#verifyTab').hide();
	if ($('#confirmTab a span').html() != "")
		$('#confirmTab').show();
	else
		$('#confirmTab').hide();
};

ns.ach.setInputTabText = function(headingRef) {
	var heading = headingRef;
	if (headingRef.indexOf('#') >= 0)
		heading = $(headingRef).html();
	var $title = $('#inputTab a');
	$title.attr('title', heading);
	$title = $('#inputTab a span');
	$title.html(heading);
};
ns.ach.setVerifyTabText = function(headingRef) {
	var heading = headingRef;
	if (headingRef.indexOf('#') >= 0)
		heading = $(headingRef).html();
	var $title = $('#verifyTab a');
	$title.attr('title', heading);
	$title = $('#verifyTab a span');
	$title.html(heading);
};
ns.ach.setConfirmTabText = function(headingRef) {
	var heading = headingRef;
	if (headingRef.indexOf('#') >= 0)
		heading = $(headingRef).html();
	var $title = $('#confirmTab a');
	$title.attr('title', heading);
	$title = $('#confirmTab a span');
	$title.html(heading);
};

ns.ach.importBatches = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#inputDiv').html(data);
			ns.ach.gotofileimport();
		}
	});
};

ns.ach.gotofileimport = function() {
	ns.common.refreshDashboard('dbFileUploadSummary');
	$('#fileupload').show();

	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		$("#inputDiv").setFocus();
	}
};

ns.ach.cancelAchFileUpload = function() {
	$('#details').slideUp();
	ns.ach.expandPortlet('summary');
};

var ACHBankLookupForm = "";
ns.ach.selectDestBank = function(theForm) {
	// if theForm is empty, we are searching again.... use same form.
	if (theForm != "")
		ACHBankLookupForm = theForm;
	// define ANY_SEARCH - including country, etc. If any search, call
	// achbanklistselect.jsp
	var bankName = $("#BANKNAME").val() == "" ? false : true;
	var rtNum = $("#ROUTINGNUMBER").val() == "" ? false : true;
	var bankIdentifierType = $("#BankIdentifierType").val();
	var destURL = "";

	if (theForm == "") {
		bankName = false;
		rtNum = false;
	}

	var optionObject = new Object;

	if (bankName || rtNum) {
		var bankNameVar = $("#BANKNAME").val();
		var fedRTNVar = $("#ROUTINGNUMBER").val();

		optionObject.bankName = bankNameVar;
		optionObject.fedRTN = fedRTNVar;
		optionObject.bankIDType = bankIdentifierType;
		destURL = "/cb/pages/jsp/ach/achbanklistselect.jsp";
		// destURL =
		// "/cb/pages/jsp/ach/achbanklistselect.jsp?bankName="+$("#BANKNAME").val()+"&fedRTN="+$("#ROUTINGNUMBER").val()+"&bankIDType="+bankIdentifierType;
	} else if ($("#Lookup_BankName").length > 0
			&& ($("#Lookup_BankName").val() != ""
					|| $("#Lookup_BankCity").val() != ""
					|| $("#Lookup_BankState").val() != ""
					|| $("#Lookup_BankCountry").val() != ""
					|| $("#Lookup_BankRTN").val() != ""
					|| $("#Lookup_BankNationalID").val() != "" || $(
					"#Lookup_BankSwiftRTN").val() != "")) {
		var bankNameVar = $("#Lookup_BankName").val();
		var fedRTNVar = $("#Lookup_BankRTN").val();
		var bankCityVar = $("#Lookup_BankCity").val();
		var bankStateVar = $("#Lookup_BankState").val();
		var bankCountryVar = $("#Lookup_BankCountry").val();

		optionObject.bankName = bankNameVar;
		optionObject.fedRTN = fedRTNVar;
		optionObject.bankCity = bankCityVar;
		optionObject.bankState = bankStateVar;
		optionObject.bankCountry = bankCountryVar;
		optionObject.bankIDType = bankIdentifierType;

		destURL = "/cb/pages/jsp/ach/achbanklistselect.jsp";
		// destURL =
		// "/cb/pages/jsp/ach/achbanklistselect.jsp?bankName="+$("#Lookup_BankName").val()+
		// "&fedRTN="+$("#Lookup_BankRTN").val()+
		// "&bankCity="+$("#Lookup_BankCity").val()+
		// "&bankState="+$("#Lookup_BankState").val()+
		// "&bankCountry="+$("#Lookup_BankCountry").val()+
		// "&bankIDType="+bankIdentifierType;
	} else {
		optionObject.bankIDType = bankIdentifierType;
		// destURL =
		// "/cb/pages/jsp/ach/achbanklist.jsp?bankName=&fedRTN=&bankIDType="+bankIdentifierType;
		destURL = "/cb/pages/jsp/ach/achbanklist.jsp";
	}
	var actionURL = "/cb/pages/jsp/ach/getACHParticipantBanksAction_preProcess.action";
	optionObject.CSRF_TOKEN = ns.home.getSessionTokenVarForGlobalMessage;
	ns.ach.searchDestinationBankForm(destURL, actionURL, optionObject);
};

ns.ach.searchDestinationBankForm = function(urlString, actionString,
		optionObject) {
	var formId = ACHBankLookupForm;
	if (!actionString) {
		$.ajax({
			url : urlString,
			type : "POST",
			data : optionObject,
			success : function(data) {
				$('#ACHsearchDestinationBankDialogID').html(data)
						.dialog('open');
			}
		});
		return;
	}
	$.ajax({
		type : "POST",
		url : actionString,
		data : $(formId).serialize(),
		success : function(data) {
			$.ajax({
				url : urlString,
				type : "POST",
				data : optionObject,
				success : function(data) {
					$('#ACHsearchDestinationBankDialogID').html(data).dialog(
							'open');
				}
			});
		}
	});
};

ns.ach.modifyDetailContent = function(tab) {
	var urlStr;
	if (tab) {
		urlStr = "/cb/pages/jsp/ach/ach_details_content.jsp?tab=true";
	} else {
		urlStr = "/cb/pages/jsp/ach/ach_details_content.jsp";
	}

	$.ajax({
		url : urlStr,
		success : function(data) {
			$("#portletContent").html(data);
		}
	});
};

ns.ach.gotoTemplates = function() {
	ns.common.showSummaryOnSuccess("summary","done");
	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#details').hide();
	$('#summary').show();
	$("#detailsPortlet").show();
/*	
	$('#addSingleTemplateLink').removeAttr('style');
	$('#addMultipleTemplateLink').removeAttr('style');
	$('#addSingleACHBatchLink').attr('style', 'display:none');
	$('#addMultipleACHBatchLink').attr('style', 'display:none');
	$('#loadTemplateLink').attr('style', 'display:none');
	$('#ACHtemplatesLink').attr('style', 'display:none');
	$('#customMappingsLink').attr('style', 'display:none');
	$('#details').hide();
	$('#quickSearchLink').show();
	$('#quicksearchcriteria').hide();
	*/
	
	// switch quick search creteria board
/*	
	$('#achsearchcriteria').attr('style', 'display:none');
	$('#templatessearchcriteria').attr('style', 'display:block');
	$('#multitemplatessearchcriteria').attr('style', 'display:block');
	*/
	
/*	$('#loadTemplateId').hide();
	$('#addACHPayeeLink').attr('style', 'display:none');
	$('#ACHBatchesLink').removeAttr('style');
	$('#ACHPayeesLink').removeAttr('style');
	$('#ACHUploadLink').removeAttr('style');

	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#addCustomMappingsLink').hide();
	$('#fileupload').hide();
	$('#summary').show();
	$("#detailsPortlet").show();*/

	ns.ach.setInputTabText('#templateInputTab');
	$('#verifyTab').hide();
	$('#confirmTab').hide();

	$.publish("common.topics.tabifyNotes");
	if (accessibility) {
		$("#ACHTransactionWizardTabs").setFocusOnTab();
	}

};

ns.ach.gotoACHBatches = function() {
	ns.common.showSummaryOnSuccess("summary","done");
	//ns.common.refreshDashboard('dbACHSummary');
	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#details').hide();
	$('#summary').show();
	$("#detailsPortlet").show();
/*	
	$('#addSingleTemplateLink').attr('style', 'display:none');
	$('#addMultipleTemplateLink').attr('style', 'display:none');
	$('#ACHBatchesLink').attr('style', 'display:none');
	$('#goBackSummaryLink').attr('style', 'display:none');
	$('#addACHPayeeLink').attr('style', 'display:none');
	$('#customMappingsLink').attr('style', 'display:none');

	$('#addSingleACHBatchLink').removeAttr('style');
	$('#addMultipleACHBatchLink').removeAttr('style');
	$('#loadTemplateLink').removeAttr('style');
	$('#details').hide();
	$('#quickSearchLink').show();
	$('#quicksearchcriteria').hide();
*/
	// switch quick search creteria board
/*	
	$('#achsearchcriteria').attr('style', 'display:block');
	$('#templatessearchcriteria').attr('style', 'display:none');
	$('#multitemplatessearchcriteria').attr('style', 'display:none');
	
*/
	
/*	
	$('#ACHtemplatesLink').removeAttr('style');
	$('#ACHPayeesLink').removeAttr('style');
	$('#ACHUploadLink').removeAttr('style');
	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#addCustomMappingsLink').hide();
	$('#fileupload').hide();
	$('#summary').show();
	$("#detailsPortlet").show();
*/
	ns.ach.setInputTabText('#normalInputTab');
	ns.ach.setVerifyTabText('#normalVerifyTab');
	ns.ach.setConfirmTabText('#normalConfirmTab');
	ns.ach.showInputDivSteps();

	$.publish("common.topics.tabifyNotes");
	if (accessibility) {
		$("#ACHTransactionWizardTabs").setFocusOnTab();
	}
};

ns.ach.gotoACHPayees = function() {
	ns.common.refreshDashboard('dbPayeeSummary');
	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#details').hide();
	$('#summary').show();
	$("#detailsPortlet").show();
/*	
	$('#addSingleTemplateLink').attr('style', 'display:none');
	$('#addMultipleTemplateLink').attr('style', 'display:none');
	$('#goBackSummaryLink').attr('style', 'display:none');
	$('#addSingleACHBatchLink').attr('style', 'display:none');
	$('#addMultipleACHBatchLink').attr('style', 'display:none');
	$('#loadTemplateLink').attr('style', 'display:none');
	$('#customMappingsLink').attr('style', 'display:none');
	$('#details').hide();
	$('#quickSearchLink').hide();
	$('#quicksearchcriteria').hide();
	$('#loadTemplateId').hide();
*/
/*
	$('#ACHtemplatesLink').removeAttr('style');
	$('#ACHBatchesLink').removeAttr('style');
	$('#ACHUploadLink').removeAttr('style');
	$('#addACHPayeeLink').removeAttr('style');
	$('#ACHPayeesLink').attr('style', 'display:none');

	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#addCustomMappingsLink').hide();
	$('#fileupload').hide();
	$('#summary').show();
	$("#detailsPortlet").show();
*/
	ns.ach.showInputDivSteps();

	$.publish("common.topics.tabifyNotes");
	if (accessibility) {
		$("#ACHTransactionWizardTabs").setFocusOnTab();
	}
};

ns.ach.gotoStep = function(stepnum) {
	ns.common.gotoWizardStep('detailsPortlet',stepnum);
	
	var tabindex = stepnum - 1;
	$('#ACHTransactionWizardTabs').tabs('enable', tabindex);
	$('#ACHTransactionWizardTabs').tabs('option', 'active', tabindex);
	$('#ACHTransactionWizardTabs').tabs('disable', (tabindex + 1) % 3);
	$('#ACHTransactionWizardTabs').tabs('disable', (tabindex + 2) % 3);

	$.publish("common.topics.tabifyNotes");
	if (accessibility) {
		$("#ACHTransactionWizardTabs").setFocusOnTab();
	}
};

/**
 * Close Details island and expand Summary island
 */
ns.ach.closeACHTabs = function() {
	ns.ach.expandPortlet('summary');
	$('#details').slideUp();

	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		$("#details").setFocus();
	}
};

// The method is used by to get ACH JSP page into a div.
ns.ach.submitACHBatchFormPreprocess = function(divID) {
	// todo: ACHBatchForm submit
	$
			.ajax({
				type : "POST",
				data : $("#ACHBatchForm").serialize(),
				async : false,
				url : "/cb/pages/jsp/ach/AddACHBatchAction_processingRequestParameters.action",
				success : function(data) {
					$(divID).html(data);
				}
			});

	// $.get(urlString,
	// function(data) {
	// $(divID).html(data);
	// });
};

ns.ach.submitACHEntryFormPreprocess = function(divID) {
	// todo: ACHEntryForm submit
	$
			.ajax({
				type : "POST",
				data : $("#ACHEntryForm").serialize(),
				async : false,
				url : "/cb/pages/jsp/ach/editACHEntryAction_processingRequestParameters.action",
				success : function(data) {
					$(divID).html(data);
				}
			});
};

ns.ach.achbatch_setup = function() {

	// $.debug(true);
	// registerGlobalAjaxHandler();
	// ajaxSetup();
	// setup();

	// //////////////////// ADD/EDIT ACH Batch, Page 1 (Input)
	$.subscribe('beforeVerifyACHBatchForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('verifyACHBatchFormComplete', function(event, data) {
		// $.log('Form Verified! ');
		ns.ach.gotoStep(2);

	});

	$.subscribe('errorVerifyACHBatchForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successVerifyACHBatchForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	// //////////////////// ADD/EDIT ACH Batch, Page 2 (Verify/Confirm)
	$.subscribe('beforeConfirmACHBatchForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmACHBatchFormComplete', function(event, data) {
		// $.log('Form Verified! ');
		ns.ach.gotoStep(3);
		ns.common.showStatus("Your ACH Batch has been submitted.");
		$('#pendingApprovalACHID').trigger("reloadGrid");
		$('#pendingACHGridId').trigger("reloadGrid");
		// can't edit a completed, no need to reload
		// $('#completedACHGrid').trigger("reloadGrid");
		$('#details').slideDown();
	});

	$.subscribe('errorConfirmACHBatchForm', function(event, data) {
		// $.log('Form Verifying error! ');
		var resStatus = event.originalEvent.request.status;
		if (resStatus === 700) // 700 indicates interval validation error
			ns.common.openDialog("authenticationInfo");

	});

	$.subscribe('successConfirmACHBatchForm', function(event, data) {
		// $.log('Form Verifying error! ');

	});

	// //////////////////// VIEW ACH Batch
	$.subscribe('closeViewACHBatch', function(event, data) {
		ns.ach.closeACHTabs();
		// ns.common.showStatus();
	});

	$.subscribe('beforeViewACHBatchForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names
	});

	$.subscribe('reverseEnableViewACHBatch', function(event, data) {
		// refresh the ENTRIES section within the ACHEntries div
		// disable the reverseEnableViewACHBatch
		$('#reverseEnableViewACHBatchLink').hide();
		// enable the reverseViewACHBatch button
		// enable the checkAll button
		// enable the checkNone button
		$('#reverseViewACHBatchLink').show();
		$('#reverseViewACHBatchLink_checkAll').show();
		$('#reverseViewACHBatchLink_checkNone').show();

		var urlString = "/cb/pages/jsp/ach/achbatch-reverse.jsp";
		$.ajax({
			url : urlString,
			success : function(data) {
				$('#ViewACHEntries').html(data);
			}

		});
	});

	// //////////////////// Delete ACH Batch
	$.subscribe('closeDeleteACHBatch', function(event, data) {
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});

	$.subscribe('beforeDeleteACHBatchForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names

	});
	$.subscribe('errorDeleteACHBatchForm', function(event, data) {
		// $.log('Form Loading error! ');
		ns.common.showStatus();
		ns.ach.closeACHTabs();
	});
	$.subscribe('successDeleteACHBatchForm', function(event, data) {
		$('#pendingApprovalACHID').trigger("reloadGrid");
		$('#pendingACHGridId').trigger("reloadGrid");
		// can't delete a completed
		// $('#completedACHGrid').trigger("reloadGrid");
		ns.ach.closeACHTabs();
		ns.common.hideStatus();
	});

	// //////////////////// Delete ACH Entry
	$.subscribe('closeDeleteACHEntry', function(event, data) {
		ns.common.closeDialog("#addEditACHEntryDialogID");
	});

	$.subscribe('beforeDeleteACHEntryForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names

	});
	$.subscribe('errorDeleteACHEntryForm', function(event, data) {
		// $.log('Form Loading error! ');
	});

	// //////////////////// Edit ACH Entry
	$.subscribe('beforeEditACHEntryForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names

	});
	$.subscribe('errorEditACHEntryForm', function(event, data) {
		// $.log('Form Loading error! ');
	});

	// //////////////////// Add ACH Entry
	ns.ach.addEditEntry = function(formId, urlString) {
		var isNew = $('#isNew').val();
		$.ajax({
			url : urlString,
			type : "POST",
			data : $("#" + formId).serialize(),
			success : function(data) {
				// Show Entry Section
				ns.ach.showACHEntryDetails(data);
				// below condition is for showing input fields when add new button is clicked 
				if(typeof(isNew)!="undefined" && isNew=='true'){
					openParticipantDetails();
				}
			}
		});
	};

	ns.ach.showACHEntryDetails = function(data) {
		$('#achAddEditEntrySection').html(data);
		$('#achAddEditEntrySection').show();
		$('#inputDiv').hide();
		ns.ach.hideInputDivSteps();

		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
			$("#achAddEditEntrySection").setFocus();
		}
	};

	$.subscribe('cancelACHEntry', function(event, data) {
		/*
		 * $.ajax({ url : "/cb/pages/jsp/ach/achbatchaddedit.jsp", success :
		 * function(data) { $('#inputDiv').html(data);
		 * ns.ach.showInputDivSteps();
		 * 
		 * $.publish("common.topics.tabifyDesktop"); if (accessibility) {
		 * $("#inputDiv").setFocus(); } } });
		 */
		$("#ACHTransactionWizardTabs").show();
		$('#inputDiv').show();
		$('#achAddEditEntrySection').empty();
		$('#ACHTransactionWizardTabs li[role="tab"]').attr('style','display: list-item;');
	});

	$.subscribe('beforeAddACHEntryForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names
		removeValidationErrors();
	});
	$.subscribe('errorAddACHEntryForm', function(event, data) {
		// $.log('Form Loading error! ');
	});
	$.subscribe('completeAddACHEntryForm', function(event, data) {
		$("#isDeleteId").val("false");
		ns.ach.refreshACHEntries();
	});

	$.subscribe('closeACHEntryImportAddenda', function(event, data) {
		$('#importEntryAddendaDialogID').dialog('close');
	});

	$.subscribe('beforeACHEntryImportAddendaForm', function(event, data) {
		// $.log('Form Loading error! ');
		// clear everything from the VIEW Dialog ID to not cause conflicting DIV
		// names

	});
	$.subscribe('errorACHEntryImportAddendaForm', function(event, data) {
		// $.log('Form Loading error! ');
	});
	$.subscribe(
					'successACHEntryImportAddendaForm',
					function(event, data) {
						// submit should have happened.
						$('#importEntryAddendaDialogID').dialog('close');

						var urlString = "/cb/pages/jsp/ach/ACHEntryImportAddendaAction_executeAddendaString.action";
						$.ajax({
							url : urlString,
							contentType : "application/json;charset=utf-8",
							dataType : "json",
							async : false,
							success : function(data) {
								$("#ACHADDENDA").val(data.addendaString);
							},
							error : function(msg) {
							}
						});
					});
};

ns.ach.achpayee_setup = function() {

	// //////////////////// ADD/EDIT ACH Payee, Page 1 (Input)
	$.subscribe('beforeVerifyACHPayeeForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('verifyACHPayeeFormComplete', function(event, data) {
		// $.log('Form Verified! ');
		ns.ach.gotoStep(2);
        $('#achAddEditEntrySection').hide();

	});

	$.subscribe('errorVerifyACHPayeeForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successVerifyACHPayeeForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	// //////////////////// ADD/EDIT ACH Payee, Page 2 (Verify/Confirm)
	$.subscribe('beforeConfirmACHPayeeForm', function(event, data) {
		// $.log('About to verify form! ');
		removeValidationErrors();
	});

	$.subscribe('confirmACHPayeeFormComplete', function(event, data) {
		$('#StandardPayeeLink').click();
		// $.log('Form Verified! ');
	});

	$.subscribe('errorConfirmACHPayeeForm', function(event, data) {
		// $.log('Form Verifying error! ');
	});

	$.subscribe('successConfirmACHPayeeForm', function(event, data) {
		// $.log('Form Verifying error! ');
		/**
		 * Close Details island and expand Summary island
		 */

		$('#details').slideUp();
		ns.ach.refreshACHPayeeGrids(); // just do a full refresh
		//ns.ach.gotoStep(3);
		ns.ach.expandPortlet('summary');
		$('#verifyTab').removeClass('ui-state-active');
		ns.common.showStatus($.trim(data.textContent));
		ns.common.refreshDashboard("showdbPayeeSummary", true);
	});

	$.subscribe('successConfirmACHSecurePayeeForm', function(event, data) {
		$.ajax({
			url : "/cb/pages/jsp/ach/achmanagedpayees.jsp",
			success : function(data) {
				$("#achAddEditEntrySection").html(data);
				ns.ach.gotoStep(1);
                $('#inputDiv').hide();
				ns.ach.hideInputDivSteps();
                $('#verifyDiv').empty();
                $('#confirmDiv').empty();
                $("#achAddEditEntrySection").show();

                $.publish("common.topics.tabifyDesktop");
                if (accessibility) {
                    $("#achAddEditEntrySection").setFocus();
                }
                ns.common.selectDashboardItem("ACHPayeesLink");
			}
		});
	});

	// //////////////////// VIEW ACH Payee
	$.subscribe('closeViewACHPayee', function(event, data) {
		ns.common.closeDialog('#viewACHPayeeDetailsDialogID');
		// ns.common.showStatus();
	});
};

$.subscribe('clickAddNewBatchTopic', function(event, data) {
	ns.ach.addNewBatch();
});

$.subscribe('backToACHMultiBatchForm', function(event, data) {
	ns.ach.backToACHMultiBatch();
});

$.subscribe('completeSaveToMultiTempForm', function(event, data) {
	ns.ach.backToACHMultiBatch();
});

$.subscribe('clickSaveToMultiTempForm', function(event, data) {
	var actionName = "/cb/pages/jsp/ach/" + $("#strutsActionName").val()
			+ "_saveToMultiTemplate.action";
	$("#AddBatchNew").attr("action", actionName);
});

$.subscribe('clickEditToMultiTempForm', function(event, data) {
	var actionName = "/cb/pages/jsp/ach/" + $("#strutsActionName").val()
			+ "_saveToMultiTemplate.action";
	$("#AddBatchNew").attr("action", actionName);
});

$.subscribe('successSaveToMultiTempForm', function(event, data) {
});

$.subscribe('successSaveMultiTempForm', function(event, data) {
	ns.common.showSummaryOnSuccess('summary','done');
	ns.ach.closeACHTabs();
	ns.common.showStatus();
});

$.subscribe('completeSaveMultiTempForm', function(event, data) {
});

$.subscribe('completeConfirmBatchesForm', function(event, data) {
	ns.ach.gotoStep(2);
});

$.subscribe(
				'successSubmitACHBatchesForPaymentForm',
				function(event, data) {
					var resStatus = event.originalEvent.request.status;
					if (resStatus != 700) // 700 indicates interval validation
					// error
					{
						$('#verifyDiv').empty(); // clear out form data from
						// verify page
						$
								.ajax({
									url : "/cb/pages/jsp/ach/achmultibatchaddeditconfirm.jsp?confirm=true",
									success : function(data) {
										$('#confirmDiv').html(data);
									}
								});
						ns.ach.gotoStep(3);
						ns.common.showStatus();
					}
				});

$.subscribe('errorSubmitBatchesForPaymentForm', function(event, data) {
	var resStatus = event.originalEvent.request.status;
	if (resStatus === 700) // 700 indicates interval validation error
		ns.common.openDialog("authenticationInfo");
	else {
		$.ajax({
			url : "/cb/pages/jsp/ach/achmultibatchaddeditconfirm.jsp",
			success : function(data) {
				$('#verifyDiv').html(data);
			}
		});
	}
});

$.subscribe(
				'clickCheckBatchesFormTopic',
				function(event, data) {
					$("#AddBatchFormId")
							.attr("action",
									"/cb/pages/jsp/ach/editMultiACHTemplateAction_recheckValidation.action");
				});

$.subscribe('clearDateError', function(event, data) {
	$(".dateError").html("");
});

$.subscribe('cancelACHMultiTemplateBatchComplete', function(event, data) {
	ns.common.closeDialog();
	// var urlString =
	// "/cb/pages/jsp/ach/achmultitemplateaddedit.jsp?DeleteMultiBatch=DeleteACHBatch";
	$.ajax({
		url : ns.ach.cancelACHMultiTemplateBatchUrl,
		success : function(data) {
			$('#inputDiv').html(data);
		}
	});
});

$.subscribe('cancelACHMultiTemplateBatchSuccess', function(event, data) {
});

$.subscribe('deleteACHEntryClick',
				function(event, data) {
					ns.common.closeDialog();
					var showErrors = jQuery('#achEntriesGridId').hasClass(
							"ShowErrors");
					if (showErrors
							&& jQuery('#achEntriesGridId').jqGrid(
									'getGridParam', 'records') <= 1) {
						showErrors = false;
						jQuery('#achEntriesGridId').removeClass("ShowErrors");
					}
					$.ajax({
						url : ns.ach.deleteAchEntryByIDUrl,
						data : "curPage="
								+ jQuery('#achEntriesGridId').jqGrid(
										'getGridParam', 'page')
								+ "&curRowNum="
								+ jQuery('#achEntriesGridId').jqGrid(
										'getGridParam', 'rowNum')
								+ "&ShowErrors=" + showErrors,
						// "&DeleteACHEntry=" + ns.ach.deleteAchEntryID,
						success : function(data) {
							ns.ach.refreshACHEntries();
						}
					});
				});

$.subscribe('clickAddACHTempForm', function(event, data) {
	$("#AddBatchNew").attr("action",
			"/cb/pages/jsp/ach/addACHBatchTemplate_verify.action");
});

$.subscribe('clickEditACHTempForm', function(event, data) {
	$("#AddBatchNew").attr("action",
			"/cb/pages/jsp/ach/editACHBatchTemplate_verify.action");
});

$.subscribe('completeAddACHTempForm', function(event, data) {
});

$.subscribe('errorAddACHTempForm', function(event, data) {
	ns.common.showStatus(data.innerHTML);
});

$.subscribe('successAddACHTempForm', function(event, data) {
	ns.ach.closeACHTabs();
	ns.common.showStatus();
	ns.ach.reloadSingleACHTemplatesSummaryGrid();
});

$.subscribe('successSaveAsTemplateTopic', function(event, data) {
	ns.common.showStatus();
	ns.ach.reloadSingleACHTemplatesSummaryGrid();
});

$.subscribe('clickSaveAsTemplateTopic', function(event, data) {
	var action = $("#strutsActionName").val();
	if(action.match('Child')){
		action = "addChildSupportTemplate";
	} else if(action.match('Tax')){
		action = "addTaxBatchTemplate";
	} else {
		action = "addACHBatchTemplate";
	}
	var url = "/cb/pages/jsp/ach/" + action + "_init_saveAsTemplate.action";
	$.ajax({
		url : url,
		success : function(data) {
			$('#saveAsACHTemplateDialogID').html(data).dialog('open');
		}
	});
});

$.subscribe('saveChildTaxEntryClick', function(event, data) {
	$("#viewAddendaId").val("false");
	removeValidationErrors();
});

$.subscribe('saveACHEntryClick', function(event, data) {
	$("#viewAddendaId").val("false");
	removeValidationErrors();
});

$.subscribe('saveChildTaxEntrySuccess', function(event, data) {
	$("#isDeleteId").val("false");
	ns.common.closeDialog();
	ns.ach.refreshACHEntries();
});

$.subscribe('viewAddendaClick', function(event, data) {
	// used by both ACH (Healthcare) and TAX PAYMENT Addendas
	$("#viewAddendaId").val("true");
	removeValidationErrors();
});

$.subscribe('viewACHEntryAddendaSuccess', function(event, data) {
	$("#ACHADDENDA").val(data.innerHTML);

	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		$("#details").setFocus();
	}
});

$.subscribe('viewTaxAddendaSuccess', function(event, data) {
	$("#addendaInputId").val(data.innerHTML);

	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		$("#details").setFocus();
	}
});

$.subscribe('closeChildTaxEntryDialogTopic', function(event, data) {
	if ($("#isDeleteId").val() == "true") {
		ns.ach.deleteChildTaxEntry();
	}
	ns.common.closeDialog("#addEditACHEntryDialogID");
});

$.subscribe('hideCustomMappingTopic', function(event, data) {
	ns.ach.hideCustomMappingButton();
});

$.subscribe('loadAchTemplateGrid', function(event, data) {
	ns.common.refreshDashboard('showdbTemplateSummary', true);
});
ns.ach.beforeLoadACHForm = function() {
	$('#details').slideUp();
	$('#quicksearchcriteria').hide();
	$('#fileupload').hide();
	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#addCustomMappingsLink').hide();
	$('#loadTemplateId').hide();
	$('#summary').show();
	$("#detailsPortlet").show();

	ns.ach.foldPortlet('summary');

	// set portlet title (defined in each page loaded to inputDiv by Id
	// "PageHeading"
	var heading = $('#PageHeading').html();
	var $title = $('#details .portlet-title');
	$title.html(heading);

	ns.ach.gotoStep(1);
	$('#details').slideDown();
};

ns.ach.beforeLoadManageNewPayeeForm = function() {

	// UPLOAD might change the names of the tabs, so change them back.
	ns.ach.setInputTabText('#normalInputTab');
	ns.ach.setVerifyTabText('#normalVerifyTab');

	// ns.ach.foldPortlet('summary');

	ns.ach.gotoStep(1);
    $('#inputDiv').hide();
    $('#achAddEditEntrySection').show();
};

ns.ach.enableLoadButton = function(selectedvalue) {
	if (selectedvalue == "-1")
		$("#loadButtonId").addClass("ui-state-disabled").attr("disabled", true);
	else if (selectedvalue.indexOf("MOREACHTEMPLATES") >= 0) {
		$("#ACHtemplatesLink").click();
	} else if (selectedvalue.indexOf("MORETAXTEMPLATES") >= 0) {
		$("#TaxtemplatesLink").click();
	} else if (selectedvalue.indexOf("MORECHILDTEMPLATES") >= 0) {
		$("#ChildsptemplatesLink").click();
	} else {
		$("#loadButtonId").removeClass("ui-state-disabled").removeAttr(
				"disabled");
	}
	$('#loadButtonId').trigger('click');
};

ns.ach.getTemplates = function() {
	$.ajax({
		url : "/cb/pages/jsp/ach/inc/loadTemplate.jsp",
		success : function(data) {
			$("#loadTemplateId").html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#details").setFocus();
			}
		}
	});
};

ns.ach.loadTemplate = function(tokenString,achType) {
	var selectedvalue = $("#TemplateID").val();
	if (selectedvalue.indexOf("Multiple_") >= 0) {
		if (selectedvalue.indexOf("Multiple_USER") >= 0) {
			selectedvalue = selectedvalue.substr(13);
		} else if (selectedvalue.indexOf("Multiple_BUSINESS") >= 0) {
			selectedvalue = selectedvalue.substr(17);
		}
		urlString = "/cb/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action";
	} else {
		
		var ids=selectedvalue.split(':');
		
		var ID='';
		var RecID='';
		if(ids[0])
			ID=ids[0];
		if(ids[1])
			RecID=ids[1];
			if(achType=='ACHBatch')
			{
				urlString = "/cb/pages/jsp/ach/addACHBatch_initLoadTemplate.action?ID="+ID+"&RecID="+RecID;
			}
			else if(achType=='ChildSupportPayment')
			{
			
				urlString = "/cb/pages/jsp/ach/addChildSupportBatch_initLoadTemplate.action?ID="+ID+"&RecID="+RecID;
			}
			else if(achType=='TaxPayment')
			{
				urlString ="/cb/pages/jsp/ach/addTaxBatch_initLoadTemplate.action?ID="+ID+"&RecID="+RecID;
			}
			
	}

	$.ajax({
		type : "POST",
		url : urlString,
		data : {
			TemplateID : selectedvalue,
			CSRF_TOKEN : tokenString,
			loadMultipleBatch : "true"
		},
		success : function(data) {
			$("#inputDiv").html(data);
			ns.ach.beforeLoadACHForm();

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}

			if (accessibility) {
				$("#ACHTransactionWizardTabs").setFocusOnTab();
			}
		}
	});
};

ns.ach.managedPayees = function() {
	$.ajax({
		type : "POST",
		data : $("#ACHEntryForm").serialize(),
		async : false,
		url : "/cb/pages/jsp/ach/editACHEntryAction_saveParameters.action",
		success : function(data) {
			$.ajax({
				url : "/cb/pages/jsp/ach/achmanagedpayees.jsp",
				success : function(data) {
					$("#isDeleteId").val("false");
					ns.ach.hideInputDivSteps();

					$("#achAddEditEntrySection").html(data);

					$.publish("common.topics.tabifyDesktop");
					if (accessibility) {
						$("#achAddEditEntrySection").setFocus();
					}
				}
			});
		}
	});
};

ns.ach.editSecurePayee = function(urlString) {
	$.ajax({
		type : "POST",
		data : $("#ACHEntryForm").serialize(),
		async : false,
		url : "/cb/pages/jsp/ach/editACHEntryAction_saveParameters.action",
		success : function(data) {
			$.ajax({
				url : urlString,
				success : function(data) {
					$("#isDeleteId").val("false");
					$("#achAddEditEntrySection").html(data);

					$.publish("common.topics.tabifyDesktop");
					if (accessibility) {
						$("#achAddEditEntrySection").setFocus();
					}
				}
			});
		}
	});
};

ns.ach.deleteChildTaxEntry = function() {
	$.ajax({
		url : ns.ach.deleteChildTaxEntryUrl,
		success : function(data) {
		}
	});
};

ns.ach.expandPortlet = function(summary) {
	$('#ACHGridTabs').show();
	$('#templateGridTabs').show();
	$('#summary').show();
	$('#childsptypeGridTabs').show();
	$('#taxtypeGridTabs').show();
	$('#ACHTemplateGridTabs').show();
};

ns.ach.foldPortlet = function(summary) {
	//$('#ACHGridTabs').hide();
	$('#templateGridTabs').hide();
	$('#summary').hide();
	$('#childsptypeGridTabs').hide();
	$('#taxtypeGridTabs').hide();
	//$('#ACHTemplateGridTabs').hide();
	$('#achCalendar').hide()
};

ns.ach.taxFormTypeChange = function() {
	var selectedTaxFormvalue = $("#entryTaxFormsID").val();

	$
			.ajax({
				url : "/cb/pages/jsp/ach/editTaxChildACHEntryAction_entryTaxFormID.action",
				type : "POST",
				async : false,
				data : {
					EntryTaxFormsID : selectedTaxFormvalue,
					CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
				},
				success : function(data) {
					// Show ACH Entry details.
					ns.ach.showACHEntryDetails(data);
				}
			});
};

ns.ach.backToACHMultiBatch = function() {
	var urlString = "/cb/pages/jsp/ach/achmultibatchaddedit.jsp";
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#inputDiv').html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
};

ns.ach.addNewBatch = function() {
	var urlString = "/cb/pages/jsp/ach/editMultiACHTemplateAction_addbatch.action";
	$.ajax({
		url : urlString,
		type : "post",
		data : "NewBatchType=" + $("#newBatchTypeId").val() + "&"
				+ $("#AddBatchFormId").serialize(),
		success : function(data) {
			$('#inputDiv').html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
};


ns.ach.importACHBatch = function() {
	if($('#strutsActionName').val()=='addACHBatch' || $('#strutsActionName').val()=='editACHBatch')
	{
	ns.common.refreshDashboard('dbACHSummary');
	ns.common.selectDashboardItem('addSingleACHBatchLink');
	}
	else
	{
	ns.common.refreshDashboard('dbTemplateSummary');
	ns.common.selectDashboardItem('addSingleTemplateLink');
	}
	
	
	
	var urlString ='/cb/pages/jsp/ach/'+ $("#strutsActionName").val() + '_initACHBatchImport.action';
	
	var tokenString=$("[name='CSRF_TOKEN']").val();
	$.ajax({
		type : "POST",
		url : urlString,
		data : {
			CSRF_TOKEN : tokenString
		},
		success : function(data) {
			$("#inputDiv").html(data);
			ns.ach.refreshACHEntries();
		}
	});
};



ns.ach.refreshACHEntries = function(){
	var showErrors = jQuery('#achEntriesGridId').hasClass("ShowErrors");
	if (showErrors
			&& jQuery('#achEntriesGridId').jqGrid('getGridParam', 'records') <= 1) {
		showErrors = false;
		jQuery('#achEntriesGridId').removeClass("ShowErrors");
	}
	var type = $("#achType").val();
	var postData = "curPage="
		+ jQuery('#achEntriesGridId').jqGrid('getGridParam', 'page')
		+ "&curRowNum="
		+ jQuery('#achEntriesGridId').jqGrid('getGridParam', 'rowNum')
		+ "&ShowErrors=" + showErrors + "&achType="+type;
	var actionUrl = "/cb/pages/jsp/ach/reCalculateEntryTotal.action";
	
	$.ajax({
		url : actionUrl,
		data: postData,
		success : function(data) {
			// Hide Add ACHEntry screen
			$("#achAddEditEntrySection").hide();
			$("#achAddEditEntrySection").empty();

			// Show AddACHBatch scren screen again.
			$("#inputDiv").show();

			// reload the grid
			$('#achEntryDetails').html(data);

            // Sometimes changes in a taxform will change the
            // ACHBatch CoEntryDesc and/or Company Descretionary Data
            if ($("#TaxFormChangedCoDiscretionaryDataSpan").length > 0)
            {
                var coDiscretionaryData = ($("#TaxFormChangedCoDiscretionaryDataSpan").html()).trim();
                $("#achBatchDescretionaryData").val(coDiscretionaryData);
                $("#coDiscretionaryDataError").html("The Discretionary Data has changed because of the taxform selected.");
            } else $("#coDiscretionaryDataError").empty();
            if ($("#TaxFormChangedCoEntryDescSpan").length > 0)
            {
                var coEntryDesc = ($("#TaxFormChangedCoEntryDescSpan").html()).trim();
                $("#coEntryDescHidden").val(coEntryDesc);
                $("#coEntryDesc").val(coEntryDesc);
                $("#coEntryDescError").html("The Company Entry Description has changed because of the taxform selected.");
                $('#coEntryDesc').attr('disabled',true);
            } else $("#coEntryDescError").empty();

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
	ns.ach.showInputDivSteps();
};

ns.ach.deleteMultiBatchTemplate = function() {
	var urlString = "/cb/pages/jsp/ach/deleteMultiACHTemplateAction.action";
	$.ajax({
		url : urlString,
		success : function(data) {
			ns.ach.reloadMultipleACHTemplatesSummaryGrid();
			ns.ach.closeACHTabs();
			ns.common.showStatus(data);
		}
	});
};

ns.ach.editACHMultiTemplateBatch = function(urlString) {
	// var urlString =
	// "/cb/pages/jsp/ach/achmultibatchaddeditbatch.jsp?EditBatchID=" + batchID;
	$.ajax({
		url : urlString,
        data : $("#AddBatchFormId").serialize(),
		success : function(data) {
			$('#inputDiv').html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
};

ns.ach.deleteACHMultiTemplateBatch = function(urlString) {
	// var urlString =
	// "/cb/pages/jsp/ach/achmultibatchaddeditbatch.jsp?DeleteBatchID=" +
	// batchID;
	// var urlString =
	// "/cb/pages/jsp/ach/editMultiACHTemplateAction_deletebatch.action";
	$.ajax({
		url : urlString,
        data : $("#AddBatchFormId").serialize(),
		success : function(data) {
			$('#deleteMultiACHTemplateDialogID').html(data).dialog('open');
		}
	});
};

ns.ach.otherTaxAdd = function() {
	var urlString = '/cb/pages/jsp/ach/addChildTaxTypeAction.action';
	$.ajax({
		type : "POST",
		url : urlString,
		data : $("#OtherTaxId").serialize(),
		success : function(data) {
			$("#resultmessage").html(data);
			ns.common.showSummaryOnSuccess('summary','done');
			ns.ach.closeTaxTabs();
		}
	});
};

$.subscribe('mappingListDoneSuccessACH', function(event, data) {
	$('#mappingDiv').hide();
	$('#addCustomMappingsLink').hide();

	// ns.ach.importEntries(ns.ach.importACHBatchEntriesUrl);
	// $('#summary').show();
});

ns.ach.reverse = function(urlString) {
	// var urlString = '/cb/pages/jsp/ach/achbatch-reverse.jsp?Initialize=true';
	$.ajax({
		url : '/cb/pages/jsp/ach/reverseACHBatch_init.action',
		success : function(data) {
			$('#viewACHBatchDetailsDialogID').html(data).dialog('open').css('width','800');
			$('#viewACHBatchDetailsDialogID').parent().css('width','800');
			//ns.common.resizeDialog('viewACHBatchDetailsDialogID');
		}
	});
};

ns.ach.reverseSend = function() {
	if ($("input[id^='hold']:checkbox:checked").length > 0) {
		$('#checkErrorId').html('');
		$.ajax({
			url : '/cb/pages/jsp/ach/reverseACHBatch_execute.action',
			success : function(data) {
				ns.common.closeDialog();
			}
		});
	} else {
		$('#checkErrorId').html(
				'<div style="color:red;">Invalid batch size.</div>');
	}
};

ns.ach.reverseDoCheck = function(setCheckboxVar, activeIDVar) {
	var urlString = '/cb/pages/jsp/ach/reverseACHBatch_makeEntryActiveInActive.action';
	$.ajax({
		// QTS679726
		url : urlString,
		type : "POST",
		data : {
			entryId : activeIDVar,
			active : setCheckboxVar,
			CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
		},
		success : function(data) {
			// ns.common.closeDialog();
		}
	});
};
/**
 * hide Custom Mapping button.
 */
ns.ach.hideCustomMappingButton = function() {
	//$('#customMappingsLink').attr('style', 'display:none');
	$('#customMappingsLink').hide();
};

$.subscribe('achCustomMappingsOnCompleteTopics', function(event,data) {
	gotoStepInCustomMapping(1);
	$('#addCustomMappingsLink').removeAttr('style');
	$('#summary').hide();
	$('#fileupload').hide();
    $('#mappingDiv').show();
    
	$("#detailsPortlet").hide();
	$("#customMappingDetails").hide();
	$("#mappingDiv").show();
	
	$.publish("common.topics.tabifyDesktop");
	
	var detailsTabIndex = ns.common.getSectionTabIndex("details");
	
	$("#customMappingDetails").tabify({baseIndex:detailsTabIndex})
	if(accessibility){
		accessibility.setFocusOnDashboard();
	}
});

$.subscribe('achAddCustomMappingsOnCompleteTopics', function(event,data) {
	gotoStepInCustomMapping(1);
	var urlString = "/cb/pages/jsp/custommapping.jsp";
	var MappingDefinitionIDVar="0";
	$.ajax({
		url: urlString,
		type:"POST",
		data: {MappingDefinitionID: MappingDefinitionIDVar, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		success: function(data){
			$('#inputCustomMappingDiv').html(data);
			$.publish("common.topics.tabifyDesktop");
			
			var detailsTabIndex = ns.common.getSectionTabIndex("details");
			
			$("#customMappingDetails").tabify({baseIndex:detailsTabIndex})
			if(accessibility){
				$("#customMappingDetails").setInitialFocus();
			}
		}
	});
	$('#customMappingDetails').show();
	$('#mappingDiv').hide();
	$("#detailsPortlet").hide();
});

/**
 * 
 * @param stepnum
 * @return No return value
 */
function gotoStepInCustomMapping(stepnum){
	var tabindex = stepnum - 1;
	$('#customMappingWizardTabs').tabs( 'enable', tabindex );
	$('#customMappingWizardTabs').tabs( 'option','active', tabindex );
	$('#customMappingWizardTabs').tabs( 'disable', (tabindex+1)%3 );
	$('#customMappingWizardTabs').tabs( 'disable', (tabindex+2)%3 );
	
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#customMappingDetails").setInitialFocus();
	}
}

ns.ach.continueEditPendingRec = function() {
	// QTS 725881: Don't CLOSE DIALOG until after we serialize the data
	// ns.common.closeDialog('#editPendingACHBatchDialogID');
	$.publish('beforeLoadACHForm');
	var urlString = "/cb/pages/jsp/ach/editACHBatch_init.action";
	$.ajax({
		type : "POST",
		url : urlString,
		data : $("#EditACHInstanceForm").serialize(),
		success : function(data) {
			ns.common.closeDialog('#editPendingACHBatchDialogID');
			$.publish('loadACHFormComplete');
			$('#inputDiv').html(data);
		}
	});

};

ns.ach.continueDeletePendingRec = function() {
	var urlString = '/cb/pages/jsp/ach/deleteACHBatch_init.action';
	$.ajax({
		type : "POST",
		url : urlString,
		data : $("#deleteACHInstanceForm").serialize(),
		success : function(data) {
			$("#deleteACHBatchDialogID").bind("dialogbeforeclose",
					function(event, ui) {
						$.publish('reloadPendingACHBatchSummaryGrid');
					});
			$('#deleteACHBatchDialogID').html(data).dialog('open');
			ns.common.resizeDialog('deleteACHBatchDialogID');
		}
	});
};

ns.ach.recheckValidatonMultiTemplate = function() {
	var urlString = '/cb/pages/jsp/ach/editMultiACHTemplateAction_recheckValidation.action';
	$.ajax({
		type : "POST",
		url : urlString,
		data : $("#AddBatchFormId").serialize(),
		success : function(data) {
			$("#inputDiv").html(data);

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
};

/**
 * Revert Child,Tax Entry.
 */
ns.ach.revertChildTaxEntry = function() {
	var urlString = '/cb/pages/jsp/ach/editTaxChildACHEntryAction_revertChildTaxEntry.action';
	$.ajax({
		type : "POST",
		url : urlString,
		data : $("#TaxPayFormId").serialize(),
		async : false,
		success : function(data) {
			$('#achAddEditEntrySection').empty();
			$('#achAddEditEntrySection').html(data);
		}
	});
};

ns.ach.removeAddEditACHTemplateObject = function() {
	$.ajax({
		url : "/cb/pages/jsp/ach/removeachtemplateobject.jsp",
		type : "GET",
		success : function(data) {
			ns.common.closeDialog();
		}
	});
};

$.subscribe('completeConfirmUploadedAchEntryProcess', function(event, data) {
	$.log('completeConfirmUploadedAchEntryProcess ');
});

$.subscribe('successConfirmUploadedAchEntryProcess', function(event, data) {
	$.log('successConfirmUploadedAchEntryProcess ');
});

$.subscribe('errorConfirmUploadedAchEntryProcess', function(event, data) {
	$.log('errorConfirmUploadedAchEntryProcess ');
});

$.subscribe('beforeConfirmUploadedAchEntryProcess', function(event, data) {
	$.log('beforeConfirmUploadedAchEntryProcess ');
});

$(document).ready(function() {
	// jQuery.LINT.level = 1;
	$.debug(false);
	ns.ach.setup();
	ns.ach.achbatch_setup();
	ns.ach.achpayee_setup();

});

ns.ach.enableDisableSelectMenu = function (menuId, isdisabled, width){
	var selectMenuObj = $("#"+menuId);
	if(selectMenuObj.length > 0 && selectMenuObj.selectmenu('widget')){
		selectMenuObj.selectmenu('destroy');
		selectMenuObj[0].disabled = isdisabled;
		selectMenuObj.selectmenu({width: width});
	}
};

/* ACH module specific method to show validation errors */
ns.ach.batchCustomValidation = function(form, errors) {
	customValidation(form ,errors);
	
	// Check if error contains effectiveDate
	if(errors.fieldErrors){
		if(errors.fieldErrors.hasOwnProperty("date")){
			$.get("/cb/pages/jsp/ach/inc/achBatchEffectiveDate.jsp", function(data){
				$("#effectiveDateDiv").html(data);
			});
		}
		
		if(errors.fieldErrors.hasOwnProperty("ACHEntry")){
			$.get("/cb/pages/jsp//ach/achbatchaddeditentrygrid.jsp", function(data){
				$("#achEntryGridDiv").html(data);
			});
		}
	}
};

$.subscribe('customMappingBefore', function(event,data) {
	removeValidationErrors();	
});	

ns.ach.achFileUpload = function() {
	ns.ach.hideCustomMappingButton();
};

$.subscribe('achmultibatchCustomValidation', function(event, data) {
	var urlStr = "/cb/pages/jsp/ach/achmultibatchaddedit.jsp";
    $.ajax({
        url: urlStr,
        success: function(data){
            $('#inputDiv').html(data);
        }
    });
});


//Show the active dashboard based on the user selected summary
ns.ach.showACHDashboard = function(achUserActionType,achDashboardElementId){
	if(achUserActionType == 'ACHBatch'){
		ns.common.refreshDashboard('dbACHSummary');	
		$('#customMappingDetails').hide();
	}else if(achUserActionType == 'ACHBatchTemplate'){
	
		ns.common.refreshDashboard('dbTemplateSummary');
		$('#customMappingDetails').hide();
	}
	else if(achUserActionType == 'ACHPayee'){
		ns.common.refreshDashboard('dbPayeeSummary');
		$('#customMappingDetails').hide();
	}
	else if(achUserActionType == 'ACHFileUpload'){
		ns.common.refreshDashboard('dbFileUploadSummary');
		$('#customMappingDetails').hide();
	}
	else{//In case of normal Transfer top menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbACHSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(achDashboardElementId && achDashboardElementId !='' && $('#'+achDashboardElementId).length > 0){
		//$("#"+transferDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+achDashboardElementId).trigger("click");}, 10);
	}
};