$.subscribe('pendingApprovalACHGridCompleteEvents',
		function(event, data) {
			// Hide "frequency" column in tax module
			if ($("li[menuId='pmtTran_tax']").hasClass('ui-state-active')) {
				jQuery("#pendingApprovalACHID").jqGrid('hideCol', "frequency");
			}
			ns.ach.handleGridEventsAndSetWidth("#pendingApprovalACHID",
					"#ACHGridTabs");
			// Add additional row(approval information)
			ns.ach.addApprovalsRows("#pendingApprovalACHID");
		});

$.subscribe('pendingACHGridCompleteEvents', function(event, data) {
	// Hide "frequency" column in tax module
	if ($("li[menuId='pmtTran_tax']").hasClass('ui-state-active')) {
		jQuery("#pendingACHGridID").jqGrid('hideCol', "frequency");
	}

	ns.ach.handleGridEventsAndSetWidth("#pendingACHGridID", "#ACHGridTabs");
});

$.subscribe('closeImportACHFilePageTopics', function(event, data) {
	$("#detailsPortlet").hide();
});

$.subscribe('ACHEntryGridCompleteEvents', function(event, data) {
	prepareFooterOnLoad();
	ns.ach.handleEntryGridEvents("#achEntriesGridId", false);
	ns.ach.handleGridEventsAndSetWidth("#achEntriesGridId", "#ACHGridTabs");
	ns.ach.addEntryExtraRows("#achEntriesGridId");
	// QTS 719133: When viewing the ACH Batch EDIT screen, if the ACH Entries
	// window is large (20 entries),
	// and the user clicks the NEXT PAGE button, scroll back to the top of the
	// entrys window so that
	// the first row in the next page displays.
	var $result = $('#achEntryGridDiv');
	ns.common.scrollTo($result);

	//Enable disable ach screen elements
	ns.ach.enableDisableACHAddEditScreenElements();

	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		$("#ACHTransactionWizardTabs").setFocusOnTab();
	}
});

function prepareFooterOnLoad(){
	// set as footer
	// below names achPayee.nameSecure,achPayee.accountNumberSecure,amount & amountIsDebit are used because they
	// are permanent columns. They have no relation to the value shown at there footer. It is just for display purpose
	$.ajax({
		type : "POST",
		url : '/cb/pages/jsp/ach/achbatchaddeditentryheader.jsp',
		success : function(data) {
			var $data = $(data);
			var TotalNumberCredits = $data.find("#TotalNumberCredits").html();
			var TotalCreditAmount = $(data).find("#TotalCreditAmount").html();
			var TotalNumberDebits = $(data).find("#TotalNumberDebits").html();
			var TotalDebitAmount = $(data).find("#TotalDebitAmount").html();
			$('#achEntriesGridId').jqGrid('footerData','set',
					{
					'achPayee.accountNumberSecure':TotalCreditAmount,
					'amount':TotalNumberDebits,
					'amountIsDebit':TotalDebitAmount}, false);
			// below condition is added to handle UI case so that the footer looks align
			// for XCK and other ACH type where Participant Name doesn't suit alignment we use itemResearchNumber
			var achPayeeAccountNumberSecure = $("#achEntriesGridId").jqGrid('getGridParam','colNames').indexOf('Participant Name');
			var achPayeeAccountNumberSecure2 = $("#achEntriesGridId").jqGrid('getGridParam','colNames').indexOf('Receiver Name');
			if(achPayeeAccountNumberSecure!='-1' || achPayeeAccountNumberSecure2 !='-1'){
				$('#achEntriesGridId').jqGrid('footerData','set',{'achPayee.nameSecure':TotalNumberCredits},false);
			}else{
				$('#achEntriesGridId').jqGrid('footerData','set',{'itemResearchNumber':TotalNumberCredits},false);
			}
		}
	});
}

ns.ach.enableDisableACHAddEditScreenElements = function(){
	var records = $('#achEntriesGridId').jqGrid('getGridParam', 'records');
	var isEdit = $("#isEdit");
	if(records > 0){
		//If ACHEntry Records available then disable
		ns.ach.enableDisableSelectMenu("AddEditACHBatchCompanyID", true, $( "#AddEditACHBatchCompanyID" ).selectmenu( "option", "width" ));
		ns.ach.enableDisableSelectMenu("standardEntryClassCodeId", true, $( "#standardEntryClassCodeId" ).selectmenu( "option", "width" ));
		ns.ach.enableDisableSelectMenu("defaultTaxFormID", true, 250);

		ns.ach.enableDisableSelectMenu("FOREIGNEXCHANGE", true, 160);
		ns.ach.enableDisableSelectMenu("countryID", true, 160);
		ns.ach.enableDisableSelectMenu("CURRENCY", true, 160);
		ns.ach.enableDisableSelectMenu("paymentTypeID", true, 120);

		//Now, if records available turn on following controls.
		$("#setAllMountsButtonId").button({ disabled: false });
		$("#importAmmounts").button({ disabled: false });
		//document.getElementById("holdAllId").disabled = false;
	} else {
		//If ACHEntry records not available then enable the controls
		if(isEdit.length <= 0){
			ns.ach.enableDisableSelectMenu("AddEditACHBatchCompanyID", false, $( "#AddEditACHBatchCompanyID" ).selectmenu( "option", "width" ));
		}
		ns.ach.enableDisableSelectMenu("standardEntryClassCodeId", false, $( "#standardEntryClassCodeId" ).selectmenu( "option", "width" ));
		var companyID = $("#AddEditACHBatchCompanyID").val();
		if(companyID == "" || companyID == "-1")
		{
			ns.ach.enableDisableSelectMenu("defaultTaxFormID", true, 370);
		}
		else
		{
		ns.ach.enableDisableSelectMenu("defaultTaxFormID", false, 370);
		}
		ns.ach.enableDisableSelectMenu("FOREIGNEXCHANGE", false, 160);
		ns.ach.enableDisableSelectMenu("countryID", false, 160);
		ns.ach.enableDisableSelectMenu("CURRENCY", false, 160);
		ns.ach.enableDisableSelectMenu("paymentTypeID", false, 120);

		$("#setAllMountsButtonId").button({ disabled: true });
		$("#importAmmounts").button({ disabled: true });
		//document.getElementById("holdAllId").disabled = true;
	}
};

$.subscribe('pendingApprovalACHPayeeGridCompleteEvents',
				function(event, data) {
					ns.ach.handleGridEventsAndSetWidth(
							"#pendingApprovalACHPayeeGridId",
							"#ach_payeesPendingApprovalTab");
					// Add additional row(approval/rejection information)
					ns.ach
							.addRejectReasonRowsForPendingACHPayee("#pendingApprovalACHPayeeGridId");
				});

$.subscribe('ACHEntryGridPagingTopicEvents', function(event, data) {
	var strutsActionName = "/cb/pages/jsp/ach/" + $("#strutsActionName").val()
	+ '_saveUserInput.action';

	$.ajax({
		type : "POST",
		url : strutsActionName,
		data : $("#AddBatchNew").serialize(),
		async : false,
		success : function(data) {
			/*// reload the Entry Grid DIV
			$.get("/cb/pages/jsp/ach/achbatchaddeditentryheader.jsp", function(
					data) {
				$('#achEntryHeaderDiv').html(data);
			});*/
		}
	});
});

$.subscribe('standardACHPayeeGridCompleteEvents', function(event, data) {
	ns.ach.handleGridEventsAndSetWidth("#standardACHPayeeGridId",
			"#ACHPayeeGridTabs");
});

$.subscribe('allACHPayeeGridCompleteEvents', function(event, data) {
	ns.ach.handleGridEventsAndSetWidth("#allACHPayeeGridId");
});

$.subscribe('pendingApprovalACHManagedPayeeGridCompleteEvents',
				function(event, data) {
					// Add additional row(approval/rejection information)
					ns.ach
							.addRejectReasonRowsForPendingACHPayee("#pendingApprovalManagedACHPayeeGridId");
				});

$.subscribe('IATACHPayeeGridCompleteEvents', function(event, data) {
	ns.ach.handleGridEventsAndSetWidth("#IATACHPayeeGridId",
			"#ACHPayeeGridTabs");
});

$.subscribe('completedACHGridCompleteEvents', function(event, data) {
	// Hide "frequency" column in tax module
	if ($("li[menuId='pmtTran_tax']").hasClass('ui-state-active')) {
		jQuery("#completedACHGridID").jqGrid('hideCol', "frequency");
	}
	ns.ach.handleGridEventsAndSetWidth("#completedACHGridID", "#ACHGridTabs");
});

ns.ach.handleGridEventsAndSetWidth = function(gridID, tabsID) {
	// Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
};
$.subscribe('cancelSingleACHBatchComplete', function(event, data) {
	ns.common.closeDialog("#deleteACHBatchDialogID");
});

// After the ACH is deleted, the grid summary should be reloaded.
$.subscribe('cancelACHSuccessTopics', function(event, data) {
	$.publish("reloadSelectedACHGridTab");
	if (data.innerHTML != "") {
		ns.common.showStatus();
	} else {
		ns.common.showStatus($("#deleteBatchMessageId").val());
	}
});

$.subscribe('cancelACHErrorTopics', function(event, data) {
	ns.common.showError();
});

$.subscribe('cancelMultipleACHTemplateComplete', function(event, data) {
	$('#multipleACHTemplatesGridID').trigger("reloadGrid");
	ns.common.showStatus();
});

$.subscribe('cancelMultipleACHTemplateSuccessTopics', function(event, data) {
	ns.ach.reloadMultipleACHTemplatesSummaryGrid();
});

$.subscribe('sendInquiryACHFormSuccess', function(event, data) {
	ns.common.closeDialog("#inquiryACHBatchDialogID");
	// Display the result message on the status bar.
	ns.common.showStatus();
	$.publish('refreshMessageSmallPanel');
});

$.subscribe('singleACHTemplatesGridCompleteEvents', function(event, data) {
	// Adjust the width of grid automatically.
	ns.ach.handleGridEventsAndSetWidth("#singleACHTemplatesGridID",
			"#templateGridTabs");
	ns.ach.addApprovalsRowsForTemplate("#singleACHTemplatesGridID");
    //$("#templatessearchcriteria").show();
    //$("#multitemplatessearchcriteria").hide();
});

$.subscribe('multipleACHTemplatesGridCompleteEvents', function(event, data) {
	// Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
	ns.ach.addApprovalsRowsForTemplate("#multipleACHTemplatesGridID");
	//$("#templatessearchcriteria").hide();
    //$("#multitemplatessearchcriteria").show();
});

// After the ACH templates is deleted, the grid summary should be reloaded.
$.subscribe('cancelSingleACHTemplateSuccess', function(event, data) {
	ns.ach.reloadSingleACHTemplatesSummaryGrid();
	if (data.innerHTML != "") {
		ns.common.showStatus();
	} else {
		ns.common.showStatus($("#deleteBatchMessageId").val());
	}
});

$.subscribe('cancelSingleACHTemplateComplete', function(event, data) {
	ns.common.closeDialog('#deleteACHTemplateDialogID');
});

$.subscribe('cancelSingleACHTemplateError', function(event, data) {
	ns.common.showError();
});

$.subscribe('ACHEntryConfirmGridCompleteEvents', function(event, data) {
	ns.ach.handleEntryGridEvents("#achEntriesConfirmGridId", true);
	// ns.common.resizeWidthOfGrids();
	ns.ach.handleGridEventsAndSetWidth("#achEntriesConfirmGridId",
			"#ACHGridTabs");
	ns.ach.addEntryExtraRowsConfirm("#achEntriesConfirmGridId");
});

$.subscribe('reloadPendingACHBatchSummaryGrid', function(event, data) {
	$('#pendingACHGridID').trigger("reloadGrid");
});

$.subscribe('triggerHiddenHoldAllIdButton', function(event, data) {
	$('#holdAllId').trigger('click');
});


$.subscribe('triggerRowSelection', function(event, data) {
	$('#hold'+event.originalEvent.rowid).trigger('click');
});

$.subscribe('skipSingleACHBatchComplete', function(event, data) {
	ns.common.closeDialog("#skipACHBatchDialogID");
});

//After the ACH is skipped, the grid summary should be reloaded.
$.subscribe('skipACHSuccessTopics', function(event, data) {
	$.publish("reloadSelectedACHGridTab");
	if (data.innerHTML != "") {
		ns.common.showStatus();
	} else {
		ns.common.showStatus($("#skipBatchMessageId").val());
	}
});

ns.ach.handleEntryGridEvents = function(gridId, isDisable) {
	var ids = jQuery(gridId).jqGrid('getDataIDs');
	var isTaxChild = $(gridId).hasClass("TaxChild");
	var currentPageSize = $(gridId).jqGrid("getGridParam","rowNum");
	var currentPageNumber = $(gridId).jqGrid("getGridParam","page");
	var entryIndex = currentPageSize *(currentPageNumber - 1);//Get starting index.

	var parentCheckBoxFlag = "";
	for ( var i = 0; i < ids.length; i++, entryIndex++) {
		var rowId = ids[i];
		var id = $(gridId).jqGrid('getCell', rowId, 'ID');
		var active = $(gridId).jqGrid('getCell', rowId, 'active');
		var isDebit = $(gridId).jqGrid('getCell', rowId, 'amountIsDebit');
		var curAmount = $(gridId).jqGrid('getCell', rowId,
				'amountValue.currencyStringNoSymbol');
		var prevAmount = curAmount;
		var act = "";
		var disable = "";
		var disableAmount = "";
		var confirm = "";
		var crdrstr = "Debit";
		if (active == "false") {
			act = "checked";
			prevAmount = "0.00"; // don't include an amount if inactive
			$("#achEntriesGridId").jqGrid('setSelection',rowId,true);
		}else{
			parentCheckBoxFlag+=1;
		}
		if (isDebit == "Credit") {
			crdrstr = "Credit";
		}

		if (isTaxChild) {
			disableAmount = " disabled ";
		}

		if (isDisable) {
			disable = " disabled ";
			disableAmount = " disabled ";
			confirm = "confirm";
		}

		// HOLD check-box should never be disabled. If you HOLD ALL, you should
		// then be able to unhold one of the entries.
		var checkboxInput = "<input id=\"hold"
				+ confirm
				+ rowId
				+ "\" "
				+ disable
				+ " type=\"checkbox\" class=\"hidden\" name=\"checkbox\" onclick=\"changeHold(this,'"
				+ rowId + "');\" " + act + ">";
		var hiddenInput = "<input id=\"active" + confirm + rowId
				+ "\" type=\"hidden\"  name=\"AddEditACHBatch.ACHEntries["+ entryIndex +"].Active\" value=\"" + active	+ "\">";
		var crdrInput = "<input type=\"hidden\" id=\"crdr" + confirm + rowId
				+ "\"" + disable + " name=\"crdr" + id + "\" value='"
				+ prevAmount + " " + crdrstr + "'>";
		var amountInput = "<input type=\"text\" id=\"amount"
				+ confirm
				+ rowId
				+ "\""
				+ disableAmount
				+ " class=\"ui-widget-content\" name=\"AddEditACHBatch.ACHEntries["+ entryIndex +"].Amount\" value=\""+ curAmount+ "\" size=\"11\" " +
						"maxlength=\"11\" onchange=\"ns.ach.updateAchBatchEntryAmount('"+ rowId + "', 'amount" + confirm + rowId + "','"+ id+"');\">";

		if (confirm == "") {
			$("#" + id).html(checkboxInput + hiddenInput + crdrInput);
			$("#amount" + id).html(amountInput);
		} else {
			/*$("#confirm" + id).html(checkboxInput + hiddenInput + crdrInput);*/
			$("#confirmAmount" + id).html(amountInput);
		}
	}

	// Set the Hold Entries check box based on count of Total # Credits and
	// Total # Debits. (Ref: QTS#721023)
	if (ids.length > 0) {
		if ($('[name=TotalNumberCredits]').val() == "0"
				&& $('[name=TotalNumberDebits]').val() == "0") {
			$('#holdAllId').attr("checked", true);
			$('#cb_achEntriesGridId').attr("checked",true);
		} else {
			$('#cb_achEntriesGridId').attr("checked",false);
			$('#holdAllId').attr("checked", false);
		}
	} else {
		$('#holdAllId').attr("checked", false);
	}


	if(ids.length>0){
		if(parentCheckBoxFlag==''){
			$('#cb_achEntriesGridId').attr("checked",true);
		}else{
			$('#cb_achEntriesGridId').attr("checked",false);
		}
	}else{
		$('#cb_achEntriesGridId').attr("checked",false);
	}

};

ns.ach.updateAchBatchEntryAmount = function(rowId, entTxtBoxId, entryId) {
	var entryAmountVar = $('#' + entTxtBoxId).val();
	var urlStringVar = "/cb/pages/jsp/ach/getACHEntriesAction_setEntryAmount.action";
	$.ajax({
		url : urlStringVar,
		async : false,
		type : "POST",
		data : {
			'AddEditACHBatch.CurrentEntry' : entryId,
			'AddEditACHBatch.EntryAmount' : entryAmountVar,
			CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
		},
		success : function(data) {
			$('#' + entTxtBoxId).val(data);
			ns.ach.totalThisAmount(rowId);
		}
	});
}


ns.ach.formatSingleTemplateApproversInfo = function(cellvalue, options, rowObject) {
	var approverNameInfo="";
	var approverNameOrUserName="";
	var approverGroup="";
	var approverInfos=rowObject.fundsTransaction.approverInfos;
		if(approverInfos!=undefined)
		{
			for(var i=0;i<approverInfos.length;i++){
			var approverInfo = approverInfos[i];
			if(i!=0)
			{
			approverNameInfo="," + approverNameInfo
			}

			var approverName = approverInfo.approverName;
			var approverIsGroup=approverInfo.approverIsGroup;

			if( approverIsGroup){
				approverGroup = "(" + js_group + ")";
			}
			approverNameOrUserName = approverName + approverGroup;
			approverNameInfo=approverNameOrUserName+approverNameInfo;
			}
		}
		return approverNameInfo;
};

ns.ach.formatApproversInfo = function(cellvalue, options, rowObject) {
	var approverNameInfo="";
	var approverNameOrUserName="";
	var approverGroup="";
	var approverInfos=rowObject.approverInfos;
		if(approverInfos!=undefined)
		{
			for(var i=0;i<approverInfos.length;i++){
			var approverInfo = approverInfos[i];
			if(i!=0)
			{
			approverNameInfo="," + approverNameInfo
			}

			var approverName = approverInfo.approverName;
			var approverIsGroup=approverInfo.approverIsGroup;

			if( approverIsGroup){
				approverGroup = "(" + js_group + ")";
			} else {
				approverGroup = "";
			}
			approverNameOrUserName = approverName + approverGroup;
			approverNameInfo=approverNameOrUserName+approverNameInfo;
			}
		}
		return approverNameInfo;
};


ns.ach.formatPendingApprovalsACHActionLinks = function(cellvalue, options,
		rowObject) {
	// cellvalue is the ID of the ACH.
	var recId = rowObject.recID;
	var transactionType = rowObject.transactionType;
	var isRecModel = false;
	var ACHType = rowObject.ACHType;
	var view = "<a title='" + js_view
			+ "' href='#' onClick='ns.ach.viewACHBatchDetails(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var editURL = rowObject.map.EditURL;
	var deleteURL = rowObject.map.DeleteURL;
	var skipURL = rowObject.map.SkipURL;
	if (transactionType == 'ACH' && recId != '' && false) {
		editURL = rowObject.map.EditRecURL;
		deleteURL = rowObject.map.DeleteRecURL;
		isRecModel = true;
	}
	var edit = "<a title='" + js_edit
			+ "' href='#' onClick='ns.ach.editPendingACHBatch(\"" + editURL
			+ "\", \"" + transactionType + "\", \"" + recId + "\",\"" + isRecModel
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a title='" + js_delete
			+ "' href='#' onClick='ns.ach.deletePendingACHBatch(\"" + deleteURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	var skip = "<a title='" + js_skip
	+ "' href='#' onClick='ns.ach.skipPendingACHBatch(\"" + skipURL
	+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall  ffiUiIco-icon-skipRecurring'></span></a>";
	// Tax and Child Support do not support Recurring INSTANCE EDIT/DELETE
	if (ACHType != "ACHBatch") {
		edit = "<a title='" + js_edit
				+ "' href='#' onClick='ns.ach.editSingleACHBatch(\""
				+ rowObject.map.EditURL
				+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		del = "<a title='" + js_delete
				+ "' href='#' onClick='ns.ach.deleteACHBatch(\""
				+ rowObject.map.DeleteURL
				+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	}
	// var ACHId = $('#pendingApprovalACHID').jqGrid('getCell', rowId, 'ID');
	var inquiry = "<a title='" + js_inquiry
			+ "' href='#' onClick='ns.ach.inquireTransaction(\""
			+ rowObject.map.InquireURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

	if (rowObject["canEdit"] == "false") {
		edit = "";
	}
	if (rowObject["canDelete"] == "false") {
		del = "";
	}
	if (rowObject["canSkip"] == "false" || rowObject["recModel"] == true) {
		skip = "";
	}
	if (rowObject.canView == "false") {
		view = "";
	}
	// if resultOfApproval="Deletion", and status didnot equal "10", hide
	// deletion and edit icons
	if (rowObject["resultOfApproval"] == "Deletion"
			&& rowObject["batchStatus"] != "10") {
		del = "";
		edit = "";
		skip = "";
	}
	if (rowObject["resultOfApproval"] == "Skip") {
		skip = "";
	}
	if (rowObject.map.canInquire == "false") {
		inquiry = "";
	}

	return ' ' + view + ' ' + edit + ' ' + del + ' ' + skip + ' ' + inquiry;
};

ns.ach.formatPendingACHActionLinks = function(cellvalue, options, rowObject) {
	// cellvalue is the ID of the ACH.
	var recId = rowObject.recID;
	var transactionType = rowObject.transactionType;
	var isRecModel = false;
	var ACHType = rowObject.ACHType;
	var view = "<a title='" + js_view
			+ "' href='#' onClick='ns.ach.viewACHBatchDetails(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var editURL = rowObject.map.EditURL;
	var deleteURL = rowObject.map.DeleteURL;
	var skipURL = rowObject.map.SkipURL;
	if (transactionType == 'ACH' && recId != '' && false) {
		editURL = rowObject.map.EditRecURL;
		deleteURL = rowObject.map.DeleteRecURL;
		isRecModel = true;
	}
	var edit = "<a title='" + js_edit
			+ "' href='#' onClick='ns.ach.editPendingACHBatch(\"" + editURL
			+ "\", \"" + transactionType + "\", \"" + recId + "\",\"" + isRecModel
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a title='" + js_delete
			+ "' href='#' onClick='ns.ach.deletePendingACHBatch(\"" + deleteURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	var skip = "<a title='" + js_skip
	+ "' href='#' onClick='ns.ach.skipPendingACHBatch(\"" + skipURL
	+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall  ffiUiIco-icon-skipRecurring'></span></a>";
	// QTS 664609: Tax and Child Support do not support Recurring INSTANCE
	// EDIT/DELETE
	if (ACHType != "ACHBatch") {
		edit = "<a title='" + js_edit
				+ "' href='#' onClick='ns.ach.editSingleACHBatch(\""
				+ rowObject.map.EditURL
				+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		del = "<a title='" + js_delete
				+ "' href='#' onClick='ns.ach.deleteACHBatch(\""
				+ rowObject.map.DeleteURL
				+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	}
	// var ACHId = $('#pendingApprovalACHID').jqGrid('getCell', rowId, 'ID');
	var inquiry = "<a title='" + js_inquiry
			+ "' href='#' onClick='ns.ach.inquireTransaction(\""
			+ rowObject.map.InquireURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

	if (rowObject["canEdit"] == "false") {
		edit = "";
	}
	if (rowObject["canDelete"] == "false") {
		del = "";
	}
	if (rowObject["canSkip"] == "false") {
		skip = "";
	}
	if (rowObject.canView == "false") {
		view = "";
	}
	if (rowObject.map.canInquire == "false") {
		inquiry = "";
	}

	return ' ' + view + ' ' + edit + ' ' + del + ' ' + skip + ' ' + inquiry;
};

ns.ach.formatHoldLinks = function(cellvalue, options, rowObject) {
	var ele = "<div style=\"margin-top: 0px;\" valign=\"middle\" align=\"center\" id=\""
			+ rowObject.ID + "\"/>";
	return ele;
};

ns.ach.formatConfirmHoldLinks = function(cellvalue, options, rowObject) {
	var ele = "<div style=\"margin-top: 0px;\" valign=\"middle\" align=\"center\" id=\"confirm"
			+ rowObject.ID + "\"/>";
	return ele;
};

ns.ach.formatDebitLinks = function(cellvalue, options, rowObject) {
	if (rowObject.amountIsDebit == true) {
		return "Debit"
	} else {
		return "Credit";
	}
};

ns.ach.formatAmountLinks = function(cellvalue, options, rowObject) {
	var ele = "<div style=\"margin-top: 0px\" valign=\"left\" align=\"left\" id=\"amount"
			+ rowObject.ID + "\"/>";
	return ele;
};

ns.ach.formatConfirmAmountLinks = function(cellvalue, options, rowObject) {
	var ele = "<div style=\"margin-top: 0px;\" valign=\"middle\" align=\"center\" id=\"confirmAmount"
			+ rowObject.ID + "\"/>";
	return ele;
};

ns.ach.formatACHEntryActionLinks = function(cellvalue, options, rowObject) {
	// cellvalue is the ID of the ACH.
	var edit = "<a class='' title='" + js_edit
			+ "' href='#' onClick='ns.ach.editACHEntry(\"" + rowObject.ID
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a class='' title='" + js_delete
			+ "' href='#' onClick='ns.ach.deleteACHEntry(\"" + rowObject.ID
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	var errors = " ";
	if (rowObject.hasValidationErrors == "true") {
		errors = " <a class='' title='" + js_errors
				+ "' href='#' onClick='ns.ach.showErrorsACHEntry(\""
				+ rowObject.ID
				+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall  ffiUiIco-icon-error'></span></a>";
	}
	if (rowObject.map.CanEdit == "false") {
		edit = "";
	}
	if (rowObject.map.CanDelete == "false") {
		del = "";
	}

	var symbol = "";
	if (rowObject.action == 'ADDED') {
		symbol = '+';
	}

	if (rowObject.action == 'MATCHED') {
		symbol = '*';
	}
	return edit + del + errors + symbol;
};

ns.ach.formatACHEntryDebitCredit = function(cellvalue, options, rowObject) {
	// cellvalue is the true (debit) or false (credit).
	var isCredit = "" + cellvalue;
	if (isCredit == "false") {
		return "Credit";
	}
	return "Debit";
};

ns.ach.setDecimalAmount = function(amount) {
	var amtRound = Math.round(amount * Math.pow(10, 2)) / Math.pow(10, 2)
	var amtStr = "" + amtRound;
	if (amtStr.indexOf('.') == -1) {
		amtStr = amtStr + ".0"
	} // Add .0 if a decimal point doesn't exist
	var temp = (amtStr.length - amtStr.indexOf('.')); // Find out if the #
	// ends in a tenth (ie
	// 145.5)
	if (temp <= 2) {
		amtStr = amtStr + "0";
	} // if it ends in a tenth, add an extra zero to the string
	return amtStr;
};

ns.ach.formatAllACHPayeeActionLinks = function(cellvalue, options, rowObject) {
	// cellvalue is the ID of the ACH.
	var prenote = '<span style="font-weight:bold;color:red;">P</span>';
	var view = "<a class='' title='" + js_view
			+ "' href='#' onClick='ns.ach.viewACHPayeeDetails(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var edit = "<a class='' title='" + js_edit
			+ "' href='#' onClick='ns.ach.editManagedACHPayee(\""
			+ rowObject.map.EditURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var insert = "<a class='ui-button' title='" + js_insert
			+ "' href='#' onClick='ns.ach.insertACHPayee(\""
			+ rowObject.map.InsertURL
			+ "\");'><span class='ui-icon ui-icon-pin-s'></span></a>";
	if (options.colModel.formatoptions.canManageParticipant != 'true'
			|| options.colModel.formatoptions.dualApprovalMode == '1'
			|| rowObject.displayAsSecurePayee == 'true'
			|| rowObject.daStatus == 'Rejected') {
		edit = "";

	}
	if (rowObject["canInsert"] == "false") {
		insert = "";

	}

	if (rowObject["prenoteStatus"] == "PRENOTE_PENDING") {
		return ' ' + prenote + ' ' + view + ' ' + edit + ' ' + insert;
	} else {
		return ' ' + ' ' + ' ' + ' ' + view + ' ' + edit + ' ' + insert;
	}

};

ns.ach.formatPendingApprovalACHPayeeActionLinks = function(cellvalue, options,
		rowObject) {
	// cellvalue is the ID of the ACH.

	var action = '';
	var editURL = rowObject["map"].EditURL;// + "&daItemId="+rowObject.daItemId
	// + "&daAction="+rowObject.action;
	var prenote = '<span style="font-weight:bold;color:red;">P</span>';

	var viewAction = "<a class='' title='" + js_view
			+ "' href='#' onClick='ns.ach.viewACHPayeeDetails(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var editAction = "<a class='' title='" + js_edit
			+ "' href='#' onClick='ns.ach.editACHPayee(\"" + editURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var modifyAction = "<a class='' title='" + js_modify
			+ "' href='#' onClick='ns.ach.modifyACHPayee(\""
			+ rowObject.map.ModifyURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var discardAction = "<a class='' title='" + js_discard
			+ "' href='#' onClick='ns.ach.discardACHPayee(\""
			+ rowObject.map.DiscardURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
	var view = rowObject.view;

	action += viewAction;
	if (options.colModel.formatoptions.canManageParticipant == 'true'
			&& rowObject.displayAsSecurePayee != 'true') {
		var edit = rowObject.edit;
		if (edit != 'undefined' && edit == 'true') {
			action += editAction;
		}
		var modifyChange = rowObject.modifyChange;
		if (modifyChange != 'undefined' && modifyChange == 'true') {
			action += modifyAction;
		}
		var discard = rowObject.discard;
		if (discard != 'undefined' && discard == 'true') {
			action += discardAction;
		}
	}

	if (rowObject["prenoteStatus"] == "PRENOTE_PENDING") {
		return ' ' + prenote + ' ' + action;
	} else {
		return ' ' + ' ' + ' ' + ' ' + action;
	}
};

ns.ach.formatStandardACHPayeeActionLinks = function(cellvalue, options,
		rowObject) {
	// cellvalue is the ID of the ACH.
	var prenote = '<span style="font-weight:bold;color:red;">P</span>';
	var view = "<a class='' title='" + js_view
			+ "' href='#' onClick='ns.ach.viewACHPayeeDetails(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var edit = "<a class='' title='" + js_edit
			+ "' href='#' onClick='ns.ach.editACHPayee(\""
			+ rowObject.map.EditURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a class='' title='" + js_delete
			+ "' href='#' onClick='ns.ach.deleteACHPayee(\""
			+ rowObject.map.DeleteURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

	if (options.colModel.formatoptions.canManageParticipant != 'true'
			|| rowObject["displayAsSecurePayee"] == "true") {
		edit = "";
		del = "";
	}
	if (rowObject["prenoteStatus"] == "PRENOTE_PENDING") {
		return ' ' + prenote + ' ' + view + ' ' + edit + ' ' + del;
	} else {
		return ' ' + ' ' + ' ' + ' ' + view + ' ' + edit + ' ' + del;
	}

};

ns.ach.formatCompletedACHActionLinks = function(cellvalue, options, rowObject) {
	// cellvalue is the ID of the ACH.
	var view = "<a title='" + js_view
			+ "' href='#' onClick='ns.ach.viewACHBatchDetails(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var inquiry = "<a title='" + js_inquiry
			+ "' href='#' onClick='ns.ach.inquireTransaction(\""
			+ rowObject.map.InquireURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

	if (rowObject.map.canInquire == "false") {
		inquiry = "";

	}
	return ' ' + view + ' ' + inquiry;
};

ns.ach.formatMultipleACHTemplatesActionLinks = function(cellvalue, options,
		rowObject) {
	// cellvalue is the ID of the ACH.
	var load = "<a class='' title='" + js_load
			+ "' href='#' onClick='ns.ach.loadMultipleACHTemplate(\""
			+ rowObject.map.LoadURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
	var view = "<a class='' title='" + js_view
			+ "' href='#' onClick='ns.ach.viewMultipleACHTemplate(\""
			+ rowObject.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var edit = "<a class='' title='" + js_edit
			+ "' href='#' onClick='ns.ach.editMultipleACHTemplate(\""
			+ rowObject.map.EditURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a class='' title='" + js_delete
			+ "' href='#' onClick='ns.ach.deleteMultipleACHTemplate(\""
			+ rowObject.map.DeleteURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

	if (rowObject["resultOfApproval"] != undefined
			&& rowObject["resultOfApproval"] != "") {
		// load = "<a class='ui-button' title='" +js_cannot_load+ "'><span
		// class='sapUiIconCls icon-download ui-state-disabled'></span></a>";
		load = "";
		;
	}
	if (rowObject["canEdit"] == "false") {
		edit = "";
	}
	if (rowObject["canDelete"] == "false") {
		del = "";
	}
	// if resultOfApproval="Deletion", and status didnot equal
	// "APPROVAL_REJECTED", hide deletion and edit icons
	if (rowObject["resultOfApproval"] == "Deletion"
			&& rowObject["batchStatus"] != "APPROVAL_REJECTED") {
		del = "";
		edit = "";

	}
	return load + ' ' + view + ' ' + edit + ' ' + del;
};

ns.ach.formatSingleACHTemplatesActionLinks = function(cellvalue, options,
		rowObject) {
	// cellvalue is the ID of the ACH.
	var load = "<a class='' title='" + js_load
			+ "' href='#' onClick='ns.ach.loadSingleACHTemplate(\""
			+ rowObject.fundsTransaction.map.LoadURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
	var view = "<a class='' title='" + js_view
			+ "' href='#' onClick='ns.ach.viewSingleACHTemplate(\""
			+ rowObject.fundsTransaction.map.ViewURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var edit = "<a class='' title='" + js_edit
			+ "' href='#' onClick='ns.ach.editSingleACHTemplate(\""
			+ rowObject.fundsTransaction.map.EditURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a class='' title='" + js_delete
			+ "' href='#' onClick='ns.ach.deleteSingleACHTemplate(\""
			+ rowObject.fundsTransaction.map.DeleteURL
			+ "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	if (rowObject["fundsTransaction"].canLoad == "false") {
		// load = "<a class='' title='" +js_cannot_load+ "'><span
		// class='sapUiIconCls icon-download ui-state-disabled'></span></a>";
		load = "";
	}
	if (rowObject["fundsTransaction"].canEdit == "false") {
		edit = "";
	}
	if (rowObject["fundsTransaction"].canDelete == "false") {
		del = "";
	}
	// if resultOfApproval="Deletion", and status didnot equal "10", hide
	// deletion and edit icons
	if (rowObject["fundsTransaction"].resultOfApproval == "Deletion"
			&& rowObject["fundsTransaction"].statusValue != "10") {
		del = "";
		edit = "";

	}
	return load + ' ' + view + ' ' + edit + ' ' + del;
};

ns.ach.formatSingleACHTemplatesType = function(cellvalue, options, rowObject) {
	var achTemplateType = rowObject["fundsTransaction"].templateType;
	var type = $("#achTypeId").val();
	var result = "";
	if (type == "ACHBatch") {
		result = achTemplateType;
	}
	if (type == "ChildSupportPayment") {
		result = "Child Support Payment (CCD + DED Credit) ";
	}
	if (type == "TaxPayment") {
		result = "Tax Payment (CCD + TXP Credit)";
	}
	return result;

};

// Formatter of Credit Amount column.
ns.ach.formatCreditAmountColumn = function(cellvalue, options, rowObject) {
	var creditAmount = cellvalue;
	return creditAmount;
};

// Formatter of Debit Amount column.
ns.ach.formatDebitAmountColumn = function(cellvalue, options, rowObject) {
	var debitAmount = cellvalue;
	return debitAmount;
};

// Formatter of multiple template summary Amount column
ns.ach.formatMultipleTemplateAmountColumn = function(cellvalue, options,
		rowObject) {
	var amount = "";
	if (cellvalue == "") {
		amount = "--";
	} else {
		amount = rowObject["amountValue"].currencyStringNoSymbol + ' '
				+ rowObject["amountValue"].currencyCode;
	}
	return amount;
};

// Formatter of multiple template summary Status column.
ns.ach.formatMultipleTemplateCommonStatusNameColumn = function(cellvalue,
		options, rowObject) {
	var commonStatusName = "";
	if (cellvalue == "") {
		commonStatusName = "--";
	} else {
		commonStatusName = cellvalue;
	}
	return commonStatusName;
};

// Add approval information in the pending approval summary gird.
ns.ach.addApprovalsRows = function(gridID) {
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	var approvalsOrReject = "";
	var approvalerNameOrRejectReason = "";
	var rejectedOrSubmitedBy = "";
	var approverNameOrUserName = "";
	var approverGroup = "";
	var submittedForTitle = js_submitted_for;
	var num = 0;
	for ( var i = 0; i < ids.length; i++) {
		var rowId = ids[i];

		var userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'userName'));

		var status = $(gridID).jqGrid('getCell', rowId, 'batchStatus');
		var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');

		var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
		var submittedFor = $(gridID).jqGrid('getCell', rowId,
				'resultOfApproval');
		// if status is 12, means that the transfer is rejected.
		var currentID = gridID + " #" + rowId;


		// templates can display approval items mixed with ACTIVE status
		// objects.
		if (status != 10 && status != 8)
			continue;
		if (status == 10) {
			approvalsOrReject = js_reject_reason;
			rejectedOrSubmitedBy = js_rejected_by;
			if (rejectReason == '') {
				approvalerNameOrRejectReason = "No Reason Provided";
			} else {
				approvalerNameOrRejectReason = ns.common
						.spcialSymbolConvertion(rejectReason);
			}
			approverNameOrUserName = approverNameInfo;
			num = num + 1;
			if (approverNameInfo != "") {
				var className = "pltrow1";
				if (rowId % 2 == 0) {
					className = "pdkrow";
				}
				className = 'ui-widget-content jqgrow ui-row-ltr';
				$(currentID).after(
						ns.common.addRejectReasonRow(rejectedOrSubmitedBy,
								approverNameOrUserName, approvalsOrReject,
								approvalerNameOrRejectReason, className, 11));
			} else {
				var className = "pltrow1";
				if (rowId % 2 == 0) {
					className = "pdkrow";
				}
				className = 'ui-widget-content jqgrow ui-row-ltr';

				$(currentID).after(
						ns.common.addRejectReasonRow('', '', approvalsOrReject,
								approvalerNameOrRejectReason, className, 11));
			}
		} else {
			approvalsOrReject = js_approval_waiting_on;
			approvalerNameOrRejectReason = approverNameInfo;
			rejectedOrSubmitedBy = js_submitted_by;
			approverNameOrUserName = userName;
			num = num + 1;
			if (approverNameInfo != "") {
				var className = "pltrow1";
				if (rowId % 2 == 0) {
					className = "pdkrow";
				}
				// remove the grid class, and add FF class.
				// $(currentID).removeClass();
				className = 'ui-widget-content jqgrow ui-row-ltr';

				$(currentID).after(
						ns.common.addSubmittedForRow(approvalsOrReject,
								approvalerNameOrRejectReason,
								submittedForTitle, submittedFor,
								rejectedOrSubmitedBy, approverNameOrUserName,
								className, 11));

			} else {
				var className = "pltrow1";
				if (rowId % 2 == 0) {
					className = "pdkrow";
				}
				// remove the grid class, and add FF class.

				// QTS 638252: if a transaction (ACH, Wires, Transfer, billpay,
				// cashcon) is in approvals,
				// is then edited, then values are modified so it exceeds the
				// limits with no approval
				// OR exceeds bank limits, it will get an error. If the
				// transaction is then cancelled,
				// the transaction is still in the "Approvals Summary" window,
				// but not in approvals
				// because it was cancelled and not resubmitted. Added the
				// following error message to the
				// "Approvals Summary" window, "Submission to approvals FAILED,
				// please resubmit or delete"
				// so the transaction can be edited to start submission again.
				// $(currentID).removeClass();
				className = 'ui-widget-content jqgrow ui-row-ltr';
				$(currentID)
						.after(
								'<tr class=\"'
										+ className
										+ ' skipRowActionItemRender\"><td colspan="11" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'
										+ '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'
										+ js_submission_approval_failed
										+ '&nbsp&nbsp&nbsp  </b>'
										+ '&nbsp</td></tr>');
			}
		}
	}
};

ns.ach.addApprovalsRowsForTemplate = function(gridID) {
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	var approvalsOrReject = "";
	var approvalerNameOrRejectReason = "";
	var rejectedOrSubmitedBy = "";
	var approverNameOrUserName = "";
	var approverGroup = "";
	var submittedForTitle = js_submitted_for;

	var num = 0;
	for ( var i = 0; i < ids.length; i++) {
		var rowId = ids[i];
		var currentID = gridID + " #" + rowId;
		var submittedFor = $(gridID).jqGrid('getCell', rowId,
				'fundsTransaction.resultOfApproval');

		// single batch ACH template - show additional information

		var userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId,
				'fundsTransaction.userName'));

		var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'fundsTransaction.approverInfos'));
		var rejectReason = $(gridID).jqGrid('getCell', rowId,
				'fundsTransaction.rejectReason');
		var status = $(gridID).jqGrid('getCell', rowId,
				'fundsTransaction.statusValue');

		if (submittedFor == '') {
			// Multi-ACH Batch Templates don't use fundsTransaction, so check
			// for "resultOfApproval" field
			submittedFor = $(gridID).jqGrid('getCell', rowId,
					'resultOfApproval');
			if (submittedFor == '') // not a Multi-ACH Batch template either
				continue;
			approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
			userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'userName'));

			rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
			status = $(gridID).jqGrid('getCell', rowId, 'batchStatus');
			if (status == 'Active')
				continue;
			if (status == "APPROVAL_PENDING")
				status = 8;
			else
				status = 10;
		}
		{
			// single batch ACH template - show additional information


			// templates can display approval items mixed with ACTIVE status
			// objects.
			if (status != 10 && status != 8)
				continue;
			if (status == 10) {
				approvalsOrReject = js_reject_reason;
				rejectedOrSubmitedBy = js_rejected_by;
				if (rejectReason == '') {
					approvalerNameOrRejectReason = "No Reason Provided";
				} else {
					approvalerNameOrRejectReason = ns.common
							.spcialSymbolConvertion(rejectReason);
				}
				approverNameOrUserName = approverNameInfo;
				num = num + 1;
				if (approverNameInfo != "") {
					var className = "pltrow1";
					if (rowId % 2 == 0) {
						className = "pdkrow";
					}
					className = 'ui-widget-content jqgrow ui-row-ltr';
					$(currentID).after(
							ns.common
									.addRejectReasonRow(rejectedOrSubmitedBy,
											approverNameOrUserName,
											approvalsOrReject,
											approvalerNameOrRejectReason,
											className, 11));
				} else {
					var className = "pltrow1";
					if (rowId % 2 == 0) {
						className = "pdkrow";
					}
					className = 'ui-widget-content jqgrow ui-row-ltr';

					$(currentID).after(
							ns.common
									.addRejectReasonRow('', '',
											approvalsOrReject,
											approvalerNameOrRejectReason,
											className, 11));
				}
			} else {
				approvalsOrReject = js_approval_waiting_on;
				approvalerNameOrRejectReason = approverNameInfo;
				rejectedOrSubmitedBy = js_submitted_by;
				approverNameOrUserName = userName;
				num = num + 1;
				if (approverNameInfo != "") {
					var className = "pltrow1";
					if (rowId % 2 == 0) {
						className = "pdkrow";
					}
					// remove the grid class, and add FF class.
					// $(currentID).removeClass();
					className = 'ui-widget-content jqgrow ui-row-ltr';
					var substr = '';
					if (submittedFor != '') {
						substr = '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'
								+ submittedForTitle
								+ '</b> &nbsp&nbsp&nbsp '
								+ submittedFor;
					}

					$(currentID).after(
							ns.common.addSubmittedForRow(approvalsOrReject,
									approvalerNameOrRejectReason,
									submittedForTitle, submittedFor,
									rejectedOrSubmitedBy,
									approverNameOrUserName, className, 11));

				} else {
					var className = "pltrow1";
					if (rowId % 2 == 0) {
						className = "pdkrow";
					}
					// remove the grid class, and add FF class.

					// QTS 638252: if a transaction (ACH, Wires, Transfer,
					// billpay, cashcon) is in approvals,
					// is then edited, then values are modified so it exceeds
					// the limits with no approval
					// OR exceeds bank limits, it will get an error. If the
					// transaction is then cancelled,
					// the transaction is still in the "Approvals Summary"
					// window, but not in approvals
					// because it was cancelled and not resubmitted. Added the
					// following error message to the
					// "Approvals Summary" window, "Submission to approvals
					// FAILED, please resubmit or delete"
					// so the transaction can be edited to start submission
					// again.
					// $(currentID).removeClass();
					className = 'ui-widget-content jqgrow ui-row-ltr';
					$(currentID)
							.after(
									'<tr class=\"'
											+ className
											+ ' skipRowActionItemRender\"><td colspan="11" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'
											+ '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'
											+ js_submission_approval_failed
											+ '&nbsp&nbsp&nbsp  </b>'
											+ '&nbsp</td></tr>');
				}
			}
		}
	}
};

// Add approval information in the pending approval summary gird.
ns.ach.addEntryExtraRows = function(gridID) {
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	for ( var i = 0; i < ids.length; i++) {
		var rowId = ids[i];
		var hasErrors = $(gridID).jqGrid('getCell', rowId,
				'hasValidationErrors');
		var daStatus = $(gridID).jqGrid('getCell', rowId,
				'payeeDAStatus');
		// if status is 12, means that the transfer is rejected.
		var currentID = gridID + " #" + rowId;

		var rowText = "";
		if (hasErrors != "false") {
			rowText = '&nbsp;&nbsp;&nbsp;&nbsp;' + js_ach_entry_error
					+ ' &nbsp&nbsp&nbsp';
		}
		if (daStatus == "Pending Approval") {
			rowText += '&nbsp;&nbsp;&nbsp;&nbsp;' + js_ach_da_payee
					+ ' &nbsp&nbsp&nbsp';
		}
		if (rowText != "") {
			var className = "pltrow1"
			if (rowId % 2 == 0) {
				className = "pdkrow";
			}
			// remove the grid class, and add FF class.
			// $(currentID).removeClass();
			className = 'ui-widget-content jqgrow ui-row-ltr';
			// $(currentID).addClass(className); <tr class=\"' + className +
			// '\">
			// $(currentID).after('<tr class=\"' + className + '\"><td
			// colspan="2">&nbsp</td><td colspan="2"><b>' + approvalsOrReject +
			// ' &nbsp&nbsp&nbsp </b>' + approvalerNameOrRejectReason +
			// '</td><td colspan="3"><b>' + rejectedOrSubmitedBy + '</b>
			// &nbsp&nbsp&nbsp ' + approverNameOrUserName + '</td><td
			// colspan="4">&nbsp</td></tr>');
			$(currentID)
					.after(
							'<tr class=\"'
									+ className
									+ '\"><td colspan="11" align="center"><span style="font-weight:bold;color:red;">'
									+ rowText + '</span></td></tr>');
		}
	}
};

// Add approval information in edit confirm grid.
ns.ach.addEntryExtraRowsConfirm = function(gridID) {
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	for ( var i = 0; i < ids.length; i++) {
		var rowId = ids[i];
		var daStatus = $(gridID).jqGrid('getCell', rowId,
				'payeeDAStatus');

		if (daStatus == "Pending Approval") {
			var className = 'ui-widget-content jqgrow ui-row-ltr';
			var rowText = '<tr class=\"'
					+ className
					+ '\"><td colspan=\"10\" align=\"center\"><span style="font-weight:bold;color:red;">&nbsp;&nbsp;&nbsp;&nbsp;'
					+ js_ach_da_payee + ' &nbsp&nbsp&nbsp</span></td></tr>';
			$(gridID).find('#' + rowId).after(rowText);
		}
	}
};

// reload currently selected grid
$.subscribe('reloadSelectedACHGridTab', function(event,data) {
	$(".gridSummaryContainerCls").each(function(i){
		if($(this).is(":visible")){
	        var gridId = $(this).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
	        ns.common.setFirstGridPage('#'+gridId);
	        $('#'+gridId).trigger("reloadGrid");
		}
    });
});

// Reload ACH summary jgrid..
ns.ach.reloadACHSummaryGrid = function() {

	$('#pendingApprovalACHID').trigger("reloadGrid");
	$('#pendingACHGridID').trigger("reloadGrid");
	$('#completedACHGridID').trigger("reloadGrid");
	// ns.ach.addApprovalsRows("#pendingApprovalACHID");
};

ns.ach.reloadChildSummaryGrid = function() {
	$('#pendingApprovalChildID').trigger("reloadGrid");
	$('#pendingChildGridId').trigger("reloadGrid");
	$('#completedChildGrid').trigger("reloadGrid");
	ns.ach.addApprovalsRows("#pendingApprovalChildID");
};

ns.ach.reloadTaxSummaryGrid = function() {
	$('#pendingApprovalTaxID').trigger("reloadGrid");
	$('#pendingTaxGridId').trigger("reloadGrid");
	$('#completedTaxGrid').trigger("reloadGrid");
	ns.ach.addApprovalsRows("#pendingApprovalTaxID");
};

// Reload single ACH templates summary jgrid..
ns.ach.reloadSingleACHTemplatesSummaryGrid = function(gridId) {
	$('#singleACHTemplatesGridID').trigger("reloadGrid");
	ns.ach.addApprovalsRowsForTemplate('#singleACHTemplatesGridID');
};

// Reload multiple ACH templates summary jgrid..
ns.ach.reloadMultipleACHTemplatesSummaryGrid = function(gridId) {
	$('#multipleACHTemplatesGridID').trigger("reloadGrid");
};

// view ACH templates.
ns.ach.loadACHTemplate = function(ACHID, collectionName) {
	ns.ach.viewACHBatchDetails(ACHID, collectionName, "true");
};

// edit ACH template.
ns.ach.editSingleACHTemplate = function(urlString) {
	$.publish('beforeLoadACHForm');
	$.ajax({
		url : urlString,
		success : function(data) {
			$.publish('loadACHFormComplete');
			$('#inputDiv').html(data);
			var heading = $('#PageHeading').html();
			var $title = $('#details .portlet-title');
			$title.html(heading);

		},
		complete: function(data){
			$('#detailsPortletWizardStatusId').show();
		}
	});
	ns.common.selectDashboardItem("addSingleTemplateLink");
};

// delete ACH template.
ns.ach.deleteSingleACHTemplate = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#deleteACHTemplateDialogID').html(data).dialog('open');
			ns.common.resizeDialog('deleteACHTemplateDialogID');
		}
	});
};

ns.ach.loadSingleACHTemplate = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#summary').empty();
			$('#inputDiv').html(data).show();
			$('#achAddEditEntrySection').hide();
			ns.ach.loadACHPageForSelectedTemplate();
			ns.ach.showInputDivSteps();
			ns.ach.selectSingleACHTemplateDashboard();
			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setFocus();
			}
		}
	});
};

ns.ach.selectSingleACHTemplateDashboard = function() {
	ns.common.refreshDashboard('dbACHSummary');
	if ($('#addSingleACHBatchLink').length > 0) {
		ns.common.selectDashboardItem("addSingleACHBatchLink");
	} else if ($('#addSingleChildspBatchLink').length > 0) {
		ns.common.selectDashboardItem("addSingleChildspBatchLink");
	} else if ($('#addSingleTaxBatchLink').length > 0) {
		ns.common.selectDashboardItem("addSingleTaxBatchLink");
	}
};


ns.ach.viewSingleACHTemplate = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#viewACHBatchDetailsDialogID').html(data).dialog('open');
			ns.common.resizeDialog('viewACHBatchDetailsDialogID');
		}
	});
};

ns.ach.loadMultipleACHTemplate = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
		$('#summary').empty();
			$('#inputDiv').html(data).show();
			$('#achAddEditEntrySection').hide();
			ns.ach.loadACHPageForSelectedTemplate();
			ns.ach.showInputDivSteps();
			ns.ach.selectMultipleACHTemplateDashboard();
			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setInitialFocus();
			}
		}
	});
};

ns.ach.selectMultipleACHTemplateDashboard = function() {
	ns.common.refreshDashboard('dbACHSummary');
	if ($('#addMultipleACHBatchLink').length > 0) {
		ns.common.selectDashboardItem("addMultipleACHBatchLink");
	} else if ($('#addMultipleChildSupportBatchLink').length > 0) {
		ns.common.selectDashboardItem("addMultipleChildSupportBatchLink");
	} else if ($('#addMultipleTaxBatchLink').length > 0) {
		ns.common.selectDashboardItem("addMultipleTaxBatchLink");
	}
};


ns.ach.viewMultipleACHTemplate = function(urlString) {
	// QTS 730201: clear out the inputDiv in case we have edited a Multi-Batch
	// Template
	// both VIEW and EDIT use the same form elements
	$('#details').slideUp();
	$('#inputDiv').html("");
	$.ajax({
		url : urlString,
		success : function(data) {
			$("#viewACHMultiBatchDetailsDialogID").html(data).dialog('open').css('height','auto').addClass("viewDeletePopup");
			ns.common.resizeDialog('viewACHMultiBatchDetailsDialogID');
		}
	});
};

// The function is used by above two load template functions :
// loadSingleACHTemplate() and loadMultipleACHTemplate().
ns.ach.loadACHPageForSelectedTemplate = function() {
	// $.log('About to load form! ');
	$('#details').hide();
	$('#quicksearchcriteria').hide();

	ns.ach.foldPortlet('summary');

	// set portlet title (defined in each page loaded to inputDiv by Id
	// "PageHeading"
	var heading = $('#PageHeading').html();
	var $title = $('#details .portlet-title');
	$title.html(heading);

	ns.ach.gotoStep(1);
	$('#details').slideDown();
};

// edit multiple ACH template.
ns.ach.editMultipleACHTemplate = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#inputDiv').html(data).show();
			$('#achAddEditEntrySection').hide();
			ns.ach.loadACHPageForSelectedTemplate();
			ns.ach.showInputDivSteps();

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#inputDiv").setInitialFocus();
			}
		},
		complete: function(data){
			$('#detailsPortletWizardStatusId').show();
		}
	});
	// $.ajax({
	// url : urlString,
	// success : function(data) {
	// $('#editMultipleACHTemplateLink').click();
	// ns.ach.hideInputDivSteps();
	// }
	// });
	ns.common.selectDashboardItem("addMultipleTemplateLink");
};

// inquire ACH transaction.
ns.ach.inquireTransaction = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#inquiryACHBatchDialogID').html(data).dialog('open');
		}
	});
};
ns.ach.setAllAmountsForBatch = function() {
	var urlString = "/cb/pages/jsp/ach/achbatch_setallamounts.jsp";
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#setAllAmountsDialogID').html(data).dialog('open');
		}
	});
};

ns.ach.viewACHBatchDetails = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#viewACHBatchDetailsDialogID').html(data).dialog('open');
			ns.common.resizeDialog('viewACHBatchDetailsDialogID');
		}
	});
};

ns.ach.viewACHPayeeDetails = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#viewACHPayeeDetailsDialogID').html(data).dialog("open");
			ns.common.resizeDialog('viewACHPayeeDetailsDialogID');
		}
	});
};

ns.ach.editACHPayee = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			ns.ach.beforeLoadACHForm();

			$('#inputDiv').html(data).show();
			$('#achAddEditEntrySection').hide();
			ns.ach.foldPortlet('summary');

			// set portlet title (defined in each page loaded to inputDiv by Id
			// "PageHeading"
			var heading = $('#PageHeading').html();
			var $title = $('#details .portlet-title');
			$title.html(heading);

			ns.ach.gotoStep(1);
			$('#details').slideDown();

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#ACHTransactionWizardTabs").setFocusOnTab();
			}

			ns.ach.showInputDivSteps();

			//No need of third step i.e. confirmation for payee
			$("#confirmTab").hide();
			ns.common.selectDashboardItem("addACHPayeeLink");
		}
	});
};

ns.ach.editManagedACHPayee = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$("#achAddEditEntrySection").html(data);
			ns.ach.showInputDivSteps();
            ns.ach.gotoStep(1);
            $("#inputDiv").hide();
            $("#achAddEditEntrySection").show();

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#achAddEditEntrySection").setInitialFocus();
			}

			//No need of third step i.e. confirmation for payee
			if($("#confirmTab").length > 0){
				$("#confirmTab").hide();
			}
		}
	});
};

ns.ach.deleteACHPayee = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#deleteACHPayeeDialogID').html(data).dialog('open');
		}
	});
};

ns.ach.modifyACHPayee = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#modifyACHPayeeDialogID').html(data).dialog('open');
			ns.ach.showInputDivSteps();
		}
	});
};

ns.ach.discardACHPayee = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#discardACHPayeeDialogID').html(data).dialog('open');
			ns.ach.hideInputDivSteps();
		}
	});
};

ns.ach.insertACHPayee = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
            ns.ach.gotoStep(1); // make sure we are in the INPUT div.
            ns.ach.hideInputDivSteps();
            $("#inputDiv").hide();
            $('#verifyDiv').empty();
            $('#confirmDiv').empty();

			$('#achAddEditEntrySection').html(data);
            $("#achAddEditEntrySection").show();
			$('#isDeleteId').val("false");

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#achAddEditEntrySection").setInitialFocus();
			}
		}
	});
};

ns.ach.cancelManagedPayee = function(urlString) {
	ns.ach.insertACHPayee(urlString);
}

ns.ach.showErrorsACHEntry = function(entryID, collectionName) {
	var urlString = "/cb/pages/jsp/ach/achbatchentryshowerror.jsp";
	var entryIDVar = entryID;
	$.ajax({
		url : urlString,
		type : "POST",
		data : {
			EntryID : entryIDVar,
			CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
		},
		success : function(data) {
			$('#viewACHEntryErrorDetailsDialogID').html(data).dialog('open');
			ns.common.resizeDialog('viewACHEntryErrorDetailsDialogID');
		}
	});
};

ns.ach.deleteACHEntry = function(entryID, collectionName) {
	var curAmount = $("#amount" + entryID + " input").val();
	var action = $("#AddBatchNew").attr("action");
	var len = action.indexOf("_");
	var isTaxChild = $("#achEntriesGridId").hasClass("TaxChild");
	var entryURLString = "/cb/pages/jsp/ach/deleteACHEntryAction_init.action";
	var urlStr;
	if (isTaxChild) // Tax & Child Support do not allow editing the amount in
	{
		// summary page
		entryURLString = "/cb/pages/jsp/ach/deleteTaxChildACHEntryAction_init.action";
		var urlStr = action.substr(0, len) + '_beforeACHEntryOperation.action';
	}

	if (urlStr) {
		$.ajax({
			url : urlStr,
			type : "POST",
			data : $("#AddBatchNew").serialize(),
			success : function(data) {
				ns.ach.callEntryOperation(entryURLString, entryID, curAmount);
			}
		});
	} else {
		ns.ach.callEntryOperation(entryURLString, entryID, curAmount);
	}

};

ns.ach.editACHEntry = function(entryID, collectionName) {
	var curAmount = $("#amount" + entryID + " input").val();
	var action = $("#AddBatchNew").attr("action");
	var len = action.indexOf("_");
	var isTaxChild = $("#achEntriesGridId").hasClass("TaxChild");
	var entryURLString = "/cb/pages/jsp/ach/editACHEntryAction_init.action";
	var urlStr;
	if (isTaxChild) // Tax & Child Support do not allow editing the amount in
	{
		// summary page
		entryURLString = "/cb/pages/jsp/ach/editTaxChildACHEntryAction_init.action";
		urlStr = action.substr(0, len) + '_beforeACHEntryOperation.action';
	}

	if (urlStr) {
		$.ajax({
			url : urlStr,
			type : "POST",
			data : $("#AddBatchNew").serialize(),
			success : function(data) {
				ns.ach.callEntryOperation(entryURLString, entryID, curAmount);
			}
		});
	} else {
		ns.ach.callEntryOperation(entryURLString, entryID, curAmount);
	}
};

ns.ach.callEntryOperation = function(urlString, entryID, curAmount) {
	$.ajax({
		url : urlString,
		type : "POST",
		data : {
			entryID : entryID,
			curAmount : curAmount,
			CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
		},
		success : function(data) {
			$('#isDeleteId').val("false");
			$('#achAddEditEntrySection').html(data);
			$('#achAddEditEntrySection').show();
			$('#inputDiv').hide();
			ns.ach.hideInputDivSteps();

			$.publish("common.topics.tabifyDesktop");
			if (accessibility) {
				$("#details").setInitialFocus();
			}
		}
	});
};

ns.ach.deleteACHBatch = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#deleteACHBatchDialogID').html(data).dialog('open');
			ns.common.resizeDialog('deleteACHBatchDialogID');
		}
	});
};

ns.ach.deletePendingACHBatch = function(urlString, transactionType, recid) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#deleteACHBatchDialogID').html(data).dialog('open');
			ns.common.resizeDialog('deleteACHBatchDialogID');
		}
	});

};

ns.ach.skipPendingACHBatch = function(urlString, transactionType, recid) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#skipACHBatchDialogID').html(data).dialog('open');
			ns.common.resizeDialog('skipACHBatchDialogID');
		}
	});

};

ns.ach.deleteMultipleACHTemplate = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#inputDiv').html(data);
			ns.ach.loadACHPageForSelectedTemplate();

		}
	});
};

ns.ach.editPendingACHBatch = function(urlString, transactionType, recid, isRecModel) {
	if (isRecModel == "true") {
		$.ajax({
			url : urlString,
			success : function(data) {
				$('#editPendingACHBatchDialogID').html(data).dialog('open');
				ns.common.resizeDialog('editPendingACHBatchDialogID');
			}
		});
	} else {
		$.publish('beforeLoadACHForm');
		$.ajax({
			url : urlString,
			success : function(data) {
				$.publish('loadACHFormComplete');
				$('#inputDiv').html(data);
				var heading = $('#PageHeading').html();
				var $title = $('#details .portlet-title');
				$title.html(heading);
			}
		});
	}
	ns.common.selectDashboardItem('addSingleACHBatchLink');
};

ns.ach.editSingleACHBatch = function(urlString) {
	$.publish('beforeLoadACHForm');
	$.ajax({
		url : urlString,
		success : function(data) {
			$.publish('loadACHFormComplete');
			$('#inputDiv').html(data);
			var heading = $('#PageHeading').html();
			var $title = $('#details .portlet-title');
			$title.html(heading);
			//ns.common.selectDashboardItem('addSingleChildspBatchLink');
			//ns.common.selectDashboardItem('addSingleTaxBatchLink');
		},
		complete: function(data){
			$('#detailsPortletWizardStatusId').show();
			//ns.common.selectDashboardItem('addSingleChildspBatchLink');
			//ns.common.selectDashboardItem('addSingleTaxBatchLink');
		}
	});
};

// The method is used by ACH_summary.jsp and templates_summary.jsp to get ACH
// and templates , to improve performance.
ns.ach.getACHSummaryDatas = function(urlString, divID) {
	$.get(urlString, function(data) {
		$(divID).html(data);
	});
};

ns.ach.gotoManagedPayees = function() {
	$.ajax({
		url : "/cb/pages/jsp/ach/achmanagedpayees.jsp",
		success : function(data) {
            $("#achAddEditEntrySection").html(data);

            ns.ach.gotoStep(1);
            $('#inputDiv').hide();
            ns.ach.hideInputDivSteps();
            $('#verifyDiv').empty();
            $('#confirmDiv').empty();
            $("#achAddEditEntrySection").show();

            $.publish("common.topics.tabifyDesktop");
            if (accessibility) {
                $("#achAddEditEntrySection").setFocus();
            }
		}
	});
};

ns.ach.formatAchCompanyIDandName = function(cellvalue, options, rowObject) {

	var _companyID = rowObject["fundsTransaction"].companyID;
	var _companyName = rowObject["fundsTransaction"].coName;

	if (_companyName != undefined && _companyName != "") {
		_companyID = _companyID + " / " + _companyName;
	}
	return _companyID;
}

ns.ach.formatAchEffectiveDate = function(cellvalue, options, rowObject) {
	var _recID = rowObject["recID"];
	var _transactionType = rowObject["transactionType"];
	var _effectiveDate = rowObject["date"];
	/* We dont need this formatinng keeping this method for future use
	if (_transactionType == "ACH")
		if (_recID != "") {
			_effectiveDate = "*" + _effectiveDate;
		}
		*/
	return _effectiveDate;

}

ns.ach.formatACHPayeesCompanyNameID = function(cellvalue, options, rowObject) {
	if (rowObject.payeeGroup != "ACH Company Scope") {
		return "--";
	} else {
		return cellvalue;
	}
}

// Add reject information in pending ach payee grid.
ns.ach.addRejectReasonRowsForPendingACHPayee = function(gridID) {
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	var approvalerNameOrRejectReason = "";
	for ( var i = 0; i < ids.length; i++) {
		var rowId = ids[i];
		var daStatus = $(gridID).jqGrid('getCell', rowId, 'daStatus');
		var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
		var rejectedBy = $(gridID).jqGrid('getCell', rowId, 'rejectedBy');

		if (daStatus == 'Rejected') {
			if (rejectReason == '') {
				approvalerNameOrRejectReason = "No Reason Provided";
			} else {
				approvalerNameOrRejectReason = ns.common
						.spcialSymbolConvertion(rejectReason);
			}

			var className = 'ui-widget-content jqgrow ui-row-ltr';
			$(gridID).find('#' + rowId).after(
					ns.common.addRejectReasonRow(js_rejected_by, rejectedBy,
							js_reject_reason, approvalerNameOrRejectReason,
							className, 11));
		}
	}
};

// //////////////////// Delete ACH Payee
$.subscribe('closeDeleteACHPayee', function(event, data) {
	ns.common.closeDialog('#deleteACHPayeeDialogID');
});

$.subscribe('beforeDeleteACHPayeeForm', function(event, data) {
});

$.subscribe('errorDeleteACHPayeeForm', function(event, data) {
});
$.subscribe('successDeleteACHPayeeForm', function(event, data) {
	ns.common.closeDialog('#deleteACHPayeeDialogID');
	ns.ach.refreshACHPayeeGrids();
	ns.common.showStatus(data);
});

// ////////////////////Modify ACH Payee
$.subscribe('closeModifyACHPayee', function(event, data) {
	ns.common.closeDialog('#modifyACHPayeeDialogID');
});

$.subscribe('beforeModifyACHPayeeForm', function(event, data) {
});
$.subscribe('errorModifyACHPayeeForm', function(event, data) {
	ns.common.closeDialog('#modifyACHPayeeDialogID');
	ns.common.showStatus();
});
$.subscribe('successModifyACHPayeeForm', function(event, data) {
	ns.ach.refreshACHPayeeGrids();
	ns.common.showStatus();
});

// ////////////////////Discard ACH Payee
$.subscribe('closeDiscardACHPayee', function(event, data) {
	ns.common.closeDialog('#discardACHPayeeDialogID');
});

$.subscribe('beforeDiscardACHPayeeForm', function(event, data) {
});
$.subscribe('errorDisardACHPayeeForm', function(event, data) {
	ns.common.closeDialog('#discardACHPayeeDialogID');
	ns.common.showStatus();
});
$.subscribe('successDisardACHPayeeForm', function(event, data) {
	ns.ach.refreshACHPayeeGrids();
	ns.common.showStatus();
});

ns.ach.refreshACHPayeeGrids = function() {
	$.ajaxSetup({
		async : false
	});
	// ajax call to determine if the pending ach payee grid should be hidden
	$.ajax({
		type : "GET",
		url : "/cb/pages/jsp/ach/inc/achpayee_pendingapproval-pre.jsp",
		success : function(data) {
			$("#divIDForReloadACHpayees").html(data);
			if (ns.ach.pendingApprovalPayees == "0") {
				$('#ach_payeesPendingApprovalTab')
						.attr('style', 'display:none');
			} else {
				$('#ach_payeesPendingApprovalTab').removeAttr('style');
			}
		}
	});
	$('#pendingApprovalACHPayeeGridId').trigger("reloadGrid");
	$('#standardACHPayeeGridId').trigger("reloadGrid");
	$('#IATACHPayeeGridId').trigger("reloadGrid");
	$.ajaxSetup({
		async : true
	});
};


