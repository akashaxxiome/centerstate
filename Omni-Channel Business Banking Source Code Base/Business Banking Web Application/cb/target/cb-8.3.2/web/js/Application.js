jQuery.sap.declare("Application");
jQuery.sap.require("sap.ui.app.Application");
jQuery.sap.require("sap.banking.ui.core.routing.AppRouter");//Load the router
jQuery.sap.require("sap.banking.ui.core.routing.ApplicationRouter");//Load the application router
jQuery.sap.require("sap.banking.ui.routing.DeviceManagementRouter");
jQuery.sap.require("sap.banking.ui.routing.Routers");
jQuery.sap.require("sap.banking.ui.routing.ChartsRouter");
jQuery.sap.require("sap.banking.ui.common.Help");

/**
* This is main appliction class for ebanking
* It initializes some of the global settings and 
* i18n model and other application level activities
*/
sap.ui.app.Application.extend("Application",{

	JS_ROOT: "../../../web/js",
	UI5_SOURCE_PATH: JS_ROOT + "/ui5",
	//Help module to show help
	helpModule: null,
	/**
	* Init function which gets called before calling main
	* this is nice place to do application initialization
	*/
	init: function(){
		this.helpModule = sap.banking.ui.common.Help;
		this.helpModule.init();
		this.setLogLevel();
		//this.initializeModel();
	},

	/**
	* This function initializes the global model and sets it to core
	*/
	initializeModel: function(){
		$.sap.log.info("initializeModel");
		//set the global model set the image model if any
		var model = new sap.ui.model.json.JSONModel("../../../web/js/ui5/data/global.json");
		sap.ui.getCore().setModel(model);
	},

	/**
	* This function sets the log level
	* Desired log levels,
	* Devlopement:sap.log.Level.INFO 
	* Production :sap.log.Level.ERROR 
	*/
	setLogLevel: function(){
		//set the log level to info for production change this to error
		$.sap.log.setLevel($.sap.log.Level.ERROR);
	},

	/**
	* Main function for ebanking SPA
	* In future when we truely move to single page app this would be kick off point for our app
	* We will have main app view and app controlller which would get placed at root.
	*/
	main: function(){
		/*var root = this.getRoot();
		var mainView  = sap.ui.jsview("app","view.app");
		mainView.placeAt(root);*/
	}

});