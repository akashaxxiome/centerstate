/**
* Description: Device Management Service
* @class DeviceManagementService
* @extends BankingService
* @singleton Device Management Service is implemented as Singleton instance
* @namespace sap.banking.ui.services.DeviceManagementService
*/
(function($,window,undefined){
	"use strict";

	jQuery.sap.declare("sap.banking.ui.services.DeviceManagementService");
	jQuery.sap.require("sap.banking.ui.services.BankingService");
	jQuery.sap.require("sap.banking.ui.services.ServiceConstants");

	sap.banking.ui.services.DeviceManagementService = (function() {		
			var uniqueInstance; //Singleton instance
			function constructor() {
				var CONSTANTS = sap.banking.ui.services.ServiceConstants.DEVICEMANAGEMENTSERVICE;

				/**
				* DeviceManagementService augmentation with base BankingService
				*/
				var Service = sap.banking.ui.services.BankingService.extend("DeviceManagementService",{
					
					/**
					* API which returns the device bean with passed device id
					* @param {String} deviceId, device id to be retrieved
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Object} MobileDevice bean
					*/
					getDevice: function(deviceId,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>getDevice>id: " +deviceId);
						//this.read(this.entities.DEVICES);
					},

					/**
					* API which returns devices registered by user
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback			
					* @return {Array} of MobileDevice beans
					*/
					getDevices: function(fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>getDevices");
						this.read(this.entities.DEVICES,fnSuccess,fnError);
					},

					/**
					* API which  returns list of countries
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Array} of MobileCountries beans					
					*/
					getCountries: function(fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>getCountries");
						var entityCountry = this.entities.COUNTRIES + "?$expand=MobileCarriers";
						this.read(entityCountry,fnSuccess,fnError);						
					},

					/**
					* API for wizard's init step, which returns the initialized MobileDevice bean
					* @param {Object} oEntry bean for initialzation
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Array} of MobileDevice beans					
					*/
					initDevice: function(oEntry,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>initMobileDevice");
						var that = this;

						var _fnSuccess = function(response){
							that.updateDateFormats(response);
							that.updateBooleanProperties(response);
							var data;
							if(response){
								data = [response];
							}
							if(fnSuccess){
								fnSuccess.apply(this,data);
							}	
						}

						this.init(this.entities.DEVICES,oEntry,_fnSuccess,fnError);
					},

					/**
					* API for wizard's verify step, which verifies device bean
					* @param {Object} oEntry bean for verification
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Array} of MobileDevice beans										
					*/
					verifyDevice: function(oData,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>verifyMobileDevice");
						var oEntry = $.extend(true,{},oData);
						if(oEntry){
							var sDate = oEntry.MobileDeviceAlertSettings.NoAlertStartDate;
							var eDate = oEntry.MobileDeviceAlertSettings.NoAlertEndDate;							
							sDate = (sDate === "") ? null : sDate;
							eDate = (eDate === "") ? null : eDate; 
							if(sDate != null){
								var startDate = this.parseDate(sDate, "MM/dd/yyyy", true);	
								oEntry.MobileDeviceAlertSettings.NoAlertStartDate = startDate;
							}else{
								oEntry.MobileDeviceAlertSettings.NoAlertStartDate = null;
							}

							if(eDate != null){
								var endDate = this.parseDate(eDate, "MM/dd/yyyy", true);
								oEntry.MobileDeviceAlertSettings.NoAlertEndDate = endDate;
							}else{
								oEntry.MobileDeviceAlertSettings.NoAlertEndDate = null;
							}

							oEntry = this.updatePropertiesToBoolean(oEntry);
							oEntry.DeviceName = $.trim(oEntry.DeviceName);
						}
						this.verify(this.entities.DEVICES,oEntry,fnSuccess,fnError);
					},

					 /**
				     * Parse a string which is formatted according to the given format options.
				     * @param {string}	sValue	the string containing a formatted date/time value
				     * @param {boolean}	bUTC	whether to use UTC, if no timezone is contained
				     * @return {Date}	the parsed value
				     */
				    parseDate: function(dateStr, dateFormat, isUTC) {
				       	dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				       		pattern : dateFormat, UTC: isUTC, strictParsing: true
				       	});
				    	return dateFormat.parse(dateStr);
				    },
				    
					/**
					* API for wizard's confirm step, which returns the confirmed 
					* device bean
					* @param {Object} oEntry bean for confirmation
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Array} of MobileDevice beans
					*/
					confirmDevice: function(oEntry,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>confirmMobileDevice");
						this.confirm(this.entities.DEVICES,oEntry,fnSuccess,fnError);
					},

					/*
					* This API deletes the MobileDevice with passed device identifier 
					* @param {Number} deviceId id of device which needs to be deleted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					*/
					deleteMobileDevice: function(deviceId,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>deleteMobileDevice");
						this.remove(this.entities.DEVICES,deviceId,fnSuccess,fnError);
					},

					/*
					* API for getting all restricted MSISDN numbers 
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Array} List of RestrictedMSISDN beans in success call back
					*/
					getRestrictedMSISDNS:function(fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>getRestrictedMSISDNS");
						this.read(this.entities.RESTRICTEDMSISDNS,fnSuccess,fnError);
					},

					/*
					* API for adding restricted MSISDN number 
					* @param {Number} restricedMSISDN id of device which is need to be restricted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Object} RestrictedMSISDN bean in success call back
					*/
					addRestrictedMSISDN:function(restricedMSISDN,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>addRestrictedMSISDN");
						if(!restricedMSISDN){
							throw new Error("RestricedMSISDN is required");
						}

						var oEntry = {
							__metadata: {			
								"type" : "com.sap.banking.mobile.endpoint.v1_0.beans.RestrictedMsisdn"
							}
						}

						oEntry = $.extend(oEntry,restricedMSISDN);
						this.create(this.entities.RESTRICTEDMSISDNS,oEntry,fnSuccess,fnError);
					},

					/*
					* API for adding restricted MSISDN numbers, This api can take multiple MSISDN numbers
					* TODO:Create Batch operation with BankingService
					* it is implemented as batch operation
					* @param {Array} restricedMSISDNs id of device which is need to be restricted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					* @return {Array} of RestrictedMSISDN bean in success call back					
					*/
					addRestrictedMSISDNs:function(restricedMSISDNs,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>addRestrictedMSISDN");
						if(!restricedMSISDNs){
							throw new Error("RestricedMSISDN is required");
						}
						//this.create(this.entities.RESTRICTEDMSISDNS,deviceId,fnSuccess,fnError);
					},

					/*
					* API for deleting restricted MSISDN number 
					* @param {Number} restricedMSISDN id of device which is need to be deleted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					*/
					deleteRestrictedMSISDN: function(systemId,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>deleteRestrictedMSISDN");
						if(!systemId){
							throw new Error("Please provide systemId");
						}

						var sysId = Number(systemId);
						this.remove(this.entities.RESTRICTEDMSISDNS,sysId,fnSuccess,fnError);
					},

					/*
					* API for deleting restricted MSISDN numbers, This api can take multiple MSISDN numbers
					* TODO: Create Batch operation with BankingService
					* it is implemented as batch operation
					* @param {Array} restricedMSISDNs id of device which is need to be restricted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					*/
					deleteRestrictedMSISDNs: function(systemIds,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>deleteRestrictedMSISDNs");
						//this.remove(this.entities.RESTRICTEDMSISDNS,systemId,fnSuccess,fnError);
						//Below is temp workaround for delete bactch
						for (var i = systemIds.length - 1; i >= 0; i--) {
							var systemId = systemIds[i];
							this.deleteRestrictedMSISDN(systemId,fnSuccess,fnError);
						};
					},

					/*
					* API for accepting carrier terms during device registration 
					* @param {Number} deviceId id of device which is need to be deleted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					*/
					acceptDeviceRegistrationCarrierTerms: function(deviceId,carrierCode,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>acceptDeviceRegistrationCarrierTerms");
						if(!deviceId){
							throw new Error("Device id is required!!!");
						}						
						
						var params = {
							"DeviceId": deviceId + "L",
							"CarrierCode": "'" + carrierCode + "'"
						}						
						this.callFunction(this.functionImports.ACCEPTDEVICEREGCARRIERTERMS, params, fnSuccess, fnError);
					},
					
					/*
					* API for declining carrier terms during device registration 
					* @param {Number} deviceId id of device which is need to be deleted
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					*/
					declineDeviceRegistrationCarrierTerms: function(deviceId,fnSuccess,fnError){
						this.logger.debug("DeviceManagementService>declineDeviceRegistrationCarrierTerms");
						if(!deviceId){
							throw new Error("Device id is required!!!");
						}						
						
						var params = {
							"DeviceId": deviceId + "L"
						}
						this.callFunction(this.functionImports.DECLINEDEVICEREGCARRIERTERMS, params, fnSuccess, fnError);
					},

					/***************** DeviceManagementService Helper Functions *******************/
					updateBooleanProperties: function(device){
						if(device){
							if(device.AcceptLoginToken === null || device.AcceptLoginToken === "F" ){
								device.AcceptLoginToken = false;
							}else{
								device.AcceptLoginToken = true;
							}

							var alertSettings = device.MobileDeviceAlertSettings;
							if(alertSettings){
								if(alertSettings.AlertsEnabled === null || alertSettings.AlertsEnabled == "F" ){
									device.MobileDeviceAlertSettings.AlertsEnabled = false;
								}else{
									device.MobileDeviceAlertSettings.AlertsEnabled = true;
								}

								if(alertSettings.NoAlertDateRange === null || alertSettings.NoAlertDateRange == "F" ){
									device.MobileDeviceAlertSettings.NoAlertDateRange = false;
								}else{
									device.MobileDeviceAlertSettings.NoAlertDateRange = true;
								}

								if(alertSettings.NoAlertTimeRange === null || alertSettings.NoAlertTimeRange == "F" ){
									device.MobileDeviceAlertSettings.NoAlertTimeRange = false;
								}else{
									device.MobileDeviceAlertSettings.NoAlertTimeRange = true;
								}

								if(alertSettings.NoAlertWeekEnd === null || alertSettings.NoAlertWeekEnd == "F" ){
									device.MobileDeviceAlertSettings.NoAlertWeekEnd = false;
								}else{
									device.MobileDeviceAlertSettings.NoAlertWeekEnd = true;
								}
							}
						}
						return device;
					},

					updatePropertiesToBoolean: function(device){
						if(device){
							if(device.AcceptLoginToken === false){
								device.AcceptLoginToken = "F";
							}

							if(device.AcceptLoginToken === true){
								device.AcceptLoginToken = "T";
							}
							var alertSettings = device.MobileDeviceAlertSettings;
							if(alertSettings){
								if(alertSettings.AlertsEnabled == false){
									device.MobileDeviceAlertSettings.AlertsEnabled = "F";
								}

								if(alertSettings.AlertsEnabled == true){
									device.MobileDeviceAlertSettings.AlertsEnabled = "T";
								}

								if(alertSettings.NoAlertDateRange == false){
									device.MobileDeviceAlertSettings.NoAlertDateRange = "F";
								}

								if(alertSettings.NoAlertDateRange == true){
									device.MobileDeviceAlertSettings.NoAlertDateRange = "T";
								}

								if(alertSettings.NoAlertTimeRange == false){
									device.MobileDeviceAlertSettings.NoAlertTimeRange = "F";
								}

								if(alertSettings.NoAlertTimeRange == true){
									device.MobileDeviceAlertSettings.NoAlertTimeRange = "T";
								}

								if(alertSettings.NoAlertWeekEnd == false){
									device.MobileDeviceAlertSettings.NoAlertWeekEnd = "F";
								}
								
								if(alertSettings.NoAlertWeekEnd == true){
									device.MobileDeviceAlertSettings.NoAlertWeekEnd = "T";
								}
							}
						}
						return device;
					},

					updateDateFormats: function(response){
						var dateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "MM/dd/yyyy"});
						var startDate = response.MobileDeviceAlertSettings.NoAlertStartDate;
						var endDate = response.MobileDeviceAlertSettings.NoAlertEndDate;


						if(startDate && startDate != null){
							response.MobileDeviceAlertSettings.NoAlertStartDate = dateFormat.format(startDate);
						}
						
						if(endDate && endDate != null){
							response.MobileDeviceAlertSettings.NoAlertEndDate = dateFormat.format(endDate);
						}							
					}

				});

				

				/***************** DeviceManagementService End *******************/

				var params = {
					uri: CONSTANTS.URI,
					entities: CONSTANTS.ENTITIES,
					functionImports: CONSTANTS.FUNCTIONIMPORTS
				}
				return new Service(params);
			}
		return {
			getInstance: function() {
				if(!uniqueInstance) { // Instantiate only if the instance doesn't exist.
					uniqueInstance = constructor();
				}
				return uniqueInstance;
			}
		}
	})();
}(jQuery,window));