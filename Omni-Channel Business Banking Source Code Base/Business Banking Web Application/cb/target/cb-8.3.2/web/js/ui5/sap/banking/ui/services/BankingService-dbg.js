/**
* Base Banking Servic class
* this will peform REST communication
*/

jQuery.sap.declare("sap.banking.ui.services.BankingService");
sap.ui.base.Object.extend("sap.banking.ui.services.BankingService",{
	uri: undefined,
	userName: undefined,
	password: undefined,
	
	constructor: function(aUri,aUserName,aPassword){
		console.info("BankingService-Constructor");
		this.uri = aUri;
		this.userName = aUserName;
		this.password = aPassword;
	},

	get: function(){
		console.info("BankingService-Constructor");
	},

	post: function(){
		console.info("BankingService-Constructor");
	},

	put: function(){
		console.info("BankingService-Constructor");
	},

	"delete": function(){
		console.info("BankingService-Constructor");
	},

	/**
	* Base success handler
	* TODO: handle global success message
	*/
	successHandler: function(d){
		jQuery.sap.info("BankingService-successHandler");
	},

	/**
	* Base error handler
	* TODO: handle global error messages to show errors
	*/
	errorHandler: function(d){
		jQuery.sap.info("BankingService-errorHandler");	
	}
});