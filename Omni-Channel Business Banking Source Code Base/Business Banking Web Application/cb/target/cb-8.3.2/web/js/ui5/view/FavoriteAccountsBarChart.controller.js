/**
* Controller for charts of cash flow
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.FavoriteAccountsBarChart",{	

	model:undefined,
	
	onInit: function(){
		this.initializei18NModel();
	},
	
	initializei18NModel: function(){	
		this.initResourceModel("charts");
		
		//Register device i18n model
		this.getView().setModel(this.resourceModel, "i18n");
	},
	
	initializeModel:function(){
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
			url:"/cb/pages/jsp/home/FavoriteAccountsPortletAction_loadFavoriteAccountsChartData.action?isChart=true",
			success: function(data) {
				//console.log("ajax success");
				// set data in json model
				controllerHandler.model.setData({
                      businessData : data.gridModel
				 });
				// set data in view
				controllerHandler.getView().setModel(controllerHandler.model);
			},
			error : function(jqXHR, sTextStatus, oError) {
				console.info(sTextStatus);
			},
			complete:function(jqXHR, textStatus){
				controllerHandler.renderBarchart();
			}
		};
		this.getChartData(oConfig);
	},

	renderBarchart:function (){
			var dimensionName = 'Closing Balance';
		 
		    var barContainer = this.byId("barContainerFavoriteAccount");
	  		var messageHolder = this.byId("messageHolderBarFavoriteAccount");
	  		var noBalanceText = this.byId("noBalanceTextBarFavoriteAccount");
	  		
	  		var oTextView = new sap.ui.commons.TextView();
			 // while data is loading
			 oTextView.setText("Please wait we are loading you chart...");
			 var hasData = false;
			 var closingBal="";
			 if(this.model.oData.businessData.length!=0){
				 for(var i=0;i<this.model.oData.businessData.length;i++){
					 if(hasData){
						 break;
					 }
					 closingBal=this.model.oData.businessData[i].map.displayClosingBalanceValue;
					 if(closingBal!=null && closingBal != 0){
						 hasData = true;
						 // need to set this as chart use this data for rendering
						// this.model.oData.businessData[i].displayClosingBalance.amountValue.currencyStringNoSymbol =closingBal; 
					 }
				 }
				if(hasData){
					messageHolder.setVisible(false);
					noBalanceText.setVisible(false);
					var oChartConfig = {
						chartName : "Favourite Accounts",
						x_axisName : "Account",
						y_axisName : "Closing Balance",
						x_axisDataProperty : "displayText",
						y_axisDataProperty : "map/displayClosingBalanceValue",
						dataPath : "/businessData"
					}
	 
					var oBarChart = this.createColumnChart(oChartConfig);
	 				barContainer.destroyContent(); 
					barContainer.addContent(oBarChart);
				 }else{
					 messageHolder.setVisible(false);
					 noBalanceText.setVisible(true);
				 }
			 }else{
				 noBalanceText.setVisible(false);
			 }
			 
	},
	
	onBeforeRendering: function(){
		//jQuery.sap.log.info("onBeforeRendering");
		this.initializeModel();
	},

	onAfterRendering: function(){
		//jQuery.sap.log.info("onAfterRendering");
	},

	onExit: function(){
		//jQuery.sap.log.info("onExit");
	},
	
	onFavAcctsBarChartSettingsClick:function (){
		ns.home.changeFavAccountPortletView('chartSettings');
	}
});
