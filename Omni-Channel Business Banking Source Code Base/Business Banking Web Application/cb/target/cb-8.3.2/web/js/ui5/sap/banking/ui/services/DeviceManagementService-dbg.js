/**
* DeviceManagementService class
* This class provices the REST communication to oData mbanking services
*/
jQuery.sap.declare("sap.banking.ui.services.DeviceManagementService");
jQuery.sap.require("sap.banking.ui.services.BankingService");

sap.banking.ui.services.BankingService.extend("sap.banking.ui.services.DeviceManagementService",{

	getDevice: function(deviceId,successCallback,errorCallback){
		jQuery.sap.log.info("DeviceManagementService-getDevice");
	},

	getDevices: function(successCallback,errorCallback){
		jQuery.sap.log.info("DeviceManagementService-getDevices");
		$.ajax(this.uri).done(successCallback || this.successHandler).fail(errorCallback || this.errorHandler);
	},

	verifyDevice: function(oDevice,successCallback,errorCallback){
		jQuery.sap.log.info("verifyDevice");
	},

	createDevice: function(oDevice,successCallback,errorCallback){
		jQuery.sap.log.info("DeviceManagementService-createDevice");
	},

	modifyDevice: function(oDevice,successCallback,errorCallback){
		jQuery.sap.log.info("DeviceManagementService-modifyDevice");
	},

	deleteDevice: function(deviceId,successCallback,errorCallback){
		jQuery.sap.log.info("DeviceManagementService-deleteDevice");
	}

});
