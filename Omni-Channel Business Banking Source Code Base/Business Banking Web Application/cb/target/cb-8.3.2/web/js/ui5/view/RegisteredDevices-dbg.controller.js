jQuery.sap.require("sap.banking.ui.services.DeviceManagementService");
jQuery.sap.require("sap.banking.ui.services.ServiceConstants");

sap.ui.controller("view.RegisteredDevices", {
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.RegisteredDevices
*/
	service: undefined,	

	deviceList: undefined,

	eventBus:sap.ui.getCore().getEventBus(),

	onInit: function() {
		this.getView().setBusy(true);
		this.initializeService();	
		
		var that = this;
		var successHandler = function(d){
			that.deviceList = d.value;
			that.getView().setBusy(false);
		}

		var errorHandler = function(d){
			that.getView().setBusy(false);
		}

		this.service.getDevices(successHandler,errorHandler);	
	},

	/**
	* This function initializes the service instance for device managent
	*/	
	initializeService: function(){
		var constant = sap.banking.ui.services.ServiceConstants;
		var uri = constant.deviceManagement.URI;
		var userName = "username";
		var password = "password";
		this.service = new sap.banking.ui.services.DeviceManagementService(uri,userName,password);
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.RegisteredDevices
*/
	onBeforeRendering: function() {

	},

	testDevice: function(deviceId){
		console.info("testDevice" + deviceId);
	},

	deleteDevice: function(deviceId){
		console.info("deleteDevice" + deviceId);
		$( "#deleteDeviceDialog" ).dialog({
			resizable: true,
			modal: true,
			width:400,
			buttons: {
				"Delete": function() {
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
	},

	editDevice: function(deviceId){
		console.info("editDevice" + deviceId);
		var device = this.getDeviceDetails(deviceId);		
		this.eventBus.publish("ModifyDeviceChannel","editDetails",device);
	},

	registerEvents: function(){
		var that = this;

		var actionButtonsClickHandler = function(e){
			var $actionLink = $(e.currentTarget);
			var deviceId = $actionLink.attr("ux-data-device-id");
			var action = $actionLink.attr("ux-data-action-type");
			if(action === "test"){
				that.testDevice(deviceId);
			}else if(action === "edit"){
				that.editDevice(deviceId);
			}else if(action === "delete"){
				that.deleteDevice(deviceId);
			}
		};
		$(document).on("click","#registeredDevicesGrid a[ux-data-column-type=action]",actionButtonsClickHandler);

	},
/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.RegisteredDevices
*/
	onAfterRendering: function() {
		console.info("controller onAfter rendering");
		$("#registeredDevicesGrid").datagrid({
			url:"./data/devices.json",
			datatype: "json",
		   	colNames:['Id','Name', 'Number', 'Carrier','Status','Action'],
		   	colModel:[
		   		{name:'id',index:'id', width:50,align:"right"},
		   		{name:'name',index:'name', width:150,align:"center"},
		   		{name:'number',index:'number', width:150,align:"center"},
		   		{name:'carrier',index:'carrier', width:100,align:"center"},
		   		{name:'status',index:'status', width:100,align:"center"},
		   		{name: 'action',index:'action',width:100,align:"right",formatter:this.actionColumnFormatter}
		   	],
		   	rowNum:10,
		   	rowList:[10,20,30],
		   	pager: "#registeredDevicesPager",
		   	sortname: 'id' /* Initial sort name*/,
		    viewrecords: true,
		    sortorder: "asc",
		    caption:"Registered Devices",

		});

		this.registerEvents();
	},

	actionColumnFormatter: function(cellvalue, options, rowObject){
		console.info("controller-actionColumnFormatter");
		var testLink = "<a class='ui-button' ux-data-column-type='action' ux-data-device-id='" + rowObject.id + "' ux-data-action-type='test'><span class='ui-icon ui-icon-mail-closed'></span></a>";
		var editLink = "<a class='ui-button' ux-data-column-type='action' ux-data-device-id='" + rowObject.id + "' ux-data-action-type='edit'><span class='ui-icon ui-icon-pencil'></span></a>";
		var deleteLink = "<a class='ui-button' ux-data-column-type='action' ux-data-device-id='" + rowObject.id + "' ux-data-action-type='delete'><span class='ui-icon ui-icon-trash'></span></a>";
		return testLink + editLink + deleteLink;
	},

	getDeviceDetails: function(deviceId){		
		var device = {};
		for(var i=0;i<this.deviceList.length;i++){
			if(this.deviceList[i].id === deviceId){
				device = this.deviceList[i];
				break;
			}	
		}
		return device;
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.RegisteredDevices
*/
	onExit: function() {

	}

});