/**
* Controller for charts
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.ConsolidatedBalanceDonutChart",{	

	model:undefined,
	
	onInit: function(){
		//jQuery.sap.log.info("onInit");
		this.initResourceModel("charts");
		this.initializei18NModel();
	},

	initializei18NModel: function(){	
		//Register device i18n model
		this.getView().setModel(this.resourceModel, "i18n");
	},
	
	// call the action and copies data in model 
	initializeModel:function(){		
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
				url:'/cb/pages/jsp/home/ConsolidatedBalancePortletAction_loadConBalData.action?isChart=true',
				containerId:"ConsolidatedBalancePortletContainer",
				success: function(data) {

					hasData=false;
					// convert data as required
					var newDataModel = controllerHandler.updateAmounts(data);
					
					// set data in json model
					controllerHandler.model.setData({
	                      businessData : newDataModel
					 });
					
					// set data in view
					controllerHandler.getView().setModel(controllerHandler.model);
				},
				error : function(jqXHR, sTextStatus, oError) {
					console.info(sTextStatus);
				},
				complete : function(jqXHR,textStatus){
					// once the call is complete render the donut
					controllerHandler.renderDonut();
					//controllerHandler.showBusyIndicator(false);
				}
		}
		this.getChartData(oConfig);
	},

	renderDonut: function(){
		// pie charts holder
		var pieContainer = this.byId("pieContainerConsolidatedBalance");
		// div holder
		var messageHolder = this.byId("textConsolidatedBalance");
		// note holder
		var noteHolder = this.byId("donutChartNoteLabel");
		var oTextView = new sap.ui.commons.TextView();
		// while data is loading
		oTextView.setText("Please wait we are loading you chart...");
		if(hasData){
				messageHolder.setVisible(false);
				var dimensionName = 'Balance';
				var oChartConfig = {
					chartName : "Consolidated Balance",
					legendSectionName : "Account Group",
					legendValueProperty : "ChartAccountDisplayText",
					dataValueProperty : "balanceAmountValue",
					dataSectionName : "Balance",
					dataPath : "/businessData"
				}
				var oPieChart = this.createDonutChart(oChartConfig);
				pieContainer.destroyContent(); 
				pieContainer.addContent(oPieChart);
		}else{
			 pieContainer.setWidth("0%");
			 noteHolder.setVisible(false);
		}	
	},
	
	hasData : false,
	
	updateAmounts : function(data) {
		
		for(var i=0;i < data.gridModel.length; i++) {
			var model = data.gridModel[i];
			data.gridModel[i]["ChartAccountDisplayText"]= this.getChartAccountDisplayText(data.gridModel[i].displayTextAccountsType, data.gridModel[i].balance);
			if(model.balance){
				var amountNoComma = model.balance.replace(/,/g, "");
				data.gridModel[i].amountString = amountNoComma;
			}else{
				data.gridModel[i].amountString = 0;
			}
			var displayValue = data.gridModel[i].balance;
			/*if(data[i].balanceAmountValue<0){
				displayValue = "("+displayValue+")";
			}*/
			data.gridModel[i].displayTextAccountsType = data.gridModel[i].displayTextAccountsType+" : "+displayValue;
			//if(data[i].amountString>0){
				hasData=true;
			//}
		}
		return data.gridModel;
	},

	onAfterRendering: function(){
		//jQuery.sap.log.info("onAfterRendering");
	},
	
	onBeforeRendering: function(){
		//jQuery.sap.log.info("onBeforeRendering donut");
		window.location.hash = "";
		//this.showBusyIndicator(true);
		this.initializeModel();
	},
	
	onExit: function(){
		//jQuery.sap.log.info("onExit");
	},

	//This function sets the busy indicator on parent portlet
	showBusyIndicator: function(bBusy){
		var oView = this.getView();
		//var $portletContainer = $("#" + oView.getId()).closest(".portlet-content");
		var $portletContainer = $("#ConsolidatedBalancePortletContainer").closest(".portlet-content");
		if($portletContainer){
			if(bBusy){
				$portletContainer.setBusy(true);
			}else{
				$portletContainer.setBusy(false);
			}	
		}		
	}
});
