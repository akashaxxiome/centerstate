/**
* BankingService
* Descripton BankingService, its base service for all banking service implementations, It will make use SAPUI5 oData model
* 			 for communication to oData services over REST.	
* @namespace sap.banking.ui.services.BankingService
* TODO:
*		1. cache if required
*		2. callbacks
*		3. validation error handling
*/

jQuery.sap.declare("sap.banking.ui.services.BankingService");
sap.ui.base.Object.extend("sap.banking.ui.services.BankingService",{
	uri: undefined,
	userName: undefined,
	password: undefined,
	oModel:null,
	useJson: true,
	ACTIONS:{
		INIT:"init",
		VERIFY:"verify",
		CONFIRM:"confirm"
	},
	entities:{},
	functionImports:{},
	logger:jQuery.sap.log,

	/**
	* @constructor for BankingService
	* @param sParams {Object} Accepts the service initialization parameters
	*/
	constructor: function(sParams){
		this.logger.info("BankingService-Constructor");
		if(!sParams){
			throw new Error("Error in initializing service, please provide service URI");
		}
		this.uri = sParams.uri;
		this.userName = sParams.userName;
		this.password = sParams.password;		
		this.entities = sParams.entities;
		this.functionImports  = sParams.functionImports;
		this.oModel = new sap.ui.model.odata.ODataModel(this.uri, this.useJson);
		this.oModel.setHeaders({"Cache-Control":"no-cache, no-store"}); //Prevent IE from caching
		this.oModel.setHeaders({"Pragma":"no-cache"});//Prevent IE from caching
		this.oModel.refreshSecurityToken(); 
	},


	/**
	* This api returns the service end point
	* @returns {String} Service Endpoint
	*/
	getEndPoint: function(){
		return this.uri;
	},

	/**
	* This method sets the odata model
	*/
	setModel: function(oDataModel){
		this.oModel = oDataModel;
	},

	/**
	* This method returns the oModel
	*/
	getModel: function(){
		return this.oModel;
	},

	/**
	* This function sets the entities supported by service
	*/
	getEntities: function(oEntities){
		this.entities = oEntities;
	},	


	/**
	* This function returns the entities supported by service
	*/
	getEntities: function(){
		return this.entities;
	},

	/**
	* This API implements oDATA read.
	* @param {String} entity for read operation
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	read: function(entityName,fnSuccess,fnError){
		this.logger.info("BankingService>read");
		var that = this;
		var successCallBack = function(response){
			that.logger.info("BankingService>read>success");
			that.successHandler(that,response,fnSuccess);
		}

		var errorCallBack = function(error){	
			that.logger.info("BankingService>read>error");
			that.errorHandler(that,error,fnError);
		}

		var currentDate = new Date();
		var currentTime = currentDate.getTime();

		this.oModel.read(entityName,null,{_:currentTime},true,successCallBack,errorCallBack);
	},


	/**
	* This API implements the create call for entity on oDATA service
	* @param {String} entity name
	* @param {String} oEntry object for create operation
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	create: function(entityName,oEntry,fnSuccess,fnError){
		this.logger.info("BankingService>create");
		var that = this;
		var params = {
			success:function(response){
				that.logger.info("BankingService>create>success");
				that.successHandler(that,response,fnSuccess);
			},
			error:function(error){
				that.logger.info("BankingService>create>error");
				that.errorHandler(that,error,fnError);
			}
		};

		this.oModel.create(entityName,oEntry,params);
	},


	/**
	* This API implements the modify call for entity on oDATA service
	* @param {String} entity name
	* @param {String} id name
	* @param {Object} oEntry object for modify operation
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	modify: function(entityName,id,oEntry,fnSuccess,fnError){
		this.logger.info("BankingService>modify");
		var that = this;

		var modifyEntity;
		if(typeof id === "string"){
			modifyEntity = entityName + "('" + id +"')" ;	
		}else{
			modifyEntity = entityName + "(" + id +")" ;
		}

		var params = {
			success:function(response){
				that.logger.info("BankingService>modify>success");
				that.successHandler(that,response,fnSuccess);
			},
			error:function(error){
				that.logger.info("BankingService>modify>error");
				that.errorHandler(that,error,fnError);
			}
		};

		this.oModel.update(modifyEntity, oEntry, params);
	},


	/**
	* This API implements oData delete on oDATA service
	* @param {String} entity name
	* @param {String} id of entity to be deleted
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	remove: function(entityName,id,fnSuccess,fnError){
		this.logger.info("BankingService>remove>id:" + id);
		if(!entityName){
			throw new Error("Provde entity to be deleted");
		}

		if(!id){
			throw new Error("Provde entity id to be deleted");	
		}
		
		var that = this;
		var successCallBack = function(response){
			that.logger.info("BankingService>remove>success");
			that.successHandler(that,response,fnSuccess);
		}

		var errorCallBack = function(error){	
			that.logger.info("BankingService>remove>error");
			that.errorHandler(that,error,fnError);
		}

		var deleteEntity;
		if(typeof id === "string"){
			deleteEntity = entityName + "('" + id +"')" ;	
		}else{
			deleteEntity = entityName + "(" + id +")" ;
		}
		this.oModel.remove(deleteEntity,null,successCallBack,errorCallBack);
	},

	/**
	* This API implements call to function import on oDATA service
	* TODO: oModel.callFunction is not working properly with our function import
	* 		implementation currently call is been made via read which should be changed
	*		later.
	* @param {String} sFunctionName function name
	* @param {Object} mParameters paramers for the service
	* @param {String} sMethod HTTP method GET/POST
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	callFunction: function(sFunctionName,mParameters,fnSuccess,fnError){
		this.logger.info("BankingService>call");

		if(!sFunctionName){
			throw new Error("Function import name can not be empty!");
		}

		var that = this;
		var successCallBack = function(response){
			that.logger.info("BankingService>call>success");
			that.successHandler(that,response,fnSuccess);
		}

		var errorCallBack = function(error){	
			that.logger.info("BankingService>call>error");
			that.errorHandler(that,error,fnError);
		}


		//this.oModel.callFunction(sFunctionName, "GET", {Name : 'ImmediateAlert' }, null, successCallBack, errorCallBack) ;  
		this.oModel.read(sFunctionName, null, mParameters, true, successCallBack, errorCallBack);
		
	},

	/*******************************************************************
	* Wizard API's
	* For Wizard implementations use below API, this is how it works
	* init:   initilization step, the bean whether before adding new or modifying existing 
	*		  comes preffiled from server with some default properties
	* verify: verification step, the initialzed object is modified on screen with user updating
	*		  its properties, such a bean would get validated/verified on server and would expect error back
	* confirm: confirmation, a successfully validated object would be done when in last step i.e confirm step
	*		   wich provides confirmed bean
	********************************************************************/

	/**
	* This API implements the init functionality for wizard
	* @param {String} entity name
	* @param {Object} oEntry object of entity to be initialized
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	init: function(entityName,oEntry,fnSuccess,fnError){
		this.logger.info("BankingService>init");
		var that = this;
		var params = {
			success:function(response){
				that.logger.info("BankingService>init>success");
				that.successHandler(that,response,fnSuccess);
			},
			error:function(error){
				that.logger.info("BankingService>init>error");
				that.errorHandler(that,error,fnError);
			},
			urlParameters:{
				"action" : this.ACTIONS.INIT
			}
		};

		this.oModel.create(entityName,oEntry,params);
	},

	/**
	* This API implements the verify functionality for wizard implementation
	* @param {String} entity name
	* @param {Object} oEntry object of entity to be verified
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	verify: function(entityName,oEntry,fnSuccess,fnError){
		this.logger.info("BankingService>verify");
		var that = this;
		var params = {
			success: function(response){
				that.logger.info("BankingService>verify>success");
				that.successHandler(that,response,fnSuccess);
			},
			error:function(error){
				that.logger.info("BankingService>verify>error");
				that.errorHandler(that,error,fnError);		
			},
			urlParameters:{
				"action" : this.ACTIONS.VERIFY
			}
		}
		this.oModel.create(entityName,oEntry,params);
	},

	/**
	* This API implements the wizard confirm functionality
	* @param {String} entityName name of entity which would be confirmed
	* @param {Object} oEntry object of entity to be confirmed
	* @param {function} fnSuccess success callback
	* @param {function} fnError error callback
	*/
	confirm: function(entityName,oEntry,fnSuccess,fnError){
		this.logger.info("BankingService>confirm");
		var that = this;
		var params = {
			success: function(response){
				that.logger.info("BankingService>confirm>success");
				that.successHandler(that,response,fnSuccess);
			},
			error:function(error){
				that.logger.info("BankingService>confirm>error");
				that.errorHandler(that,error,fnError);	
			},
			urlParameters:{
				"action" : this.ACTIONS.CONFIRM
			}
		}
		this.oModel.create(entityName,oEntry,params);
	},

	/********************* Wizard API's End *****************************/

	/**
	* Base success handler
	*/
	successHandler: function(scope,response,fnSuccess){
		this.logger.info("BankingService>successHandler");
		var data;
		if(response){
			data = [response];
		}
		if(fnSuccess){
			fnSuccess.apply(this,data);
		}else{
			var successMessage = "Your request has been processed successfully.";
			$("#actionMessageContainer").html(successMessage).removeClass("errorResponse hidden").addClass("responseMessage successResponse");
		}
	},

	/**
	* Base error handler	
	*/
	errorHandler: function(scope,oError,fnError){
		scope.logger.info("BankingService>errorHandler");
		//Show the error message globally
		if(oError.response && oError.response.body){
			var errorObj;
			try{
				errorObj  = JSON.parse(oError.response.body);
			}catch(e){
				//Some parsing exception
				errorObj = {
					error:{
						code:"SE",
						message:""
					}
				}
			}
			
			var error = errorObj.error;
			if(error.code === "UAF"){
				//User Authentication failed invalidate users session and logout
				window.location.href = JS_INVALIDATE_SESSION_URL;
			}else if(error.code !== "VE"){
				var errorMessage = error.message.value;
				if(errorMessage === "undefined" || errorMessage == ""){
					errorMessage = "Error in processing your request, please try again later.";
				}
				$("#actionMessageContainer").html(errorMessage).removeClass("hidden successResponse").addClass("responseMessage errorResponse");
			}

			//If error handler is passed call it
			if(fnError){
				fnError.apply(this,[error]);
			}
		}
	}
});