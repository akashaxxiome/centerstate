/**
* Description:SmsSenderService, Extends to BankingService.
* This service provides service APIs for SMS related functionalities
* @class SmsSenderService
* @extends BankingService
* @singleton SmsSenderService Service is implemented as Singleton instance
* @namespace sap.banking.ui.services.SmsSenderService
*/
(function($,window,undefined){
	"use strict";
	jQuery.sap.declare("sap.banking.ui.services.SmsSenderService");
	jQuery.sap.require("sap.banking.ui.services.BankingService");
	jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
	
	sap.banking.ui.services.SmsSenderService = (function() {

			var uniqueInstance;//Singleton instance

			/**
			* @Constructor, creates new BankingService instance of type SmsSenderService
			*/
			function constructor() {
				var CONSTANTS = sap.banking.ui.services.ServiceConstants.SMSSENDERSERVICE;

				//Augment Banking Service
				var Service = sap.banking.ui.services.BankingService.extend("SmsSenderService",{

					/** 
					* This API sends the test message to mobile device
					* @param {String} message test message to be sent
					* @param {Array} msisdns list of MSISDN numbers for which message will be sent
					* @param {Function} fnSuccess Service success callback
					* @param {Function} fnError Service error callback
					*					ErrorObject in error callback
					*/
					sendTestMessage: function(message,msisdns,fnSuccess,fnError){
						console.info("SmsSenderService>sendTestMessage");
						if(!message){
							throw new Error("Message is required");
						}

						if(!msisdns && msisdns.length == 0){
							throw new Error("MSISDN list required");
						}

						//msisdns = [{"Value" : "8989898989"}]
						var listOfMsisdns = [];
						for(var i = 0; i <  msisdns.length; i++){
							listOfMsisdns.push({"Value": msisdns[i] + "" });
						}
						var oEntry = {
							Message: message,
							Msisdns: listOfMsisdns,
							Type: "MT",
							CharSet:"7b"
						};
						
						this.create(this.entities.SMSTEXTMESSAGES,oEntry,fnSuccess,fnError);
					}					

				});

				//Service Parameters
				var sParams = {
					uri: CONSTANTS.URI,
					entities:CONSTANTS.ENTITIES,
					functionImports: CONSTANTS.FUNCTIONIMPORTS
				}
				return new Service(sParams);
			}
		return {
			getInstance: function() {
				if(!uniqueInstance) { // Instantiate only if the instance doesn't exist.
					uniqueInstance = constructor();
				}
				return uniqueInstance;
			}
		}
	})();
}(jQuery,window));