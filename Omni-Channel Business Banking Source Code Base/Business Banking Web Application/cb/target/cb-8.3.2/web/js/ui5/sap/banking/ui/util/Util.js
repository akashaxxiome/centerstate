/**
 * Utility Class
 * Provides some utility helper methods
 */
(function($, sap, window, undefined) {

    "use strict";

    //Define NameSpace
    $.sap.declare("sap.banking.ui.util.Util");

    //Static Util Class
    sap.banking.ui.util.Util = (function() {

        var instance = {};

        var DATE_FORMAT = "MM/dd/yyyy";


        /***********************************************************
         *					Private functions
         ************************************************************/
        /*function toTitleCase(str) {
            return str.replace(/(?:^|\s)\w/g, function(match) {
                return match.toUpperCase();
            });
        }*/



        /***********************************************************
         *					Public functions
         ************************************************************/
        instance.formatDate = function(sDate) {
            var formattedDate;
            var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: DATE_FORMAT
            });
            formattedDate = dateFormat.format(sDate);
            return formattedDate;
        };

        return instance;

    }());

}(jQuery, sap, window));