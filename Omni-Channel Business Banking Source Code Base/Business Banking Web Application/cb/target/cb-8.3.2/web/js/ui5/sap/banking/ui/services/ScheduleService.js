/**
* Description:ScheduleService, Extends to BankingService.
* @class ScheduleService
* @extends BankingService
* @singleton ScheduleService Service is implemented as Singleton instance
* @namespace sap.banking.ui.services.ScheduleService
*/
(function($,window,undefined){
	"use strict";
	jQuery.sap.declare("sap.banking.ui.services.ScheduleService");
	jQuery.sap.require("sap.banking.ui.services.BankingService");
	jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
	
	sap.banking.ui.services.ScheduleService = (function() {

			var uniqueInstance;//Singleton instance

			/**
			* @Constructor, creates new BankingService instance of type ScheduleService
			*/
			function constructor() {
				var CONSTANTS = sap.banking.ui.services.ServiceConstants.SCHEDULESERVICE;

				//Augment Banking Service
				var Service = sap.banking.ui.services.BankingService.extend("ScheduleService",{

					SCHEDULER_GROUP: "DEFAULT",

					//TODO: Add Service API
					getTimeZones: function(fnSuccess,fnError){
						/*var timeZones = [{
							"Id":1,
							"Name":"Asia/Kolkata",
							"Offset":"+05:30"
						},{
							"Id":2,
							"Name":"America/New_York",
							"Offset":"-05:00"
						},{
							"Id":3,
							"Name":"Europe/London",
							"Offset":"+00:00"
						},{
							"Id":4,
							"Name":"Asia/Shanghai",
							"Offset":"+08:00"
						}];

						return timeZones;*/
						this.read(this.entities.TIMEZONES,fnSuccess,fnError);
					},

					//TODO: Add Service API
					getIntervals: function(){
						var intervals = [{
							"Id":0,
							"Name":"Absolute"
						},{
							"Id":1,
							"Name":"From start of the month"
						},{
							"Id":2,
							"Name":"From end of the month"
						},{
							"Id":3,
							"Name":"Semi Monthly"
						}];

						return intervals;
					},

					/**
					* This API returns the available JobType list, which are system defined job types
					*/
					getJobTypes: function(fnSuccess,fnError){
						console.info("ScheduleService>getJobTypes");
						this.read(this.entities.JOBTYPES,fnSuccess,fnError);
					},

					/**
					* This API returns the job configuration by job id
					*/
					getJobConfiguration: function(jobTypeId,fnSuccess,fnError){
						console.info("ScheduleService>getJobConfiguration");
						if(!jobTypeId){
							throw new Error("Job type id is required");
						}
						var entity = this.entities.SCHEDULED_JOBS + "('" + jobTypeId + "')" + "?$expand=JobProperties"; 
						this.read(entity,fnSuccess,fnError);
					},
					

					/**
					* This API creates new schedule with passed scheduling information
					* @param {Object} schedule object
					* @returns {Object} schedule
					*/
					scheduleJob: function(schedule,fnSuccess,fnError){
						console.info("ScheduleService>scheduleJob");
						if(!schedule){
							throw new Error("Job is required for scheduling!!!");
						}

						var metaData = {	
							"type" : "com.sap.banking.schedule.endpoint.v1_0.beans.ScheduledJob"
						};

						schedule.__metadata = metaData;
						var jobProperties = schedule.JobProperties;
						if(jobProperties && jobProperties.results){
							schedule.JobProperties = jobProperties.results;
						}
						
						//schedule.IntervalInDays = Number(schedule.IntervalInDays);
						//schedule.IntervalInMonths = Number(schedule.IntervalInMonths);
						
						//schedule.StartTime = "2014-07-07T08:28:55.629Z";
						//schedule.StartTime = "2013-06-01T15:30:00+09:30";
						var offset = this.getTimeZoneOffset(schedule.TimeZoneId);
						if(schedule.StartTime){
							var startTime = this.getISODateFormat(schedule.StartTime,offset);						
							schedule.StartTime = startTime;
						}

						if(schedule.EndTime){
							var endTime = this.getISODateFormat(schedule.EndTime,offset);						
							schedule.EndTime = endTime;
						}
						

						this.create(this.entities.SCHEDULED_JOBS,schedule,fnSuccess,fnError);
					},

					getISODateFormat: function(date,offset){
						var dateTime = new Date(parseInt(date.substr(6)));
						var isoDateFormat,year,month,day,hours,minutes,seconds;
						year = dateTime.getFullYear();
						month = dateTime.getMonth()+1;
						month = (month < 10) ? "0" + month : month;
						
						day = dateTime.getDate();
						day = (day < 10) ? "0" + day : day;

						hours = dateTime.getHours();
						hours = (hours < 10) ? "0" + hours : hours;

						minutes = dateTime.getMinutes();
						minutes = (minutes < 10) ? "0" + minutes : minutes;						

						seconds = dateTime.getSeconds() 
						seconds = (seconds < 10) ? "0" + seconds : seconds;

						offset = (offset) ?  offset : "";

						isoDateFormat = year + "-" + month + "-" + day + "T" + hours + ":" + minutes + ":" + seconds + offset;
						return isoDateFormat;
					},

					getTimeZoneOffset: function(timeZoneId){
						/*var timeZones =  this.getTimeZones();
						var len = timeZones.length;
						var offset;
						for(var i=0; i< len; i++){
							if(timeZones[i].Name === timeZoneId){
								offset = timeZones[i].Offset;
								break;
							}
						}
						return offset;*/
						return "";
					},

					/**
					* This API returns all sheduled jobs by user
					* @returns {Array} list of sheduled jobs
					*/
					getScheduledJobs: function(fnSuccess,fnError){
						console.info("ScheduleService>getScheduledJobs");
						var entityPath = this.entities.SCHEDULED_JOBS + "?$filter=Group eq '" + this.SCHEDULER_GROUP + "'";
						this.read(entityPath,fnSuccess,fnError);
					},

					/**
					* This API returns the scheduled job object with passed job id
					* @param {String} jobId
					* @returns {Object} scheduled job
					*/
					getScheduledJob: function(jobId,fnSuccess,fnError){
						//"http://localhost:9082/cb/odata/scheduleservice/ScheduledJobs?$filter=Name eq 'Unread Bank Message Alert'"
						console.info("ScheduleService>getScheduledJob");
						if(!jobId){
							throw new Error("job id is required");
						}
						var entityPath = this.entities.SCHEDULED_JOBS + "?$expand=JobProperties&$filter=Name eq '" + jobId + "'and Group eq '" + this.SCHEDULER_GROUP + "'";
						var _fnSuccess =  function(response){								
								var data;
								if(response && response.results){
									//Update the repeat count to -1 in case if interval type is other than absolute
									if(response.results[0].IntervalType !== "0" && response.results[0].RepeatCount === 0){
										response.results[0].RepeatCount = -1;
									}
									data = [response];
								}
								if(fnSuccess){
									fnSuccess.apply(this,data);
								}
						}
						this.read(entityPath,_fnSuccess,fnError);
					},

					/**
					* This API modifies the scheduled job
					* @param {Object} scheduled job
					*/
					modifyScheduledJob: function(scheduledJob,fnSuccess,fnError){
						console.info("ScheduleService>modifyScheduledJob");
						if(!scheduledJob){
							throw new Error("Scheduled Job is required!!!");
						}

						var metaData = {	
							"type" : "com.sap.banking.schedule.endpoint.v1_0.beans.ScheduledJob"
						};

						scheduledJob.__metadata = metaData;
						
						var jobId = scheduledJob.Name;
						var jobProperties = scheduledJob.JobProperties;
						scheduledJob.JobProperties = [];
						if(jobProperties && jobProperties.results){
							var properties  = jobProperties.results;
							var len = properties.length;
							for(var i=0; i<len;i++){
								var property = {
									Key:properties[i].Key,
									Value:properties[i].Value,
								}
								scheduledJob.JobProperties.push(property);
							}							
						}

						this.modify(this.entities.SCHEDULED_JOBS,jobId,scheduledJob,fnSuccess,fnError);
					},

					/**
					* This API deletes the scheduled job with passed job id
					* @param {String} scheduledJob id
					*/
					deleteScheduledJob: function(sheduledJobId,fnSuccess,fnError){
						console.info("ScheduleService>deleteScheduledJob");
						if(!sheduledJobId){
							throw new Error("Job id is required!!!");
						}
						this.remove(this.entities.SCHEDULED_JOBS,sheduledJobId,fnSuccess,fnError);
					},

					/**
					* This API immediately runs the scheduled job
					*/
					runScheduledJob: function(jobName,fnSuccess,fnError){
						console.info("ScheduleService>runScheduledJob");
						if(!jobName){
							throw new Error("Name is required!!!");
						}						
						
						var params = {
							"Name":"'" + jobName   + "'"						
						}

						this.callFunction(this.functionImports.RUNIMMEDIATEJOB, params, fnSuccess, fnError);

					},

					/**
					* This API returns scheduler logs
					* @returns {Array} list of sheduled jobs
					*/
					getSchedulerLogs: function(searchCriteria,fnSuccess,fnError){
						console.info("ScheduleService>getScheduledJobs");
						var entityPath;

						searchCriteria = {
							"JobName" : "ProcessAccount",
							"Status" : "INITIATED",
							"StartDate": "09/12/2014",
							"EndDate" : "09/17/2014"
						}
						if(!searchCriteria){		
							entityPath = this.entities.SCHEDULER_LOGS;	
						}else{
							//filter=Country_Region_Code eq 'ES' and Payment_Terms_Code eq '14 DAYS'

							//Status Filter
							//var entityPath = this.entities.SCHEDULER_LOGS + "?$filter=Status eq '" + searchCriteria.Status + "'";

							//Job Name Filter
							var entityPath = this.entities.SCHEDULER_LOGS + "?$filter=JobName eq '" + searchCriteria.JobName + "'";

							//Date Filter
							//var entityPath = this.entities.SCHEDULER_LOGS + "?$filter=StartDate eq '" + searchCriteria.StartDate + "' and EndDate eq '" + searchCriteria.EndDate + "'";
							
						}

						
						this.read(entityPath,fnSuccess,fnError);
					}
				});

				//Service Parameters
				var sParams = {
					uri: CONSTANTS.URI,
					entities:CONSTANTS.ENTITIES,
					functionImports: CONSTANTS.FUNCTIONIMPORTS
				}
				return new Service(sParams);
			}
		return {
			getInstance: function() {
				if(!uniqueInstance) { // Instantiate only if the instance doesn't exist.
					uniqueInstance = constructor();
				}
				return uniqueInstance;
			}
		}
	})();
}(jQuery,window));