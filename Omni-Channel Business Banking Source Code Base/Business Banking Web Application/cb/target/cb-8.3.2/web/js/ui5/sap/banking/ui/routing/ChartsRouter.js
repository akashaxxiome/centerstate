/**
* Consolidated Balance Router Class
* This router defines the routes for charts used for consolidated balance
*/

jQuery.sap.declare("sap.banking.ui.routing.ChartsRouter");
jQuery.sap.require("sap.banking.ui.core.routing.ApplicationRouter");
jQuery.sap.require("sap.banking.ui.core.routing.AppRouter");
jQuery.sap.require("sap.banking.ui.routing.Routers");

(function(window,undefined){
	var chartsRoute = [/*{
			name:"_registeredDevices",
			pattern:"registeredDevices",			
			views:[{
				view:"view.RegisteredDevices",
				viewType:"HTML",			    		
				div:"summary"
			}]
		},*/{
			name:"_favoriteAccountsBarChart",
			pattern:"favoriteAccountsBarChart",
			viewPath : "view",
			view : "FavoriteAccountsBarChart",
			viewType:"HTML",
			targetControl:"favAccUI5ColChartContainer",
			targetAggregation:"content"			
		},{
			name:"_favoriteAccountsDonutChart",
			pattern:"favoriteAccountsDonutChart",
			viewPath : "view",
			view : "FavoriteAccountsDonutChart",
			viewType:"HTML",
			targetControl:"favAccUI5DonutChartContainer",
			targetAggregation:"content"
		},{
			name:"_cashFlowSummaryBarChart",
			pattern:"cashFlowSummaryBarChart",			
			views:[{
				view:"view.CashFlowSummaryBarChart",
				viewType:"HTML",
				div:"cashFlowBarChart"
			}]
		},{
			name:"_cashFlowSummaryDonutChart",
			pattern:"cashFlowSummaryDonutChart",			
			views:[{
				view:"view.CashFlowSummaryDonutChart",
				viewType:"HTML",
				div:"cashFlowDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_consolidatedBalanceBarChartPortal",
			pattern:"consolidatedBalanceBarChartPortal",			
			views:[{
				view:"view.ConsolidatedBalanceBarChart",
				viewType:"HTML",
				div:"consolidatedBalanceSummaryBarChart"
			}]
		},{
			/*this is used to show charts from home page/home page portlets*/
			name:"_consolidatedBalanceBarChart",
			pattern:"consolidatedBalanceBarChart",
			targetControl:"conBalanceUI5ColContainer",
			targetAggregation:"content",
			viewPath:"view",
			view : "ConsolidatedBalanceBarChart",
			viewType:"HTML"
		},{
			
			name:"_consolidatedBalanceDonutChart",
			pattern:"consolidatedBalanceDonutChart",
			targetControl:"conBalanceUI5DonutContainer",
			targetAggregation:"content",
			viewPath:"view",
			view : "ConsolidatedBalanceDonutChart",
			viewType:"HTML"
		},{
			/*this is used to show charts after going into the module*/
			name:"_consolidatedBalanceDonutChartPortal",
			pattern:"consolidatedBalanceDonutChartPortal",			
			views:[{
				view:"view.ConsolidatedBalanceDonutChart",
				viewType:"HTML",
				div:"consolidatedBalanceSummaryDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_DepositAccountDonutChartPortal",
			pattern:"depositAccountDonutChartPortal",			
			views:[{
				view:"view.DepositAccountsDonutChart",
				viewType:"HTML",
				div:"depositAccHolderDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_DepositAccountBarChartPortal",
			pattern:"depositAccountBarChartPortal",			
			views:[{
				view:"view.DepositAccountsBarChart",
				viewType:"HTML",
				div:"depositAccHolderBarChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_AssetAccountDonutChartPortal",
			pattern:"assetAccountDonutChartPortal",			
			views:[{
				view:"view.AssetAccountsDonutChart",
				viewType:"HTML",
				div:"assetAccHolderDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_AssetAccountBarChartPortal",
			pattern:"AssetAccountBarChartPortal",			
			views:[{
				view:"view.AssetAccountsBarChart",
				viewType:"HTML",
				div:"assetAccHolderBarChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_CreditCardsAccountDonutChartPortal",
			pattern:"creditCardsAccountDonutChartPortal",			
			views:[{
				view:"view.CreditCardsDonutChart",
				viewType:"HTML",
				div:"creditCardsAccHolderDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_CreditCardsAccountBarChartPortal",
			pattern:"creditCardsAccountBarChartPortal",			
			views:[{
				view:"view.CreditCardsBarChart",
				viewType:"HTML",
				div:"creditCardsAccHolderBarChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_LoanAccountDonutChartPortal",
			pattern:"loanAccountDonutChartPortal",			
			views:[{
				view:"view.LoanAccountsDonutChart",
				viewType:"HTML",
				div:"loanAccHolderDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_LoanAccountBarChartPortal",
			pattern:"loanAccountBarChartPortal",			
			views:[{
				view:"view.LoanAccountsBarChart",
				viewType:"HTML",
				div:"loanAccHolderBarChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_OtherAccountDonutChartPortal",
			pattern:"otherAccountDonutChartPortal",			
			views:[{
				view:"view.OtherAccountsDonutChart",
				viewType:"HTML",
				div:"otherAccHolderDonutChart"
			}]
		},{
			/*this is used to show charts after going into the module*/
			name:"_OtherAccountBarChartPortal",
			pattern:"otherAccountBarChartPortal",			
			views:[{
				view:"view.OtherAccountsBarChart",
				viewType:"HTML",
				div:"otherAccHolderBarChart"
			}]
		}];
		chartsRouter = new sap.banking.ui.core.routing.ApplicationRouter(chartsRoute);
		chartsRouter.register(sap.banking.ui.routing.Routers.CHARTS);
		chartsRouter.initialize();
})(window);