jQuery.sap.declare("sap.banking.ui.core.routing.ApplicationRouter");
jQuery.sap.require("sap.ui.core.routing.Router");
(function(){
	var oRouters = {};
		
	sap.ui.core.routing.Router.extend("sap.banking.ui.core.routing.ApplicationRouter",  {

		_baseRoutes : [{
			name:"none",
			pattern:""
		}],
		
		_routes : {}, 

		constructor : function(oRoutes, oConfig, oOwner) {	
			//extend the routes with base routes	
			if(!oRoutes){				
				oRoutes = [];
			}
			this._routes = oRoutes;
			oRoutes = oRoutes.concat(this._baseRoutes);
			// super		
			sap.ui.core.routing.Router.apply(this, arguments);
		}
	});
	
	
	/**
	 * Registers the router to access it from another context. Use sap.ui.routing.Router.getRouter() to receive the instance
	 * 
	 * @param {String} Name of the router
	 * @public
	 */
	sap.banking.ui.core.routing.ApplicationRouter.prototype.navOut = function () {
		window.location.hash = "";
	};
	
	/**
	 * Registers the router to access it from another context. Use sap.ui.routing.Router.getRouter() to receive the instance
	 * 
	 * @param {String} Name of the router
	 * @public
	 */
	sap.banking.ui.core.routing.ApplicationRouter.prototype.register = function (sName) {
		oRouters[sName] = this;
		return this;
	};
	
	/**
	 * Get a registered router
	 * 
	 * @param {String} Name of the router
	 * @return {sap.ui.core.routing.Router} The router with the specified name, else undefined
	 * @public
	 */
	sap.banking.ui.core.routing.ApplicationRouter.getRouter = function (sName) {
		var oRouter = oRouters[sName];
		oRouter.routes = sap.banking.ui.core.routing.ApplicationRouter.getRoutes(sName);
		return oRouters[sName];
	};

	/**
	 * Get a registered routes
	 * 
	 * @param {String} Name of the router
	 * @return {Object} The route object with the specified router name, else undefined
	 * @public
	 */
	sap.banking.ui.core.routing.ApplicationRouter.getRoutes = function (sName) {
		var router = oRouters[sName];
		var oRoutes = router._routes;
		var definedRoutes = {};

		//Generate dynamic constant for routing names on router
		if(oRoutes){
			for (var i = 0; i < oRoutes.length; i++) {
				definedRoutes[oRoutes[i].name] = oRoutes[i].name;
			};
		}
		return definedRoutes;
	};
	
})();