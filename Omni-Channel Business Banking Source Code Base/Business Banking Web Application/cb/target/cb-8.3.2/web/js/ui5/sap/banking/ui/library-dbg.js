/*!
 * SAP UI development toolkit for HTML5 (SAPUI5/OpenUI5)
 * (c) Copyright 2009-2015 SAP SE or an SAP affiliate company.
 * Licensed under the Apache License, Version 2.0 - see LICENSE.txt.
 */

/**
 * Initialization Code and shared classes of library sap.ui.suite.
 */
sap.ui.define(['jquery.sap.global', 
	'sap/ui/core/library'], // library dependency
	function(jQuery) {

	"use strict";

	/**
	 * Suite controls library.
	 *
	 * @namespace
	 * @name sap.ui.suite
	 * @author SAP SE
	 * @version 1.29.0-20150421153001.0+sha.b679518
	 * @public
	 */
	
	// delegate further initialization of this library to the Core
	sap.ui.getCore().initLibrary({
		name : "sap.banking.ui",
		version: "1.29.0-20150421153001.0+sha.b679518",
		dependencies : ["sap.ui.core","sap.ui.commons"],
		types: [],
		interfaces: [],
		controls: [			
			"sap.banking.ui.common.BankingForm",
			"sap.banking.ui.common.Common",
			"sap.banking.ui.common.DatePicker",
			"sap.banking.ui.common.ExtDateRangePicker",
			"sap.banking.ui.common.Help",
			"sap.banking.ui.common.HoverButton",
			"sap.banking.ui.common.LookupBox",
			"sap.banking.ui.common.LookupBoxRenderer",
			"sap.banking.ui.common.ModuleMenu",
			"sap.banking.ui.common.SettingsMenu",
			"sap.banking.ui.common.layout.DashboardLayout",
			"sap.banking.ui.commons.WizardStepIndicator",
			"sap.banking.ui.commons.Tab",
			"sap.banking.ui.commons.TabLayout",
			"sap.banking.ui.core.mvc.BankingController",
			"sap.banking.ui.core.routing.ApplicationRouter",
			"sap.banking.ui.core.routing.AppRouter",
			"sap.banking.ui.routing.Routers",
			"sap.banking.ui.routing.ChartsRouter",
			"sap.banking.ui.routing.DeviceManagementRouter",
			"sap.banking.ui.routing.StopsRouter",
			"sap.banking.ui.services.BankingService",
			"sap.banking.ui.services.DeviceManagementService",
			"sap.banking.ui.services.ScheduleService",
			"sap.banking.ui.services.ServiceConstants",
			"sap.banking.ui.services.ServiceFactory",
			"sap.banking.ui.services.SmsSenderService",
			"sap.banking.ui.services.StopsService",
			"sap.banking.ui.util.Map",
			"sap.banking.ui.util.Util",
			"sap.banking.ui.util.Validator"
		],
		elements: []
	});
	
	return sap.banking.ui;

}, /* bExport= */ false);
