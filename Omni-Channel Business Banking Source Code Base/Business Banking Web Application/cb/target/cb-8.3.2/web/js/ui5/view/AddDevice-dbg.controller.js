/**
* Controller for add device wizard
*/
sap.ui.controller("view.AddDevice",{	
	
	currentStep:0,

	$tabWiget:undefined,

	onInit: function(){
		console.info("AddDeviceController-onInit");
		this.getView().setBusy(true);
		
		var data = {
			"name":"",	
			"country":"",
			"number":"",
			"carrier":"",
			"terms":"",
			"countries":[{
				"name":"USA",
				"carriers":[]
				},{
				"name":"IN",
				"carriers":[]}],
			"carriers":[{
					"id":1,
					"name":"AT&T"
				},{
					"id":2,
					"name":"TMOBILE"
				},{
					"id":3,
					"name":"VERIZON"
				},{
					"id":4,
					"name":"SPRINT"
				},{
					"id":5,
					"name":"VODAFONE"
				},{
					"id":6,
					"name":"AIRTEL"
				}]
		};

		var oModel = new sap.ui.model.json.JSONModel();
		oModel.setData(data);
		this.getView().setModel(oModel);
	},

	//TODO: can this fit for banking view
	initializeJQWidgets: function(){
		//TODO:Initialize the portlet for details

		//Initializing the jq tabs for wizard
		var tabId = this.getTabId();
		var $tab = $("#"+tabId).tabs().tabs("disable").tabs("enable",0);		
		this.setTabWidget($tab);
	},

	onBeforeRendering: function(){
		console.info("AddDeviceController-onBeforeRendering");
		//this.getView().setBusy(true);
	},

	onAfterRendering: function(){
		console.info("AddDeviceController-onAfterRendering");
		this.initializeJQWidgets();		
		this.getView().setBusy(false);
	},

	verifyDevice:function(){
		//TODO: Make service call to validate the device
		this.moveToNextStep();
	},

	saveDevice:function(){
		this.moveToNextStep();
	},

	confirmDevice: function(){
		//TODO:close the wizard and change the view
	},

	cancelWizard:function(){
		//TODO: clear the data model and change the  view
	},

	moveToPrevStep:function(){
		this.currentStep--;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	moveToNextStep:function(){
		this.currentStep++;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	getTabId:function(){
		var tabs = this.getView().byId("deviceWizard");
		return tabs.sId;
	},

	getTabWidget: function(){
		return this.$tabWiget;
	},

	setTabWidget: function($tab){
		this.$tabWiget = $tab;
	},

	onExit: function(){
		console.info("AddDeviceController-onExit");
	}	
});