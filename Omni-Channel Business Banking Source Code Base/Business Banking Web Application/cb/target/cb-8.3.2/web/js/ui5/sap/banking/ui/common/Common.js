/**
* Banking Common Class
*/

jQuery.sap.declare("sap.banking.ui.common.Common");
jQuery.sap.require("sap.banking.ui.util.Validator");

sap.banking.ui.common.Common = (function($,window,undefined){

	var instance = {};

	var AUTOHIDE_TIMEOUT = 1500;//Millis

	var ERRORCONSTANTS = {
		ERROR_MODEL: "ERRORS",
		ERROR_TYPES: {
			VALIDATION_ERROR:"VE",
			SERVICE_ERROR:"SE",
			BUSINESS_ERROR:"BE"
		}
	};

	/***********************************************************
	*					Private functions
	************************************************************/	
	function toTitleCase(str) {
		return str.replace(/(?:^|\s)\w/g, function(match) {
		    return match.toUpperCase();
		});
	}

	/**
	* This function creates the error model and sets to passed view
	*/
	function setErrorModel(oErrors,oView){
		//console.info("instance.setErrorModel");
		if(oErrors && oView){
			var errors = {};
			for(var key in oErrors){
				var modifiedKey = toTitleCase(key);
				var value = oErrors[key];
				errors[modifiedKey] = value;
			}


			var eModel = new sap.ui.model.json.JSONModel();				
			eModel.setData(errors);
			oView.setModel(eModel,ERRORCONSTANTS.ERROR_MODEL);			
		}
	}

	function removeEmptyBlankErrors(oErrors){
		var errors = {}
		for ( var prop in oErrors) {
			var aProp = $.trim(prop);
			var aValue = oErrors[aProp];
			if(aProp != "" && aValue != ""){
				errors[aProp] = aValue;
			}else if(aProp =="" && aValue !=""){
				errors["formError"] = aValue;
			}
		}
		return errors;
	}

	
	/***********************************************************
	*					Public functions
	************************************************************/
	instance.showValidationError = function(validationResult){
		var control = null;
		for ( var prop in validationResult) {
			//Set Error CSS and tooltip
			control = sap.ui.getCore().byId(prop);
			if(control && control != null){
				control.setTooltip(validationResult[prop]);
				control.addStyleClass("validationError");
				if(typeof control.getValueState === 'function'){
					control.setValueState(sap.ui.core.ValueState.Error);
				}
			}
		}
	},
	
	instance.clearValidationError = function(view){
		var errorControls = $(".validationError");
		var counter = 0, id = null, control= null;
		for(counter = 0; counter < errorControls.length; counter++){
			id = errorControls[counter].id;
			control = sap.ui.getCore().byId(id);
			if(control){
				control.removeStyleClass("validationError");
				control.setTooltip("");
				if(typeof control.getValueState === 'function'){
					control.setValueState(sap.ui.core.ValueState.Success);
				}
			}
		}
	},

	instance.handleSuccessMessage = function(oSuccess,oView){
		this.showSuccess(oSuccess);
	},

	instance.showSuccessMessage = function(message){
		var actionMessageContainer = $("#actionMessageContainer");
		if(actionMessageContainer){
			actionMessageContainer.hide();			
			actionMessageContainer.html(message)
			.removeClass("errorResponse hidden")
			.addClass("responseMessage successResponse")
			.fadeIn("slow")
			.delay(AUTOHIDE_TIMEOUT).fadeOut();
		}
	},
	
	instance.handleErrorMessage = function(oError,oView){
		//sap.banking.ui.util.Validator
		//Show the error message globally
		if(oError.response && oError.response.body){
			var errorObj  = JSON.parse(oError.response.body);
			var error = errorObj.error;
			if(error.code !== "VE"){
				var validationErrorRes = JSON.parse(error.message.value);
				var errorMessage = validationErrorRes["errorMessage"];
				if(errorMessage === "undefined" || errorMessage == ""){
					errorMessage = "Error in processing your request, please try again later.";
				}
				this.showErrorMessage(errorMessage);
			}else{
				this.handleError(error, oView);
			}
			
		}
	},

	instance.showErrorMessage = function(message){
		if(message){
			$("#actionMessageContainer")
			.html(message)
			.removeClass("hidden successResponse")
			.addClass("responseMessage errorResponse")
			.fadeIn("slow")
			.delay(AUTOHIDE_TIMEOUT).fadeOut();
		}
	},

	instance.handleError = function(oError, oView){
		this.removeErrors(oView);
		var errorResponse = {};
		 if(oError.code === ERRORCONSTANTS.ERROR_TYPES.VALIDATION_ERROR){
			 var validationErrorRes = JSON.parse(oError.message.value);
			 var ve = validationErrorRes.VE;
			 ve = removeEmptyBlankErrors(ve);
			 setErrorModel(ve,oView);
		 }
	},

	/**
	* This function removes the error model attached to view
	*/
	instance.removeErrors = function(oView){
		//console.info("removeErrorModel");
		if(oView){
			var viewModel = oView.getModel(ERRORCONSTANTS.ERROR_MODEL)
			if(viewModel){
				viewModel.setData({});	
			}	
		}			
	},

	/**
	* This function loads the i18n resource model
	*/
	instance.getResourceModel = function(file){
		var resourceModel;
		resourceModel =new sap.ui.model.resource.ResourceModel({bundleUrl: JS_APP_ROOT + "/i18n/" + file,bundleLocale:JS_LOCALE_NAME});
		return resourceModel;
	}

	return instance;

})(jQuery,window);