/**
* DeviceManagementRouter Class
* This router defines the routes for device management
*/

jQuery.sap.declare("sap.banking.ui.routing.DeviceManagementRouter");
jQuery.sap.require("sap.banking.ui.core.routing.ApplicationRouter");
jQuery.sap.require("sap.banking.ui.core.routing.AppRouter");
jQuery.sap.require("sap.banking.ui.routing.Routers");

(function(window,undefined){
	var deviceManagementRoutes = [{
			name:"_registeredDevices",
			pattern:"registeredDevices",			
			views:[{
				view:"view.RegisteredDevices",
				viewType:"HTML",			    		
				div:"summary"
			}]
		},{
			name:"_addDevice",
			pattern:"addDevice",			
			views:[{
				view:"view.AddDevice",
				viewType:"HTML",
				div:"details"
			}]
		},{
			name:"_modifyDevice",
			pattern:"modifyDevice/{id}",			
			views:[{
				view:"view.ModifyDevice",
				viewType:"HTML",
				div:"details"
			}]
		},{
			name:"_editDevice",
			pattern:"editDevice/{id}",			
			views:[{
				view:"view.ModifyDevice",
				viewType:"HTML",
				div:"details"
			}]
		}];
	deviceManagementRouter = new sap.banking.ui.core.routing.ApplicationRouter(deviceManagementRoutes);
	deviceManagementRouter.register(sap.banking.ui.routing.Routers.DEVICEMANAGEMENT);
	deviceManagementRouter.initialize();
})(window);