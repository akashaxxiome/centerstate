sap.ui.jsfragment("fragment.CarrierTermsDialog", { 
	createContent: function(oController ) {	
		var header,message1,message2,message3,message4,message5,message6;
		var resourceModel = oController.getResourceModel().getResourceBundle();
		header =  resourceModel.getText("HEADER");
		message1 =  resourceModel.getText("MESSAGE1");
		message2 = resourceModel.getText("MESSAGE2");
		message3 = resourceModel.getText("MESSAGE3");
		message4 = resourceModel.getText("MESSAGE4");
		message5 = resourceModel.getText("MESSAGE5");
		message6 = resourceModel.getText("MESSAGE6");
		//TODO: get the i18n messages here
		var html = '<div id="carrierTermsAndConditionsContainer" class="staticContentDialog addMobileTermsPage">' + 
		'<h2 align="center">' + header + '</h2>' + 
		'<h4>' +  message1 + '</h4>' +
		'<p>' +  message2 + '</p>' +
						'<p>' +  message3 + '</p>' +
						'<p>' +  message4 + '</p>' +
						'<p>' +  message5 + '</p>' +
						'<p>' +  message6 + '</p>' +
						'</center>' + 
					'</div >';
	
		// create the HTML control for Sample 2 (id and variable name are fixed)
		var dialogHtml = new sap.ui.core.HTML("carrierTermsDialog", {
			content : html,
			preferDOM : false,
			afterRendering : function(e) {
				//TODO
				/*if ( !e.getParameters()["isPreservedDOM"] ) {
					var $=e.getSource().$();
					$.click(function(e) {
						addColorBlockAtCursor($, e, 64, 8);
					});
				}*/
			}
		});
		
		return dialogHtml;
	} 
});