sap.ui.jsfragment("fragment.ModuleSubNavigationMenu",{
	fragmentName: "fragment.ModuleSubNavigationMenu",

	createContent: function(oController){
		console.info("createContent-" + this.fragmentName);
		var that = this;
		//$.sap.log.info("createContent-" + this.fragmentName);
		var subNavMenus = oController.getSubNavigationMenus();
		if(subNavMenus){
			var menusItems = [];
			for (var i = 0; i < subNavMenus.length; i++) {
				menusItems.push({
					key:subNavMenus[i].key, 
					text:subNavMenus[i].text
				})
			};			
			var moduleSubNavigationMenu = new sap.ui.ux3.NavigationBar(this.createId("subMenuNavControl"),{
	            items:menusItems,
	            selectedItem:menusItems[0].key,
				select: function(oEvent) {
					if(oController.onSubNavigationSelect){
						oController.onSubNavigationSelect.apply(oController,[oEvent,that]);	
					}					
					/*if (oEvent.getParameter("item").getKey() == "item5") {
						oEvent.preventDefault();
						alert("Navigation to item 5 is not allowed.")
					}*/
				}
	        });	
			//moduleSubNavigationMenu.addStyleClass("todo-moduleSubNavigation");

	        var oModuleSubMenuLayout = new sap.ui.layout.HorizontalLayout({
				content: [moduleSubNavigationMenu]
			});
	        oModuleSubMenuLayout.addStyleClass("todo-moduleSubNavigationContainer");

	        return oModuleSubMenuLayout;
		}
	}
});