jQuery.sap.declare("sap.banking.ui.commons.TabLayout");

sap.ui.core.Control.extend("sap.banking.ui.commons.TabLayout", {      // call the new Control type "my.Hello" 
                                              // and let it inherit from sap.ui.core.Control
    metadata : {                              // the Control API
        properties : {
            "name" : "string"                 // setter and getter are created behind the scenes, 
                                              // including data binding and type validation
        },
        defaultAggregation : "tabs",
		aggregations : {
	    	"tabs" : {type : "sap.banking.ui.commons.Tab", multiple : true, singularName : "tab"}
		},
    },

    //TODO:onAfterRendering intilize jq call
    
    renderer : function(oRm, oControl) {      // the part creating the HTML
        oRm.write("<span>Hello ");
        oRm.writeEscaped(oControl.getName()); // write the Control property 'name', with XSS protection
        oRm.write("</span>");
    }
});