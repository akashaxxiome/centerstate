/**
* Controller for charts of cah flow
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.CashFlowSummaryDonutChart",{

	model:undefined,

	dimensionName: undefined,
	
	onInit: function(){
		//jQuery.sap.log.info("onInit");
		this.initResourceModel("charts");
		this.initializei18NModel();
	},

	initializei18NModel: function(){	
		//Register device i18n model
		this.getView().setModel(this.resourceModel, "i18n");
	},
	
	initializeModel:function(){
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig  = {
			url:"/cb/pages/jsp/home/CashFlowPortletAction_loadCashFlowData.action?displayType=pie&isChart=true",
			containerId: "cashFlowPortletContainer",
			success: function(data) {
				hasData = false
				//console.log("ajax success");
				controllerHandler.dimensionName = data.userdata.displayDimension;
								 
				// convert data as required
				var newDataModel = controllerHandler.formatData(data.gridModel,controllerHandler.dimensionName);
				// set data in json model
				controllerHandler.model.setData({
                      businessData : newDataModel
				 });
				// set data in view
				controllerHandler.getView().setModel(controllerHandler.model);
			},
			error : function(jqXHR, sTextStatus, oError) {
				console.info("error");
			},
			complete : function(jqXHR,textStatus){
				// once the call is complete render the donut
				controllerHandler.renderDonutChart();
			}
		};
		 
		this.getChartData(oConfig); // we need to change the name as it is not clear that it is giving AJAX call internally
	},
	
	renderDonutChart : function (){
		 var dataPropertyArray =[];
		 var displayDimension ="";
		if(this.dimensionName=="openBal"){
			displayDimension = "Opening Balance";
			dataPropertyArray.push({
				name : 'Opening Balance',
                value: "openingBalString"
			});
		}
		else if(this.dimensionName=="totalDebits"){
			displayDimension = "Total Debits";
			dataPropertyArray.push({
				 name : 'Total Debits',
				 value: "totalDebits"
			});
		}
		else if(this.dimensionName=="totalCredits"){
			displayDimension = "Total Credits";
			dataPropertyArray.push({
				 name : 'Total Credits',
				 value: "totalCredits"
			});
		}
		else if(this.dimensionName=="closeBal"){
			displayDimension = "Closing Balance";
			dataPropertyArray.push({
				  name : 'Closing Balance',
				  value: "closingBalString"
			});
		}
		else if(this.dimensionName=="items"){
			displayDimension = "# of Items";
			dataPropertyArray.push({
				 name : '# of Items',
				 value: "itemsCount"
			});
		} 

  		//pie charts holder for cash flow
  		var pieContainer = this.byId("pieContainerCashflow");
  		
  		// div holder
  		var messageHolder = this.byId("messageHolderPieCashflow");
  		
  		// div holder holder
  		var noBalanceText = this.byId("noBalanceTextPieCashflow");
  		
  		// note holder
  		var noteHolder = this.byId("donutChartNoteLabel");
  		
  		// while data is loading
  		var oTextView = new sap.ui.commons.TextView();
		oTextView.setText("Please wait we are loading you chart...");

		if(this.model.oData.businessData.length!=0){
			if(hasData){
				messageHolder.setVisible(false);
				noBalanceText.setVisible(false);
				var curCode="account/currencyCode";
				var oChartConfig={
						chartName : "Cash Flow "+displayDimension,
						legendSectionName : "Account Number",
						legendValueProperty :"ChartAccountDisplayText",
						dataValueProperty : dataPropertyArray[0].value,
						dataSectionName : dataPropertyArray[0].name,
						dataPath : "/businessData"
				}
				var oPieChart = this.createDonutChart(oChartConfig);
				pieContainer.destroyContent(); 
				pieContainer.addContent(oPieChart);
			}else{
				
				messageHolder.setVisible(false);
				noBalanceText.setText("No "+  displayDimension + " in Selected Accounts.");
				noBalanceText.setVisible(true);
				noteHolder.setVisible(false);
			}
		 }else{
			 noBalanceText.setVisible(false);
			 noteHolder.setVisible(false);
		 }
  		
	},
	
	getDisplayDimensionName : function(dimension) {
		var name = '';
		if(dimension) {
			if(dimension == 'openBal') {
				name = "openingBalString";
			} else if(dimension == 'totalDebits') {
				name =  "totalDebits";
			} else if(dimension == 'totalCredits') {
				name = "totalCredits";
			} else if(dimension == 'closeBal') {
				name = "closingBalString";
			} else if(dimension == 'items') {
				name = "itemsCount";
			}
		}
		if(name) {
			return name;
		} else {
			return '0';
		}
	},

	hasData:false,
	
	formatData : function(data,selectedDimension) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			var selectedDimensionValueProperty = this.getDisplayDimensionName(selectedDimension);
			data[i]["ChartAccountDisplayText"]= this.getChartAccountDisplayText(data[i].account.accountDisplayText, data[i][selectedDimensionValueProperty]); 
			if(model.closingBalString){
				var closingBalNoComma = model.closingBalString.replace(/,/g, "");
				if(model.closingBalColor == "negativetrue"){
					data[i].closingBalString = "-"+closingBalNoComma;	
				}else {
					data[i].closingBalString = closingBalNoComma;
				}
			}else{
				data[i].closingBalString = 0;
			}
			
			if(model.itemsCount){
				var itemsCountNoComma = model.itemsCount.replace(/,/g, "");
				data[i].itemsCount = itemsCountNoComma;
			}else{
				data[i].itemsCount = 0;
			}
			
			if(model.totalDebits){
				var totalDebitsNoComma = model.totalDebits.replace(/,/g, "");
				data[i].totalDebits = totalDebitsNoComma;
			}else{
				data[i].totalDebits = 0;
			}
			
			if(model.totalCredits){
				var totalCreditsNoComma = model.totalCredits.replace(/,/g, "");
				data[i].totalCredits = totalCreditsNoComma;
			}else{
				data[i].totalCredits = 0;
			}
			
			if(model.openingBalString){
				var openingBalStringNoComma = model.openingBalString.replace(/,/g, "");
				if(model.openingBalColor == "negativetrue"){
					data[i].openingBalString = "-"+openingBalStringNoComma;
				} else {
					data[i].openingBalString = openingBalStringNoComma;
				}
			}else{
				data[i].openingBalString = 0;
			}
			if(data[i].closingBalString != 0 || data[i].itemsCount != 0 || data[i].totalDebits != 0 || data[i].totalCredits != 0 || data[i].openingBalString != 0){
				hasData=true;
			}
		}
		return data;
	},

	onBeforeRendering: function(){
		//jQuery.sap.log.info("onBeforeRendering");
		window.location.hash = "";
		this.initializeModel();
	},

	onAfterRendering: function(){
		//jQuery.sap.log.info("onAfterRendering");
	},

	onExit: function(){
		//jQuery.sap.log.info("onExit");
	},
	
	onCashFlowSummaryDonutChartSettingsClick:function (){
		ns.home.changeCashFlowPortletView('chartSettings');
	}
});