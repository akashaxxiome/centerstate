/**
* Controller for charts of cash flow
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.FavoriteAccountsDonutChart",{	

	model:undefined,
	
	onInit: function(){
		this.initResourceModel("charts");
		this.initializei18NModel();
		//jQuery.sap.log.info("onInit");
	},

	initializei18NModel: function(){	
		//Register device i18n model
		this.getView().setModel(this.resourceModel, "i18n");
	},
	
	initializeModel:function(){
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
			url:"/cb/pages/jsp/home/FavoriteAccountsPortletAction_loadFavoriteAccountsChartData.action?isChart=true",
			success: function(data) {
				//console.log("ajax success");
				// set data in json model
				controllerHandler.model.setData({
                      businessData : data.gridModel
				 });
				var newDataModel = controllerHandler.updateData(controllerHandler.model);
				// set data in view
				controllerHandler.getView().setModel(newDataModel);
			},
			error : function(jqXHR, sTextStatus, oError) {
				console.info("error");
			},
			complete:function(jqXHR, textStatus){
				controllerHandler.renderDonutChart();
			}
		};
		this.getChartData(oConfig);
	},

	updateData : function(data) {
		for(var i=0;i < data.oData.businessData.length; i++) {
			if(data.oData.businessData[i].displayClosingBalance!=undefined &&
					data.oData.businessData[i].displayClosingBalance.amountValue!=undefined){
				data.oData.businessData[i].displayText =
					data.oData.businessData[i].displayText+" : "+data.oData.businessData[i].displayClosingBalance.amountValue.currencyStringNoSymbol;//+"  -  ";//+model.oData.businessData[i].map.currencyCode;   
			}
		}
		return data;
	},
	
	renderDonutChart : function (){
			var dimensionName = 'Balance';
	  		var pieContainer = this.byId("pieContainerFavoriteAccount");
	  		var messageHolder = this.byId("messageHolderPieFavoriteAccount");
	  		var noBalanceText = this.byId("noBalanceTextPieFavoriteAccount");
	  		// note holder
	  		var noteHolder = this.byId("donutChartNoteLabel");
	  		var oTextView = new sap.ui.commons.TextView();
			 // while data is loading
			 oTextView.setText("Please wait we are loading you chart...");
			 var hasData = false;
			 var closingBal="";
			 if(this.model.oData.businessData && this.model.oData.businessData.length!=0){
				 for(var i=0;i<this.model.oData.businessData.length;i++){
					 if(hasData){
						 break;
					 }
					 closingBal=this.model.oData.businessData[i].map.displayClosingBalanceValue;
					 if(closingBal!=null  && closingBal != 0){
						 hasData = true;
						 //this.model.oData.businessData[i].displayClosingBalance.amountValue.currencyStringNoSymbol =closingBal; 
					 }
				 }
				 if(hasData){
					 messageHolder.setVisible(false);
					 noBalanceText.setVisible(false);
					 noteHolder.setVisible(true);
					var oChartConfig = {
						 chartName : "Favourite Accounts",
						 legendSectionName : "Account",
						 legendValueProperty : "displayText",
						 dataValueProperty : "map/displayClosingBalanceValue",
						 dataSectionName : "Balance",
						 dataPath : "/businessData"
					}
					var oPieChart = this.createDonutChart(oChartConfig);
					pieContainer.destroyContent(); 
					pieContainer.addContent(oPieChart);
				 }else{
					 messageHolder.setVisible(false);
					 noBalanceText.setVisible(true);
					 noteHolder.setVisible(false);
				 }
			 }else{
				 noBalanceText.setVisible(false);
				 noteHolder.setVisible(false);
			 }
	},
	
	onBeforeRendering: function(){
		//jQuery.sap.log.info("onBeforeRendering");
		this.initializeModel();
	},

	onAfterRendering: function(){
		//jQuery.sap.log.info("onAfterRendering");
	},

	onExit: function(){
		//jQuery.sap.log.info("onExit");
	},
	
	onFavAcctsDonutChartSettingsClick:function (){
		ns.home.changeFavAccountPortletView('chartSettings');
	}
	
});
