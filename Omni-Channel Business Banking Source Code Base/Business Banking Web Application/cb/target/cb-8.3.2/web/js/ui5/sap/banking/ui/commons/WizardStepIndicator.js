(function($, window) {
    $.sap.declare("sap.banking.ui.commons.WizardStepIndicator");
    $.sap.require("sap.ui.core.Control");

    sap.ui.core.Control.extend("sap.banking.ui.commons.WizardStepIndicator", {
        metadata: {
            properties: {
                "name": "string",
                "steps": {
                    type: "string[]"
                }
            }
        },

        controlId: "",

        renderer: function(rm, oControl) {
            $.sap.log.info("WizardStepIndicator-renderer");
            var steps = oControl.getSteps();
            var controlId = oControl.getId();
            rm.write("<div id='" + controlId + "' class='wizardStepIndicatorHolder'>");
            rm.write("<ul class='wizardProgressIndicatorCls'>");
            var cssClas = "";
            for (var i = 0; i < steps.length; i++) {
                if (i == 0) {
                    cssClas = "wizardStepSelectedIndicator active";
                } else {
                    cssClas = "wizardStepNormalIndicator";
                }
                rm.write("<li class='" + cssClas + "''><span>" + steps[i] + "</span></li>");
            }
            rm.write("</ul>");
            rm.write("</div>");
        },

        selectStep: function(stepId) {
            var selector = "#" + this.getId() + " ul li:eq(" + stepId + ")";
            //$(selector).addClass("active").siblings().removeClass("active");
            var steps = this.getSteps();
            $(selector).siblings().removeClass("active");
            for (var i = 0; i <= stepId; i++) {
                selector = "#" + this.getId() + " ul li:eq(" + i + ")";
                $(selector).addClass("active")
            }
        }
    });
})(jQuery, window, undefined);