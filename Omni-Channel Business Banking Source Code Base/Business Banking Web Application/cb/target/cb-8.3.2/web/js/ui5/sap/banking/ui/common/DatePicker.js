/**
 * SAPUI5 custom hover control
 * This control extends the Button control and provides hover effect to button
 */
(function($, sap, window, undefined) {
    "use strict";

    $.sap.declare("sap.banking.ui.common.DatePicker");

    sap.ui.commons.TextField.extend("sap.banking.ui.common.DatePicker", {        

        //This function sets the date to U5 Control instance
        onDateSelect: function(selectedDate){
            this.setValue(selectedDate);
        }, 

        onAfterRendering: function() {
            //this.logger.info("onAfterRendering");
            var sId = this.getId();
            $("#" + sId).datepicker({
                showOn: "button",
                buttonImage: "images/calendar.gif",
                buttonImageOnly: true,
                onSelect: $.proxy(this.onDateSelect,this)
            });
        },

        renderer: {} /*This has no renderer*/

    });
}(jQuery, sap, window));