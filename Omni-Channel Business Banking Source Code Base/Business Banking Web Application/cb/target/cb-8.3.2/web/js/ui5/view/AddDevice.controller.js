/**
* Controller for add device wizard
* This controller manages listing of devices, add/modify/delete of mobile device
* Service: DeviceManagementService, it uses DeviceManagementService for communicating over REST
* 
*/
jQuery.sap.require("sap.banking.ui.services.ServiceFactory");
jQuery.sap.require("sap.banking.ui.services.DeviceManagementService");
jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
jQuery.sap.require("sap.banking.ui.util.Validator");
jQuery.sap.require("sap.banking.ui.common.Common");
sap.ui.controller("view.AddDevice",{	
	
	//Service instance
	service: null,

	oCountriesModel:null,

	oCountriesList: null,

	//i18n resource bundle
	i18NModel: null,
	
	common: null,

	currentStep:0,

	data:null,

	//Validator instance
	validator:null,

	//Logger instance
	logger: jQuery.sap.log,

	//Wizard tab's JQ instance
	$tabWiget:null,

	controllerName: "view.AddDevice",

	onInit: function(){
		this.common = sap.banking.ui.common.Common;
		this.logger.info("AddDeviceConroller-onInit");
		this.getView().setBusy(true);
		this.initializeService();
		
		//Init events
		var oBus = sap.ui.getCore().getEventBus();		
		oBus.subscribe("ModifyDeviceChannel","editDetails",this.editDeviceListner);

		//Validator
		this.validator = sap.banking.ui.util.Validator;

		//Store registered controllers
		if(registeredControllers){
			registeredControllers.put(this.controllerName, this);	
		}
	},

	/**
	* This function initializes the service instance for device managent
	*/	
	initializeService: function(){
		this.logger.info("AddDeviceConroller-initializeService");
		var CONSTANTS = sap.banking.ui.services.ServiceConstants.DEVICEMANAGEMENTSERVICE;
		this.service = sap.banking.ui.services.ServiceFactory.getService(CONSTANTS.NAME);
	},

	/**
	* This function initializes the device model and i18n model required for the view
	*/
	initializeModel: function(){
		this.logger.debug("AddDeviceConroller-initializeModel");
		this.initializeI18NModel();
		this.initializeCountriesModel();
		this.initializeDeviceModel();		
		//Reset Error model if errors are populated
		this.validator.removeErrors(this.getView());
	},

	initializeI18NModel: function(){
		this.logger.debug("initializeI18NModel");
		//Register device i18n model
		var i18NModel = this.common.getResourceModel("devicemanagement.properties");	
		this.getView().setModel(i18NModel,"i18n");
		this.i18NModel = i18NModel;
	},

	initializeDeviceModel: function(device){
		this.logger.debug("initializeDeviceModel");
		var that = this;
		var oView = this.getView();
		var oDevice = {
			__metadata:{
				"type" : "com.sap.banking.mobile.endpoint.v1_0.beans.MobileDevice"
			}
		};	
		if(device){
			oDevice = $.extend(oDevice,device);
		}
		var fnInitSuccess = function(response){
			that.logger.debug("initializeModel-fnInitSuccess");
			that.validator.removeErrors(oView);
			if(response){
				var jModel = new sap.ui.model.json.JSONModel();				
				jModel.setData(response);
				that.getView().setModel(jModel);	
				if(response.TokenRequired === true){
					that.byId("tokenFormElement").setVisible(true);
					that.byId("token").setValue('');
					var message = that.i18NModel.getResourceBundle().getText("DEVICE_REGISRATION_TOKEN_REQUIRED_MESSAGE")
					that.byId("tokenRequiredLabel").setText(message);					
				}else{
					that.byId("tokenFormElement").setVisible(false);
					that.byId("token").setValue('');
				}
			}				
		}

		var fnInitError = function(error){
			that.logger.error("initializeModel-fnInitError");
			that.validator.removeErrors(oView);
			that.byId("tokenFormElement").setVisible(false);
			that.byId("token").setValue('');
			that.validator.handleError(error, oView);
		}
		
		this.service.initDevice(oDevice,fnInitSuccess,fnInitError);
	},

	initializeCountriesModel: function(){
		this.logger.debug("initializeCountriesModel");
		var that = this;
		var fnCountriesSuccess = function(countries){
			that.logger.debug("fnCountriesSuccess-fnInitError");
			if(countries){
				var countriesModel = new sap.ui.model.json.JSONModel();
				that.oCountriesList = countries.results;
				countriesModel.setData(countries);
				that.getView().setModel(countriesModel,"COUNTRIES");				
			}				
		}

		var fnCountriesError = function(error){
			that.logger.error("fnCountriesSuccess-fnCountriesError-" + error);
		}

		this.service.getCountries(fnCountriesSuccess,fnCountriesError);
	},


	//TODO: can this fit for banking view
	initializeJQWidgets: function(){
		this.logger.info("initializeJQWidgets");
		$("#details").show();

		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);	
		
		//Hide the registered devices portlet
		$("#registeredDevicesGridContainer").portlet("fold");

		//Initialize the portlet
		var portletId = "#" + this.getGeneratedId("detailsPortlet");
		$(portletId).addClass("wizardSupportCls");
		$(portletId).css("margin-top", "20px");
		ns.common.initializePortlet(portletId, true);
		
		ns.common.updatePortletTitle(this.getGeneratedId("detailsPortlet")+"_portlet", this.i18NModel.getResourceBundle().getText("ADD_DEVICE_PORTLET_TITILE"), false);
		
		//Initialize the portlet
		/*
		var portletId = "#" + this.getGeneratedId("detailsPortlet");
		$(portletId ).portlet({
			bookmark: true,
			bookMarkCallback: function(){
				var path = $(portletId).find('.shortcutPathClass').html();
				var ent  = $(portletId).find('.shortcutEntitlementClass').html();
				ns.shortcut.openShortcutWindow( path, ent );
			},
			title: this.i18NModel.getResourceBundle().getText("ADD_DEVICE_PORTLET_TITILE"),
			generateDOM: true,
			helpCallback: function(){
				var helpFile = $('#summary').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
		});*/
		
		// Updating wizard color status
		var generatedDetailsPortletId = this.getGeneratedId("detailsPortlet");
		ns.common.gotoAddDeviceWizardStep(generatedDetailsPortletId, 1);
		
		$("#addDeviceBookmark").click(function(){
			ns.common.bookmarkThis(generatedDetailsPortletId+"_portlet");
		});
		
		$("#addDeviceHelp").click(function(){
			ns.common.showSAPUI5Help();
		});
	},

	onBeforeRendering: function(){
		if(mainApp.helpModule){
			mainApp.helpModule.setCurrentActiveHash(hasher.getHashAsArray());
		}
		this.logger.info("onBeforeRendering");
		this.currentStep = 0;
		this.resetView();
		this.initializeModel();
		this.resetHashPath();
		this.byId("termsContainer").setVisible(false);
	},

	onAfterRendering: function(){
		this.logger.info("onAfterRendering");
		this.initializeJQWidgets();		
		this.getView().setBusy(false);
	},

	getResourceModel: function(){
		return this.i18NModel;
	},
	
	viewTermsPressHandler: function(oEvent){
		this.logger.info("viewTermsPressHandler");
		//TODO: set data to fragment
		if(this.termsDialogContentFragment == null || !this.termsDialogContentFragment){
			var fragment = sap.ui.jsfragment("fragment.CarrierTermsDialog", this);  
			this.termsDialogContentFragment = fragment;	
		}

		if(this.termsDialogContentFragment){
			var html = this.termsDialogContentFragment.getContent();
			var titleMsg = this.i18NModel.getResourceBundle().getText("CARRIER_TERMS_TITLE")
			var okButtonLabel = this.i18NModel.getResourceBundle().getText("OK")
			this.byId("acceptTermsCheckBox").setEnabled(true);	
			var oButtons = {};
			oButtons.okButton = { text:okButtonLabel,click:function(){
				$( this ).dialog( "close" );
				}
			};
			$("#carrierTCDialogContainer").html(html);
			$("#carrierTCDialogContainer").dialog({
			  title:titleMsg,	
		      modal: true,
		      width:500,
		      height:500,
		      buttons:oButtons
		    });	
		}
		
	},

	checkTermsChangeHandler: function(oEvent){
		this.logger.info("checkTermsChangeHandler");
		var model = this.getView().getModel();
		if(model){
			var data = model.getData();
			var deviceId = data.DeviceId;
			var carrierCode = data.CarrierCode;
			var params = oEvent.getParameters();
			if(params.checked){
				this.acceptCarrierTerms(deviceId,carrierCode);		
			}else{
				this.declineCarrierTerms(deviceId);
			}	
		}		
	},

	countriesChangeHandler: function(e){
		this.logger.info("countriesChangeHandler");
		var selectedCountry = e.mParameters.selectedItem.getKey();
		var model = this.getView().getModel();
		var data,device;
		if(model){
			data = model.getData();
			device = {
			"CountryCode": selectedCountry,
			"DeviceName": data.DeviceName,
			"SubscriberNumber": data.SubscriberNumber
			}
		}
	
		this.initializeDeviceModel(device);
		var carriersModel = new sap.ui.model.json.JSONModel();
			var carriers;
			var len = this.oCountriesList.length;
			for(var i = 0; i < len; i++){
				if(this.oCountriesList[i].Code == selectedCountry){
					var mobileCarriers = this.oCountriesList[i].MobileCarriers;
					if(mobileCarriers){
						carriers = mobileCarriers;
					}
					break;
				}
			}				
			carriersModel.setData(carriers);
			this.getView().setModel(carriersModel,"CARRIERS");	

	},
	

	/**
	* Carriers change handler
	* NOTE: some carriers additonaly requires token
	*		to be validated, for this reason device
	*		bean is re-initialized
	*/
	carriersChangeHandler: function(e){
		this.logger.info("carriersChangeHandler");
		/*var params = e.getParameters();
		var selectedCarrierCode = e.mParameters.selectedItem.getKey();
		this.getView().getModel().setProperty("/CarrierCode",selectedCarrierCode);*/
		var selectedCarrierCode = e.mParameters.selectedItem.getKey();
		var model = this.getView().getModel();
		var data,device;
		if(model){
			data = model.getData();
			device = {
				"DeviceName": data.DeviceName,
				"CountryCode": data.CountryCode,
				"CarrierCode": selectedCarrierCode,
				"SubscriberNumber": data.SubscriberNumber
			}
		}
		
		this.byId("termsContainer").setVisible(true);
		this.initializeDeviceModel(device);
	},
	
	verifyDeviceHandler:function(oEvent){
		this.logger.info("verifyDeviceHandler");
		var that = this;
		var oView = this.getView();
		var carrierCode = this.byId("carrier").getSelectedKey();
		var oEntry = oView.getModel().getData();
		oEntry.CarrierCode = carrierCode;
		var fnSuccess = function(response){
			that.logger.info("verifyDeviceHandler-fnSuccess");
			// Updating wizard color status
			ns.common.gotoAddDeviceWizardStep(that.getGeneratedId("detailsPortlet"), 2);
			
			that.moveToNextStep();
			/*if(response){
				if(response.IsTokenRequired === true){
					that.byId("tokenFormElement").setVisible(true);
					that.byId("token").setValue('');
					var message = that.i18NModel.getResourceBundle().getText("DEVICE_REGISRATION_TOKEN_REQUIRED_MESSAGE")
					that.byId("tokenRequiredLabel").setText(message);					
				}else{
					that.moveToNextStep();		
				}
			}*/			
		}

		var fnError = function(error){
			that.logger.error("verifyDeviceHandler-fnError" + error);
			that.validator.handleError(error, oView);
		}
		
		this.service.verifyDevice(oEntry,fnSuccess,fnError);		
	},

	saveDeviceHandler:function(){
		this.logger.info("saveDeviceHandler");
		var that = this;
		var oEntry = {};
		var oView = this.getView();
		var model = oView.getModel().getData();
		oEntry.DeviceId = model.DeviceId;

		var fnSuccess = function(response){
			that.logger.info("saveDeviceHandler-fnSuccess");
			
			// Updating wizard color status
			ns.common.gotoAddDeviceWizardStep(that.getGeneratedId("detailsPortlet"), 3);
			
			that.moveToNextStep();
		}

		var fnError =  function(error){
			that.logger.error("saveDeviceHandler-fnError" + error);
			that.validator.handleError(error, oView);
		}
		this.service.confirmDevice(oEntry,fnSuccess,fnError);
	},

	confirmDeviceHandler: function(){
		var message = this.i18NModel.getResourceBundle().getText("DEVICE_ADD_SUCCESS_MESSAGE");
		ns.common.showStatus(message);
		var that = this;
		setTimeout(function(){
			that.cancelWizardHandler.apply(that);
		},300);		
	},
	resendTokenHandler: function(){
		var model = this.getView().getModel();
		var data,device;
		if(model){
			data = model.getData();
			device = {
				"DeviceName": data.DeviceName,
				"CountryCode": data.CountryCode,
				"CarrierCode": data.CarrierCode,
				"SubscriberNumber": data.SubscriberNumber
			}
		}
		
		this.byId("termsContainer").setVisible(true);
		this.initializeDeviceModel(device);
		var message = this.i18NModel.getResourceBundle().getText("DEVICE_REGISRATION_TOKEN_RESENT_MESSAGE");
		ns.common.showStatus(message);
	},
	cancelWizardHandler:function(){
		this.logger.info("cancelWizardHandler");
		var portletId = "#" + this.getGeneratedId("detailsPortlet");
		$(portletId).portlet("fold");
		$("#registeredDevicesGridContainer").show().portlet("expand");
		ns.common.refreshDashboard('dbMobileDevicesSummary');
		$("#details").hide();
		$("#summary").show();
		var router =  sap.banking.ui.core.routing.ApplicationRouter.getRouter("deviceManagement");
		//clearing hashtag
		router.navOut();
		
		//route and load mobile device summary
		router.navTo("_registeredDevices");

		var registeredDevicesController = registeredControllers.get("view.RegisteredDevices");
		if(registeredDevicesController){
			registeredDevicesController.reloadGridData();
		}		
	},

	backHandler: function(){
		this.moveToPrevStep();
	},

	subScriberchangeEventHandler: function(){
		var model = this.getView().getModel();
		var data,device;
		var carrierCode = this.byId("carrier").getSelectedKey();
		if(carrierCode){
		if(model){
			data = model.getData();
			
			device = {
				"DeviceName": data.DeviceName,
				"CountryCode": data.CountryCode,
				"CarrierCode": carrierCode,
				"SubscriberNumber": data.SubscriberNumber
			}
				this.byId("termsContainer").setVisible(true);
				this.initializeDeviceModel(device);
			}
		}
		
		
	},
	
	moveToPrevStep:function(){
		this.validator.removeErrors(this.getView());
		this.currentStep--;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	moveToNextStep:function(){
		this.currentStep++;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	getTabWidget: function(){
		return this.getView().byId("deviceWizard").getWidget();
	},

	/** Global Events */
	editDeviceListner: function(sChannelId, sEventId, oData){
	},

	/**
	* This function returns dynamically generated id for passed dom id
	* @param {String} sDomId dom id defined in HTML view
	*/
	getGeneratedId: function(sDomId){
		//console.info("getGeneratedId");
		var element = this.getView().byId(sDomId);
		var id;
		if(element){
			id = element.sId;
		}
		return id;
	},

	onExit: function(){
		//TODO: Time for JQ widgets to destroy
	},

	/********** Helper Functions *******************/
	isTokenRequiredFormatter: function(tokenRequired){
		this.logger.info("isTokenRequiredFormatter");
		var isTokenRequired = false;
		if(tokenRequired !== null){
			isTokenRequired = (tokenRequired == true || tokenRequired == "true" ) ? true : false;
		}

		return isTokenRequired;
	},

	resetView: function(){
		this.byId("deviceName").setValue('');
		this.byId("deviceNumber").setValue('');
		this.byId("country").setSelectedItemId(0);
		this.byId("carrier").setSelectedItemId(0);
		this.byId("acceptTermsCheckBox").setChecked(false);
		this.byId("acceptTermsCheckBox").setEnabled(false);
	},

	resetHashPath: function(){
	var routers = sap.banking.ui.routing.Routers;
		sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navOut();
	},
	
	/********** Service function calls ************/
	acceptCarrierTerms: function(deviceId,carrierCode){
		this.logger.debug("acceptCarrierTerms");
		var that = this;
		var fnSuccess = function(response){
			that.logger.info("Terms have been acceppted successfully!");
		}

		var fnError =  function(error){
			that.logger.error("Error in accepting terms: " + error);
		}
		this.service.acceptDeviceRegistrationCarrierTerms(deviceId,carrierCode,fnSuccess,fnError);
	},

	declineCarrierTerms: function(deviceId){
		this.logger.debug("declineCarrierTerms");
		var that = this;
		var fnSuccess = function(response){
			that.logger.info("Terms have been acceppted successfully!");
		}

		var fnError =  function(error){
			that.logger.error("Error in accepting terms: " + error);
		}
		this.service.declineDeviceRegistrationCarrierTerms(deviceId,fnSuccess,fnError);
	}
});