/**
 * Description: Sample Service
 * @class SampleService
 * @extends BankingService
 * @namespace sap.banking.ui.services.SampleService
 */
(function($, sap, window, undefined) {
    "use strict";

    jQuery.sap.declare("sap.banking.ui.services.SampleService");
    jQuery.sap.require("sap.banking.ui.services.BankingService");
    jQuery.sap.require("sap.banking.ui.services.ServiceConstants");

    /*var params = {
        uri: CONSTANTS.URI,
        entities: CONSTANTS.ENTITIES,
        functionImports: CONSTANTS.FUNCTIONIMPORTS
    }*/

    sap.banking.ui.services.BankingService.extend("sap.banking.ui.services.SampleService", {

        /*constructor: function(oParams){          
            //var _fnSuperConstructor = sap.banking.ui.core.mvc.BankingController.prototype.constructor;
            //__fnSuperConstructor();
        },*/

        sampleMethod: function() {
            console.log("this is new method");
        },

        getEndPoint: function() {
            console.log("from overriden method");
        }


    });
})(jQuery, sap, window);