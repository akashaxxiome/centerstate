/**
* Banking Help Class
*/

jQuery.sap.declare("sap.banking.ui.common.Help");

sap.banking.ui.common.Help = (function($,window,undefined){

	var instance = {};

	var helpProperties = [];
	var currentActiveHash=[]; 
	/***********************************************************
	*					Private functions
	************************************************************/	
	function _init(config) {
		//console.info("initializing help properties");
		var that = this;
		$.ajax({
			url:"../../../web/js/ui5/data/help.properties.json",
			success: function(response){
				var data;
				if(response){
					data = response;
					if(data && data.helpPropeties){
						helpProperties = data.helpPropeties;					
					}
				}		
			},
			error: function(error){
				console.error("failed initializing help");
			}
		})
	}

	function _getHelpFileName(hash){
		var len = helpProperties.length;
		var fileName = "";
		for(var i=0; i< len; i++){
			var hashEntry = helpProperties[i].hash;
			if(hashEntry === hash){
				fileName = helpProperties[i].helpFile;
			}
		}
		return fileName;
	}

	/**
	* This function creates the error model and sets to passed view
	*/
	function _showHelp(sProjectPath){
		var hashEntries = currentActiveHash;
		var baseHashEntry = hashEntries[0];
		var helpPage = _getHelpFileName(baseHashEntry);
		if(sProjectPath == 'Consumers'){
			helpPage = '/cb/web/help/efs/'+ helpPage;
		}else if(sProjectPath == 'Business'){
			helpPage = '/cb/web/help/cb/'+helpPage;
		}
		sProjectPath = '/cb/web/help/';
		callHelp(sProjectPath,helpPage);//TODO: Modularize callhelp instead of using global space
	}

	

	
	/***********************************************************
	*					Public functions
	************************************************************/
	instance.init = function(config){
		_init();
	},
	
	instance.showHelp = function(sProjectPath){	     
		_showHelp(sProjectPath);
	},

	//TODO: remove dependency of ns.home
	instance.show = function(){
		var strProjectPath;
		if( ns.home.appUserType == 'Business'){
			strProjectPath = 'Business';
		} else if(ns.home.appUserType == 'Consumers'){
			strProjectPath = 'Consumers';
		}
		_showHelp(strProjectPath);
	},

	instance.setCurrentActiveHash = function(activeHash){
		currentActiveHash = activeHash;
	}

	return instance;

})(jQuery,window);