/**
 * Layout control
 */
var $Tabs, first = true;
var outerLayout, tabsContainerLayout;
/**
 * Flag indicating if a request is triggered by clicking on short-cut
 */
ns.common.shortcut = false;
/**
 * Stores the landing menu Id
 */
ns.common.landingMenuId = "";
/**
 * Last menu index
 */
ns.home.lastMenuId = "";
ns.home.lastTopLevelMenuId = "";

/*
 * Store current menu Id
 */
ns.home.currentMenuId = "";

ns.home.lastTimestamp = 0;

ns.home.initialNotesHeight = undefined;

ns.home.lastScrollTop = 0;

// Being set in index.jsp
ns.home.isDevice = false;
/**
 * Java script variable to decide which tab to be selected for User Preferences
 */
ns.userPreference.tabToBeSelected = -1;

ns.home.resizeOuterLayout = function() {
	if (outerLayout)
		outerLayout.resizeAll();
};

ns.home.toggleCustomTheme = function() {
	$('body').toggleClass('custom');
	ns.home.resizeOuterLayout();
};

ns.home.resizeSidebarLayout = function(pane, $Pane, state, options, layoutName) {
	if ($Pane.data("layoutContainer")) {
		$Pane.layout().resizeAll();
	}
	
	//jQuery('#accdNoti').accordion("widget").accordion("resize");
	//jQuery('#accdTool').accordion("widget").accordion("resize");

	/*if (ns.common.isInitialized($('#notificationPanes'), 'ui-panegroup')) {
		$('#notificationPanes').panegroup('resize');
		$('#toolsPanesID').panegroup('resize');
	}*/

	// Resize the width of grids when the size of window is changed or left
	// panel is colosed or opened.
	ns.common.resizeWidthOfGrids();
};

// tab1 includes everything below the logo area
ns.home.resizeTabPanelLayout = function() {
	var $TabPanel = $("#tab1").show() // make sure is 'visible'
	, tabLayout;
	// IF tabLayout exists - get Instance and call resizeAll
	if ($TabPanel.data("layoutContainer")) {
		// resize the layout-panes - if required
		tabLayout = $TabPanel.layout();
		tabLayout.resizeAll();
	}
	// else if tabLayout does not exist yet, create it now
	else { // if (ui.index > 0) // panel #0 layout is initialized in
		// document.ready
		tabLayout = $TabPanel.layout(ns.home.tabLayoutOptions);
		// also create nested-layouts in the 2 sidebars
		if (tabLayout != null) {
			//tabLayout.panes.west.layout(ns.home.sidebarLayoutOptions);
			if (tabLayout.panes.east.layout) {
				tabLayout.panes.east.layout(ns.home.sidebarLayoutOptions);
			}
		}
	}

	// call sub-routines to handle widgets INSIDE the TabPanels
	// handle_resizeEvent( tabLayout );
	// handle_InnerTabsInTab3( tabLayout );
	return;

};

// everything below the logo area
ns.home.tabLayoutOptions = {
	name : 'tabPanelLayout' // only for debugging
	,
	resizeWithWindow : false // required because layout is 'nested' inside
	// tabpanels container
	// , resizeWhileDragging: true // slow in IE because of the nested layouts
	,
	resizerDragOpacity : 0.5,
	north__resizable : false,
	south__resizable : false,
	north__closable : false,
	north__spacing_open : 2,
	south__closable : false,
	south__spacing_open : 3,
	/*west__spacing_open : 3,
	west__minSize : 250,*/
	east__minSize : 200,
	center__minWidth : 400,
	spacing_open : 3,
	spacing_closed : 8,
	contentSelector : ".ui-widget-content",
	togglerContent_open : '<div class="ui-icon"></div>',
	togglerContent_closed : '<div class="ui-icon"></div>',
	center__onresize : ns.home.resizeSidebarLayout
	/*west__resizable:		false,
	west__initClosed:		true*/
/*
 * don't need nested-layout callbacks as of Layout 1.3 RC-29 , west__onresize:
 * ns.home.resizeSidebarLayout , west__onclose: ns.home.resizeSidebarLayout ,
 * east__onresize: ns.home.resizeSidebarLayout
 */

};

ns.home.sidebarLayoutOptions = {
	name : 'sidebarLayout' // only for debugging
	,
	resizeWhileDragging : true
	// , north__size: "30%"
	,
	south__size : "60%",
	minSize : 100,
	center__minHeight : 100,
	spacing_open : 3,
	spacing_closed : 8,
	contentSelector : ".ui-widget-content",
	togglerContent_open : '<div class="ui-icon"></div>',
	togglerContent_closed : '<div class="ui-icon"></div>',
	center__onresize : ns.home.resizeSidebarLayout
};

$(document).ready(function() {
	
/*	
	$('#notes').scroll(function(){
		var scrollHeight = $('#notes').scrollTop();
	    
		if(scrollHeight > ns.home.lastScrollTop){
			//hide menu when scrolling down
			ns.home.hideMenuOnScrollDown();	
		}else{
			//show menu when scrolling up
			ns.home.showMenuOnScrollUp();
		}
		
		ns.home.lastScrollTop = scrollHeight;
	});
*/
	
	$("body").show();
	outerLayout = $("body").layout({
		name : 'outerLayout' // only for debugging
		,
		resizeWithWindowDelay : 250 // delay calling resizeAll when window is
		// *still* resizing
		,
		resizable : false,
		slidable : false,
		closable : false,
		north__paneSelector : "#outer-north",
		center__paneSelector : "#outer-center",
		south__paneSelector : "#outer-south",
		south__spacing_open : 0,
		north__spacing_open : 0
	});

	tabsContainerLayout = $("#outer-center").layout({
		name : 'tabsContainerLayout' // only for debugging
		,
		resizable : false,
		slidable : false,
		closable : false,
		north__paneSelector : "#middle-north",
		center__paneSelector : "#tabpanels",
		spacing_open : 0,
		center__onresize : ns.home.resizeTabPanelLayout
	// resize VISIBLE tabPanelLayout
	});

	ns.home.tabsLoading = true;

	// set object BEFORE initializing tabs because is used during init
	$Tabs = $("#main-menu"); // menu island
	/*$Tabs.tabs({
		show : function(evt, ui) {
			// need to resize layout after tabs init,
			// but before creating inner tabPanelLayout
			if (ns.home.tabsLoading) {
				ns.home.tabsLoading = false;
				tabsContainerLayout.resizeAll();
				// resizeAll will trigger center.onresize =
				// ns.home.resizeTabPanelLayout()
			} else
				// resize the INNER-layout each time it becomes 'visible'
				ns.home.resizeTabPanelLayout(ui);
		}
	});*/
	// .find(".ui-tabs-nav") //don't need to support menu reordering now. The
	// menu order can be customized in Preferences
	// .sortable({ axis: 'x', zIndex: 2 });

	ns.home.toggleCustomTheme();

	/*$Tabs.tabs({
		selected : 0
	});*/
	$("#tabbuttons").find('.tab1').mouseover(function(){
		$(".submenuHolderBgCls").removeClass("hideSubmenuHolder");
	});
	
	$(".submenuHolderBgCls").mouseover(function(){
		$(".overlayBg").css('display','block');
		$(".formActionItemPlaceholder").css('display','block');
	});
	$(".mainMenuBtnBarcls li a").mouseover(function(){
		$(".overlayBg").css('display','block');
		$(".formActionItemPlaceholder").css('display','block');
	});
	$(".profileInfoContainer").mouseover(function(){
		$(".overlayBg").css('display','block');
		$("#shortcutSearchInput").blur();
	});
	$(".mainSubmenuItemsCls li a").mouseover(function(){
		$(".formActionItemPlaceholder").css('display','none');
	});
	/*$(".mainSubmenuItemsCls li:nth-child(1)").mouseover(function(){
		console.log('in before function...');
		$(".overlayBg").css('display','block');
	});*/
	$(".submenuHolderBgCls").mouseout(function(){
		$(".overlayBg").css('display','none');
	});
	$(".mainMenuBtnBarcls li a").mouseout(function(){
		$(".overlayBg").css('display','none');
		$(".formActionItemPlaceholder").css('display','none');
	});
	$(".profileInfoContainer").mouseout(function(){
		$(".overlayBg").css('display','none');
	});
	$(".mainMenuBtnBarcls li a").click(function(){
		$(".overlayBg").css('display','none');
	});
	
	// adding hover class on top menu
	var $nav = $('.mainMenuBtnBarcls li');
	$nav.hover(
		function() {
			ns.common.hideStatus();
			$("#shortcutSearchInput").blur();
            $(this).children('a').addClass('hovered')
		},
		function() {
            $(this).children('a').removeClass('hovered')
		}
	);
	
	// adding hover class on submenu items
	var $nav = $('.mainSubmenuItemsCls li');
	$nav.hover(
		function() {
            $(this).children('a').addClass('hovered')
			//$('.formActionItemHolder>div', this).stop(true, true).slideDown('slow');
		},
		function() {
            $(this).children('a').removeClass('hovered')
			//$('.formActionItemHolder>div', this).slideUp('fast');
		}
	);

	$(".approvalDropdownMainCls, .alertDropdownMainCls , .msgDropdownMainCls ").mouseover(function(){
		ns.common.hideStatus();
	});
	
});
// Layout control ends

ns.home.showGlobalMessage = function() {
	$('#extension-menu').find('#globalMsgHintSpan').show();
	$('#globalmessage').show();
	ns.home.resizeTabPanelLayout();
}

ns.home.togglePortletSettings = function(menuId) {
	if(menuId !='home'){
		//$('#portletSettings').hide();
		//$("#menu_preferences_layout").css({visibility: "hidden"});
	}else{
		//$('#portletSettings').show();
		//$("#menu_preferences_layout").css({visibility: "visible"});
	}
	
};

/**
 * Quick Messages/Global Messages/Alerts Functions
 */
ns.home.loadGlobalMsg = function(menuId) {
	var urlString = "/cb/pages/common/GlobalMessageAction.action";	
	try {
		$
				.ajax({
					url : urlString,
					type : "GET",
					global: false,
					data : {
						menuId : menuId
					},
					success : function(data, textStatus, xmlHttpRequest) {
						if (data.startsWith("[[Exception:")) {
							$("#globalMsgTxtHolderID").html(js_no_global_messages);
							ns.common.hideGlobalMsg();
						} else {
							var rteElement = $(data).find('#RTE');
							if(rteElement && $(rteElement).html().trim() == ''){
								$("#globalMsgTxtHolderID").html(js_no_global_messages);
								ns.common.hideGlobalMsg();
							}else if(rteElement){
								 $("#globalMsgTxtHolderID").html(data);
								 
								 var showGlobalMessages = $("#globalMsgTxtHolderID").find("#showGlobalMessages");
								 //Only display Global Messages if they contain any new messages
								 if(showGlobalMessages != undefined && showGlobalMessages.val() == 'true'){
									 // Change the cloud color here...
									 // show short message...
									 ns.common.showCloudNotification();
									 //ns.common.showGlobalMsg();
								 }else{
									 ns.common.hideGlobalMsg();
								 }
							}
						}
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						ns.home.hideGlobalMessage();
						$("#globalMsgTxtHolderID").html(js_no_global_messages);
						ns.common.hideGlobalMsg();
					},
					complete : function(XMLHttpRequest, textStatus) {
						// alert('complete: ' + textStatus);
					}
				});
	} catch (e) {
		alert('Exception caught');
	}
};

//Updates the global message read time when user opens global message area to read global msg.
ns.home.updateGlobalMsgMenuSyncDate = function(){
	var globalMessageText = $("#globalMsgTxtHolderID").html().trim();
	//Return if there are no new message available to the user.
	if(js_no_global_messages == globalMessageText){
		return;
	}
	
	var urlString = "/cb/pages/common/GlobalMessageAction_updateMenuSyncDate.action";	
	try{
		$.ajax({
				url : urlString,
				type : "GET",
				data : {
							menuId : ns.home.currentMenuId,
							CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
						},
				success : function(data, textStatus, xmlHttpRequest) {

				}
				});
	}catch (e) {
		alert('Exception caught');
	}
};

/**
 * top level menu changed - allow cleanup of items specific to previous top
 * level menu
 */
ns.home.sessionCleanup = function(menuId) {
	var urlString = "/cb/pages/jsp/inc/" + menuId + "SessionCleanup.jsp";
	$.ajax({
		url : urlString,
		type : "POST",
		data : {
			CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage
		},
		complete : function(XMLHttpRequest, textStatus) {
			// alert('complete: ' + textStatus);
		}
	});
};

/**
 * When module and submodule are changed this function calls sessionCleanup of
 * last main menu to remove session objects
 */
ns.home.doSessionCleanUp = function() {
	var lastModuleStr = ns.home.lastMenuId;
	
	if(lastModuleStr == 'userPreferencesVirtualMenu'){//Preferences are opened in a dialog, so no need to do server side cleanup
		return;
	}
	
	var lastModule = lastModuleStr.split("_");

	var currentModuleStr = ns.home.currentMenuId;
	var currentModule = currentModuleStr.split("_");

	if (currentModule[0] !== lastModule[0]) {
		if (lastModule[0] !== "") {
			ns.home.sessionCleanup(lastModule[0]);
		}
	}
};

ns.home.hideGlobalMessage = function() {
	$('#extension-menu').find('#globalMsgHintSpan').hide();
	$('#globalmessage').hide();
	ns.home.resizeTabPanelLayout();
}

ns.home.toggleGlobalMessage = function() {
	$('#globalmessage').slideToggle('slow', ns.home.resizeTabPanelLayout);
};

ns.home.quickViewMessage = function(urlString, messageId, isNew,replyClicked) {
	$('#MessageSmallPanel tr').removeClass('currentmsg');
	$('#MessageSmallPanel tr#' + messageId).addClass('currentmsg');
	$.ajax({
		url : urlString,
		success : function(data) {
			if(!replyClicked){
			var config = ns.common.dialogs["quickViewMessageDialog"];
			var beforeDialogOpen = function() {
				$('#quickViewMsgDlgBand1').html(data);
				$('#quickViewMsgDlgBand1').show();
				$('#quickViewMsgDlgBand2').hide();
				$('#quickViewMsgDialogID').dialog('open');
				if (isNew) {
					$('#MessageSmallPanel tr.currentmsg td').removeClass('unreadMessage');
					if ('home_message' === ns.home.lastMenuId
							&& 'Inbox' === ns.message.mailbox) {
						$('#inboxMessagesGridId').trigger("reloadGrid");
					}
				}
			}
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("quickViewMessageDialog");
			$.publish("ns.common.getBankingEvents");
			$.publish("common.refreshMessages");
			}
		}
	});
};


ns.home.quickReplyMessage = function(urlString, messageId, isNew) {
	$('#MessageSmallPanel tr').removeClass('currentmsg');
	$('#MessageSmallPanel tr#' + messageId).addClass('currentmsg');
	
	// used to check state of read/unread
	if($('#unread').hasClass('topActionItemSelected')){
		// as unread & reply clicked we need to change it to read
		// view url so that message is marked viewed
		viewUrl = '/cb/pages/jsp/message/MessageViewAction_viewNotificationMessage.action?MessageID='+messageId+'&CSRF_TOKEN='+ns.home.getSessionTokenVarForGlobalMessage;
		// call view api but flag is set to distinguis actual view and this proxy view
		ns.home.quickViewMessage(viewUrl,messageId,isNew,true);
		ns.home.loadUnReadMessages(5);		
	}
	
	$.ajax({
		url : urlString,
		success : function(data) {
			var config = ns.common.dialogs["quickViewMessageDialog"];
			var beforeDialogOpen = function() {
				$('#quickViewMsgDlgBand1').html('');
				$('#quickViewMsgDlgBand1').hide();
				$('#quickViewMsgDlgBand2').html(data);
				$('#quickViewMsgDlgBand2').show();
				$('#quickViewMsgDialogID').dialog('open');
				if (isNew) {
					$('#MessageSmallPanel tr.currentmsg td').removeClass('unreadMessage');
					if ('home_message' === ns.home.lastMenuId
							&& 'Inbox' === ns.message.mailbox) {
						$('#inboxMessagesGridId').trigger("reloadGrid");
					}
				}
			}
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("quickViewMessageDialog");
			$.publish('successLoadingQuickReply');
			
		}
	});
};

ns.home.quickDeleteMessage = function(urlString, messageId, isNew) {
	$('#MessageSmallPanel tr').removeClass('currentmsg');
	$('#MessageSmallPanel tr#' + messageId).addClass('currentmsg');
	
	// view url so that message is marked viewed
	viewUrl = '/cb/pages/jsp/message/MessageViewAction_viewNotificationMessage.action?MessageID='+messageId;
	// call view api but flag is set to distinguis actual view and this proxy view
	ns.home.quickViewMessage(viewUrl,messageId,isNew,true);
	// used to maintain state of read/unread
	// if unread was active load that only
	if($('#unread').hasClass('topActionItemSelected')){
		ns.home.loadAllMessages(5);
		ns.home.loadUnReadMessages(5);
	}else{
		// load read
		ns.home.loadUnReadMessages(5);
		ns.home.loadAllMessages(5);
	}
	
	$.ajax({
		url : urlString,
		success : function(data) {
			var config = ns.common.dialogs["quickViewMessageDialog"];
			var beforeDialogOpen = function() {
				$('#quickViewMsgDlgBand1').html('');
				$('#quickViewMsgDlgBand1').hide();
				$('#quickViewMsgDlgBand2').html(data);
				$('#quickViewMsgDlgBand2').show();
				$('#quickViewMsgDialogID').dialog('open');
				if (isNew) {
					$('#MessageSmallPanel tr.currentmsg td').removeClass('unreadMessage');
					if ('home_message' === ns.home.lastMenuId
							&& 'Inbox' === ns.message.mailbox) {
						$('#inboxMessagesGridId').trigger("reloadGrid");
					}
				}
			}
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("quickViewMessageDialog");
			$.publish('successQuickDeletingMessageConfirm');
		}
	});
};

ns.home.quickDeleteAlert = function(urlString, messageId, isNew) {
	$('#AlertSmallPanel tr').removeClass('currentmsg');
	$('#AlertSmallPanel tr#' + messageId).addClass('currentmsg');
	
	// view url so that message is marked viewed
	viewUrl = '/cb/pages/jsp/alert/alertQuickViewAction.action?alertId='+messageId;
	// call view api but flag is set to distinguis actual view and this proxy view
	ns.home.quickViewAlert(viewUrl,messageId,isNew,true);
	// used to maintain state of read/unread
	// if unread was active load that only
	if($('#unReadAlert').hasClass('topActionItemSelected')){
    	ns.home.loadUnReadAlerts(5);
	}else{
		// load read
		ns.home.loadReadAlerts(5);
	}
	
	$.ajax({
		url : urlString,
		success : function(data) {
			var config = ns.common.dialogs["quickViewAlertDialog"];
			var beforeDialogOpen = function() {
                if ($('#quickViewAlertDialogID #quickViewAlertDlgBand1').length == 0) {
                    // These two div's are required since html is
                    // appended to these div's and not dialog container
                    // directly
                    $('#quickViewAlertDialogID').html("<div id='quickViewAlertDlgBand1'></div><div id='quickViewAlertDlgBand2'></div>");
                }
                $('#quickViewAlertDlgBand1').html('');      // set to empty to know we should close dialog on cancel
                $('#quickViewAlertDlgBand1').hide();
                $('#quickViewAlertDlgBand2').html(data);
                $('#quickViewAlertDlgBand2').show();
                $('#quickViewAlertDialog').dialog('open')
				$('#quickViewAlertDialogID').dialog({title:js_alert_delete_title});
                if (isNew) {
                    $('#AlertSmallPanel tr.currentmsg').removeClass('ui-state-important');
                    $('#AlertSmallPanel tr.currentmsg td').eq(0).removeClass("unreadMessage");
                    $('#AlertSmallPanel tr.currentmsg td a span').eq(0).removeClass("ui-icon-mail-closed").addClass("ui-icon-mail-open");
                    if ('home_alert' === ns.home.lastMenuId && 'inboxAlertsGridId' === ns.alert.whichgrid) {
                        $('#inboxAlertsGridId').trigger("reloadGrid");
                    }
                }
			}
			config.beforeOpen = beforeDialogOpen;
            ns.common.openDialog("quickViewAlertDialog");
            //Check if inbox alerts grid is open to auto reload it on view
            if($("#inboxAlertsGridId").is(":visible") === true){
                $("#inboxAlertsGridId").trigger("reloadGrid");
            }
            $.publish('successQuickDeletingAlertConfirm');
		}
	});
};

ns.home.quickViewAlert = function(urlString, messageId, isNew,deleteClicked) {
	ns.home.currentQuickView = messageId;
	$('#AlertSmallPanel tr').removeClass('currentmsg');
	$('#AlertSmallPanel tr#' + messageId).addClass('currentmsg');
	$.ajax({
			url : urlString,
			success : function(data) {
				if(!deleteClicked){
				var config = ns.common.dialogs["quickViewAlertDialog"];
				var beforeDialogOpen = function() {
					if ($('#quickViewAlertDialogID #quickViewAlertDlgBand1').length == 0) {
						// These two div's are required since html is
						// appended to these div's and not dialog container
						// directly
						$('#quickViewAlertDialogID').html("<div id='quickViewAlertDlgBand1'></div><div id='quickViewAlertDlgBand2'></div>");
					}
					$('#quickViewAlertDlgBand1').html(data);
					$('#quickViewAlertDlgBand1').show();
					$('#quickViewAlertDlgBand2').hide();
					$('#quickViewAlertDialogID').dialog({title:js_alert_view_title});
					if (isNew) {
						$('#AlertSmallPanel tr.currentmsg').removeClass('ui-state-important');
						$('#AlertSmallPanel tr.currentmsg td').eq(0).removeClass("unreadMessage");
						$('#AlertSmallPanel tr.currentmsg td a span').eq(0).removeClass("ui-icon-mail-closed").addClass("ui-icon-mail-open");
						if ('home_alert' === ns.home.lastMenuId && 'inboxAlertsGridId' === ns.alert.whichgrid) {
							$('#inboxAlertsGridId').trigger("reloadGrid");
						}
					}
				}
				config.beforeOpen = beforeDialogOpen;
				ns.common.openDialog("quickViewAlertDialog");
				//Check if inbox alerts grid is open to auto reload it on view
				if($("#inboxAlertsGridId").is(":visible") === true){
					$("#inboxAlertsGridId").trigger("reloadGrid");
				}
				}
			}
	});
};

ns.home.quickSendMessage = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			// console.info("ns.home.quickSendMessage");
			var config = ns.common.dialogs["quickNewMsgDialog"];
			var beforeDialogOpen = function() {
				$('#newMessageFormDiv').html('');
				$('#messageTabs').portlet('expand');
				$('#messageTabs').show();
				$('#viewMsgPortlet').portlet('clear', 'messageReply');
				if (ns.message.closeDetails)
					ns.message.closeDetails();

				$('#newMessageContentArea').html(data);
				$('#newMessageContentArea').show();
			};
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("quickNewMsgDialog");
		}
	});
};


ns.home.openPendingApprovalPayees = function() {
	var url ="/cb/pages/jsp/approvals/pendingApprovalIndex.jsp?CSRF_TOKEN"+ns.home.getSessionTokenVarForGlobalMessage+"&Initialize=true&subpage=pending";
	ns.common.loadSubmenuSummaryGrid(url,'approvals_pending','PendingApprovals','pendingPayees');
};

ns.home.openMessageDT = function(id) {
	if(ns.home.isDevice) {
		// just expand the holder
	} else {
		$('li[menuId="home_message"] a').click();
	}
};

ns.home.loadUnReadMessages = function(count) {
	var messageSearchCriteria ={
			"messageSearchCriteria.unReadOnly": "true",
			"messageSearchCriteria.readOnly": "false",
			"receivedItems":"true",
			"messageSearchCriteria.pageInfo.pageSize":count,
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
	};
	$.ajax({
		url: '/cb/pages/jsp/message/NotificationMessagesAction.action',	
		type:"POST",
		global:false,
		data: messageSearchCriteria,
		success: function(data){
			$('#messagesListHolder').html('');
			$('#messagesListHolder').html(data);
			$('#unread').addClass('topActionItemSelected');
			$('#read').removeClass('topActionItemSelected');
	   	}
	});
};

// this is load read messges not all messages
ns.home.loadAllMessages = function(count) {
	var allMessageJson ={
			"messageSearchCriteria.pageInfo.pageSize":count,
			"receivedItems":"true",
			"messageBody":"true",
			"messageSearchCriteria.unReadOnly": "false",
			"messageSearchCriteria.readOnly": "true",
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
	};
	$.ajax({
		url: '/cb/pages/jsp/message/NotificationMessagesAction.action',	
		type:"POST",
		global:false,
		data: allMessageJson,
		success: function(data){
			$('#messagesListHolder').html('');
			$('#messagesListHolder').html(data);
			$('#read').addClass('topActionItemSelected')
			$('#unread').removeClass('topActionItemSelected')
	   	}
	});
};

ns.home.loadUnReadAlerts = function(count) {
	var messageSearchCriteria ={
			"messageSearchCriteria.unReadOnly": "true",
			"messageSearchCriteria.readOnly": "false",
			"messageSearchCriteria.pageInfo.pageSize":count,
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
	};
	$.ajax({
		url: '/cb/pages/jsp/alert/alertsPanelViewAction.action',	
		type:"POST",
		global:false,
		data: messageSearchCriteria,
		success: function(data){
			$('#alertSmallPanelPortletTopBar').html('');
			$('#alertSmallPanelPortletTopBar').html(data);
			$('#unReadAlert').addClass('topActionItemSelected');
			$('#readAlert').removeClass('topActionItemSelected');
	   	}
	});
};

ns.home.loadReadAlerts = function(count) {
	var allMessageJson ={
			"messageSearchCriteria.pageInfo.pageSize":count,
			"messageBody":"true",
			"messageSearchCriteria.unReadOnly": "false",
			"messageSearchCriteria.readOnly": "true",
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
	};
	$.ajax({
		url: '/cb/pages/jsp/alert/alertsPanelViewAction.action',	
		type:"POST",
		global:false,
		data: allMessageJson,
		success: function(data){
			$('#alertSmallPanelPortletTopBar').html('');
			$('#alertSmallPanelPortletTopBar').html(data);
			$('#readAlert').addClass('topActionItemSelected');
			$('#unReadAlert').removeClass('topActionItemSelected');
	   	}
	});
};


ns.home.openAlertDT = function() {
	$('li[menuId="home_alert"] a').click();
};

$("document").ready(function(){

					/**
					 * Quick Sending New Message
					 */
					$.subscribe('beforeSendingQuickMessage', function(event,
							data) {
						$.publish('home_removeErrorsOnBegin');
						// alert('beforeSendingQuickMessage');
					});

					$.subscribe('sendingQuickMessageComplete', function(event,
							data) {
						// alert('sendingQuickMessageComplete');

					});

					$.subscribe('errorSendingQuickMessage', function(event,
							data) {
						// alert('errorSendingQuickMessage');

					});

                    $.subscribe('home_removeErrorsOnBegin', function(event, data) {
                            $('.errorLabel').html('').removeClass('errorLabel');
                            $('#formerrors').html('');
                    });

					$.subscribe('successSendingQuickMessage', function(event,
							data) {
						//alert("successSendingQuickMessage"+event.originalEvent.data.response.data);
						// alert('successSendingQuickMessage');
						$('#quickMessagingResponse').html(event.originalEvent.data.response.data);
						//$.publish('refreshMessageSmallPanel');
						//$.publish("ns.common.resetRefreshPanels");
						$.publish("ns.common.getBankingEvents");
						
						$.publish("closeDialog");

						if ('home_message' === ns.home.lastMenuId
								&& 'Inbox' === ns.message.mailbox) {
							$('#inboxMessagesGridId').trigger("reloadGrid");
						}
						
						ns.home.loadUnReadMessages(5);
					});

					/**
					 * Quick Loading Message Reply Form
					 */
					$.subscribe('beforeLoadingQuickReply',
							function(event, data) {
								// alert('beforeLoadingQuickReply');
							});

					$.subscribe('loadingQuickReplyComplete', function(event,
							data) {
						// alert('loadingReplyFormComplete');

					});

					$.subscribe('errorLoadingQuickReply',
							function(event, data) {
								// alert('errorLoadingQuickReply');

							});

					$.subscribe('successLoadingQuickReply', function(event,
							data) {
						// alert('successLoadingQuickReply');

						$('#newMessageFormDiv').html('');
						$('#messageTabs').portlet('expand');
						$('#messageTabs').show();
					    $('#details').slideUp();
						$('#viewMsgPortlet').portlet('clear', 'messageReply');
						if (ns.message.closeDetails)
							ns.message.closeDetails();

						$('#quickViewMsgDlgBand1').slideUp();
						$('#quickViewMsgDlgBand2').slideDown();

					});

					/**
					 * Quick Deleting Message Confirmation
					 */
					$.subscribe('beforeQuickDeletingMessageConfirm', function(
							event, data) {
						// alert('beforeQuickDeletingMessageConfirm');
					});

					$.subscribe('quickDeletingMessageConfirmComplete',
							function(event, data) {
								// alert('quickDeletingMessageConfirmComplete');

							});

					$.subscribe('errorQuickDeletingMessageConfirm', function(
							event, data) {
						// alert('errorQuickDeletingMessageConfirm');

					});

					$.subscribe('successQuickDeletingMessageConfirm', function(
							event, data) {
						// alert('successQuickDeletingMessageConfirm');
						$('#quickViewMsgDlgBand1').slideUp();
						$('#quickViewMsgDlgBand2').slideDown();

					});

					/**
					 * Quick Delete Message
					 */
					$.subscribe('beforeQuickDeletingMessage', function(event,
							data) {
						// alert('beforeQuickDeletingMessage');
					});

					$.subscribe('QuickDeletingMessageComplete', function(event,
							data) {
						// alert('QuickDeletingMessageComplete');
					});

					$.subscribe('errorQuickDeletingMessage', function(event,
							data) {
						// alert('errorQuickDeletingMessage');

					});

					$.subscribe('successQuickDeletingMessage', function(event,
							data) {
						// alert('successQuickDeletingMessage');
						$('#quickMessagingResponse').html(event.originalEvent.data.response.data);
						//$.publish('refreshMessageSmallPanel');
						//$.publish("ns.common.resetRefreshPanels");
						$.publish("ns.common.getBankingEvents");
						
						/*$('body').jAlert($('#quickMessagingResponse').html(),
								"success", null, 300, '25');*/
						
						$.publish("common.refreshMessages");
						
						$.publish('closeDialog');

						if ('home_message' === ns.home.lastMenuId
								&& 'Inbox' === ns.message.mailbox) {
							$('#inboxMessagesGridId').trigger("reloadGrid");
						}
						
						if($('#unread').hasClass('topActionItemSelected')){
							ns.home.loadAllMessages(5);
							ns.home.loadUnReadMessages(5);
						}else{
							// load read
							ns.home.loadUnReadMessages(5);
							ns.home.loadAllMessages(5);
						}

					});

					/**
					 * Quick Send Reply
					 */
					$.subscribe('beforeSendingQuickReply',
							function(event, data) {
								// alert('beforeSendingQuickReply');
							});

					$.subscribe('sendingQuickReplyComplete', function(event,
							data) {
						// alert('sendingQuickReplyComplete');

					});

					$.subscribe('errorSendingQuickReply',
							function(event, data) {
								// alert('errorSendingQuickReply');

							});

					$.subscribe('successSendingQuickReply', function(event,
							data) {
						$('#quickMessagingResponse').html(event.originalEvent.data.response.data);
						$.publish("ns.common.getBankingEvents");
						$.publish("closeDialog");

						if ('home_message' === ns.home.lastMenuId
								&& 'Inbox' === ns.message.mailbox) {
							$('#inboxMessagesGridId').trigger("reloadGrid");
						}
						
						$('#sentMessagesGridId').trigger("reloadGrid");
						
						ns.home.loadUnReadMessages(5);
					});

					/**
					 * Refresh quick messaging panel
					 */
					$.subscribe('beforeLoadingMsgSmallPanel', function(event,
							data) {
						// alert('beforeLoadingMsgSmallPanel');
					});

					$.subscribe('successLoadingMsgSmallPanel', function(event,
							data) {
						// alert('successLoadingMsgSmallPanel');

					});

					$.subscribe('errorLoadingMsgSmallPanel', function(event,
							data) {
						// alert('errorLoadingMsgSmallPanel');

					});

					$.subscribe('completeLoadingMsgSmallPanel', function(event,
							data) {
						// alert('completeLoadingMsgSmallPanel');

					});

					$.subscribe('cancelQuickReplyMsg', function(event, data) {
						if($('#quickViewMsgDlgBand1').html()!=''){//Coming from QuickView, go back to QuickView
							$('#quickViewMsgDlgBand1').slideDown();
							$('#quickViewMsgDlgBand2').slideUp();
						}else{//Coming from QuickReply, close Dialog
							ns.common.closeDialog();
						}
					});

					$.subscribe('cancelQuickDeleteMsg', function(event, data) {
						if($('#quickViewMsgDlgBand1').html()!=''){//Coming from QuickView, go back to QuickView
							$('#quickViewMsgDlgBand1').slideDown();
							$('#quickViewMsgDlgBand2').slideUp();
						}else{//Coming from QuickDelete, close Dialog
							ns.common.closeDialog();
						}
					});

					/**
					 * Refresh quick Approvals panel
					 */
					$.subscribe('beforeLoadingApprovalSmallPanel', function(
							event, data) {
					});

					$.subscribe('successLoadingApprovalSmallPanel', function(
							event, data) {

					});

					$.subscribe('errorLoadingApprovalSmallPanel', function(
							event, data) {

					});

					$.subscribe('completeLoadingApprovalSmallPanel', function(
							event, data) {

					});

					/**
					 * Refresh quick Alerts panel
					 */
					$.subscribe('beforeLoadingAlertSmallPanel', function(event,
							data) {
						// alert('beforeLoadingAlertSmallPanel');
					});

					$.subscribe('successLoadingAlertSmallPanel', function(
							event, data) {
						// alert('successLoadingAlertSmallPanel');

					});

					$.subscribe('errorLoadingAlertSmallPanel', function(event,
							data) {
						// alert('errorLoadingAlertSmallPanel');

					});

					$.subscribe('completeLoadingAlertSmallPanel', function(
							event, data) {
						// alert('completeLoadingAlertSmallPanel');

					});

					/**
					 * Quick Deleting Alert Confirmation
					 */
					$.subscribe('beforeQuickDeletingAlertConfirm', function(
							event, data) {
						// alert('beforeQuickDeletingAlertConfirm');
					});

					$.subscribe('quickdeletingAlertConfirmComplete', function(
							event, data) {
						// alert('quickdeletingAlertConfirmComplete');

					});

					$.subscribe('errorQuickDeletingAlertConfirm', function(
							event, data) {
						// alert('errorQuickDeletingAlertConfirm');

					});

					$.subscribe('successQuickDeletingAlertConfirm', function(
							event, data) {
						// alert('successQuickDeletingAlertConfirm');
						$('#quickViewAlertDlgBand1').slideUp();
						$('#quickViewAlertDlgBand2').slideDown();
						
					});

					/**
					 * Quick Delete Alert
					 */
					$.subscribe('beforeQuickDeletingAlert', function(event,
							data) {
						// alert('beforeQuickDeletingAlert');
					});

					$.subscribe('QuickDeletingAlertComplete', function(event,
							data) {
						// alert('QuickDeletingAlertComplete');

					});

					$.subscribe('errorQuickDeletingAlert',
							function(event, data) {
								// alert('errorQuickDeletingAlert');

							});

					$.subscribe('successQuickDeletingAlert',
						function(event, data) {
							// alert('successQuickDeletingAlert');
							$('#quickMessagingResponse').html(event.originalEvent.data.response.data);
							$.publish('refreshAlertSmallPanel');
							$/*('body').jAlert(
									$('#quickMessagingResponse')
											.html(), "success",
									null, 300, '25');*/
							
							$.publish("common.refreshAlerts");
							
							$.publish('closeDialog');

							if ('home_alert' === ns.home.lastMenuId
									&& 'inboxAlertsGridId' === ns.alert.whichgrid) {
								$('#inboxAlertsGridId').trigger(
										"reloadGrid");
								if (ns.alert.currentView
										&& ns.home.currentQuickView === ns.alert.currentView) {
									$.publish('cancelViewAlert');
								}
							}

							if ($('#closeViewAlertContainerButton').length > 0)
							{
								$("#closeViewAlertContainerButton").click();
							}
							
							if($('#unReadAlert').hasClass('topActionItemSelected')){
								ns.home.loadUnReadAlerts(5);
							}else{
								// load read
								ns.home.loadReadAlerts(5);
							}
						});

					$.subscribe('cancelQuickDeleteAlert',
							function(event, data) {
                                if($('#quickViewAlertDlgBand1').html()!=''){//Coming from QuickView, go back to QuickView
                                    $('#quickViewAlertDlgBand1').slideDown();
                                    $('#quickViewAlertDlgBand2').slideUp();
                                }else{//Coming from QuickDelete, close Dialog
                                    ns.common.closeDialog();
                                }

							});

					$.subscribe('messages.clearQuickMessageBoxArea', function(
							event, data) {
						$("#sendquickmessage_subject").val("");
						$("#sendQuickMessageMemo").val("");
					});
					
					$.subscribe('positivePayPortletGridComplete', function(event,data) {
						
						//retrive dynamic grid of positive pay portlet
						var positivePayPortletGridId = $("#positivePayPortletDataDiv .ui-jqgrid").prop('id').substring(5);
						var recordsCount = parseInt($("#"+positivePayPortletGridId).jqGrid('getGridParam','reccount'),10);
						//Show no pending exceptions message if grid is empty.
						if(isNaN(recordsCount) || recordsCount == 0) {
							 $("#positivePayPortletDataDiv").hide();
							 $("#positivePayPortletNoDataDiv").show();
						}else{
							 $("#positivePayPortletDataDiv").show();
							 $("#positivePayPortletNoDataDiv").hide();
						}
						$("#positivePayPortletDataDiv").parent().resize();
						
						$(".ppayDetailsOnHoverCls a").hover(
								function(event) {				
									//rowElement = event.fromElement.parentElement;
									rowElement = event.relatedTarget.parentElement;
									var urlString = $(rowElement.getElementsByClassName('PPAYImageUrlId')).attr('title');
									$.ajax({
										url: urlString,
										success: function(data){
											$('#imgHolder').html(data);
											}
									});
									$(".ppayHoverDetailsMaster").show();
									if(($(this).offset().left) <= ($(window).width()/2)) {
										$(".ppayHoverDetailsHolder").offset({left: ($(this).offset().left) });
									} else {
										$(".ppayHoverDetailsHolder").offset({left: ($(window).width() / 2) });
									}
								}, function() {
									// space for code to run on mouseout...
								}
							);
					});
					
	
					$.subscribe('reversePositivePayPortletGridComplete', function(event,data) {
						//retrive dynamic grid of positive pay portlet
						var reversePositivePayPortletGridId = $("#reversePositivePayPortletDataDiv .ui-jqgrid").prop('id').substring(5);
						var recordsCount = parseInt($("#"+reversePositivePayPortletGridId).jqGrid('getGridParam','reccount'),10);
						//Show no pending exceptions message if grid is empty.
						if(isNaN(recordsCount) || recordsCount == 0) {
							 $("#reversePositivePayPortletDataDiv").hide();
							 $("#reversePositivePayPortletNoDataDiv").show();
						}else{
							 $("#reversePositivePayPortletDataDiv").show();
							 $("#reversePositivePayPortletNoDataDiv").hide();
						}
						$("#reversePositivePayPortletDataDiv").parent().resize();
						
												
						$(".ppayDetailsOnHoverCls a").hover(
								function(event) {				
									//rowElement = event.fromElement.parentElement;
									rowElement = event.relatedTarget.parentElement;
									var urlString = $(rowElement.getElementsByClassName('PPAYImageUrlId')).attr('title');
									$.ajax({
										url: urlString,
										success: function(data){
											$('#imgHolder').html(data);
											}
									});
									$(".ppayHoverDetailsMaster").show();
									if(($(this).offset().left) <= ($(window).width()/2)) {
										$(".ppayHoverDetailsHolder").offset({left: ($(this).offset().left) });
									} else {
										$(".ppayHoverDetailsHolder").offset({left: ($(window).width() / 2) });
									}
								}, function() {
									// space for code to run on mouseout...
								}
							);
					});
					
					});


				

/**
 * Menu & Navigation control
 */
/**
 * Update the breadcrumb to indicate where user is located
 */
ns.home.updateBreadcrumb = function(menuName, subMenuName, menuId) {

	$("#breadcrumb span.portlet-title").eq(0).html(menuName);

	// only display sub menu when it's provided
	if (subMenuName) {
		$("#nav-submenu").show();
		/*if (subMenuName.trim() !== 'ACH') {
			subMenuName = subMenuName;
		}*/
		$("#breadcrumb span.portlet-title").eq(1).html(subMenuName);
		$("#portlet-menu").hide();
		$('#helpSubMenuName').html(menuId);
	} else {// home page
		$("#nav-submenu").hide();
		$("#portlet-menu").show();
		$('#helpSubMenuName').html('Home');
	}
};

ns.home.displayToggleDashoardLink = function() {
	$('#notes').show();
	$('#notes').scrollTop(0);
	ns.home.resizeTabPanelLayout(); // need to call resize because it take
	// effect only "notes" is shown

	if ($('#appdashboard').length > 0 && $('#appdashboard').height() > 0
			&& $('#appdashboard').is(':visible')) {
		$('#dashboardToggleLinkSpan').show();
	} else {
		$('#dashboardToggleLinkSpan').hide();
	}
	ns.common.applyCommonTabIndex();
	$.publish("common.topics.tabifyDesktop");
	if (accessibility) {
		accessibility.setFocusOnDashboard();
	}
};

$.subscribe('subMenuOnCompleteTopics', ns.home.displayToggleDashoardLink);

/*
 * remove dialogs of modules by menu id
 */
ns.home.removeDialogs = function(menuId) {
	// Check for '_' in the menuId and lastMenuId.
	// Normally '_' separates parent menu Id and sub menu Id.
	var index = menuId.indexOf('_');
	var lastIndex = ns.home.lastMenuId.indexOf('_');
	var currentParentMenuId = menuId;
	var lastParentMenuId = ns.home.lastMenuId;
	if (index != -1) {
		currentParentMenuId = menuId.substr(0, index);
	}
	if (lastIndex != -1) {
		lastParentMenuId = ns.home.lastMenuId.substr(0, lastIndex);
	}

	// Check if parent menu clicked or Sub menu clicked
	if (menuId != currentParentMenuId) {
		// remove dialogs for which Css class contains Parent menu id only.
		// e.g -> menuId is "home_prefs", then clear all dialogs with css class
		// "homeDialog"
		ns.home.destroyAndRemoveDialog($(".ui-dialog [class*='"
				+ currentParentMenuId + "']"));
	}
	if (ns.home.lastMenuId != lastParentMenuId && ns.home.lastMenuId != "") {
		// remove dialogs for which Css class contains Parent menu id only.
		// e.g -> lastMenuId is "home_prefs", then clear all dialogs with css
		// class "homeDialog"
		ns.home.destroyAndRemoveDialog($(".ui-dialog [class*='"
				+ lastParentMenuId + "']"));
	}

	// remove dialogs of last module
	if (ns.home.lastMenuId != "") {
		ns.home.destroyAndRemoveDialog($(".ui-dialog [class*='"
				+ ns.home.lastMenuId + "']"));
	}

	// remove dialogs of entering module
	ns.home.destroyAndRemoveDialog($(".ui-dialog [class*='" + menuId + "']"));

	if (ns.home.lastMenuId && ns.home.lastMenuId !== '') {
		$('#menu_' + ns.home.lastMenuId).unLoadTopics().removeData(
				'createdTopics').unbind('.topics');
	}
	ns.home.lastMenuId = menuId;
};

/**
 * Sub-menu hovering effect
 */
ns.home.subMenu = {};
ns.home.subMenu.hoverAnimateTime = 500;
ns.home.subMenu.showHoverBg = function($hoverMenu, $hoverBg) {

	if ($hoverBg.css('opacity') == '0') {
		$hoverBg.attr('curMenuId', $hoverMenu.attr('menuId'));

		$hoverBg.width($hoverMenu.width());
		$hoverBg.css('left', $hoverMenu.position().left);
		$hoverBg.animate({
			'opacity' : 1
		}, ns.home.subMenu.hoverAnimateTime);
	} else {
		if ($hoverBg.attr('curMenuId') != $hoverMenu.attr('menuId')
				|| $hoverBg.css('opacity') != '1') {
			$hoverBg.attr('curMenuId', $hoverMenu.attr('menuId'));

			$hoverBg.stop();
			$hoverBg.animate({
				'left' : $hoverMenu.position().left,
				width : $hoverMenu.width(),
				opacity : 1
			}, ns.home.subMenu.hoverAnimateTime);
		}
	}

}

/**
 * If the sub-menu is highlighted while hovering, remove the hightlighting
 * effect by setting opacity to 0
 */
ns.home.subMenu.hideHoverBg = function(subMenuId) {
	var $hoverBg = $('#' + subMenuId).find('.subMenuHover');
	if ($hoverBg.css('opacity') != '0') {
		$hoverBg.stop();
		$hoverBg.animate({
			opacity : 0
		}, ns.home.subMenu.hoverAnimateTime);
	}

};

ns.home.subListHiddenCount = 0;
ns.home.displayBackActiveMenu = function() {
	var hoverActive = $(".hoverActive").attr("id");
	if (hoverActive != null || hoverActive != undefined) {
		$("." + hoverActive).removeClass("ui-state-hover");
	}
	$(".hoverActive").removeClass("hoverActive").parent().children().addClass(
			"hidden");
	$(".active").removeClass("hidden");

}

// toggle dash board
ns.home.toggleDashBoard = function() {
	if ($('#appdashboard').data("ui-pane") != undefined) {
		if ($('#appdashboard').pane('isShown'))
			$('#appdashboard').pane('hide');
		else
			$('#appdashboard').pane('show');

	}
};


//Show menu selected
ns.home.highlightMainMenu = function(selectedMenuId) {
	$('.selectedMainMenuItemCls').removeClass("selectedMainMenuItemCls");
//	$('li[menuId="' + previousMenuId + '"]').removeClass("selectedMainMenuItemCls");
	$('li[menuId="' + selectedMenuId + '"]').addClass("selectedMainMenuItemCls");
};

ns.home.highlightMainMenuOfSubmenu = function(selectedSubmenuItem) {
	$('.selectedMainMenuItemCls').removeClass("selectedMainMenuItemCls");
	$("#"+$(selectedSubmenuItem).attr('id')).closest(".tab1").addClass("selectedMainMenuItemCls");
};

ns.home.setMenuBackgroundStyle = function(selectedMenuId,isSubmenu){
	if(isSubmenu){
		var selectedMainMenuId = $("li[menuId='"+selectedMenuId+"']").closest(".tab1").attr("menuid");
		var currentPageStyleClass = selectedMainMenuId+'PageStyle';
		var lastMenuId = ns.home.lastMenuId.split("_");
		var previousPageStyleClass = lastMenuId[0] +'PageStyle';
		$('body').removeClass(previousPageStyleClass).addClass(currentPageStyleClass);
	}else{
		var currentPageStyleClass = selectedMenuId+'PageStyle';
		var lastMenuId = ns.home.lastMenuId.split("_");
		var previousPageStyleClass = lastMenuId[0] +'PageStyle';
		$('body').removeClass(previousPageStyleClass).addClass(currentPageStyleClass);
	}
};

$("document").ready(function() {
		$('#helpSubMenuName').html('Home');
		ns.home.checkEntChangeEnabled = "";
		ns.home.clicked = false;
		ns.home.menuRefreshed = "";
		
		//Function for Menu refreshed starts
		ns.home.menuRefresh = function(){
			//console.log('ns.home.menuRefresh');
			if(ns.home.menuRefreshed == "true") {
				//$('body').jAlert($('#reinitializesessionmessage').html(),"success", null, 300, '25');
				var body = $('body'), messageDialogDiv = $("#reinitializesessionmessage");
				if (messageDialogDiv && !messageDialogDiv.hasClass('ui-dialog-content')) {
					messageDialogDiv.dialog({
						autoOpen : false,
						position: { my: "center top", at: "top", of:body},
						height : 70,
						width : 500,
						resizable : false,
						hide : { effect: "slideUp", duration: 1000 },
						show : { effect: "slideDown", duration: 800 },
						open: function( event, ui ) {
							setTimeout(function(){ messageDialogDiv.dialog('close'); }, 4000);
						}
					});
				}
				messageDialogDiv.attr("display", "block");
				messageDialogDiv.dialog('open');
				messageDialogDiv.addClass("staticContentDialog alert");
				ns.home.menuRefreshed = "false";
			}
					
			//console.log('ns.home.menuRefreshed=>'+ns.home.menuRefreshed);
			if(ns.home.clicked == false){
				$.ajax({
						type : "POST",
						url : "/cb/pages/util/CheckChangeInEntEnabledAction.action",
						success : function(data) {
									ns.home.checkEntChangeEnabled = data.checkEntChangeEnabled;
									if (ns.home.checkEntChangeEnabled == "true") {
										$.ajax({
												type : "POST",
												url : "/cb/pages/util/CheckChangeInEntAction_initialize.action",
												success : function(data) {
												}
										});
									}
							}
					});
							
				ns.home.clicked = true;
			}else{
				if(ns.home.checkEntChangeEnabled == "true"){
						$.ajax({
								type : "POST",
								url : "/cb/pages/util/CheckChangeInEntAction.action",
								success : function(data) {
								var isEntChanged = data.entChange;
								var reloadURL = data.reloadURL;
									if (isEntChanged == "true") {
										document.location.href = reloadURL;
									}
								}
						});
				}
			}
		};
		//Function for Menu refreshed ends
		
		//First Level Menu click function starts
		ns.home.topMenuClicked = function(event){
			if(ns.home.isDevice && ns.home.currentMenuId != '') {
				$(".submenuHolderBgCls").removeClass("hideSubmenuHolder");
				event.stopImmediatePropagation();
				event.stopPropagation();
				return;	
			}
			
			$(".submenuHolderBgCls").addClass("hideSubmenuHolder");

			event.stopImmediatePropagation();
			event.stopPropagation();
			
			//console.log("topMenuClicked");
			
			ns.home.hideGlobalMessage();
			
			var contextRelativePathToLoad = $(this).parent().attr('link');

			//Skip the flow for Preferences top menu click,since it does not have default submenu to load
			if(contextRelativePathToLoad == undefined || contextRelativePathToLoad == ''){
				return false;
			}
			//console.log("tabbuttons :: contextRelativePathToLoad=>"+contextRelativePathToLoad);
			
			var menuId = $(this).parent().attr('menuId');
			//console.log("tabbuttons :: menuId=>"+menuId);

			var subList = $(this).parent().attr("subList");
			//console.log("tabbuttons :: subList attr=>"+subList);
			
			var activeMenuId = undefined;

			//Required for showing module specific loading gif(spinning wheel and blocking div)
			ns.common.hidePrevMenuLoading(ns.home.currentMenuId);
			
			//Highlight the selected menu
			ns.home.highlightMainMenu(menuId);
			
			ns.home.setMenuBackgroundStyle(menuId,false);

			ns.home.currentMenuId = menuId;

			$.publish("onTopMenuClick",[ns.home.currentMenuId,menuId]);					
			
			//Temporily commenting the Overlay functionality of home page
			//Do not render inline home page when user is clicking Home menu from any other module, 
			//it is supposed to be rendered as overlay homepage, this is handled by toolbarcontroller.js
			/*if(menuId == 'home'){
				if(ns.home.currentMenuId != undefined && ns.home.currentMenuId!= '' && ns.home.currentMenuId.indexOf("home") == -1){
					return;
				}
			}*/
			$("#layoutContent").html('');//Since new content will be loaded clearing the layout content here before load
			

			//ns.home.togglePortletSettings(menuId);

			//menuRefresh MUST BE AFTER currentMenuId is set
			ns.home.menuRefresh();

			//When user clicks back button, when any dialog is open, this call is necessary to close it.
			ns.common.closeDialog();

			//remove dialogs
			ns.home.removeDialogs(menuId);

			//Save last top level menu id
			if(ns.home.lastTopLevelMenuId !== menuId){
				if(ns.home.lastTopLevelMenuId != ""){
					ns.home.sessionCleanup(ns.home.lastTopLevelMenuId);
				}
				ns.home.lastTopLevelMenuId = menuId;
			}

			var menuName = $(this).text();
			
			var subMenuName = $("#" + subList + " a").eq(0).text();
			//console.log("tabbuttons :: subMenuName=>"+subMenuName);

			if(contextRelativePathToLoad && contextRelativePathToLoad.indexOf("home-page") != -1){
				subMenuName = undefined;
			}

			if(contextRelativePathToLoad.indexOf("home-page.jsp") != -1){
				var curTimestamp = new Date();
				if(ns.home.lastTimestamp){
					var temp = curTimestamp.getTime() - ns.home.lastTimestamp;
					if (temp < 2000) { // milliseconds
						alert('Warning: clicking on same menu too fast, the second and later clicking will be ignored.');
						return;
					}
				}
				ns.home.lastTimestamp = curTimestamp.getTime();
			}

			// It's causing some display issues when clicking on shortcuts
			if(!ns.common.shortcut){
				$('#notes').hide(); // Avoid showing
			} 
			
			//TODO move this request to below ajax success, however for the first time the ajax success is not getting triggered, hence moved it outside for now.
			// if the active child menu's menuId parameter is not undefined then pass this menuId parameter to global messages else pass the available menuId value
			if(activeMenuId != undefined){
				ns.home.loadGlobalMsg(activeMenuId);
			}else{
				ns.home.loadGlobalMsg(menuId);
			}
										
			//page content in plain text before jQuery stuff is executed
			$.ajax({
				url : contextRelativePathToLoad,
				type : "POST",
				data : { CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage},
				success : function(data){
					$('#notes').html(data);
					$('#notes').show();

					//need to call resize because it take effect only "notes" is shown
					ns.home.resizeTabPanelLayout();
					
					//TODO set activeMenuId
					//if the subMenuName is valuable, then make the first 2nd-level menu ui-state-active
					
					/*if(subMenuName != undefined){
						$("#"+ subList +" li:not(.second-level-menu-separator)").eq(1).addClass("ui-state-active").addClass("ui-corner-all");
						
						//gets the menuId of active child menu
						activeMenuId = $("#"+ subList+" li.ui-state-active.ui-corner-all").attr("menuId");
					}*/
													
					// if the active child menu's menuId parameter is not undefined then pass this menuId parameter to global messages else pass the available menuId value
					/*
					if(activeMenuId != undefined){
						ns.home.loadGlobalMsg(activeMenuId);
					}else{
						ns.home.loadGlobalMsg(menuId);
					}
					*/
					
					ns.common.showLIndicator = true;
													
					$.publish("common.topics.tabifyDesktop");
					
					ns.common.applyCommonTabIndex();

					if(accessibility){
						accessibility.setFocusOnDashboard();
					}
				}
			});

			//Add browser history entry. This is added for back and forward button support.
			if(window.parent && window.parent.addHistoryEntry){
				window.parent.addHistoryEntry(menuId);
				if(subMenuName){
					window.parent.changeWindowTitle(subMenuName);
				}else{
					window.parent.changeWindowTitle(menuName);
				}
			}
			if(subMenuName){
				ns.home.setChangeTitle(subMenuName);
			}else{
				ns.home.setChangeTitle(menuName);
			}
		};
		
		ns.home.topMenuIconClicked = function(event){
			//console.log("topMenuIconClicked");
			$(this).find('.mainMenuCls').click();
		};
		
		$("#tabbuttons").find('.tab1').find('.mainMenuCls').click(ns.home.topMenuClicked);
		
		$("#tabbuttons").find('.tab1').click(ns.home.topMenuIconClicked);
		//First Level Menu click function ends
		
		$(".mainSubmenuItemsCls li a").each(function(){
			$(this).click(function(){
				//Required for showing module specific loading gif(spinning wheel and blocking div)
				ns.home.hideGlobalMessage();
				var menuId = ns.home.currentMenuId;
				ns.home.currentMenuId = $(this).parent().attr("menuId");
				
				ns.common.hidePrevMenuLoading(menuId);
				
				$.publish("onTopMenuClick",[menuId,ns.home.currentMenuId]);		
			});
		});
		
		
		//Second Level Menu Items start
		ns.home.subMenuClicked = function(event){
				$(".submenuHolderBgCls").addClass("hideSubmenuHolder");
			
				//TODO : change the selector for submenuClicked to be applied only on anchor click
				//Skip the callback if user has clicked on submenu help icon and event is propogated
				if(event.originalEvent!= undefined && $(event.originalEvent.target).hasClass('submenuHelpIcon')){
					//console.log('help clicked');
					return false;
				}else if(event.originalEvent!= undefined && $(event.originalEvent.target).is("#tabbuttons .mainSubmenuItemsCls li > a") == false){
					//console.log('anchor was not clicked');
					return false;
				}else{
					//console.log('anchor was clicked');
				}
			
				event.stopImmediatePropagation();
				event.stopPropagation();
				
				//Required for showing module specific loading gif(spinning wheel and blocking div)
				//ns.home.hideGlobalMessage();
				//var menu = ns.home.currentMenuId;
				//console.log("submenu currentMenuId=>"+menu);
				//ns.common.hidePrevMenuLoading(menu);
			
				//$("#desktopContainer").hide();
				ns.home.menuRefresh();
				
				//console.log("submenu :: parent attr id=>"+$(this).parent().attr("id"));
				var $menuTag = $("li[sublist='"+ $(this).parent().attr("id")+ "']");
				
				var menuId = $(this).attr("menuId");
				//console.log($(this));
				
				if(menuId.match("Preferences$")){
					//console.log("Preferences menu found, returning");
					return;
				}

				//It's causing some display issues when clicking on shortcuts
				if(!ns.common.shortcut){ 
					$('#notes').hide(); // Avoid showing page content in plain text before jQuery stuff is executed, It will be turned on in ns.home.displayToggleDashoardLink()
				}
				
				//console.log("submenu :: $menuTag=>");
				//console.log($menuTag);
				
				//$.publish("onTopMenuClick",[ns.home.currentMenuId,menuId]);
				
				ns.home.setMenuBackgroundStyle(menuId,true);
				
				ns.home.currentMenuId = menuId;
				
				//Highlight the selected main menu
				ns.home.highlightMainMenuOfSubmenu($(this));
				
				//console.log("submenu :: menuId=>"+menuId);

				ns.home.doSessionCleanUp();

				//When user clicks back button, when any dialog is open, this call is necessary to close it
				ns.common.closeDialog();

				//remove dialogs
				if(!ns.common.shortcut){
					ns.home.removeDialogs(menuId);
				}

				var menuName = $menuTag.find('.mainMenuCls').text();
				
				var subMenuName = $(this).find('a').text();
				
				ns.home.updateBreadcrumb(menuName,subMenuName,menuId);
				ns.home.loadGlobalMsg(menuId);
				//ns.home.togglePortletSettings(menuId);

				// Add browser histort entry. This is added for back and forward button support
				if(window.parent && window.parent.addHistoryEntry){
					window.parent.addHistoryEntry(menuId);
					window.parent.changeWindowTitle(subMenuName);
				}
				ns.home.setChangeTitle(subMenuName);
		};
		
		$("#tabbuttons").find('.mainSubmenuItemsCls').find('li').click(ns.home.subMenuClicked);
		//Second Level Menu Items ends
			
		//Extend session popup handling
		$("#extendsessionpopup").bind("dialogbeforeclose",function(event, ui) {
			ns.common.timerGoing = 'false';
			clearInterval(ns.common.timerIntervalId);
		});
});
//document ready ends

window.onload = function() {
	$('body').layout().resizeAll();
};

window.onresize = function() {
	$('body').layout().resizeAll();
};

$.subscribe('beforeSendingContactMyAdmin', function(event, data) {
	removeValidationErrors();
});

$.subscribe('successContactMyAdmin', function(event, data) {
	$('#quickMessagingResponse').html(event.originalEvent.data.response.data);
	//$.publish('refreshMessageSmallPanel');
	//$.publish("ns.common.resetRefreshPanels");
	$.publish("ns.common.getBankingEvents");
	
	//Commenting this, since we have global way to show responses
	/*$('body').jAlert($('#quickMessagingResponse').html(), "success", null, 300,
			'25');*/
	
	$.publish("closeDialog");

	if ('home_message' === ns.home.lastMenuId) {
		if ('Inbox' === ns.message.mailbox) {
			$('#inboxMessagesGridId').trigger("reloadGrid");
		} else if ('Sentbox' === ns.message.mailbox) {
			$('#sentMessagesGridId').trigger("reloadGrid");
		}
	}
});

// Showing Home ->Preferences -> Account tab
$.subscribe('ShowUserAccountPrefDetails', function(event, data) {
	ns.shortcut.navigateToSubmenus('userPreferencesVirtualMenu,accountPreferencesID');
	//ns.userPreference.tabToBeSelected = 1;
	//$('li[menuId=home_prefs] a').click();
	//ns.shortcut.goToFavorites('home_prefs');
	//$("#preferencesTabs").tabs("option", "selected", 1);
});

ns.home.getHomeSummaryDatas = function(urlString, divID) {
	$.get(urlString, function(data) {
		$(divID).html(data);
	});
}

ns.home.getHomeSummaryDatas = function(urlString, divID) {
	$.get(urlString, function(data) {
		$(divID).html(data);
	});
};

ns.home.destroyAndRemoveDialog = function(dialogArray) {
	if (dialogArray && dialogArray.length) {
		var counter = 0;
		var dialogObject = null;
		for (; counter < dialogArray.length; counter++) {
			dialogObject = $(dialogArray[counter]);
			dialogObject.dialog('destroy');// First of all call dialog provided
			// destory functionality
			dialogObject.remove();// Remove dialog from DOM
		}
	}
};

$.subscribe('onSavePortalPageDialogOpen', function(event, data) {
	if ($('#newPortalPageName') && $('#currentPortalPageName')) {
		$('#newPortalPageName').val(ns.home.currentPortalPage);
		$('#currentPortalPageName').val(ns.home.currentPortalPage);
	}
});

/*
 * This function will show error message especially generated while performing
 * operations through Portal page desktop right hand side top corner icons.(e.g.
 * Reset Portal Page)
 */
ns.home.showPortalOperationResult = function(errorMsg) {
	var $result = $('#portalIconOperationResult');
	$('#resultmsg').html(errorMsg);
	$result.addClass('ui-state-error');
	$result.slideDown();
	ns.common.scrollTo($result);
	setTimeout(function() {
		$result.slideUp();
	}, 3000);
	ns.common.closeDialog();
};

ns.home.hideDesktopLayout = function(){
	//$("#menu_preferences_layout").css({visibility: "hidden"});
	$("#desktopContainer").fadeOut(function() {
		$(this).removeClass("desktopContainerModelPopup");
	});
	$("#notes").show();
};

ns.home.hideMenuOnScrollDown = function(){
	var tabLayoutLocal = $("#tab1").layout();
	tabLayoutLocal.hide('north');
	$('#main-menu').css('visibility','hidden');
	$('body').layout().resizeAll();
};

ns.home.showMenuOnScrollUp = function(){
	var tabLayoutLocal = $("#tab1").layout();
	tabLayoutLocal.show('north');
	$('#main-menu').css('visibility','visible');
	$('body').layout().resizeAll();
};

/**
  * This function returns the locale name to load i18n property files for
  * JQ widgets
  */
  ns.home.getJQLocale = function(){
	var serverLocale,aLocale,jqLocale;
	serverLocale = ns.home.locale;
	aLocale = serverLocale.split("_");
	jqLocale = aLocale[0];
	if (ns.home.locale === "en_GB"
			|| ns.home.locale === "fr_CH"
				|| ns.home.locale === "pt_BR"
					|| ns.home.locale === "zh_CN"
						|| ns.home.locale === "zh_TW"
		) {
		jqLocale = jqLocale + "-" + aLocale[1];
	}

    return jqLocale;
  };
  
  
ns.home.setChangeTitle = function(menuName){
	  var windowTitle="";
		windowTitle = js_FFI_label;
		if(menuName){
			window.parent.document.title = windowTitle + ' - ' + menuName;
		} else {
			window.parent.document.title = windowTitle;
		}
};
 

//Functions moved from index.jsp
ns.home.executeAfterDomReady = function(){
	console.log("ns.home.executeAfterDomReady");
	var tempTimeout;
			
	//bind mousewheel event
	//call resizeAll() mehtod to resize the page when user zoomed the page by using mouse
	//browser version=IE8
	if(jQuery.browser.msie && document.documentMode == "8") {
		
		$("#outer-north").resize(function() {
			if(tempTimeout) clearTimeout(tempTimeout);
			tempTimeout=setTimeout(function(){$("body").layout().resizeAll()},400);
		});
	}

	//Load the Struts2 jQuery Grid dependencies and then call SJQ overrides and i18n overrides
	ns.common.loadSJQDependencies().done(function(){		
		//Override jQGrid Functions
		ns.common.overrideJQGridFunctions();	

		//Load locale specific JS files for grid and datapicker. 
		ns.home.initI18N();
	
		if(window.location.hash!='' && window.location.hash != undefined){
			ns.common.hideLoading(ns.common.landingMenuId);
			ns.common.landingMenuId = window.location.hash.split("#")[1];
			window.location.hash='';
		}
	
		$('li[menuId="' + ns.common.landingMenuId + '"]').find('a').eq(0).click();
		$('li[menuId="' + ns.common.landingMenuId + '"]').addClass("selectedMainMenuItemCls");
		//add your class here  menu is =  ns.common.landingMenuId
		var landingPageClass = ns.common.landingMenuId+'PageStyle';
		$('body').addClass(landingPageClass);
	});		

	$(".reloadIE").show();

	$('body').layout().resizeAll();		
				
	ns.common.resetDialogState();					
	
	$("#footerHolderDiv").css("position", "fixed");
	$("#tabpanels").css("top", "0");

	$('#remotefxdialog').addHelp(function(){
		var helpFile = $('#remotefxdialog').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$("#helpMe").addHelp(function(){
		var helpFile = $("#helpMe").find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$("#changeLanguage").addHelp(function(){
		var helpFile = $("#changeLanguage").find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$("#debugDiv").addHelp(function(){
		var helpFile = $("#debugDiv").find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$("#authenticationInfoDialogID").addHelp(function(){
		var helpFile = $("#authenticationInfoDialogID").find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$("#checkFileImportResultsDialogID").bind( "dialogbeforeclose", function(event, ui) {
		ns.common.closeDialog('#openFileImportDialogID');
		if(ns.common.canCloseFileUploadPortlet == true){
			$.publish('cancelFileImportForm');
		}
	});
}

//TODO: Attach the namespace for below functions, these were moved from index.jsp
function sessionActivityReportGobacktoFFihome(){
	window.location.href="/cb/pages/jsp/invalidate-session.jsp";
};

if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {  
	ns.home.isDevice = true;
} else {
	ns.home.isDevice = false;
};

function replyMessage(){
	var preparedUrl = "/cb/pages/jsp/message/SendReplyAction_initNotificationReply.action?MessageID="+$("#recievedMsgID").val();
	ns.home.quickReplyMessage(preparedUrl,$("#recievedMsgID").val());
};

function deleteMessage(){
	var preparedUrl = "/cb/pages/jsp/message/DeleteMessagesAction_confirm.action?quickDelete=true&selectedMsgIDs="+$("#recievedMsgID").val();
	ns.home.quickDeleteMessage(preparedUrl,$("#recievedMsgID").val());
};

function deleteAlert(){
	var preparedUrl = "/cb/pages/jsp/alert/deleteInboxAlertsAction_confirm.action?quickDelete=true&selectedMsgIDs="+$("#recievedAlertID").val();
	ns.home.quickDeleteAlert(preparedUrl,$("#recievedAlertID").val(),true);
} ;

ns.home.initI18N = function() {
	var jqLocale = ns.home.getJQLocale();
	//For multiselect widget
	$.widget("ech.extmultiselect", $.ech.extmultiselect, {
		options: {
			checkAllText: js_multiselect_check_all,
			uncheckAllText: js_multiselect_uncheck_all,
			noneSelectedText: js_multiselect_select_options
		}
	});
	//For multiselect filter
	$.widget("ech.multiselectfilter", $.ech.multiselectfilter, {
		options: {
			label: js_multiselectfilter_filter,
			placeholder: js_multiselectfilter_placeholder
		}
	});

	var fnCallback = function() {
		$.jgrid.no_legacy_api = true;
		$.jgrid.useJSON = true;
	}

	if (ns.home.locale != 'en_US') {
		$.when(
			$.cachedScript("/cb/struts/i18n/jquery.ui.datepicker-" + jqLocale + ".min.js"),
			$.cachedScript("/cb/web/js/common/widgets/i18n/ux.extdaterangepicker.locale." + jqLocale + ".min.js"),
			$.cachedScript('/cb/web/js/calendar/i18n/fullCalendar.locale-' + jqLocale + '.js')
		).done(fnCallback);
	} else {
		$.when(
			$.cachedScript('/cb/web/js/calendar/i18n/fullCalendar.locale-' + jqLocale + '.js')
		).done(fnCallback);
	}
};
