/**
 * Layout Controller manages users layouts
 */


//TODO Items
//GetUserLayouts after clone and modify


var layoutController = (function($, window, undefined) {
    var lc = {};

    var config = {
        urls: {
            "PORTAL_SETTINGS": "/cb/pages/jsp/home/getPortalSettings.action",
            "USER_LAYOUTS": "/cb/pages/jsp/home/getLayouts.action",
            "CLONE_LAYOUT": "/cb/pages/jsp/home/cloneLayout.action",
            "DELETE_LAYOUT": "/cb/pages/jsp/home/deleteLayout.action",
            "MODIFY_LAYOUT": "/cb/pages/jsp/home/modifyLayout.action",
            "SAVE_LAYOUT": "/cb/pages/jsp/home/saveLayout.action",
            "RESET_LAYOUT": "/cb/pages/jsp/home/resetLayout.action",
            "LOAD_LAYOUT": "/cb/pages/jsp/home/getLayout.action",
            "GET_DEFAULT_LAYOUT": "/cb/pages/jsp/home/getDefaultLayout.action"
        }
    };

    //Properties
    var userLayouts = [];
    var portalSettings = {};
    var selectedLayoutId = "";

    /**
     * Initialization function
     */
    function _init() {
        //getUsersLayouts();
        loadDefaultLayout();
        //getPortalSettings();
    };

    /**
     * This function loads the default layout for the user from available layouts
     */
    function loadDefaultLayout() {
        var aConfig = {
            url: config.urls.GET_DEFAULT_LAYOUT,
            global: false,
            success: function(data) {
                if (data != undefined && data.response != undefined && data.response.data != undefined) {
                    var layoutId = data.response.data.id;
                    //console.info("loading default layout: "+ layoutId);
                    //_loadLayout(layoutId);
                    _renderLayout(data);
                } else {
                    console.info("No default layout found to load.");
                }
            }
        };

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    };

    /**
     * This function gets the layout defined for the user
     */
    function getUsersLayouts() {
        //console.info("getUsersLayouts");
        var aConfig = {
            type: "GET",
            url: config.urls.USER_LAYOUTS,
            success: function(d) {
                if (d && d.response && d.response.data) {
                    userlayouts = d.response.data;
                }
            }
        };

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    };

    /**
     * This function returns the portal settings
     */
    function getPortalSettings() {
        //console.info("getPortalSettings");
        var aConfig = {
            type: "GET",
            url: config.urls.PORTAL_SETTINGS,
            success: function(d) {
                if (d && d.response && d.response.data) {
                    portalSettings = d.response.data;
                }
            }
        };

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    };

    /**
     * Loads the layout with layoutId
     */
    function _load(layoutId) {
        //console.info("_load: " + layoutId);
        var userlayout;
        var len = userlayouts.length;
        for (var i = 0; i < len; i++) {
            if (userlayouts[i].id == layoutId) {
                userlayout = userlayouts[i];
                break;
            }
        }

        var $layoutContent = $("#desktopContainer").find("#layoutContent");
        $layoutContent.empty();
        var columns = userlayout.columns;
        if (columns) {
            var NO_OF_COLUMNS = columns.length;
            var NO_OF_PORTLETS;
            for (var columnCtr = 0; columnCtr < NO_OF_COLUMNS; columnCtr++) {
                var column = columns[columnCtr];
                $layoutContent.append("<div class='column portlet ui-sortable " + column.cssClass + "' id='column" + columnCtr + "'></div>");
                var portlets = column.portlets;
                if (portlets) {
                    NO_OF_PORTLETS = portlets.length;
                    for (var portletCtr = 0; portletCtr < NO_OF_PORTLETS; portletCtr++) {
                        var portlet = portlets[portletCtr];
                        var updateLayout = false;
                        _addPortlet(portlet, updateLayout, columnCtr, portletCtr, userlayout);
                    }
                }
            }
        }

        if (userlayout.type != "system") {
            initSortable();
        }

        selectedLayoutId = layoutId;
        $.publish("onLayoutComplete", userlayout);
    };

    function _renderLayout(data){
    	 var userlayout = data.response.data;
				
		//show spinning wheel till all portlets load
        /*$('#desktopContainer').showLoading({
			'indicatorID' : 'home-portlets',
			'addClass': 'loading-indicator-dripcircle',
		    'afterShow': function() {
						setTimeout( "jQuery('#outer-center').hideLoading({'indicatorID':'home-portlets'})",
								$.getUISettings().AjaxSpinningWheelTimeout);
					}
			}
		);*/
		
        var $layoutContent = $("#desktopContainer").find("#layoutContent");
        $layoutContent.empty();
        var columns = userlayout.columns;
        var totalPortlets = 0;
		ns.common.usersPortlets = [];
		
        if (columns) {
            var NO_OF_COLUMNS = columns.length;
            var NO_OF_PORTLETS;
            for (var columnCtr = 0; columnCtr < NO_OF_COLUMNS; columnCtr++) {
                var column = columns[columnCtr];
                $layoutContent.append("<div class='column portlet ui-sortable " + column.cssClass + "' id='column" + columnCtr + "'></div>");
                var portlets = column.portlets;
                if (portlets) {
                    NO_OF_PORTLETS = portlets.length;
                    for (var portletCtr = 0; portletCtr < NO_OF_PORTLETS; portletCtr++) {
                        totalPortlets++;
                        var portlet = portlets[portletCtr];
                        var updateLayout = false;
                        _addPortlet(portlet, updateLayout, columnCtr, portletCtr, userlayout);
						ns.common.usersPortlets.push({"portletId": portlet.id, loaded: false });
                    }
                }
            }

            //Create a template of 2 column layouts if current layout is blank
            if (NO_OF_COLUMNS == 0) {
                $layoutContent.append("<div class='column portlet ui-sortable col50' id='column0'></div>");
                $layoutContent.append("<div class='column portlet ui-sortable col50' id='column1'></div>");
            }

        }

        if (totalPortlets == 0) {
            ns.layout.toggleBlankLayoutNotice(true);
        } else {
            ns.layout.toggleBlankLayoutNotice(false);
        }

        if (userlayout.type != "system") {
            initSortable();
        }

        selectedLayoutId = userlayout.id;
        $.publish("onLayoutComplete", userlayout);
    };

    /**
     * Loads the particular layout from Server with provided layoutId
     */
    function _loadLayout(layoutIdParam) {
        var aConfig = {
            url: config.urls.LOAD_LAYOUT,
            data: {
                layoutId: layoutIdParam
            },
            success: function(data) {
                var userlayout = data.response.data;
                _renderLayout(data);
            }

        }

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    };

    /**
     * This function makes copy if existing layout and saves with different name
     * This allows the user to clone standard and custom layouts and add his own
     * portlets
     */
    function _cloneLayout(layout) {
        //console.info("_cloneLayout");
        var scope = this;
        var aConfig = {
            url: config.urls.CLONE_LAYOUT,
            type: "POST",
            dataType: 'json',
            data: layout,
            success: function(data) {
                console.info("success in cloning");
                //getUsersLayouts();

            },
            error: function(e) {
                console.info("error in cloning: " + e);
            }
        };

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    }

    /**
     * This function modifies the current selected layout
     */
    function _saveLayout(layout) {
        //console.info("_saveLayout");
        var scope = this;
        var aConfig = {
            url: config.urls.SAVE_LAYOUT,
            type: "POST",
            dataType: 'json',
            data: layout,
            success: function(data) {
                //console.info("success: ");
                if (data.resultMessage && data.resultMessage != '') {
                    ns.common.showStatus(data.resultMessage);
                }

                if (data.savedLayout != undefined && data.savedLayout.id != undefined && data.savedLayout.id != '') {
                    _loadLayout(data.savedLayout.id);
                }
            },
            error: function(e) {
                console.info("error: " + e);
            }
        }

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    }


    /**
     * This function modifies the current selected layout
     */
    function _modifyLayout(layout) {
        //console.info("_modifyLayout");
        var scope = this;
        var aConfig = {
            url: config.urls.MODIFY_LAYOUT,
            type: "POST",
            dataType: 'json',
            data: layout,
            success: function(d) {
                console.info("success: ");
                //getUsersLayouts();
            },
            error: function(e) {
                console.info("error: " + e);
            }
        };

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    }

    /**
     * This function deletes the layout with passed layoutId
     */
    function _deleteLayout(layoutId) {
        //console.info("_deleteLayout: " + layoutId);
        var scope = this;
        var aConfig = {
            type: "POST",
            url: config.urls.DELETE_LAYOUT + "?layoutId=" + layoutId,
            success: function(d) {
                console.info("success");
                //scope.getUsersLayouts();
            },
            error: function(e) {
                console.info("error-" + e);
            }
        };

        var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
    };

    /**
     * This function adds the portlet
     */
    function _addPortlet(portlet, updateLayout, columnIndex, portletIndex, userLayout) {
        //console.debug("_addPortlet-", portlet.id, portlet.title, portlet.viewAction, portlet.editAction, updateLayout, columnIndex, portletIndex);
        if (portlet.id && portlet.title && portlet.viewAction) {
            $('#' + portlet.id).remove();
            var portletHTML = '<div id="' + portlet.id + '" class="portlet-state-initial"><div>' + js_loading_message + '</div></div>';
            if (columnIndex != null && portletIndex != null) {
                if ($('#layoutContent #column' + columnIndex + ' .portlet')[portletIndex]) {
                    $(portletHTML).insertBefore($('#layoutContent #column' + columnIndex + ' .portlet')[portletIndex]);
                } else {
                    $('#layoutContent #column' + columnIndex).append($(portletHTML));
                }
            } else {
                if ($('#layoutContent #column0 .portlet')[0])
                    $(portletHTML).insertBefore($('#column0 .portlet')[0]);
                else
                    $('#layoutContent #column0').append($(portletHTML));
            }

            initPortlet(portlet);
            ns.home.getViewMode($('#' + portlet.id), portlet.viewAction, true);
            if (updateLayout) {
                updatePortalPageLayout();
            }
        } else {
            console.error("Error, portlet is currently not available");
        }
    };

    /**
     * This function adds the portlet
     */
    function _addSelectedPortlet(id, title, viewAction, editAction, updateLayout, columnIndex, portletIndex) {
        if (id && title && viewAction) {
            $('#' + id).remove();
            var portletHTML = '<div id="' + id + '"/>';
            if (columnIndex != null && portletIndex != null) {
                if ($('#layoutContent #column' + columnIndex + ' .portlet')[portletIndex]) {
                    $(portletHTML).insertBefore($('#column' + columnIndex + ' .portlet')[portletIndex]);
                } else {
                    $('#layoutContent #column' + columnIndex).append($(portletHTML));
                }
            } else {
                var isSingleColumnLayout = false;
                if ($('#layoutContent #column1').length == 0) {
                    isSingleColumnLayout = true;
                }

                if (isSingleColumnLayout) {
                    if ($('#layoutContent #column0 .portlet')[0]) {
                        $(portletHTML).insertBefore($('#layoutContent #column0 .portlet')[0]);
                    } else {
                        $('#layoutContent #column0').append($(portletHTML));
                    }
                } else {
                    var col0Width = $('#layoutContent #column0').outerWidth();
                    var col1Width = $('#layoutContent #column1').outerWidth();

                    if (col0Width == col1Width) {
                        var portletsGroup0 = $('#column0 .portlet-content');
                        var portletsGroup1 = $('#column1 .portlet-content');

                        if (portletsGroup0.length <= portletsGroup1.length) {
                            if ($('#layoutContent #column0 .portlet')[0]) {
                                $(portletHTML).insertBefore($('#layoutContent #column0 .portlet')[0]);
                            } else {
                                $('#layoutContent #column0').append($(portletHTML));
                            }
                        } else {
                            if ($('#layoutContent #column1 .portlet')[0]) {
                                $(portletHTML).insertBefore($('#layoutContent #column1 .portlet')[0]);
                            } else {
                                $('#layoutContent #column1').append($(portletHTML));
                            }
                        }

                    } else if (col1Width >= col0Width) {
                        if ($('#layoutContent #column1 .portlet')[0]) {
                            $(portletHTML).insertBefore($('#layoutContent #column1 .portlet')[0]);
                        } else {
                            $('#layoutContent #column1').append($(portletHTML));
                        }
                    } else {
                        if ($('#layoutContent #column0 .portlet')[0]) {
                            $(portletHTML).insertBefore($('#layoutContent #column0 .portlet')[0]);
                        } else {
                            $('#layoutContent #column0').append($(portletHTML));
                        }
                    }
                }
            }

            var portlet = {
                "id": id,
                "title": title,
                "viewAction": viewAction,
                "editAction": editAction
            };

            initPortlet(portlet);

            $('#' + id).portlet('hide');
            $('#' + id).portlet('container').slideToggle('slow', function() {
                ns.home.getViewMode($('#' + id), viewAction, true);
            });
            if (updateLayout) {
                updatePortalPageLayout();
            }
        } else {
            alert("Sorry, this portlet is currently not available");
        }
    };




    /*****************************************************************
     * Helper functions
     *****************************************************************/

    /**
     * This function makes portlet columns sortable, sortable feature is
     * only allowed for custom layouts
     */
    function initSortable() {
        $(".column").sortable({
            cursor: "hand",
            connectWith: ".column",
            handle: ".portlet-header",
            cancel: ".portlet-toggle",
            placeholder: "portlet-placeholder ui-corner-all"
        });
    }


    /**
     * This function intializes the portlet. The basic config is extended with portletConfig and userLayout
     */
    function initPortlet(portletConfig) {
        //console.info("initPortlet");
        var hasEdit = true;
        if (!portletConfig.editAction) {
            hasEdit = false;
        }

        var config = {
            generateDOM: true,
            title: "",
            minmax: false,
            removeOnClose: true,
            edit: hasEdit,
            editAction: portletConfig.editAction,
            viewAction: "",
            duration: 'slow',
            help: false,
            highlight: true,
            resizeTitle: true,
            sortableHeaderClass: 'portlet-header-cursor',
            helpCallback: function() {
                var helpFile = $('#' + id).find('.moduleHelpClass').html();
                callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
            },
            editCallback: function(event, portlet, isHidden) {
                ns.home.getEditMode(portlet, portletConfig.editAction, isHidden); //TODO
            },
            viewCallback: function(event, portlet, isHidden) {
                ns.home.getViewMode(portlet, portletConfig.viewAction, isHidden); //TODO
            }
        };

        //Extend the basic properties with passed portlet properties
        $.extend(config, portletConfig);

        ns.layout.isMenuRollOver = false;

        //Instantiate the portlet
        if (config.id) {
            $('#' + config.id).portlet(config);
            var $portletSettings = $('<span/>').addClass("sapUiIconCls icon-action-settings portletSettingsIcon ui-widget-content");

            $portletSettings.mouseover(function(event) {
                var lMarginFromRight = 24;
                var lMarginFromTop = 25;
                var lLeftPos = $(this).position().left;
                var lTopPos = parseInt($(this).offset().top) + lMarginFromTop;
                var lMenuWidth = $("#" + config.id + "_portlet .portletHeaderMenuContainer").width() - lMarginFromRight;
                
                /*--- code to auto adjust the portlet dropdown in the available window container ---*/
                var docHeight = $(window).height()-35; // window height - footer
                var subDropdownHght = $("#"+config.id+ "_portlet .portletHeaderMenuContainer").height();
                var isDropdownVisible = (lTopPos + subDropdownHght <= docHeight);
                var lTopPosNew = parseInt($(this).offset().top)  - subDropdownHght ;

                if(isDropdownVisible)
                	$("#"+config.id+ "_portlet .portletHeaderMenuContainer").css("left", parseInt(lLeftPos - lMenuWidth)).css("top", lTopPos);
                else
                	$("#"+config.id+ "_portlet .portletHeaderMenuContainer").css("left", parseInt(lLeftPos - lMenuWidth)).css("top", lTopPosNew);
                
                $("#" + config.id + "_portlet .portletHeaderMenuContainer").fadeIn().mouseenter(function(event) {
                    ns.layout.isMenuRollOver = true;
                }).mouseleave(function(event) {
                    ns.layout.isMenuRollOver = false;
                    $("#" + config.id + "_portlet .portletHeaderMenuContainer").fadeOut();
                });

                $("#" + config.id + "_portlet .portletHeaderMenuContainer li").mouseover(function() {
                    $(this).addClass("ui-state-hover");
                }).mouseout(function() {
                    $(this).removeClass("ui-state-hover");
                });
            }).mouseout(function() {
                setTimeout(function() {
                    if (!ns.layout.isMenuRollOver) {
                        $("#" + config.id + "_portlet .portletHeaderMenuContainer").fadeOut();
                    }
                }, 300);
            });

            $('#' + config.id).portlet('addCustomContainer', $portletSettings);
        }
    }

    /*****************************************************************
     * Public methods for layout controller
     ******************************************************************/
    lc.init = function(config) {
        _init(config);
    };

    lc.load = function(layoutId) {
        _load(layoutId);
    };

    lc.loadLayout = function(layoutId) {
        _loadLayout(layoutId);
    };

    lc.cloneLayout = function(layoutId, layoutName, isDefault) {
        _cloneLayout(layoutId, layoutName, isDefault);
    };

    lc.modifyLayout = function(layout) {
        _modifyLayout(layout);
    };

    lc.saveLayout = function(layout) {
        _saveLayout(layout);
    };

    lc.deleteLayout = function(layoutId) {
        _deleteLayout(layoutId)
    };

    lc.addPortlet = function(portlet, updateLayout, columnIndex, portletIndex, userLayout) {
        _addPortlet(portlet, updateLayout, columnIndex, portletIndex, userLayout);
    };

    lc.addSelectedPortlet = function(portlet, updateLayout, columnIndex, portletIndex, userLayout) {
        _addSelectedPortlet(portlet, updateLayout, columnIndex, portletIndex, userLayout);
    };

    lc.getSelectedLayoutId = function() {
        return selectedLayoutId;
    };

    return lc;

}(jQuery, window));
