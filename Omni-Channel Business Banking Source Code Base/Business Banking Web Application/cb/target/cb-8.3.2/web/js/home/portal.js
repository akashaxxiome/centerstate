
ns.home.loadPortalPage = function(loadPortalAction) {
	$.post(loadPortalAction, function(data){
		if(data && data.portalPage && data.columnCount && data.portalPageName) {
			var portalPageData = data.portalPage;
			var length = portalPageData.length;
			for(var i=0;i<length;i++) {
				var columnData = portalPageData[i];
				if(columnData) {
					var portletCounts = columnData.length;
					for(var j=0;j<portletCounts;j++) {
						var portletData = columnData[j];
						if(portletData 
								&& portletData.portletId 
								&& portletData.title 
								&& portletData.viewAction) {
							ns.home.addPortlet(portletData.portletId, portletData.title, portletData.viewAction, portletData.editAction, false, i, j);
						}
					}
				}
			}
			if(data.columnCount == '1') {
				$('#column1').hide();
				$('#column2').hide();
				$('#column0').width("100%");
				$('#column1').empty();
			} else if(data.columnCount == '2') {
				$('#column1').show();
				$('#column2').hide();
				$('#column1').width("49%");
				$('#column0').width("49%");
			} else if(data.columnCount == '3') {
				$('#column0').hide();
				$('#column1').hide();
				$('#column2').show();
				$('#column2').width("100%");
				
			}
			ns.home.currentPortalPage = data.portalPageName;
			/*if($('#newPortalPageName') && $('#currentPortalPageName')) {
				$('#newPortalPageName').val(data.portalPageName);
				$('#currentPortalPageName').val(data.portalPageName);
			}*/
			ns.home.newPortalPageName = data.portalPageName;
			ns.home.currentPortalPageName = data.portalPageName;
		}
	});
};

ns.home.initPortlet = function(id, title, viewAction, editAction) {
		
		if(id) {
			var hasEdit = true;
			if(!editAction) {
				hasEdit = false;
			}
			$('#' + id).portlet({
				generateDOM:true,
				title:title,
				minmax: true,
				close:true,
				removeOnClose:true,
				closeCallback: ns.home.onClickRemovePortlet,
				edit:hasEdit,
				editAction: editAction,
				viewAction: viewAction,
				duration: 'slow',
				highlight: true,
				resizeTitle: true,
				sortableHeaderClass: 'portlet-header-cursor',
				helpCallback:
					function(){
						var helpFile = $('#' + id).find('.moduleHelpClass').html();
						callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
					},
				editCallback:
					function(event, portlet, isHidden){
						ns.home.getEditMode(portlet, editAction, isHidden);
					},
				viewCallback:
					function(event, portlet, isHidden){
						ns.home.getViewMode(portlet, viewAction, isHidden);
					}
			});
		}
	}
	
ns.home.onPositionUpdate = function(event, data) {
		updatePortalPageLayout();
	}

	$.subscribe('onSortStart', function (){
		var height1 = $('#column0').height();
		var height2 = $('#column1').height();
		if(height1 != height2) {
			if(height1 < height2)
				$('#column0').height(height2);
			else 
				$('#column1').height(height1);
		}	
	});
	
	$.subscribe('onSortStop', function (){
		$('#column0').css('height', 'auto');
		$('#column1').css('height', 'auto');
	});
	
	$.subscribe('onupdate', ns.home.onPositionUpdate);
	
	
	$('#portlet-menu').html($('#preference-menu-bar').html());
	
ns.home.addPortlet = function(id, title, viewAction, editAction, updateLayout, columnIndex, portletIndex){
		if(id && title && viewAction) {
			$('#' + id).remove();
			var portletHTML = '<div id="' + id + '"/>' ;
			if(columnIndex != null && portletIndex != null) {
				if($('#column' + columnIndex + ' .portlet')[portletIndex]) {
					$(portletHTML).insertBefore($('#column' + columnIndex + ' .portlet')[portletIndex]);
				} else {
					$('#column' + columnIndex).append($(portletHTML));
				}
			} else {
				if($('#column0 .portlet')[0])
					$(portletHTML).insertBefore($('#column0 .portlet')[0]);
				else
					$('#column0').append($(portletHTML));
			}
			
			ns.home.initPortlet(id, title, viewAction, editAction);
			$('#' + id).portlet('hide');
			$('#' + id).portlet('container').slideToggle('slow', function() {
				ns.home.getViewMode($('#' + id), viewAction, true);
			});
			if(updateLayout) {
				updatePortalPageLayout();
			}
		} else {
			alert("Sorry, this portlet is currently not available");
		}
	}

ns.home.removePortlet = function(portletID) {
	var portlet = $("#"+portletID).portlet();
	ns.home.onClickRemovePortlet(null, portlet);
};

ns.home.onClickRemovePortlet = function(event, portlet) {
		var txtCancel = js_CANCEL;
		var txtOK = js_OK;
		var l10nButtons = {};
		l10nButtons[txtCancel] = function() { $(this).dialog('close'); };
		l10nButtons[txtOK] = function() { ns.home.onRemovePortlet(portlet); $(this).dialog('close'); };
		var result = $('#removeConfirmDialog').dialog({
			modal: true,
			width: 300,
			height: 150,
			autoOpen: true,
			buttons: l10nButtons,
			show: 'fold',
			hide: 'clip',
			resizable: false,
			close:function(event, ui) { 
				$('#removeConfirmDialog').dialog('destroy');
			}
		});
	}
	
ns.home.getEditMode = function(portlet, editAction, isHidden) {
		ns.home.loadPortletContentWithEffect(portlet, editAction, isHidden,"EDIT");
	}
	
ns.home.getViewMode = function(portlet, viewAction, isHidden) {
		ns.home.loadPortletContentWithEffect(portlet, viewAction, isHidden,"VIEW");
	}
	
ns.home.loadPortletContentWithEffect = function(portlet, action, isHidden,aMode) {
		if(isHidden) {
			loadPortletContent(portlet, action,aMode);
		} else {
			portlet.slideToggle('slow', function(){
				loadPortletContent(portlet, action,aMode);
			});
		}
	}
	
ns.home.createPortletId = function(portletId) {
		var guid = $.Guid.New();
		return portletId + "_" + guid.replace(new RegExp('-','g'),'_') ;
	}
	
ns.home.onRemovePortlet = function(portlet) {
	var portletId = portlet.attr('id');
	var container = portlet.portlet('container');
		container.slideUp('slow', function() {
			portlet.remove();
			container.remove();
			updatePortalPageLayout();
		});
	$.delAttrByRoot(portletId);
	portletId = null;
}

ns.home.showPortletHelp = function(portletID){
	var helpFile = $('#' + portletID).find('.moduleHelpClass').html();
	callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
};
	
ns.home.onChangeLayout = function() {
		var style = $('input[name="pageLayoutStyle"]:checked').val();
		if(style == 2 && $('#column1').is(":hidden")) {
			$('#column1').show();
			$('#column2').hide();
			$('#column1').width("49%");
			$('#column0').width("49%");
			
			//copy portlets to right column
			var portlets = $('#column0').children();
			if(portlets && portlets.length > 1) {
				var num = portlets.length;
				var startIdx = Math.floor(num / 2);
				if(num % 2 == 1) {
					startIdx++;
				}
				for(var i=startIdx; i < num; i++) {
					$('#column1').append(portlets[i]);
				}
			}
			$('#column0').children().each(function(){
				if($(this).find('.visualize').length>0){
					$(this).find('.portlet-content').portlet('resizeChart');
				}
			});
			$('#column1').children().each(function(){
				if($(this).find('.visualize').length>0){
					$(this).find('.portlet-content').portlet('resizeChart');
				}
			});
			updatePortalPageLayout();
		} else if (style == 1 && $('#column1').is(":visible")) {
			$('#column1').hide();
			$('#column2').hide();
			$('#column0').width("100%");
			$('#column0').append($('#column1').children());
			$('#column1').empty();
			$('#column0').children().each(function(){
				if($(this).find('.visualize').length>0){
					$(this).find('.portlet-content').portlet('resizeChart');
				}
			});
			updatePortalPageLayout();
		} else if (style == 3) {
			$('#column2').show();
			$('#column0').hide();
			$('#column1').hide();
			$('#column2').width("100%");
			
			ns.home.addPortletToCarousel();
			$('#globalmessage').hide();
			updatePortalPageLayout();
		}

		ns.common.resizeWidthOfGrids();

	}

	ns.home.addPortletToCarousel = function() {
		 var oCarousel2 = new sap.ui.commons.Carousel();
	        oCarousel2.setOrientation("horizontal");
	        oCarousel2.setVisibleItems(1);
	        oCarousel2.setHeight("500px");
	        //oCarousel2.setWidth("1020px");

	        var count = 0;
	        $('#column0').children().each(function(){
	        	 var html1 = new sap.ui.core.HTML("html"+ count++, {
	                 // the static content as a long string literal
	                 content:$(this).html() ,
	                 preferDOM : false
	        	 });
	        	
		        var oPanel = new sap.ui.commons.Panel({
		                width : "99%"
		        });
		        
		        oPanel.addContent(html1);
		        
		        oCarousel2.addContent(oPanel);
			});
	        
	        $('#column1').children().each(function(){
	        	var html1 = new sap.ui.core.HTML("html"+ count++, {
	                 // the static content as a long string literal
	                 content:$(this).html() ,
	                 preferDOM : false
	        	 });
	        	
		        var oPanel = new sap.ui.commons.Panel({
		                width : "99%"
		        });
		        
		        oPanel.addContent(html1);
		        
		        oCarousel2.addContent(oPanel);
			});
	        oCarousel2.placeAt("portletCarousel");
	}

	
	$('.preference-menu').hover(function(){
		$(this).toggleClass('ui-state-highlight');
	});
	
	$('.layout-chooser').mouseover(function(){
		$(this).addClass('ui-state-hover');
	});
	
	$('.layout-chooser').mouseout(function(){
		$(this).removeClass('ui-state-hover');
	});
	
	$('.layout-chooser').click(function(){
		$(this).find('input').attr('checked','checked');
	});
	
	$.subscribe('onBeforeLayoutDialogOpen', function() {
		var columnNum = $('.column:visible').length;
		var pageLayoutRadioID = "pageLayoutRadioID" + (columnNum);
		//following tweak is needed as there might be multiple radio buttons due to duplicate dialog problem
		$('input:radio[id='+pageLayoutRadioID+']').each(function(index) {
		    $(this).attr('checked', true);
		});
	});
	
ns.home.switchToView = function(portlet) {
	portlet.portlet('hideCtrlIcon', '.view');
	portlet.portlet('showCtrlIcon', '.edit');
}

ns.home.getPortalPageLayout = function() {
	var layout = "";
	var columns = $('#portal-container').children('.column');
	if(columns) {
		$.each(columns, function() {
			var portlets = $(this).children('.portlet');
			if(portlets) {
				$.each(portlets, function() {
					var id = $(this).find('.portlet-content').attr('id');
					if(id) {
						layout += id + ',';
					}
				});
				layout = layout.substring(0, layout.length - 1);
			}
			layout += ";";
		});
	}
	return layout;
}

ns.home.onOpenPortalPagesDialog = function(url,csrfVarForOpenPortalPagesDialog) {
	/*if($('#portalPagesDialog'.length>0)) {
		$('#portalPagesDialog').remove();
	}*/
	
	var txtClose = js_CLOSE;
	var txtDelete = js_DELETE;
	var txtLOAD = js_LOAD
	var l10nButtons = {};
	l10nButtons[txtClose] = function() { $(this).dialog('close'); };
	l10nButtons[txtDelete] = function() { onDeletePortalPage(); };
	l10nButtons[txtLOAD] = function() { onLoadPortalPage(); $(this).dialog('close');};
			
	var dialogObj = $("<div id='portalPagesDialog'/>").dialog({
		width: 400,		
		show: 'fold',
		hide: 'clip',
		resizable: false,
		modal: true,
		position: ['center','middle'],
		title: js_Opening,
		buttons: l10nButtons,
		close: function(event, ui) {
			//Destroy dialog
			$('#portalPagesDialog').dialog('destroy');
			$('#portalPagesDialog').remove();
		}
	});
	
	dialogObj.addHelp(function(){
		var helpFile = dialogObj.find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	$.ajax({
		url: url,
		//type: "GET",
		
		//URL Encryption: Change "get" method to "post" method
		//Edit by Dongning
		type: "POST",
		data: {CSRF_TOKEN: csrfVarForOpenPortalPagesDialog},
		success: function(data) {
			dialogObj.html(data);
			dialogObj.dialog("open");
			dialogObj.dialog('option', 'title', js_Pages);
		}
	});
}

ns.home.onOpenPortalPortletsDialog = function(url, csrfVarForOpenPortalPortletsDialog) {
	/*if($('#portalPortletsDialog')) {
		$('#portalPortletsDialog').remove();
	}*/
	
	var txtClose = js_CLOSE;
	var l10nButtons = {};
	l10nButtons[txtClose] = function() { $(this).dialog('close'); };
	
	var dialogObj = $("<div id='portalPortletsDialog'/>").dialog({
		width: 400,	
		maxHeight:0,
		height:600,
		show: 'fold',
		hide: 'clip',
		resizable: false,
		modal: true,
		position: ['center','middle'],
		title: js_Opening
	});
	
	dialogObj.addHelp(function(){
		var helpFile = dialogObj.find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	$.ajax({
		url: url,
		//type: 'GET',
		//URL Encryption: Change "get" method to "post" method
		//Edit by Dongning
		type: "POST",
		data: {CSRF_TOKEN: csrfVarForOpenPortalPortletsDialog},
		dataType: 'html',
		success: function(data) {
			dialogObj.html(data);			
			
			$('#portletList td').hover(function(){
				$(this).toggleClass("ui-state-hover");
			});
			dialogObj.dialog('option', 'title', js_Portlets);			
			dialogObj.dialog('open');			
			var portletIds = ns.home.getPortalPageLayout();
			if(portletIds) {
				var anchors = $('#portalPortletsDialog a');
				if(anchors) {
					$.each(anchors, function() {
						var id = $(this).attr('id');
						if(portletIds.indexOf(id) != -1 && id!='GENERICPORTLET') {
							ns.home.changeButtonIcon($(this));
						}
					});
				}
			}
		}
	});
};


ns.home.onOpenLayoutPortletsDialog = function(url, csrfVarForOpenPortalPortletsDialog) {
	/*if($('#portalPortletsDialog')) {
		$('#portalPortletsDialog').remove();
	}*/
	
	var txtClose = js_CLOSE;
	var l10nButtons = {};
	l10nButtons[txtClose] = function() { $(this).dialog('close'); };
	
	var dialogObj = $("<div id='portalPortletsDialog'/>").dialog({
		width: 400,	
		maxHeight:0,
		height:600,
		show: 'fold',
		hide: 'clip',
		resizable: false,
		modal: true,
		position: ['center','middle'],
		title: js_Opening
	});
	
	dialogObj.addHelp(function(){
		var helpFile = dialogObj.find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	$.ajax({
		url: url,
		type: "POST",
		global:false,
		data: {CSRF_TOKEN: csrfVarForOpenPortalPortletsDialog},
		dataType: 'html',
		success: function(data) {
			dialogObj.html(data);			
			
			$('#portletList td').hover(function(){
				$(this).toggleClass("ui-state-hover");
			});
			dialogObj.dialog('option', 'title', js_Portlets);			
			dialogObj.dialog('open');	
			
			var anchors = $('#portalPortletsDialog a');
			if(anchors) {
				$.each(anchors, function() {
					var portletID = $(this).attr('id');
					
					if(portletID!='GENERICPORTLET'){
						var renderedPortlet = $("#desktopContainer").find('[id^='+portletID+'_]');
						if($(renderedPortlet).length>0){
							ns.home.changeButtonIcon($(this));
						}
					}
				});
			}
		}
	});
};


$('#newPortalPageSaveAnchor').button({
	icons: {primary:'ui-icon-disk'}
});

$('#newPortalPageCancelAnchor').button();

ns.home.onClickCloseSavePageForm = function() {
	$('#confirmSavePortalPageError').hide();
	$('#confirmSavePortalPageErrorMsg').html('');
	$.publish("common.topics.tabifyDialog");

	var config = ns.common.dialogs["savePortalPage"];
	var beforeDialogOpen = function(){			
		//add help
		$("#savePortalPageDialogID").addHelp(function(){		
			var helpFile = $("#savePortalPageDialogID").find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		});
		
		$('#newPortalPageName').val(ns.home.currentPortalPageName);
	}
	config.beforeOpen = beforeDialogOpen;
	ns.common.openDialog("savePortalPage");
};


ns.home.openSaveLayoutPage = function() {
	$.publish("common.topics.tabifyDialog");
	
	var saveLayoutURL = "/cb/pages/jsp/home/inc/saveCurrentLayout.jsp";
	$.ajax({
		url : saveLayoutURL,
		success : function(data) {
			$("#msgDisplayHolder").css("display", "none");
			var config = ns.common.dialogs["savePortalPage"];
			var beforeDialogOpen = function() {
				$('#saveLayoutBand').html(data);
				$('#saveLayoutBand').slideDown();
				$('#deleteLayoutBand').hide();
				
				//add help
				$("#savePortalPageDialogID").addHelp(function(){		
					var helpFile = $("#savePortalPageDialogID").find('.moduleHelpClass').html();
					callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
				});
			};
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("savePortalPage");
		}
	});
};

ns.home.hideErrorMsg = function() {
	var newName = $('#newPortalPageName').val();
	if(!$.trim(newName)) {
		$('#confirmSavePortalPageError').hide();
	}
};

ns.home.onOpenSavePageConfirmDialog = function() {
	if($('#confirmSavePortalPage').data("ffi-dialog") != undefined){
		$('#confirmSavePortalPage').dialog('destroy');
	}
	//$('#confirmSavePortalPage').dialog('destroy');
	$('#confirmSavePortalPage').remove();
	var newName = $('#newPortalPageName').val();
	var newNameString = $.trim(newName);
	/*
	if(!newNameString) {
		$('#confirmSavePortalPageError').show();
		$('#confirmSavePortalPageErrorMsg').html('Page name cannot be empty!');
		return;
	}else{
		if(/^[^a-zA-Z0-9]/.test(newNameString)){
			$('#confirmSavePortalPageError').show();
			$('#confirmSavePortalPageErrorMsg').html('Page name must start with number or English letter');
			return;
		}else if(!(/^[\w\s]+$/.test(newNameString))){
			$('#confirmSavePortalPageError').show();
			$('#confirmSavePortalPageErrorMsg').html('Page name must be the combination of English letter, number, underline mark or space');
			return;
		}else {
			$('#confirmSavePortalPageError').hide();
		}
	}
	*/
	var isDefault = $('#newPortalPageSetDefault').is(':checked');
	var mode = $('input[name="newPortalPageSaveMode"]:checked').val();
	
	$('#confirmSavePortalPageName').html(newName);
	if(isDefault) {
		$('#confirmSavePortalPageDefault').html(js_Yes);
	} else {
		$('#confirmSavePortalPageDefault').html(js_No);
	}
	if(mode == 1) {
		$('#confirmSavePortalPageMode').html(js_home_createNewPage);
		$('#confirmSavePortalPageUpdateWarn').hide();
		$('#confirmSavePortalPageResetWarn').hide();
	}
	if(mode == 2) {
		$('#confirmSavePortalPageMode').html(js_home_overrideCurPage);
		$('#confirmSavePortalPageUpdateWarn').show();
		$('#confirmSavePortalPageResetWarn').hide();
	}
	if(mode == 3) {
		$('#confirmSavePortalPageMode').html(js_home_resetCurPage);
		$('#confirmSavePortalPageUpdateWarn').hide();
		$('#confirmSavePortalPageResetWarn').show();
	}
	
	onSavePortalPage(newName, isDefault, mode);
	/*	
	var confirmSavePortalBtns = {};
	var txtCANCEL = js_CANCEL;
	var txtCONFIRM = js_CONFIRM;
	confirmSavePortalBtns[txtCANCEL] = function(){	$(this).dialog('close'); }
	confirmSavePortalBtns[txtCONFIRM] = function() { 
				$(this).dialog('close'); 
				onSavePortalPage(newName, isDefault, mode);
			}
	
	var dialogObj = $('#confirmSavePortalPage').dialog({
		width: 400,
		autoOpen: true,
		show: 'fold',
		hide: 'clip',
		resizable: false,
		modal: true,
		position: ['center','middle'],
		title: js_home_ConfirmSavePortalPage,
		buttons: confirmSavePortalBtns
	});
*/
}

ns.home.changePortalPage = function(pageName, action) {
	$.ajax({
			url: action,
			type: "POST",
			data: {portalPageName : pageName,CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			success: function(data) {
				//Clear dialogs before reloading desktop section. 
				//Using lastMenuId as this variable must be containing homepage menu Id.
				ns.home.removeDialogs(ns.home.lastMenuId);
				
				isPortalModified = false;
				$('#notes').html(data);
				$.publish("common.topics.tabifyAll");
				if(accessibility){				
					accessibility.setFocusAfterPageLoad();
				}

			}
	});
}

ns.home.onClickUpdateCurrent = function() {
	var currentPageName = $('#currentPortalPageName').val();
	if(currentPageName === undefined || currentPageName == ""){
		currentPageName = ns.home.currentPortalPageName;
	}
	$('#newPortalPageName').val(currentPageName);
	$('#newPortalPageName').attr('disabled',true);
	$('#newPortalPageName').addClass('ui-state-disabled');
}

ns.home.onClickCreateNew = function() {
	$('#newPortalPageName').attr('disabled',false);
	$('#newPortalPageName').removeClass('ui-state-disabled');
	$('#newPortalPageSetDefault').attr('disabled',false);
	$('#newPortalPageSetDefault').removeClass('ui-state-disabled');
}

ns.home.onClickResetDefault = function() {
	var currentPageName = $('#currentPortalPageName').val();
	if(currentPageName === undefined || currentPageName == ""){
		currentPageName = ns.home.currentPortalPageName;
	}
	$('#newPortalPageName').val(currentPageName);
	$('#newPortalPageName').attr('disabled',true);
	$('#newPortalPageName').addClass('ui-state-disabled');
	$('#newPortalPageSetDefault').attr('disabled',false);
	$('#newPortalPageSetDefault').removeClass('ui-state-disabled');
}

//add for buttons in portal-portlet-dialog.jsp page
ns.home.togglePortlet = function(buttonId, id, title, viewAction, editAction, updateLayout){
	var obj=$("a[id='" + buttonId + "']");
	var clickType = obj.children().hasClass('ui-icon-close');
	if (clickType==true) {
		//click close
		ns.home.addPortlet(id, title, viewAction , editAction , updateLayout);
		ns.home.changeButtonIcon(obj);
	}else{
		//click check
		var portlet = $("div .portlet-content[id^='" + buttonId + "']");
		ns.home.onRemovePortlet(portlet);
		ns.home.changeButtonIcon(obj, true);
	}
}

ns.home.changeButtonIcon = function(obj, isClose){
	if(!isClose) {
		//click close
		obj.children().removeClass('ui-icon-close');
		obj.children().addClass('ui-icon-check');
		// obj.children().removeClass('opacity50');
	}else{
		//click check
		obj.children().removeClass('ui-icon-check');
		obj.children().addClass('ui-icon-close');
		// obj.children().addClass('opacity50');
	}
}
//add end

//QTS 684571: A portlet's hover is not working properly when mouse moves directly into jQgrid.
//This function bind the portlet's hover event to every jQgrid it owns.
ns.home.bindPortletHoverToTable = function(portlet){
	if (portlet) {
		portlet.portlet('bindHoverToTable');
	}
}
//QTS 684571 end
