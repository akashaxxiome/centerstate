/******************** Function required for form submission using AJAX ******************/
//Function to fetch JSP page to show Edit screen for pending transfer.
ns.pendingTransactionsPortal.editSingleTransfer = function( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
			ns.common.removeDialog('innerdeletetransferdialog');
			//$('#editHomeTransferDialogID').html(data).dialog('open');
			
			var config = ns.common.dialogs["editHomeTransfer"];
			var beforeDialogOpen = function(){			
				$('#editHomeTransferDialogID').html(data);				
			};
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("editHomeTransfer");	
			
			ns.pendingTransactionsPortal.gotoStep(1); 
		}
	});
};

//Function to fetch JSP page to show delete screen for pending transfer.
ns.pendingTransactionsPortal.deleteTransfer = function( urlString){
	$.ajax({
		url: urlString,
		success: function(data){
			//$('#deleteHomeTransferDialogID').html(data).dialog('open');
			var config = ns.common.dialogs["deleteHomeTransfer"];
			var beforeDialogOpen = function(){			
				$('#deleteHomeTransferDialogID').html(data);				
			};
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("deleteHomeTransfer");	
		}
	});
};

//Function to fetch JSP page to show delete screen for pending BillPayment.
ns.pendingTransactionsPortal.deleteBillpay = function ( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
			//$('#deleteHomeBillpayDialogID').html(data).dialog('open');
			var config = ns.common.dialogs["deleteHomeBillpay"];
			var beforeDialogOpen = function(){			
				$('#deleteHomeBillpayDialogID').html(data);				
			};
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("deleteHomeBillpay");	
		}
	});
};

//Function to fetch JSP page to show edit screen for pending BillPayment.
ns.pendingTransactionsPortal.editSingleBillpay = function ( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
			//$('#editHomeBillPaymentDialogID').html(data).dialog('open');
			var config = ns.common.dialogs["editHomeBillPayment"];
			var beforeDialogOpen = function(){			
				$('#editHomeBillPaymentDialogID').html(data);				
			};
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("editHomeBillPayment");	
			ns.pendingTransactionsPortal.gotoStep(1); 
		}
	});
};


ns.pendingTransactionsPortal.gotoStep = function(stepnum){
	
	ns.common.gotoWizardStep('pendingTransactionNavigationPortlet',stepnum);
	
	var tabindex = stepnum - 1;
	$('#PendingTransactionWizardTabs').tabs( 'enable', tabindex );
	$('#PendingTransactionWizardTabs').tabs( 'option','active', tabindex );
	$('#PendingTransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
	$('#PendingTransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );
//	$('#TransactionWizardTabs').tabs( "option", "disabled", [0,2] );
			
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){						
		var selectedTab = $("#PendingTransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
		$("#"+selectedTab).find(":tabbable:first").focus();						
	}
}
/******************** Formatter function used for grid ********************/
//Formatter of Frequency column
ns.pendingTransactionsPortal.formatTransferFrequencyColumn = function(cellvalue, options, rowObject){
	if(cellvalue == undefined ){
		if( rowObject.frequency == undefined ){
			return js_one.time;
		} else if(rowObject.frequency == ''){
			return js_one.time;
		} else	{
			return rowObject.frequency;  
		}
	} else {
		if(cellvalue == '')	{
			return js_one.time;
		} else	{
			return cellvalue;  
		}
	}
};

//Formatter of Type column.
/*ns.pendingTransactionsPortal.formatTypeColumn = function( cellvalue, options, rowObject ){
	var _transferDestination = rowObject.transferDestination;

	if( _transferDestination == "INTERNAL" || _transferDestination == "ITOI" ){
		_transferDestination = js_transfer_dest_ITOI;	
	} else if ( _transferDestination == "ITOE" ){
		_transferDestination = js_transfer_dest_ITOE;
	} else if ( _transferDestination == "ETOI" ){
		_transferDestination = js_transfer_dest_ETOI;
	}

	return _transferDestination;
};*/

//Formatter of Frequency column
ns.pendingTransactionsPortal.formatTransferNumberColumn = function(cellvalue, options, rowObject){
	if(cellvalue=='999'){
			return "--";
	}else if(cellvalue){
		return cellvalue;
	}else{
		return '';
	}
};

//Formatter of Amount column.
ns.pendingTransactionsPortal.formatNumberPayments = function ( cellvalue, options, rowObject ){
	if (cellvalue=='999'){
			return "--";
	} else if (cellvalue){
		return cellvalue;
	} else {
		return '';
	}
};

//Formatter of From Amount column.
ns.pendingTransactionsPortal.formatFromAmountColumn = function( cellvalue, options, rowObject ){
	var fromAmount ="";
	if(rowObject["fromAccount"].currencyCode==null){
		fromAmount = cellvalue + ' ' + rowObject["amountValue"].currencyCode;
	}else{
		fromAmount = cellvalue + ' ' + rowObject["fromAccount"].currencyCode;
	}
	if(rowObject["isAmountEstimated"]){
		fromAmount = '&#8776' + ' ' + fromAmount;
	}
	return fromAmount;
};

ns.pendingTransactionsPortal.formatCombinedAmount = function( cellvalue, options, rowObject ){
	var fromAmount = ns.pendingTransactionsPortal.formatFromAmountColumn(rowObject.amountValue.currencyStringNoSymbol, options, rowObject);
	var toAmount = ns.pendingTransactionsPortal.formatToAmountColumn(rowObject.toAmountValue.currencyStringNoSymbol, options, rowObject);
	if( toAmount == "--" ){
		return  fromAmount;
	}else{
		return  fromAmount +'--' + toAmount;
	}
};

ns.pendingTransactionsPortal.formatCombinedAccount = function(cellvalue, options, rowObject) {
	var fromAccount = ns.common.HTMLEncode(rowObject.fromAccount.accountDisplayText);
	var toAccount = ns.common.HTMLEncode(rowObject.toAccount.accountDisplayText);
	return "<span class='formatCombinedAccountColumn'>"+fromAccount + "</span><span class='gridColumnIcon'><span class='ffiUiMiscIco ffiUiIco-misc-multilinerowdataindicatorico'></span>" +toAccount+"</span>";
};


ns.pendingTransactionsPortal.formaAccountAndPayee = function(cellvalue, options, rowObject) {
	var from =  ns.common.HTMLEncode(rowObject.account.accountDisplayText);
	var to = rowObject.payeeName;
	var payeeNickName =  rowObject.payee.nickName;
	var userAccountNumber = rowObject.payee.userAccountNumber;
	var nickNameAndAccNumber = payeeNickName;

	if(userAccountNumber!=undefined && userAccountNumber!=""){
		nickNameAndAccNumber = nickNameAndAccNumber+" -- "+userAccountNumber;
	}
	
	return "<span class='formatCombinedAccountColumn'>"+from + "</span><span class='gridColumnIcon'><span class='ffiUiMiscIco ffiUiIco-misc-multilinerowdataindicatorico'></span>" +to+" ( "+nickNameAndAccNumber+" ) "+"</span>";
};

//Formatter of To Amount column.
ns.pendingTransactionsPortal.formatToAmountColumn = function( cellvalue, options, rowObject ){
	var toAmount = "";
	if( cellvalue == "0.00"){
		toAmount = "--";
	} else {
		toAmount = cellvalue + ' ' + rowObject["toAccount"].currencyCode;
		if(rowObject["isToAmountEstimated"]){
			toAmount = '&#8776' + ' ' + toAmount;
		}
	}
	return toAmount;
};

//Formatter of Amount column.
ns.pendingTransactionsPortal.formatAmountColumn = function ( cellvalue, options, rowObject ){
		var amount = cellvalue + ' ' + rowObject["amountValue"].currencyCode;
		return amount;
};

//Formatter of Frequency column from Bill-payment grid
ns.pendingTransactionsPortal.formatBillpayFrequencyColumn = function (cellvalue, options, rowObject){
	if(cellvalue == undefined ){
		if( rowObject.frequency == undefined ){
			return "None";
		}else{
			return rowObject.frequency;  //rowObject.frequency has value, means the Billpay is a RecBillpay.
		}
	} else{
		if(cellvalue == "None"){
			return "One Time";
		}else{
			return cellvalue;
		}
	}
};

/******************** UTILITY FUNCTION SECTION ********************/	
//Function to provide links in Action column of Pending Transfer grid.
ns.pendingTransactionsPortal.formatPendingTransfersActionLinks = function(cellvalue, options, rowObject) { 
	var enableButtons = rowObject.map.enableButtons;
	if(enableButtons != 'true')
	{
		return '';
	}
	var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.pendingTransactionsPortal.editSingleTransfer(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.pendingTransactionsPortal.deleteTransfer(\""+ rowObject.map.DeleteURL +"\", \"PendingTransfers\", \"false\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	
	if(rowObject["canEdit"] == "false") {
		edit = "";
	}
	if(rowObject["canDelete"] == "false") {
		del = "";
	}
	return ' ' + edit + ' '  + del ;
};

//Function to provide links with proper URL in Action column of Pending Bill Payments grid.
ns.pendingTransactionsPortal.formatPendingBillpayActionLink = function (cellvalue, options, rowObject) { 
	var isEntitled = rowObject.map.isEntitled;
	
	/*if(isEntitled != 'true'){
		return '';
	}*/
	
	var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.pendingTransactionsPortal.editSingleBillpay(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.pendingTransactionsPortal.deleteBillpay(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

	if(rowObject["canEdit"] == "false") {
		edit = "";
	}
	if(rowObject["canDelete"] == "false") {
		del = "";
	}
	return ' '  + edit + ' '  + del + ' ' ;
};

//Add approval information in the pending approval summary gird.
ns.pendingTransactionsPortal.addTransfersExtraRow = function(gridID,estimatedAmountLabel){
	var ids = jQuery("#"+gridID).jqGrid('getDataIDs');
	var rowId = ids[ids.length-1];
	var curRow = $("#" + rowId, $("#"+gridID));
	var className = "ui-widget-content jqgrow ui-row-ltr";
	if(ids.length % 2 == 1)
	{
		className = "ui-widget-content jqgrow ui-row-ltr ui-state-zebra";
	}		
	curRow.after('<tr class=\"' + className + '\"><td colspan="9" align="left"> <b> ' + estimatedAmountLabel + '</b></td></tr>');
};

//This function will set refreshed Grid response in the Div identified by passed divID  
ns.pendingTransactionsPortal.getHomeSummaryDatas = function( urlString, divID ){
	$.get(urlString, function(data) {
		$(divID).html(data);
		ns.common.resizeWidthOfGrids();
	});
};

// Initialize footer section for Pending Payments grid
ns.pendingTransactionsPortal.initPendingPaymentsGrid = function(gridID, topic){
	$.subscribe(topic, function(){
		//$('#' + gridID).jqGrid('setGridParam',{footerrow:'true'});
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		if (ids.length > 0) {
			var data = $('#' + gridID).jqGrid('getGridParam','userData');
			if(data) {
				$('#' + gridID).jqGrid('footerData','set',{'amountValue.currencyStringNoSymbol': data.totalAmountValue, 
				'numberPayments': data.totalText}, false);
				$('#pendingPaymentsNoData').hide();
				$('#pendingPaymentsData').show();
			}
		} else {
            var data = $('#' + gridID).jqGrid('getGridParam','userData');
            if (data && data.noPaymentsRecords && !$('#' + gridID).hasClass("noPaymentsRecords"))
            {
                $('#' + gridID).addClass('noPaymentsRecords');     // don't repeat this action
				var className = "ui-widget-content jqgrow ui-row-ltr";
			    $("#" + gridID + " tbody").after('<tr class=\"' + className + '\"><td colspan="9" align="left">  <b>' + data.noPaymentsRecords + '</b></td></tr>');
				//Hide footer row
				$('#gview_' + gridID + " .ui-jqgrid-ftable").hide();
				$('#pendingPaymentsNoData').html(data.noPaymentsRecords);
				$('#pendingPaymentsData').hide();
				$('#pendingPaymentsNoData').show();
			}
		}
	});
};

//Initialize footer section for Pending Transfer grid
ns.pendingTransactionsPortal.initPendingTransfersGrid = function(gridID, topic){
	ns.common.resizeWidthOfGrids();
	$.subscribe(topic, function(){
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		if (ids.length > 0) {
			var data = $('#' + gridID).jqGrid('getGridParam','userData');
			if(data) {
				$('#' + gridID).jqGrid('footerData','set',
					{
					//'date': data.estimatedAmountLabel, 
					'amountValue.currencyStringNoSymbol': data.fromTotalAmountValue, 
					'toAmountValue.currencyStringNoSymbol': data.toTotalAmountValue, 
					'numberTransfers': data.totalText
					}, false);

				if(data.estimatedAmountLabel && data.estimatedAmountLabel!=''){
					ns.pendingTransactionsPortal.addTransfersExtraRow(gridID,data.estimatedAmountLabel);
				}
				$('#pendingTransfersNoData').hide();
				$('#pendingTransfersData').show();
			}
		} else {			
            var data = $('#' + gridID).jqGrid('getGridParam','userData');
            if (data && data.noTransferRecords && !$('#' + gridID).hasClass("noTransferRecords"))
            {
                $('#' + gridID).addClass('noTransferRecords');     // don't repeat this action
				var className = "ui-widget-content jqgrow ui-row-ltr";
			    $("#" + gridID + " tbody").after('<tr class=\"' + className + '\"><td colspan="9" align="left">  <b>' + data.noTransferRecords + '</b></td></tr>');
				//Hide footer row
				$('#gview_' + gridID + " .ui-jqgrid-ftable").hide();
				$('#pendingTransfersNoData').html(data.noTransferRecords);
				$('#pendingTransfersData').hide();
				$('#pendingTransfersNoData').show();
			}			
		}
	});
};

//Formatter of payment Status column
ns.pendingTransactionsPortal.formatBillpayStatusColumn = function(cellvalue, options, rowObject){
		if(cellvalue == 13){//PMS_BATCH_INPROCESS
			return js_status_in_process;
		}else if(cellvalue == 12){//PMS_FUNDS_ALLOCATED
			return js_status_in_process;
		}else{//other status
			return js_status_pending;
		}
};


//Formatter of Transfer Status column
ns.pendingTransactionsPortal.formatTransferStatusColumn = function(cellvalue, options, rowObject){
	if(cellvalue == 20){//TRS_INPROCESS
		return js_status_in_process;
	}else if(cellvalue == 19){//TRS_IMMED_INPROCESS
		return js_status_in_process;
	}else if(cellvalue == 11){//TRS_BATCH_INPROCESS
		return js_status_in_process;
	}else if(cellvalue == 23){//TRS_FUNDS_INPROCESS
		return js_status_in_process;
	}else if(cellvalue == 21){//TRS_FUNDSPROCESSED
		return js_status_in_process;
	}else{//other status
		return js_status_pending;
	}
};