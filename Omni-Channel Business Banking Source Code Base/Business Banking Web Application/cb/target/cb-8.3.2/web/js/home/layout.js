/**
* Layout js handles event binding for all layout management actions
* ns.layout is registered in namespace.js
*/
ns.layout = (function($,window,undefined){
	//layout instance
	var instance = {};

	var config = {
		urls:{
			"SOME_ACTION":"/cb/pages/jsp/home/resetLayout.action"
		}
	};


	//var $layoutContainer = $("#desktopContainer");
	//var $notesContainer = $("#notes");

	/**
	* initialization function
	*/
	function _init(aConfig){
		$.extend(config,aConfig);
		//registerEvents();
	}

	function registerEvents(){
		//console.info("registering events layout");
	}

	function _changeLayout(layoutId){
		//console.info("_changeLayout");
		
		if(layoutController && layoutId!=''){
			layoutController.loadLayout(layoutId);
		}
	}
	
	function _modifyLayout(newLayoutName,replaceLayoutId){
		//console.info("_modifyLayout");
		//Construct the layout data
		var portletsGroup0 = $('#column0 div.portlet');
		var portletsGroup1 = $('#column1 div.portlet');

		var layoutColumns = [];
		var column0PortletIds = "";
		var column1PortletIds = "";
		var column0Size = portletsGroup0.length;
		var column1Size = portletsGroup1.length;

		if(column0Size > 0){
			for(var i = 0; i < column0Size; i++){
				var $portlet = $(portletsGroup0[i]);
				column0PortletIds = column0PortletIds + "," +  $portlet.attr("id");
			}
		}

		if(column1Size > 0){
			for(var i = 0; i < column1Size; i++){
				var $portlet = $(portletsGroup1[i]);
				column1PortletIds = column1PortletIds  + "," +  $portlet.attr("id");
			}
		}			

		layoutColumns[0] = column0PortletIds;
		layoutColumns[1] = column1PortletIds;
		var isDefault = $("#makeItDefault").is(":checked");
		
		var layoutData = {
			"sourceLayoutId":layoutController.getSelectedLayoutId(),
			"newLayoutName":newLayoutName,
			"replaceLayoutId":replaceLayoutId,
			"columns":layoutColumns,
			"isDefault": isDefault,
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
		}

		layoutController.modifyLayout(layoutData);		
	}
	
	function _saveLayout(newLayoutName,replaceLayoutId){
		//console.info("_saveLayout");
		//Construct the layout data
		/*
		var portletsGroup0 = $('#column0 div.portlet');
		var portletsGroup1 = $('#column1 div.portlet');
		*/
		var portletsGroup0 = $('#column0 .portlet-content');
		var portletsGroup1 = $('#column1 .portlet-content');
		
		var layoutColumns = [];
		var column0PortletIds = "";
		var column1PortletIds = "";
		var column0Size = portletsGroup0.length;
		var column1Size = portletsGroup1.length;

		if(column0Size > 0){
			for(var i = 0; i < column0Size; i++){
				var $portlet = $(portletsGroup0[i]);
				column0PortletIds = column0PortletIds + "," +  $portlet.attr("id");
			}
		}

		if(column1Size > 0){
			for(var i = 0; i < column1Size; i++){
				var $portlet = $(portletsGroup1[i]);
				column1PortletIds = column1PortletIds  + "," +  $portlet.attr("id");
			}
		}			

		layoutColumns[0] = column0PortletIds;
		layoutColumns[1] = column1PortletIds;
		var isDefault = $("#makeItDefault").is(":checked");
		
		var layoutData = {
			"sourceLayoutId":layoutController.getSelectedLayoutId(),
			"newLayoutName":newLayoutName,
			"replaceLayoutId":replaceLayoutId,
			"columns":layoutColumns,
			"isDefault": isDefault,
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
		}

		layoutController.saveLayout(layoutData);		
	}

	function _cloneLayout(){
		//console.info("_cloneLayout");
		var layoutName = $("#inputSaveLayout").val();
		var layoutId = layoutController.getSelectedLayoutId();
		var isDefault = $("#makeItDefault").is(":checked");

		var cloneData = {
			"layoutId":layoutId,
			"layoutName": layoutName,
			"isDefault": isDefault,
			"CSRF_TOKEN":ns.home.getSessionTokenVarForGlobalMessage
		};
		
		layoutController.cloneLayout(cloneData);
	}

	function _toggeleInlineView(showInline){
		$(".desktopLayoutDialogHeaderDiv").addClass("hidden");
		
		if(showInline){
			//Check if home page is loaded already, if not need to load home page.
			if($("#desktopContent").html().trim()==''){
				getHomePageContent();
			}
			$("#desktopContainer").fadeIn();
			$("#notes").hide();
		}else{
			$("#desktopContainer").hide();
			$("#notes").show();
		}
	}

	function _toggeleOverlayView(showOverlay){
		if(showOverlay){
			//Check if home page is loaded already, if not need to load home page.
			if($("#desktopContent").html().trim()==''){
				getHomePageContent();
			}
			$(".desktopLayoutDialogHeaderDiv").removeClass("hidden");
			$("#desktopContainer").fadeIn();
			$("#notes").hide();
			ns.common.resizeWidthOfGrids();
		}else{
			$("#desktopContainer").fadeOut();
			$("#notes").show();
		}
	}
	
	function _toggleBlankLayoutNotice(showNotice){
		if(showNotice != undefined && showNotice == true){
			$("#blankLayoutNotice").show();
		}else{
			$("#blankLayoutNotice").hide();
		}
	}
	
	function getHomePageContent(){
		var contextRelativePathToLoad = $("#menu_home").attr('link');
		
		//page content in plain text before jQuery stuff is executed
		var aConfig = {
			url : contextRelativePathToLoad,
			type : "POST",
			global: false,
			data : { CSRF_TOKEN : ns.home.getSessionTokenVarForGlobalMessage},
			success : function(data){
				$('#desktopContent').empty();
				$('#desktopContent').html(data);

				//need to call resize because it take effect only "notes" is shown
				ns.home.resizeTabPanelLayout();
				
				$.publish("common.topics.tabifyDesktop");
				
				ns.common.applyCommonTabIndex();

				if(accessibility){
					accessibility.setFocusOnDashboard();
				}
			}
		};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
        $.ajax(ajaxSettings);
	}
	
	function _togglePortlet(buttonId, id, title, viewAction, editAction, updateLayout){
		var obj=$("a[id='" + buttonId + "']");
		var clickType = obj.children().hasClass('ui-icon-close');
		// var clickType = obj.children().hasClass('opacity50');
		if (clickType==true) {
			//click close
			layoutController.addSelectedPortlet(id, title, viewAction , editAction , updateLayout);
			ns.home.changeButtonIcon(obj);
		}else{
			//click check
			var portlet = $("div .portlet-content[id^='" + buttonId + "']");
			ns.home.onRemovePortlet(portlet);
			ns.home.changeButtonIcon(obj, true);
		}
	}
	
	function _addPortlet(id, title, viewAction , editAction , updateLayout){
		layoutController.addSelectedPortlet(id, title, viewAction , editAction , updateLayout);
	}

	/*****************************************************************
	* Public methods
	******************************************************************/
	instance.init = function(aConfig){
		_init(aConfig);
	};

	instance.changeLayout = function(layoutId){
		_changeLayout(layoutId);
	};

	instance.modifyLayout = function(layoutName){
		_modifyLayout(layoutName);
	};

	//Save dialog handler functions
	instance.saveLayoutHandler = function(event,newLayoutName,replaceLayoutId){
		_saveLayout(newLayoutName,replaceLayoutId);
	};	
	
	//Toggle Portlet
	instance.togglePortlet = function(buttonId, id, title, viewAction, editAction, updateLayout){
		_togglePortlet(buttonId, id, title, viewAction, editAction, updateLayout);
	};	
	
	//Add portlet
	instance.addPortlet = function(id, title, viewAction, editAction, updateLayout){
		_addPortlet(id, title, viewAction, editAction, updateLayout);
	};	
	
	instance.toggleInlineView = function(showInline){
		_toggeleInlineView(showInline);
	};

	instance.toggleOverlayView = function(showOverlay){
		_toggeleOverlayView(showOverlay);		
	};
	
	instance.toggleBlankLayoutNotice = function(showNotice){
		_toggleBlankLayoutNotice(showNotice);		
	};

	return instance;
}(jQuery,window));