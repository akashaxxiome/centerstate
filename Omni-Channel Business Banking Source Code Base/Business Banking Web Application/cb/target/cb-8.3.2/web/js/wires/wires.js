ns.wire.isChangeBeneScope = false;
ns.wire.showScopeWarning = false;

/*if(!ns.wire.uiroller){
	ns.wire.uiroller = new ns.common.UIRoller();
	ns.wire.uiroller.addComponent("beneficiaryLink");
	ns.wire.uiroller.addComponent("createBeneficiaryLink");
	ns.wire.uiroller.addComponent("customMappingDetails");
	ns.wire.uiroller.addComponent("customMappingsLink");
	ns.wire.uiroller.addComponent("addCustomMappingsLink");
	ns.wire.uiroller.addComponent("details");
	ns.wire.uiroller.addComponent("FileImportEntryLink");
	ns.wire.uiroller.addComponent("fileupload");
	ns.wire.uiroller.addComponent("fileuploadPortlet", {
		"openCallback": function(compId){
							$('#'+compId).portlet("show");
						},
		"closeCallback": function(compId){
							$('#'+compId).portlet("hide");
						},
		"isOpenCallback": function(compId){
			if($('#'+compId).data("ui-portlet") != undefined){
				return $('#'+compId).portlet("isShown");
			}			
		}
	});
	ns.wire.uiroller.addComponent("mappingDiv");
	ns.wire.uiroller.addComponent("newWireTemplateCriteria");
	ns.wire.uiroller.addComponent("newWireTransferCriteria");
	ns.wire.uiroller.addComponent("quickNewWireTemplateLink");
	ns.wire.uiroller.addComponent("quickNewWireTransferLink");
	ns.wire.uiroller.addComponent("quicksearchcriteria");
	ns.wire.uiroller.addComponent("quickSearchLink");
	ns.wire.uiroller.addComponent("quicksearchReleaseWireCriteria");
	ns.wire.uiroller.addComponent("quicksearchReleaseWireLink");
	ns.wire.uiroller.addComponent("quicksearchTemplatecriteria");
	ns.wire.uiroller.addComponent("quickSearchTemplateLink");
	ns.wire.uiroller.addComponent("releaseWiresLink");
	ns.wire.uiroller.addComponent("summary");
	ns.wire.uiroller.addComponent("templatesLink");
	ns.wire.uiroller.addComponent("wiresLink");
	ns.wire.uiroller.addComponent("wiresReleaseConfirmDiv");
	ns.wire.uiroller.addComponent("wiresReleaseDiv");
	ns.wire.uiroller.addComponent("newSingleWiresTransferID");
	ns.wire.uiroller.addComponent("newBatchWiresTransferID");
	ns.wire.uiroller.addComponent("newSingleWiresTemplateID");

	ns.wire.uimode1 = new ns.common.UIMode("wiresummary");
	ns.wire.uimode1.addComponent("quickNewWireTransferLink");
	ns.wire.uimode1.addComponent("newWireTransferCriteria");
	ns.wire.uimode1.addComponent("templatesLink");
	ns.wire.uimode1.addComponent("beneficiaryLink");
	ns.wire.uimode1.addComponent("quickSearchLink");
	ns.wire.uimode1.addComponent("releaseWiresLink");
	ns.wire.uimode1.addComponent("summary");
	ns.wire.uimode1.addComponent("FileImportEntryLink");
	ns.wire.uimode1.addComponent("newSingleWiresTransferID");
	ns.wire.uimode1.addComponent("newBatchWiresTransferID");

	ns.wire.uimode2 = new ns.common.UIMode("templatesummary");
	ns.wire.uimode2.addComponent("quickNewWireTemplateLink");
	ns.wire.uimode2.addComponent("newWireTemplateCriteria");
	ns.wire.uimode2.addComponent("wiresLink");
	ns.wire.uimode2.addComponent("beneficiaryLink");
	ns.wire.uimode2.addComponent("quickSearchTemplateLink");
	ns.wire.uimode2.addComponent("summary");
	ns.wire.uimode2.addComponent("FileImportEntryLink");
	ns.wire.uimode2.addComponent("releaseWiresLink");
	ns.wire.uimode2.addComponent("newSingleWiresTemplateID");

	ns.wire.uimode3 = new ns.common.UIMode("wirebeneficiary");
	ns.wire.uimode3.addComponent("createBeneficiaryLink");
	ns.wire.uimode3.addComponent("wiresLink");
	ns.wire.uimode3.addComponent("templatesLink");
	ns.wire.uimode3.addComponent("summary");
	ns.wire.uimode3.addComponent("FileImportEntryLink");
	ns.wire.uimode3.addComponent("releaseWiresLink");

	ns.wire.uimode4 = new ns.common.UIMode("releasewire");
	ns.wire.uimode4.addComponent("wiresLink");
	ns.wire.uimode4.addComponent("templatesLink");
	ns.wire.uimode4.addComponent("beneficiaryLink");
	ns.wire.uimode4.addComponent("wiresReleaseDiv");
	ns.wire.uimode4.addComponent("FileImportEntryLink");
	ns.wire.uimode4.addComponent("quicksearchReleaseWireLink");

	ns.wire.uimode5 = new ns.common.UIMode("fileimport");
	ns.wire.uimode5.addComponent("fileupload");
	ns.wire.uimode5.addComponent("fileuploadPortlet");
	ns.wire.uimode5.addComponent("customMappingsLink");
	ns.wire.uimode5.addComponent("wiresLink");
	ns.wire.uimode5.addComponent("templatesLink");
	ns.wire.uimode5.addComponent("beneficiaryLink");
	ns.wire.uimode5.addComponent("releaseWiresLink");


	ns.wire.uimode6 = ns.wire.uimode1.copyTo("singlewire");
	ns.wire.uimode6.addComponent("details");
	//ns.wire.uimode6.removeComponent("summary");
	ns.wire.uimode6.addComponent("popupcriteria");
	ns.wire.uimode6.addComponent("newWireTransferCriteria");

	ns.wire.uimode6.setOnComplete(function(){
		$('#newSingleWiresTransferID').click();
	});

	ns.wire.uiroller.addMode(ns.wire.uimode1);
	ns.wire.uiroller.addMode(ns.wire.uimode2);
	ns.wire.uiroller.addMode(ns.wire.uimode3);
	ns.wire.uiroller.addMode(ns.wire.uimode4);
	ns.wire.uiroller.addMode(ns.wire.uimode5);
	ns.wire.uiroller.addMode(ns.wire.uimode6);
}
*/

ns.wire.setupWires = function(){
    /*
	 * Control dashboard button display
	 */
    $.subscribe('wireTemplatesSummaryLoadedSuccess', function(event,data){
        ns.wire.gotoTemplates();
    });

    $.subscribe('wiresSummaryLoadedSuccess', function(event,data){
        ns.wire.gotoWires();
    });

    $.subscribe('beneficicariesSummaryLoadedSuccess', function(event,data){
        ns.wire.gotoBeneficiaries();
    });

    //Loading the form of wire type choosing.
    $.subscribe('beforeLoadWiresTransferForm', function(event,data) {
        $.log('About to load form! ');
        $('#details').slideUp();
        $('#popUpcriteria').hide();
        ns.common.selectDashboardItem("newSingleWiresTransferID");
        if($('#wiresCalendar').length > 0){
			$('#summary').empty();
		}
    });
    $.subscribe('beforeLoadWireBatchForm', function(event,data) {
        ns.common.selectDashboardItem("newBatchWiresTransferID");
        if($('#wiresCalendar').length > 0){
			$('#summary').empty();
		}
    });

    $.subscribe('loadWiresTransferFormComplete', function(event,data) {
        $.log('Form Loaded! ');
        $('#wiresGridTabs').portlet('fold');
        //set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
        var heading = $('#PageHeading').html();
        var $title = $('#details .portlet-title');
        $title.html(heading);

        ns.wire.gotoStep(1);
        $('#details').slideDown();
    });

    $.subscribe('errorLoadWiresTransferForm', function(event,data) {
        $.log('Form Loading error! ');
    });

    $.subscribe('beforeLoadBeneficiaryForm', function(event,data) {
        $.log('About to load form! ');
        $('#details').slideUp();
        $('#popUpcriteria').hide();
        $("#summary").hide();
        ns.common.refreshDashboard('dbPayeeSummary');
        ns.common.selectDashboardItem("createBeneficiary");
        ns.common.selectDashboardItem("createBeneficiaryLink");
    });
    
    $.subscribe('loadBeneficiaryFormComplete', function(event,data) {
        $.log('Form Loaded! ');
        $('#beneficiariesGridTabs').portlet('fold');
        $('#daBeneficiariesGridTabs').portlet('fold');
        //set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
        var heading = $('#PageHeading').html();
        var $title = $('#details .portlet-title');
        $title.html(heading);

        ns.wire.gotoStep(1);
        $('#details').slideDown();

        $.publish("common.topics.tabifyDesktop");
        if(accessibility){
        	$("#details").setInitialFocus();
        }
    });

    $.subscribe('errorLoadBeneficiaryForm', function(event,data) {
        $.log('Form Loading error! ');
    });
    $.subscribe('backToInput', function(event,data) {
        ns.wire.gotoStep(1);
        ns.common.hideStatus();//In case there is message shown for successful Template save.
    });

    //Loading the form of new wire transfer

    $.subscribe('selectWiresTypeCompleteTopic', function(event,data) {
        $.log('Form Loaded! ');
       // $('#wiresGridTabs').portlet('fold');
        //$('#wireTemplateGridTabs').portlet('fold');
        //set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
        var heading = $('#PageHeading').html();
        var $title = $('#details .portlet-title');
        $title.html(heading);

        ns.wire.gotoStep(1);
        $('#details').slideDown();
		ns.wire.showScopeWarning = false;

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#details").setInitialFocus();
		}
    });

 	$.subscribe('clearWireTemplateQuickSearchTopic', function(event,data) {
        $.log('Clear quick search criteria! ');
        $('#popupcriteria').show();
		$('#quicksearchReleaseWireCriteria').hide();
		$('#quicksearchTemplatecriteria').toggle();

		$("#showTemplateTypeID").selectmenu('value',"-1");
		$("#searchAcctID").selectmenu('value',"");
		$("#searchTemplateNameID").val("");
		$("#searchBeneficiaryNameID").val("");
		$("#searchBeneficiaryAccountNumberID").val("");
		$("#searchBeneficiaryRoutingNumberID").val("");
    });

    $.subscribe('clickNewWireTemplateTopic', function(event,data) {
    	$.log('Clear quick search criteria! ');
		$('#popupcriteria').show();
		$('#quicksearchReleaseWireCriteria').hide();
		$('#quicksearchTemplatecriteria').attr("style","display:none");
		$("#showTemplateTypeID").selectmenu().selectmenu('value',"-1");
		$("#searchAcctID").selectmenu().selectmenu('value',"");
		$("#searchTemplateNameID").val("");
		$("#searchBeneficiaryNameID").val("");
		$("#searchBeneficiaryAccountNumberID").val("");
		$("#searchBeneficiaryRoutingNumberID").val("");		
		ns.common.refreshDashboard('dbTemplateSummary');
		ns.common.selectDashboardItem("newSingleWiresTemplateID");
    });

	/**
	 * Verify wire transfer form start..., if validation passed, go to step 3
	 */
    $.subscribe('beforeVerifyWireTransferForm', function(event,data) {
        removeValidationErrors();
    });

    $.subscribe('verifyWireTransferFormComplete', function(event,data) {
		ns.wire.gotoStep(2);
    });

    $.subscribe('errorVerifyWireTransferForm', function(event,data) {
        $.log('Form Verifying error! ');
    });

    $.subscribe('successVerifyWireTransferForm', function(event,data) {
        $.log('Form Verifying success! ');
    });
    /**
	 * Verify wire transfer form end.
	 */
    /**
	 * Send wire transfer start.
	 */
    $.subscribe('beforeSendWireTransferForm', function(event,data) {
        removeValidationErrors();
    });

    $.subscribe('sendWireTransferFormSuccess', function(event,data) {
    	ns.common.showStatus($('#wireTransferResultMessage').html());
        ns.wire.gotoStep(3);
        $.publish("saveAsTemplateSubmitOnClickTopics");
    });

	/**
	* Reload template grid.
	*/
	$.subscribe('cancelWireTransferFormClickTopics', function(event,data) {
	 //  ns.wire.reloadWiresTemplatesGrids();
	   //ns.common.refreshDashboard('dbWireSummary');
	});

    /**
	 * Send wire transfer end.
	 */
    $.subscribe('cancelWireTransferForm', function(event,data) {
    	ns.wire.closeWireTransferTabs();
        ns.common.hideStatus();
		$("#summary").show();
		if(ns.wire.enableOnChange){
			if(!ns.wire.isTemplate){
				//ns.wire.uiroller.gotoMode("wiresummary");
				ns.wire.resetWireTypeSelectMenu();				
			} else {
				//ns.wire.uiroller.gotoMode("templatesummary");
				ns.wire.resetWireTemplateTypeSelectMenu();
			}
		}
		if($("#isManagePayeeModified").val() == 'true') {
			$('#pendingApprovalWiresGridID').trigger("reloadGrid");
		}
		ns.wire.enableOnChange = false;
		// ns.common.refreshDashboard('showdbWireSummary', true);
    });
    /**
	 * Send wire transfer end to refresh grid datas.
	 */
    $.subscribe('sendWireTransferFormComplete', function(event,data) {    	
    	/*var selectedTab = $("#wiresGridTabs").tabs("option","active");    	
    	if(selectedTab == 0){
			$('#pendingApprovalWiresGridID').trigger("reloadGrid");
		}
		if(selectedTab == 1){
			$('#pendingWiresGridID').trigger("reloadGrid");			
		}
		if(selectedTab == 2){
			$('#completedWiresGridID').trigger("reloadGrid");
		}*/
    	ns.common.setFirstGridPage('#pendingApprovalWiresGridID');
    	$('#pendingApprovalWiresGridID').trigger("reloadGrid");
    	ns.common.setFirstGridPage('#pendingWiresGridID');
    	$('#pendingWiresGridID').trigger("reloadGrid");	
    	ns.common.setFirstGridPage('#completedWiresGridID');
    	$('#completedWiresGridID').trigger("reloadGrid");
    });

    //Insert the selected destination bank, used by "wirebanklistitem.jsp"
    $.subscribe('insertSelectedBankFormClikTopics', function(event,data) {

    	var formId="#newWiresTransferFormID";
        var targetURL = ns.wire.getTargetURL( $("#wireDestinationIDForJS").val() );
        var action = $("#bankLookupRaturnPage").val();
        targetURL =  "/cb/pages/jsp/wires/"+action;

        var isBeneficiary = $("#isBeneficiary").val();
		var isPending = $("#IsPending").val();

		if (isPending == "undefined" || isPending == null) {
			isPending = "";
		}

		var wireType = $("#selectPayeeDestinationID").val();
		if(isBeneficiary =="true" || isPending == "true"){
            formId="#newBeneficiaryFormID";
        }
        $.ajax({
            type: "POST",
            url: "/cb/pages/jsp/wires/getWireTransferBanksAction_insertBankInfo.action",
            data: $(formId).serialize(),
            success: function(data) {
                $.ajax({
                    type: "POST",
                    url: targetURL,
                    success: function(data) {
                    	 $('#inputDiv').html(data);

                        $.publish("common.topics.tabifyDesktop");
                    	if(accessibility){
                    		$("#inputDiv").setFocus();
                    	}
                    	//below command to open the old form
                    	$('#expand').trigger('click');
                    	// below command to show/hide the expand collapse button
                    	$('#expand').toggle();
                    	// manage not required as free form
                    	$('#manageBeneficiaryButtonID').toggle();
                    	// below hides the expand area
                    	$('#manageBeneficiaryButtonIDHolder').parents("table:first").hide();
                    	
                    	if($(".selecteBeneficiaryMessage div#freeFormBeneLoaded").html()!=' '){
                    		$(".selecteBeneficiaryMessage").html('');
                    		$(".selecteBeneficiaryMessage").html("<div class='sectionhead nameTitle' id='freeFormBeneLoaded' style='cursor:auto'>Beneficiary Info</div> ");
                    		$('#expand').hide();
                    		$('#collapse').hide();
                    	}
                    }
                });
                ns.common.closeDialog("#searchDestinationBankDialogID");
            }
        });

    });

    $.subscribe('closeSelectedBankFormClikTopics', function(event,data) {
    	ns.common.closeDialog("#searchDestinationBankDialogID");
    });

    //Close the search bank window, used by wirebanklistselect.jsp.
    $.subscribe('closeSerachBankListOnClickTopics', function(event,data) {
    	ns.common.closeDialog("#searchDestinationBankDialogID");
    });

    //Research bank list, used by wirebanklistselect.jsp.
    $.subscribe('researchBankListOnClickTopics', function(event,data) {
		//QTS 679740
        $.ajax({
            type: "POST",
            //url: "/cb/pages/jsp/wires/wirebanklist.jsp?searchAgain=true",
			url: "/cb/pages/jsp/wires/getWireTransferBanksAction_init.action",
			data: $("#researchWireBankListFormID").serialize(),
            //data: {searchAgain: "true", CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
            success: function(data) {
                $('#searchDestinationBankDialogID').html(data);
            }
        });
    });

  //Manual entry wire bank, used by wirebanklist.jsp
    $.subscribe('manualEntryOfWireBankOnClickTopics', function(event,data) {

    	var action = $("#bankLookupRaturnPage").val();
        targetURL =  "/cb/pages/jsp/wires/"+action;

        $.ajax({
            type: "POST",
            url: "/cb/pages/jsp/wires/getWireTransferBanksAction_manualEntryBank.action",
            data: $("#BankFormID").serialize(),
            success: function(data) {
			$.ajax({
                    type: "POST",
                    url: targetURL,
                    success: function(data) {
                        $('#inputDiv').html(data);
                        $.publish("common.topics.tabifyDesktop");
                    	if(accessibility){
                    		$("#inputDiv").setFocus();
                    	}
                    	//below command to open the old form
                    	$('#expand').trigger('click');
                    	// below command to show/hide the expand collapse button
                    	$('#expand').toggle();
                    	// manage not required as free form
                    	$('#manageBeneficiaryButtonID').toggle();
                    	// below hides the expand area
                    	$('#manageBeneficiaryButtonIDHolder').parents("table:first").hide();
                    	
                    	if($(".selecteBeneficiaryMessage div#freeFormBeneLoaded").html()!=' '){
                    		$(".selecteBeneficiaryMessage").html('');
                    		$(".selecteBeneficiaryMessage").html("<div class='sectionhead nameTitle' id='freeFormBeneLoaded' style='cursor:auto'>Beneficiary Info</div> ");
                    		$('#expand').hide();
                    		$('#collapse').hide();
                    	}
                    }
                });
                ns.common.closeDialog("#searchDestinationBankDialogID");
            }
        });
    });


    $.subscribe('closeWireImportAddenda', function(event,data) {
        $('#importFED_CTPAddendaDialogID').dialog('close');
    });

    $.subscribe('beforeWireImportAddendaForm', function(event,data) {
        //$.log('Form Loading error! ');
        // clear everything from the VIEW Dialog ID to not cause conflicting DIV names

    });
    $.subscribe('errorWireImportAddendaForm', function(event,data) {
        //$.log('Form Loading error! ');
    });
    $.subscribe('successWireImportAddendaForm', function(event,data) {
// submit should have happened.
        $('#importFED_CTPAddendaDialogID').dialog('close');

        var urlString = "/cb/pages/jsp/wires/FedWireImportAddendaAction_executeAddendaString.action";
        $.ajax ({
            url: urlString,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            async: false,
            success: function(data) {
                $(".WireAddenda").val(data.addendaString);
            },
            error: function(msg) {
            }
        });
    });



}

ns.wire.getTargetURL = function(dest){
	  var targetURL = "/cb/pages/jsp/wires/wiretransfernew.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      if (dest == "DOMESTIC")	{
          targetURL = "/cb/pages/jsp/wires/wiretransfernew.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      } else if ( dest == "INTERNATIONAL" ){
          targetURL = "/cb/pages/jsp/wires/wiretransfernew.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      } else if ( dest == "HOST" ){
          targetURL = "/cb/pages/jsp/wires/wirehost.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      } else if ( dest == "DRAWDOWN" ){
          targetURL = "/cb/pages/jsp/wires/wiredrawdown.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      } else if ( dest == "FEDWIRE" ){
          targetURL = "/cb/pages/jsp/wires/wirefed.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      } else if ( dest == "BOOKTRANSFER" ){
          targetURL = "/cb/pages/jsp/wires/wirebook.jsp?DontInitialize=true&CSRF_TOKEN="+$("#TEMP_CSRF_TOKEN").val();
      }
        return targetURL;
    }

ns.wire.gotoStep = function(stepnum){
	
	ns.common.gotoWizardStep('detailsPortlet',stepnum);
	
    var tabindex = stepnum - 1;
    $('#TransactionWizardTabs').tabs( 'enable', tabindex );
    $('#TransactionWizardTabs').tabs( 'option','active', tabindex );
    $('#TransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
    $('#TransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );
    $('#TransactionWizardTabs').tabs( 'disable', (tabindex+3)%3 );

    $.publish("common.topics.tabifyDesktop");
    if(accessibility){
    	$("#TransactionWizardTabs").setFocusOnTab();
    }
}

/**
 * Close Details island and expand Summary island
 */
ns.wire.closeWireTransferTabs = function(){
    $('#wiresGridTabs').portlet('expand');
    $('#wireTemplateGridTabs').portlet('expand');
    $('#beneficiariesGridTabs').portlet('expand');
    $('#daBeneficiariesGridTabs').portlet('expand');
    $('#details').slideUp();
}

/*
//Submit the form again and for setting the payee selected.
ns.wire.submmitWireTransferForm = function(urlString, preProcessType){
    $.ajax({
        type: "POST",
        url: "/cb/pages/jsp/wires/addWireTransferAction_" + preProcessType + ".action",
        data: $("#newWiresTransferFormID").serialize(),
        success: function(data) {
            //$('#inputDiv').html(data);
            $.ajax({
                url: urlString,
                success: function(data){
                    $("#inputDiv").html(data);
                }
            });
        }
    });
}
*/

ns.wire.searchBeneficiaryDestinationBankForm = function(urlString,actionString){
    $.ajax({
        type: "POST",
        url: actionString,
        data: $("#newBeneficiaryFormID").serialize(),
        success: function(data) {
           /* $.ajax({
                type: "POST",
                url: urlString,
                data: $("#newBeneficiaryFormID").serialize(),
                success: function(data) {*/
                    $('#searchDestinationBankDialogID').html(data).dialog('open');
              /*  }
            });*/
        }
    });
}

ns.wire.searchDestinationBankForm = function(urlString, actionString){
	/*
    var formId="#newWiresTransferFormID";
    var isBeneficiary=$("#isBeneficiary").val();
    if(actionString==undefined){
    	$.ajax({
            url: urlString,
            success: function(data) {
                $('#searchDestinationBankDialogID').html(data).dialog('open');
            }
        });
    }else{
	    if(isBeneficiary=="true"){
	        formId="#newBeneficiaryFormID";
	    }
	    $.ajax({
	        type: "POST",
	        url: actionString,
	        data: $(formId).serialize(),
	        success: function(data) {
	            $.ajax({
	                url: urlString,
	                success: function(data) {
	                    $('#searchDestinationBankDialogID').html(data).dialog('open');
	                }
	            });
	        }
	    });
    }*/

	$.ajax({
		type: "POST",
        url: urlString,
        data: $("#newWiresTransferFormID").serialize(),
        success: function(data) {
            $('#searchDestinationBankDialogID').html(data).dialog('open');
        }
    });

}

ns.wire.gotoTemplates = function(){
	ns.common.refreshDashboard('dbTemplateSummary');
	$("#summary").show();
	$("#mappingDiv").hide();
	$("#fileupload").hide();
	$("#customMappingDetails").hide();
	$('#wiresReleaseDiv').hide();
	
	//ns.wire.uiroller.gotoMode("templatesummary");
	/*
	$('#quickNewWireTemplateLink').removeAttr('style');
    $('#quickNewWireTransferLink').attr('style','display:none');
    $('#createBeneficiaryLink').attr('style','display:none');

    $('#wiresLink').removeAttr('style');
    $('#templatesLink').attr('style','display:none');
    $('#beneficiaryLink').removeAttr('style');

    $('#details').hide();
    $('#quickSearchLink').hide();
    $('#quickSearchTemplateLink').removeAttr('style');
    $('#quicksearchcriteria').hide();
    $('#wiresReleaseDiv').hide();
    $('#wiresReleaseConfirmDiv').hide();
    $('#summary').show();
    $('#fileupload').hide();
	$('#customMappingsLink').hide();
	$('#FileImportEntryLink').show();
    $('#quicksearchReleaseWireLink').attr('style','display:none');
    $('#quicksearchReleaseWireCriteria').hide();
    $('#releaseWiresLink').show();
    */
}

ns.wire.gotoBeneficiaries = function(){
	ns.common.refreshDashboard('dbPayeeSummary');
	$("#summary").show();
	$("#mappingDiv").hide();
	$("#fileupload").hide();
	$("#customMappingDetails").hide();
	$('#wiresReleaseDiv').hide();
	
	//ns.wire.uiroller.gotoMode("wirebeneficiary");
	/*
    $('#quickNewWireTransferLink').attr('style','display:none');
    $('#quickNewWireTemplateLink').attr('style','display:none');
    $('#createBeneficiaryLink').removeAttr('style');

    $('#wiresLink').removeAttr('style');
    $('#templatesLink').removeAttr('style');
    $('#beneficiaryLink').attr('style','display:none');

    $('#details').hide();
    $('#quickSearchLink').hide();
    $('#quickSearchTemplateLink').attr('style','display:none');
    $('#quicksearchcriteria').hide();
    $('#wiresReleaseDiv').hide();
    $('#wiresReleaseConfirmDiv').hide();
    $('#summary').show();
    $('#fileupload').hide();
	$('#customMappingsLink').hide();
	$('#FileImportEntryLink').show();
	$('#quicksearchReleaseWireLink').attr('style','display:none');
    $('#quicksearchReleaseWireCriteria').hide();
    $('#releaseWiresLink').show();
    */
}

//Fold Wire pay tabs
$.subscribe('closeWiresTabs', function(event,data) {
	$.log('Fold wires tabs! ');
	if ($('#cancelFormButtonOnInput').length > 0) {
		$('#cancelFormButtonOnInput').click();	
	}
	if ($('#cancelMultiFormButton').length > 0) {
		$('#cancelMultiFormButton').click();	
	}		
});

$.subscribe('calendarLoadedSuccess', function(event,data){
	//$('#goBackSummaryLink').show();
	//$('#goBackSummaryLink').attr('style','display:inline-block');
});

function hideCalendar(){
	if ($("#wiresCalendar").length > 0) {
		/*$('#goBackSummaryLink').click();*/
		/*$("#summary").hide();*/
	}	
}

ns.wire.gotoWires = function(){
	//ns.wire.uiroller.gotoMode("wiresummary");
	ns.common.refreshDashboard('dbWireSummary');
	$("#summary").show();
	$("#mappingDiv").hide();
	$("#fileupload").hide();
	$("#customMappingDetails").hide();
	$('#wiresReleaseDiv').hide();
	
	//$('#goBackSummaryLink').attr('style','display:none');
	
	/*
	$('#quickNewWireTemplateLink').attr('style','display:none');
    $('#quickNewWireTransferLink').removeAttr('style');
    $('#createBeneficiaryLink').attr('style','display:none');

    $('#templatesLink').removeAttr('style');
    $('#wiresLink').attr('style','display:none');
    $('#beneficiaryLink').removeAttr('style');
    $('#wiresReleaseDiv').hide();
    $('#wiresReleaseConfirmDiv').hide();
    $('#details').hide();

    $('#quickSearchLink').show();
    $('#quickSearchTemplateLink').attr('style','display:none');
    $('#quicksearchReleaseWireLink').attr('style','display:none');
    $('#quicksearchReleaseWireCriteria').hide();
    $('#releaseWiresLink').show();
    $('#summary').show();
    $('#quicksearchTemplatecriteria').hide();
    $('#customMappingDetails').hide();
    $('#mappingDiv').hide();
    $('#fileupload').hide();
	$('#customMappingsLink').hide();
	$('#FileImportEntryLink').show();
	*/
}

ns.wire.getAccountsOnVerifyPage = function(urlString){
    $.ajax({
        type: "POST",
        url: urlString,
        data: $("#WireTransferNewConfirmFormID").serialize(),
        success: function(data) {
            $('#verifyDiv').html(data)
        }
    });
}

ns.wire.viewDisplayAmount = function(action){
	var _url ="";
	var hideLink="";
	if(action=="add"){
		_url = "/cb/pages/jsp/wires/addWireTransferAction_getWireAmounts.action";
		hideLink = "#viewUSDAmountLink_addwire";
	} else {
		_url = "/cb/pages/jsp/wires/modifyWireTransferAction_getWireAmounts.action";
		hideLink = "#viewUSDAmountLink_editwire";
	}
    $.ajax({
        type: "GET",
        url: _url,
        success: function(data) {
        	if(data) {
        		$("#viewUSDAmountSpan").text(data.displayAmount);
        		$(hideLink).hide();
        	}
        }
    });
}

ns.wire.viewDisplayPaymentAmount = function(action){
	var _url ="";
	var hideLink="";
	if(action=="add"){
		_url = "/cb/pages/jsp/wires/addWireTransferAction_getWireAmounts.action";
		hideLink = "#viewPaymentAmountLink_addwire";
	} else {
		_url = "/cb/pages/jsp/wires/modifyWireTransferAction_getWireAmounts.action";
		hideLink = "#viewPaymentAmountLink_editwire";
	}
    $.ajax({
        type: "GET",
        url: _url,
        success: function(data) {
        	if(data) {
        		$("#viewPaymentAmountSpan").text(data.displayPaymentAmount);
        		$(hideLink).hide();
        		$("#viewPaymentAmountCurrencySpan").show();
        	}
        }
    });
}

$.subscribe('sendWireTemplateFormClick', function(event,data) {
    var url_action=$("#url_action").val();
    var url="/cb/pages/jsp/wires/" + url_action;
    $.ajax({
        type: "POST",
        url: url,
        data: $("#WireTransferNewConfirmFormID").serialize(),
        success: function(data){
            if(url_action=="addWireTemplateAction_autoEntitle.action" ||
               url_action=="modifyWireTemplateAction_autoEntitle.action" ){
				$('#wiresAutoEntitleConfirmDiv').html(data);
                $('#autoEntitleConfirmDialogID').dialog('open');
		    }
            if(url_action=="addWireTemplateAction.action" ||
               url_action=="modifyWireTemplateAction.action" ){
                $("#confirmDiv").html(data);
                ns.common.showStatus($('#wireTransferResultMessage').html());
                ns.wire.gotoStep(3);
                $('#saveAsTemplateHolder').hide();
               // ns.wire.reloadWiresTemplatesGrids();
            }
        }
    });
});

$.subscribe('saveAsTemplateComplete', function(event, data) {
	var url_action=$("#url_template_action").val();
	ns.common.closeDialog('#wireSaveAsTemplateDialogID');
	if((event.originalEvent.status ==='success') && (url_action=="addWireTransferAction_autoEntitle.action" ||
	     	   url_action=="modifyWireTransferAction_autoEntitle.action") ){
		//$('#wiresAutoEntitleConfirmDiv').html(data);
		$('#autoEntitleConfirmDialogID').dialog('open');
	}

});

$.subscribe('saveAsTemplateOnClick', function(event,data) {
    var url_action=$("#url_action").val();
    var url="/cb/pages/jsp/wires/" + url_action;
    $.ajax({
        type: "POST",
        url: url,
        data: $("#saveAsTemplateFormID").serialize(),
        success: function(data){
        	
        	ns.common.closeDialog('#wireSaveAsTemplateDialogID');
        	
        	if(url_action=="addWireTransferAction_autoEntitle.action" ||
        	   url_action=="modifyWireTransferAction_autoEntitle.action" ){
				$('#wiresAutoEntitleConfirmDiv').html(data);
                $('#autoEntitleConfirmDialogID').dialog('open');
		    } 
        	
        }
    });
});

/**
	 * Send autoentitle confrim.
	 */
$.subscribe('beforeSaveWireTemplateFormWithAutoEntitle', function(event,data) {
    removeValidationErrors();
});

$.subscribe('sendSaveWireTemplateFormWithAutoEntitleSuccess', function(event,data) {
	var url_action = $("#url_action").val();
	//If this is a saveAsTemplate, do nothing.
    if(url_action=="addWireTransferAction_saveAsTemplate.action" ||
       url_action=="modifyWireTransferAction_saveAsTemplate.action"	){
    	//Do Nothing
    	ns.common.showStatus($('#wireTransferResultMessage').html());
    } else {
        //If this is a new template, go to step 3.
    	$('#saveAsTemplateHolder').hide();
    	ns.common.showStatus($('#wireTransferResultMessage').html());
        ns.wire.gotoStep(3);
       // ns.wire.reloadWiresTemplatesGrids();
    }

});

$.subscribe('sendSaveWireTemplateFormWithAutoEntitleError', function(event,data) {
	var url_action = $("#url_action").val();
	//If this is a saveAsTemplate, do nothing.
    if(url_action=="addWireTransferAction_saveAsTemplate.action" ||
       url_action=="modifyWireTransferAction_saveAsTemplate.action"	){
    	//Do Nothing
    	var errorMessage = event.originalEvent.request.responseJSON.response.data;
    	ns.common.showError(errorMessage);
    }

});


$.subscribe('sendSaveWireTemplateFormWithAutoEntitleComplete', function(event,data) {
	ns.common.closeDialog('#autoEntitleConfirmDialogID');
	$('#wiresAutoEntitleConfirmDiv').empty();
});
$.subscribe('cancelFormOnAutoEntitle', function(event,data) {
	ns.common.closeDialog('#autoEntitleConfirmDialogID');
	$('#wiresAutoEntitleConfirmDiv').empty();
});

$.subscribe('sendAutoEntileFormComplete', function(event,data) {

    });

$.subscribe('saveAsTemplateSubmitOnClickTopics', function(event,data) {
	var initSaveAsTemplateAction=$("#initSaveAsTemplateAction").val();
	var url="/cb/pages/jsp/wires/" + initSaveAsTemplateAction;
    $.ajax({
        type: "POST",
        url: url,
        data: $("#WireTransferNewConfirmFormID").serialize(),
        success: function(data){
            $('#templateSaveAsID').html(data);
        	/*$('#wireSaveAsTemplateDialogID').html(data).dialog('open');*/
        }
    });
});

$.subscribe('saveAsTemplateSuccessTopics', function(event,data) {
	var url_action = $("#url_action").val();
	ns.common.closeDialog('#wireSaveAsTemplateDialogID');
	if(url_action=="addWireTransferAction_autoEntitle.action" ||
       url_action=="modifyWireTransferAction_autoEntitle.action" ){
   		$('#autoEntitleConfirmDialogID').dialog('open');
	}else{
		ns.common.showStatus($('#wireTemplateResultMessage').html());
	}
});

$.subscribe('saveAsTemplateDASuccessTopics', function(event,data) {
		ns.common.showStatus($('#wireTemplateResultMessage').html());
});

/**
* Verify wire Beneficiary form start..., if validation passed, go to step 3
*/
$.subscribe('beforeVerifyBeneficiaryForm', function(event,data) {
    removeValidationErrors();
});

$.subscribe('verifyBeneficiaryFormComplete', function(event,data) {
    ns.wire.gotoStep(2);
});

$.subscribe('errorVerifyBeneficiaryForm', function(event,data) {
    $.log('Form Verifying error! ');

});

$.subscribe('successVerifyBeneficiaryForm', function(event,data) {
    $.log('Form Verifying success! ');
});

/**
* Send wire beneficiary start.
*/
$.subscribe('beforeSendBeneficiaryForm', function(event,data) {
    removeValidationErrors();
});

$.subscribe('sendBeneficiaryFormSuccess', function(event,data) {
	ns.common.showStatus($('#wireBeneficiaryResultMessage').html());
    ns.wire.gotoStep(3);
});

/**
	 * Send wire Beneficiary end to refresh grid datas.
	 */
$.subscribe('sendBeneficiaryFormComplete', function(event,data) {
    $('#wireBeneficiariesGridID').trigger("reloadGrid");
});

$.subscribe('errorSendWireTransferForm', function(event,data) {
    $.log('wire transfer error! ');

    var resStatus = event.originalEvent.request.status;
    if(resStatus === 700) //700 indicates interval validation error
        ns.common.openDialog("authenticationInfo");

});

//Submit the form again and for setting the PayeeDestination selected.
ns.wire.submmitBeneficiaryForm = function(urlString){
    $.ajax({
        type: "POST",
        url: urlString,
        data: $("#newBeneficiaryFormID").serialize(),
        success: function(data) {
            $("#inputDiv").html(data);
        }
    });
}

$.subscribe('beforeBeneficiaryOnClickTopics', function(event,data) {
    $.log('About to manage beneficiary! ');
    $("#summary").show();
	$('#operationresult').hide();
    $('#popupcriteria').hide();
    $('#details').hide();
});

$.subscribe('beforeWiresSummaryOnClickTopics', function(event,data) {
    $.log('About to manage wire! ');
	$('#operationresult').hide();
    $('#popupcriteria').hide();
	ns.wire.resetWireTypeSelectMenu();
	 $('#details').hide();
	});

$.subscribe('beforeWireTemplatesOnClickTopics', function(event,data) {
    $.log('About to manage template! ');
	$('#operationresult').hide();
    $('#popupcriteria').hide();
	ns.wire.resetWireTemplateTypeSelectMenu();
	$('#details').hide();
});

$.subscribe('manageBeneficiaryClickTopic', function(event,data) {
    var url ="/cb/pages/jsp/wires/" + $("#urlIDForJS").val();
    $.ajax({
        type: "POST",
        url: url,
        data: $("#newWiresTransferFormID").serialize(),
        success: function(data) {
            $("#inputDiv").html(data);
        }
    });
});

$.subscribe('closeBeneficiary', function(event,data) {
	$('#viewWireBeneficiariesDialogID').dialog('close');
});

ns.wire.payeeSaveOnClick = function() {
	// Refresh pending grid
	// Synchronous Call
	$.ajaxSetup({
		async: false
    });
	ns.wire.retrunBeneficiaryToFromURL();
	var manageBeneficiary = $("#manageBeneficiary").val();
	var destination = $("#returnPage").val();
    if( destination == "" || manageBeneficiary =="" ){
    	ns.common.showSummaryOnSuccess('summary','done');
	} else {
		$("#isManagePayeeModified").val("true");
    	$("#summary").hide();
    	$('#wiresGridTabs').portlet('fold'); 	
        $('#wireTemplateGridTabs').portlet('fold');
    }

	// Asynchronous Call
	$.ajaxSetup({
		async: true
    });
};

ns.wire.cancelWireBeneficiaryFormOnClick = function() {
	// Synchronous Call
	$.ajaxSetup({
		async: false
    });
	ns.wire.retrunBeneficiaryToFromURL();
	$('#beneficiariesGridTabs').portlet('expand');
	$('#daBeneficiariesGridTabs').portlet('expand');
	/*
	// Refresh Pending beneficiary grid
	if ($('#daWiresLink').length > 0) {
		$('#daWiresLink').click();
	}*/

	// Refresh master beneficiary grid
	$('#wireBeneficiariesGridID').trigger("reloadGrid");

	// Asynchronous Call
	$.ajaxSetup({
		async: true
    });
};

ns.wire.showSummaryWhileManagingPayee = function(isManagingPayee) {
	if(isManagingPayee && isManagingPayee === 'true'){
		$("#summary").hide();
	}
};

ns.wire.retrunBeneficiaryToFromURL = function(){
	var manageBeneficiary = $("#manageBeneficiary").val();
	var destination = $("#returnPage").val();
    if( destination == "" || manageBeneficiary =="" ){
        ns.wire.closeWireTransferTabs();
        ns.common.hideStatus();
     //   ns.common.refreshDashboard('showdbPayeeSummary',true);
    	$("#summary").show();
    }else{

		urlString = "/cb/pages/jsp/wires/"+destination;


        $.ajax({
            url: urlString,
            success: function(data) {
                $("#inputDiv").html(data);
            }
        });
        ns.wire.gotoStep(1);
    }
}

/** Open the input div on the summary div */
ns.wire.openDetailsDiv = function(){
    $('#wiresGridTabs').portlet('fold');

    //set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
    var heading = $('#PageHeading').html();
    var $title = $('#details .portlet-title');
    $title.html(heading);

    ns.wire.gotoStep(1);
    $('#details').slideDown();
}

$.subscribe('addWireOnBatchCompleteTopic', function(event,data) {
	//$('#selectWirePayeeID').selectmenu("value", "");
	//$('#selectWirePayeeID').selectmenu("change");
	ns.wire.gotoStep(1);
	// below code for cr:765155 to hide to load template when free form clicked
	var isBatch = $('#newBatchWiresTransferID span.dashboardSelectedItem').size();
	if(isBatch>0){
		$('#loadWireTransferTemplateFormID').parent('div.leftPaneInnerBox').parent('div.leftPaneInnerWrapper').hide();
		$('#disbaledExpandBtn').hide();
	}
})

sendNewWireTransferOnBatchClick = function(urlString) {
	var isMangePayee = $('#isManagePayeeModified').val();
    $.ajax( {
        url : urlString,
        success : function(data) {
            $("#inputDiv").html(data);
            if($("#isManagePayeeModified").length > 0) {
            	$('#isManagePayeeModified').val(isMangePayee);
            }
        }
    });
    ns.wire.gotoStep(1);
};

$.subscribe('verifyWireBatchBeforeTopics', function(event,data) {
    removeValidationErrors();
});

$.subscribe('verifyWireBatchSuccessTopics', function(event,data) {
    ns.wire.gotoStep(2);
});

//Reload all wires templates grid.
ns.wire.reloadWiresTemplatesGrids = function(){
	//var selectedTab = $("#wireTemplateGridTabs").tabs("option","active");
	/*if(selectedTab == 0){
		 $('#wireTemplatesUserGridID').trigger("reloadGrid");
	}
	else if(selectedTab == 1){
		$('#wireTemplatesBusinessGridID').trigger("reloadGrid");
	}
	else if(selectedTab == 2){
		$('#wireTemplatesBankGridID').trigger("reloadGrid");
	} */
	
	ns.common.setFirstGridPage('#wireTemplatesUserGridID');
	ns.common.setFirstGridPage('#wireTemplatesBusinessGridID');
	ns.common.setFirstGridPage('#wireTemplatesBankGridID');
	$('#wireTemplatesUserGridID').trigger("reloadGrid");
	$('#wireTemplatesBusinessGridID').trigger("reloadGrid");
	$('#wireTemplatesBankGridID').trigger("reloadGrid");
};

$.subscribe('sendWireBacthFormBeforeTopics', function(event,data) {
    removeValidationErrors();
});

$.subscribe('sendWireBacthFormSuccessTopic', function(event,data) {
	ns.common.showStatus($('#wireBatchResultMessage').html());
    ns.wire.gotoStep(3);
});

$.subscribe('sendWireBacthFormCompleteTopic', function(event,data) {
	ns.common.setFirstGridPage('#pendingWiresGridID');
    $('#pendingWiresGridID').trigger("reloadGrid");
});

/**
* Send wire batch end.
*/
$.subscribe('cancelWireBatchForm', function(event,data) {
    ns.wire.closeWireTransferTabs();
    ns.common.hideStatus();
	$("#summary").show();
    $('#newSingleWiresTransferID').show();
	$('#newBatchWiresTransferID').show();
	showDashboard();
	if($("#isManagePayeeModified").val() == 'true') {
		$('#pendingApprovalWiresGridID').trigger("reloadGrid");
		$('#pendingWiresGridID').trigger("reloadGrid");	
	}

	/*var selectedTab = $("#wiresGridTabs").tabs("option","active");    	
	if(selectedTab == 0){
		$('#pendingApprovalWiresGridID').trigger("reloadGrid");
	}
	if(selectedTab == 1){
		$('#pendingWiresGridID').trigger("reloadGrid");			
	}
	if(selectedTab == 2){
		$('#completedWiresGridID').trigger("reloadGrid");
	}*/
	
	/*ns.common.setFirstGridPage('#pendingApprovalWiresGridID');
	$('#pendingApprovalWiresGridID').trigger("reloadGrid");
	ns.common.setFirstGridPage('#pendingWiresGridID');
	$('#pendingWiresGridID').trigger("reloadGrid");	
	ns.common.setFirstGridPage('#completedWiresGridID');
	$('#completedWiresGridID').trigger("reloadGrid");*/
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#details").setInitialFocus();
	}
});

// $.subscribe('cancelAddWireOnBatchFormClickTopics', function(event,data) {
    // $.ajax({
        // url: "/cb/pages/jsp/wires/" + $("#backToBatchURLForJS").val(),
        // success: function(data) {
            // $("#inputDiv").html(data);
        // }
    // });
    // ns.wire.gotoStep(1);
// });

/**
* Url Encryption Edit by Dongning
*/
$.subscribe('cancelAddWireOnBatchFormClickTopics', function(event,data) {
	var isMangePayee = $('#isManagePayeeModified').val();
    var urlString = ns.wire.cancelWireURLFromWireBatch;
	$.ajax({
        url: urlString,
        success: function(data) {
            $("#inputDiv").html(data);
            if($("#isManagePayeeModified").length > 0) {
            	$('#isManagePayeeModified').val(isMangePayee);
            }
        }
    });
    ns.wire.gotoStep(1);
});

$.subscribe('releaseWiresOnClickTopics', function(event, data) {
    $.log('About to release wire! ');
    $("#summary").hide();
    $('#details').hide();
    $('#popupcriteria').hide();
	wireReleaseAll = false;
	wireRejectAll = false;
	wireRelease = {};	
});

$.subscribe('releaseWiresonCompleteTopics', function(event, data) {
    ns.wire.gotoReleaseWire();

    $.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#details").setFocus();
	}
});

ns.wire.gotoReleaseWire = function(){
	ns.common.refreshDashboard('dbReleaseWireSummary');
	$("#summary").hide();
	$("#mappingDiv").hide();
	$("#customMappingDetails").hide();
	$('#wiresReleaseDiv').show();
	$("#fileupload").hide();
	//ns.wire.uiroller.gotoMode("releasewire");

	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#details").setFocus();
	}
	/*
    $('#quickNewWireTransferLink').hide();
    $('#quickNewWireTemplateLink').hide();
    $('#createBeneficiaryLink').hide();
    $('#releaseWiresLink').hide();
    $('#quickSearchLink').hide();
    $('#quickSearchTemplateLink').hide();
    $('#wiresReleaseConfirmDiv').hide()
    $('#details').hide();
    $('#summary').hide();
    $('#wiresLink').show();
    $('#templatesLink').show();
    $('#beneficiaryLink').show();
    $('#wiresReleaseDiv').show();
    $('#FileImportEntryLink').show();
    $('#fileupload').hide();
	$('#customMappingsLink').hide();
    $('#quicksearchReleaseWireLink').show();
	$('#quicksearchcriteria').hide();
	ns.common.resizeWidthOfGrids();
	*/
}

$.subscribe('cancelFromWiresReleaseOnCompleteTopics', function(event,data){
    ns.wire.gotoWires();
});

$.subscribe('submitWiresReleaseOnCompleteTopics', function(event,data) {
    $('#wiresReleaseDiv').hide();
    $('#wiresReleaseConfirmDiv').show();
});

$.subscribe('submitWiresReleaseConfirmOnCompleteTopics', function(event,data){
    ns.wire.gotoWires();
    $('#wiresReleaseConfirmDiv').hide();
});

$.subscribe('backFromWiresReleaseConfirmOnCompleteTopics', function(event,data){
    $('#wiresReleaseDiv').show();
    $('#wiresReleaseConfirmDiv').hide();
});


$.subscribe('cancelReleaseWireForm', function(event,data){
	wireRelease = {};	
	wireReleaseAll = false;
	wireRejectAll = false;
});

$.subscribe('cancelFromWiresReleaseConfirmOnCompleteTopics', function(event,data){
    //ns.wire.gotoWires();
	ns.common.refreshDashboard('showdbReleaseWireSummary',true);
	$("#wiresReleaseDiv").show();
	$("#wiresReleaseConfirmDiv").hide();
});

//	$.subscribe('wireBatchAddTemplateSubmitClickTopics', function(event,data) {
//		$.ajax({
//			  type: "POST",
//			  url: "/cb/pages/jsp/wires/modifyWireBatchTemplateAction_addWrieTemplateSent.action",
//			  data: $("#frmBatchOnAddTemplateID").serialize(),
//			  success: function(data) {
////				$.ajax({
////				  url: "/cb/pages/jsp/wires/wirebatchedittemplate.jsp?DontInitializeBatch=true",
////				  success: function(data) {
////					 $('#inputDiv').html(data);
////				  }
////				});
//			  }
//		});
//	});
$.subscribe('wireBatchAddTemplateSubmitBeforeTopics', function(event,data) {
    removeValidationErrors();
});

// $.subscribe('wireBatchAddTemplateSubmitSuccessTopics', function(event,data) {
    // $.ajax({
        // url: "/cb/pages/jsp/wires/wirebatchedittemplate.jsp?DontInitializeBatch=true",
        // success: function(data) {
            // $('#inputDiv').html(data);
        // }
    // });
    // ns.common.closeDialog('#wireBatchEditAddTemplateDialogID');
// });


/**
* Url Encryption Edit by Dongning
*/
$.subscribe('wireBatchAddTemplateSubmitSuccessTopics', function(event,data) {
	/*var urlString=ns.wire.getwirebatchedittemplateUrl();
    $.ajax({
        url: urlString,
        success: function(data) {
            $('#inputDiv').html(data);
        }
    });*/
    ns.common.closeDialog('#wireBatchEditAddTemplateDialogID');
});

$.subscribe('deleteWireInBatchCompleteTopics', function(event,data) {
	//ns.common.closeDialog('#deleteWireInBatchDialogID');
	/*
	*Delete wire from batch can close dialog now(Wrong previous dialog ID)
	*Edit by Dongning
	*/
	ns.common.closeDialog('#deleteWireInBatchyDialogID');
});

// $.subscribe('deleteWireInBatchSuccessTopics', function(event,data) {
    // var wireBack_url = $("#wireBackURLForJS").val();
    // $.ajax({
        // url: "/cb/pages/jsp/wires/" + wireBack_url,
        // success: function(data) {
            // $('#inputDiv').html(data);
        // }
    // });
// });

/**
* Url Encryption Edit by Dongning
*/
$.subscribe('deleteWireInBatchSuccessTopics', function(event,data) {
    $.ajax({
        url: ns.wire.deleteTransferFromBatchURL,
        success: function(data) {
            $('#inputDiv').html(data);
        }
    });
});

/**
* Url Encryption Edit by Dongning
*/
ns.wire.viewWireTransferFromBatch = function(urlString){
     $.ajax({
         url: urlString,
         success: function(data) {
        	 $('#viewWiresDetailsDialogID').css("display","none");
        	 $('#viewWiresDetailsDialogID').parent().css("display","none");
             $('#viewWiresDetailsFromBatchDialogID').html(data).dialog('open');
         }
     });
 }

ns.wire.closeviewWireTransferDialogInBacth = function(){
    $('#viewWiresDetailsFromBatchDialogID').dialog('close');
    $('#viewWiresDetailsDialogID').parent().css("display","block");
	$('#viewWiresDetailsDialogID').css("display","block");
}

// ns.wire.viewTotalsInBatchConfirm = function(urlString){
    // $.ajax({
        // url: "/cb/pages/jsp/wires/" + urlString,
        // success: function(data) {
            // $('#verifyDiv').html(data);
        // }
    // });
// }
/**
* Url Encryption Edit by Dongning
*/
ns.wire.viewTotalsInBatchConfirm = function(){
    $.ajax({
        url: ns.wire.viewTotalsInBatchConfirmURL,
        success: function(data) {
            $('#verifyDiv').html(data);
        }
    });
}


$.subscribe('onClickUploadWireFormTopics', function(event,data){
	//load import dialog if not loaded
	var config = ns.common.dialogs["fileImport"];
	config.autoOpen = "false";
	ns.common.openDialog("fileImport");

	var configImportResults = ns.common.dialogs["fileImportResults"];
	configImportResults.autoOpen = "false";
	ns.common.openDialog("fileImportResults");
	$('#releaseWiresGridDivID').hide();
	$('#wiresReleaseConfirmDiv').hide();
	
	$('#operationresult').hide();
    ns.wire.gotofileimport();
    $("#details").hide();
});


ns.wire.cancelFileImport = function() {
	ns.common.refreshDashboard('dbWireSummary');
	$("#fileupload").hide();
	$("#customMappingDetails").hide();
	$("#summary").show();
};

ns.wire.gotofileimport = function() {
	$("#fileupload").show();
	$("#mappingDiv").hide();
	$("#summary").hide();
	$("#customMappingDetails").hide();
	ns.common.refreshDashboard('dbFileUploadSummary');
	
	//ns.wire.uiroller.gotoMode("fileimport");
	/*
	$('#fileupload').show();
    $('#fileuploadPortlet').portlet('show');
    $('#summary').hide();
    $('#quickSearchLink').hide();
    $('#quickSearchTemplateLink').hide();
    $('#quickNewWireTransferLink').hide();
    $('#createBeneficiaryLink').hide();
    $('#quickNewWireTemplateLink').hide();
    $('#newWireTransferCriteria').hide();
    $('#quicksearchcriteria').hide();
    $('#quicksearchTemplatecriteria').hide();
    $('#details').hide();
    $('#mappingDiv').hide();
    $('#releaseWireDiv').hide();
    $('#wiresReleaseConfirmDiv').hide();
    $('#customMappingsLink').removeAttr('style');
    $('#wiresLink').show();
    $('#FileImportEntryLink').hide();
    $('#newWireTemplateCriteria').hide();
    $('#templatesLink').show();
    $('#beneficiaryLink').show();
    $('#quicksearchReleaseWireLink').attr('style','display:none');
    $('#quicksearchReleaseWireCriteria').hide();
    $('#releaseWiresLink').show();
    */
}

/**
* Security checking for wires importing file.
*/
$.subscribe('errorImportingWiresFileFormTopics', function(event,data) {
    var resStatus = event.originalEvent.request.status;
    if(resStatus === 700) //700 indicates interval validation error
        ns.common.openDialog("authenticationInfo");

});

//Sort wire imported results.
ns.wire.sortImportedResults = function(sortURL){
    $.ajax({
        url: "/cb/pages/jsp/wires/" + sortURL ,
        success: function(data){
            $("#checkFileImportResultsDialogID").html(data);
        }
    });
}

/* TO show the PRINTER READY window */
// ns.wire.printerReadyButtonOnClick = function(){
    // var url = "/cb/pages/jsp/wires/inc/print_wiretransfersend.jsp";
	// var isHostWire = $("#isHostWireID").val();
	// if( isHostWire == "true" ){
		// url = "/cb/pages/jsp/wires/inc/print_wirehostsend.jsp";
	// }
    // $.ajax({
        // url: url,
        // success: function(data){
            // $("#printerReadyWireDialog").html(data).dialog('open');
            // var titleText = $('#wireTypeTextID').html();
            // $("#ui-dialog-title-printerReadyWireDialog").html(titleText);
        // }
    // });
// }

/**
* Url Encryption Edit by Dongning
*/
/* TO show the PRINTER READY window */
ns.wire.printerReadyButtonOnClick = function(printReadyUrl){
    $.ajax({
        url: printReadyUrl,
        success: function(data){
            $("#printerReadyWireDialog").html(data).dialog('open');
            var titleText = $('#wireTypeTextID').html();
            $("#ui-dialog-title-printerReadyWireDialog").html(titleText);
        }
    });
}

ns.wire.printWire = function(){
	$('#printerReadyWireDialog').print();
}

$.subscribe('mappingListDoneSuccessWire', function(event,data) {
    $('#mappingDiv').hide();
	$('#customMappingsLink').removeAttr('style');
    //$('#summary').show();
	$("#customMappingDetails").hide();
	$('#fileupload').show();
    //$('#fileuploadPortlet').portlet('show');
	$('#addCustomMappingsLink').hide();
	
	ns.wire.gotofileimport();


	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#fileUploaderPanelID").setFocus();
	}
});

$.subscribe('errorSendWireBatchForm', function(event,data) {
    $.log('wire batch error! ');

    var resStatus = event.originalEvent.request.status;
    if(resStatus === 700) //700 indicates interval validation error
        ns.common.openDialog("authenticationInfo");

});

$.subscribe('loadWireTempCompleteTopic', function(event,data) {
   ns.wire.showScopeWarning = true;

   $.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#TransactionWizardTabs").setInitialFocus();
	}
});

//Release wires for rejecting or releasing all.
ns.wire.setAll = function(idx) {
	var confirmVal = '';
	if(idx == 1) {
		confirmVal = confirm($('#releaseWireMsg').val());
	} else {
		confirmVal = confirm($('#rejectWireMsg').val());
	}
	
	if(!confirmVal){
		return;
	}
    var showVal = "";
    if( idx == 1)
        showVal = "Released";
    else if ( idx == 2)
        showVal = "Rejected";
    if(idx == 1){
    	wireReleaseAll = true;
    	wireRejectAll = false;
    	wireRelease = {};	
    } else {
    	wireReleaseAll = false;
    	wireRejectAll = true;
    	wireRelease = {};	
    }
    // get rowIds, aim to get number of rows
    var ids = $("#releaseWiresGridID").jqGrid('getDataIDs');
    // operation each row
    for(var i=0;i<ids.length;i++){
        var rowId = ids[i];
        var currentID = "#" + rowId;
        //find the last cell of the current row
       
        var $InnerSelect = $(currentID, $("#releaseWiresGridID")).find("select");
        var lastCellInGrid = $InnerSelect.parent();
        var InnerSelect = $InnerSelect[0];
        if (InnerSelect.options[idx].disabled != true) {
            InnerSelect.selectedIndex = idx;
            lastCellInGrid.find('.ui-selectmenu-status').html(showVal);
        }
    }//for
};

$(document).ready(function(){
    $.debug(false);
    ns.wire.setupWires();
});

//Show the active dashboard based on the user selected summary
ns.wire.showWireDashboard = function(wireUserActionType,wireDashboardElementId){
	if(wireUserActionType == 'Wires'){
		ns.common.refreshDashboard('dbWireSummary');	
	}else if(wireUserActionType == 'Template'){
		ns.common.refreshDashboard('dbTemplateSummary');
	}else if(wireUserActionType == 'Payee'){
		ns.common.refreshDashboard('dbPayeeSummary');
	}else if(wireUserActionType == 'ReleaseWires'){
		ns.common.refreshDashboard('showdbReleaseWireSummary',true);
	}else{//In case of normal Wires top menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbWireSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(wireDashboardElementId && wireDashboardElementId !='' && $('#'+wireDashboardElementId).length > 0){
		//$("#"+wireDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+wireDashboardElementId).trigger("click");}, 50);
	}
};

ns.wire.resetWireTypeSelectMenu = function(){
	if (ns.wire.isSelectMenuAvailable("#wiresTypesID")) {
		$('#wiresTypesID').get(0).selectedIndex=0;
		$('#wiresTypesID').selectmenu('destroy');
		$("#wiresTypesID").selectmenu({width:'9em'});
	}
};

ns.wire.resetWireTemplateTypeSelectMenu = function(){
	if (ns.wire.isSelectMenuAvailable("#wiresTemplateTypesID")) {
		$('#wiresTemplateTypesID').get(0).selectedIndex=0;
		$('#wiresTemplateTypesID').selectmenu('destroy');
		$("#wiresTemplateTypesID").selectmenu({width:'12em'});
	}
};

// This function is used to check whether given select box element is present or not and its legnth > 0.
ns.wire.isSelectMenuAvailable = function(id){
	if ($(id).length > 0) {
		return true;
	}
	return false;
}

// This topic is used to reset beneficiary scope
$.subscribe('onCloseBeneficiaryScopeTopics', function(event,data) {
	if (ns.wire.callOnCloseTopic == true) {
		ns.wire.isChangeBeneScope = false;
    	ns.wire.changeBeneScope(ns.wire.origSelect);
	}
});

$.subscribe('cancelAddWireOnBatchFormComplete', function(event,data) {
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#details").setInitialFocus();
	}
});

$.subscribe('onImportWireFormCompleteTopics', function(event,data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#fileUploaderPanelID").setFocus();
	}
});

$.subscribe("addWireClickTopic",function(e,d){
	var $currentTarget = $(e.originalEvent.currentTarget);
	var nextWireIndex = $currentTarget.attr("nextWireIndex");
	addWire(nextWireIndex);
});

$.subscribe('customMappingBefore', function(event,data) {
	removeValidationErrors();	
});	

/*
 * This opens a confirmation dialog on change of wire type.
 */
ns.wire.changeWireTypeConfirmation = function(isBatch){
	$.ajax({
		url: "/cb/pages/jsp/wires/wiretypechangeconfirmation.jsp?isBatch="+isBatch,
		success: function(data){
			$('#ui-dialog-title-wireTypeChangeConfirmationDialogID').html( js_CONFIRM );
			$('#wireTypeChangeConfirmationDialogID').html(data).dialog('open');
		}
	});
}

ns.wire.changeWireTypeConfirmationForTemplate = function(isBatch){
	$.ajax({
		url: "/cb/pages/jsp/wires/wiretypechangeconfirmation.jsp?isTemplate=true",
		success: function(data){
			$('#ui-dialog-title-wireTypeChangeConfirmationDialogID').html( js_CONFIRM );
			$('#wireTypeChangeConfirmationDialogID').html(data).dialog('open');
		}
	});
}


$.subscribe('onWireCustomMappingClickTopics', function(event,data) {
	$("#summary").hide();
	$("#fileupload").hide();
	$("#customMappingDetails").hide();
	$('#wiresReleaseDiv').hide();
});

$.subscribe('changeWireType', function(event,data) {
	ns.wire.oldWireType = $("#wiresTypesID").val();
	ns.common.closeDialog('#wireTypeChangeConfirmationDialogID');
	$('#newSingleWiresTransferID').click();
});

$.subscribe('changeWireBatchType', function(event,data) {
	ns.wire.oldWireType = $("#wiresTypesID").val();
	ns.common.closeDialog('#wireTypeChangeConfirmationDialogID');
	$('#newBatchWiresTransferID').click();
});

$.subscribe('changeWireTypeTemplate', function(event,data) {
	ns.wire.oldWireTemplateType = $("#wiresTemplateTypesID").val(); 
	ns.common.closeDialog('#wireTypeChangeConfirmationDialogID');
	$('#newSingleWiresTemplateID').click();
});

$.subscribe('closeConfirmationDialog', function(event,data) {
	ns.common.closeDialog();
	if(!ns.wire.isTemplate){
		$("#wiresTypesID").selectmenu("value",ns.wire.oldWireType);
	}else{
		$("#wiresTemplateTypesID").selectmenu("value",ns.wire.oldWireTemplateType);
	}
});

/* Functions ported from wires_dashboard.jsp */
function hideDashboard(){
	$('#appdashboard').hide();
	$('#wiresGridTabs').hide();
	$('#wireTemplateGridTabs').hide();
	$('#beneficiariesGridTabs').hide();
}

function showDashboard(){
	$('#appdashboard').show();
	$('#wiresGridTabs').show();
	$('#wireTemplateGridTabs').show();
	$('#beneficiariesGridTabs').show();
}

$.subscribe('prepareTemplateJsp', function(event,data) {
	$('#wireTemplateDiv').toggle();
});


$.subscribe('setDisableOnChange', function(event,data) {
	ns.wire.enableOnChange = false;
});

function quickSearchOnclick(){
	$('#popupcriteria').show();
    //$('#newWireTransferCriteria').hide();
	//$('#newWireTemplateCriteria').hide();
	$('#quicksearchReleaseWireCriteria').hide();
	$('#quicksearchcriteria').toggle();
}

function newWireOnclick(){
	$('#popupcriteria').show();
    $('#quicksearchcriteria').hide();
	$('#newWireTemplateCriteria').hide();
	$('#quicksearchReleaseWireCriteria').hide();
	$('#newWireTransferCriteria').toggle();
}

function quickSearchReleaseWireOnclick(){
	$('#popupcriteria').show();
    $('#newWireTransferCriteria').hide();
	$('#newWireTemplateCriteria').hide();
	$('#quicksearchTemplatecriteria').hide();
	$('#quicksearchReleaseWireCriteria').toggle();
}

function newWireTempOnclick(){
	$('#popupcriteria').show();
    $('#quicksearchTemplatecriteria').hide();
	$('#newWireTransferCriteria').hide();
	$('#quicksearchReleaseWireCriteria').hide();
	$('#newWireTemplateCriteria').toggle();
}

$("#appdashboard-right").hide();

function openTemplates(){
	$("#templatesLink").trigger('click');
}

function openWires(){
	$("#wiresLink").trigger('click');
}

function openBeneficiary(){
	$("#beneficiaryLink").trigger('click');
}

function openFileImport(){
	$("#FileImportEntryLink").trigger('click');
}

function releaseWires(){
	$("#releaseWiresLink").trigger('click');
}

function reloadWires(){
	$('#menu_pmtTran_wire a').trigger('click');
}
