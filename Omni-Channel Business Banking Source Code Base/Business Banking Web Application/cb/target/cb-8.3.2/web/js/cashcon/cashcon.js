	
	ns.cashcon.setup = function (){
		
		ns.cashcon.gotoCashcon();
	
		/*
		 * Control dashboard button display 
		 */
		$.subscribe('cashconSummaryLoadedSuccess', function(event,data){
			ns.cashcon.gotoCashcon();
		});
		
		/*
		 *  Load cash concentration form
		 */
		$.subscribe('beforeLoadCashconForm', function(event,data) {
			$.log('About to load form! ');
	        //$('#details').slideUp();
	        
			$('#quicksearchcriteria').hide();
	
		});
		   
		$.subscribe('loadCashconFormComplete', function(event,data) {
			$.log('Form Loaded! ');
			ns.cashcon.foldPortlet('summary');
			//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
			var heading = "";
			//var $title = $('#details .portlet-title');
			//$title.html(heading);
			
	        ns.cashcon.gotoStep(1);    
			$('#details').slideDown();
			
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#inputDiv").setFocus();
			}
		
		});
	
		$.subscribe('errorLoadCashconForm', function(event,data) {
			$.log('Form Loading error! ');
			
		});
		
	
		/*
		 * Verify transfre form
		 */
		$.subscribe('beforeVerifyCashconForm', function(event,data) {
			$.log('About to verify form! ');
			removeValidationErrors();
				
		});
		
		$.subscribe('verifyCashconFormComplete', function(event,data) {
			$.log('Form Verified! ');
			ns.cashcon.gotoStep(2);	
			
		});
	
		$.subscribe('errorVerifyCashconForm', function(event,data) {
			$.log('Form Verifying error! ');
			
		});
		
		$.subscribe('successVerifyCashconForm', function(event,data) {
			$.log('Form Verifying success! ');
			
		});
		
	
		
		/*
		 * Send cash concentration form
		 */
		$.subscribe('beforeSendCashconForm', function(event,data) {
			$.log('About to send cash concentration! ');
		});
		
		$.subscribe('sendCashconFormComplete', function(event,data) {
			$.log('sending cash concentration complete! ');
		});
	
		$.subscribe('sendCashconFormSuccess', function(event,data) {
			$.log('sending cash concentration succeeded! ');
			
			ns.common.showStatus($('#cashconResultMessage').html());
			ns.cashcon.gotoStep(3);	
		}); 
		
	
		$.subscribe('errorSendCashconForm', function(event,data) {
			$.log('transfer error! ');
			
			var resStatus = event.originalEvent.request.status;
			if(resStatus === 700) //700 indicates interval validation error
				ns.common.openDialog("authenticationInfo");
	
		});
	
		/*
		 * Save single transfer template 
		 */
		$.subscribe('beforeSaveSingleCashconTemplate', function(event,data){
			$.log('beforeSaveSingleCashconTemplate');
			removeValidationErrors();
		});
		
		$.subscribe('errorSaveSingleCashconTemplate', function(event,data){
			$.log('errorSaveSingleCashconTemplate');
		});
		
		
		$.subscribe('quickSearchCashconComplete', function(event,data) {
			$.publish("reloadSelectedGridTab");
			$('#details').hide();
			$('#cashconGridTabs').portlet('expand');
			ns.common.hideStatus();
		});
		
		/*
		 * Delete Cashcon
		 */
		$.subscribe('beforeDeleteCashcon', function(event,data){
			$.log('beforeDeleteCashcon');
		});
		
		$.subscribe('errorDeleteCashcon', function(event,data){
			$.log('errorDeleteCashcon');
			ns.common.showError();
			ns.common.closeDialog('#innerdeletetransferdialog');
		});
		
		$.subscribe('deleteCashconComplete', function(event,data){
			$.log('deleteCashconComplete');
		});
		
		$.subscribe('successDeleteCashcon', function(event,data){
			$.log('successDeleteCashcon');
			ns.common.showStatus();
			ns.common.closeDialog('#innerdeletetransferdialog');
			ns.cashcon.reloadCashconSummaryGrid();
			ns.cashcon.closeCashconTabs();
		});
		
		$.subscribe('cancelCashconForm', function(event,data) {
		    
			$.log('About to cancel Form! ');
			ns.cashcon.closeCashconTabs();
			ns.common.hideStatus();
			$('#cashconGridTabs').portlet('expand');
			$('#cashconGridTabs').show();
			ns.common.selectDashboardItem("goBackSummaryLink");
		});
		 
		$.subscribe('backToInput', function(event,data) {
		    
			$.log('About to back to input! ');
			ns.cashcon.gotoStep(1);
		});
		
		$.subscribe('calendarLoadedSuccess', function(event,data){
			$('#quickSearchLink').hide();
			$('#goBackSummaryLink').show();
		});
		
		$.subscribe('calendarToSummaryLoadedSuccess', function(event,data){
			$('#goBackSummaryLink').hide();
			$('#quickSearchLink').show();
	
		});
		
		$.subscribe('resetCashconConfirmDiv', function(event,data){
			$("#confirmDiv").html($("#resetConfirmDiv").html());
			$.publish("reloadSelectedGridTab");
		});
		
		$.subscribe('summaryLoadFunction', function(event,data){
			//$('#goBackSummaryLink').hide();
			$('#details').slideUp();	
			$('#cashconGridTabs').portlet('expand');
			$('#cashconGridTabs').show();
		});
	};
	
	ns.cashcon.backInput  = function() {
		ns.cashcon.gotoStep(1);
		ns.common.hideStatus();
	};
	
	
	ns.cashcon.gotoCashcon = function(){
		//$('#goBackSummaryLink').attr('style','display:none');
		$('#quickSearchLink').show();
		$('#addSingleCashconLink').removeAttr('style');
		$('#cashconLink').attr('style','display:none');
		$('#details').hide();
		ns.common.hideStatus();
		
	
	}
	
	ns.cashcon.gotoStep = function (stepnum){
		
		ns.common.gotoWizardStep('detailsPortlet',stepnum);
		
		var tabindex = stepnum - 1;
		$('#TransactionWizardTabs').tabs( 'enable', tabindex );
		$('#TransactionWizardTabs').tabs( 'option','active', tabindex );
		$('#TransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
		$('#TransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );
	//	$('#TransactionWizardTabs').tabs( "option", "disabled", [0,2] );
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#TransactionWizardTabs").setFocusOnTab();
		}
	}
	
	ns.cashcon.expandPortlet = function(summary) {
		$('#cashconGridTabs').portlet('expand');
	};
	
	ns.cashcon.foldPortlet = function(summary) {
		$('#cashconGridTabs').portlet('fold');
		$('#cashconGridTabs').hide();
	};
	
	/**
	 * Close Details island and expand Summary island
	 */
	ns.cashcon.closeCashconTabs = function(){
		ns.cashcon.expandPortlet('summary');
	    $('#details').slideUp();
	}
	
	ns.cashcon.showCashConDashboard= function(cashConUserActionType,cashConDashboardElementId){
		if(cashConUserActionType === 'CashCon'){
			ns.common.refreshDashboard('dbcashConSummary');
		}
		//Simulate dashboard item click event if any additional action is required
		if(cashConDashboardElementId && cashConDashboardElementId !='' && $('#'+cashConDashboardElementId).length > 0){
				//Timeout is required to display spinning wheel for the dashboard button click
			setTimeout(function(){$("#"+cashConDashboardElementId).trigger("click");}, 10);
		}
	}

	
	$(document).ready(function(){
	//	jQuery.LINT.level = 1;
		$.debug(false);
	
		ns.cashcon.setup();
	});
