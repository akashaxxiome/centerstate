/* Called after verifying Cash Flow Summary parameters */
$.subscribe('verifyCashFlowCompleteTopic', function(event, data) {
	ns.cash.viewCashFlow();
});

$.subscribe('goTocashflowReporting', function(event, data) {
	ns.shortcut.goToFavorites('reporting_cashmgt');
});

ns.cash.viewCashFlow = function() {
	ns.common.setFirstGridPage('#cashflowSummaryID');
	$('#cashflowSummaryID').trigger("reloadGrid");
	var title = js_cashflow_summary_portlet_title;
	var date = $('#CashFlowCriteriaDate').val();
	if(date==null || date=="" || date==undefined || date=="MM/dd/yyyy") {
		date =  formatDate(new Date());  
	}
	$('#cashflowSummary').portlet('title',title + ' '+ date );  
};

function formatDate(d) {
	  var dd = d.getDate();
	  if ( dd < 10 ) dd = '0' + dd;
	  var mm = d.getMonth()+1;
	  if ( mm < 10 ) mm = '0' + mm;
	  var yy = d.getFullYear();
	  if ( yy < 10 ) yy = '0' + yy;
	  return    mm+'/'+dd+'/'+yy  ;
	}


$.subscribe('transferMoneyButtonComplete', function(event, data) {
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#details").setInitialFocus();
	}
});