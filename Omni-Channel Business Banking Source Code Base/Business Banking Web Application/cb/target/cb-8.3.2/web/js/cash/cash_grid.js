ns.cash.isDisbursementsByPresentment = false;

    $(document).ready(function(){
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
	});

ns.cash.resizeWidthOfGridCashSummary = function(){
		if( $("#summary").length > 0 ){
			ns.common.resizeWidthOfGrids();
		}

	}	
	
	//use to refresh account history summary for all account types.
ns.cash.refreshSummaryForm = function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#cashflowSummaryGrid").html(data);
		  }   
		});   
	}
	
ns.cash.refreshDashboardForm = function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#appdashboard").html(data);
		  }   
		});   
	}

	
	
	//cashflow summary grid complete event.
	$.subscribe('cashflowSummaryEvent', function(event,data) {
		var gridID = 'cashflowSummaryID';
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		
		if (ids.length > 0) {
			//Following line is commented to fix CR 770903 due to this sorting was not working. Its not required now
			// since previously the account and nickname was one column. Now these are two separate columns.  
			//ns.cash.customCashflowNicknameColumn("#cashflowSummaryID");		
			ns.cash.populateCashFlowSummaryFooter("#cashflowSummaryID");
			//$("#cashflowSummaryID_pager_center").hide();
			ns.common.resizeWidthOfGrids();
			$('#cashflowSummary').slideDown();
		}
		ns.common.resizeWidthOfGrids();
		$('#cashflowSummary').slideDown();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			accessibility.setFocusOnDashboard();
		}
	});
	
	ns.cash.customCashflowNicknameColumn = function(gridID){
		//custom Account Number and Account Nickname header
		if($('#cashflowSummaryID_account').size() == 0) {
			/*$('<div>Account</div>')
			.attr('id', 'cashflowSummaryID_account')
			.attr('style','cursor:pointer')
			.bind('click', function() {
				$(gridID).jqGrid('setGridParam',{sortname:'account'});	
				if($('#cashflowSummaryID_account').attr('order') == 'asc') {
					$('span.ui-icon-desc',$('#cashflowSummaryID_account')).removeClass("ui-state-disabled");
					$('span.ui-icon-asc',$('#cashflowSummaryID_account')).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'desc'});
					$('#cashflowSummaryID_account').attr('order','desc');
				} else {
					$('span.ui-icon-asc',$('#cashflowSummaryID_account')).removeClass("ui-state-disabled");	
					$('span.ui-icon-desc',$('#cashflowSummaryID_account')).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
					$('#cashflowSummaryID_account').attr('order','asc');
				}
				$('.s-ico').hide();
				$(gridID).trigger('reloadGrid');
			}).insertBefore($("#jqgh_cashflowSummaryID_accountNickName").addClass("gridNicknameField"))
			.append("<span class='s-ico' style='display:none'><span sort='asc' class='ui-grid-ico-sort ui-icon-asc ui-state-disabled ui-icon ui-icon-triangle-1-n ui-sort-ltr'></span><span sort='desc' class='ui-grid-ico-sort ui-icon-desc ui-state-disabled ui-icon ui-icon-triangle-1-s ui-sort-ltr'></span></span>");
			
			$('span.ui-icon-asc',$('#cashflowSummaryID_account')).removeClass("ui-state-disabled");	
			$('span.ui-icon-desc',$('#cashflowSummaryID_account')).addClass("ui-state-disabled");
            ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
			$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
			$('#cashflowSummaryID_account').attr('order','asc');
			$('span.s-ico',$('#cashflowSummaryID_account')).show();
			$('span.s-ico',$("#jqgh_cashflowSummaryID_accountNickName")).hide();*/
			ns.common.setFirstGridPage(gridID); 
			$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
			$('#cashflowSummaryID_account').attr('order','asc');
			$('span.s-ico',$('#cashflowSummaryID_account')).show();
		} else { 
			$('span.s-ico',$('#cashflowSummaryID_account')).show();
			if($(gridID).jqGrid('getGridParam','sortname') == 'account') {
				$('span.s-ico',$('#cashflowSummaryID_account')).show();
			}
			if($(gridID).jqGrid('getGridParam','sortname') == 'accountNickName') {
				$('span.s-ico',$('#cashflowSummaryID_account')).hide();
				$('span.s-ico',$('#jqgh_cashflowSummaryID_accountNickName')).show();
			}
		}
	}
	
	ns.cash.populateCashFlowSummaryFooter = function(gridID){
	
		var data = $(gridID).jqGrid('getGridParam','userData');
			if(data) {	
			var tempOpenLedger = data.displayOpeningLedger;
			var opeLedger= data.displayOpeningLedger;
			var totalDisplay = "";
			var openingTotalColor = data.displayOpeningLedgerTotalColor;
			var closingTotalColor = data.displayClosingLedgerTotalColor;
			var displayDebitsTotalColor=data.displayDebitsTotalColor;
			var displayCreditsTotalColor=data.displayCreditsTotalColor;	

			if(tempOpenLedger!=null && tempOpenLedger!=undefined && tempOpenLedger!=""){
				opeLedger = tempOpenLedger.substring(tempOpenLedger.indexOf(") ")+1);
				totalDisplay = tempOpenLedger.substring(0, tempOpenLedger.indexOf(") ")+1);
			}
			if ( openingTotalColor==""|| openingTotalColor==null){
				openingTotalColor = "BLACK";
			}
			
			if (closingTotalColor==""|| closingTotalColor==null){
				closingColor = "BLACK";
			}
			if ( displayDebitsTotalColor==""|| displayDebitsTotalColor==null){
				displayDebitsTotalColor = "BLACK";
			}
			
			if (displayCreditsTotalColor==""|| displayCreditsTotalColor==null){
				displayCreditsTotalColor = "BLACK";
			}
			

			var openLedgerTotalHTML = totalDisplay+'<span style="color:' + openingTotalColor+ '" encode="false">' + opeLedger + '</span>';
			var closeLedgerTotalHTML = '<span style="color:' +closingTotalColor+'" encode="false">' + data.displayClosingLedger + '</span>';	
			var debitsTotalHTML = '<span style="color:' +displayDebitsTotalColor+'" encode="false">' + data.displayTotalDebits + '</span>';	
			var creditsTotalHTML = '<span style="color:' +displayCreditsTotalColor+'" encode="false">' + data.displayTotalCredits + '</span>';					
				$(gridID).jqGrid('footerData','set',{'displayOpeningLedger': openLedgerTotalHTML, 'numTransactions': data.numTransactions, 'displayTotalDebits': debitsTotalHTML, 'displayTotalCredits': creditsTotalHTML, 'displayClosingLedger': closeLedgerTotalHTML}, false);
			}		
	}
	
	//formatter for account nickname column
ns.cash.formatAccountNickNameColumn = function(cellvalue, options, rowObject){
		var _toAccountRountingNumber = rowObject["account"].routingNum; 
		var _toAccountDisplayText = rowObject["account"].displayText;
		var _toAccountNickName = rowObject["account"].nickName; 
		var _toAccountCurrencyCode = rowObject["account"].currencyCode;
		var _toAccountName = _toAccountRountingNumber + " : ";
		if( _toAccountNickName != undefined && _toAccountNickName != "" ){
			_toAccountName = _toAccountName +  _toAccountNickName;
		} 
		if( _toAccountCurrencyCode != undefined && _toAccountCurrencyCode != "" ){
			_toAccountName = _toAccountName + " - " + _toAccountCurrencyCode;
		}
		return _toAccountName;		
	}
	
 /**Format Account column for cash flow grid*/
 ns.cash.formatAccountColumn = function(cellvalue, options, rowObject){
	var _toAccountName = "";
	var _toAccountDisplayText = rowObject["account"].displayText;
	if( _toAccountDisplayText != undefined && _toAccountDisplayText != "" ){
		_toAccountName =  ""+_toAccountDisplayText;
	} 
	return _toAccountName;	
 }

	//formatter for amount column
ns.cash.formatCashAmountColumn = function(cellvalue, options, rowObject){
		var amount = "";
		if(cellvalue == null || cellvalue==""){
			amount = "---";
		} else {
		   	amount = cellvalue;
		}
		return amount;
	}
	
	//formatter for Opening Ledger column
ns.cash.formatOpeningLedgerColumn = function(cellvalue, options, rowObject){
		var amount = "";
		if(cellvalue == null || cellvalue==""){
			amount = "---";
		} else {
		var color = rowObject["openingBalColor"];
		if(color==null || color == undefined||color==""){
			color ="BLACK";
		}
		if(color=="negativetrue"){
			color = "RED";
		}else if(color=="negativefalse"){
			color = "BLACK";
		}
		   	amount = "<font color='"+color+"'>"+cellvalue+"</font>";
		}
		return amount;
	}
	
	//formatter for Total Debits column
ns.cash.formatTotalDebitsColumn = function(cellvalue, options, rowObject){
		var amount = "";
		if(cellvalue == null || cellvalue==""){
			amount = "---";
		} else {
		var amt =0.0;
		if(rowObject["displayTotalDebitsCurrency"] != null && rowObject["displayTotalDebitsCurrency"] != undefined){		
			amt = rowObject["displayTotalDebitsCurrency"].amountValue;
		}
		if(amt<0){
		amount= "<font color='red'>"+cellvalue+"</font>";
		} else{
		   	amount = cellvalue;
		}
		}
		return amount;
	}
	
	//formatter for Total Credits column
ns.cash.formatTotalCreditsColumn = function(cellvalue, options, rowObject){
		var amount = "";
		if(cellvalue == null || cellvalue==""){
			amount = "---";
		} else {
		var amt =0.0;
		if(rowObject["displayTotalCreditsCurrency"] != null && rowObject["displayTotalCreditsCurrency"] != undefined){		
		amt = rowObject["displayTotalCreditsCurrency"].amountValue;
		}
		if(amt<0){
		amount= "<font color='red'>"+cellvalue+"</font>";
		} else{
		   	amount = cellvalue;
		}
		}
		return amount;
	}
	
	
	//formatter for Closing ledger column
ns.cash.formatClosingLedgerColumn = function(cellvalue, options, rowObject){
		var amount = "";
		if(cellvalue == null || cellvalue==""){
			amount = "---";
		} else {
		var color = rowObject["closingBalColor"];
		if(color==null || color == undefined||color==""){
			color ="BLACK";
		}
		if(color=="negativetrue"){
			color = "RED";
		}else if(color=="negativefalse"){
			color = "BLACK";
		}
		   	amount = "<font color='"+color+"'>"+cellvalue+"</font>";
		}
		return amount;
	}
	
	//formatter the date
ns.cash.dateFormatter = function(cellvalue, options, rowObject){
		var date = "";
		if(cellvalue == null || cellvalue==""){
			date = "---";
		} else {
			date = cellvalue.substr(0,10);
		}
		return date;
	}
	
ns.cash.searchImageFormatter = function(cellvalue, options, rowObject){ 

		var checkNumber = "<a href=\"#\" onClick=\"ns.cash.openImageWindow('"+rowObject["map"].ViewURL+"')\" >" + cellvalue + "</a>";
		return ' ' + checkNumber;
	}
	
ns.cash.openImageWindow = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#searchImageDialog').html(data).dialog('open');
			}
		});
	}
	
ns.cash.zoomImage = function(urlString){
		$.ajax({    
			url: urlString,     
			success: function(data) {  
			ns.common.closeDialog();
			$("#searchImageDialog").html(data).dialog('open');
			}   
		});   
	}
	
ns.cash.rotationFunction = function(urlString){
		$.ajax({    
			url: urlString,     
			success: function(data) {  
			ns.common.closeDialog();
			$("#searchImageDialog").html(data).dialog('open');
			}   
		});   
	}
	
	
	$.subscribe('deleteMappingComplete', function(event,data) {
		ns.common.closeDialog("#deleteMappingDialogID");
		$('#custommappingGridID').trigger("reloadGrid");
		ns.common.showStatus();
		
	});
	
	$.subscribe('mappingSuccess', function(event,data) {
		$('#pPaysummary').hide();
		$('#pPayList').hide();
		$('#mappingDiv').show();
		$('#ppaymappingLink').hide();
		$('#addCustomMappingsLink').removeAttr('style');
	});
	$.subscribe('addMappingSuccessCash', function(event,data) {
		ns.cash.gotoStep(1);
		$('#pPaysummary').show();
		$('#appdashboard').hide();
		$('#mappingDiv').hide();
	});
	
	$.subscribe('buildFormComplete', function(event,data) {
		ns.cash.gotoStep(2);

	});
	$.subscribe('buildFormBefore', function(event,data) {
		removeValidationErrors();
	});
	$.subscribe('customMappingBefore', function(event,data) {
		removeValidationErrors();	
	});
	
ns.cash.gotoStep = function(stepnum){
	
		ns.common.gotoWizardStep('ppay_workflowID',stepnum);
		
		var tabindex = stepnum - 1;
		$('#PPayWizardTabs').tabs( 'enable', tabindex );
		$('#PPayWizardTabs').tabs( 'option','active', tabindex );
		$('#PPayWizardTabs').tabs( 'disable', (tabindex+1)%3 );
		$('#PPayWizardTabs').tabs( 'disable', (tabindex+2)%3 );
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#PPayWizardTabs").setFocusOnTab();
		}
	}

	$.subscribe('addMappingFormComplete', function(event,data) {
		ns.cash.gotoStep(2);
	});
	
	$.subscribe('addMappingFormComplete2', function(event,data) {
		$('#pPaysummary').hide();
		$('#appdashboard').show();
		$('#mappingDiv').show();
		//$('#custommappingGridID').trigger("reloadGrid");
		
	});
	
	$.subscribe('FileUploadFormComplete', function(event,data) {
		ns.common.closeDialog("#uploadFileDialogID");
		ns.common.showStatus();
	});
	
	ns.cash.redirectToAccountHistory = function(urlString){
		//deposit and other Account request the same java server page
		urlString = urlString+"&CSRF_TOKEN="+ns.home.getSessionTokenVarForGlobalMessage;
		$.ajax({
			   url: urlString,
			   type: "POST",
			   success: function(data){
			   $("#notes").html(data);
			   //Set Account History menu as "Active"
			   $(".selectedMainMenuItemCls").removeClass("selectedMainMenuItemCls");
			   $('li[menuId="acctMgmt"]').addClass("selectedMainMenuItemCls");

			   //Set page title
			   var menuId = "acctMgmt";
			   var menuName = $("li[menuId='acctMgmt'] a").eq(0).html();
			   var submenuId = "acctMgmt_history";
			   var submenuName = $("li[menuId='acctMgmt_history'] a").eq(0).html().trim();
			   ns.home.updateBreadcrumb(menuName, submenuName, menuId);
			   
				//Add browser history entry. This is added for back and forward button support.
				if(window.parent && window.parent.addHistoryEntry){
				   //add menu id entry in history
				   window.parent.addHistoryEntry(submenuId);
				   // set window title
				   window.parent.changeWindowTitle(submenuName);
				}
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
				   accessibility.setFocusOnDashboard();
				}
		   }
		});
	};
	
	//Customs to account nick name column for template
	ns.cash.formatOfItemsColumn = function( cellvalue, options, rowObject ){ 
			var linkNew = "0";	
			var _accountGroupID = 1;
			var isFooter = false; 
			var _ofItems = rowObject["numTransactions"]; 
			var linkVar = "/cb/pages/jsp/account/InitAccountHistoryAction.action"; 
			
			/*if (rowObject["account"] != undefined ) {
				var _acountID = rowObject["account"].ID;
				var _accountBankID = rowObject["account"].bankID;
				var _acountRoutingNum = rowObject["account"].routingNum;
				var _dataClassification = rowObject["dataClassification"];
			}*/
			
			if (rowObject["map"] != undefined ) {
				isFooter = false;
				linkVar = rowObject["map"].ViewURL;
			} else {
				isFooter = true;
			}
			
			if (isFooter) {
				linkNew = _ofItems;
			} else {
				if(_ofItems != "0") {
					linkNew = "<a href=\"#\" onClick=\"ns.cash.redirectToAccountHistory('"+linkVar+"');\">"+_ofItems+"</a>";
				} 
			}
			return linkNew; 
	}

ns.cash.cashLockboxAccID="";
ns.cash.cashLockboxDataClassificationForBack="";
ns.cash.cashLockboxDateForBack="";
ns.cash.cashPresentmentClassificationForBack="";
ns.cash.cashPresentmentDateForBack="";
ns.cash.cashPresentmentPresentForBack="";
ns.cash.cashAccClassificationForBack="";
ns.cash.cashAccDateForBack="";
ns.cash.cashForBack="";



//dongning	
	ns.cash.refreshCashLockboxDetailGrid = function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#lockboxDetailGrid").html(data);
		  }   
		});   
	}
		
	$.subscribe('cashaccdisbursementEvent', function(event,data) {
		ns.cash.customCashNicknameColumnForDisbursement("#cashaccdisbursementID");
		ns.cash.formatAccountDisbursementNickNameColumn("#cashaccdisbursementID");
		ns.cash.formatAccountDisbursementAccountColumn("#cashaccdisbursementID");
		ns.cash.customToCashDisburseDetailColumnForAcc("#cashaccdisbursementID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('cashpresentSummaryEvent', function(event,data) {
		ns.cash.customToCashaccDisbursementColumn("#cashpresentSummaryID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('cashdisbursepostdetailEvent', function(event,data) {
		ns.cash.customToCashDisburseDetailColumnForPost("#cashdisbursepostdetailID");
		ns.cash.formatDisbursementPostDetailNickNameColumn("#cashdisbursepostdetailID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('cashdisbursedetailEvent', function(event,data) {
		ns.cash.customToCashDisburseItemColumn("#cashdisbursedetailID");
		ns.common.resizeWidthOfGrids();
		//CR 730095, Disable the 'Page Number' input and 'Navigate to Last' button since we don't provide "jumping to a specific page" function.
		$("#cashdisbursedetailID_pager").find("#last").addClass('ui-state-disabled');
		$("#cashdisbursedetailID_pager").find("#last").unbind('click');
		$("#cashdisbursedetailID_pager").find(".ui-pg-input").attr("disabled","disabled");
	});

	$.subscribe('cashlockboxsummaryEvent', function(event,data) {
		ns.cash.customCashNicknameColumn("#cashlockboxsummaryID");
		ns.cash.formatCashLockboxNickNameColumn("#cashlockboxsummaryID");
		ns.cash.formatCashLockboxAccountColumn("#cashlockboxsummaryID");
		$("#cashlocklistForAccountSummary").hide();
		ns.common.resizeWidthOfGrids();
		$.publish("common.topics.tabifyNotes");

	});
	
	$.subscribe('cashlockboxdepositEvent', function(event,data) {
		ns.cash.customToCashLockboxDetailColumn("#cashlockboxdepositID");
		ns.common.resizeWidthOfGrids();
		//CR 730202, Disable the 'Page Number' input and 'Navigate to Last' button since we don't provide "jumping to a specific page" function.
		$("#cashlockboxdepositID_pager").find("#last").addClass('ui-state-disabled');
		$("#cashlockboxdepositID_pager").find("#last").unbind('click');
		$("#cashlockboxdepositID_pager").find(".ui-pg-input").attr("disabled","disabled");

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#cashlockboxdepositID").setFocus();
		}
	});
	
	$.subscribe('cashlockboxdetailEvent', function(event,data) {
		ns.common.resizeWidthOfGrids();
		//CR 730202, Disable the 'Page Number' input and 'Navigate to Last' button since we don't provide "jumping to a specific page" function.
		$("#cashlockboxdetailID_pager").find("#last").addClass('ui-state-disabled');
		$("#cashlockboxdetailID_pager").find("#last").unbind('click');
		$("#cashlockboxdetailID_pager").find(".ui-pg-input").attr("disabled","disabled");
	});
	
	ns.cash.customCashNicknameColumn = function(gridID){
		var gridIdAppendForColumn = gridID;
      	if (gridID.startsWith('#')){
      		gridIdAppendForColumn = gridID.substring(1);
      	}
		 
		$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"_lockboxAccountName")).show();
		
		if($(gridID).jqGrid('getGridParam','sortname') == 'name') {
			$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"_lockboxAccountName")).show();
		}
		if($(gridID).jqGrid('getGridParam','sortname') == 'accountNickName') {
			$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"_lockboxAccountName")).hide();
			$('span.s-ico',$("#jqgh_cashlockboxsummaryID_lockboxAccountNickName")).show();
		}
	}
	
	ns.cash.customCashNicknameColumnForDisbursement = function(gridID){
		$('span.s-ico',$('#jqgh_cashaccdisbursementID_account')).show();
		if($(gridID).jqGrid('getGridParam','sortname') == 'ACCOUNTID') {
			$('span.s-ico',$('#jqgh_cashaccdisbursementID_account')).show();
		}
		if($(gridID).jqGrid('getGridParam','sortname') == 'NICKNAME') {
			$('span.s-ico',$('#jqgh_cashaccdisbursementID_account')).hide();
			$('span.s-ico',$("#jqgh_cashaccdisbursementID_accountNickName")).show();
		}
	}
	
	ns.cash.formatAccountDisbursementNickNameColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _toAccountRountingNumber = $(gridID).jqGrid('getCell', rowId, 'map.rountingNumber');
			var _toAccountName = $(gridID).jqGrid('getCell', rowId, 'map.accountName');
			var _toAccountcurrencyType = $(gridID).jqGrid('getCell', rowId, 'map.currencyType');
			var _toAccountDisbursementName = _toAccountRountingNumber;
			
			if( _toAccountName.length > 0 ){
				_toAccountDisbursementName = _toAccountDisbursementName + " : " + _toAccountName;
			} 	
			 
			if( _toAccountcurrencyType.length > 0 ){
				_toAccountDisbursementName = _toAccountDisbursementName + " - " + _toAccountcurrencyType;
			}
			$(gridID).jqGrid('setCell', rowId, 'accountNickName', _toAccountDisbursementName);
		}
	}
	
	ns.cash.formatAccountDisbursementAccountColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _toAccountDisplayText = $(gridID).jqGrid('getCell', rowId, 'map.accountDisplayText');
			$(gridID).jqGrid('setCell', rowId, 'account', _toAccountDisplayText);
		}
	}
	
	ns.cash.formatDisbursementPostDetailNickNameColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			//Construct "To Account Nickname" column.
			var _toPostDetailRountingNumber = $(gridID).jqGrid('getCell', rowId, 'map.rountingNumber');
			var _toPostDetailDisplayText = $(gridID).jqGrid('getCell', rowId, 'map.accountDisplayText');
			var _toPostDetailAccountName = $(gridID).jqGrid('getCell', rowId, 'map.accountName');
			var _toPostDetailcurrencyType = $(gridID).jqGrid('getCell', rowId, 'map.currencyType');
			var _toDisbursementPostDetailName = _toPostDetailRountingNumber;
			
			if( _toPostDetailDisplayText.length > 0 ){
				_toDisbursementPostDetailName = _toDisbursementPostDetailName + " : " + _toPostDetailDisplayText;
			} 	
			if( _toPostDetailAccountName.length > 0 ){
				_toDisbursementPostDetailName = _toDisbursementPostDetailName + " - " + _toPostDetailAccountName;
			} 
			if( _toPostDetailcurrencyType.length > 0 ){
				_toDisbursementPostDetailName = _toDisbursementPostDetailName + " - " + _toPostDetailcurrencyType;
			}
			$(gridID).jqGrid('setCell', rowId, 'accountNickName', _toDisbursementPostDetailName);
		}
	}
			
	ns.cash.customToCashaccDisbursementColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _presentment = $(gridID).jqGrid('getCell', rowId, 'presentment');
			var _numItemsPending = $(gridID).jqGrid('getCell', rowId, 'numItemsPending');
			var _accountsDate = $(gridID).jqGrid('getCell', rowId, 'map.date');
			var _dateClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');
			var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
			var link = "<a href=\"#\" onClick=\"ns.cash.redirectPresentmentDisbursementDetail('"+linkVar+"')\" >" +_numItemsPending + "</a>";
				   
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_numItemsPending']:eq("+i + ")").html(link);
		}
		ns.cash.cashPresentmentPresentForBack=_presentment;
		ns.cash.cashPresentmentClassificationForBack=_dateClassification;
		ns.cash.cashPresentmentDateForBack=_accountsDate;
	}
	
	ns.cash.redirectPresentmentDisbursementDetail = function(urlString){
		$.ajax({
			url: urlString,

			success: function(data){
				$('#cashDisbursedetail').html(data).show();
				$("#transfer").hide();
				$("#criteria").hide();
				$("#cashAccDisbursement").hide();
				$("#cashPresentment").hide();
				$("#cashDibursementGridTabs").hide();
				$("#dbCashDibursementSummary a span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
		   }
		});
	}
	
	ns.cash.customToCashDisburseDetailColumnForAcc = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _numItemsPending = $(gridID).jqGrid('getCell', rowId, 'numItemsPending');
			var _accountID = $(gridID).jqGrid('getCell', rowId, 'account.accountID');
			var _accountsDate = $(gridID).jqGrid('getCell', rowId, 'map.date');
			var _dateClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');
			var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
			if(_numItemsPending!=-1)
			{
				var link = "<a href=\"#\" onClick=\"ns.cash.redirectCashDisburseDetail('"+ linkVar + "')\" >" +_numItemsPending + "</a>";
			} else {
				var link = "";
			}
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_numItemsPending']:eq("+i + ")").html(link);
		}
		ns.cash.cashAccClassificationForBack=_dateClassification;
		ns.cash.cashAccDateForBack=_accountsDate;
		ns.cash.cashForBack=_accountsDate;
	}
	
	
	ns.cash.redirectCashDisburseDetail = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#cashDisbursedetail').html(data).show();
				$("#criteria").hide();
				$("#transferMoney").hide();
				$("#cashAccDisbursement").hide();
				$("#cashPresentment").hide();
				$("#cashDibursementGridTabs").hide();
				$("#dbCashDibursementSummary a span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
				ns.cash.isDisbursementsByPresentment = false;
		   }
		});

	}
	
	ns.cash.customToCashDisburseDetailColumnForPost = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _numItemsPending = $(gridID).jqGrid('getCell', rowId, 'numItemsPending');
			var _accountID = $(gridID).jqGrid('getCell', rowId, 'account.accountID');
			var _accountsDate = $(gridID).jqGrid('getCell', rowId, 'map.date');
			var _dateClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');
			var _presentment = $(gridID).jqGrid('getCell', rowId, 'map.presentment');
			var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
			var link = "<a href=\"#\" onClick=\"ns.cash.redirectCashDisburseDetail_2('"+ linkVar + "')\" >" +_numItemsPending + "</a>";
				   
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_numItemsPending']:eq("+i + ")").html(link);
		}
		ns.cash.cashForBack=_accountsDate;
	}
	
	ns.cash.redirectCashDisburseDetail_2 = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#cashDisbursedetail').html(data).show();
				$("#criteria").hide();
				$("#transferMoney").hide();
				ns.cash.isDisbursementsByPresentment = true;
		   }
		});
		
	}
	
	ns.cash.customToCashDisburseItemColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _payee = $(gridID).jqGrid('getCell', rowId, 'payee');
			var _accountID = $(gridID).jqGrid('getCell', rowId, 'map.accountID');
			var _accountsDate = $(gridID).jqGrid('getCell', rowId, 'map.date');
			var _dateClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');
			var _presentment = $(gridID).jqGrid('getCell', rowId, 'map.searchCriteriaValue');
			var _transactionIndex = $(gridID).jqGrid('getCell', rowId, 'map.tranIndex');
			var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
			var link = "<a href=\"#\" onClick=\"ns.cash.redirectCashDisburseItem('"+ linkVar + "')\" >" +_payee + "</a>";
				   
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_payee']:eq("+i + ")").html(link);
		}
		
	}
		
	ns.cash.redirectCashDisburseItem = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#cashDisbursedetail').html(data);
		   }
		});
	}

   ns.cash.formatCashLockboxNickNameColumn = function(gridID){
	    
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			//Construct "To Account Nickname" column.
			var _toLockboxRountingNumber = $(gridID).jqGrid('getCell', rowId, 'lockboxAccount.routingNumber');
			var _toLockboxDisplayText = $(gridID).jqGrid('getCell', rowId, 'lockboxAccount.displayText');
			var _toLockboxAccountNickname = $(gridID).jqGrid('getCell', rowId, 'lockboxAccountNickname');
			var _toLockboxAccountCurrencyType = $(gridID).jqGrid('getCell', rowId, 'lockboxAccount.currencyType');
			var _toLockboxName = _toLockboxRountingNumber  + " : " + _toLockboxAccountNickname;

			 
			if( _toLockboxAccountCurrencyType.length > 0 ){
				_toLockboxName = _toLockboxName + " - " + _toLockboxAccountCurrencyType;
			}
			$(gridID).jqGrid('setCell', rowId, 'lockboxAccountNickName', _toLockboxName);
		}
	}
   
   ns.cash.formatCashLockboxAccountColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _toAccountName = "";
			var _toLockboxRountingNumber = $(gridID).jqGrid('getCell', rowId, 'lockboxAccount.routingNumber');
			var _toLockboxDisplayText = $(gridID).jqGrid('getCell', rowId, 'lockboxAccount.displayText');
			var _toLockboxName = _toLockboxRountingNumber;

			if( _toLockboxDisplayText != undefined && _toLockboxDisplayText != "" ){
				_toAccountName =  ""+_toLockboxDisplayText;
			}
			$(gridID).jqGrid('setCell', rowId, 'lockboxAccountName', _toAccountName);
		}
	}
	
	ns.cash.customToCashLockboxColumn = function(cellvalue, options, rowObject){
			var linkVar = rowObject.map.ViewURL;
			return "<a href=\"#\" onClick=\"ns.cash.redirectCashLockboxDoposits('" + linkVar + "')\" >" + cellvalue + "</a>";
	}
		
	ns.cash.redirectCashLockboxDoposits = function(urlString){
		$.ajax({			
			url: urlString,
			success: function(data){	
				$('#LockboxListForAccount').html(data).show();
				$('#search').hide();
				$('#search_pane').hide();
				$('#cashLockboxList').hide();
     			$('#gview_cashlockboxdepositID').show();
				$('#backFormButtonForLockDeposit').show();
				$('#gbox_cashlockboxdepositID').show();
				
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#notes").setFocus();
				}
		   }
		});
	}
	
	ns.cash.customToCashLockboxDetailColumn = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _dateClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');
			var _lockboxAccountID = $(gridID).jqGrid('getCell', rowId, 'accountID');
			var _lockboxNumber = $(gridID).jqGrid('getCell', rowId, 'lockboxNumber');
			var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
			var link = "<a href=\"#\" onClick=\"ns.cash.redirectCashLockboxDetail('"+ linkVar +"')\" >" + _lockboxNumber + "</a>";
				   
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_lockboxNumber']:eq("+i + ")").html(link);
		}
		
		ns.cash.cashLockboxAccID=_lockboxAccountID;		
	}
	
	ns.cash.redirectCashLockboxDetail = function(urlString){		
		$.ajax({
			url: urlString,		
			success: function(data){     
				$('#LockboxListForAccount').html(data);
				$('#search').hide();
				$('#cashLockboxList').hide();
				ns.common.resizeWidthOfGrids();
				
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#notes").setFocus();
				}
		   }
		});
	}
	
	$.subscribe('GoBackToCashLockbox', function(event,data) {
		ns.cash.redirectToCashLockbox("#cashlockboxdepositID");
	});
	
	ns.cash.redirectToCashLockbox = function(gridID){
		ns.cash.cashLockboxDataClassificationForBack=$("#LockboxDataClassification").val();
		ns.cash.cashLockboxDateForBack = $("#LockboxCriteriaDate").val();

		$.ajax({
			url: "/cb/pages/jsp/cash/cashlockbox.jsp?LockboxDataClassification=" + ns.cash.cashLockboxDataClassificationForBack + "&LockboxCriteriaDate=" +ns.cash.cashLockboxDateForBack,				
			success: function(data){   		
				$('#note').html(data).show();
				$('#backFormButtonForLockDeposit').hide();
				$('#gbox_cashlockboxdepositID').hide();
				$('#lockboxDepositIcon_portlet').hide();
				$('#search').show();
				$('#search_pane').show();
				$('#cashLockboxList').show();
				ns.common.resizeWidthOfGrids();
				
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#notes").setFocus();
				}
		   }
		});
		
	}
	
	$.subscribe('GoBackToLockPost', function(event,data) {
		redirectToCashLockPost("#cashlockboxdetailID");
	});
	
	function redirectToCashLockPost(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _accountID = $(gridID).jqGrid('getCell', rowId, 'map.accountID');
			var _dateClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');

			ns.cash.cashLockboxAccID = _accountID; 
		}
		
		$.ajax({

			url: "/cb/pages/jsp/cash/cashlockboxdeposits.jsp?AccountID=" + ns.cash.cashLockboxAccID + "&lockboxtransactions-reload=true&DataClassification=" ,			
			success: function(data){  
				$('#LockboxListForAccount').html(data).show();
				ns.common.resizeWidthOfGrids();
				
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#notes").setFocus();
				}
		   }
		});
		
	}
	
	$.subscribe('GoBackToPresentment', function(event,data) {
		redirectToCashPresentment("#cashdisbursepostdetailID");
	});
	
	function redirectToCashPresentment(gridID){
		$.ajax({
			url: "/cb/pages/jsp/cash/cashdisbursement.jsp?DisbursementDataClassification=" + ns.cash.cashPresentmentClassificationForBack + "&DisbursementCriteriaDate=" + ns.cash.cashPresentmentDateForBack,			
			success: function(data){  
				$("#cashDibursementGridTabs").show();
				$("#cashDisbursementSummaryBtn span").addClass("dashboardSelectedItem");
				$('#cashAccDisbursement').show();
				$('#cashPresentment').show();
				$('#transfer').show();
				$("#criteria").show();
				$('#cashDisbursedetail').hide();
				$('#cashDisbursedetailForAcc').hide();
				ns.common.resizeWidthOfGrids();
				
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#notes").setFocus();
				}
		   }
		});
	}
	
	$.subscribe('GoBackToCashOption', function(event,data) {	
		if (ns.cash.isDisbursementsByPresentment) {
			redirectToCashOption("#cashdisbursedetailID");
			ns.cash.isDisbursementsByPresentment = false;
		} else {
			redirectToCashAccount("#cashdisbursedetailID");
		}		
	});

	function redirectToCashAccount(gridID) {
		$.ajax({					
			url: "/cb/pages/jsp/cash/cashdisbursement.jsp?DisbursementDataClassification=" + ns.cash.cashAccClassificationForBack + "&DisbursementCriteriaDate=" + ns.cash.cashForBack,			
			success: function(data){
			$('#cashPresentment').hide();
			$('#cashDisbursedetail').hide();	
			$('#cashDisbursedetailForAcc').hide();
			$('#cashAccDisbursement').show();
			$("#criteria").show();
			$('#transferMoney').show();	
			$("#cashDibursementGridTabs").show();
			$("#cashDisbursementSummaryBtn span").addClass("dashboardSelectedItem");
			ns.common.resizeWidthOfGrids();
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#notes").setFocus();
			}
			}
		});				
	}
	
	function redirectToCashOption(gridID){	
	    var ids = jQuery(gridID).jqGrid('getDataIDs');
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var _objBackURL = $(gridID).jqGrid('getCell', rowId, 'map.objBackURL');
			var _searchCriteriaValue = $(gridID).jqGrid('getCell', rowId, 'map.searchCriteriaValue');
			var _date = $(gridID).jqGrid('getCell', rowId, 'map.date');
			var _dataClassification = $(gridID).jqGrid('getCell', rowId, 'map.dataClassification');
			ns.common.resizeWidthOfGrids();
		}

		$.ajax({						
			url: "/cb/pages/jsp/cash/cashdisbursepostdetail.jsp?Presentment=" + ns.cash.cashPresentmentPresentForBack + "&Date=" + ns.cash.cashPresentmentDateForBack +"&DataClassification=" + ns.cash.cashPresentmentClassificationForBack,			
			success: function(data){ 
			$('#cashDisbursedetail').html(data).show();
			$('#transferMoney').show();			
			ns.common.resizeWidthOfGrids();		
			
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#notes").setFocus();
			}
			}
		});
	}
	
	$.subscribe('onUploadCancelButton', function(event,data) {
		ns.cash.refreshDashboardForm(ns.cash.originalDashboardFormURL);
		ns.cash.refreshPPayListGrid(ns.cash.refreshPPayListGridURL);
		
		// UPLOAD might change the names of the tabs, so change them back.
        var heading = $('#normalInputTab').html();
        var $title = $('#inputTab a');
        $title.attr('title',heading);
        $title = $('#inputTab a span');
        $title.html(heading);

        heading = $('#normalVerifyTab').html();
        $title = $('#verifyTab a');
        $title.attr('title',heading);
        $title = $('#verifyTab a span');
        $title.html(heading);
		
        //set portlet title
        heading = $('#normalSummaryTab').html();
        $title = $('#pPaysummary .portlet-title');
        $title.html(heading);
        
		$('#pPayList').show();
		$('#pPaysummary').show();
		ns.cash.refreshPPayGrid(ns.cash.refreshPPayGridURL);
	});

	

	$.subscribe('prepareJsonData', function(event,data) {		
		var gridId = "cashflowSummaryID";
		var searchCriteriaBeanNames ={
			"cashflowSummaryID":"cashFlowSearchCriteria"
		};
		var beanToLoad = searchCriteriaBeanNames[gridId];
		var dataFromDashboard = {};
		dataFromDashboard[beanToLoad+".dataClassification"]=function(){return $("#CashFlowDataClassification").val();},
		dataFromDashboard[beanToLoad+".cashFlowCriteriaDate"]=function(){return $("#CashFlowCriteriaDate").val();},
		dataFromDashboard[beanToLoad+".currency"]=function(){return $('#CashDisplayCurrencyCodeID option:selected').val();},
		$("#"+gridId).jqGrid('setGridParam',{postData: dataFromDashboard});
	});

	//Formatter for Check Amount Value
	ns.cash.checkAmountFormatter = function(cellvalue, options, rowObject){
		return ns.cash.Amountformatter(cellvalue, options,rowObject['checkAmount']);
	}
	
	//Formatter for One Days Funds Needed  Amount Value
	ns.cash.oneDayFundsFormatter = function(cellvalue, options, rowObject){
		return ns.cash.Amountformatter(cellvalue, options,rowObject['oneDayFundsNeeded']);
	}
	
	//Formatter for Two Days Funds Needed  Amount Value
	ns.cash.twoDaysFundsFormatter = function(cellvalue, options, rowObject){
		return ns.cash.Amountformatter(cellvalue, options,rowObject['twoDayFundsNeeded']);
	}
		//Formatter for Immediate Funds Needed Amount Value
	ns.cash.immediateFundsFormatter = function(cellvalue, options, rowObject){
		return ns.cash.Amountformatter(cellvalue, options,rowObject['immediateFundsNeeded']);
	}
		//Formatter for Total Debit Amount Value
	ns.cash.totalDebitsFormatter = function(cellvalue, options, rowObject){
		return ns.cash.Amountformatter(cellvalue, options,rowObject['totalDebits']);
	}
	
	ns.cash.Amountformatter = function(cellvalue, options, obj){
		var data=cellvalue;
		if(cellvalue == null || cellvalue == undefined) {
			return "";
		}
		if(obj != null && obj != undefined) {
			var compareData = obj.amountValue;
			if(compareData != null && compareData<0){
				data="<font color='red'>"+data+"</font>";
			}
		}
		return data;
	}
	