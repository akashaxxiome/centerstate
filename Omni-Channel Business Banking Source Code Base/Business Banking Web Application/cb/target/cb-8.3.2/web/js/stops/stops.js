ns.stops.setup = function (){

	ns.stops.gotoStops();

	/*
	 *  Load Stop Payment form
	 */
	$.subscribe('beforeLoadStopPaymentForm', function(event,data) {
		//$.log('About to load form! ');
		$('#details').hide();
		$('#stopquicksearchcriteria').hide();
		ns.common.selectDashboardItem("stopsLink");
	});

	$.subscribe('loadStopPaymentFormComplete', function(event,data) {
		//$.log('Form Loaded! ');
		$('#stopsGridTabs').portlet('fold');
		$('#stopsGridTabs').hide();
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		//var $title = $('#details .portlet-title');
		//$title.html(heading);
		ns.common.updatePortletTitle("detailsPortlet",heading,false);
		
        ns.stops.gotoStep(1);
		$('#details').slideDown();
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#details").setInitialFocus();
		}
	});

	$.subscribe('errorLoadStopPaymentForm', function(event,data) {
		//$.log('Form Loading error! ');

	});


	/*
	 * Verify transfre form
	 */
	$.subscribe('beforeVerifyStopPaymentForm', function(event,data) {
		//$.log('About to verify form! ');
		removeValidationErrors();

	});

	$.subscribe('verifyStopPaymentFormComplete', function(event,data) {
		//$.log('Form Verified! ');
		

	});

	$.subscribe('errorVerifyStopPaymentForm', function(event,data) {
		//$.log('Form Verifying error! ');

	});

	$.subscribe('successVerifyStopPaymentForm', function(event,data) {
		//$.log('Form Verifying success! ');
		ns.stops.gotoStep(2);

	});



	/*
	 * Send Stop Payments form
	 */
	$.subscribe('beforeSendStopPaymentForm', function(event,data) {
		//$.log('About to send transfer! ');

	});

	$.subscribe('sendStopPaymentFormComplete', function(event,data) {
		//$.log('sending transfer complete! ');

	});

	$.subscribe('sendStopPaymentFormSuccess', function(event,data) {
		ns.stops.gotoStep(3);
		ns.common.showStatus($('#stopPaymentResultMessage').html());
	});

	$.subscribe('errorSendStopPaymentForm', function(event,data) {
		//$.log('transfer error! ');

	});


	$.subscribe('quickSearchStopsComplete', function(event,data) {
        ns.stops.reloadStopsSummaryGrid();
        $('#details').hide();
		$('#stopsGridTabs').portlet('expand');
	});

	$.subscribe('quickSearchStopsBefore', function(event,data) {
        if ($("#dateRangeValue").val() == null || $("#dateRangeValue").val() == "") {
			if ($("#StartDateID").val() == null || $("#StartDateID").val() == "") {
				$("#StartDateID").val($("#StartDateSessionValue").val());
			}
			if ($("#EndDateID").val() == null || $("#EndDateID").val() == "") {
				$("#EndDateID").val($("#EndDateSessionValue").val());
			}
		}
	});
	
	/*
	 * Delete Stop Payment
	 */
	$.subscribe('beforeDeleteTransfer', function(event,data){
		//$.log('beforeDeleteTransfer');
	});

	$.subscribe('errorDeleteTransfer', function(event,data){
		//$.log('errorDeleteTransfer');
		ns.common.showStatus();
		$('#innerdeletetransferdialog').dialog('close');
	});

	$.subscribe('deleteTransferComplete', function(event,data){
		//$.log('deleteTransferComplete');
	});

	$.subscribe('successDeleteTransfer', function(event,data){
		//$.log('successDeleteTransfer');
		ns.common.showStatus();
		$('#innerdeletetransferdialog').dialog('close');
		ns.stops.closeStopPaymentTabs();
	});

	$.subscribe('cancelPaymentStopForm', function(event,data) {
		ns.stops.closeStopPaymentTabs();
		ns.common.hideStatus();
		$('#stopsGridTabs').portlet('expand');
		$('#stopsGridTabs').show();
		ns.common.refreshDashboard('dbStopSummary');
	});

	$.subscribe('donePaymentStopForm', function(event,data) {

		//$.log('About to cancel Form! ');
		$('#completedStopsGrid').trigger("reloadGrid");
		ns.stops.closeStopPaymentTabs();
		ns.common.hideStatus();

		$.publish('refreshMessageSmallPanel');
		$('#stopsGridTabs').portlet('expand');
		$('#stopsGridTabs').show();
		$('#details').slideUp();
		ns.common.refreshDashboard('showdbStopSummary',true);
	});


	$.subscribe('stops_backToInput', function(event,data) {

		//$.log('About to back to input! ');
		ns.stops.gotoStep(1);
	});

	$.subscribe('summaryLoadFunction', function(event,data){
		//$('#goBackSummaryLink').hide();
		$('#details').slideUp();	
		/*$('#stopquicksearchcriteria').show();*/
		$('#stopsGridTabs').portlet('expand');
		$('#stopsGridTabs').show();
	});
	
};

ns.stops.gotoStops = function (){
	$('#details').hide();
	$('#stopsquickSearchLink').show();

};

ns.stops.gotoStep = function (stepnum){
	
	ns.common.gotoWizardStep('detailsPortlet',stepnum);
	
	var tabindex = stepnum - 1;
	$('#StopsTransactionWizardTabs').tabs( 'enable', tabindex );
	$('#StopsTransactionWizardTabs').tabs( 'option','active', tabindex );
	$('#StopsTransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
	$('#StopsTransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );

	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		//$('#StopsTransactionWizardTabs').setFocusOnTab();	
		var selectedTab = $("#StopsTransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
		$("#"+selectedTab).find(":tabbable:first").focus();	
	}	
};

/**
 * Close Details island and expand Summary island
 */
ns.stops.closeStopPaymentTabs = function (){
	$('#stopsGridTabs').portlet('expand');
	$('#details').slideUp();
};

ns.stops.showStopDashboard= function(stopsUserActionType,stopsDashboardElementId){
	if(stopsUserActionType == 'Stops'){
		ns.common.refreshDashboard('dbStopSummary');
	}
	//Simulate dashboard item click event if any additional action is required
	if(stopsDashboardElementId && stopsDashboardElementId !='' && $('#'+stopsDashboardElementId).length > 0){
		//$("#"+transferDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+stopsDashboardElementId).trigger("click");}, 10);
	}
}
// Topic to clear the quick search criteria fields and removing the validation errors
$.subscribe('onClickQuickSearchStopsFormTopics', function(event,data) {
	
	//	QTS 715577. 
	ns.stops.closeStopPaymentTabs();
	ns.common.hideStatus();
	// .toggle doesn't seem to work in Internet Explorer
	// see http://www.code-styling.de/english/jquery-132-causes-problems-at-ie-8
	//but this works
	$('#stopquicksearchcriteria').toggle($('#stopquicksearchcriteria').css('display')=='none');
	
	// Clears quick search criteria fields excluding from and to date
	removeValidationErrors();
	$("#StartDateID").val($("#StartDateSessionValue").val());
	$("#EndDateID").val($("#EndDateSessionValue").val());
	$("#dateRangeValue").selectmenu('value',"");
	
	if($("#AccountsName").data("ui-selectmenu") != undefined){
		$("#AccountsName").selectmenu('value',"");
	}
	
	if($("#PayeeName").data("ui-selectmenu") != undefined){
		$("#PayeeName").selectmenu('value',"");
	}
	
	$("#Amount").val("");
});

$(document).ready(function(){
	$.debug(false);
	ns.stops.setup();
});

