(function($) {
	$.widget("ui.portlet", {
		options: {
			generateDOM: false,			//if true, generate portlet header and outer container automatically
			title: '',					//title displayed in portlet header
			minmax: false,				//whether to display min/max icon
			close: false,				//whether to display Close icon
			edit: false,				//whether to display Edit icon
			help: false,					//whether to display Help icon
			settings:false,				//whether to display Settings icon
			bookmark: false,			//whether to display Bookmark icon
			helpCallback: '',			//function to be called when clicking on Help icon
			settingsCallback: '',		//function to be called when clicking on Settings icon
			closeCallback: '',			//function to be called when clicking on Close icon - will override default behaviour
			removeOnClose: false,		//whether to remove this portlet from DOM when close
			viewCallback: '',			//function to be called when clicking on View icon
			editCallback: '',			//function to be called when clicking on Edit icon
			bookMarkCallback: '',		//function to be called when clicking on Bookmark icon
			duration: 'slow',			//duration will be applied to animation for UI state change of portlets
			highlight: false, 			//highlight the portlet by adding surrounding border 
			resizeTitle: false,         //if true, portlet title length will be changed automatically when window is resized
			sortableHeaderClass: '', 	//style class applied to header of portlets supporting drag and drop
			sortableOptions: {}
		},    	
    	
        _create: function() {
            var self = this,
                portlet_classes = "portlet ui-widget ui-widget-content ui-corner-all innerPortletsContainer",
                portlet_header_classes = "portlet-header ui-widget-header ui-corner-all",
                portlet_content_classes = "portlet-content",
                portlet_title_classes = "portlet-title",
                DOT = ".",
                HEADER_CLS = "portlet-header",
                CONTENT_CLS = "portlet-content",
                CLOSE_CLS = "ui-icon ui-icon-close close",
                HELP_CLS = "ui-icon ui-icon-help help",
                MINUS_CLS = "ui-icon ui-icon-minus shrink",
            	EDIT_CLS = "ui-icon ui-icon-wrench edit",
              	VIEW_CLS = "ui-icon ui-icon-arrowreturnthick-1-w view",
              	BOOKMARK_CLS = "ui-icon ui-icon-bookmark bookmark",
              	PORTLET_ID_SUFFIX = "_portlet";


            this.expanded = true;
            this.shown = true;
            
            this.el = $(this.element); //this.el will finally point to the container element
            if(this.options.generateDOM) { //generate portlet header and outer container automatically
            	var container = $("<div/>").insertBefore(this.el).addClass(portlet_classes).attr('id', this.el.attr('id') + PORTLET_ID_SUFFIX);
				this.el.prependTo(container).addClass(portlet_content_classes);
				
				var title=$("<span/>");
				$("<div/>").prependTo(container).append(title).addClass(portlet_header_classes);
            	title.attr('style','position:absolute;').addClass(portlet_title_classes).html(this.options.title).attr('title',this.options.title);
            	
            	this.el = container;
            }

            this.hd = this.el.find(DOT + HEADER_CLS);
            this.ct = this.el.find(DOT + CONTENT_CLS);

            //QTS:660515 - This is to handle the case where we applied portlet widget on a jQuery tabbedpanel.
            //When user changed the folding/expanding status in one tab, then click on another tab, the fold/expand
            //icon may become insync with the content area.
            var $tabs = this.hd.find('li>a');
            if($tabs.length > 1){ //no issue when there is only one tab
	            $tabs.click(function(){
	            	var $visibleContent = $(this).parents(".portlet:first").children(".ui-widget-content:visible");
	            	if($visibleContent.length == 0)
	            		$visibleContent = $(this).parents(".portlet:first").children(".portlet-content:visible");
	            	if($visibleContent.length > 0)
	            		self.hd.find('.shrink').addClass("ui-icon-minus").removeClass("ui-icon-plus");
	            	else
	            		self.hd.find('.shrink').removeClass("ui-icon-minus").addClass("ui-icon-plus");
	            });
            }//QTS:660515 ends
            
            this.el.addClass(portlet_classes);
            this.hd.addClass(portlet_header_classes);
            this.ct.addClass(portlet_content_classes);
            
            //Add a container to hold custom span. 
            this.customContainer = $('<span/>').appendTo(this.hd).attr('class',"customContainerCls").hide();
            
            //Add a container to hold custom span. 
            this.calendarContainer = $('<span/>').appendTo(this.hd).attr('class',"calendarContainerCls").hide();
            
            //Add a container to hold custom span. 
            this.wizardContainer = $('<span/>').appendTo(this.hd).attr('class',"wizardStatusContainerCls").hide();
            
            //Add a container to hold all header controls
            this.hdctl = $('<span/>').appendTo(this.hd).attr('class',"headerctls").hide();
            
        	//display/hide header controls and adjust title width
			var hdctlTimer = null;
            this.el.mouseenter(function() {
				hdctlTimer = setTimeout(function(){self.hdctl.fadeIn('fast');},200);
				self.setTitleWidth($(this).width()-(self.hdctl.find('.ctllink').size()*18+25));
			});
        	this.el.mouseleave(function() {
				if(hdctlTimer){clearTimeout(hdctlTimer);}
				self.hdctl.fadeOut();
				self.setTitleWidth();
        	});
			
			if(this.options.resizeTitle){
				$(window).bind('resize.'+this.el.attr('id'),function(){
					var setMyWidth= function(){self.setTitleWidth();};
					setTimeout(setMyWidth,500);
					if(self.element.find('.visualize').length > 0){
						self.resizeChart();
					}
				});
			}

            
            //Add bookmark button to add shortcut for a specific function
            if(this.options.bookmark){

	            var bookMarkHandler = this.options.bookMarkCallback;
	        	if ($.isFunction(bookMarkHandler)) {

					var shortcut = $('<span/>');
					
					$("<a/>").prependTo(this.hdctl).append(shortcut).attr('class','ctllink ui-corner-all').attr('title',js_shotcut_bookmarkThisPage).attr('href','javascript:void(0)')
	            	.hover(function(){
	            		$(this).toggleClass('ui-state-highlight');
	            	});
					shortcut.addClass(BOOKMARK_CLS);

					var portlet = $(this.element);
					
					this.hd.find('.bookmark').bind('click', function(event) {
						bookMarkHandler(event, portlet);
					});
	            }
            };

            
            //edit button
            if(this.options.edit){
	            var edit = $('<span/>');
				
				$("<a/>").prependTo(this.hdctl).append(edit).attr('class','ctllink ui-corner-all').attr('name','editButton').attr('href','javascript:void(0)')
            	.hover(function(){
            		$(this).toggleClass('ui-state-highlight');
            	});
				edit.addClass(EDIT_CLS);
	            
	            var container = this.el;
	            var portlet = $(this.element);
	            var header = this.hd;
	            var editHandler = this.options.editCallback;
	            if ($.isFunction(editHandler)) {
		            this.hd.find('a[name="editButton"]').bind('click', function(event) {
		            	$(this).hide();
		            	container.find('a[name="viewButton"]').show();
		            	var isHidden = container.find('.portlet-content').is(":hidden");
		            	if(isHidden) {
		            		header.find('.shrink').removeClass("ui-icon-plus").addClass("ui-icon-minus");
		            	}
		            	editHandler(event, portlet, isHidden);
		            });
	            	
	            };
            };

            //view button
            if(this.options.edit){
            	var view = $('<span/>');
				$("<a/>").prependTo(this.hdctl).append(view).attr('class','ctllink ui-corner-all').attr('name','viewButton').attr('href','javascript:void(0)').hide()
	           	.hover(function(){
	        		$(this).toggleClass('ui-state-highlight');
	        	});
				view.addClass(VIEW_CLS);

	            var container = this.el;
	            var portlet = $(this.element);
	            var header = this.hd;
	            var viewHandler = this.options.viewCallback;
	            if ($.isFunction(viewHandler)) {
		            this.hd.find('a[name="viewButton"]').bind('click', function(event) {
		            	$(this).hide();
		            	container.find('a[name="editButton"]').show();
		            	var isHidden = container.find('.portlet-content').is(":hidden");
		            	if(isHidden) {
		            		header.find('.shrink').removeClass("ui-icon-plus").addClass("ui-icon-minus");
		            	}
		            	viewHandler(event, portlet, isHidden);		            	
		            });
	            };
            };

            //minimize/maximize button
            if(this.options.minmax){
            	var duration = this.options.duration;
            	var minus = $('<span/>');
				
				$("<a/>").prependTo(this.hdctl).append(minus).attr('class','ctllink ui-corner-all').attr('href','javascript:void(0)')
            	.hover(function(){
            		$(this).toggleClass('ui-state-highlight');
            	});
				minus.addClass(MINUS_CLS);

	            this.hd.find('.shrink').bind('click', function(event) {
	                $(this).toggleClass("ui-icon-minus").toggleClass("ui-icon-plus");
	                $portletContent = $(this).parents(".portlet:first").find(".portlet-content");
	                if($portletContent.length > 0){
	                	//fix applied with jquery upgrade, in case of tabs expand only selected tab
	                	$portletContent = $(this).parents(".portlet:first").find(".portlet-content[aria-expanded='true']");
	                	if($portletContent.length > 0){
	                		$portletContent.slideToggle(duration, function(){self.resizeGrids()});
	                	}else{
	                		$portletContent = $(this).parents(".portlet:first").find(".portlet-content");
	                		$portletContent.slideToggle(duration, function(){self.resizeGrids()});
	                	}	                	
	                } else {
	                	//This is used where we want to apply portlet controls to other jquery ui
	                	//widget, such as tabbedpanel. In that case it's not practical to add the 
	                	//portlet-content class to its content areas. 
	                	$(this).parents(".portlet:first").children("div.ui-widget-content[aria-expanded='true']").slideToggle(duration, function(){self.resizeGrids()});
	                }
	                this.expanded = !this.expanded;
	            });
            };
            
            //close button
            if(this.options.close){
            	var close = $('<span/>');
				
				$("<a/>").prependTo(this.hdctl).append(close).attr('class','ctllink ui-corner-all').attr('href','javascript:void(0)')
            	.hover(function(){
            		$(this).toggleClass('ui-state-highlight');
            	});
				close.addClass(CLOSE_CLS);

	            var closeHandler = this.options.closeCallback;
	            var duration = this.options.duration;
	            var removeOnClose = this.options.removeOnClose;
	            var container = this.el;
	            var portlet = $(this.element);
	            
		        this.hd.find('.close').bind('click', function(event) {
		        	if ($.isFunction(closeHandler)) {
		        		closeHandler(event, portlet);
		            }else {
		        		container.slideUp(duration, function(){
				            if(removeOnClose) {
				            	$(this).remove();
					        }
				        });
		        	}
	            });
            };
            
            //help button
            if(this.options.help){
	            var helpHandler = this.options.helpCallback;
	        	if ($.isFunction(helpHandler)) {

					var help = $('<span/>');
					$("<a/>").prependTo(this.hdctl).append(help).attr('class','ctllink ui-corner-all').attr('href','javascript:void(0)')
	            	.hover(function(){
	            		$(this).toggleClass('ui-state-highlight');
	            	});
					help.addClass(HELP_CLS);

					var portlet = $(this.element);
					
					this.hd.find('.help').bind('click', function(event) {
							helpHandler(event, portlet);
					});
	            }
            };
            
            if(this.options.highlight) {
            	this.el.mouseenter(function() {
    				$(this).addClass("portlet-border-highlight");
    			 });
    				
            	this.el.mouseleave(function() {
    				$(this).removeClass("portlet-border-highlight");
    			 });
            }
            if(this.options.sortableHeaderClass) {
            	var sortableHeaderClass = this.options.sortableHeaderClass;
            	this.el.find(".portlet-header").mouseout(function() {
 				   $(this).removeClass(sortableHeaderClass);
 				});
 			 
            	this.el.find(".portlet-header").mouseover(function() {
 				   $(this).addClass(sortableHeaderClass);
 				});
            }
        },
		
		destroy: function() {
			if($(this).data('ui-portlet') != undefined){
				$(this).portlet('unbindHoverToTable');
			}
			
			if(this.options.resizeTitle) {$(window).unbind('resize.'+this.el.attr('id'));}
			
			this.hdctl.children('.ctllink').each(function(i, alink){
				var $icon = $(alink).children('.ui-icon');
				if ($icon) $icon.unbind();
				$(alink).unbind();
				$(alink).remove();
			});
			
			if(this.options.sortableHeaderClass) {
            	this.el.find(".portlet-header").unbind();
            }
			
			if (this.options.generateDOM) {
				$(this.element).removeClass('portlet-content');
				$(this.element).insertAfter(this.el);
				this.el.unbind();
				this.el.remove();
			}
			$.Widget.prototype.destroy.apply(this,arguments);
			this.hdctl = null;
			this.hd=null;
			this.element=null;
			this.el = null;
			
		},
		
        title : function(title){
        	var $titleEle = $(this.element).parents(".portlet:first").find('.portlet-title');
			if (!title) 
				return $titleEle.html();
			$titleEle.html(title).attr('title', title);
        },

        fold : function(event) {
        	var o = this.options;
        	var duration = o.duration;
        	if(o.generateDOM){
        		$(this.element).parents(".portlet:first").find('.portlet-content').slideUp();
        		$(this.element).parents(".portlet:first").find('.shrink').removeClass("ui-icon-minus").addClass("ui-icon-plus");
        	} else {
                $portletContent = $(this.element).find(".portlet-content");
                if($portletContent.length > 0){
                	$portletContent.slideUp(duration);
                } else {
                	//This is used where we want to apply portlet controls to other jquery ui
                	//widget, such as tabbedpanel. In that case it's not practical to add the 
                	//portlet-content class to its content areas. 
                	$(this.element).children(".ui-widget-content").slideUp(duration);
                };
        		$(this.element).find('.shrink').removeClass("ui-icon-minus").addClass("ui-icon-plus");
        	};
        	this.expanded = false;
        },

        expand : function(event) {
			var self = this;
        	var o = this.options;
        	var duration = o.duration;
        	if(o.generateDOM){
        		$(this.element).parents(".portlet:first").find('.portlet-content').slideDown(duration);
        		$(this.element).parents(".portlet:first").find('.shrink').addClass("ui-icon-minus").removeClass("ui-icon-plus");
        	} else {
                $portletContent = $(this.element).find(".portlet-content");
                if($portletContent.length > 0){
                	//fix applied with jquery upgrade, in case of tabs expand only selected tab
                	$portletContent = $(this.element).find(".portlet-content[aria-expanded='true']");
                	if($portletContent.length > 0){
                		$portletContent.slideDown(duration,function(){self.resizeGrids()});
                	}else{
                		$portletContent = $(this.element).find(".portlet-content");
                		$portletContent.slideDown(duration,function(){self.resizeGrids()});
                	}
                	
                } else {
                	//This is used where we want to apply portlet controls to other jquery ui
                	//widget, such as tabbedpanel. In that case it's not practical to add the 
                	//portlet-content class to its content areas. 
                	$(this.element).children("div.ui-widget-content[aria-expanded='true']").slideDown(duration,function(){self.resizeGrids()});
                };
        		$(this.element).find('.shrink').addClass("ui-icon-minus").removeClass("ui-icon-plus");
        	};
        	this.expanded = true;
         },

        hide : function(event) {
        	$(this.element).parents(".portlet:first").hide();
        	this.shown = false;
        },
        
		show : function(event) {
			$(this.element).parents(".portlet:first").show();
			this.shown = true;
		},
		
		isShown: function(event) {
			return this.shown;
		},
		
		isExpanded: function(event) {
			return this.expanded;
		},
		
		
		clear : function(elementId) {
			if (!elementId) {
				$(this.element).html('');
			} else {
				$(this.element).find('#' + elementId).html('');
			}
		},

		container: function() {
			return $(this.element).parents(".portlet:first");
		},
		
		edit: function(){
			this.hd.find('a[name="editButton"]').trigger('click');
		},
		
		addCtrlIcon: function($icon, handler) {
			var button = $('<a/>').appendTo(this.hd.find('.headerctls')).append($icon).attr('class',"ctllink ui-corner-all").attr('href',"javascript:void(0)").hover(function(){
				$(this).toggleClass('ui-state-highlight');
			})
			
			if(handler) {
				$icon.bind('click', function(event) {
			        handler($(this.element));
			    });
			}
			
		},
		
		addCustomContainer: function($icon){
			this.hd.find('.customContainerCls').append($icon);
			this.hd.find('.customContainerCls').show();
		},
		
		addCalendarContainer: function($icon, handler){
			this.hd.find('.calendarContainerCls').append($icon);
			this.hd.find('.calendarContainerCls').show();
			
			if(handler) {
				$icon.bind('click', function(event) {
			        handler($(this.element));
			    });
			}
		},
		
		//Commonly used 3 step wizard status  
		addWizardStatusContainer: function(){
			this.el = $(this.element);
			var portletID = this.el.attr('id');
			var portletWizardStatusId = portletID + "WizardStatusId" ;
			var $portletWizardStatus = $('<span/>').attr('id', portletWizardStatusId).addClass("wizardStepIndicatorHolder");
			var wizardStepIndicator = "<ul class='wizardProgressIndicatorCls'><li id='__wizardStep1'><span>"+js_input+"</span></li><li id='__wizardStep2'><span>"+js_verify+"</span></li><li id='__wizardStep3'><span>"+js_confirm+"</span></li></ul>";
			$($portletWizardStatus).html(wizardStepIndicator);
			this.hd.find('.wizardStatusContainerCls').append($portletWizardStatus);
			this.hd.find('.wizardStatusContainerCls').show();
		},
		
		//User defined custom wizard status
		addCustomWizardStatusContainer: function($icon){
			this.hd.find('.wizardStatusContainerCls').append($icon);
			this.hd.find('.wizardStatusContainerCls').show();
		},
		
		bindCtrlEvent: function(classId, event, handler){
			this.hd.find(classId).parents('a:first').bind(event, handler);
		},
		
		removeCtrlIcon: function(classId) {
			this.hd.find(classId).parents('a:first').remove();
		},

		hideCtrlIcon: function(classId) {
			this.hd.find(classId).parents('a:first').hide();
		},
		
		showCtrlIcon: function(classId) {
			this.hd.find(classId).parents('a:first').show();
		},
		
		resizeGrids: function(){
			var $grids = $(this.element).find('.ui-jqgrid:visible');
			$grids.each(function(){
				//4 is padding-left + padding-right defined by ui-widget-content in desktop.css
				var grid_width = $(this).parent().width() - 4;
				var $grid = $(this).find(".ui-jqgrid-bdiv .ui-jqgrid-btable");
				$grid.jqGrid('setGridWidth', grid_width, true);
				
			});
		},
		//qts677209
		resizeChart: function(){
			var chartId = $(this.element).find("[id$='_chart_data']").attr("id");
			var chart_title = $(this.element).find('.visualize-title').html();
			var temp = $(this.element).find('.visualize').attr('class').split('-');
			var chart_type = temp[temp.length-1];
			var chart_width = $(this.element).width() - 107;
			if(chart_type == 'pie'){
				var chart_width = $(this.element).width() - 87;
			}
			$(this.element).find('.visualize').remove();
			$('#'+chartId).visualize({type: chart_type, height: '150px', width: chart_width, title: chart_title});
			if(chart_type == 'bar'){
				$(this.element).find('.visualize-key').hide();
			}
		},
		
		setTitleWidth: function(width){
			if (!width){
				width=$(this.el).width()-25;
			}
			$(this.el).find(".portlet-title").css({"width":width+"px"});
		},
		
		bindHoverToTable: function(){
			var self = $(this.element);
			$.each(this.el.find('.ui-jqgrid-btable'), function(i, table){
				$(table).bind('mouseenter.portlet',function(event) {
					var tempId = $(event.relatedTarget).parents('.portlet:first').attr('id');
					if (tempId !== self.portlet('container').attr('id')){
							self.portlet('container').trigger('mouseenter');
					}
				});
				$(table).bind('mouseleave.portlet',function(event) {
					var tempId = $(event.relatedTarget).parents('.portlet:first').attr('id');
					if (tempId !== self.portlet('container').attr('id')){
						self.portlet('container').trigger('mouseleave');
					}
				});
			});
		},
		
		unbindHoverToTable: function(){
			$.each(this.el.find('.ui-jqgrid-btable'), function(i, table){
				$(table).unbind('mouseenter.portlet');
				$(table).unbind('mouseleave.portlet');
			});
		}
    });

})(jQuery); 
