ns.contactpoints.setup = function(){
	//console.info("setting up for contact points");
	ns.contactpoints.initView();
	ns.contactpoints.registerEvents();		
};

ns.contactpoints.initView = function(){
	/*$('#newContactPointPortlet').portlet({
		title: js.contactpoints.addCPPortlet.title,
		bookmark: true,
		generateDOM: true,
		close: true,
		closeCallback: function(){
			$("#newContactPointPortlet_portlet").slideUp("slow");
		},
		helpCallback: function(){
			var helpFile = $('#newContactPointPortlet').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){
			var path = $('#newContactPointPortlet').find('.shortcutPathClass').html();
			var ent  = $('#newContactPointPortlet').find('.shortcutEntitlementClass').html();
			ns.shortcut.openShortcutWindow( path, ent );
		}
	});	*/
	
	$('#contactPointType').selectmenu({width:150});
	$("#newContactPointPortlet_portlet").hide();
	
};

ns.contactpoints.gridOnComplete = function(){	
	//console.info("contactPointsGridOnComplete");	
	var rowsIds = $("#contactPointsGrid").jqGrid("getDataIDs");
	if(rowsIds){
		$.each(rowsIds,function(i,rowsId){
			ns.contactpoints.gridOnSelectRow(rowsId,true);			
		});
		
		var records = rowsIds.length;
		$("#records").val(records);
		ns.contactpoints.addErrorLabels(records);

	}
	
	//disabling center pager since right now there is not any supported attribute to disable center pager buttons
	$("#contactPointsGrid_pager_center").hide();
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#contactPointsGrid").setInitialFocus();
	}
	
};

ns.contactpoints.addErrorLabels = function(records){
	//console.info("ns.contactpoints.addErrorLabels");
	for(i=1;i<=records;i++){

		var nameErrElement = "<span id='Name" + i + "Error' class='errorLabel dashboardLabelMargin'></span>";
		var addressErrElement = "<span id='Address" + i + "Error' class='errorLabel dashboardLabelMargin'></span>";
		var typeErrElement = "<span id='Type" + i + "Error' class='errorLabel dashboardLabelMargin'></span>";
		
		var nameSelector = "input[name=Name" + i + "]"; 
		var typeSelector = "select[name=Type" + i + "]";
		var addressSelector = "input[name=Address" + i + "]";
		
		$(nameErrElement).appendTo($(nameSelector).parent());
		$(typeErrElement).appendTo($(typeSelector).parent());
		$(addressErrElement).appendTo($(addressSelector).parent());
	}
};

ns.contactpoints.contactPointLinkClickHandler = function(e){
	
	$('#cpControlBox').show();
	$('#quicksearchcriteria').hide();
	$('#deleteInboxAlertsLink').hide();
	
	$('#alerts_main_summary').hide();	
	$("#alertsMainSummaryContainer").hide();
	$("#alertsGridContainer").hide();
	$("#details").hide();
	$("#newContactPointPortlet").portlet('fold');
	$("#newContactPointPortlet").hide();
	

	ns.contactpoints.getContactPoints();
	ns.contactpoints.initDeviceGridView();

};

ns.contactpoints.mobileDeviceLinkClickHandler = function(e){
	/*$('#mobileControlBox').show();
	$('#cpControlBox').hide();
	$('#quicksearchcriteria').hide();
	$('#deleteInboxAlertsLink').hide();
	
	$('#alerts_main_summary').hide();	
	$("#alertsMainSummaryContainer").hide();
	$("#alertsGridContainer").hide();
	$("#details").hide();*/
	//alert(112);
	$('#alerts_main_summary').hide();	
	$("#alertsMainSummaryContainer").hide();
	$("#alertsGridContainer").hide();
	$("#details").hide();
	
	$("#contactPointsPortlet").hide();
	$("#newContactPointPortlet").hide();
	
	$("#alertTabs").hide();	
	$("#summary").show();
	$('#mobileControlBox').show();	
	ns.contactpoints.loadMobileDevicesView();	
	ns.common.refreshDashboard('dbMobileDevicesSummary');
};

ns.contactpoints.onEditfunc = function(rowId){
	
};

ns.contactpoints.gridOnSelectRow = function(rowId,status){
	//console.info("ns.contactpoints.gridOnSelectRow");
	$("#contactPointsGrid").jqGrid('editRow',rowId,ns.contactpoints.onEditfunc);
	$("#" + rowId + "_name").addClass("txtbox ui-widget-content ui-corner-all");	
	$("#" + rowId + "_contactPointType").selectmenu({width:100});
	$("#" + rowId + "_address").addClass("txtbox ui-widget-content ui-corner-all");
	
	$("#" + rowId + "_contactPointID").attr("name", "ID" +  rowId);
	$("#" + rowId + "_name").attr("name","Name" +  rowId);
	$("#" + rowId + "_contactPointType").attr("name", "Type" +  rowId);
	$("#" + rowId + "_address").attr("name", "Address" +  rowId);
	$("#" + rowId + "_secure").attr("name", "Secure" +  rowId);
	
};


ns.contactpoints.addNewCPClickHandler = function(e){
	$("#newContactPointPortlet").portlet('expand');
	$("#newContactPointPortlet").show();
	$("#newContactPointPortlet_portlet").slideDown();

	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#addNewCPForm").setInitialFocus();
	}
	$("#contactPointsPortlet").portlet('fold');
	$("#contactPointsPortlet").hide();
	$("#summary").hide();
	ns.common.selectDashboardItem("addNewCPLink");
};

ns.contactpoints.submitContactPointHandler = function(e){
	//console.info("TODO:submitContactPointHandler");
	ns.contactpoints.createUpdateContactPoint("create");
};

ns.contactpoints.cancelCPClickHandler = function(e){
	//console.info("cancelCPClickHandler");
	$("#newContactPointPortlet").hide();
	$("#newContactPointPortlet_portlet").slideUp();
	ns.contactpoints.clearFormElements(["#name","#contactPointType","#address"]);
	$(".errorLabel").html("");
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		accessibility.setFocusOnDashboard();
	}
	$("#contactPointsPortlet").portlet('expand').show();
	$("#summary").show();
	//ns.common.selectDashboardItem("contactPointsLink")
};

$.subscribe('cancelNewCp', function(event,data) {
	ns.common.selectDashboardItem("contactPointsLink");
	ns.common.refreshDashboard('showdbContactPointsSummary',true);
});
/*
ns.contactpoints.saveCPLinkHandler = function(e){
	//console.info("Need to implement save");
	ns.contactpoints.createUpdateContactPoint("save");
};
*/


ns.contactpoints.deleteCPLinkHandler = function(e){
	//console.info("Need to implement delete");
};


ns.contactpoints.initGrid = function(){	
	//console.info("init grid" + $("#contactPointsGrid").jqGrid());
	$("#contactPointsGrid").jqGrid("setGridParam", {		
		//gridComplete:ns.contactpoints.gridOnComplete,	
	    onSelectRow: ns.contactpoints.gridOnSelectRow,	    
	});
	
	//$("#saveCPBtn").click(ns.contactpoints.saveCPLinkHandler);
};

$.subscribe('ns.contactpoints.gridOnComplete', function(event,data) {
	ns.contactpoints.gridOnComplete();
});

ns.contactpoints.initDeleteDialog = function(){
	//console.info("initDeleteDialog");
	var cancelDeleteHandler = function(){
		$("#deleteCPDialog").dialog('close');
		$("#deleteCPDialogContainer").removeData("cp");
	}
	$("#cancelDeleteCPBtn").click(cancelDeleteHandler);
	
	var deleteCPHandler = function(){
		//console.info("deleting after confirmation..");   
		
		var contactPointId = $("#deleteCPDialogContainer").data("cp");
		//console.info("deleting ..... cp" +contactPointId);   
		
		if(contactPointId){
			ns.contactpoints.deleteContactPoint(contactPointId);			
		}		
		cancelDeleteHandler();
	}
	$("#deleteCPBtn").click(deleteCPHandler);
};


ns.contactpoints.openDeleteDialog = function(contactPointId){
	//console.info("openDeleteDialog");
	//$("#deleteCPDialogContainer").data("cp",contactPointId);
	$("#deleteCPDialog").dialog('open');
};

ns.contactpoints.registerEvents = function(){ 
	$("#contactPointsLink").click(ns.contactpoints.contactPointLinkClickHandler);
	$("#addNewCPLink").click(ns.contactpoints.addNewCPClickHandler);	
	$("#submitAddNewContactPoint").click(ns.contactpoints.submitContactPointHandler);	
	$("#cancelAddNewContactPoint").click(ns.contactpoints.cancelCPClickHandler);		
	$("#deleteCPLink").click(ns.contactpoints.deleteCPLinkHandler);
	
	//Mobile device events
	$("#mobileDevicesLink").click(ns.contactpoints.mobileDeviceLinkClickHandler);
	$("#addNewDeviceLink").click(ns.contactpoints.addNewDeviceClickHandler);	
	
	$.subscribe('removeErrorsOnBegin', function(event, data) {
		$('.errorLabel').html('').removeClass('errorLabel');
		$('#formerrors').html('');
	});
	
	$.subscribe('loadContactPoints', function(event,data) {
		//ns.common.refreshDashboard('dbContactPointsSummary');
	});
	$.subscribe('beforeAddingNewCP', function(event,data) {
		$.publish('removeErrorsOnBegin');
	});
	
	$.subscribe('addingNewCPComplete', function(event,data) {
		//$('#contactPointsGrid').trigger("reloadGrid");		
	});
	
	$.subscribe('errorCreatingNewCP', function(event,data) {
		//alert('errorSend');
		
	});

	$.subscribe('successCreatingCP', function(event,data) {
		
		ns.contactpoints.cancelCPClickHandler();
		ns.common.showSummaryOnSuccess("summary","done");
		ns.common.selectDashboardItem('contactPointsLink');

	});
	
	
	$.subscribe('successUpdatingCP', function(event,data) {
		$('#contactPointsGrid').trigger("reloadGrid");
	});
	
	$.subscribe('useralerts.closeAddNewCPForm',ns.contactpoints.cancelCPClickHandler);

};

ns.contactpoints.getContactPoints = function(){
	//console.info("-getContactPoints");
	var URL = "/cb/pages/jsp/alert/contactPointsInit.action";
	$.ajax({   
		  url: URL,   
		  success: function(data) {
			$('#summary').slideDown();
			$('#summary').html(data);			
			ns.contactpoints.initGrid();
		  }   
	});
};

/**
* This function initializes the device management grid view
*/
ns.contactpoints.initDeviceGridView = function(){

}

ns.contactpoints.getGridData = function(){
	var rowsIds = $("#contactPointsGrid").jqGrid("getDataIDs");
	var gridData;
	if(rowsIds){
		$.each(rowsIds,function(i,rowId){
				 var rowData = $("#contactPointsGrid").jqGrid('getRowData',rowId);
				 var address = $("#1_address").val();
				 var name = $("#1_name").val(); 
				 var contactPointType = $("#1_contactPointType").val();
				 var contactPointId = rowData.contactPointID;
				 var secure = rowData.secure;
				 var tempStr = "&ID" + rowId + "=" + contactPointId + "&Name_" + rowId + "=" + name + "&Type_" + rowId + "=" + contactPointType + "&Address_" + rowId + "=" + address + "&Secure_" + rowId + "=" + secure; 
				gridData = (gridData)? (gridData + tempStr):tempStr;			 
		});
	}
	return gridData;
}


ns.contactpoints.createUpdateContactPoint = function(operation){
	//console.info("This will add new contact point");
	var formURL,formData,message;
	if(operation === "create"){
		formURL = $("[name=addNewCPForm]").attr("action");
		formData = $("[name=addNewCPForm]").serialize();
		message = "New ContactPoint has been created successfully!";
	}else{
		formURL = $("[name=saveCPForm]").attr("action");
		formData = $("[name=saveCPForm]").serialize();		
		message = "Contact Points has been created successfully!";
	}
	
	$.ajax({
	  type: 'POST',
	  url: formURL,
	  data:formData
	});
};

ns.contactpoints.beforeDeleteContactPoint = function(contactPointId){
	//console.info("ns.contactpoints.beforeDeleteContactPoint");
	$("#deleteCPDialogContainer").data("cp",contactPointId);
	if(ns.contactpoints.isDeleteDialogLoaded == false){
		ns.contactpoints.loadDeleteConfirmDialog();
	}else{
		ns.contactpoints.openDeleteDialog(contactPointId);
	}	
};

ns.contactpoints.deleteContactPoint = function(contactPointId){	
	//console.info("This will delete contact point" + contactPointId);
	var deleteAction = "/cb/pages/jsp/alert/deleteContactPoint.action"		
	var contactPoint = "contactPointId=" +contactPointId;
	$.ajax({
	  type: 'GET',
	  url: deleteAction,
	  data:contactPoint,
	  success: function(res){
		$('#contactPointsGrid').trigger("reloadGrid"); 
	  }
	});
};

ns.contactpoints.sendImmediateAlert = function(contactPointId){
	//console.info("immediate alert needs to be send!!!" +  contactPointId);
	var sendAction = "/cb/pages/jsp/alert/testContactPoint.action"		
	var contactPoint = "contactPointId=" +contactPointId;
	$.ajax({
	  type: 'GET',
	  url: sendAction,
	  data:contactPoint
	});
};


ns.contactpoints.isDeleteDialogLoaded = false;
ns.contactpoints.loadDeleteConfirmDialog = function(){
	//console.info("loadDeleteConfirmDialog");
	var deleteCPDiaURL = "/cb/pages/jsp/alert/inc/delete_contactpoint.jsp";
	$.ajax({
	  type: 'GET',
	  url: deleteCPDiaURL,
	  success: function(response){
		//console.info("received delete dialog");
		$("#deleteCPDialogContainer").html(response);
		ns.contactpoints.initDeleteDialog();
		$("#deleteCPDialog").dialog('open');
		ns.contactpoints.isDeleteDialogLoaded = true;
	  }
	});
};

ns.contactpoints.clearFormElements = function(formElements){
	$.each(formElements,function(i,element){
		$(element).val("");
	});
	$("#contactPointType").selectmenu("value","-1");
};

//************************ Column Formatters **********************************//
ns.contactpoints.nameColumnFormatter = function(cellvalue, options, rowObject) { 
	var value = "";	
	if (rowObject.contactPointType === 3){
		value = "Mobile Device";
	}else{
		value = ns.common.HTMLEncode(cellvalue);
	} 
	return '' + value + '';
};

ns.contactpoints.typeColumnFormatter = function(cellvalue, options, rowObject) { 
	var value = "";	
	if (rowObject.contactPointType === 3){
		value = js.contactpoints.type.messagecenter || "Text Message";
	}else if(rowObject.contactPointType === 2){
		value = js.contactpoints.type.email || "Email";
	}else{
		value = cellvalue;
	}
	return '' + value + '';	
};

ns.contactpoints.actionColumnFormatter = function(cellvalue, options, rowObject) {
	var testLink = "<a class='' title='" + js.contactpoints.action.testLabel + "' href='#' onClick='ns.contactpoints.sendImmediateAlert(\"" + rowObject.contactPointID  + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-message'></span></a>";
	var trashLink = "<a class='' title='" + js.contactpoints.action.deleteLabel+ "' href='#' onClick='ns.contactpoints.beforeDeleteContactPoint(\"" + rowObject.contactPointID  + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>"; 
	return ' ' + testLink + ' ' + trashLink + ' ';
};


//************************ Custom Editors **********************************//
ns.contactpoints.addCustomHiddenBox = function(value, options){
	var el = document.createElement("input");
	  el.type="text";
	  el.value = value;
	  return el;
};

ns.contactpoints.setCustomHiddenValue = function(elem, operation, value){
	return $(elem).find("input").val();
};

//************************ Form Validations **********************************//


//************************ Mobile Device Management **************************//
/**
* This function loads the initial view for 
* mobile device management screen
*/
ns.contactpoints.loadMobileDevicesView = function(){
	//var routers = sap.banking.ui.routing.Routers;
	//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navTo("_registeredDevices");	
	$.publish("sap-ui-renderMobileDeviceView",{route: "_registeredDevices"});
}

ns.contactpoints.addNewDeviceClickHandler = function(e){	
	//var routers = sap.banking.ui.routing.Routers;
	//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navTo("_addDevice");
	$.publish("sap-ui-renderMobileDeviceView",{route: "_addDevice"});
	$("#summary").hide();
	ns.common.selectDashboardItem("addNewDeviceLink");
};


$(document).ready(function() {
	ns.contactpoints.setup();
});
