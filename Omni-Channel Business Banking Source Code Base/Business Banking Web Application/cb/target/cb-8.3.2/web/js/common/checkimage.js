ns.checkimage.setup = function() {
	ns.checkimage.registerEvents();
	//load the check image dialog
	var config = ns.common.dialogs["checkImage"];
	config.autoOpen = "false";
	ns.common.openDialog("checkImage");
}

ns.checkimage.registerEvents = function() {
	$("a[buttonType='close']").click(ns.checkimage.closeDialog);
}

ns.checkimage.rotate = function(rotation) {
	var rotationFunctionURL;
	if (rotation == "90") {
		rotationFunctionURL = $("#rotatePlus90").val();
	} else if (rotation == "-90") {
		rotationFunctionURL = $("#rotateMinus90").val();
	}
	ns.checkimage.rotateImage(rotationFunctionURL);
};

ns.checkimage.rotateImage = function(urlString) {
	$.ajax({
		url : urlString,
		success : function(data) {
			ns.checkimage.closeDialog();
			$("#checkImageDialogID").html(data).dialog('open');
			ns.imagesearch.initCarousel();
		}
	});
}


ns.checkimage.closeDialog = function() {
	ns.common.closeDialog('checkImageDialogID');
}

ns.checkimage.emailImage = function() {
	var email = $("input[name=email]").val();
	$.ajax({
		url : email,
		success : function(data) {
			ns.checkimage.closeDialog();
			$('#checkImageDialogID').html(data).dialog('open');
			ns.imagesearch.initCarousel();
		}
	});
}

ns.checkimage.zoom = function(direction) {
	var urlString = "";
	var zoom = $("#zoom").val();
	var zoomVal=eval(zoom);
	if (isNaN(zoomVal)){
		zoom=0;
	}
	else{
		zoom=zoomVal;
	}
	if ( direction == "in" ) {
		zoom=zoom+1;
		urlString = "/cb/pages/jsp/common/checkimage.jsp?paramRotationBy=0&paramZoom=" + zoom;
	}else{
		zoom=zoom-1;
		urlString = "/cb/pages/jsp/common/checkimage.jsp?paramRotationBy=0&paramZoom=" + zoom;
	}
	
	
	$.ajax({    
		url: urlString,     
		success: function(data) {  
			ns.common.closeDialog();
			$("#checkImageDialogID").html(data).dialog('open');
			ns.imagesearch.initCarousel();
		}   
	});   
}


ns.checkimage.visibleImageListItem = undefined; 

ns.checkimage.printImage = function() {
	//console.info("ns.checkimage.printImage");
	if(ns.checkimage.visibleImageListItem){
		$(ns.checkimage.visibleImageListItem).find("div[containerType=image]").print(); //active image
	}else{		
		$("#checkImageDialogID div[containerType=image]").print();
	}	
}


$(document).ready(function() {
	ns.checkimage.setup();
});


$.subscribe("checkImage.initCarouselTopic", function(event,data) {
	ns.imagesearch.initCarousel();
});