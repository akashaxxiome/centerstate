/**
 * This js file contains all the overridden functions for jQuery and struts2 jQuery plugin
 * It defines the function in common name space
 */

/**
 * Function loads the Struts2 jQuery Dependencies
 */
ns.common.loadSJQDependencies = function() {
    var dfd = new $.Deferred();
    var jqLocale = ns.home.getJQLocale();
    $.struts2_jquery.gridLocal = jqLocale;
    var SJQVERSION = "?s2j=" + $.struts2_jquery.version;

    //Files which are required to be loaded synchronously
    $.struts2_jquery.require("i18n/grid.locale-" + $.struts2_jquery.gridLocal + ".js", "", "/cb/struts/");
    $.struts2_jquery.require("js/plugins/jquery.jqGrid" + $.struts2_jquery.minSuffix + ".js", '', '/cb/struts/');
    $.struts2_jquery.require("js/struts2/jquery.grid.struts2" + jQuery.struts2_jquery.minSuffix + ".js");

    /*$.struts2_jquery.require("js/jqGridSortableColumns.min.js","","/cb/web/");
    $.struts2_jquery.require("js/JsonXml.js","","/cb/web/");
    $.struts2_jquery.require("js/grid.import.js","","/cb/web/");
    $.struts2_jquery.require("js/plugins/grid.inlinedit.js","","/cb/struts/");
    $.struts2_jquery.require("js/plugins/grid.formedit.js","","/cb/struts/");*/


    //Plugin files can be loaded asynchronously
    var SJQVERSION = "?s2j=" + $.struts2_jquery.version;
    $.when(
        $.cachedScript("/cb/web/js/jqGridSortableColumns.min.js"),
        $.cachedScript("/cb/web/js/grid.import.js"),
        $.cachedScript("/cb/struts/js/plugins/grid.inlinedit.js" + SJQVERSION),
        $.cachedScript("/cb/struts/js/plugins/grid.formedit.js" + SJQVERSION)
    ).done(function() {
        dfd.resolve();
    });

    //dfd.resolve();
    return dfd;
};

//Main function to call different overrides, if you need to override define a new function and call from here
// This will keep overrides and extensions to SJQ, SJQGrid clean
ns.common.overrideJQGridFunctions = function() {
    ns.common.overrideJQGridDestroyClearBeforeUnload();
    ns.common.overrideSJQFormSubmit();
    ns.common.overrideSJQGridConstructor();
    ns.common.overrideSJQGridParseGridParams();
};


//Overridden functions for jQuery Grid
ns.common.overrideJQGridDestroyClearBeforeUnload = function() {
    //Extend defaults
    $.extend($.jgrid.defaults, {
        altRows: true,
        altclass: 'ui-state-zebra',
        autowidth: true,
        gridview: true,
        loadui: 'disable'/*,
        ajaxGridOptions: {
            error: function() {
                ns.common.resizeWidthOfGrids();
            }
        }*/

    });

    //Extend for destroy and clearBeforeUnload
    $.jgrid.extend({
        destroy: function() {
            var events = $(window).data('events');
            if (events) {
                var tempHandler = [],
                    count = 0;
                $.each(events.unload, function(i, unloadObj) {
                    if (unloadObj.namespace === '') {
                        tempHandler[count++] = unloadObj.handler;
                    }
                })
                for (var i = 0; i < count; i++) {
                    $(window).unbind('unload', tempHandler[i]);
                }
                delete tempHandler;
            }
            return this.each(function() {
                if (this.grid) {
                    var gid = this.id;
                    try {
                        $(this).jqGrid('clearBeforeUnload');
                        $("#gbox_" + gid).find('.ui-jqgrid-resize-mark').removeClass().unbind().remove();
                        $("#gbox_" + gid).find('.jqgrid-overlay').removeClass().unbind().unbind().remove();;
                        $("#gbox_" + gid).find('.loading').removeClass().unbind().unbind().remove();
                        $.delAttrByRoot(this.id);
                        $("#gbox_" + gid).remove();
                        //alert('done');
                    } catch (_) {}
                }
            });
        },

        clearBeforeUnload: function() {
            return this.each(function() {
                var grid = this.grid;
                var gboxID = "#gbox_" + this.id;

                // Iterate over headers to release headers
                var i, j, l = grid.headers.length;
                var sortIconContainer = null;
                var searchRow = $(grid.hDiv).find('tr.ui-search-toolbar');
                var labelRow = $(grid.hDiv).find('tr.ui-jqgrid-labels');
                var searchRowTH = $(searchRow).find('th');
                var labelRowTH = $(labelRow).find('th');
                var spanArray = null;
                for (i = 0; i < l; i++) {
                    //Access sort image and Header separator span and release them
                    spanArray = $(grid.headers[i].el).find('span');
                    for (j = 0; j < spanArray.length; j++) {
                        $(spanArray[j]).removeClass();
                        $(spanArray[j]).unbind();
                        spanArray[j].innerHTML = null;
                        spanArray[j] = null;
                    }
                    sortIconContainer = $(grid.headers[i].el).find('div.ui-jqgrid-sortable');
                    sortIconContainer.removeClass();
                    sortIconContainer.unbind();
                    sortIconContainer = null;
                    grid.headers[i].el = null;
                    $(searchRowTH).removeClass().unbind();
                    $(labelRowTH).removeClass().unbind();
                }

                // Remove header rows and thead
                $(searchRow).removeClass().unbind();
                if ($(labelRow).data("ui-sortable") != undefined) {
                    $(labelRow).sortable('destroy').removeClass().unbind();
                }

                $(grid.hDiv).find('table').unbind().remove();

                //$(document).unbind("mouseup"); // TODO add namespace
                grid.headers = null;
                grid.cols = null;
                $(grid.hDiv).unbind("mousemove"); // TODO add namespace

                var mainPagerTable = null;
                var pagerTDcollection = null;
                var pagerTablecollection = null;
                var pagerNavigationTableTD = null;
                if (this.p.pager) { // if not part of grid
                    mainPagerTable = $(gboxID).find('table.ui-pg-table').filter(":first");
                    pagerNavigationTableTD = mainPagerTable.find('table.navtable').find('td');
                    for (i = 0; i < pagerNavigationTableTD.length; i++) {
                        var spanCollection = $(pagerNavigationTableTD[i]).find('span')
                        for (j = 0; j < spanCollection.length; j++) {
                            $(spanCollection[j]).removeClass().unbind();
                            $(spanCollection[j]).innerHTML = null;
                        }
                        $(pagerNavigationTableTD[i]).find('ui-pg-div').removeClass().unbind();
                        $(pagerNavigationTableTD[i]).removeClass().unbind().empty();
                    }
                    pagerTablecollection = mainPagerTable.find('table.ui-pg-table');
                    for (i = 0; i < pagerTablecollection.length; i++) {
                        pagerTDcollection = $(pagerTablecollection[i]).find('td');
                        for (j = 0; j < pagerTDcollection.length; j++) {
                            $(pagerTDcollection[j]).find('span').removeClass().unbind();
                            $(pagerTDcollection[j]).find('select').removeClass().unbind();
                            $(pagerTDcollection[j]).find('input').removeClass().unbind();
                            $(pagerTDcollection[j]).removeClass().unbind().empty();
                        }

                    }
                    mainPagerTable.find('td').removeClass().unbind().html('');
                    mainPagerTable.removeClass().unbind().remove();
                    this.p.pager = null;
                }

                var dataTable = $(gboxID).find('table.ui-jqgrid-btable');
                var anchorInsideDataTD = dataTable.find('td a');
                for (i = 0; i < anchorInsideDataTD.length; i++) {
                    anchorInsideDataTD[i].onclick = '';
                    anchorInsideDataTD.innerHTML = null;
                }

                this.p.colModel = null;
                var tBody = $("tbody", grid.bDiv).filter(':first');
                $("td", tBody[0]).unbind().empty();
                $("tr", tBody[0]).removeClass().unbind().remove();

                this.p.colModel = null;
                dataTable.unbind().removeClass();
                $(grid.bDiv).unbind();

                if (grid.footers.length) {
                    var footerTDs = $(gboxID).find('table.ui-jqgrid-ftable').find('td');
                    for (i = 0; i < footerTDs.length; i++) {
                        $(footerTDs[i]).unbind().removeClass();
                        footerTDs[i].innerHTML = null;
                    }
                }

                grid.footers = null;
                $(gboxID).find('tr').unbind().removeClass().remove();

                $(grid.cDiv).unbind().remove();

                $(this).unbind();
                grid.dragEnd = null;
                grid.dragMove = null;
                grid.dragStart = null;
                grid.emptyRows = null;
                grid.populate = null;
                grid.populateVisible = null;
                grid.scrollGrid = null;
                grid.selectionPreserver = null;

                grid.bDiv = null;
                grid.cDiv = null;
                grid.hDiv = null;
                grid.cols = null;
                grid.footers = null;

                this.grid = null;
                this.formatCol = null;
                this.sortData = null;
                this.updatepager = null;
                this.refreshIndex = null;
                this.setHeadCheckBox = null;
                this.constructTr = null;
                this.formatter = null;
                this.addXmlData = null;
                this.addJSONData = null;
                this.p = null;
            });
        }
    });
};

//This js contains all the overridden functions for jQuery and struts2 jQuery plugin
ns.common.overrideSJQFormSubmit = function() {
    //Overriden formsubmit function required keep track of submit button refference	
    //console.info("starting overrides for struts2 jquery");
    $.struts2_jquery.formsubmit = function($elem, o, topic) {
        var self = this,
            params = {};
        o.actionTopic = topic;
        self.log('formsubmit : ' + o.id);
        if (!self.loadAtOnce) {
            self.require("js/plugins/jquery.form" + self.minSuffix + ".js");
        }

        if (o.targets) {
            self.subscribeTopics($elem, o.reloadtopics, self.handler.form, o);
            self.subscribeTopics($elem, o.listentopics, self.handler.form, o);

            self.subscribeTopics($elem, topic, self.handler.form, o);
            $.each(o.targets.split(','), function(i, target) {
                self.subscribeTopics($(self.escId(target)), "_sj_div_effect_" + target + o.id, self.handler.effect, o);
                if (self.ajaxhistory) {
                    self.history($elem, topic, target);
                }
            });
            $.each(o.formids.split(','), function(i, f) {
                $(self.escId(f)).bind("submit", function(e) {
                    e.preventDefault();
                });
            });
            $elem.click(function(e) {
                $elem.publish(topic);
                ns.common.trackElement($elem);
                return false;
            });
        } else {
            // Submit Forms without AJAX
            $elem.click(function(e) {
                var form = $(self.escId(o.formids)),
                    orginal = {};
                orginal.formvalidate = true;
                e.preventDefault();
                if (o.validate) {
                    orginal.formvalidate = self.validateForm(form, o);
                    if (o.onaftervalidation) {
                        $.each(o.onaftervalidation.split(','), function(i, topic) {
                            $elem.publish(topic, $elem, orginal);
                        });
                    }
                }

                if (orginal.formvalidate) {
                    form.submit();
                }
                ns.common.trackElement($elem);
                return false;
            });
            if (o.listentopics) {
                params.formids = o.formids;
                params.validate = o.validate;
                $elem.subscribe(o.listentopics, function(event) {
                    var form = $(self.escId(event.data.formids)),
                        orginal = {
                            formvalidate: true
                        };

                    if (event.data.validate) {
                        orginal.formvalidate = self.validateForm(form, o);
                        if (o.onaftervalidation) {
                            $.each(o.onaftervalidation.split(','), function(i, topic) {
                                $elem.publish(topic, $elem, orginal);
                            });
                        }
                    }

                    if (orginal.formvalidate) {
                        form.submit();
                    }
                }, params);
            }
        }
    };
};

//Override SQJ Grid constructor
ns.common.overrideSJQGridConstructor = function() {
    var _originalGrid = $.struts2_jquery_grid.grid;

    //A function which gets called after original Struts2 jQGrid function constructor is called
    var _executeAfterOriginalGrid = function() {
        //Load the custom jqGrid css file
        $.struts2_jquery.requireCss("ui.jqgrid.css", "/cb/web/css/");
    }

    $.struts2_jquery_grid.grid = function($elem, o) {
        var self = this;
        var jqLocale = ns.home.getJQLocale();
        self.local = jqLocale;
        self.gridLocal = jqLocale;
        var gridArguments = arguments;

        //For the grids on home page, user does not have ability to save Grid preferences, skipping call to fetch Grid preferences.
        var isHomePageGrid = ns.common.checkForHomePageGrid(o.id);
        if (!isHomePageGrid) {
            //console.log("getting preferences for=>"+o.id);
            $.ajax({
                url: '/cb/pages/jsp/personalization/gridConfigAction_getGridPreference.action',
                type: "POST",
                data: {
                    id: o.id,
                    CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage
                },
                dataType: "json",
            })
                .done(function(response, textStatus, jqXHR) {
                    if (response != null) {
                        var colStates = response.columnModels;
                        if (colStates !== undefined && colStates != null) {
                            for (var i = 0; i < o.colModel.length; i++) {
                                //var testname = permutationJson[cmName];
                                var colItem = o.colModel[i];
                                var cmName = colItem.name;
                                $.extend(true, colItem, colStates[cmName]);
                            }
                            o.sortname = response.sortname;
                            o.sortorder = response.sortorder;
                            //Restore the rowNum to the standard default grid row number size
                            o.rowNum = ns.home.StdGridRowNum;
                            //Save columnPermutation as custom variable
                            if (response.permutation) {
                                o.columnPermutation = response.permutation;
                            }
                        }
                    }
                    $.when(_originalGrid.apply(self, gridArguments)).done(function(x) {
                        _executeAfterOriginalGrid();
                    });
                });
        } else {
            //console.log("NOT getting preferences for=>"+o.id);

            //Home page grids should be excluded from global ajax settings
            //There are grids on portlets which shouldn't be blocking UI
            //Exlude grids on home page, portlets applying local ajax setttings
            //Note: Since call is made local, the caller needs to show spinner
            //		which should be done by passing onBeforeTopics and 
            //		onCompleteTopics with jQGrid
            var localAaxSettings = {
                ajaxGridOptions: {
                    global: false
                }
            }
            $.extend(o, localAaxSettings);
            if (o.ongridcompletetopics == undefined || o.ongridcompletetopics == '') {
                o.ongridcompletetopics = "localAjaxGridCompleteTopic";
            } else {
                o.ongridcompletetopics = o.ongridcompletetopics.concat(",localAjaxGridCompleteTopic");
            }

            if (o.onbef == undefined || o.onbef == '') {
                o.onbef = "localAjaxGridOnBeforeTopic";
            } else {
                o.onbef = o.onbef.concat(",localAjaxGridOnBeforeTopic");
            }
            $.when(_originalGrid.apply(self, gridArguments)).done(function(x) {
                _executeAfterOriginalGrid();
            });
        }
    };
};

//Override parse grid parameters
ns.common.overrideSJQGridParseGridParams = function() {
    var _originalParseGridParam = $.struts2_jquery_grid.parseGridParams;
    $.struts2_jquery_grid.parseGridParams = function($elem, o, params) {
        if (o.ongridcompletetopics == undefined || o.ongridcompletetopics == '') {
            o.ongridcompletetopics = "commonGridComplete";
        } else {
            o.ongridcompletetopics = o.ongridcompletetopics.concat(",commonGridComplete");
        }

        /*if(o.onsortcoltopics == undefined || o.onsortcoltopics == ''){
	        o.onsortcoltopics = "gridSortingComplete";
	    }else{
	        o.onsortcoltopics = o.onsortcoltopics.concat(",gridSortingComplete");
	    }*/
		
		if (o.onerr == undefined || o.onerr == '') {
            o.onerr = "commonGridErrorTopic";
        } else {
            o.onerr = o.onerr.concat(",commonGridErrorTopic");
        }
		
        return _originalParseGridParam.apply(this, arguments);
    };
};

//Sets the busy indicator on closest portlet for element passed
$.subscribe("localAjaxGridOnBeforeTopic", function(oEvent, element) {
    $(element).closest(".portlet-content").setBusy(true);
});

//Topic to listen to local ajax grid complete topic
$.subscribe("localAjaxGridCompleteTopic", function(oEvent, element) {
    $.ajaxSetup({
        global: true
    });
    $(element).closest(".portlet-content").setBusy(false);
});