;(function($){
/**
 * Date Range Picker Chinese Translation
 */
	$.fn.extdaterangepicker.setDefaults({
		dateFormat :"yy-mm-dd",
		rangeSplitter : ":",
		presetRanges : [
				{key:"TODAY",text: '今天', dateStart: 'today', dateEnd: 'today' },
				{key:"YESTERDAY",text: '昨天', dateStart: 'today-1', dateEnd: 'today-1' },
				{key:"CURRENT_WEEK",text: '这一周', dateStart: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(5)} },
				{key:"CURRENT_MONTH",text: '本月', dateStart: function(){return Date.parse('today').moveToFirstDayOfMonth();}, dateEnd: function(){return Date.parse('today').moveToLastDayOfMonth();}},
				{key:"NEXT_WEEK",text: '下周', dateStart: function(){return Date.parse('today').moveToDayOfWeek(1,1)}, dateEnd: function(){return Date.parse('today').moveToDayOfWeek(1,1).addDays(4)} },
				{key:"NEXT_MONTH",text: '下个月', dateStart: function(){return Date.parse('today').moveToMonth(Date.parse('today').getMonth()+1).moveToFirstDayOfMonth(); }, dateEnd: function(){return Date.parse('today').moveToMonth(Date.parse('today').getMonth()+1).moveToLastDayOfMonth(); } },
				{key:"NEXT_30_DAYS",text: '接下来的30天', dateStart: 'today', dateEnd: 'today+30days' },
				{key:"LAST_WEEK",text: '上个星期', dateStart:function(){return Date.today().addWeeks(-1).moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: function(){return Date.today().addWeeks(-1).moveToDayOfWeek(0,-1).addDays(5)} },
				{key:"LAST_7_DAYS",text: '最近7天', dateStart: 'today-6days', dateEnd: 'today' },
				{key:"LAST_30_DAYS",text: '持续30天', dateStart: 'today-29days', dateEnd: 'today' },
				{key:"LAST_60_DAYS",text: '持续60天', dateStart: 'today-59days', dateEnd: 'today' },
				{key:"LAST_90_DAYS",text: '最后90天用量', dateStart: 'today-89days', dateEnd: 'today' },
				{key:"MONTH_TO_DATE",text: '本月至今', dateStart: function(){ return Date.parse('today').moveToFirstDayOfMonth();  }, dateEnd: 'today' },
				{key:"LAST_MONTH",text: '上个月', dateStart: function(){ return Date.parse('1 month ago').moveToFirstDayOfMonth();  }, dateEnd: function(){ return Date.parse('1 month ago').moveToLastDayOfMonth();  } },
				{key:"PLUS_MINUS_30_DAYS",text: '+/ - 30天', dateStart: 'today-30days', dateEnd: 'today+30days' },
				{key:"CURRENT_MONTH_ACNT_MGNT",text: '本月', dateStart: function(){return Date.parse('today').moveToFirstDayOfMonth();}, dateEnd: 'today'},
				{key:"CURRENT_WEEK_ACNT_MGNT",text: '这一周', dateStart: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: 'today'}
		],
		presets: {
			specificDate: '具体日期',			
			dateRange: '日期范围'
		}
		});	
})(jQuery);