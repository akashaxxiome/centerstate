/****************************************************************
* This js contains all the overridden functions for jQuery
*
*****************************************************************/
/**
* Overridden filter function. which applies filter only to non blank value of expression
* passed. prior to jQuery upgrade it was not applying blank expression , With jQuery upgrade
* filter gets applied even for blank expression passed.
* A override is required as visualize charts are not working after jQuery upgrade
* NOTE: This should be removed when charts are not using visualize library. This was mainly added to fix chart rendering issue
* after jQuery upgrade.
*/
var oFilter = $.fn.filter;
$.fn.filter = function(exp) {
	if(typeof exp == "string"){
		exp = $.trim(exp);		
		if(exp.length>0){
			return oFilter.apply(this, arguments);	
		}else{
			return this;
		}
	}else{
		return oFilter.apply(this, arguments);	
	}	
};