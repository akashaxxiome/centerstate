/* ========================================================================
 * Datagrid: datagrid.js v0.0.1
 * ========================================================================
 * jQGrid Extension to support oData REST communication
 * This jQuery plugin extends the jQGrid bahavior to support oData
 * It also considers search criteria if grid is initialized with postData
 * e.g postData: {
 *   	searchCriteria:{
 *   		"StartDate": "05/05/2014",
 *   		"EndDate": "05/05/2015",
 *   		"PayeeName":"Mahesh Parab",
 *   		"Amount": "20.00"
 *   	}
 *	}
 * ======================================================================== */
;
(function($, window, document, undefined) {
    "use strict";

    //Initialize namespace if not found
    if (!$.fn.datagrid) {
        $.fn.datagrid = {};
    };

    $.fn.datagrid = function(options) {

        /**
         * This function crates filter for search criteria passed
         * Criteria is oData specific
         */
        var _createSearchFilter = function(oCriteria) {
            var searchFilter, value;
            for (var key in oCriteria) {
                value = oCriteria[key];
                if (typeof value === "string") {
                    value = "'" + value + "'";
                }
                if (searchFilter) {
                    searchFilter = searchFilter + " and " + key + " eq " + value;
                } else {
                    searchFilter = key + " eq " + value;
                }
            }
            return searchFilter;
        };

        // Merge the passed in options with the default options
        var config = {
            serializeGridData: function(postData) {
                console.debug("serializeGridData, postData", postData);
                var params = {
                    $top: postData.rows,
                    $skip: (parseInt(postData.page, 10) - 1) * postData.rows,
                    $inlinecount: "allpages"
                };

                //Include sort index if any
                if (postData.sidx) {
                    params.$orderby = postData.sidx + " " + postData.sord;
                }

                //Include search if any currently supported only contains search
                /*if (postData.searchField && (postData.searchString && postData.searchString !=="")) {
			        var searchSting = isNaN(Number(postData.searchString)) ? "'" + postData.searchString + "'" : postData.searchString;
					params.$filter = "" + postData.searchField + " eq " + searchSting;
			    }*/

                //Consider the search criteria if grid is initilazed with postData, this will help in passing search criteria along with jQgrid
                if (postData.searchCriteria) {
                    params.$filter = _createSearchFilter(postData.searchCriteria);
                }
                console.debug("Grid Params", params);
                return params;
            },

            loadBeforeSend: function(xhr, settings) {
                console.log("loadBeforeSend");
                settings.url = settings.url + "&$format=json";
                xhr.setRequestHeader("x-csrf-token", ns.home.getSessionTokenVarForGlobalMessage);
            },

            beforeProcessing: function(data, status, xhr) {
                console.log("beforeProcessing");
                var currentPage = $(this).jqGrid("getGridParam", "page");
                var rowNum = $(this).jqGrid("getGridParam", "rowNum");
                var totoalRecords = data.d.__count;
                var totoalPages = Math.ceil(totoalRecords / rowNum);
                data.page = currentPage || 1;
                data.records = totoalRecords; // || 10; //total records
                data.total = totoalPages; //TODO: Get total pages from server response
            }
        };

        options = $.extend({}, $.fn.datagrid.DEFAULTS, config, options);

        //Initialize the jqgrid
        //TODO: handle multiple selectors
        $(this).jqGrid(options);

        /* return the elements passed in */
        return this.each(function() {
            var $this = $(this);
        });
    };

    // Default options for data grid
    $.fn.datagrid.DEFAULTS = {
        datatype: "json",
        jsonReader: {
            root: "d.results",
        }
    };

})(jQuery, window, document);