ns.approval.addActionsRowsForApprovePaymentsGrid = function()
{
	var ids = jQuery("#approvePaymentsGrid").jqGrid('getDataIDs');
	$(".approvalViewkBox").css({"display":"inline-block", "width":"82px","height":"auto", "margin":"5px","position":"relative"}).addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-selectmenu");
	for(var i=1;i<=ids.length;i++){  
		$("#approvalCheckBox" + i).selectmenu({width:82});
        var optionVal = $('#approvalCheckBox' + i).val();
		ns.approval.checkIfDisplayReject( optionVal, "displayReason" + i );
	}
	
	// add startIndex and rows hidden field
	var startIndex = $("#approvePaymentsGrid").jqGrid('getCell', 1, 'rn');
	var rows = ids.length;
	if(document.getElementsByName("startIndex").length == 0)
	{
		$("#approvePaymentsGrid").append("<input type='hidden' name='startIndex' value='" + startIndex + "' />");
		$("#approvePaymentsGrid").append("<input type='hidden' name='submitRows' value='" + rows + "'/>");
	}
	else
	{
		$("#approvePaymentsGrid input:hidden[name='startIndex']").val(startIndex);
		$("#approvePaymentsGrid input:hidden[name='submitRows']").val(rows);
	}
};

ns.approval.formatAccount = function(cellvalue, options, rowObject)
{
	if(cellvalue == "" || cellvalue == null || cellvalue == undefined)
		return "<div align=\"center\">--</div>";
	return cellvalue;
};

ns.approval.formatApprovePaymentActionLinks = function(cellvalue, options, rowObject)
{
	var view = "<span align=\"center\"><a class='approvalViewkBox' href=\"#\" onClick=\"ns.approval.viewApprovePaymentDetails('"+ rowObject.map.ViewURL +"');\">" +js_view+ "</a></span>";
	var ids = jQuery("#approvePaymentsGrid").jqGrid('getDataIDs');
	var isSelectInput = "<input type=\"hidden\" name=\"isSelected\" value=\"false\">";
	var notifyInput = "<input type=\"hidden\" name=\"notifyPrimaryContact\" value=\"false\">";
	var indexInput = "";
	var finalHolder='';
	var rowId = options.rowId;
	var transType = "";
	if(rowObject.item != undefined) {
		transType = rowObject.item.typeAsString;
	}
	if(transType!=undefined && transType!=""){
		// all ACH types should show release instead of approved.  The text for these values should match TWTransactionTypes values
		var releaseOrApprove = ns.approval.decision_approved;
		var releaseOrApproveText = js_appr_approved;
		if (transType ==  "ACH Batch" || transType == "Recurring ACH Batch" || transType == "Tax Payment" ||
			transType == "Recurring Tax Payment" || transType == "Child Support Payment" || transType == "Recurring Child Support Payment") {
			releaseOrApprove = ns.approval.decision_release;
			releaseOrApproveText = js_appr_release;
		}
		
		var optionStr = "<option value=" + "'" +  ns.approval.decision_hold + "'" +  " selected>" + js_appr_hold + "</option>";
		optionStr += "<option value=" + "'" +  releaseOrApprove + "'" +  ">" + releaseOrApproveText + "</option>";
		optionStr += "<option value=" + "'" +  ns.approval.decision_rejected + "'" +  ">" + js_appr_rejected + "</option>";

		var cellApprovalActions = "<select class=\"txtbox\" id=\"approvalCheckBox" + rowId + "\" name=\"approvalActions\" onchange='ns.approval.checkIfDisplayReject( $(this).val(), \"displayReason" + rowId + "\");' >"
			+ optionStr + "</select>";
		
		var cellReason = "<div id=\"displayReason" + rowId + "\">&nbsp;&nbsp;&nbsp;" + js_reason + "&nbsp;&nbsp;&nbsp;"
			+ "<input class=\"ui-widget-content ui-corner-all small-input\" type=\"text\" name=\"reason\" size=\"33\" maxlength=\"255\" value=''"
			+ " onChange='ns.approval.verifyReason( document.approvePaymentsForm, \"" + rowId + "\" );' ></div>";
		var errorSpan = '<span id="rejectedReason' + rowId +'Error"></span>'
		var itemIndexValue= rowId-1;
		indexInput = "<input type=\"hidden\" name=\"itemIndex\" value=\"" + itemIndexValue + "\">";
		finalHolder= cellApprovalActions + view  + cellReason + errorSpan	+ isSelectInput + notifyInput + indexInput;
		
		//$("#approvalCheckBox" + rowId).selectmenu({width:82});
	}
	return finalHolder;
};

ns.approval.formatItemID = function(cellvalue, options, rowObject)
{
	var view = "<input type=\"hidden\" name=\"itemID\"	value='" + cellvalue + "'>";
	return view;
};

ns.approval.viewApprovePaymentDetails = function(urlString)
{
	//var urlString = detailsURL;
	$.ajax({
		url: urlString,
		success: function(data){
			
			var heading = $('#PageHeading', data).html();
			var $title =$('#ui-dialog-title-viewApprovePaymentDialogID');
			$title.html(heading);
			
			$('#viewApprovePaymentDialogID').html(data).dialog('open');;
			
			ns.common.resizeDialog('viewApprovePaymentDialogID');
		}
	});
};

ns.approval.verifyReason = function( thisForm, count )
{
	if( typeof( thisForm.reason[0] ) == "undefined" ) {
		if( thisForm.reason.value.length > 255 ) {
			window.alert(js_appr_reason_length);
			thisForm.reason.value = thisForm.reason.value.substr( 0, 255 );
		}
	} else {
		if( thisForm.reason[count].value.length > 255 ) {
			window.alert(js_appr_reason_length);
			thisForm.reason[count].value = thisForm.reason[count].value.substr( 0, 255 );
		}
	}
};

ns.approval.checkIfDisplayReject = function( optionVal, idOfRejectDisplay)
{
	displayObj = document.getElementById( idOfRejectDisplay );

	if( !ns.approval.checkIsHold( optionVal )) {
		displayObj.style.visibility='visible';
		displayObj.style.marginBottom='5px';
	} else {
		displayObj.style.visibility='hidden';
	}
};

ns.approval.applyToAllComboBoxes = function(optionForAllName )
{
	$('select[id^="approvalCheckBox"]').selectmenu().selectmenu("index", $('#' + optionForAllName).prop('selectedIndex'));
	$('a[id^="approvalCheckBox"]').each(function (index, Element) {
		var i = index + 1;
		var optionVal = $('#approvalCheckBox' + i).val();
		ns.approval.checkIfDisplayReject(optionVal, "displayReason" + i);
	});
};

$.subscribe('approvePaymentsGridCompleteEvents', function(event,data) {
	ns.approval.addActionsRowsForApprovePaymentsGrid();
	ns.common.resizeWidthOfGrids();
});

$.subscribe('submitApprovePaymentsFormComplete', function(event,data) {
	//$("#approvalGroupSummary").html(data);
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#notes").setFocus();
	}
});

$.subscribe('submitApprovePaymentsFormSuccessTopics', function(event,data) {
	$("#approvalGroupSummary").hide();
	$("#targetApprovePaymentsVerifyContainer").show();
});


ns.approval.formatAccountDisplay = function(cellvalue, options, rowObject)
{
	var fromAccount = rowObject.map.FromAct;
	var toAccount = rowObject.map.ToAct;
	if(fromAccount != "" && toAccount != "" && toAccount != undefined) {
		return "<span class='formatCombinedAccountColumn'>"+rowObject.map.FromAct + "</span><span class='gridColumnIcon'><span class='ffiUiMiscIco ffiUiIco-misc-multilinerowdataindicatorico'></span>" +rowObject.map.ToAct+"</span>";
	} else if(fromAccount != ""   && ((toAccount == "" || toAccount == undefined) && fromAccount != undefined)) {
		return "<span class='formatCombinedAccountColumn'>"+rowObject.map.FromAct + "</span>";
	} else if(fromAccount == "" && (toAccount == "" || toAccount == undefined)) {
		return "";
	}
	return "";
};

ns.approval.submittingUserName = function(cellvalue, options, rowObject)
{
	var strResult = rowObject.item.transaction.resultOfApproval;
	var cellResult = "<div>" + strResult + "</div>";
	return cellvalue+cellResult;
};
ns.approval.amountUpdate= function(cellvalue, options, rowObject)
{
	var currencyStringNoSymbol = rowObject.item.transaction.approvalAmount.currencyStringNoSymbol;
	var currencyCode = rowObject.item.transaction.approvalAmount.currencyCode;
	var cellResult = "<div>" + currencyStringNoSymbol +" " + currencyCode+ "</div>";
	return cellResult;
};

ns.approval.formatSubmittedDate  = function(cellvalue, options, rowObject) 
{
	var view = "";
	if(rowObject.map.isSameDay == "true" || rowObject.item.transaction.sameDayCashCon) {
		view = "* " + cellvalue;
		$('#sameDayNote').css('display','block');
	} else {
		view =  cellvalue;
	}
	return view;
};