

ns.message.setup = function(){
	
	ns.message.mailbox = 'Inbox';//indicating which mailbox (Inbox/Sentbox) under operation
	
	$.subscribe('removeErrorsOnSuccess', function(event, data) {
			$('.errorLabel').html('').removeClass('errorLabel');
			$('#formerrors').html('');
	});
	
	$.subscribe('removeErrorsOnBegin', function(event, data) {
			$('.errorLabel').html('').removeClass('errorLabel');
			$('#formerrors').html('');
	});
	
	//Show the active dashboard based on the user selected summary
	ns.message.showMessageDashboard = function(actionType,messageDashboardElementId){
		ns.common.refreshDashboard('dbMessagesSummary');
		//Simulate dashboard item click event if any additional action is required
		if(messageDashboardElementId && messageDashboardElementId !='' && $('#'+messageDashboardElementId).length > 0){
			//Timeout is required to display spinning wheel for the dashboard button click
			setTimeout(function(){$("#"+messageDashboardElementId).trigger("click");}, 10);
		}
	};
	
	/**
	 * Creating New Message
	 */
	$.subscribe('beforeLoadMessageForm', function(event,data) {
		//alert('beforeLoad');
		$('#messageTabs').hide();
		$('#newMsgPortlet').portlet('expand');
		$('#newMsgPortlet').show();
		$('#actionMsgPortlet').portlet('fold');
		$('#actionMsgPortlet').hide();
		ns.common.selectDashboardItem("newMessageLink");
	});

	$.subscribe('loadMessageFormComplete', function(event,data) {
		//alert('completeLoad');
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#newMsgPortlet").setFocus();			
		}
	});

	$.subscribe('errorLoadMessageForm', function(event,data) {
		//alert('errorLoad');

		
	});

	$.subscribe('successLoadMessageForm', function(event,data) {
		//alert('successLoad');

		$('#messageTabs').portlet('fold');

		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
//		var heading = $('#PageHeading').html();
//		var $title = $('#details .portlet-title');
//		$title.html(heading);

		$('#newMsgPortlet').portlet('expand');
		$('#actionMsgPortlet').portlet('fold');
		$('#details').slideDown();

	});


	
	/**
	 * Sending New Message
	 */
	$.subscribe('beforeSendingNewMessage', function(event,data) {
		$.publish('removeErrorsOnBegin');
		//alert('beforeSend');
	});

	$.subscribe('sendingNewMessageComplete', function(event,data) {
		//alert('completeSend');
		if(accessibility){
			accessibility.setFocusOnDashboard();
		}
	});

	$.subscribe('errorSendingNewMessage', function(event,data) {
		//alert('errorSend');		
	});

	$.subscribe('successSendingNewMessage', function(event,data) {
		ns.common.showSummaryOnSuccess("summary","done");
		ns.common.selectDashboardItem("goBackSummaryLink");
		$.publish('refreshMessageSmallPanel');
		ns.common.showStatus();
		ns.message.closeDetails();
		$('#messageTabs').portlet('expand');
		$('#messageTabs').show();
	    $('#details').slideUp();
	   
	});

	
	/**
	 * Deleting Confirmation (From Grid)
	 */
	$.subscribe('beforeDeletingMessagesConfirm', function(event,data) {
		var selectedInboxMsgIDs="";
		var gridId = "inboxMessagesGridId";
		var multiSelectedInboxMsgIDs = $('#'+gridId).jqGrid('getGridParam', 'selarrrow');
		var multiSelectedInboxMsgIDsArray=[];
		var selectedId = "";
		for(var i=0;i<multiSelectedInboxMsgIDs.length;i++){
			selectedId = $("#"+gridId).jqGrid('getRowData',multiSelectedInboxMsgIDs[i]).messageIds;
			multiSelectedInboxMsgIDsArray.push(selectedId);
		}
		if(multiSelectedInboxMsgIDsArray.length==1 && $('#multicheck').val()=="true"){
			$('#'+data.formids +' #allSelectedInboxMsgIDs').val('');
			$('#'+data.formids +' #selectedInboxMsgIDs').val(multiSelectedInboxMsgIDsArray);
			selectedInboxMsgIDs = multiSelectedInboxMsgIDsArray;
		}else if(multiSelectedInboxMsgIDsArray.length>1 && $('#multicheck').val()=="true"){
			$('#'+data.formids +' #allSelectedInboxMsgIDs').val(multiSelectedInboxMsgIDsArray);
			selectedInboxMsgIDs = multiSelectedInboxMsgIDsArray;
			$('#'+data.formids +' #selectedInboxMsgIDs').val('');
		}else{
			$('#'+data.formids +' #allSelectedInboxMsgIDs').val('');
			selectedInboxMsgIDs = $('#'+data.formids +' #selectedInboxMsgIDs').val();
		}
		if(selectedInboxMsgIDs== undefined || selectedInboxMsgIDs == ''){
			ns.common.showStatus("Please select messages to delete!");
			return false;
		}
	});


	$.subscribe('DeletingMessagesConfirmComplete', function(event,data) {
		//alert('completeConfirm');
	});

	$.subscribe('errorDeletingMessagesConfirm', function(event,data) {
		//alert('errorConfirm');
		
	});

	$.subscribe('successDeletingMessagesConfirm', function(event,data) {
		//alert('successConfirm');
		$('#DeleteMessagesConfirmDialogID').dialog('open');
	
	});
	
	
	/**
	 * Deleting Confirmation (From Grid)
	 */
	$.subscribe('beforeDeletingSentMessagesConfirm', function(event,data) {
		var selectedInboxMsgIDs="";
		var gridId = "sentMessagesGridId";
		var multiSelectedInboxMsgIDs = $('#'+gridId).jqGrid('getGridParam', 'selarrrow');
		var multiSelectedInboxMsgIDsArray=[];
		var selectedId = "";
		for(var i=0;i<multiSelectedInboxMsgIDs.length;i++){
			selectedId = $("#"+gridId).jqGrid('getRowData',multiSelectedInboxMsgIDs[i]).messageIds;
			multiSelectedInboxMsgIDsArray.push(selectedId);
		}
		if(multiSelectedInboxMsgIDsArray.length==1 && $('#multicheck').val()=="true"){
			$('#'+data.formids +' #selectedSentMsgIDs').val(multiSelectedInboxMsgIDsArray);
			selectedInboxMsgIDs = multiSelectedInboxMsgIDsArray;
		}else if(multiSelectedInboxMsgIDsArray.length>1 && $('#multicheck').val()=="true"){
			$('#'+data.formids +' #allSelectedInboxMsgIDs').val(multiSelectedInboxMsgIDsArray);
			selectedInboxMsgIDs = multiSelectedInboxMsgIDsArray;
			$('#'+data.formids +' #selectedSentMsgIDs').val('');
		}else{
			$('#'+data.formids +' #allSelectedInboxMsgIDs').val('');
			selectedInboxMsgIDs = $('#'+data.formids +' #selectedSentMsgIDs').val();
		}
		if(selectedInboxMsgIDs== undefined || selectedInboxMsgIDs == ''){
			ns.common.showStatus("Please select messages to delete!");
			return false;
		}
	});


	$.subscribe('DeletingSentMessagesConfirmComplete', function(event,data) {
		//alert('completeConfirm');
	});

	$.subscribe('errorDeletingSentMessagesConfirm', function(event,data) {
		//alert('errorConfirm');
		
	});

	$.subscribe('successDeletingSentMessagesConfirm', function(event,data) {
		//alert('successConfirm');
		$('#DeleteMessagesConfirmDialogID').dialog('open');
	});


	
	/**
	 * Loading Reply Form
	 */
	$.subscribe('beforeLoadingReplyForm', function(event,data) {
		//alert('beforeLoadingReplyForm');
		//$("#actionMsgPortlet").hide();
	});

	$.subscribe('loadingReplyFormComplete', function(event,data) {
		//alert('loadingReplyFormComplete');
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#sendReplyForm").setFocus();
		}		
		
	});

	$.subscribe('errorLoadingReplyForm', function(event,data) {
		//alert('errorLoadingReplyForm');
		
	});

	$.subscribe('successLoadingReplyForm', function(event,data) {
		//alert('successLoadingReplyForm');
		//$('#messageView').pane('title', $('#subjectInViewMsg').html());
		//$('#messageView').pane('showHeader');
		//$('#messageView').pane('fold');
		//$('#messageReply').pane('title', $('#subjectInReplyMsg').html());
		//$('#messageReply').pane('show');
		//$('#messageReply').pane('expand');
		$('#messageView').slideUp();
		$('#messageReplyWrapper').slideDown();
		ns.common.updatePortletTitle("actionMsgPortlet", js_reply_message, false);
	});	
	

	
	/**
	 * Deleting Confirmation (From View)
	 */
	$.subscribe('beforeDeletingMessageConfirm', function(event,data) {
		//alert('beforeDeletingMessageConfirm');
	});

	$.subscribe('deletingMessageConfirmComplete', function(event,data) {
		//alert('deletingMessageConfirmComplete');
		
	});

	$.subscribe('errorDeletingMessageConfirm', function(event,data) {
		//alert('errorDeletingMessageConfirm');
		
	});

	$.subscribe('successDeletingMessageConfirm', function(event,data) {
		//alert('successDeletingMessageConfirm');
		$('#DeleteMessagesConfirmDialogID').dialog('open');

	});	


	/**
	 * Delete Messages
	 */
	$.subscribe('beforeDeletingMessage', function(event,data) {
		//alert('beforeDeletingMessage');
	});

	$.subscribe('deletingMessageComplete', function(event,data) {
		//alert('deletingMessageComplete');
		
	});

	$.subscribe('errorDeletingMessage', function(event,data) {
		//alert('errorDeletingMessage');
		
	});

	$.subscribe('successDeletingMessage', function(event,data) {
		//alert('successDeletingMessage');
		ns.common.showSummaryOnSuccess("summary","done");
		ns.common.showStatus();
		$.publish('closeDialog');
		$('#details').slideUp();
		
		$.publish('refreshMessageSmallPanel');
		$('#messageTabs').portlet('expand');
		$('#messageTabs').show();
	    $('#details').slideUp();
	    $('#selectedInboxMsgIDs').val('');
        $('#allSelectedInboxMsgIDs').val('');
	});	

	/**
	 * Send Reply
	 */
	$.subscribe('beforeSendingReply', function(event,data) {
		//alert('beforeSendingReply');
	});

	$.subscribe('sendingReplyComplete', function(event,data) {
		//alert('sendingReplyComplete');
		
	});

	$.subscribe('errorSendingReply', function(event,data) {
		//alert('errorSendingReply');
		
	});

	$.subscribe('successSendingReply', function(event,data) {
		//alert('successSendingReply');
		ns.common.showSummaryOnSuccess("summary","done");
		ns.common.showStatus();
		$.publish('cancelReply');
		$('#messageTabs').portlet('expand');
		$('#messageTabs').show();
	    $('#details').slideUp();
	});	
	
	
	$.subscribe('cancelReply', function(event,data) {
	    
		$.log('About to cancel Form! ');
		//$('#messageReply').pane('hide');
		$('#messageReplyWrapper').slideUp();
		//$('#messageView').pane('hideHeader');
		//$('#messageView').pane('expand');
		$('#messageView').slideDown();
		ns.common.updatePortletTitle("actionMsgPortlet", js_view_message, false);
		$("#loadReplyFormSubmit").focus();
		ns.common.selectDashboardItem("goBackSummaryLink");
	});

	
	$.subscribe('cancelForm', function(event,data) {
		$.log('About to cancel Form! ');
		
		ns.common.selectDashboardItem("goBackSummaryLink");
		ns.message.closeDetails();
		ns.common.hideStatus();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			accessibility.setFocusOnDashboard();
		}
		$('#messageTabs').portlet('expand');
		$('#messageTabs').show();
	    $('#details').slideUp();
	    
	});
	
	
	$.subscribe('mailboxChanged', function(event,data) {
		if ('Inbox' === ns.message.mailbox) {
			/*$('#deleteInboxMessageLink').removeAttr('style');
			$('#deleteSentMessageLink').attr('style','display:none');*/
		} else if ('Sentbox' === ns.message.mailbox) {
			/*$('#deleteSentMessageLink').removeAttr('style');
			$('#deleteInboxMessageLink').attr('style','display:none');*/
		}

	});
	$.publish('mailboxChanged');

	
	$.subscribe('messages.clearMessageBoxArea', function(event,data) {
		$("#subject").val('');
		$("#sendMemo").val('');
		CKEDITOR.instances.sendMemo.setData('');
	});
	
	$.subscribe('reloadCurrentMessagesGrid', function(event,data) {
		$.publish('refreshMessageSmallPanel');
		if ('Inbox' === ns.message.mailbox) {
			$('#inboxMessagesGridId').trigger("reloadGrid");
		} else if ('Sentbox' === ns.message.mailbox) {
			$('#sentMessagesGridId').trigger("reloadGrid");
		}
	});
	
	$.subscribe('openMessageSummary', function(event,data) {
		$('#messageTabs').portlet('expand');
		$('#messageTabs').show();
	    $('#details').slideUp();
	});
	
	$.subscribe('closeMessageDialog', function(event,data) {
		$('#selectedSentMsgIDs').val('');
		$('#selectedInboxMsgIDs').val('');
		ns.common.closeDialog();
	});
};



/**
 * Close Details island and expand Summary island
 */
ns.message.closeDetails = function(){
	$('#messageTabs').portlet('expand');
	$('#messageTabs').show();
    $('#details').slideUp();
}



$(document).ready(function() {
	$.debug(false);
	ns.message.setup();
});

