
/******************** Function required for form submission using AJAX ******************/
// This function will initiate an AJAX call to save Account settings for logged in user.
ns.userPreference.saveAccountPreference = function() {
	var urlString = '/cb/pages/jsp/user/SaveAccountPrefAction.action';
	$.ajax({
		type: "POST",
		url: urlString,
		data: $("#accountsPrefSetup").serialize(),
		dataType: 'HTML',
		success: function(data) {
			ns.common.closeDialog('accountPrefsDialogId');
			ns.common.showStatus(data);
			//If user has come from Online statement module to add accounts, stay on same menu and reload the module, else restart from Home 
			if(ns.home.currentMenuId == 'acctMgmt_onlineStatements'){
				setTimeout('ns.shortcut.navigateToSubmenus("acctMgmt_onlineStatements")','10');	
			}else{
				$.publish('quickSaveAccountPreferencesSetting');
			}
			
		},
		 error: function(xmlHttpRequest, textStatus, errorThrown){
			ns.common.showError(xmlHttpRequest.responseText);		
			ns.common.closeDialog("accountPrefsDialogId");
		}
	});
	return false;
};

$.subscribe('quickSaveAccountPreferencesSetting', function(event,data) {
	ns.common.closeDialog("accountPrefsDialogId");
	setTimeout(function(){
		var location = window.location.href.substring(0,window.location.href.indexOf("#"));
		 window.location.href = location;
	},500);
});

//This function will initiate an AJAX call to save Menu settings for logged in user.
ns.userPreference.saveMenuPreference = function() {
	// Get tabs to be displayed as decided by logged in user
	var ids = ns.userPreference.getDisplayTabs();

	// Get start page value selected by User.
	var startPageValue = $('#startPageSelect').val();	

	// Construct URL and Post data using AJAX call
	var urlString = '/cb/pages/jsp/user/EditMenuPreferenceAction.action';
	$.ajax({
		type: "POST",
		url: urlString,
		dataType: 'JSON',
		data: {
			tabsToDisplay:ids,
			startPage:startPageValue,
			CSRF_TOKEN: document.getElementById('CSRF_TOKEN').value	},
		success: function(data) {
			$("#resultDialog").dialog({
				   beforeClose: function(event, ui) {
					   // Reload/refresh whole page to apply changes.
					   ns.userPreference.reloadUserPreference();
					}
			});
			$("#resultDialog").dialog('open');
			$("#resultDialog").attr("style", "min-height:0px");
		}
	});
	return false;
};

/************** UTILITY FUNCTION SECTION ********************/
// This function will provide all the menus displayed to user.
// These menus are shown in "Menus" tab in a List box.
ns.userPreference.getDisplayTabs = function() {	
	var tabs = $('#displayTabs').children();
	var ids = "";
	if(tabs) {
		$.each(tabs, function() {
			ids += $(this).attr('id') + ",";
		});
	}	
	return ids;
};

//This function will take care of navigating back to the back URL page, if cancel button is pressed.
ns.userPreference.GoBackToURLOnCancel = function (backLinkURL){
	if(backLinkURL != null) {
		ns.shortcut.goToFavorites(backLinkURL);
	} else {
		$('#ffiHomeID').click();
	}
	return false;
};

//This function will navigate to the Add Bill Pay and Online Banking Accounts page in Service Center module.
ns.userPreference.addInternalAccount = function(selectedValue) {
	ns.common.closeDialog('accountPrefsDialogId');
	
	var url = "/cb/pages/jsp/servicecenter/index.jsp?";
	var CRSF = $("#CSRF_TOKEN");

	if(selectedValue=='jsp.user.preferences.account.add.internal.account.addBillPay'){
		ns.common.loadSubmenuSummary(url+"'"+CRSF+"'"+'Initialize=true&amp;subpage=services','serviceCenter_services','ServiceCenter','addAccountsLink');
	}
	else if(selectedValue=='jsp.user.preferences.account.add.internal.account.addOnlineBankingAccounts'){
		ns.common.loadSubmenuSummary(url+"'"+CRSF+"'"+'Initialize=true&amp;subpage=services','serviceCenter_services','ServiceCenter','addOnlineAccountsLink');
	}
};

//This function will reset the tab passed. This is helpful for all reset functionalities provided in Preferences tabs.
ns.userPreference.resetPreferences = function(tabToBeSelected){	
	if(tabToBeSelected == 'MY_PROFILE_TAB'){
		$("#preferencesTabs").tabs("load", $("#myProfileTab").index());
		$("#preferencesTabs").tabs('option','active', $("#myProfileTab").index());
	}else if(tabToBeSelected == 'ACCOUNT_TAB'){
		$("#preferencesTabs").tabs("load", $("#accountSettingTab").index());
		$("#preferencesTabs").tabs('option','active', $("#accountSettingTab").index());
	}else if(tabToBeSelected == 'MENU_TAB'){		
		$("#preferencesTabs").tabs("load", $("#menuTab").index());		
		$("#preferencesTabs").tabs('option',"active", $("#menuTab").index());
	}else if(tabToBeSelected == 'MISC_TAB'){
		$("#preferencesTabs").tabs("load", $("#otherSettingTab").index());
		$("#preferencesTabs").tabs('option','active', $("#otherSettingTab").index());
	}else{
		$("#preferencesTabs").tabs("load", 0);
		$("#preferencesTabs").tabs('option',"active", 0);
	}
};

ns.userPreference.reloadUserPreference = function(){
	//Show loading symbol
	ns.common.showLoading();
	$(window).unload(function() {
		ns.common.hideLoading();
	});
	window.location.reload();
};


//This function will remove bound click event, which could be attached while loading my profile page.
ns.userPreference.removeBoundEvent = function(){
	//Remove bound click event, if it is attached.
	$("#subList li").unbind("click",clearUserPrefVerifiedFlagForSubmenu);
	$("#tabbuttons li a").unbind("click",clearUserPrefVerifiedFlagForMainmenu);
};

/************************ Topics Subscription ********************/
// After successfully saving User profile
$.subscribe('afterSuccessfulSave', function(event,data) {
	$("#resultDialog").dialog({
		   beforeClose: function(event, ui){
			// Reload/refresh whole page to apply changes.
			   ns.userPreference.reloadUserPreference();
		   }
	});
	$("#resultDialog").dialog('open');
});

// Topic to remove validation error, if any, before User Profile form submission
$.subscribe('beforeVerifyUserProfile', function(event,data) {
	removeValidationErrors();
});

// This topic will be called from SecurityCheck.jsp, once credential validation will be done.
$.subscribe('reloadUserProfileTab', function(event,data) {
	ns.userPreference.resetPreferences('MY_PROFILE_TAB');
});

$.subscribe('preferencesTabsCompleteTopics', function(event,data) {	
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#preferencesTabs").setFocusOnTab();
	}
});

// note - this needs to reload since the show session page may have changed which requires index.jsp to be reloaded.
$.subscribe('saveOtherPreferences', function(event,data) {
	$("#resultDialog").dialog({
		   beforeClose: function(event, ui) {
			// Reload/refresh whole page to apply changes.
			   ns.userPreference.reloadUserPreference();
			}
	});
	 var resStatus = event.originalEvent.request.status;
		if(resStatus == 200) {
			$("#resultDialog").dialog('open');
		}
});
