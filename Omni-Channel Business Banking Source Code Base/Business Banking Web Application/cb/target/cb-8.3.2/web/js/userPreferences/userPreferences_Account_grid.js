/************************ Topics Subscription ********************/
// Subscribe event to fire when Internal grid loading completes
$(document).subscribe('ns-userPreference-makeInternalAccGridRowsEditable', function(event, data){
	//Get no. of rows in the grid
	var noOfRows = jQuery("#internalAccountsGrid").jqGrid('getGridParam','records');
			
	//Iterate over all rows and make them editable
	for(var counter = 1; counter <= noOfRows; counter++){
		jQuery("#internalAccountsGrid").jqGrid('editRow',counter, false, function(rowId){
			ns.userPreference.setProperIdsToEditableControls(rowId, 'internalAccountsGrid');});
	}
});

//Subscribe event to fire when External grid loading completes
$(document).subscribe('ns-userPreference-makeExternalAccGridRowsEditable', function(event, data){
	//Get no. of rows in the grid
	var noOfRows = jQuery("#externalAccountsGrid").jqGrid('getGridParam','records');
			
	//Iterate over all rows and make them editable
	for(var counter = 1; counter <= noOfRows; counter++){
		jQuery("#externalAccountsGrid").jqGrid('editRow',counter, false, function(rowId){
			ns.userPreference.setProperIdsToEditableControls(rowId, 'externalAccountsGrid');});
	}
});


/************** UTILITY FUNCTION SECTION ********************/
// Function to mangle ids of editable controls in a grid row. This is needed to post data 
// correctly on the server side.
ns.userPreference.setProperIdsToEditableControls = function (rowId,gridId){
	//Get all input tags in a row, using rowId supplied
	var inputElements = jQuery("#"+gridId).find("#"+rowId).find("input");
	
	//Get rowData, a map representing values shown in a table row against their column names as key
	var rowData = jQuery("#"+gridId).jqGrid('getRowData', rowId);
	var numberMaskedValue = rowData["numberMasked"];
	var isAcctActive = rowData["map.ActiveAcct"].search(/checked/i) != -1? true : false;
		
	// Iterate over input tag array and change their ids as desired.
	var element = null;
	for(var counter = 0; counter < inputElements.length; counter++){
		element = inputElements[counter];
		if(element.type == "checkbox"){
			//Check if element is Active check box or not
			if(element.id.search('Active') != -1){
				element.name = "Acct"+numberMaskedValue+"active";
				if(!element.checked){
					disabled = true;
				}
				// provide onclick event handler
				element.onclick = function(){
					ns.userPreference.toggleEditableControlState(this);
				};
			}
			
			//Check if element is OnlineStatement check box or not
			if(element.id.search('Online') != -1){
				element.name = "Acct"+numberMaskedValue+"statements";
				element.disabled = !isAcctActive;
			}
		}else if(element.type == "text"){
			element.name = "Acct"+numberMaskedValue+"nickname";
			element.disabled = !isAcctActive;
		}
	}
};

ns.userPreference.formatCurrentBalance= function(cellvalue, options, rowObject) {
	var action = cellvalue.toString();
	if((rowObject['map'])['CurrentBalance']!=null && (rowObject['map'])['CurrentBalance']!=undefined && (rowObject['map'])['CurrentBalance']!=""){
			 var isNegative = '0.00';
			 isNegative = (rowObject['map'])['CurrentBalance'].toString().trim();
			 if(isNegative.indexOf("-") === 0){
			 action = action.replace("-","");	 
			 action = "<font color='red'>"+action+"</font>";
			 }
		}
	return action;
};

// Function to toggle disabled state of Other editable controls depending on
// whether Active checkbox is checked or not.
ns.userPreference.toggleEditableControlState = function (activeChkBox){
	// Get parent row and find elements in parent Row to enable or disable
	var inputElements = $(activeChkBox).parents('tr:first').find("input");
		
	// Iterate over input tag array and enable or disable them
	for(var counter = 0; counter < inputElements.length; counter++){
		if(inputElements[counter].id == activeChkBox.id){
			continue;
		}else{
			inputElements[counter].disabled = !activeChkBox.checked;
			
			//If element of type checkbox then uncheck that
			if(inputElements[counter].type == 'checkbox' && !activeChkBox.checked){
				inputElements[counter].checked = false;
			}
		}	
	}
};
