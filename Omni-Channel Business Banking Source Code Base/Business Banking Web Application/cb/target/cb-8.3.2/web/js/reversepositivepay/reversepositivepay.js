ns.cash.setup = function(){
	ns.cash.gotoReversePositivePay();
};

ns.cash.gotoReversePositivePay = function(){
	ns.common.refreshDashboard('dbReversePositivePaySummary');
};

$.subscribe('buildRPPayFormBefore', function(event,data) {
	removeValidationErrors();
});

$.subscribe('buildRPPayFormSuccess', function(event,data) {
	ns.cash.gotoStep(2);
});

$.subscribe('buildRPPayFormError', function(event,data) {
});

$.subscribe('buildRPPayFormComplete', function(event,data) {
});

$.subscribe('clearValidationErrorsTopic', function(event,data) {
	$('.errorLabel').html('').removeClass('errorLabel');
	$('#formerrors').remove();
});

$(document).ready(function(){
	$.debug(false);
	ns.cash.setup();
});

//Show the active dashboard based on the user selected summary
ns.cash.showDashboard = function(userActionType,dashboardElementId){
	ns.cash.refreshRPPayGridURL = "/cb/pages/jsp/reversepositivepay/reversepositivepay_exception_grid.jsp";
	ns.cash.refreshRPPayGrid(ns.cash.refreshRPPayGridURL);
	ns.common.refreshDashboard('dbReversePositivePaySummary');
	
	//Simulate dashboard item click event if any additional action is required
	if(dashboardElementId && dashboardElementId !='' && $('#'+dashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+dashboardElementId).trigger("click");}, 10);
	}
};

showRPPayGridSummary = function(gridTabId){
	//Hide all Grid Summary containers
	$(".gridSummaryContainerCls" ).each(function(i) {
        $(this).hide();
    });
	
	var gridSummaryId  = gridTabId + "SummaryGrid";

	//Show the user selected Grid Summary container
    $('#'+gridSummaryId).show();
    $('#'+gridSummaryId).removeClass('hidden');
    var gridId = $('#'+gridSummaryId).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
    //By default to avoid grid loading, datatype is local, make it json and reload grid.
    ns.common.setFirstGridPage('#'+gridId);
    $('#'+gridId).jqGrid('setGridParam',{datatype:'json'}).trigger("reloadGrid");
    
	//Find user selected grid tab
	var userSelctorGridTab = gridTabId;
	//Hide user selected grid tab from dropdown menu.
	//$("#"+userSelctorGridTab).hide();
	
	//Get title of user selected grid tab
	var selectedGridTabTitle = $("#"+userSelctorGridTab).html().trim();
	if(selectedGridTabTitle != undefined && selectedGridTabTitle!= ''){
		$("#selectedGridTab").html(selectedGridTabTitle); //Set selected grid tab title
	}
	if(gridTabId == 'approvalRPPay') {
		helpGridId = "approvalRPPaySummaryGrid";
		$('#rppay_workflowID .wizardStatusContainerCls').hide();
		$('#rppay_workflowID .searchPanelToggleAreaCls').hide()
		if($('#allTypesOfAccount').is(':visible')) {
			$('#allTypesOfAccount').slideToggle();
		}
	} else {
		helpGridId = "pendingRPPaySummaryGrid";
		$('#rppay_workflowID .wizardStatusContainerCls').show();
		$('#rppay_workflowID .searchPanelToggleAreaCls').show()
		ns.cash.gotoStep(1);
	}
	
};
