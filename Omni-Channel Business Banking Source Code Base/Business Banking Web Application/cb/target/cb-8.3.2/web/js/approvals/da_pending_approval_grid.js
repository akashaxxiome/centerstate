
/** Funtion to add dynamic dropdown of approval actions and adding reject reason text field. */
ns.approval.addActionsRowsForDAPendingApprovalGrid = function()
{
	$(".approvalViewkBox").css({"display":"inline-block", "width":"82px","height":"auto", "margin":"5px","position":"relative"}).addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-selectmenu");
	var ids = jQuery("#daApprovePaymentsGrid").jqGrid('getDataIDs');
	for(var i=1;i<=ids.length;i++){  
		$("#DAApprovalCheckBox" + i).selectmenu({width:82});
        var optionVal = $('#DAApprovalCheckBox' + i).val();
		ns.approval.checkIfDisplayRejectDA( optionVal, "displayReasonDA" + i, "daReason" + i + "Error");
	}
	
	// add startIndex and rows hidden field
	var startIndex = $("#daApprovePaymentsGrid").jqGrid('getCell', 1, 'rn');
	var rows = ids.length;
			
	if(document.getElementsByName("startIndexDA").length == 0)
	{
		$("#daApprovePaymentsGrid").append("<input type='hidden' name='startIndexDA' value='" + startIndex + "' />");
		$("#daApprovePaymentsGrid").append("<input type='hidden' name='submitRowsDA' value='" + rows + "'/>");
	}
	else
	{
		$("#daApprovePaymentsGrid input:hidden[name='startIndexDA']").val(startIndex);
		$("#daApprovePaymentsGrid input:hidden[name='submitRowsDA']").val(rows);
	}
};

ns.approval.escapeCharacters = function()
{
	var ids = jQuery("#daApprovePaymentsGrid").jqGrid('getDataIDs');
	var indexInput = "";
	var reasonColspan = 0;
	for(var i=0;i<ids.length;i++){
		var inputID = "#reasonInputID" + i;
		if( $(inputID).length > 0 ){
			var oriReason =  $("#reasonInputID" + i).attr("value");
			var escapedReason = ns.common.spcialSymbolConvertion( oriReason );
			$("#reasonInputID" + i).attr("value", escapedReason);
		}
	}
};

$.subscribe('escapeSpecialCharacters', function(event,data) {
	ns.approval.escapeCharacters();
});

/** Funtion to add view link in pending payees grid */
ns.approval.formatDAPendingApprovalActionLinks = function(cellvalue, options, rowObject)
{
	var view = "";
	if (rowObject.itemType == 'ACHPayee') {
		view = "<span align=\"center\"><a class='approvalViewkBox' href=\"#\" onClick=\"ns.approval.viewDAPendingApprovalDetails('"+ rowObject.map.ACHPayeeViewURL +"');\">" +js_view+ "</a></span>";
	} else {
		view = "<span align=\"center\"><a class='approvalViewkBox' href=\"#\" onClick=\"ns.approval.viewDAPendingApprovalDetails('"+ rowObject.map.WirePayeeViewURL +"');\">" +js_view+ "</a></span>";
	}
	
		// Set hidden fields
		var indexNumber = options.rowId;
		var id = rowObject.ID;
		var itemType = rowObject.itemType;
		var action = rowObject.action;
		var daItemId = rowObject.daItemId;
		var submittedByName = rowObject.submittedByName;
		var finalHolder='';
		
		var hiddenFields = "<input type=\'hidden\' name=\'DaItemId\' value=" + daItemId +">";
		hiddenFields += "<input type=\'hidden\' name=\'ID\'	value=" + id + ">";
		hiddenFields += "<input type=\'hidden\' name=\'ItemType\'	value=" + itemType + ">";
		hiddenFields += "<input type=\'hidden\' name=\'UserAction\'	value=" + action + ">";
		hiddenFields += "<input type=\"hidden\" name=\"SubmittedBy\" value=\'" + submittedByName + "'>";
		
		var optionStr = "<option value=" + "'" +  ns.approval.decision_hold + "'" +  " selected>" + js_appr_hold + "</option>";
		optionStr += "<option value=" + "'" +  ns.approval.decision_approved + "'" +  ">" + js_appr_approved + "</option>";
		optionStr += "<option value=" + "'" +  ns.approval.decision_rejected + "'" +  ">" + js_appr_rejected + "</option>";

		var cellApprovalActions = "<select class=\"txtbox\" id=\"DAApprovalCheckBox" + indexNumber + "\" name=\"approvalActionsDA\" onchange='ns.approval.checkIfDisplayRejectDA( $(this).val(), \"displayReasonDA" + indexNumber + "\", \"daReason" + indexNumber + "Error\");' >"
			+ optionStr + "</select>";
		
		var itemIndexValue= indexNumber-1;
		var daReasonSpan = "daReason" + itemIndexValue + "Error"; 
		var daReasonErrorCell = "<span id=\"" +daReasonSpan + "\"></span>";
		
		var cellReason = "<div id=\"displayReasonDA" + indexNumber + "\">&nbsp;&nbsp;&nbsp;" + js_reason + "&nbsp;&nbsp;&nbsp;"
			+ "<input id=\"reasonInputID" + indexNumber + "\" + class=\"ui-widget-content ui-corner-all small-input\" type=\"text\" name=\"daReason\" style=\"width:300px\" size=\"70\" maxlength=\"120\" value='' onChange='ns.approval.verifyReasonDA( document.daPendingApprovalForm, \"" + indexNumber + "\" );' ></div>"  
			+ daReasonErrorCell;
		
		indexInput = "<input type=\'hidden\' name=\'itemIndexDA\' value=\'" + itemIndexValue + "\"'>";
		
		finalHolder=cellApprovalActions + view + cellReason + indexInput + hiddenFields;
				
		return finalHolder;
};

ns.approval.formatDaItemId = function(cellvalue, options, rowObject)
{
	var view = "<input type=\"hidden\" name=\"DaItemId\"	value='" + cellvalue + "'>";
	var hiddenFields = "<input type=\"hidden\" name=\"ID\"	value='" + rowObject.ID + "'>";
		hiddenFields += "<input type=\"hidden\" name=\"ItemType\"	value='" + rowObject.itemType + "'>";
		hiddenFields += "<input type=\"hidden\" name=\"UserAction\"	value='" + rowObject.action + "'>";
		
	return view + hiddenFields;
};

ns.approval.viewDAPendingApprovalDetails = function(urlString)
{
	//var urlString = detailsURL;
	$.ajax({
		url: urlString,
		success: function(data){
			
			var heading = $('#PageHeading', data).html();
			var $title =$('#ui-dialog-title-viewDAPendingApprovalDialogID');
			$title.html(heading);
			
			$('#viewDAPendingApprovalDialogID').html(data).dialog('open');
			
			ns.common.resizeDialog('viewDAPendingApprovalDialogID');
		}
	});
};

$.subscribe('daPendingApprovalGridCompleteEvents', function(event,data) {
	ns.approval.addActionsRowsForDAPendingApprovalGrid();
	ns.common.resizeWidthOfGrids();
	$("#daPendingApprovalForm").show();
});

$.subscribe('daSubmitPendingApprovalFormComplete', function(event,data) {
	//$("#targetPending").html(data);
});


$.subscribe('daSubmitPendingApprovalFormSuccessTopics', function(event,data) {
	$("#daPendingApprovalForm").hide();
	$("#targetApprovePaymentsVerifyContainer").show();
	$("#pendingPayeesFormAction").hide();
	$("#approvalGroupSummary").hide();
});

ns.approval.returnApprovePayeesToFromURL = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data) {
			$("#inputDiv").html(data);
		}
	});	
}

/** Confirm approval of pending payees data. - Click topic */
$.subscribe('confirmApprovePayeesFormClick', function(event,data) {
	$("#daPendingApprovalForm").show();
	$("#targetApprovePaymentsVerifyContainer").hide();
});

/** Confirm approval of pending payees data. - Complete topic */
$.subscribe('confirmApprovePayeesFormComplete', function(event,data) {
	if(event.originalEvent.status != "error") {
	$("#daPendingApprovalForm").show();
	$("#targetApprovePaymentsVerifyContainer").hide();
	$("#summary").show();
	//$("#approvalGroupSummary").show();
	//$('#daApprovePaymentsGrid').trigger("reloadGrid");
	ns.shortcut.navigateToSubmenus('approvals_pending,pendingPayess');
}
});

/** Cancel approval of pending payees data. */
$.subscribe('cancelApprovePayeesFormComplete', function(event,data) {
	$("#daPendingApprovalForm").show();
	$("#targetApprovePaymentsVerifyContainer").hide();
	$("#pendingPayeesFormAction").show();
	$("#approvalGroupSummary").show();
});

/** submit from verify page - remove the vefiry dialog */
$.subscribe('submitApprovePayeesVerifyFormBefore', function(event,data) {
	$('.includeApprovalDialog').each(function(){
		$(this).remove();
	});
});


/** Function to verify the reason text length. */
ns.approval.verifyReasonDA = function( thisForm, count )
{
	count = parseInt(count);
	if( count == 0 ) {
		if (thisForm.daReason.value != undefined) {
			if( thisForm.daReason.value.length > 120 ) {
				window.alert(js_appr_reason_length);
				thisForm.daReason.value = thisForm.daReason.value.substr( 0, 120 );
			}
		}
	} else {
		if( thisForm.daReason[count].value.length > 120 ) {
			window.alert(js_appr_reason_length);
			thisForm.daReason[count].value = thisForm.daReason[count].value.substr( 0, 120 );
		}
	}
};


/** Function to show the reason text field if user selects Approved/Rejected action. */ 
ns.approval.checkIfDisplayRejectDA = function( optionVal, idOfRejectDisplay, idOfError)
{	
	displayObj = document.getElementById( idOfRejectDisplay );
	if (optionVal == ns.approval.decision_rejected || optionVal == ns.approval.decision_approved ) {
		displayObj.style.visibility='visible';
		displayObj.style.marginBottom='5px';
	} else {
		displayObj.style.visibility='hidden';
	}
    if (optionVal != ns.approval.decision_rejected)
    {
        // reset the error if it is showing.
        $("#"+idOfError).html("");
    }
};

/** Funation to apply dropdown approval action to all the drop down of DA items. */
ns.approval.applyToAllComboBoxesDA = function(optionForAllName )
{
	$('select[id^="DAApprovalCheckBox"]').selectmenu().selectmenu("index", $('#' + optionForAllName).prop('selectedIndex'));
	$('a[id^="DAApprovalCheckBox"]').each(function (index, Element) {
		var i = index + 1;
		var optionVal = $('#DAApprovalCheckBox' + i).val();
		ns.approval.checkIfDisplayRejectDA(optionVal, "displayReasonDA" + i, "daReason" + i + "Error");
	});
};
