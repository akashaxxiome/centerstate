ns.transfer.setup = function(){
	$.subscribe('toggleTransferGrids', function(event,data){
		$('#transferGridTabs').hide();
		$('#transferTemplateGridTabs').hide();
	});
	
	/*
	 * Control dashboard button display
	 */
	$.subscribe('templatesSummaryLoadedSuccess', function(event,data){
		ns.transfer.gotoTemplates();
		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
				accessibility.setFocusOnDashboard();
		}
		$("#details").hide();
	});

	$.subscribe('externalAccountLoadedSuccess', function(event,data){
		ns.transfer.gotoTransfers();
		ns.transfer.gotoTemplates();
		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
				accessibility.setFocusOnDashboard();
		}
	});
	
	$.subscribe('transfersSummaryLoadedSuccess', function(event,data){
		ns.transfer.gotoTransfers();
		$.publish("common.topics.tabifyDesktop");
		if (accessibility) {
				accessibility.setFocusOnDashboard();
		}
	});


	/*
	 *  Load transfer form
	 */
	$.subscribe('beforeLoadTransferForm', function(event,data) {
		$.log('About to load form! ');
		$('#details').slideUp();
		$('#detailsPortlet').show();
		$('#quicksearchcriteria').hide();
		$('#quickSearchLink').hide();

		// When user click calender and then after clicks new / multiple payment link then this code is used to hide calender
		if ($("#calendar").length > 0) {
			//console.info("beforeLoadTransferForm");
			//$('#goBackSummaryLink').click();
			$('#summary').empty();
		}
		$("#calendar").hide();
		
		
		//displays/shows Details/Review/Complete tabs if already hide - hides Details/Review/Complete tabs in case of if user if doesn't have permission 'Transfers Add External Account.
		if($("#detailsPortletWizardStatusId").not(':visible')){
			$("#detailsPortletWizardStatusId").show();
		} 
	});
	
	$.subscribe('loadMultipleForm', function(event,data) {
		$("#transferFileImportLink").hide();
		$("#transferFileImportHolder").show();
	});
	
	$.subscribe('loadSigleTransferFormComplete', function(event,data) {
		$.log('Form Loaded! ');
		//$('#transferGridTabs').portlet('fold');
		//$('#transferTemplateGridTabs').portlet('fold');
		$('#transferGridTabs').hide();
		$('#transferTemplateGridTabs').hide();
		$('#detailsPortlet').portlet('expand');
		$('#transferFileImportLink').attr('style','display:none');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.transfer.gotoStep(1);
		$('#details').slideDown();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
			$("#"+selectedTab).find(":tabbable:first").focus();			
		}
	});

	$.subscribe('loadTransferFormComplete', function(event,data) {
		$.log('Form Loaded! ');
		$('#transferGridTabs').hide();
		$('#transferTemplateGridTabs').hide();
		//$('#transferGridTabs').portlet('fold');
		//$('#transferTemplateGridTabs').portlet('fold');
		$('#detailsPortlet').show();
		$('#detailsPortlet').portlet('expand');
		//$('#transferFileImportLink').removeAttr('style');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.transfer.gotoStep(1);
		$('#details').slideDown();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
			$("#"+selectedTab).find(":tabbable:first").focus();						
		}
	});

	$.subscribe('editSingleTransferFormComplete', function(event,data) {
		$.log('Form Loaded! ');
		ns.common.selectDashboardItem("addSingleTransferLink");
		$('#transferGridTabs').hide();
		$('#transferTemplateGridTabs').hide();
		//$('#transferGridTabs').portlet('fold');
		//$('#transferTemplateGridTabs').portlet('fold');
		$('#detailsPortlet').portlet('expand');
		$('#detailsPortlet').show();
		$('#transferFileImportLink').attr('style','display:none');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.transfer.gotoStep(1);
		$('#details').slideDown();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
			$("#"+selectedTab).find(":tabbable:first").focus();
		}
	});
	
	
	$.subscribe('hideShowEditTransferTemplateForm', function(event,data) {
			ns.common.selectDashboardItem("addMultipleTemplateLink");
			$('#transferFileImportLink').hide();
			$('#transferGridTabs').hide();
			$('#transferTemplateGridTabs').hide();
			//$('#transferGridTabs').portlet('fold');
			//$('#transferTemplateGridTabs').portlet('fold');
			$('#detailsPortlet').show();
			$('#detailsPortlet').portlet('expand');
	});


	$.subscribe('editSingleTransferTemplateFormComplete', function(event,data) {
		$.log('Form Loaded! ');
		ns.common.selectDashboardItem("addSingleTemplateLink");
		
		$('#transferGridTabs').hide();
		$('#transferTemplateGridTabs').hide();
		//$('#transferGridTabs').portlet('fold');
		//$('#transferTemplateGridTabs').portlet('fold');
		$('#detailsPortlet').show();
		$('#detailsPortlet').portlet('expand');
		$('#transferFileImportLink').attr('style','display:none');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.transfer.gotoStep(1);
		$('#details').slideDown();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
			$("#"+selectedTab).find(":tabbable:first").focus();
		}
	});

	$.subscribe('loadSingleTransferTemplateFormComplete', function(event,data) {
		$.log('Form Loaded! ');
		$('#transferGridTabs').hide();
		$('#transferTemplateGridTabs').hide();
		//$('#transferGridTabs').portlet('fold');
		//$('#transferTemplateGridTabs').portlet('fold');
		$('#detailsPortlet').portlet('expand');
		$('#detailsPortlet').show();
		$('#transferFileImportLink').attr('style','display:none');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.transfer.gotoStep(1);
		$('#details').slideDown();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
			$("#"+selectedTab).find(":tabbable:first").focus();
		}
	});

	$.subscribe('errorLoadTransferForm', function(event,data) {
		$.log('Form Loading error! ');

	});


	/*
	 * Verify transfre form
	 */
	$.subscribe('beforeVerifyTransferForm', function(event,data) {
		$.log('About to verify form! ');
		/*below added for cr 758985, as sj:a submits form in an asynchronous manner hence form values are not set but before that form 
		get submitted hence moving it to before topic.
		This topic is called from multiple locations all have same call with false, only in case of multiple(template/new/edit template)
		this is called but has no effect as the values mentioned inside are not found*/
		onSubmitCheck(false);
		removeValidationErrors();

	});

	$.subscribe('verifyTransferFormComplete', function(event,data) {
		$.log('Form Verified! ');

	});

	$.subscribe('errorVerifyTransferForm', function(event,data) {
		$.log('Form Verifying error! ');

	});

	$.subscribe('successVerifyTransferForm', function(event,data) {
		$.log('Form Verifying success! ');
		ns.transfer.gotoStep(2);

	});



	/*
	 * Send transfer form
	 */
	$.subscribe('beforeSendTransferForm', function(event,data) {
		$.log('About to send transfer! ');

	});

	$.subscribe('sendTransferFormComplete', function(event,data) {
		$.log('sending transfer complete! ');

	});

	$.subscribe('sendTransferFormSuccess', function(event,data) {
		$.log('sending transfer succeeded! ');
		//console.info("sendTransferFormSuccess");
		ns.transfer.gotoStep(3);
		//ns.common.showStatus($('#transferResultMessage').html());
		//ns.transfer.reloadTransfersSummaryGrid();
		//ns.transfer.reloadSingleAndMultipleTransferTemplates();
	});

	$.subscribe('errorSendTransferForm', function(event,data) {
		$.log('transfer error! ');

		var resStatus = event.originalEvent.request.status;
		if(resStatus === 700) //700 indicates interval validation error
			ns.common.openDialog("authenticationInfo");

	});

	/*
	 * Send Single Transfer Template
	 */
	$.subscribe('beforeSendTransferTemplate', function(event,data) {
		$.log('About to send transfer template! ');

	});

	$.subscribe('sendTransferTemplateComplete', function(event,data) {
		$.log('sending transfer template complete! ');

	});

	$.subscribe('sendTransferTemplateSuccess', function(event,data) {
		$.log('sending transfer template succeeded! ');
		ns.transfer.gotoStep(3);
		//ns.common.showStatus($('#transferResultMessage').html());
		//ns.transfer.reloadSingleAndMultipleTransferTemplates();
	});

	$.subscribe('errorSendTransferTemplate', function(event,data) {
		$.log('transfer template error! ');

		var resStatus = event.originalEvent.request.status;
		if(resStatus === 700) //700 indicates interval validation error
			ns.common.openDialog("authenticationInfo");

	});

	/*
	 * Save single transfer template
	 */
	$.subscribe('beforeSaveSingleTransferTemplate', function(event,data){
		$.log('beforeSaveSingleTransferTemplate');
		removeValidationErrors();
	});

	$.subscribe('errorSaveSingleTransferTemplate', function(event,data){
		$.log('errorSaveSingleTransferTemplate');
	});

	$.subscribe('saveSingleTransferTemplateComplete', function(event,data){			
		$.log('saveSingleTransferTemplateComplete');
	});

	$.subscribe('saveSingleTransferTemplateSuccess', function(event,data){
		$.log('saveSingleTransferTemplateSuccess');
		ns.transfer.reloadTransfersSummaryGrid();
		//ns.common.showStatus();
	});

	$.subscribe('quickSearchTransfersComplete', function(event,data) {
		$.publish("reloadSelectedGridTab");
		//$('#quicksearchcriteria').slideToggle();
		$('#details').hide();
		$('#transferGridTabs').portlet('expand');
		$('#transferGridTabs').show();
	});

	/*
	 * Delete Transfer
	 */
	$.subscribe('beforeDeleteTransfer', function(event,data){
		$.log('beforeDeleteTransfer');
	});

	$.subscribe('errorDeleteTransfer', function(event,data){
		$.log('errorDeleteTransfer');
		ns.common.showError();
		ns.common.closeDialog('#innerdeletetransferdialog');
	});
	
	$.subscribe('errorSkipTransfer', function(event,data){
		$.log('errorSkipTransfer');
		ns.common.showError();
	});

	$.subscribe('deleteTransferComplete', function(event,data){
		$.log('deleteTransferComplete');
//		toggleGridOnFormToggle();
	});

	$.subscribe('successDeleteTransfer', function(event,data){
		$.log('successDeleteTransfer');
		//ns.common.showStatus();
		ns.common.closeDialog('#innerdeletetransferdialog');
		ns.transfer.reloadTransfersSummaryGrid();
		ns.transfer.closeTransferTabs();
	});

	$.subscribe('successSaveTemplateComment', function(event,data){
		//ns.common.showStatus();
		ns.common.closeDialog('#viewMultiTransfersTemplateDialogID');
	});

	$.subscribe('cancelForm', function(event,data) {
		$.log('About to cancel Form! ');
		ns.common.selectDashboardItem("summaryBtnLink");
		ns.transfer.closeTransferTabs();
		$('#transferFileImportLink').attr('style','display:none');
		ns.common.hideStatus();
		
		$('#quickSearchLink').toggle();
		
	});

	// Function for cancel button on multiple templates form and transfers form.
	$.subscribe('cancelMultipleTransferForm', function(event,data) {
		$.log('About to cancel Form! ');
		ns.transfer.closeTransferTabs();
		$('#transferFileImportLink').attr('style','display:none');
		$('#transferGridTabs').portlet('expand');
		$('#transferGridTabs').show();
		$('#transferTemplateGridTabs').portlet('expand');
		ns.common.hideStatus();
	});
	
	// Reload current active grid from first page
	$.subscribe('reloadSelectedTransferGridTab', function(event,data) {
		//In case of Consumer we hide Pending approval tab if there are no transfers, we need to check for every search if there are any transfer satisfying entered search criteria
		$(".gridSummaryContainerCls").each(function(i){
			if($(this).is(":visible")){
		        var gridId = $(this).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
		        ns.common.setFirstGridPage('#'+gridId);
		        $('#'+gridId).trigger("reloadGrid");
			}
	    });
		
		ns.transfer.reloadHomePendingTransfersSummaryGrid();
	});	

	// Function to reload templates grid
	$.subscribe('cancelTemplatesForm', function(event,data) {
		ns.common.selectDashboardItem("templatesLink");	
		if($("#summary").not(':visible')){
			$("#summary").show();
		} 
		$.log('About to cancel templates Form! ');
		ns.transfer.closeTransferTabs();
		ns.common.hideStatus();		
	});
	
	$.subscribe('loadTransferSummary', function(event,data) {
		$('#details').hide();
		$("#summary").show();
		ns.common.refreshDashboard('dbTransferSummary');
		$('#transferfileupload').hide();
		$('#mappingDiv').hide();
		$('#customMappingDetails').hide();
	});
	
	$.subscribe('loadTransferTemplateSummary', function(event,data) {
		$('#details').hide();
		$("#summary").show();
		$('#transferfileupload').hide();
		$('#mappingDiv').hide();
		$('#customMappingDetails').hide();
		ns.common.refreshDashboard('dbTemplateSummary');
	});
	
	
	$.subscribe('loadSingleTransferForm', function(event,data) {
		ns.common.selectDashboardItem("addSingleTransferLink");
		$("#transferFileImportHolder").hide();
	});
	
	$.subscribe('loadMultiTransferForm', function(event,data) {
		ns.common.selectDashboardItem("addMultipleTransferLink");
	});
	
	$.subscribe('loadSingleTempTransferForm', function(event,data) {
		$("#transferFileImportHolder").hide();
		ns.common.refreshDashboard('dbTemplateSummary');
		ns.common.selectDashboardItem("addSingleTemplateLink");
	});
	
	$.subscribe('loadMultiTempTransferForm', function(event,data) {
		ns.common.refreshDashboard('dbTemplateSummary');
		ns.common.selectDashboardItem("addMultipleTemplateLink");
	});
	// Event to reload multiple templates grid
	$.subscribe('cancelMultipleTemplatesForm', function(event,data) {
		$.log('About to cancel templates Form! ');
		ns.common.refreshDashboard('dbTemplateSummary');
		$('#transferFileImportLink').attr('style','display:none');
		ns.transfer.closeTransferTabs();
		ns.common.hideStatus();		
	});

	$.subscribe('cancelTransferFileImportForm', function(event,data) {

		//ns.transfer.showMultipleTransferDashBoard();
		ns.transfer.cancelFileUploadTopics();
        $('#detailsPortlet').portlet('expand').show();
		ns.common.hideStatus();
		 if(ns.common.moduleType =="transfer"){
			 ns.common.refreshDashboard('dbTransferSummary');
			 ns.common.selectDashboardItem("addMultipleTransferLink");
		 }else{
			 ns.common.refreshDashboard('dbTemplateSummary');
			 ns.common.selectDashboardItem("addMultipleTemplateLink");
		 }
	});

	$.subscribe('cancelTransferTemplateFileImportForm', function(event,data) {

		ns.transfer.showMultipleTransferTemplateDashBoard();
		ns.transfer.cancelFileUploadTopics();
        $('#detailsPortlet').portlet('expand');
		ns.common.hideStatus();
		if(accessibility){
			accessibility.setFocusOnDashboard();						
		}
	});

	$.subscribe('cancelMultipleTransferNewForm', function(event,data) {
		ns.common.selectDashboardItem("summaryBtnLink");
		$('#transferFileImportLink').attr('style','display:none');
		ns.transfer.closeTransferTabs();
		ns.common.hideStatus();
	});
	
	$.subscribe('cancelMultipleTransferTemplateNewForm', function(event,data) {
		ns.common.selectDashboardItem("templatesLink");
		$('#transferFileImportLink').attr('style','display:none');
		ns.transfer.closeTransferTabs();
		ns.common.hideStatus();
	});

	$.subscribe('backToInput', function(event,data) {

		$.log('About to back to input! ');
		ns.transfer.gotoStep(1);
	});



	$.subscribe('calendarLoadedSuccess', function(event,data){
		//console.info("calendarLoadedSuccess");
		$('#quickSearchLink').hide();
		$('#quicksearchcriteria').hide();
		// $('#goBackSummaryLink').show();
	});
	$.subscribe('calendarToSummaryLoadedSuccess', function(event,data){
		// $('#goBackSummaryLink').hide();
		$('#quickSearchLink').show();

	});
	$.subscribe('onClickUploadTransferFormTopics', function(event,data){
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "false";
		ns.common.openDialog("fileImport");
		
		var configImportResults = ns.common.dialogs["fileImportResults"];
		configImportResults.autoOpen = "false";
		ns.common.openDialog("fileImportResults");	
		
	    ns.transfer.gotofileimport();
	    
	    $("#detailsPortlet").hide();
	    //$('#transfersLink').attr('style','display:none');
	    //$('#templatesLink').attr('style','display:none');
	});
	$.subscribe('transfermappingSuccess', function(event,data) {
		$('#mappingDiv').show();
		$('#transferfileupload').hide();
		$('#transferAddCustomMappingsLink').removeAttr('style');
		$('#transferCustomMappingsLink').hide();
		$("#transferFileUploadLink").show();
		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			accessibility.setFocusOnDashboard();
		}
	});
	
	$.subscribe('transferFileUploadLinkClickTopics', function(event,data) {
		ns.transfer.gotofileimport();
    });
	
    $.subscribe('transfermappingCancel', function(event,data) {
        $('#transferCustomMappingsLink').hide();
    });

	$.subscribe('openTransferFileUploadTopics', function(event,data) {
 		$("#openFileImportDialogID").dialog('open');
	    $('#details').slideDown();
 		$('#detailsPortlet').portlet('fold');
	});

	$.publish("common.topics.tabifyDesktop");
};


ns.transfer.gotofileimport = function(){
	ns.common.refreshDashboard('dbFileUploadSummary');
	$('#transferfileupload').show();
	$('#detailsPortlet').portlet('fold');

	$('#transferCustomMappingsLink').removeAttr('style');
	$('#transferfileupload').show();
	$('#customMappingDetails').hide();
	$('#mappingDiv').hide();
	$('#transferAddCustomMappingsLink').hide();

	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#transgerfileUploaderPanelID").setFocus();	
	}
	
};

ns.transfer.foldPortlet = function(summary) {
	$('#transferGridTabs').portlet('fold');
	$('#transferGridTabs').hide();

};
ns.transfer.cancelFileUploadTopics= function() {
	$('#transferfileupload').hide();
	$('#detailsPortlet').show();
};
ns.transfer.gotoTemplates = function(){
	ns.common.refreshDashboard('dbTemplateSummary');
	ns.transfer.isLoadTransferTemplate = false;
/*	$('#addSingleTemplateLink').removeAttr('style');
	$('#addMultipleTemplateLink').removeAttr('style');
	$('#transfersLink').attr('style','display:none');
	$('#addSingleTransferLink').attr('style','display:none');
	$('#addMultipleTransferLink').attr('style','display:none');
	$('#templatesLink').attr('style','display:none');
	$('#details').hide();
	$('#quickSearchLink').hide();
	$('#quicksearchcriteria').hide();
	$('#transferfileupload').hide();
	$('#mappingDiv').hide();
	$('#transferAddCustomMappingsLink').hide();
	$('#transferFileImportLink').attr('style','display:none');
	$('#customMappingDetails').hide();
	$('#transferCustomMappingsLink').attr('style','display:none');
	$('#goBackSummaryLink').hide();

	if($('#manageExternalAccountsLink').length) {
		$('#inputTab').show();
	    $('#verifyTab').show();
	    $('#confirmTab').show();
	    
	    $('#addExternalTransferAccountLink').attr('style','display:none');
	    $('#manageExternalAccountsLink').show();
	}*/
}


ns.transfer.gotoTransfers = function(){
	ns.common.refreshDashboard('dbTransferSummary');
/*	$.publish("resetSearchCriteriaFields");
	$('#addSingleTemplateLink').attr('style','display:none');
	$('#addMultipleTemplateLink').attr('style','display:none');
	$('#transfersLink').attr('style','display:none');
	$('#goBackSummaryLink').attr('style','display:none');
	$('#addSingleTransferLink').removeAttr('style');
	$('#addMultipleTransferLink').removeAttr('style');
	$('#templatesLink').attr('style','display:none');
	$('#details').hide();
	$('#quickSearchLink').show();
	$('#transferfileupload').hide();
	$('#mappingDiv').hide();
	$('#transferAddCustomMappingsLink').hide();
	$('#transferFileImportLink').attr('style','display:none');
	$('#customMappingDetails').hide();
	$('#transferCustomMappingsLink').attr('style','display:none');

	if($('#manageExternalAccountsLink').length) {
		$('#inputTab').show();
	    $('#verifyTab').show();
	    $('#confirmTab').show();
	    
	    $('#addExternalTransferAccountLink').attr('style','display:none');
	    $('#manageExternalAccountsLink').show();
	}*/
};


$.subscribe('resetSearchCriteriaFields', function(event,data){
	$("#Amount").val("");
	$("#fromAccountId").val("");
	$("#toAccountId").val("");
});

ns.transfer.gotoStep = function(stepnum){
	
	ns.common.gotoWizardStep('detailsPortlet',stepnum);
	
	var tabindex = stepnum - 1;
	$('#TransactionWizardTabs').tabs( 'enable', tabindex );	
	$('#TransactionWizardTabs').tabs( 'option','active', tabindex );
	$('#TransactionWizardTabs').tabs( 'disable', (tabindex+1)%3 );
	$('#TransactionWizardTabs').tabs( 'disable', (tabindex+2)%3 );
//	$('#TransactionWizardTabs').tabs( "option", "disabled", [0,2] );
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){						
		var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");			
		$("#"+selectedTab).find(":tabbable:first").focus();						
	}
};


/**
 * Close Details island and expand Summary island
 */
ns.transfer.closeTransferTabs = function(){
	//console.info("ns.transfer.closeTransferTabs");
	$('#transferGridTabs').show();
	$('#transferTemplateGridTabs').show();
	$('#transferGridTabs').portlet('expand');
	$('#transferTemplateGridTabs').portlet('expand');
    $('#details').slideUp();
    $('#quickSearchLink').toggle();
}

ns.transfer.viewTemplateHistory = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#viewMultiTransfersTemplateDialogID').html(data).dialog('open');
		}
	});
}

ns.transfer.viewExternalAccounts = function(urlString){

	$.ajax({
		url: urlString,
		success: function(data){
			$('#viewExternalAccountsDialogID').html(data).dialog('open');
		}
	});
}

ns.transfer.showMultipleTransferDashBoard = function(){
	ns.common.refreshDashboard('dbTransferSummary');
	
/*	$('#addSingleTemplateLink').attr('style','display:none');
	$('#addMultipleTemplateLink').attr('style','display:none');
	$('#transfersLink').attr('style','display:none');
	$('#goBackSummaryLink').attr('style','display:none');
	$('#addSingleTransferLink').removeAttr('style');
	$('#addMultipleTransferLink').removeAttr('style');
	$('#templatesLink').removeAttr('style');
	$('#quickSearchLink').show();
	$('#transferfileupload').hide();
	$('#mappingDiv').hide();
	$('#transferAddCustomMappingsLink').hide();
	$('#transferFileImportLink').removeAttr('style');
	$('#customMappingDetails').hide();
	$('#transferCustomMappingsLink').attr('style','display:none');*/
}

ns.transfer.showMultipleTransferTemplateDashBoard = function(){
	ns.common.refreshDashboard('dbTransferSummary');

	/*$('#addSingleTemplateLink').removeAttr('style');
	$('#addMultipleTemplateLink').removeAttr('style');
	$('#transfersLink').removeAttr('style');
	$('#addSingleTransferLink').attr('style','display:none');
	$('#addMultipleTransferLink').attr('style','display:none');
	$('#templatesLink').attr('style','display:none');
	$('#quickSearchLink').hide();
	$('#quicksearchcriteria').hide();
	$('#transferfileupload').hide();
	$('#mappingDiv').hide();
	$('#transferAddCustomMappingsLink').hide();
	$('#transferFileImportLink').removeAttr('style');
	$('#customMappingDetails').hide();
	$('#transferCustomMappingsLink').attr('style','display:none');*/
};

$(document).ready(function(){
	$.debug(false);
	ns.transfer.setup();
});

//Show the active dashboard based on the user selected summary
ns.transfer.showTransferDashboard = function(transferUserActionType,transferDashboardElementId){
	if(transferUserActionType == 'Transfer'){
		ns.common.refreshDashboard('dbTransferSummary');	
	}else if(transferUserActionType == 'Template'){
		ns.common.refreshDashboard('dbTemplateSummary');
	}else{//In case of normal Transfer top menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbTransferSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(transferDashboardElementId && transferDashboardElementId !='' && $('#'+transferDashboardElementId).length > 0){
		//$("#"+transferDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+transferDashboardElementId).trigger("click");}, 10);
	}
};

$.subscribe('customMappingBefore', function(event,data) {
	removeValidationErrors();	
});

// Fold transfer tabs
$.subscribe('closeTransferTabs', function(event,data) {
	ns.transfer.closeTransferTabs();
});

// Topic to clear the quick search criteria fields and removing the validation errors
$.subscribe('onClickQuickSearchTransferFormTopics', function(event,data) {
	//	QTS 715577. 
	ns.transfer.closeTransferTabs();
	ns.common.hideStatus();
	$.publish("reloadSelectedGridTab");	
	/*$('#quicksearchcriteria').toggle();*/
	$('#quicksearchcriteria').slideToggle();
	$('#quickSearchLink').toggle();
	
	// Clears quick search criteria fields excluding from and to date
	removeValidationErrors();
	$("#dateRangeValue").selectmenu('value',"");
	if($("#FromAccountId").data("ui-selectmenu") != undefined){
		$("#FromAccountId").selectmenu('value',"");
	}
	if($("#ToAccountId").data("ui-selectmenu") != undefined){
		$("#ToAccountId").selectmenu('value',"");
	}
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#details").setInitialFocus();
	}
});

/**
 * Topic defined to call Struts action, which will initialize Batch task
 */
$.subscribe('initializeBatchTask', function(event,data) {
	//Get hidden url
	var urlString = document.getElementById('hiddenInitTaskUrl').value;
	
	$.ajax
	({
		type: "GET",
		url: urlString,
		success: function(data) {
			$.publish("backToInput");
			$.publish("hideOperationResult");
		}
	});
});

$.subscribe('completeConfirmUploadedTransfersProcess', function(event,data){
	$.log('completeConfirmUploadedTransfersProcess ');
});

$.subscribe('successConfirmUploadedTransfersProcess', function(event,data){
	$.log('successConfirmUploadedTransfersProcess ');
});

$.subscribe('errorConfirmUploadedTransfersProcess', function(event,data){
	$.log('errorConfirmUploadedTransfersProcess ');
});

$.subscribe('beforeConfirmUploadedTransfersProcess', function(event,data){
	$.log('beforeConfirmUploadedTransfersProcess ');
});


$.subscribe('loadSingleTransferTemplateComplete', function(event,data){
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");
		$("#"+selectedTab).find(":tabbable:first").focus();			
	}
});

$.subscribe('loadMultipleTransferTemplateComplete', function(event,data){
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		var selectedTab = $("#TransactionWizardTabs").find("ul li.ui-state-active").attr("aria-controls");
		$("#"+selectedTab).find(":tabbable:first").focus();					
	}
});

$.subscribe("transferFileImportSuccessHandler",function(event,data){	
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#transgerfileUploaderPanelID").setFocus();
	}	
});