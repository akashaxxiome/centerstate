// Copyright Patrick Roebuck roebuck@v-page.com www.v-page.com 

var newWin;
var newWindowSimple;
var helpWindow;
var helpWindowSimple;
var strProjectFramesPagePlus = "CBHelp.html#"; // defines the name of the start or frameset page for the WebHelp project
var strProjectPath;
var strProjectPath = "\\help\\";
var strNullHelpMappingPage = "null";
var strHelpPage;
var isNav4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) == 4);

function callHelp(strProjectPath,strHelpPage)
{	
	//Check if there is no help page ('null' or empty '') mapped which will result in 404 error, so navigate to default help.
	var checkForNullHelpPage = strProjectPath + strNullHelpMappingPage;
	if(strHelpPage == checkForNullHelpPage || strHelpPage==strProjectPath){
		strHelpPage = "";
	}
	
	var strTriHelpWindowOptions = ",toolbar=yes,resizable=yes";//defines whether to display the toolbar (yes) or hides it (no)
	strTriHelpWindowOptions += ",top=40";//defines the distance from the top of the screen that the browser window is opened
	strTriHelpWindowOptions += ",left=25";//defines the distance from the left of the screen that the browser window is opened
	strTriHelpWindowOptions += ",width=750";//defines the width of the browser window
	strTriHelpWindowOptions += ",height=520";//defines the height of the browser window
	
	//QTS:754703 if help file contains the path for efs or cb set the project path accordiingly
	if(strHelpPage.indexOf("efs/")!=-1){
		strProjectPath +="efs/";
		strProjectFramesPagePlus = "cbhelp.html#"; //For EFS file is with lower case, This was failing on linux environment		
	}else{
		strProjectPath +="cb/";
	}

	var i18nStrProjectPath = strProjectPath;
	var i18nStrHelpPage = strHelpPage;
	if(typeof(ns.home.locale) != "undefined" && ns.home.locale != "en_US"){
		var i18nStrProjectPath = strProjectPath + ns.home.locale + "/";
			
		var helpPathIndex = strHelpPage.lastIndexOf('/');
		var helpPathString = strHelpPage.substring(0, helpPathIndex+1);
		var i18nHelpPathString = helpPathString + ns.home.locale + "/";
		var helpFile = strHelpPage.substring(helpPathIndex+1, strHelpPage.length);
		if (helpFile != "undefined" && helpFile !="null" && helpFile !="" ) {
			i18nStrHelpPage = i18nHelpPathString + helpFile;
		}
	} 
		
	if (isNav4){
		helpWindow = window.open("", newWin, strTriHelpWindowOptions);
		if (helpWindowSimple && !helpWindowSimple.closed){
			helpWindowSimple.close();
		}
		helpWindow.close();
		helpWindow = window.open("", newWin, strTriHelpWindowOptions);
		helpWindow.location.href = i18nStrProjectPath + strProjectFramesPagePlus + i18nStrHelpPage;
		helpWindow.focus();
	}else if (helpWindow == null || helpWindow.closed){
		if (helpWindowSimple !=null && !helpWindowSimple.closed){
			helpWindowSimple.close();
		}
		helpWindow = window.open("", newWin, strTriHelpWindowOptions);
		if(helpWindow){
			if( helpWindow.location.href == "about:blank" ) {
				helpWindow.location.href = i18nStrProjectPath + strProjectFramesPagePlus + i18nStrHelpPage;
			}else{
				helpWindow.bsscright.location.href = i18nStrHelpPage;
			}
			helpWindow.focus();			
		}
	}else if (helpWindow && !helpWindow.closed){
		if (helpWindowSimple && !helpWindowSimple.closed){
			helpWindowSimple.close();
		}
		helpWindow.bsscright.location.href = i18nStrHelpPage;
		helpWindow.focus();
	}
}

function callHelpSimple(strProjectPath,strHelpPage)
{	
	var strSingleHelpWindowOptions = ",toolbar=no";//defines whether to display the toolbar (yes) or hides it (no)
	strSingleHelpWindowOptions += ",top=40";//defines the distance from the top of the screen that the browser window is opened
	strSingleHelpWindowOptions += ",left=225";//defines the distance from the left of the screen that the browser window is opened
	strSingleHelpWindowOptions += ",width=550";//defines the width of the browser window
	strSingleHelpWindowOptions += ",height=500";//defines the height of the browser window
	if (isNav4){
	helpWindowSimple = window.open("", newWindowSimple, strSingleHelpWindowOptions);
	if (helpWindow && !helpWindow.closed){helpWindow.close();}
	helpWindowSimple.close();
	helpWindowSimple = window.open("", newWindowSimple, strSingleHelpWindowOptions);
	helpWindowSimple.location.href = strProjectPath + strHelpPage;
	helpWindowSimple.focus();
	}
	else if (helpWindowSimple == null || helpWindowSimple.closed){
	if (helpWindow && !helpWindow.closed){helpWindow.close();}
	helpWindowSimple = window.open("", newWindowSimple, strSingleHelpWindowOptions);
	helpWindowSimple.location.href = strProjectPath  + strHelpPage;
	}
	else if (helpWindowSimple && !helpWindowSimple.closed){
	if (helpWindow && !helpWindow.closed){helpWindow.close();}
	helpWindowSimple.parent.location.href = strProjectPath + strHelpPage;
	helpWindowSimple.focus();
	}
}

