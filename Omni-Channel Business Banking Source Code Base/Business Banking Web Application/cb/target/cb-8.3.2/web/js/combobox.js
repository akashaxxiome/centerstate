//convert a <select> to comboxbox with autocompletion
(function($) {
	//New feature "mouse wheel/scrool lock" on combobox(ui.autocomplete and ui.menu).
	var _autoCompleteClose = $.ui.autocomplete.prototype.close;
	$.ui.autocomplete.prototype.close = function(event){
		_autoCompleteClose.apply(this, arguments);
		$("body").unbind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox");
		$(window).unbind('resize.'+this.element.attr('id'));
		$(document).unbind('mouseup.'+this.element.attr('id'));
	};

	var _autoCompleteSuggest = $.ui.autocomplete.prototype._suggest;
	$.ui.autocomplete.prototype._suggest = function( items ) {
		_autoCompleteSuggest.apply(this, arguments);
		this._refreshPosition();
		//bind a resize event to window
		var self = this;
		$(window).bind('resize.'+this.element.attr('id'),function(){self._refreshPosition();});
		$(document).bind('mouseup.'+this.element.attr('id'),function(){
			if(!self.menu.mousein){
					self.element.blur();
				}
		});
	};
	$.ui.autocomplete.prototype.markin = function(){this.menu.mousein = true;};
	$.ui.autocomplete.prototype.markout = function(){this.menu.mousein = false;};

	//add '_refreshPosition' method to autocomplete.
	$.ui.autocomplete.prototype._refreshPosition = function(){
		this.menu.element.position({
			my: "left top",
			at: "left bottom",
			of: this.element,
			collision: "none"
		})
		if(($(this.menu.element).offset().top+this.menu.element.height())>$('body').height()) {
			if ($(this.menu.element).offset().top>this.menu.element.height()){
				this.menu.element.position({
					my: "left bottom",
					at: "left top",
					of: this.element,
					collision: "none"
				});
			}
		}
	};

	var _menuCreate = $.ui.menu.prototype._create;
	$.ui.menu.prototype._create = function(){
		_menuCreate.apply(this, arguments);
		var self = this;
		this.element.mouseenter(function(){
			$("body").unbind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox");
			self.mousein = true;
		});
		this.element.mouseleave(function(){
			if ($(this).css('display')=='block'){
				$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
				self.mousein = false;
			}
		});
	};
	
	/*override _renderItem function to convert label as HTML, with default implementation
	 * label is appending as text*/
	var _renderItem = $.ui.autocomplete.prototype._renderItem;
	$.ui.autocomplete.prototype._renderItem = function(ul, item ){		
		return $( "<li>" )
		 .append( '<a>' + item.label + '</a>' )
		.appendTo( ul );
	};

	$.widget("ui.combobox", {
		options: {
			size: '30',
			hint: 'Type to search',
			searchFromFirst : null  //this option defines whether the autocompleter search from the first character only. If it is not set to true or false and the select has 'searchFromFirst' class, it also does search from first character only.
		},

		_create: function() {
			//added a container div for alignment.
			var selectBoxDiv = $("<div class ='selectBoxHolder'></div");
			var self = this;
			var select = this.element.hide();
			select = select.wrap(selectBoxDiv);
			this.select = select;

			var source = null;
			if (this.options.searchFromFirst == true || (this.options.searchFromFirst === null && this.element.hasClass('searchFromFirst'))){
				source = function(request, response) {
						$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
						var matcher = new RegExp('^'+request.term, "i");
						response(select.children("option").map(function() {
							var text = $.trim($(this).text());
							if (this.value && (!request.term || matcher.test(text)))
								return {
									id: this.value,
									label: text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "i"), "<strong>$1</strong>"),
									value: text
								};
						}))
					};
			}else{
				source = function(request, response) {
						$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
						var matcher = new RegExp(request.term, "i");
						response(select.children("option").map(function() {
							var text = $.trim($(this).text());
							if (this.value && (!request.term || matcher.test(text)))
								return {
									id: this.value,
									label: text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
									value: text
								};
						}));
					};
			};
			var input = $("<input type='text'>")
				.insertAfter(select)
				.attr('id',this.element.attr('id')+"_autoComplete")
				.attr('tabindex',this.element.attr('tabindex')||0)
				.autocomplete({
					source: source,
					delay: 0,
					select: function( event, ui ) {
						select.val(ui.item.id);
						select.change();
						self._trigger("selected", event, {
							item: select.find("[value='" + ui.item.id + "']")
						});
						input.css('color','');
					},
					change: function(event,ui){
						if(!ui.item){
							self._clearOnBlur();
						}
					},
					minLength: 0
				})
				.addClass("combobox ui-widget ui-widget-content ui-corner-left");
			
			//Check accessibility info
			var labelId = select.attr("aria-labelledby");
			var required = select.attr("aria-required");
			if(labelId){
				input.attr("aria-labelledby", labelId);
			}
			if(required){
				input.attr("aria-required", "true");
			}
			
			//Get Font size and Family so that this could be seen consistantly in dropped menu list and
			//input text box.
			var fontSize = input.autocomplete('widget').css('font-size');
			var fontFamily = input.autocomplete('widget').css('font-family');
			if(fontSize != ''){
				input.css('font-size', fontSize);
			}
			
			if(fontFamily != ''){
				input.css('font-family', fontFamily);
			}

			input.mouseenter(function(){
				$(this).autocomplete('markin');
			});
			input.mouseleave(function(){
				$(this).autocomplete('markout');
			});

			//a fix for getting rid of hint as soon as user starts typing
			input.keyup(function(){
				var text = input.val();		
				var sText = "";
				sText = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(self.options.hint) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "");
				if(sText){
					input.val(sText);
				}				
			});
			
			input.attr('size', self.options.size);
			this.input = input;

			//save reference to select in data for ease in calling methods
			this.input.data('selectelement', this.element);
			
			var ua = $.browser;
			if ( ua.mozilla && ua.version.slice(0,3) == "1.9" ) {
				var button = $("<button/>")
				.insertAfter(input).html('&nbsp;')
				.attr("tabIndex", -1)
				.attr("title", js_showAllItems)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				}).removeClass("ui-corner-all")
				.addClass("ui-corner-right ui-button-icon")
				.click(function() {
					// close if already visible
					if (input.autocomplete("widget").is(":visible")) {
						input.autocomplete("close");
							return false;
					}
					// pass empty string as value to search for, displaying all results
					input.autocomplete("search", "");
						return false;
				});
			}
			else{
				var button = $("<button/>")
				.insertAfter(input).html('&nbsp;')
				.attr("tabIndex", -1)
				.attr("title", js_showAllItems)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				}).removeClass("ui-corner-all")
				.addClass("ui-corner-right ui-button-icon comboboxbtn")
				.click(function() {
					// close if already visible
					if (input.autocomplete("widget").is(":visible")) {
						input.autocomplete("close");
							return false;
					}
					// pass empty string as value to search for, displaying all results
					input.autocomplete("search", "");
						return false;
				});
			}
			this.button = button;

			//QTS:758711, Accessibility fix: on arror key down trigger a search or perform keydown/up on opened menu
			this.button.keydown(function(e){
				if(e.keyCode === 38 || e.keyCode === 40){
					self.input.focus();
					var $menu = $(".ui-autocomplete:visible");
					if($menu.length>0){
						$menu.menu( "focus", null, $menu.find( ".ui-menu-item:first"));				
					}else{
						var strSearchText = (self.input.val() === self.options.hint) ? "" : self.input.val();
						self.input.autocomplete("search", strSearchText);	
					}	
				}				
			});

			//Let the <input> box display the current selected value of the <select>
			//If no option is selected (value is '' or -1), display "Type to search'
			var selectedVal = select.find("option:selected").val();
        	var selectedText = select.find("option:selected").text();
			if(selectedVal != '' && selectedVal != '-1'){
				input.attr('value', $.trim(selectedText));
			} else {
				input.css('color','#c8c8c8');
				input.attr('value', self.options.hint);	       	 	
			}
		        input.click(function(){
					if($(this).val() == self.options.hint){
						$(this).attr('value', '');
						input.css('color','');
					}
		        });
	
		        input.blur(function(){					
					if($(this).val() == '' || $(this).val()==self.options.hint){
						self._clearOnBlur();
					}					
		        });
			
			if(this.element.attr('disabled')==true) this._setOption('disabled',true);

		},

		reset: function() {
			var options = this.options;
			var select = this.element;
			var selectedVal = select.find("option:selected").val();
        	var selectedText = select.find("option:selected").text();
			if(selectedVal != '' && selectedVal != '-1'){
				this.input.attr('value', $.trim(selectedText));
			} else {
				input.css('color','#c8c8c8');
				this.input.attr('value', options.hint);
			}

		},

		clear: function() {
			var options = this.options;
			this.element.attr('selectedIndex','0');
			this.input.attr('value', this.options.hint);
			this.input.css('color','#c8c8c8');
			var self = this;
			this.input.click(function(){
				var $input = $(this);
				if($input.val() == self.options.hint){
					$input.attr('value', '');
					$input.css('color','');
				}
		    });
	
		     this.input.blur(function(){
				var $input = $(this);
				if($input.val() == '' || $input.val()==self.options.hint){
					self.element.val('');
					$input.css('color','#c8c8c8');
					$input.attr('value', self.options.hint);
				}
		    });
		
		},

	    destroy: function() {
			// revert DOM to original state ...
			this.element.show();
			this.input.remove();
			this.input=null;
			this.button.remove();
			this.button=null;
			$.Widget.prototype.destroy.apply(this, arguments);
		},

		_setOption: function(key, value) {
			this.options[key] = value;
			if (key == 'disabled') {
				if(value == true){
					this.input.autocomplete('close');
					this.element
						.add(this.input)
						.add(this.button)
						.attr('disabled',true)
						.addClass(
								this.widgetFullName + '-disabled' + ' ' +
								this.namespace + '-state-disabled')
							.attr("aria-disabled", value);
				}else{
					this.element
						.add(this.input)
						.add(this.button)
						.remove('disabled')
						.removeClass(
								this.widgetFullName + '-disabled' + ' ' +
								this.namespace + '-state-disabled')
							.attr("aria-disabled", value);
				}
			}
		},

		/**
		* This function resets the underlying select menu if current search
		* value inside the text box do not match with select menu options
		*/
		_clearOnBlur:function(){
			//console.info("_clearOnBlur from base combo box class");
			var $input = this.input;			
			var searchValue = $input.val();

			var hasHintOption = (($(this.select).find("option[value=-1]").length>0) || ($(this.select).find("option[value='']").length>0)) ? true : false;
			if(!hasHintOption){
				//create default option hint
				var optionsStr = $("<option value='-1'>" + this.options.hint + "</option>");
				var firstOption = $(this.select).find("option:first");
				$(optionsStr).insertBefore(firstOption);				
			}
			
			var options = $(this.select).find("option");
			var found = false;
			for(var i=0;i<options.length;i++){
				var option = options[i];
				if($(option).text() === searchValue){
					found = true;
					$(option).attr("selected","selected");
				}
			}
			
			if(!found){
				//If entered item is not found in existing combo box selects the options which is -1 or blank
				if($(this.select).find("option[value='']").length>0){
					$(this.select).find("option[value='']").attr("selected","selected");
				}else{
					$(this.select).find("option[value=-1]").attr("selected","selected");
				}				
			}
			
			$input.attr('style', 'color:#c8c8c8');
			$input.val(this.options.hint);
		}
	});

})(jQuery);
