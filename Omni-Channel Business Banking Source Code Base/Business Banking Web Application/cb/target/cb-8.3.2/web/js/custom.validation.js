function customValidation(form, errors) {   
try{ 		
		//Now its not mandatory to provide error fields this function will automaticallty create error element
		var $form = $(form);
		var formId = $form.attr("id");
		if(formId){		
			formId = "#" + formId;			
			if($(formId +" ul#formerrors").length == 0){
				$form.prepend("<ul id='formerrors'></ul>");
			}
			var list = $(formId +" ul#formerrors");
			list.html('');
			
			//Set role alert for accessibility
			list.attr("role", "alert");
			list.attr("aria-atomic", "true");

			//The element sitting above all others
			var $topElement = undefined;
			
			//Handle non field errors 
			if (errors.errors) {
					$.each(errors.errors, function(index, value) { 
							if(value !== ""){
								$('<li/>').appendTo(list).attr('class','errorLabel').html(getHTMLEncodedString(value));
								list.append('\n');
							}
							$topElement = list;
					});
			}
			
			//Handle field errors 
			if (errors.fieldErrors) {
					var errFieldId = "";
					$.each(errors.fieldErrors, function(index, value){
							index = index.replace(/\./g,'\\.');
							errFieldId = formId + ' span#' + index + 'Error';
							if($(errFieldId).length == 0){
								//$("<span id='" + index + "Error'></span>").insertAfter($(formId + " [name=" + index + "]" ))
								addErrorLabel(formId,index);
							}
							var elem = $(errFieldId);
							if(elem != null && elem.offset() != null) {
								if(!$topElement || elem.offset().top < $topElement.offset().top){
									$topElement = elem;
								}
								elem.html(getHTMLEncodedString(value[0]));
								elem.addClass('errorLabel');
								
								//Set accessibility related attribute
								setAccessibilityInfo(formId + " [name$=" + index + "]", elem.attr("id"));
							}
				});
			}
			if($topElement){
				_scrollTo($topElement);
			}

			if(typeof accessibility !== "undefined"){
				accessibility.focusToErrorField($form);
			}  
		}else{
			console.warn("Please provide valid form id.");
		}	
	}catch(err) {	
		console.warn("Error "+err.message);
	}
}

function addErrorLabel(formId,aField){
	var isDatePicker = false;
	var isSelect=false;
	isDatePicker = isDatePickerControl(formId + " [name$=" + aField + "]" );
	isSelect = isSelectBoxControl(formId + " [name$=" + aField + "]");
	
	if(isSelect){
		//handle select types differently since actual select box is hidden and we see generated select menu.
		var selectBoxId = $(formId + " [name=" + aField + "]" ).attr("id");
		$("<br><span id='" + aField + "Error'></span>").insertAfter($(formId + " [id=" + selectBoxId + "-menu]" ));		
	}else if(isDatePicker){
		$("<br><span id='" + aField + "Error'></span>").insertAfter($(formId + " [name$=" + aField + "]" ).siblings("img"));//appending label right after cal image
	}else{
		$("<br><span id='" + aField + "Error'></span>").insertAfter($(formId + " [name$=" + aField + "]" ));
	}
}

function isDatePickerControl (id){
	return $(id).hasClass("hasDatepicker");
} 

function isSelectBoxControl(id){
	var control = $(id);
	if(id && control.is("select")){
		if($(id + ":hidden").length>0){
			return true;
		}
	};
	return false;
}

function setAccessibilityInfo(controlId, errorSpanId){
	var isSelectBox = isSelectBoxControl(controlId);
	var control = null;
	if(isSelectBox) {
		control = $(controlId).parent().find("input");		
	} else {
		control = $(controlId);
	}
	control.attr("aria-describedby", errorSpanId);
	control.attr("aria-invalid", "true");
}

function removeValidationErrors(){
	 var errorLabels = $('.errorLabel'), counter = 0, id = "", controlId = "", control = null;
	 for(counter = 0; counter < errorLabels.length; counter++){
		 id = errorLabels[counter].id.replace("Error", "");
		 controlId = "#" + id.charAt(0).toUpperCase() + id.replace("Error", "").slice(1);
		 control = $(controlId);
		 if(control){
			 if(isSelectBoxControl(controlId)){
				 control = control.parent().find("input")
			 } 
			 control.removeAttr("aria-invalid");
		 }
	 }
	 
	 errorLabels.html('').removeClass('errorLabel');
	  //Clean up all the form level validation error messages
	 $('[id=formerrors]').html("");
}


var _inView = function($elem, viewAll) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    if (viewAll)
		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	else
		return (elemTop >= docViewTop);
};


var _scrollTo = function($elem){
	if($('#notes').length>0){
    var off = $('#notes').offset().top - $elem.offset().top;
    if(off > 0) //The element is scrolled above the top of notes
    	$("#notes").animate({scrollTop: $("#notes").scrollTop()-off-15}, {duration:500, easing:"linear"});
    if(!_inView($('#notes')))
    	$("html, body").animate({scrollTop: 0}, {duration:500, easing:"linear"}); 
	}
};
//Added this call here as custom.validation is used in jsp-ns pages also and we do not have access to common.js.
//HTML encode a string, but leave <br> alone since it is often returned in error messages
var getHTMLEncodedString = function (value){ 
	return value.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/&lt;br&gt;/g,'<br>');
};