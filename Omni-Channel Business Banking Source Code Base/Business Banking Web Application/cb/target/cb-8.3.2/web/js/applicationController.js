/*
 * This is application controller which drives some of the application level events and setup
 */
var applicationController = (function($) {
    var _isInitialzed = false;
    var _bootStrapPeformed = false;
    var config = {
        initAccessibility: true,
        ui5: {
            "id": "ui5-core",
            "src": "../../../web/sapui5/sap-ui-core-nojQuery.js",
            "async": true,
            "data-sap-ui-theme": "sap_banking_base",
            "data-sap-ui-theme-roots": '{"sap_banking_base" : "../../../web/css/ui5/"}',
            "data-sap-ui-libs": "sap.ui.commons,sap.ui.layout,sap.viz,sap.ui.ux3",
            "data-sap-ui-modules": "sap.ui.core.plugin.DeclarativeSupport",
            "data-sap-ui-xx-bindingSyntax": "complex",
            "data-sap-ui-language": "en",
            "data-sap-ui-preload": "async",
            "data-sap-ui-resourceroots": '{"ui5": "../../../web/js/ui5/"}',
            "data-sap-ui-xx-preloadLibCss": "sap.ui.core,sap.ui.commons,sap.ui.layout,sap.ui.unified,sap.viz,sap.ui.ux3,sap.banking.ui"
        }
    };

    var _init = function(aConfig) {
        _isInitialzed = true;
        $.extend(config, aConfig);
        _registerEvents();
        _initAccessibility();
        _publishPageLoadEvents();
    };

    var _initAccessibility = function() {
        if (typeof accessibility === "object") {
            if (config.initAccessibility) {
                accessibility.init();
            } else {
                _turnOfAccessibility();
            }
        } else {
            _turnOfAccessibility();
        }
    };

    var _publishPageLoadEvents = function() {
        //Publish get banking event with delayed call
        setTimeout(function() {
            $.publish("ns.common.getBankingEvents");
        }, 30000);
    };

    var _isUI5Loaded = function() {
        return (typeof sap === "undefined") ? false : true;
    };

    var _initializeUI5Application = function(oData) {
        if (_bootStrapPeformed === false) {
            //UI5 initial set up activities
            //NOTE: Paths configured here will be resolved relative to index.jsp
            JS_ROOT = "../../../web/js";
            JS_APP_ROOT = JS_ROOT + "/ui5";
            jQuery.sap.registerModulePath('Application', '../../../web/js/Application');
            jQuery.sap.registerModulePath('sap.banking', JS_APP_ROOT + '/sap/banking/');
            jQuery.sap.registerModulePath('view', JS_APP_ROOT + '/view');
            jQuery.sap.registerModulePath('util', JS_APP_ROOT + '/util');
            jQuery.sap.registerModulePath('fragment', JS_APP_ROOT + '/fragment');

            // Kick off application when core is ready!
            sap.ui.getCore().attachInit(function() {
                //Load the framework library
                sap.ui.getCore().loadLibrary("sap.banking.ui");
                jQuery.sap.require("Application");
                window.mainApp = new Application({
                    root: "notes"
                });

                //Store all contollers        
                jQuery.sap.require("sap.banking.ui.util.Map");
                registeredControllers = new sap.banking.ui.util.Map();
                //var routers = sap.banking.ui.routing.Routers;
                if(oData.beforeNavigate && typeof oData.beforeNavigate === 'function'){
                	oData.beforeNavigate.apply();
                }
				sap.banking.ui.core.routing.ApplicationRouter.getRouter(oData.router).navTo(oData.route);
            });


            //Adavanced way of component initialization, this banking UI component is called as ui5 componenet
            //This will enable the features associated with component based developement and extensions
            //Modules Supported: Stops
            //Banking UI component do not have any root view
            //NOTE: BUG: When component gets initialized it was rearranging DOM node for desktop out of notes
            //Make sure to look at this once stops will be checked in
            /*var oBankingUIComponent = new sap.ui.core.ComponentContainer({
                name: "ui5"
            });
            oBankingUIComponent.placeAt("notes");*/


            //If extensions are required enable extended commponent here
            //E.g oBankingUIComponentExtension            
            /*
            jQuery.sap.registerModulePath("ui5", JS_APP_ROOT);
            var oBankingUIComponentExtension = new sap.ui.core.ComponentContainer({
                name: "ui5Extension"
            });
            oBankingUIComponentExtension.placeAt("notes");
            */
            _bootStrapPeformed = true;
            //$(document).publish("sap-ui-coreCompleteTopic");

        }
    };

    /*
     * This function makes the accessibility off inturn making the acccesibility object undefined so that all calls
     * on accessibility would get turned off inside application
     */
    var _turnOfAccessibility = function() {
        accessibility = undefined;
    };

    var _registerEvents = function() {
        //Load library on demand
        $(document).subscribe("sap-ui-loadDependentLibrary", function(oEvent, oData) {
            var sLibrary = oData.library;
            sap.ui.getCore().loadLibrary(sLibrary);

            if (oData.attachInit) {
                oData.attachInit.apply(this, arguments);
            }
        });


        //Render charts with common topic in async way
        $(document).subscribe("sap-ui-renderChartsTopic", function(oEvent, oData) {
           console.warn("sap-ui-renderChartsTopic", oData.chartRoute);
            oData.route = oData.chartRoute;
            oData.router = "charts";
            if (_isUI5Loaded() === false) {
                /*var oAttributes = {
                    "id": "ui5-core",
                    "src": "../../../resources/sap-ui-core-nojQuery.js",
                    "async": true,
                    "data-sap-ui-theme": "sap_banking_base",
                    "data-sap-ui-theme-roots": '{"sap_banking_base" : "../../../web/css/ui5/"}',
                    "data-sap-ui-libs": "sap.ui.commons,sap.ui.layout,sap.viz,sap.ui.ux3",
                    "data-sap-ui-modules": "sap.ui.core.plugin.DeclarativeSupport",
                    "data-sap-ui-xx-bindingSyntax": "complex",
                    "data-sap-ui-language": "en",
                    "data-sap-ui-preload": "async",
                    "data-sap-ui-resourceroots": '{"ui5": "../../../web/js/ui5/"}',
                    "data-sap-ui-xx-preloadLibCss": "sap.ui.core,sap.ui.commons,sap.ui.layout,sap.ui.unified,sap.viz,sap.ui.ux3,sap.banking.ui"
                }*/

                var fnCallback = function() {
                    if (!_bootStrapPeformed) {
                        _initializeUI5Application(oData);
                    }else{
                    	sap.ui.getCore().attachInit(function() {
		                    if (_isUI5Loaded() && _bootStrapPeformed == false) {
		                        _initializeUI5Application(oData);
		                    }
		                    var routers = sap.banking.ui.routing.Routers;
		                    if(oData.beforeNavigate && typeof oData.beforeNavigate === 'function'){
		                    	oData.beforeNavigate.apply();
		                    }
		                    sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navTo(oData.chartRoute);
	                	});
                    }
                }

                //NOTE: UI5 loads the dependent files synchrnously, which gives a frozen browser when framework requires
                //Dependent files, To imporve on loading we load these dependent files asynchrnously
                //so that browser would cache it, and when UI5 needs these files would then be served from browser cache
                /*$.when(
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/d3.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/jqueryui/jquery-ui-widget.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/jqueryui/jquery-ui-mouse.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/jqueryui/jquery-ui-draggable.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/jqueryui/jquery-ui-droppable.js"),
                    $.cachedScript("/cb/resources/sap/viz/libs/sap-viz.js"),
                    $.cachedScript("/cb/web/js/Application.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/signals.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/hasher.js"),
                    $.cachedScript("/cb/resources/sap/ui/thirdparty/crossroads.js")
                ).done(function() {*/
                    //When these files are ready load the framework
                    var oAttributes = config.ui5;
                    asyncLoader.loadScript(oAttributes, fnCallback);
                /*});*/
            } else {
                //Peform router navigation only when library is ready!                
                sap.ui.getCore().attachInit(function() {
                    if (_isUI5Loaded() && _bootStrapPeformed == false) {
                        _initializeUI5Application(oData);
                    }
                    var routers = sap.banking.ui.routing.Routers;
                    if(oData.beforeNavigate && typeof oData.beforeNavigate === 'function'){
                    	oData.beforeNavigate.apply();
                    }
                    sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navTo(oData.chartRoute);
                });
            }
        });

        //Render Mobile Device View in async way
        $(document).subscribe("sap-ui-renderMobileDeviceView", function(oEvent, oData) {
            oData.router = "deviceManagement";
            if (_isUI5Loaded() === false) {
                var oConfig = {
                    "data-sap-ui-libs": "sap.ui.commons,sap.ui.ux3"
                }

                var fnCallback = function() {
                    if (!_bootStrapPeformed) {
                        _initializeUI5Application(oData);
                    }
                    
                    sap.ui.getCore().attachInit(function() {
                    if (_isUI5Loaded() && _bootStrapPeformed == false) {
                        _initializeUI5Application(oData);
                    }
                    //var routers = sap.banking.ui.routing.Routers;
                    //sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navTo(oData.route);
                });
                }

                var oAttributes = $.extend(config.ui5, oConfig);
                asyncLoader.loadScript(oAttributes, fnCallback);
            } else {
                //Peform router navigation only when library is ready!                
                sap.ui.getCore().attachInit(function() {
                    if (_isUI5Loaded() && _bootStrapPeformed == false) {
                        _initializeUI5Application(oData);
                    }else{
						var routers = sap.banking.ui.routing.Routers;
						sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navTo(oData.route);
					}                    
                });
            }
        });
    };

    return {
        init: function(aConfig) {
            _init(aConfig);
        }
    }
})(jQuery);