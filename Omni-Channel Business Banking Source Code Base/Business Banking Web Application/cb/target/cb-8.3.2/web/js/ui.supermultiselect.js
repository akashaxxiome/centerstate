/*
 * JQuery UI SuperMultiselect
 * 
 * Author:
 *  Jerry Dong (gerald_dong[at]hotmail[dot]com)
 *
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *  
 * Modified with JQuery UI Multiselect
 */

/*
 * jQuery UI Multiselect
 *
 * Authors:
 *  Michael Aufreiter (quasipartikel.at)
 *  Yanick Rochon (yanick.rochon[at]gmail[dot]com)
 * 
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 * 
 * http://www.quasipartikel.at/multiselect/
 *
 * 
 * Depends:
 *	ui.core.js
 *	ui.sortable.js
 *
 * Optional:
 * localization (http://plugins.jquery.com/project/localisation)
 * scrollTo (http://plugins.jquery.com/project/ScrollTo)
 * 
 * Todo:
 *  Make batch actions faster
 *  Implement dynamic insertion through remote calls
 */
(function($) {
	$.widget("ui.supermultiselect", {
		  options: {
				width: null,
				height: null,
				toIds: [ ],
				isCopySender: false,
				sortable: false,
				draggable: true,
				searchable: true,
				addRemoveAllButton : false,
				containedPool: "body",
				animated: 'fast',
				show: 'slideDown',
				hide: 'slideUp',
				sortableOptions: {},
				beforeAdd: function( item, fromList, toList ) { 
									return true; 
								},
				beforeRemove: function( item, homeList, guestList ) { 
									return true; 
								},
				beforeReceive: function( item, sender, rcvList) { 
									return true; 
								},
				beforeUpdate: function(e, ui, updateList) {
									return true;
								},
				beforeCreate: function(wgt) {
									return true;
								},
				beforeApplyItem: function(item, wgt) {
									return true;
								},
				afterAdd: function( item, fromList, toList ) { 
									return true; 
								},
				afterRemove: function( item, homeList, guestList ) { 
									return true; 
								},
				afterReceive: function( item, sender, rcvList ) { 
									return true; 
								},
				afterUpdate: function(e, ui, updateList) {
									
								},
				afterApplyItem: function(item, wgt) {
									
								},
				afterCreate:	function(wgt) {
									
								}
			},
			/**
			 * Private methods:
			 */
			_create: function() {
				
				var beforeHandler = this.options.beforeCreate;
				if( beforeHandler != undefined && $.isFunction(beforeHandler) && beforeHandler(this) != true )
					return;
				
				this.count = 0; // number of currently selected options
				this.id = this.element.attr("id");
				
				this.element.hide();
				this.element.data("ownner", this);
				this.element.data("owner", this);
				
				this.container = $('<div id="' + this.id + '_sms" class="ui-supermultiselect ui-helper-clearfix ui-widget"></div>')
									.insertAfter(this.element);
				
				this.listContainer = $('<div class="selected"></div>').appendTo(this.container);
				
				this.listActions = "";
				if(this.options.addRemoveAllButton){
					this.listActions = $('<div class="actions ui-widget-header ui-helper-clearfix">'
						+ '<input type="text" class="search empty ui-widget-content ui-corner-all"/>'
						+ '<span class="count">0 '+ $.ui.supermultiselect.locale.itemsCount + '</span><span id="'+this.id+'RemoveAllLink" class="removeAllLink">- Remove All</span></div>')
					.appendTo(this.listContainer);
				}else{
					this.listActions = $('<div class="actions ui-widget-header ui-helper-clearfix">'
						+ '<input type="text" class="search empty ui-widget-content ui-corner-all"/>'
						+ '<span class="count">0 '+ $.ui.supermultiselect.locale.itemsCount + '</span></div>')
					.appendTo(this.listContainer);
				}
				
				this.theList = $('<ul id="' + this.id + '_sms_list" class="selected connected-list"></ul>')
					.appendTo(this.listContainer);
				

				// set dimensions
				var w = this.options.width,
					h = this.options.height;

				if( w == null )	w = this.element.width()+1;
				if( h == null )	h = this.element.height();
				this.container.width(w);
				this.listContainer.width(w);

				
				// fix list height to match <option> depending on their individual header's heights
				this.theList.height(h);
				
				if ( !this.options.animated ) {
					this.options.show = 'show';
					this.options.hide = 'hide';
				}
				
				// init the original father(named mother) attr
				var that = this;
				this.element.children().map(function(i, option){
					option = $(option);
					if( option.attr("mother") == undefined )
						option.attr("mother", that.id);
					
					if( option.attr('oidx') == undefined )
						option.attr('oidx', i);
				});
				
				// set from and to ids 
				// prepare for draggable function
				$.map(this.options.toIds, function(id){
					var toList = $("#" + id ).data("owner"); 
					if(toList != null)
						toList.theList.addClass(that.id + "_toList");
				});
				

				// init lists
				this._populateLists(this.element.find('option'));
				
				
				this.theList.sortable({
					connectWith: "." + that.id + "_toList",
					helper: function(e, ui){
						var _helper = $('<div class="ui-supermultiselect ui-helper-clearfix ui-widget">'
							+ '<div class="selected"><ul class="selected"></ul></div></div>');
							var _item = ui.clone();
							_helper.find('ul').append(_item).width($(this).width());
							_helper.width($(this).width());
							
							return _helper;
					},
					update: function(e, ui){
						var handler = that.options.beforeUpdate;
						if( !$.isFunction(handler) || handler( e, ui, that) )
						{
							that.updateSelect();
							if( $.isFunction(that.options.afterUpdate) ) {
								that.options.afterUpdate(e, ui, that);
							}
						}
						
					},
					receive: function(event, ui){
						var handler = that.options.beforeReceive;
						if( !$.isFunction(handler) || handler( ui.item, ui.sender, that) )
						{
							var senderID = $(ui.sender).attr('id');
							senderID = senderID.substr(0, senderID.length -  9 );
							
							var $sender = $('#' + senderID).data('owner');
							if( $sender.isCopySender() )
							{
								var fakeItem = $sender.theList.find(".dragging-mirror").eq(0);
								
								if( fakeItem == null ) return false;
								
								var item = ui.item;
									
								fakeItem.removeClass("dragging-mirror").addClass("ui-element").show();
								fakeItem.data('optionLink', item.data('optionLink') );
								item.data('optionLink', item.data('optionLink').clone() );
								
								$sender._applyItemState( fakeItem );
								$sender.updateSelect();
							}
							
							that._applyItemState(ui.item);
							
							if( $.isFunction(that.options.afterReceive) ) {
								that.options.afterReceive(ui.item, ui.sender, that);
							}
								
						} else {
							$(ui.sender).sortable('cancel');
						}
					},
					remove: function(event, ui){
						var handler = that.options.beforeSend;
						if( $.isFunction(handler))
						{
							handler( event, ui, that)
						}
					},
					start: function(event, ui){
						if(that.isCopySender() ){
							that.theList.find('.dragging-mirror').each(function(){
								$(this).remove();
							});
							var fakeItem = ui.item.clone().insertAfter(ui.item);
							fakeItem.addClass("dragging-mirror").removeClass('ui-element');
						}
					},
					placeholder: 'ui-state-highlight',
					appendTo: that.options.containedPool,
	  				containment: that.options.containedPool,
	  				revert: 'invalid'
				});
				
				this.theList.sortable('option', this.options.sortableOptions);
				
				// set up livesearch
				if (this.options.searchable) {
					this._registerSearchEvents(this.listContainer.find('input.search'));
				} else {
					$('.search').hide();
				}
				
				var afterHandler = this.options.afterCreate;
				if($.isFunction(afterHandler))
					afterHandler(this);
			},
			_populateLists: function(options) {
				// 1st. clear
				this.theList.children('.ui-element').remove();
				this.count = 0;

				var that = this;
				$(options).each(function() {
					var option = $(this);
					var optionText = option.text().replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
					var node = $('<li class="ui-state-default ui-element" title="'
									+ optionText + '"><span class="ui-icon"/><font>'
									+ optionText + '</font><a href="#" class="action"><span class="ui-corner-all ui-icon"/></a></li>'
									).hide();
					node.appendTo(that.theList).show();
					
					node.data('optionLink', option);
					
					that._applyItemState(node);
					
					that.count += 1;
				});

				// update count
				this._updateCount();
		  },
		  _applyItemState: function(item) {
				var that = this;

				var beforeHandler = this.options.beforeApplyItem;
				if( beforeHandler != undefined && $.isFunction(beforeHandler) && beforeHandler(item, this) != true )
					return;
				
				item.attr("container", this.container.attr("id"));
				
				// set item sortable function
				if (this.options.sortable)
					item.children('span').addClass('ui-icon-arrowthick-2-n-s').removeClass('ui-helper-hidden').addClass('ui-icon');
				else
					item.children('span').removeClass('ui-icon-arrowthick-2-n-s').addClass('ui-helper-hidden').removeClass('ui-icon');

				// set item draggable function


				if ( item.data('optionLink').attr("mother") != this.id ) {
					item.find('a.action').remove();
					item.append('<a href="#" class="action"><span class="ui-corner-all "/>- Remove</a>');
					//item.find('a.action span').addClass('ui-icon-minus').removeClass('ui-icon-plus');
					this._registerRemoveEvents(item.find('a.action'));
				} else if ( this.options.toIds.length > 0 ) {
					item.find('a.action').remove();
					item.append('<a href="#" class="action"><span class="ui-corner-all "/>+ Add</a>');
					
					//item.find('a.action span').addClass('ui-icon-plus').removeClass('ui-icon-minus');
					this._registerAddEvents(item.find('a.action'));
				} else {
					item.find('a.action').remove();
					item.append('<a href="#" class="action"><span class="ui-corner-all "/>+ Add</a>');
					//item.find('a.action span').removeClass('ui-icon-plus').removeClass('ui-icon-minus').addClass('ui-helper-hidden');
					item.find('a.action').click(function(){ return false; });
				}
				// set hover event
				this._registerHoverEvents(item);
				
				var afterHandler = this.options.afterApplyItem;
				if($.isFunction(afterHandler))
					afterHandler(item, this);
		},

		// taken from John Resig's liveUpdate script
		_filter: function(list) {
			var input = $(this);
			var rows = list.children('li'),
				cache = rows.map(function(){
					
					return $(this).text().toLowerCase();
				});
			
			var term = $.trim(input.val().toLowerCase()), scores = [];
			
			if (!term) {
				rows.show();
			} else {
				rows.hide();

				cache.each(function(i) {
					if (this.indexOf(term)>-1) { scores.push(i); }
				});

				$.each(scores, function() {
					$(rows[this]).show();
				});
			}
		},
		_registerHoverEvents: function(elements) {
			elements.removeClass('ui-state-hover');
			elements.mouseover(function() {
				$(this).addClass('ui-state-hover');
			});
			elements.mouseout(function() {
				$(this).removeClass('ui-state-hover');
			});
		},
		_registerAddEvents: function(elements) {
			var that = this;
			elements.click(function() {
				var item = $(this).parent();
				if( that.options.toIds.length > 0 ) {
					var toList = $("#" + that.options.toIds[0]).data("owner");
					var handler = that.options.beforeAdd;
					
					if( !$.isFunction(handler) || handler(item, that, toList) )
					{
						that.addItem(item, toList);
					} 
				}
				else {
					that.trigger("noToAddError", [item, that]);
				}
				return false;
			});
		},
		_registerRemoveEvents: function(elements) {
			var that = this;
			
			elements.click(function() {
				var item = $(this).parent();
				var homeId = item.data('optionLink').attr('mother');
				
				var homeList = $("#" + homeId).data("owner");
				if( homeList != null) {	
					var handler = that.options.beforeRemove;
					
					if( !$.isFunction(handler) || handler(item, homeList, that) )
					{
						that.removeItem(item, homeList);

					}
				}
				else {
					that.trigger("noFromRemoveError", [item, that]);
				}
				return false;
			});
	 	},
		_registerSearchEvents: function(input) {
			var that = this;

			input.focus(function() {
				$(this).addClass('ui-state-active');
			})
			.blur(function() {
				$(this).removeClass('ui-state-active');
			})
			.keypress(function(e) {
				if (e.keyCode == 13)
					return false;
			})
			.keyup(function() {
				that._filter.apply(this, [that.theList]);
			});
		},
		_updateCount: function() {
			this.listContainer.find('span.count').text(this.count+" "+$.ui.supermultiselect.locale.itemsCount);
		},
		applyItemState: function(item) {
			this._applyItemState(item);
		},
		updateCount: function(){
			this.count = 0;
			var that = this;
			this.element.children().each(function(){
				that.count += 1;
			});
			
			this._updateCount();
		},
		addItem: function(item, toList){
			if(this.isCopySender()){
				// make item' copy
				var _item = item.clone();
				_item.data('optionLink', item.data('optionLink').clone() );
				
				_item.appendTo(toList.theList).hide()[toList.options.show](toList.options.animated);
				_item.data('optionLink').appendTo(toList.element);
			
				toList._applyItemState(_item);
				
				toList.updateCount();
				
			} else {
				// transfer item
				item.appendTo(toList.theList).hide()[toList.options.show](toList.options.animated);
				item.data('optionLink').appendTo(toList.element);
				
				this.updateCount();
				
				toList._applyItemState(item);
				
				toList.updateCount();

			}

			if( $.isFunction(this.options.afterAdd) ) {
				this.options.afterAdd(item, this, toList);
			}
		},
		isCopySender: function(){
			return this.options.isCopySender;
		},
		updateSelect: function()
		{
			var that = this;
			this.element.html("");
			this.count = 0;
			// apply the new sort order to the original selectbox
			this.theList.find('li.ui-element').each(function() {
				if ($(this).data('optionLink') && $(this).attr("container") == that.container.attr("id") )
				{
					$(this).data('optionLink').appendTo(that.element);
					
					that.count += 1;
				}
			});
			
			this._updateCount();
		},
		insertItem: function(item, i)
		{
			var items = this.theList.children('li.ui-element');
			
			if( i >= items.length)
				i = items.length - 1;
			
			if( items.length == 0 ) {
				item.appendTo(this.theList).hide()[this.options.show](this.options.animated);
				item.data('optionLink').appendTo( this.element );
			} else if ( i < 0 ) {
				item.insertBefore( items.eq(0) ).hide()[this.options.show](this.options.animated);
				item.data('optionLink').insertBefore( this.element.children().eq(0) );
			}
			else {
				item.insertAfter( items.eq(i) ).hide()[this.options.show](this.options.animated);
				item.data('optionLink').insertAfter( this.element.children().eq(i) );
			}
		},
		removeItem: function(item, homeList)
		{
			if(homeList.isCopySender())
			{
				item.data('optionLink').remove();
				item.remove();
			} else {
				var oidx = item.data('optionLink').attr('oidx');
				
				var homeItems = homeList.theList.children('li.ui-element');
				var i = 0;
				while( i < homeItems.size() && homeItems.eq(i).data('optionLink').attr('oidx') <= oidx) 
					i++;
		
				homeList.insertItem(item, i-1 );

				homeList._applyItemState(item);
				
				homeList.updateCount();
			}
			
			this.updateCount();

			if( $.isFunction(this.options.afterRemove) ) {
				this.options.afterRemove(item, homeList, this);
			}
		},
		destroy: function() {
	       $.Widget.prototype.destroy.apply(this, arguments); // default destroy
	        // now do other stuff particular to this widget
			this.theList.children().remove();
			this.theList.sortable('destroy');
			this.container.remove();
			this.element.show();
		}
	});
		
	$.extend($.ui.supermultiselect, {
		locale: {
			addAll:'Add all',
			removeAll:'Remove all',
			itemsCount:'items remain'
		}
	});

})(jQuery);
