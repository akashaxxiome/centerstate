ns.common.sortableCol = {};

(function($){
	var _sortableColumn = $.fn.jqGrid.sortableColumns;
	$.fn.jqGrid.sortableColumns = function(tblrow){
		this.each(function (){
			var ts = this;
			ts.p.sortable = function(){ns.common.sortableCol.update(ts.p.id);}
		});
		_sortableColumn.apply(this,arguments);
	}

})(jQuery);

ns.common.sortableCol.update = function(id){
	var $elem = $("#"+id);
	if(!$elem.data("updateURL")){
		return;
	}
	var columns = ns.common.sortableCol.getSetting(id);
	var colModel = $elem.jqGrid("getGridParam", "colModel");
	if(colModel) {
		var hiddenColumnsList = '';
	   	for(var i = 0; i < colModel.length; i++) {
	       	if(colModel[i].name && colModel[i].hidden) {
	       		hiddenColumnsList += colModel[i].name +',';
	       	}
	   	}
		$.ajax({
			url: $elem.data("updateURL"),
			type:"POST",
			data: {hiddenColumnNames: hiddenColumnsList, columnPerm: columns},
			success: function(data) {
				isPortalModified = true;
			}
		});
    }
}


ns.common.sortableCol.getSetting = function(id){
	var seq = "",
		$list = $("#gbox_"+id+" .ui-jqgrid-htable tr:first >th:not(:has(#jqgh_cb,#jqgh_rn,#jqgh_subgrid))"),
		regExp = new RegExp(id+"_");
	$list.each(function(){
		seq += this.id.replace(regExp,"");
		seq += ",";
	});
	return seq.substr(0,seq.length-1);
}

ns.common.sortableCol.rearrangeColumns = function(columns,id){
	var items = columns.split(","),
		permutation = [],
		$grid = $("#"+id),
		$list = $("#gbox_"+id+" .ui-jqgrid-htable tr:first >th:not(:has(#jqgh_cb,#jqgh_rn,#jqgh_subgrid))"),
		regExp = new RegExp(id+"_"),
		i,j;
	for(i=0; i<$("#gbox_"+id+" .ui-jqgrid-htable tr:first >th:has(#jqgh_cb,#jqgh_rn,#jqgh_subgrid)").length; i++){
		permutation.push(i);
	}
	for(i=0; i<items.length; i++){
		var columnName = items[i];
		for(j=0; j<$list.length; j++){
			var colId = $list[j].id.replace(regExp,"");
			if(colId == columnName){
				permutation.push($($list[j]).index());
			}
		}
	}
	$grid.jqGrid("remapColumns",permutation,true);
}

