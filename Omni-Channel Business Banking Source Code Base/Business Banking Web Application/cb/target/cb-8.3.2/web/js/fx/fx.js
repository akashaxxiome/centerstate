$.subscribe('closeForeignExchangeUitilityDialog', function(event,data){
	$.ajax({
		type: "POST",   
		url: "/cb/pages/jsp/transfers/favoriteRates_saveFavoriateRates.action",
		data: $("#FXCalculator").serialize(),
		success: function(data) {
			//do nothing
		}   
	});
});

ns.fx.calculateRate = function(){
	$.ajax({
		type: "POST",   
		url: "/cb/pages/jsp/fx/fx_selected.jsp?fxcalculate=true",
		data: $("#FXCalculator").serialize(),
		success: function(data) {
			$('#calResultId').html(data);
		}   
	});
}
ns.fx.addFavoriteRate = function(){
	$.ajax({   
		type: "POST",   
		url: "/cb/pages/jsp/transfers/favoriteRates_addFavoriteRateToFavoriteList.action",
		data: $("#FXCalculator").serialize(),
		success: function(data) {
			$('#StoredGrid').trigger("reloadGrid");
		}   
	});
}

ns.fx.deleteFavoriteRate = function( urlString ){
	$.ajax({   
		type: "POST",   
		url: urlString,
		data: $("#FXCalculator").serialize(),
		success: function(data) {
			$('#StoredGrid').trigger("reloadGrid");
		}   
	});
}

$.subscribe('beforeCalculateFxRates', function(event,data) {
			removeValidationErrors();
});

function formatFavoriteRateActionLinks(cellvalue, options, rowObject)
{
	//cellvalue is the ID of the favorite rate.
	var tempURL = "/cb/pages/jsp/transfers/favoriteRates_deleteFavoriteRateFromFavoriteList.action?" + "fromCurrencyCode=" + rowObject.map.fromCurrencyCode +"&toCurrencyCode=" + rowObject.map.toCurrencyCode;
	var del = "<a title='" +js_delete+ "' href='#' onClick='ns.fx.deleteFavoriteRate(\"" + rowObject.map.DeleteURL + "\" );'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash' style='float:left;'></span></a>";                                              
	return del;
}