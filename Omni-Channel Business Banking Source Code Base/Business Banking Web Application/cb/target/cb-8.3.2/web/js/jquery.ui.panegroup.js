(function($) {
	$.widget("ui.panegroup", {
		options: {
			title: '',
			fillSpace: true,
			autoHeight: false
		},    	
    	
        _create: function() {
            var self = this;

            self.el = $(self.element);
            self.paneIds = [];
            self.triggerId = '';
          
            self.switchPanes = function(){

            	//about to expand this pane, fold the expanded pane first
            	$.each(self.paneIds, function(index, value){
            		$paneDiv = $('#'+value); // value is the Id of a pane
            		if($paneDiv.pane('isOpen') && (value !== self.triggerId)){
            			$paneDiv.pane('fold');
            		};
            	});
            	
            	//don't do anything if clicking on an expanded pane
            	if (!$('#' + self.triggerId).pane('isOpen'))
					$('#' + self.triggerId).pane('expand');
            	self.triggerId = '';
            };
            
            
            self.beforeSwitch = function(currentPane){
            	
            	var paneId = $(currentPane.element).attr('id');

            	if(self.triggerId === ''){
            		self.triggerId = paneId;
            		self.switchPanes();
            		return false; //doesn't switch here, 
            					  //let the paneGroup iterate and process all panes
            	}
            	
            	return true;

            };

            //find all pane children and keep references to them 
            self.el.find('div.pane-content').each(function(){
            	var paneId = $(this).attr('id');
            	self.paneIds.push(paneId);
            	$(this).pane('option','beforeMinmax',self.beforeSwitch);
            });
            
            
    		self.resize();
            
            //expand the first by default
    		$paneDiv = $('#'+self.paneIds[0]); 
    		$paneDiv.pane('expand');
            
            
        },
        
		
		resize: function() {
    		var options = this.options,
    			maxHeight;

    		$(this.element).css("overflow","hidden");
    		
    		if ( options.fillSpace ) {
    			if ( $.browser.msie ) {
    				var defOverflow = this.element.parent().css( "overflow" );
    				this.element.parent().css( "overflow", "hidden");
    			}
    			maxHeight = $(this.element).height();
    			if ($.browser.msie) {
    				this.element.parent().css( "overflow", defOverflow );
    			}

    			$.each(this.paneIds, function(index, paneId) {
    				var $pane = $( '#'+paneId ).parents('.pane:first');
    				maxHeight -= $pane.find('.pane-header').outerHeight( true );
				});
				
    			$.each(this.paneIds, function(index, paneId) {
    				var $pane = $( '#'+paneId ).parents('.pane:first');
    				var $paneContent = $pane.children('.ui-widget-content'); 
    				$paneContent.height( Math.max( 0, maxHeight -
    						$paneContent.innerHeight() + $paneContent.height() ) ).css( "overflow", "auto" );
				});

    		} else if ( options.autoHeight ) {
    			maxHeight = 0;

    			$.each(this.paneIds, function(index, paneId) {
    				var $pane = $( '#'+paneId ).parents('.pane:first');
    				var $paneContent = $pane.find('.ui-widget-content'); 
    				maxHeight = Math.max( maxHeight, $paneContent.height( "" ).height() );
    				$paneContent.height( maxHeight ).css( "overflow", "auto" );
				});

    		};
    		
    		return this;
    	}

    });

})(jQuery); 
