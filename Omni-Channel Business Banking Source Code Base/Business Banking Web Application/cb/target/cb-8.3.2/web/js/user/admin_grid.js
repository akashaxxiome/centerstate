$.subscribe('adminPermAcctGrpGridCompleteEvents', function(event,data) {
	ns.common.resizeWidthOfGrids();
});

/**
* Show clone user permissions button and clone account permissions button both.
* Hide details
*/
ns.admin.showCloneButtons = function(oneAdmin, self, viewOnly){
	//show clone user permissions button and clone account permissions button both.
	if (viewOnly != 'TRUE') {
		//$('#appdashboard').pane('show');
		//$('#appdashboard').show();
		if ((oneAdmin == 'TRUE' && self == 'TRUE')) {
			$('#cloneUserPermissionsButton').attr('style','display:none');
		} else {
			$('#cloneUserPermissionsButton').removeAttr('style');
		}
		$('#cloneAccountPermissionsButton').removeAttr('style');
		// hide details
	} else {
		//$('#appdashboard').pane('hide');
		//$('#appdashboard').hide();
	}
	$('#details').hide();
}

ns.admin.deleteUser = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#deleteUserDialogID').html(data).dialog('open');
		}
	});
}

ns.admin.editUser = function(urlString)
{
	$.publish("beforeLoad");
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#inputDiv').html(data);
			$.publish("completeUserLoad");
		}
	});
}

ns.admin.editDAUser = function(urlString)
{
	$.publish("beforeLoad");
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#inputDiv').html(data);
			$.publish("completeUserLoad");
		}
	});
}

ns.admin.userPermissions = function(urlString, oneAdmin, self)
{
	//QTS#772322:Changes to resolve issue with account access permissions tab not shown when clicking on Permissions button for subsequent User.
	ns.admin.topLavelPermissionTab = ''; //clear any previously saved tab info for last user
	ns.common.refreshDashboard('showdbCloneUserPermissionSummary',true);
	$('#summary').hide();
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#userPermissionsLink').click();
		}
	});
	ns.admin.showCloneButtons(oneAdmin, self, "FALSE");
	ns.admin.currentVisibleDashboard = "USERS";

}

ns.admin.viewUserPermissions = function( urlString, oneAdmin, self)
{
	$('#summary').hide();
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#userPermissionsLink').click();
		}
	});
	// view only user permissions - don't show clone buttons
	ns.admin.showCloneButtons(oneAdmin, self, "TRUE");
	ns.admin.currentVisibleDashboard = "USERS";
	
}

ns.admin.approvalGroups = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#approvalGroupAssignLink').click();
		}
	});
}

function formatUserActionLinks(cellvalue, options, rowObject)
{
	// edit/delete/permissions only available if the user is an admin of the row
	if (rowObject.map.IsAnAdminOf == 'TRUE') {
		var del = "<a title='" +js_delete+ "' href='#' onClick='ns.admin.deleteUser(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var perms = "";
		if(!rowObject.usingEntProfiles) {
			perms = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.userPermissions(\"" + rowObject.map.PermissionsURL + "\", \"" + rowObject.map.OneAdmin + "\", \"" + rowObject.map.Self + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
		}
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editUser(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var app = "<a title='" +js_approval_groups+ "' href='#' onClick='ns.admin.approvalGroups(\"" + rowObject.map.ApprovalsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approvalGroups'></span></a>";

		// don't allow user to delete himself
		if (ns.admin.dualApprovalMode == "FALSE" && rowObject.map.Self == 'TRUE') {
			del = "";
		}
		// don't allow user to edit his own permissions unless there is only one admin
		if (rowObject.map.OneAdmin == 'FALSE' && ns.admin.dualApprovalMode == "FALSE" && rowObject.map.Self == 'TRUE') {
			perms = "";
		}
		if (rowObject.map.ApprovalsAdmin != 'TRUE') {
			app = "";
		}
		return ' ' + edit + ' ' + del + ' ' + perms + ' ' + app;
	} else {
		edit = "";
		del = "";
		perms = "";
        // QTS 733690: The CB70SP2 code always allows Approval Groups link, so don't modify it to be disabled
//		app = "<a class='ui-button' title='" +js_approval_groups+ "' href='#' ><span class='ui-icon ui-icon-transfer-e-w ui-state-disabled' style='float:left;'></span></a>";
        app = "<a class='ui-button' title='" +js_approval_groups+ "' href='#' onClick='ns.admin.approvalGroups(\"" + rowObject.map.ApprovalsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approvalGroups'></span></a>";		return ' ' + edit + ' ' + del + ' ' + perms + ' ' + app;
	}
}

/** Funtion to set user action in Pending user grid. This function dynamically sets the action buttons. */
function formatDAPendingChangesUserActionLinks(cellvalue, options, rowObject)
{
	var edit = "<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled'></span></a>";
	var view = "<a class='ui-button' title='" +js_cannot_view+ "' href='#' ><span class='ui-icon ui-icon-info ui-state-disabled'></span></a>";
	var approve = "<a class='ui-button' title='Approve' href='#' ><span class='ui-icon ui-icon-check ui-state-disabled' style='float:left;'></span></a>";
	
	var permissions ="<a class='ui-button' title='" +js_permissions+ "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled' style='float:left;'></span></a>";
	var submitForApproval = "<a class='ui-button' title='Submit for Approval' href='#'><span class='ui-icon ui-icon-circle-check ui-state-disabled' style='float:left;'></span></a>";
	var discard = "<a class='ui-button' title='Discard Changes' href='#'><span class='ui-icon ui-icon-cancel ui-state-disabled' style='float:left;'></span></a>";
	var modifyChange ="<a class='ui-button' title='Modify Changes' href='#' ><span class='ui-icon ui-icon-tag ui-state-disabled' style='float:left;'></span></a>";
	var reject = "<a class='ui-button' title='Reject' href='#'><span class='ui-icon ui-icon-close ui-state-disabled' style='float:left;'></span></a>";
    var viewUserPermissions = "";
	var action = "";

	// Has Manage Admin User permission
	if( rowObject.map.EntitlementCanAdminister == 'TRUE' ){
		// Edit link
		if (rowObject.edit == undefined) {
			edit = "";
		} else if (rowObject.edit == 'TRUE') {
			edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDAUser(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			action += edit;
		}

		// View link
		if (rowObject.view == undefined) {
			view ="";
		} else if (rowObject.view == 'TRUE') {
			view = "<a title='View' href='#' onClick='ns.admin.viewUserInformation(\""+rowObject.map.ViewURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
			action += view;
		}

		// Modify change link
		if (rowObject.modifyChange == undefined) {
			modifyChange ="";
		} else if (rowObject.modifyChange == 'TRUE') {
			modifyChange = "<a  title='Modify Changes' href='#' onClick='ns.admin.modifyChange(\"" + rowObject.map.ModifyChangeURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			if(!rowObject.usingEntProfiles) {
				viewUserPermissions = "<a  title='View Permissions' href='#' onClick='ns.admin.viewUserPermissions(\"" + rowObject.map.ViewUserPermissionsURL + "\", \"" + rowObject.map.OneAdmin + "\", \"" + rowObject.map.Self + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-permission'></span></a>";
			}
			action += modifyChange;
			action += viewUserPermissions;
		}
		
		// Permission link
		if (rowObject.permission == undefined) {
			permissions = "";
		} else if (rowObject.permission == 'TRUE') {
			if(!rowObject.usingEntProfiles) {
				permissions = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.userPermissions(\"" + rowObject.map.PermissionsURL + "\", \"" + rowObject.map.OneAdmin + "\", \"" + rowObject.map.Self + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			}
			action += permissions;
		}
		
		// Submit for Approval link
		if (rowObject.submitForApproval == undefined) {
			submitForApproval = "";
		} else if (rowObject.submitForApproval == 'TRUE') {
			submitForApproval = "<a title='Submit for Approval' href='#' onClick='ns.admin.submitForApproval(\"" + rowObject.map.SubmitForApprovalURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>";
			action += submitForApproval;
		}

		// Discard link
		if (rowObject.discard == undefined) {
			discard = "";
		} else if (rowObject.discard == 'TRUE') {
			discard = "<a title='Discard Changes' href='#' onClick='ns.admin.discardUserChange(\"" + rowObject.map.DiscardURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
			action += discard;
		}
		
		// Approve link
		if (rowObject.approve == undefined) {
			approve = "";
		} else if (rowObject.approve == 'TRUE') {
			approve = "<a  title='Approve' href='#' onClick='ns.admin.approve(\"" + rowObject.map.ApproveURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
			action += approve;
		}

		// Reject link
		if (rowObject.reject == undefined) {
			reject = "";
		} else if (rowObject.reject == 'TRUE') {
			reject = "<a  title='Reject' href='#' onClick='ns.admin.rejectUserChange(\"" + rowObject.map.RejectURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";
			action += reject;
		}
		return action;
	} else {
		return "";
	}
}

function formatDAPendingChangesUserAdminColumn(cellvalue, options, rowObject)
{
	if (rowObject.map.CanAdministerAnyGroup == undefined || rowObject.map.CanAdministerAnyGroup == "FALSE") {
		return "No";
	} else if ( rowObject.map.CanAdministerAnyGroup == "TRUE") {
		return "Yes";
	}
}

ns.admin.formatActGrpAcctNum = function(cellvalue, options, rowObject){
	if(rowObject.primaryAccount && rowObject.primaryAccount == "1")
		return cellvalue + " *";
	if(rowObject.coreAccount && rowObject.coreAccount == "0")
		return cellvalue + " \u2022";
	return cellvalue;
}

/** This function is used for showing approve popup window. */
ns.admin.approve = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			var aprvDg = $('#approvalWizardDialogID');
            aprvDg.html(data);
            var viewContent = aprvDg.children().detach();
			aprvDg.dialog('open');
			viewContent.appendTo(aprvDg); 
		}
	});
}

/** This function is used for calling approve popup window function. */
ns.admin.rejectUserChange = function( urlString )
{
	ns.admin.approve(urlString);
}

/** This function is used for showing discard popup window. */
ns.admin.discardUserChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#verifyDiscardUserDialogID').html(data).dialog('open');
		}
	});
}
/** This function is used for showing view user info popup window. */
ns.admin.viewUserInformation = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#viewUserInformationDialogID').html(data).dialog('open');
		}
	});
}

/** This function is used for showing submit for approval popup window. */
ns.admin.submitForApproval = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#submitUserForApprovalDialogID').html(data).dialog('open');
		}
	});
}

/** This function is used for showing modify popup window. */
ns.admin.modifyChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			ns.common.showSummaryOnSuccess("summary","done");
		}
	});
};

/** This function is used for returning users grid CRUD link actions. */
function formatProfileActionLinks(cellvalue, options, rowObject)
{
	//cellvalue is the ID of the profile.
	if (rowObject.map.IsAnAdminOf == 'TRUE') {
		var edit="<a title='" +js_edit+ "' href='#' onClick='ns.admin.editProfile(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var clone="<a title='" +'Clone'+ "' href='#' onClick='ns.admin.editProfile(\"" + rowObject.map.CloneURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-clone'></span></a>";
		//Show edit icon to shared/cross profiles only and not the channels.
		//Also adding check for usingEntProfiles since we dont want to hide edit link for retail users since they will also be non-cross profiles.
		if(rowObject.channelId != 'X' && rowObject.usingEntProfiles == true) {
			edit = "";
			clone="";
		}
		var del = "";
		if(rowObject.usingEntProfiles == false) {
			clone="";
		}
		if(rowObject.map.CanDel == 'TRUE') {
			del = "<a title='" +js_delete+ "' href='#' onClick='ns.admin.deleteProfile(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
		}
		var perms = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.profilePermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
		
		return ' ' + edit + ' ' + del + ' ' + perms + ' ' + clone;
	} else {
		var edit = "";
		var del = "";
		var perms = "";
		return ' ' + edit + ' ' + del + ' ' + perms;
	}
}
/**
 * This methods formats the profile status text.
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {String}
 */
function formatProfileStatus(cellvalue, options, rowObject) {
	if(rowObject.status != undefined && rowObject.status == "ACTIVE"){
		return "Active";
	}else{
		return "Inactive"
	}
}

/** Funtion to set Profile action in Pending Profile grid. This function dynamically sets the action buttons. */
function formatDAPendingChangesProfileActionLinks(cellvalue, options, rowObject)
{
	var edit = "<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled'></span></a>";
	var view = "<a class='ui-button' title='" +js_cannot_view+ "' href='#' ><span class='ui-icon ui-icon-info ui-state-disabled'></span></a>";
	var approve = "<a class='ui-button' title='Approve' href='#' ><span class='ui-icon ui-icon-check ui-state-disabled' style='float:left;'></span></a>";
	var permissions ="<a class='ui-button' title='" +js_permissions+ "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled' style='float:left;'></span></a>";
	var submitForApproval = "<a class='ui-button' title='Submit for Approval' href='#'><span class='ui-icon ui-icon-circle-check ui-state-disabled' style='float:left;'></span></a>";
	var discard = "<a class='ui-button' title='Discard Changes' href='#'><span class='ui-icon ui-icon-cancel ui-state-disabled' style='float:left;'></span></a>";
	var modifyChange ="<a class='ui-button' title='Modify Changes' href='#' ><span class='ui-icon ui-icon-tag ui-state-disabled' style='float:left;'></span></a>";
	var reject = "<a class='ui-button' title='Reject' href='#'><span class='ui-icon ui-icon-close ui-state-disabled' style='float:left;'></span></a>";
    var viewProfilePermissions = "";
	var action = "";

	
	// Has Manage Admin User permission
	if( rowObject.map.IsAnAdminOf == 'TRUE' ){
		// Edit link
		if (rowObject.edit == undefined) {
			edit = "";
		} else if (rowObject.edit == 'TRUE') {
			edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDAProfile(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			action += edit;
		}

		// View link
		if (rowObject.view == undefined) {
			view ="";
		} else if (rowObject.view == 'TRUE') {
			view = "<a title='View' href='#' onClick='ns.admin.viewProfileInformation(\""+rowObject.map.ViewURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
			action += view;
		}

		// Modify change link
		if (rowObject.modifyChange == undefined) {
			modifyChange ="";
		} else if (rowObject.modifyChange == 'TRUE') {
			modifyChange = "<a title='Modify Changes' href='#' onClick='ns.admin.modifyChange(\"" + rowObject.map.ModifyChangeURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			viewProfilePermissions = "<a title='View Permissions' href='#' onClick='ns.admin.viewProfilePermissions(\"" + rowObject.map.ViewProfilePermissionsURL + "\", \"" + rowObject.map.OneAdmin + "\", \"" + rowObject.map.Self + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			action += modifyChange;
			action += viewProfilePermissions;
		}
		
		// Permission link
		if (rowObject.permission == undefined) {
			permissions = "";
		} else if (rowObject.permission == 'TRUE') {
			permissions = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.profilePermissions(\"" + rowObject.map.PermissionsURL + "\", \"" + rowObject.map.OneAdmin + "\", \"" + rowObject.map.Self + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			action += permissions;
		}
		
		// Submit for Approval link
		if (rowObject.submitForApproval == undefined) {
			submitForApproval = "";
		} else if (rowObject.submitForApproval == 'TRUE') {
			submitForApproval = "<a title='Submit for Approval' href='#' onClick='ns.admin.submitProfileForApproval(\"" + rowObject.map.SubmitForApprovalURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>";
			action += submitForApproval;
		}

		// Discard link
		if (rowObject.discard == undefined) {
			discard = "";
		} else if (rowObject.discard == 'TRUE') {
			discard = "<a title='Discard Changes' href='#' onClick='ns.admin.discardProfileChange(\"" + rowObject.map.DiscardURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
			action += discard;
		}
		
		// Approve link
		if (rowObject.approve == undefined) {
			approve = "";
		} else if (rowObject.approve == 'TRUE') {
			approve = "<a title='Approve' href='#' onClick='ns.admin.approve(\"" + rowObject.map.ApproveURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
			action += approve;
		}

		// Reject link
		if (rowObject.reject == undefined) {
			reject = "";
		} else if (rowObject.reject == 'TRUE') {
			reject = "<a title='Reject' href='#' onClick='ns.admin.rejectProfileChange(\"" + rowObject.map.RejectURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";
			action += reject;
		}
		return action;
	} else {
		return "";
	}
}

ns.admin.deleteProfile = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#deleteProfileDialogID').html(data).dialog('open');
		}
	});
}

ns.admin.editProfile = function(urlString)
{
	$.publish("beforeLoad");
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#inputDiv').html(data);
			$.publish("completeUserLoad");
		}
	});
}

ns.admin.editDAProfile = function(urlString)
{
	$.publish("beforeLoad");
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#inputDiv').html(data);
			$.publish("completeUserLoad");
		}
	});
}

/** This function is used for calling approve popup window function. */
ns.admin.rejectProfileChange = function( urlString )
{
	ns.admin.approve(urlString);
}

/** This function is used for showing discard popup window. */
ns.admin.discardProfileChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#verifyDiscardProfileDialogID').html(data).dialog('open');
		}
	});
}
/** This function is used for showing view profile info popup window. */
ns.admin.viewProfileInformation = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#viewProfileInformationDialogID').html(data).dialog('open');
		}
	});
}

/** This function is used for showing submit for approval popup window. */
ns.admin.submitProfileForApproval = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#submitProfileForApprovalDialogID').html(data).dialog('open');
		}
	});
}

/** This function is used for showing modify popup window. */
ns.admin.modifyProfileChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			ns.admin.reloadPendingUsersAndUsersGrid();
			//$('#usersWithPendingChangesGrid').trigger("reloadGrid");
			//$('#adminUserGrid').trigger("reloadGrid");
		}
	});
}

ns.admin.profilePermissions = function(urlString)
{
	//QTS#772322:Changes to resolve issue with account access permissions tab not shown when clicking on Permissions button for subsequent User.
	ns.admin.topLavelPermissionTab = ''; //clear any previously saved tab info for last user
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#profilePermissionsLink').click();
		}
	});
	$('#profileSummary').hide();
	ns.admin.currentVisibleDashboard = "PROFILES";
}

ns.admin.viewProfilePermissions = function( urlString, oneAdmin, self)
{
	$('#summary').hide();
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#profilePermissionsLink').click();
		}
	});
	$('#profileSummary').hide();
	// view only user permissions - don't show clone buttons
	ns.admin.showCloneButtons(oneAdmin, self, "TRUE");
	ns.admin.currentVisibleDashboard = "PROFILES";
}

ns.admin.deleteGroup = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#deleteGroupDialogID').html(data).dialog('open');
		}
	});
}

ns.admin.editGroup = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editGroupLink').click();
		}
	});
}

ns.admin.groupPermissions = function(urlString)
{
	$('#permissionsDiv').empty();
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#groupPermissionsLink').click();
		}
	});
	$('#summary').hide();
}

//?EditGroup_GroupName=${DescendantItem.GroupName}&EditGroup_GroupId=${DescendantItem.GroupId}&EditGroup_Supervisor=${Supervisor.Id}&EditGroup_DivisionName=${DescendantItemDivision.GroupName}">

function formatGroupActionLinks(cellvalue, options, rowObject)
{
	if (rowObject.map.IsAnAdminOf == 'true') {
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editGroup(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit' ></span></a>";
		var del = "<a title='" +js_delete+ "' href='#' onClick='ns.admin.deleteGroup(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
		var perms = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.groupPermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
		return ' ' + edit + ' ' + del + ' ' + perms;
	} else {
		var edit = "";
		var del = "";
		var perms = "";
		return ' ' + edit + ' ' + del + ' ' + perms;
	}
}

ns.admin.deleteDivision = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#deleteDivisionDialogID').html(data).dialog('open');
		}
	});
}

ns.admin.editDivision = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editDivisionLink').click();
		}
	});
}

ns.admin.divisionPermissions = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#divisionPermissionsLink').click();
		}
	});
	$('#summary').hide();
}

ns.admin.divisionLocations = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			/*$.ajax({   
				url: "/cb/pages/jsp/user/admin_locations_summary.jsp",
				success: function(data) {
					$('#summary').html(data).show();
				}   
			});	*/
			ns.common.refreshDashboard('showdbLocationSummary',true);
		}
	});	
	$('#details').hide();
	//$('#admin_divisionsTab').tabs('enable', 1);
	//$('#admin_divisionsTab').tabs('option','active', 1);
}

function formatDivisionActionLinks(cellvalue, options, rowObject)
{
	if (rowObject.map.IsAnAdminOf == 'true') {
		var edit = "<a  title='" +js_edit+ "' href='#' onClick='ns.admin.editDivision(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del = "<a title='" +js_delete+ "' href='#' onClick='ns.admin.deleteDivision(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
		var perms = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.divisionPermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
		var locs = ' ';
		if (rowObject.map.LocationsAdmin == 'true') {
			locs = "<a title='" +js_locations+ "' href='#' onClick='ns.admin.divisionLocations(\"" + rowObject.map.LocationsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-location'></span></a>";
		}
		return ' ' + edit + ' ' + del + ' ' + perms + ' ' + locs;
	} else {
		var edit = '';
		var del = '';
		var perms = '';
		var locs = '';
		/*var edit = "<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled' style='float:left;'></span></a>";
		var del = "<a class='ui-button' title='" +js_cannot_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled' style='float:left;'></span></a>";
		var perms = "<a class='ui-button' title='" +js_permissions+ "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled' style='float:left;'></span></a>";
		var locs = "<a class='ui-button' title='" +js_locations+ "' href='#' ><span class='ui-icon ui-icon-home ui-state-disabled' style='float:left;'></span></a>";*/
		return ' ' + edit + ' ' + del + ' ' + perms + ' ' + locs;
	}
}

ns.admin.deleteLocation = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#deleteLocationDialogID').html(data).dialog('open');
		}
	});
}

ns.admin.editLocation = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editLocationLink').click();
		}
	});
}

function formatLocationActionLinks(cellvalue, options, rowObject)
{
	var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editLocation(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del = "<a title='" +js_delete+ "' href='#' onClick='ns.admin.deleteLocation(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	return ' ' + edit + ' ' + del;
}

ns.admin.editAdmin = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editAdminLink').click();
		}
	});
}

function formatDivisionAdmins(cellvalue, options, rowObject)
{
	var editAdmin = '';
	if (rowObject.map.IsAnAdminOf == 'true') {
		//cellvalue is the list of administrators to display.
		editAdmin = "<a class='ui-button' title='" +js_edit_admin+ "' href='#' onClick='ns.admin.editAdmin(\"" + rowObject.map.AdminEditURL + "\");'><span class='ui-icon ui-icon-wrench' style='float:left;'></span></a>";
	}
	return editAdmin + ' ' + cellvalue;
}

function formatGroupAdmins(cellvalue, options, rowObject)
{
	var editAdmin = '';
	if (rowObject.map.IsAnAdminOf == 'true') {
		//cellvalue is the list of administrators to display.
		editAdmin = "<a class='ui-button' title='" +js_edit_admin+ "' href='#' onClick='ns.admin.editAdmin(\"" + rowObject.map.AdminEditURL + "\");'><span class='ui-icon ui-icon-wrench' style='float:left;'></span></a>";
	}
	return editAdmin + ' ' + cellvalue;
}

ns.admin.refreshGrid = function(gridID)
{
	$(gridID).trigger("reloadGrid");
}

$.subscribe('closeDeleteUserDialog', function(event, data)
{
	ns.common.showSummaryOnSuccess("summary","done");
	ns.common.closeDialog('#deleteUserDialogID');
});

$.subscribe('closeApprovalWizardDialog', function(event, data)
{
	$('#approvalWizardDialogID').dialog('close');
	
});


$.subscribe('closeDeleteProfileDialog', function(event, data)
{
	ns.common.closeDialog('#deleteProfileDialogID');
});



$.subscribe('closeDeleteLocationDialog', function(event, data)
{
	if(!$('#operationresult').is(':visible')){
		$('#details').hide();
		///ns.admin.refreshDivisionModule();
		ns.common.refreshDashboard('showdbLocationSummary',true);
	}
});

$.subscribe('refreshUserGrid', function(event, data)
{
	ns.common.showSummaryOnSuccess("summary","done");
});

$.subscribe('refreshProfileGrid', function(event, data)
{
	ns.common.showSummaryOnSuccess("profileSummary","done");
});

//account formatter.
function formatAccountColumn( cellvalue, options, rowObject )
{
		
	var account = rowObject.routingNum + " : " + cellvalue;
	if (rowObject.primaryAccount != undefined && rowObject.primaryAccount == 1){
		account = account + ' *';
	}
	if (rowObject.coreAccount != undefined && rowObject.coreAccount != 1)
	{
		account = account + ' &#8226;'
	}
	return account;
}
//edit account configuration
ns.admin.editAccountConfig = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editAccountConfigLink').click();
			$("#summary").hide();
		}
	});
}

//view account configuration
ns.admin.viewAccountConfig = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$("#DACompanyAccountConfigurationViewDialogId").html(data).dialog('open');	
		}
	});
}

$.subscribe('pendingDAChangesGridCompleteEvents', function(event,data) {
	if($("#pendingChangesID").jqGrid('getGridParam', 'reccount') == 0){
 		$('#company_PendingApprovalTab').slideUp();
 		ns.admin.hideshowAdminCompanyLinks(true);
 	}else{
 		var daStatus = $('#pendingChangesID').jqGrid('getCell', 1, 'status');
 		if(daStatus == 'Pending Approval'){
 			$('#company_PendingApprovalTab').slideDown();
 	 		ns.admin.hideshowAdminCompanyLinks(false);
 		}else{
 			$('#company_PendingApprovalTab').slideDown();
 	 		ns.admin.hideshowAdminCompanyLinks(true);
 		}
 	}
 	ns.admin.addRejectReasonRowsForCompanyPendingChanges('#pendingChangesID');
 	ns.common.resizeWidthOfGrids();	
});

ns.admin.hideshowAdminCompanyLinks = function(editableFlag){
	
	 var	urlString = "/cb/pages/jsp/home/inc/companySubmenu.jsp";
		
     $.ajax({
         url: urlString,
         success: function(data){
		
			$("#menu_admin_company").html(data);

         }
     });
	
	
	/*if(editableFlag == true){
		$("#editAdminCompanyLinks").show();
 		$("#viewAdminCompanyLinks").hide();
 	}else{
 		$("#editAdminCompanyLinks").hide();
 		$("#viewAdminCompanyLinks").show();
 	}	*/
}

$.subscribe('pendingUserDAChangesGridCompleteEvents', function(event,data) {
	if($("#usersWithPendingChangesGrid").jqGrid('getGridParam', 'reccount') == 0){
 		$('#admin_pendingUsersTab').slideUp();
 	}else{
 		$('#admin_pendingUsersTab').slideDown();
 	}	
});

$.subscribe('pendingProfileDAChangesGridCompleteEvents', function(event,data) {
	var pId = data.p.id;
	var isGridSearchActive = ns.common.isGridSearchActive(pId);
	if(isGridSearchActive != true){
		if($("#profilesWithPendingChangesGrid").jqGrid('getGridParam', 'reccount') == 0){
	 		$('#admin_pendingProfilesTab').hide();
	 		$('#profileBlankDiv').hide();
	 	}else{
	 		$('#admin_pendingProfilesTab').slideDown();
	 		$('#profileBlankDiv').show();
	 	}
	}
});

//Add reject information in pending wire beneficiary grid.
	ns.admin.addRejectReasonRowsForCompanyPendingChanges = function(gridID){
		if( gridID == "#pendingChangesID" ){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			var reasonText = "";
			
			for(var i=0;i<ids.length;i++){  
				var rowId = ids[i];
				var daStatus = $('#pendingChangesID').jqGrid('getCell', rowId, 'status');
				var rejectReason = $('#pendingChangesID').jqGrid('getCell', rowId, 'rejectReason');
				var rejectedBy = $('#pendingChangesID').jqGrid('getCell', rowId, 'rejectedBy');
				
				//if status is 12, means that the transfer is rejected.
				var currentID = "#pendingChangesID #" + rowId;
				
				if(daStatus == 'Rejected') {
					rejectReasonTitle = js_reject_reason;
					rejectedByTitle = js_rejected_by;
					if(rejectReason == ''){
						reasonText = "No Reason Provided";
					} else{
						reasonText = ns.common.spcialSymbolConvertion(rejectReason);
					}
					className = 'ui-widget-content jqgrow ui-row-ltr';
					
					$(currentID).after(ns.common.addRejectReasonRow(rejectedByTitle,rejectedBy,rejectReasonTitle,reasonText,className,4));
				}
			}
		}
	}


	
ns.admin.getCompanySummaryDatas = function( urlString, divID ){
		$.get(urlString, function(data) {
			$(divID).html(data);
		});
	}
	
/**
*view transaction group based on a list(on page).
*/
ns.admin.viewTG = function( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#companyContent').html(data);
			$('#companyDetails').slideDown();
			$('#companyTabs').portlet('fold');


		}
	});
}


//format Admin Profile Pending changes action link (edit)
function formatPendingChangesActionLinks(cellvalue, options, rowObject)
{
	var action = '';

	if(options.colModel.formatoptions.canAdminister == 'TRUE')
	{
		var daStatusPendingApproval = 	options.colModel.formatoptions.daStatusPendingApproval;
		var daStatusInProcess = 	options.colModel.formatoptions.daStatusInProcess;
		var daStatusRejected = 	options.colModel.formatoptions.daStatusRejected;
		var status = rowObject.status;
		var canApprove = options.colModel.formatoptions.canApprove;

		if(status==daStatusInProcess)
		{
			action = "<a title='Submit for Approval' onClick='ns.admin.companySubmitForDAChanges(\"" + options.colModel.formatoptions.submitForApporvalURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>&nbsp;";
			action = action + "<a title='Discard Changes' onClick='ns.admin.companyDiscardDAChanges(\"" + options.colModel.formatoptions.discardURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";	/*To do ui-icon ui-icon-cancel icon to changes*/		
		}
		else if(status==daStatusPendingApproval)
		{
			action = "<a class='' title='Modify Changes' onClick='ns.admin.companyModifyDAChanges(\"" + options.colModel.formatoptions.modifyURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			if(canApprove == 'true')
			{
				action = action + "&nbsp; &nbsp; <a class='' title='Approve' onClick='ns.admin.companyApproveDAChanges(\"" + options.colModel.formatoptions.approveURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
				action = action + "&nbsp; &nbsp; <a class='' title='Reject' onClick='ns.admin.companyRejectDAChanges(\"" + options.colModel.formatoptions.rejectURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";/* To do change icon ui-icon ui-icon-close */
			}
		}
		else if(status==daStatusRejected)
		{
			action = "<a class='' title='Modify Changes' onClick='ns.admin.companyModifyDAChanges(\"" + options.colModel.formatoptions.modifyURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
		}
	}
	return ' ' + action;

}

/**
* Submit company pending DA Changes
*/
ns.admin.companySubmitForDAChanges = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
		$('#DACompanySubmitForApprovalDialogId').html(data).dialog('open');
		}
	});
}

/**
* Discard company pending DA Changes
*/
ns.admin.companyDiscardDAChanges = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
		$('#DACompanyDiscardChangesDialogId').html(data).dialog('open');
		}
	});
	
}

/**
* Approve company pending DA Changes
*/
ns.admin.companyApproveDAChanges = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
			$('#DACompanyReviewChangesDialogId').html(data);
		}
	});
}

/**
* Reject company pending DA Changes
*/
ns.admin.companyRejectDAChanges = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
			$('#DACompanyReviewChangesDialogId').html(data);
		}
	});
}

/**
* Modify company pending DA Changes
*/
ns.admin.companyModifyDAChanges = function(urlString)
{
	/*$.ajax({
		url: urlString,
		success: function(data){
		$('#resultmessage').html(data);
		//ns.common.showStatus();
		refreshDashboardWithSelectionHistory();
		}
	});*/
	
	$.ajax({
		url: urlString,
		success: function(data){
		$('#resultmessage').html(data);
		//ns.common.showStatus();
		            $.ajax({
		                url: "/cb/pages/jsp/home/inc/companySubmenu.jsp",
		                success: function(companySubmenuResponse){
	      
						$("#menu_admin_company").html(companySubmenuResponse);
				
						refreshDashboardWithSelectionHistory();
		                }
		            });
		}
	});
	
}

/**
* Submit pending DA Changes for approval
*/
ns.admin.submitForApprovalOld = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
		$('#dualApprovalConfirmationDialog').html(data).dialog('open');
		}
	});
}

$.subscribe('completeDualApprovalAction', function(event,data) { 
		ns.common.closeDialog('#dualApprovalReviewPendingChangesDialog');
		ns.admin.refreshPendingBusinessIsland();
		ns.common.showStatus();
	});
	
$.subscribe('DACompanyApprovalReviewCompleteTopics', function(event,data) { 
	

	  $.ajax({
              url: "/cb/pages/jsp/home/inc/companySubmenu.jsp",
              success: function(companySubmenuResponse){
			
				$("#menu_admin_company").html(companySubmenuResponse);
			
				 var statusMessage = $('#resultmessage').html();
					ns.common.showStatus(statusMessage);
					// OCBSCRUMS-3232: Loading company module to respective submenu
				   refreshDashboardWithSelectionHistory();
        }      });
	
	/*ns.common.closeDialog('#approvalWizardDialogID');
	var statusMessage = $('#resultmessage').html();
    ns.common.showStatus(statusMessage);
	
	 * QTS- #740193.
	 * In IE8, due to slow processing of javascript ns.admin.reloadCompany and ns.admin.refreshCompanyModule conflicts and give grid 
	 * related javascript error. To go around this problem, we are calling ns.admin.refreshCompanyModule on AJAX success of 
	 * of ns.admin.reloadCompany().
	 
	//ns.admin.reloadCompany(ns.admin.refreshCompanyModule);
    
    // OCBSCRUMS-3232: Loading company module to respective submenu
    refreshDashboardWithSelectionHistory();*/
});

$.subscribe('completeDualApprovalActionRequest', function(event,data) { 
		//ns.common.closeDialog('#dualApprovalConfirmationDialog');
		ns.admin.refreshPendingBusinessIsland();
		ns.common.closeDialog('#dualApprovalReviewPendingChangesDialog');
		//ns.common.showStatus();
	});

$.subscribe('DACompanySubmitForApprovalCompleteTopics', function(event,data) { 
	ns.common.closeDialog('#DACompanySubmitForApprovalDialogId');
});
	
$.subscribe('DACompanyDiscardChangesCompleteTopics', function(event,data) { 
	ns.common.closeDialog('#DACompanyDiscardChangesDialogId');
});
	
$.subscribe('reloadPendingBusinessIsland', function(event, data)
{   
	ns.admin.refreshPendingBusinessIsland();
});

/**
* Refresh pending business island
*/
ns.admin.refreshPendingBusinessIsland = function()
{
	$.ajaxSetup({
		async: false
	});

	if ($('#adminCompanyPendingApprovalLink').length > 0) {
		ns.admin.refreshGrid('#pendingChangesID');
	}
	

	$.ajaxSetup({
            async: true
    	});
	
}

//format account configuration action link (view)
function formatAccountConfigActionViewLinks(cellvalue, options, rowObject)
{
	var view = "<a class='' title='" +js_view+ "' href='#' onClick='ns.admin.viewAccountConfig(\"" + rowObject.map.ViewURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	return ' ' + view;
}

//format account configuration action link (edit)
function formatAccountConfigActionLinks(cellvalue, options, rowObject)
{
	var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.admin.editAccountConfig(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	return ' ' + edit;
}

//add indicators for account configration grid
$.subscribe('completeAccountCongiGridLoad', function(event, data)
{
	$('#accountConfigGrid_pager_right').html('* ' + js_PrimaryAccount + ' &#8226; ' + js_ExternalAccount);
	ns.admin.formatDARows("#accountConfigGrid");
	ns.common.resizeWidthOfGrids();
});

ns.admin.formatDARows = function(gridID){
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	for(var i=0;i<ids.length;i++){  
		var rowId = ids[i];
		var highLightField = $(gridID).jqGrid('getCell', rowId, 'map.highLightField');
		if( highLightField == 'true' ){
			var displayObj = document.getElementById( 'accountConfigGrid' );
			var currRow = displayObj.rows[rowId];
			$(currRow).attr("style", "color:red;font-weight:bold;");
		} 
	}

};

//add indicators for group account grid
$.subscribe('completeAccountGroupsGridLoad', function(event, data)
{
	$('#accountGroupsGridId_pager_right').html('* ' + js_PrimaryAccount + ' &#8226; ' + js_ExternalAccount);
});

$.subscribe('shrinkLocGridToFit', function(event, data)
{
	//Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
	
	var validDivisionName = $('#dashBoardDivisionId').val();
	if($.trim(validDivisionName)=='' && $('#adminLocationGrid').jqGrid('getGridParam','records')>0){
		$('#adminLocationGrid').jqGrid('clearGridData');
	}
	
});

$.subscribe('pendingDAChangesUserGridCompleteEvents', function(event,data) {

	var gridID = 'usersWithPendingChangesGrid';
	var ids = jQuery('#' + gridID).jqGrid('getDataIDs');

	if (ids.length > 0) {
	ns.admin.addRejectReasonRows("#usersWithPendingChangesGrid");
		ns.common.resizeWidthOfGrids();
		$('#admin_usersPendingApprovalTab').show();
	}else{
		$('#admin_usersPendingApprovalTab').hide();
	}
});

// This function is used to refersh Pending Users grid
$.subscribe('refreshDAPendingUsersGrid', function(event, data)
{
	// Reload Pending users grid - Dual approval
	if ($('#admin_pendingUsersTab').length > 0) {
		ns.admin.refreshGrid('#usersWithPendingChangesGrid');	
	}	
});

// This function is used to refersh Pending company grid
$.subscribe('refreshDAPendingCompanyGrid', function(event, data)
{
	// Reload Pending users grid - Dual approval
	/*if ($('#adminCompanyPendingApprovalLink').length > 0) {
		ns.admin.refreshGrid('#pendingChangesID');	
	}*/	
	//updateAdminChanges('dbAdminCompanyProfileSummary');
});

//Add approval information in the pending approval summary gird.
ns.admin.addRejectReasonRows = function(gridID){
		
	var rejectReason = "";
	var reasonText = "";

	var ids = jQuery(gridID).jqGrid('getDataIDs');
	for(var i=0;i<ids.length;i++){

		var rowId = ids[i];
		var daStatus = $(gridID).jqGrid('getCell', rowId, 'status');
		var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
		var rejectedBy = $(gridID).jqGrid('getCell', rowId, 'rejectedBy');
		var curRow = $("#" + rowId, $(gridID));


		if(daStatus == 'Rejected') {
			rejectReasonTitle = js_reject_reason;
			rejectedByTitle = js_rejected_by;
			if(rejectReason == ''){
				reasonText = "No Reason Provided";
			} else{
				reasonText = ns.common.spcialSymbolConvertion(rejectReason);
			}
			className = "ui-widget-content jqgrow ui-row-ltr "
			if(i % 2 == 1){
				className = "ui-widget-content jqgrow ui-row-ltr ui-state-zebra";
			}
			
			curRow.after(ns.common.addRejectReasonRow(rejectedByTitle,rejectedBy,rejectReasonTitle,reasonText,className,10));
		}
	}
}

ns.admin.getPendingUsersSummaryDatas = function( urlString, divID ){
	$.get(urlString, function(data) {
		$(divID).html(data);
	});
}

$.subscribe('hideUserGrids', function(event, data)
{
	$("#adminUsersPendingApprovalChangeDiv").hide();
});

$.subscribe('openDACompanyProfileViewDialog', function(event, data)
{
	$('#DACompanyProfileViewDialogId').dialog('open');
});

$.subscribe('openDABaiViewDialog', function(event, data)
{
	$('#DACompanyBaiViewDialogId').dialog('open');
});

$.subscribe('openDATGViewDialog', function(event, data)
{
	$('#DACompanyTGViewDialogId').dialog('open');
});

ns.admin.reloadCompany = function(successFunction)
{
	$.ajax({
		  type: "GET",
		  url: "/cb/pages/jsp/user/inc/corpadmininfo-pre.jsp",
		  success: function(data) {
			  	$("#tempDivIDForMethodNsAdminReloadCompany").html(data);
				if( ns.admin.pendingApprovalCompanyChangeNum == "0" ){
					$('#company_PendingApprovalTab').hide();
				} else {
					$('#company_PendingApprovalTab').show();
				}
			// Call extra Success function, if provided.	
			if(successFunction && (typeof successFunction === 'function')){
				successFunction.call();
			}	
		  }
	});
};

//---------------------------
//New Division JS Codes
//---------------------------

/** Funtion to set user action in Pending user grid. This function dynamically sets the action buttons. */
function formatDAPendingChangesDivisionActionLinks(cellvalue, options, rowObject)
{
	var edit = "";
	var view = "";
	var approve = "<a title='Approve' href='#' ><span class='ui-icon ui-icon-check ui-state-disabled'></span></a>";
	var permissions = "<a title='" +js_permissions+ "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled'></span></a>";
	var submitForApproval = "<a title='Submit for Approval' href='#' ><span class='ui-icon ui-icon-circle-check ui-state-disabled'></span></a>";
	var modifyChange = "<a title='Modify Changes' href='#' ><span class='ui-icon ui-icon-tag ui-state-disabled'></span></a>";
	var reject = "<a title='Reject' href='#' ><span class='ui-icon ui-icon-close ui-state-disabled'></span></a>";
	var viewPermissions = "";
	var action = "";
	var viewPermissionsVar = "";
	
	if ( rowObject.map.IsAdminOfDivision == 'true' && rowObject.action != 'Added') {
		viewPermissionsVar = 'FALSE';
		// Edit link
		if (rowObject.editEnable == undefined) {
			edit ="";
		} else if (rowObject.editEnable == 'TRUE') {
			edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDADivisionNotForAdd(\"" + rowObject.map.EditURLNotForAdd + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			action += edit;
		}

		// View link
		if (rowObject.viewEnable == undefined) {
			view ="";
		} else if (rowObject.viewEnable == 'TRUE') {
			view = "<a title='View' href='#' onClick='ns.admin.viewDivisionInformation(\""+rowObject.map.ViewURLNotForAdd+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
			action += view;
		}

		// PermissionEnable change link
		if (rowObject.permissionEnable == undefined) {
			permissions = "";
		} else if (rowObject.permissionEnable == 'TRUE') {
			permissions = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.divisionPermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			action += permissions;
		}

		// SubmitForApprovalEnable change link
		if (rowObject.submitForApprovalEnable == undefined) {
			submitForApproval = "";
		} else if (rowObject.submitForApprovalEnable == 'TRUE') {
			submitForApproval = "<a title='Submit for Approval' href='#' onClick='ns.admin.submitDivisionForApproval(\"" + rowObject.map.ApprovalURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>";
			action += submitForApproval;
		}
		
		// discardEnable change link
		if (rowObject.discardEnable == undefined) {
			discard = "";
		} else if (rowObject.discardEnable == 'TRUE') {
			discard = "<a title='Discard Changes' href='#' onClick='ns.admin.discardDivisionChange(\"" + rowObject.map.DiscardURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
			action += discard;
		}

		// modifyChangeEnable change link
		if (rowObject.modifyChangeEnable == undefined) {
			modifyChange = "";
		} else if (rowObject.modifyChangeEnable == 'TRUE') {
			modifyChange = "<a title='Modify Changes' href='#' onClick='ns.admin.modifyDivisionChange(\"" + rowObject.map.ModifyURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			viewPermissions = "<a title='View Permissions' href='#' onClick='ns.admin.divisionPermissions(\"" + rowObject.map.ViewPermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-permission'></span></a>";
			action += modifyChange;
			action += viewPermissions;
		}

		// ApproveEnable change link
		if (rowObject.approveEnable == undefined) {
			approve = "";
		} else if (rowObject.approveEnable == 'TRUE') {
			approve = "<a title='Approve' href='#' onClick='ns.admin.approveDADivisionChange(\"" + rowObject.map.ApprovalDADivisionChangeURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
			action += approve;
		}

		// RejectEnable change link
		if (rowObject.rejectEnable == undefined) {
			reject = "";
		} else if (rowObject.rejectEnable == 'TRUE') {
			reject = "<a title='Reject' href='#' onClick='ns.admin.rejectDivisionChange(\"" + rowObject.map.RejectURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";
			action += reject;
		}
		
		return action; 
	} 
	else if ( rowObject.map.CanAdminister == 'true' && rowObject.action == 'Added') {
		// Edit link
		if (rowObject.editEnable == undefined) {
			edit = "";
		} else if (rowObject.editEnable == 'TRUE') {
			edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDADivision(\"" + rowObject.map.EditURLForAdd + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			action += edit;
		}

		// View link
		if (rowObject.viewEnable == undefined) {
			view = "";
		} else if (rowObject.viewEnable == 'TRUE') {
			view = "<a title='View' href='#' onClick='ns.admin.viewDivisionInformation(\""+rowObject.map.ViewURLForAdd+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
			action += view;
		}

		// PermissionEnable change link
		if (rowObject.permissionEnable == undefined) {
			permissions = "";
		} else if (rowObject.permissionEnable == 'TRUE') {
			permissions = "<a title='" +js_permissions+ "' href='#' onClick='ns.admin.divisionPermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			action += permissions;
		}

		// SubmitForApprovalEnable change link
		if (rowObject.submitForApprovalEnable == undefined) {
			submitForApproval ="";
		} else if (rowObject.submitForApprovalEnable == 'TRUE') {
			submitForApproval = "<a title='Submit for Approval' href='#' onClick='ns.admin.submitDivisionForApproval(\"" + rowObject.map.ApprovalURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>";
			action += submitForApproval;
		}
		
		// discardEnable change link
		if (rowObject.discardEnable == undefined) {
			discard = "";
		} else if (rowObject.discardEnable == 'TRUE') {
			discard = "<a title='Discard Changes' href='#' onClick='ns.admin.discardDivisionChange(\"" + rowObject.map.DiscardURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
			action += discard;
		}

		// modifyChangeEnable change link
		if (rowObject.modifyChangeEnable == undefined) {
			modifyChange = "";
		} else if (rowObject.modifyChangeEnable == 'TRUE') {
			modifyChange = "<a title='Modify Changes' href='#' onClick='ns.admin.modifyDivisionChange(\"" + rowObject.map.ModifyURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			action += modifyChange;
		}

		// ApproveEnable change link
		if (rowObject.approveEnable == undefined) {
			approve = "";
		} else if (rowObject.approveEnable == 'TRUE') {
			approve = "<a title='Approve' href='#' onClick='ns.admin.approveDADivisionChange(\"" + rowObject.map.ApprovalDADivisionChangeURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
			action += approve;
		}

		// RejectEnable change link
		if (rowObject.rejectEnable == undefined) {
			reject = "<a title='Reject' href='#' ><span class='ui-icon ui-icon-close'></span></a>";
		} else if (rowObject.rejectEnable == 'TRUE') {
			reject = "<a title='Reject' href='#' onClick='ns.admin.rejectDivisionChange(\"" + rowObject.map.RejectURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";
			action += reject;
		}
		
		return action; 
	}else {
		return ' ';
	}

}  

$.subscribe('pendingDAChangesGridCompleteEventsForDivision', function(event,data) {
	var pId = data.p.id;
	var isGridSearchActive = ns.common.isGridSearchActive(pId);
	if(isGridSearchActive != true){
		if($("#pendingDivisionChangesID").jqGrid('getGridParam', 'reccount') == 0){
			$('#admin_pendingDivisionsTab').slideUp();
			$('#divisionBlankDiv').hide();
		}else{
			$('#admin_pendingDivisionsTab').slideDown();
			$('#divisionBlankDiv').show();
		}
	}

	ns.admin.addRejectReasonRowsForDivisionPendingChanges('#pendingDivisionChangesID');
 	ns.common.resizeWidthOfGrids();	
});

ns.admin.editDADivision = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			var heading =  $($.parseHTML(data)).filter("#PageHeading").html(); 
			ns.common.updatePortletTitle("detailsPortlet",heading,false);
			$('#inputDiv').html(data);
			$('#permissionsDiv').hide();
			$('#details').hide();
			$('#admin_divisionsTab').portlet('fold');
			ns.admin.gotoStep(1);
			//$('#details').slideDown();
			$('#details').show();
			$('#summary').hide();	
		}
	});
}

ns.admin.editDADivisionNotForAdd = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editDivisionLink').click();
		}
	});
}

/** This function is used for showing discard popup window. */
ns.admin.discardDivisionChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#verifyDiscardPendingDialogID').html(data).dialog('open');
		}
	});
}

$.subscribe('DADivisionDiscardChangesCompleteTopics', function(event,data) { 
	ns.common.closeDialog('#verifyDiscardPendingDialogID');
	ns.admin.refreshDivisionModule();
});

/**
* Refresh Division Module
*/
ns.admin.refreshDivisionModule = function()
{	
	$.get('/cb/pages/jsp/user/admin_divisions_summary.jsp',function(data){
	$('#summary').html(data);
	});
}

$.subscribe('DADivisionSubmitForApprovalCompleteTopics', function(event,data) { 
	ns.common.closeDialog('#DADivisionSubmitForApprovalDialogId');
	ns.admin.refreshDivisionModule();
});

/** This function is used for showing submit for approval popup window. */
ns.admin.submitDivisionForApproval = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#DADivisionSubmitForApprovalDialogId').html(data).dialog('open');
		}
	});
}

/** This function is used for showing submit for approval popup window. */
ns.admin.viewDivisionInformation = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#viewDivisionPendingDialogID').html(data).dialog('open');
		}
	});
}

/**
* Modify division pending DA Changes
*/
ns.admin.modifyDivisionChange = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
		$('#resultmessage').html(data);
		//ns.common.showStatus();
		ns.admin.refreshDivisionModule();
		}
	});
	
}

/** This function is used for showing approve popup window. */
ns.admin.approveDADivisionChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#approvalWizardDialogID').html(data).dialog('open');
            ns.admin.checkResizeApprovalDialog();
		}
	});
}

$.subscribe('DADivisionApprovalReviewCompleteTopics', function(event,data) { 
	ns.common.closeDialog('#approvalWizardDialogID');
	var statusMessage = $('#resultmessage').html();
    ns.common.showStatus(statusMessage);
	ns.admin.refreshDivisionModule();
});

$.subscribe('DADivisionApprovalReviewCompleteTopics1', function(event,data) { 
	ns.common.closeDialog('#approvalWizardDialogID');
	var statusMessage = $('#resultmessage').html();
    ns.common.showStatus(statusMessage);
	ns.admin.refreshDivisionModule();
});

ns.admin.rejectDivisionChange = function( urlString )
{
	ns.admin.approveDADivisionChange(urlString);
}

//Add reject information in pending wire beneficiary grid.
ns.admin.addRejectReasonRowsForDivisionPendingChanges = function(gridID){
	if( gridID == "#pendingDivisionChangesID" ){
		var ids = jQuery(gridID).jqGrid('getDataIDs');			
		for(var i=0;i<ids.length;i++){  			
			var rowId = ids[i];
			var daStatus = $(gridID).jqGrid('getCell', rowId, 'status');
			var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
			var rejectedBy = $(gridID).jqGrid('getCell', rowId, 'rejectedBy');
			var currentID = "#pendingDivisionChangesID #" + rowId;

			if(daStatus == 'Rejected') {
				rejectReasonTitle = js_reject_reason;
				rejectedByTitle = js_rejected_by;
				if(rejectReason == '' || rejectReason == undefined){
					reasonText = "No Reason Provided";
				} else{
					reasonText = ns.common.spcialSymbolConvertion(rejectReason);
				}
				var className = 'ui-widget-content jqgrow ui-row-ltr';
				if(i % 2 == 1){
					className = "ui-widget-content jqgrow ui-row-ltr ui-state-zebra";
				}
				
				$(currentID).after(ns.common.addRejectReasonRow(rejectedByTitle,rejectedBy,rejectReasonTitle,reasonText,className,5));
			}
		}
	}
}

$.subscribe('refreshDivisionGrid', function(event, data)
{
	ns.admin.refreshDivisionModule();	
});

$.subscribe('closeDeleteDivisionDialog', function(event, data)
{	
	if(!$('#operationresult').is(':visible')){
		ns.admin.refreshDivisionModule();
	}
});

$.subscribe('refreshLocationGrid', function(event, data)
{
	ns.admin.refreshDivisionModule();
});

//---------------------------
//DA Group JS Codes
//---------------------------

$.subscribe('pendingDAChangesGridCompleteEventsForGroup', function(event,data) {
	var pId = data.p.id;
	var isGridSearchActive = ns.common.isGridSearchActive(pId);
	if(isGridSearchActive != true){
		if($("#pendingGroupChangesID").jqGrid('getGridParam', 'reccount') == 0){
			$('#admin_groupsPendingTab').slideUp();
			$('#groupBlankDiv').hide();
		}else{
			$('#admin_groupsPendingTab').slideDown();
			$('#groupBlankDiv').show();
		}
	}
	ns.admin.addRejectReasonRowsForGroupPendingChanges('#pendingGroupChangesID');
 	ns.common.resizeWidthOfGrids();	
});

/** Funtion to set user action in Pending user grid. This function dynamically sets the action buttons. */
function formatDAPendingChangesGroupActionLinks(cellvalue, options, rowObject)
{
	var edit="<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled' style='float:left;'></span></a>";
	var view ="<a class='ui-button' title='" +js_cannot_view+ "' href='#' ><span class='ui-icon ui-icon-info ui-state-disabled' style='float:left;'></span></a>";
	var approve = "<a class='ui-button' title='Approve' href='#' ><span class='ui-icon ui-icon-check ui-state-disabled' style='float:left;'></span></a>";
	var permissions = "<a class='ui-button' title='" + js_permissions + "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled' style='float:left;'></span></a>";
	var submitForApproval = "<a class='ui-button' title='Submit for Approval' href='#'><span class='ui-icon ui-icon-circle-check ui-state-disabled' style='float:left;'></span></a>";
	var modifyChange = "<a class='ui-button' title='Modify Changes' href='#' ><span class='ui-icon ui-icon-tag ui-state-disabled' style='float:left;'></span></a>";
	var reject = "<a class='ui-button' title='Reject' href='#'><span class='ui-icon ui-icon-close ui-state-disabled' style='float:left;'></span></a>";
	var viewPermissions = "";
	var action = "";
	var viewPermissionsVar = "";
	
	if ( rowObject.map.IsAdminOfDivision == 'true' && rowObject.action != 'Added') {
		viewPermissionsVar = 'FALSE';
		// Edit link
		if (rowObject.editEnable == undefined) {
			edit="<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.editEnable == 'TRUE') {
			if(rowObject.map.SupervisorFound == 'true'){
				edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDAGroupNotForAdd(\"" + rowObject.map.EditURLNotForGroupAdd1 + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			}else{
				edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDAGroupNotForAdd(\"" + rowObject.map.EditURLNotForGroupAdd2 + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			}			
			action += edit;
		}

		// View link
		if (rowObject.viewEnable == undefined) {
			view = "<a class='ui-button' title='" +js_cannot_view+ "' href='#' ><span class='ui-icon ui-icon-info ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.viewEnable == 'TRUE') {
			view = "<a title='View' href='#' onClick='ns.admin.viewGroupInformation(\""+rowObject.map.ViewURLNotForGroupAdd+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
			action += view;
		}

		// PermissionEnable change link
		if (rowObject.permissionEnable == undefined) {
			permissions = "<a class='ui-button' title='" + js_permissions + "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled' style='float:left;'></span></a>";
			} else if (rowObject.permissionEnable == 'TRUE') {
			permissions = "<a title='" + js_permissions + "' href='#' onClick='ns.admin.groupPermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			action += permissions;
		}

		// SubmitForApprovalEnable change link
		if (rowObject.submitForApprovalEnable == undefined) {
			submitForApproval = "<a class='ui-button' title='Submit for Approval' href='#'><span class='ui-icon ui-icon-circle-check ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.submitForApprovalEnable == 'TRUE') {
			submitForApproval = "<a title='Submit for Approval' href='#' onClick='ns.admin.submitGroupForApproval(\"" + rowObject.map.SubmitURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>";
			action += submitForApproval;
		}
		
		// discardEnable change link
		if (rowObject.discardEnable == undefined) {
			discard = "<a class='ui-button' title='Discard Changes' href='#'><span class='ui-icon ui-icon-cancel ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.discardEnable == 'TRUE') {
			discard = "<a title='Discard Changes' href='#' onClick='ns.admin.discardGroupChange(\"" + rowObject.map.DiscardURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
			action += discard;
		}

		// modifyChangeEnable change link
		if (rowObject.modifyChangeEnable == undefined) {
			modifyChange = "<a class='ui-button' title='Modify Changes' href='#' ><span class='ui-icon ui-icon-tag ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.modifyChangeEnable == 'TRUE') {
			modifyChange = "<a title='Modify Changes' href='#' onClick='ns.admin.modifyGroupChange(\"" + rowObject.map.ModifyURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			viewPermissions = "<a title='View Permissions' href='#' onClick='ns.admin.groupPermissions(\"" + rowObject.map.ViewPermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-permission'></span></a>";
			action += modifyChange;
			action += viewPermissions;
		}

		// ApproveEnable change link
		if (rowObject.approveEnable == undefined) {
			approve = "";
		} else if (rowObject.approveEnable == 'TRUE') {
			approve = "<a  title='Approve' href='#' onClick='ns.admin.approveDADivisionChange(\"" + rowObject.map.ApprovalURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
			action += approve;
		}

		// RejectEnable change link
		if (rowObject.rejectEnable == undefined) {
			reject = "";
		} else if (rowObject.rejectEnable == 'TRUE') {
			reject = "<a title='Reject' href='#' onClick='ns.admin.rejectGroupChange(\"" + rowObject.map.RejectURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";
			action += reject;
		}
		
		return action; 
	} 
	else if ( rowObject.map.CanAdminister == 'true' && rowObject.action == 'Added') {
		// Edit link
		if (rowObject.editEnable == undefined) {
			edit = "<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.editEnable == 'TRUE') {
			edit = "<a title='" +js_edit+ "' href='#' onClick='ns.admin.editDAGroupForAdd(\"" + rowObject.map.EditURLForGroupAdd + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
			action += edit;
		}

		// View link
		if (rowObject.viewEnable == undefined) {
			view = "<a class='ui-button' title='View' href='#' ><span class='ui-icon ui-icon-info ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.viewEnable == 'TRUE') {
			view = "<a title='View' href='#' onClick='ns.admin.viewGroupInformation(\""+rowObject.map.ViewURLForGroupAdd+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
			action += view;
		}

		// PermissionEnable change link
		if (rowObject.permissionEnable == undefined) {
		 	permissions = "<a class='ui-button' title='" + js_permissions + "' href='#' ><span class='ui-icon ui-icon-gear ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.permissionEnable == 'TRUE') {
			permissions = "<a title='" + js_permissions + "' href='#' onClick='ns.admin.groupPermissions(\"" + rowObject.map.PermissionsURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-viewPermission'></span></a>";
			action += permissions;
		}

		// SubmitForApprovalEnable change link
		if (rowObject.submitForApprovalEnable == undefined) {
			submitForApproval = "<a class='ui-button' title='Submit for Approval' href='#' ><span class='ui-icon ui-icon-circle-check ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.submitForApprovalEnable == 'TRUE') {
			submitForApproval = "<a title='Submit for Approval' href='#' onClick='ns.admin.submitGroupForApproval(\"" + rowObject.map.SubmitURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-approve'></span></a>";
			action += submitForApproval;
		}
		
		// discardEnable change link
		if (rowObject.discardEnable == undefined) {
			discard = "<a class='ui-button' title='Discard Changes' href='#' ><span class='ui-icon ui-icon-cancel ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.discardEnable == 'TRUE') {
			discard = "<a title='Discard Changes' href='#' onClick='ns.admin.discardGroupChange(\"" + rowObject.map.DiscardURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
			action += discard;
		}

		// modifyChangeEnable change link
		if (rowObject.modifyChangeEnable == undefined) {
		 	modifyChange = "<a class='ui-button' title='Modify Changes' href='#' ><span class='ui-icon ui-icon-tag ui-state-disabled' style='float:left;'></span></a>";
		} else if (rowObject.modifyChangeEnable == 'TRUE') {
			modifyChange = "<a title='Modify Changes' href='#' onClick='ns.admin.modifyGroupChange(\"" + rowObject.map.ModifyURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-modifyChanges'></span></a>";
			action += modifyChange;
		}

		// ApproveEnable change link
		if (rowObject.approveEnable == undefined) {
			approve = "";
			} else if (rowObject.approveEnable == 'TRUE') {
			approve = "<a title='Approve' href='#' onClick='ns.admin.approveDADivisionChange(\"" + rowObject.map.ApprovalURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
			action += approve;
		}

		// RejectEnable change link
		if (rowObject.rejectEnable == undefined) {
			reject = "";
		} else if (rowObject.rejectEnable == 'TRUE') {
			reject = "<a title='Reject' href='#' onClick='ns.admin.rejectGroupChange(\"" + rowObject.map.RejectURLForGroup + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-close'></span></a>";
			action += reject;
		}
		
		return action; 
	}else {
		return ' ';
	}

} 

/**
* Refresh Group Module
*/
ns.admin.refreshGroupModule = function()
{
	$.get('/cb/pages/jsp/user/admin_groups_summary.jsp',function(data){
		$('#summary').html(data);
	});
}

ns.admin.editDAGroupNotForAdd = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#editGroupLink').click();
		}
	});
}

ns.admin.editDAGroupForAdd = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			var heading =  $($.parseHTML(data)).filter("#PageHeading").html(); 
			ns.common.updatePortletTitle("detailsPortlet",heading,false);

			$('#inputDiv').html(data);
			$('#permissionsDiv').hide();
			$('#details').hide();
			$('#admin_groupsTab').portlet('fold');
			ns.admin.gotoStep(1);
			$('#details').slideDown();	
			$('#summary').hide();
		}
	});
}

/** This function is used for showing submit for approval popup window. */
ns.admin.viewGroupInformation = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#viewGroupPendingDialogID').html(data).dialog('open');
		}
	});
}

/**
* Modify group pending DA Changes
*/
ns.admin.modifyGroupChange = function(urlString)
{
	$.ajaxSetup({
        async: false
	});
	
	$.ajax({
		url: urlString,
		success: function(data){
		$('#resultmessage').html(data);
		ns.admin.reloadGroups();
		ns.common.showSummaryOnSuccess('summary','done');
		}
	});
	$.ajaxSetup({
        async: true
	});
};

/** This function is used for showing discard popup window. */
ns.admin.discardGroupChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#verifyDiscardGroupPendingDialogID').html(data).dialog('open');
		}
	});
}

/** This function is used for showing submit for approval popup window. */
ns.admin.submitGroupForApproval = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#DAGroupSubmitForApprovalDialogId').html(data).dialog('open');
		}
	});
}

/** This function is used for showing approve popup window. */
ns.admin.approveDAGroupChange = function( urlString )
{
	$.ajax({
		url: urlString,
		success: function(data)
		{
			$('#approvalWizardDialogID').html(data).dialog('open');
            ns.admin.checkResizeApprovalDialog();
		}
	});
};

$.subscribe('DAGroupDiscardChangesCompleteTopics', function(event,data) {
	$.ajaxSetup({
        async: false
	});
	
	ns.common.closeDialog('#verifyDiscardGroupPendingDialogID');
	ns.admin.reloadGroups();
	ns.common.showSummaryOnSuccess('summary','done');
		
	$.ajaxSetup({
        async: true
	});
});

$.subscribe('DAGroupSubmitForApprovalCompleteTopics', function(event,data) {
	$.ajaxSetup({
        async: false
	});
	
	ns.common.closeDialog('#DAGroupSubmitForApprovalDialogId');
	ns.admin.reloadGroups();
	ns.common.showSummaryOnSuccess('summary','done');

	$.ajaxSetup({
        async: true
	});
});

$.subscribe('GroupCompleteTopic', function(event,data) {
	$.ajaxSetup({
        async: false
	});
	setTimeout(function(){ns.common.showStatus();}, 80);
	$.ajaxSetup({
        async: true
	});
});

$.subscribe('DAGroupApprovalReviewCompleteTopics', function(event,data) { 
	$.ajaxSetup({
        async: false
	});
	ns.common.closeDialog('#approvalWizardDialogID');
	var statusMessage = $('#resultmessage').html();
    ns.common.showStatus(statusMessage);
    ns.admin.reloadGroups();
	ns.common.showSummaryOnSuccess('summary','done');
	$.ajaxSetup({
        async: true
	});
});

$.subscribe('closeDeleteGroupDialog', function(event, data)
{
	if(!$('#operationresult').is(':visible')){
		ns.common.closeDialog('#deleteGroupDialogID');
		ns.admin.refreshGroupModule();
	}
});

ns.admin.rejectGroupChange = function( urlString )
{
	ns.admin.approveDAGroupChange(urlString);
}

//Add reject information in pending group grid.
ns.admin.addRejectReasonRowsForGroupPendingChanges = function(gridID){
	if( gridID == "#pendingGroupChangesID" ){
		var ids = jQuery(gridID).jqGrid('getDataIDs');			
		for(var i=0;i<ids.length;i++){  			
			var rowId = ids[i];	
			var daStatus = $(gridID).jqGrid('getCell', rowId, 'status');
			var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
			var rejectedBy = $(gridID).jqGrid('getCell', rowId, 'rejectedBy');
			var currentID = "#pendingGroupChangesID #" + rowId;
			if(daStatus == 'Rejected') {
				rejectReasonTitle = js_reject_reason;
				rejectedByTitle = js_rejected_by;
				if(rejectReason == '' || rejectReason == undefined){
					reasonText = "No Reason Provided";
				} else{
					reasonText = ns.common.spcialSymbolConvertion(rejectReason);
				}
				var className = 'ui-widget-content jqgrow ui-row-ltr';
				if(i % 2 == 1){
					className = "ui-widget-content jqgrow ui-row-ltr ui-state-zebra";
				}
				
				$(currentID).after(ns.common.addRejectReasonRow(rejectedByTitle,rejectedBy,rejectReasonTitle,reasonText,className,5));
			}
		}
	}
}

$.subscribe('completeResetAddedGroup', function(event, data)
{
	$('#permissionsDiv').hide();
	$('#details').hide();
	$('#admin_groupsTab').portlet('fold');
	ns.admin.gotoStep(1);
	$('#details').slideDown();	
});

$.subscribe('refreshGroupGrid', function(event, data)
{
	ns.admin.refreshGroupModule();
});

$.subscribe('completeAccountAccessPermissionsGridLoad', function(event, data)
{
	ns.common.resizeWidthOfGrids();
	ns.admin.addValidationErrorsRow("#accountAccessPermissionsGrid");
	 if($("#accountAccessPermissionsGrid").jqGrid('getGridParam','records') ==1){
		 $("#accountID").lookupbox('destroy');
		var label = $("#accountAccessPermissionsGrid").jqGrid('getRowData',1).displayText;
		var optionDiv ="<option selected>"+label+"</option>";
		$('#accountID').html('');
		$('#accountID').append(optionDiv);
		setAccountLook();
	}
});

function setAccountLook(){
	defaultOption = {value:"All Accounts", label:"All Accounts"};
	$("#accountID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"accountPermissions",
		"size":"45",
		"source":"/cb/pages/common/PermissionAccountsLookupBoxAction.action",
		"defaultOption":defaultOption
	});
}
function setInitDetails(){
	setAccountLook();
	accountSelect();
	$("#accountID").change(accountSelect);
}

$.subscribe('completeaccountAccessPermissionsVerifyGridLoad', function(event, data)
{
	ns.common.resizeWidthOfGrids();
});

//account formatter.
ns.admin.formatAccountColumn = function ( cellvalue, options, rowObject )
{
	var account = rowObject.routingNum + " : " + cellvalue;
	if(rowObject.currencyCode != null) {
		account = account + ' - ' + rowObject.currencyCode;
	}
	if (rowObject.primaryAccount == 1){
		account = account + ' *';
	}
	if (rowObject.coreAccount != 1)
	{
		account = account + ' &#8226;';
	}
	if (rowObject.map.DAPending == 'true') {
		account = "<span class=\"columndataDA\">" + account + "</span>";
	}
	if(rowObject.nickName != null) {
		account = account + '<br><br>' + rowObject.nickName;
	}
	return account;
};


ns.admin.getCheckBoxTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input type=\"checkbox\"";
		elementTag += " " + "name=\"" + arrVal[0] + "\"";
		elementTag += " " + "value=\"" + arrVal[1] + "\"";
		if(arrVal[2].length > 0) {
			elementTag += " " + arrVal[2];
		}
		elementTag += " " + "border=\"0\">";
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getOnlyCheckBoxTag = function(state){
	return "<input type='checkbox'" + state + "/>";
};

ns.admin.getVerifyCheckBoxTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input type=\"checkbox\"";
		if(arrVal[1].length > 0) {
			elementTag += " " + arrVal[1];
		}
		elementTag += " " + "disabled>";
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getAdminTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input";
		elementTag += " " + "type=\"" + arrVal[3] + "\"";
		elementTag += " " + "name=\"" + arrVal[0] + "\"";
		elementTag += " " + "value=\"" + arrVal[1] + "\"";
		if(arrVal[2].length > 0) {
			elementTag += " " + arrVal[2];
		}
		elementTag += " " + "border=\"0\">";
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getVerifyAdminTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input";
		elementTag += " " + "type=\"" + arrVal[3] + "\"";
		elementTag += " " + "name=\"" + arrVal[0] + "\"";
		elementTag += " " + "value=\"" + arrVal[1] + "\"";
		if(arrVal[2].length > 0) {
			elementTag += " " + arrVal[2];
		}
		elementTag += " " + "disabled>";
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getInitTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input type=\"checkbox\"";
		elementTag += " " + "name=\"" + arrVal[0] + "\"";
		elementTag += " " + "value=\"" + arrVal[1] + "\"";
		if(arrVal[2].length > 0) {
			elementTag += " " + arrVal[2];
		}
		elementTag += " " + "border=\"0\">";
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getVerifyInitTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input type=\"checkbox\"";
		elementTag += " " + "name=\"" + arrVal[0] + "\"";
		elementTag += " " + "value=\"" + arrVal[1] + "\"";
		if(arrVal[2].length > 0) {
			elementTag += " " + arrVal[2];
		}
		elementTag += " " + "disabled>";
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getTextBoxTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "<input class=\"ui-widget-content ui-corner-all\" type=\"text\" style=\"width:70px;\" size=\"10\" maxlength=\"17\"";
		elementTag += " " + "name=\"" + arrVal[0] + "\"";
		elementTag += " " + "value=\"" + ns.common.HTMLEncode(arrVal[1]) + "\"";
		elementTag += " " + ">";
		elementTag += " ";
		if(arrVal[3] != undefined && arrVal[3]=="true") {
			if(arrVal[4] != undefined && arrVal[4]=="Y") {
				elementTag += "<span align=\"left\" nowrap=\"\" class=\"columndataDA\">" + arrVal[2] + "</span>";
				if(arrVal[5] != undefined && arrVal[5] != "null") {
					elementTag += "<span class=\"sectionhead_greyDA\">" + arrVal[5] + "</span>";
				}
			} else {
				elementTag += "<span align=\"left\" nowrap=\"\" class=\"columndata\">" + arrVal[2] + "</span>";
			}
		} else {
			elementTag += "<span align=\"left\" nowrap=\"\" class=\"columndata\">" + arrVal[2] + "</span>";
		}
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.getOnlyTextBoxTag = function(state){
	return "<input class=\"ui-widget-content ui-corner-all\" type=\"text\" style=\"width:70px;\" size=\"10\" maxlength=\"17\" disabled/>";
}

ns.admin.getTextTag = function ( obj ) {
	var arrVal = obj.split(";");
	if(arrVal.length > 0) {
		var elementTag = "";
		elementTag += ns.common.HTMLEncode(arrVal[1]);// Limit Value
		elementTag += " ";
		if(arrVal[3] != undefined && arrVal[3]=="true") {
			if(arrVal[4] != undefined && arrVal[4]=="Y") {
				
				elementTag += "<span align=\"left\" nowrap=\"\" class=\"columndataDA\">" + arrVal[2] + "</span>";
				if(arrVal[5] != undefined && arrVal[5] != "null") {
					elementTag += "<span class=\"sectionhead_greyDA\">" + arrVal[5] + "</span>";
				}
			} else {
				elementTag += "<span align=\"left\" nowrap=\"\" class=\"columndata\">" + arrVal[2] + "</span>";
			}
		} else {
			elementTag += "<span align=\"left\" nowrap=\"\" class=\"columndata\">" + arrVal[2] + "</span>";
		}
		return elementTag;
	} else {
		return "";
	}
};

ns.admin.addValidationErrorsRow = function (gridID){
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	for(var i=0;i<ids.length;i++){
		var rowId = ids[i];
		var className = 'ui-widget-content jqgrow ui-row-ltr';
		var columnCount = jQuery(gridID).jqGrid('getGridParam', 'colNames').length - 1;
		var accountIndex =  $(gridID).jqGrid('getCell', rowId, 'map.AccountIndex');
		var rowText = '<tr class=\"' + className + '\" style="display:none;" ><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan=\"' + columnCount +'\" align=\"center\"><span id=\"limit_error' + accountIndex + 'Error\" class="errorLabel"></span></td></tr>';
		$(gridID).find('#' + rowId).after(rowText);
	}
};

ns.admin.formatAdminColumn = function ( cellvalue, options, rowObject )
{
	if(rowObject.map.AdminCheckBox != null) {
		var ele = ns.admin.getAdminTag(rowObject.map.AdminCheckBox);
		return ' ' + ele;
	} else {
		return "&nbsp;";
	}
};

ns.admin.formatVerifyAdminColumn = function ( cellvalue, options, rowObject )
{
	if(rowObject.map.AdminCheckBox != null) {
		var ele = ns.admin.getVerifyAdminTag(rowObject.map.AdminCheckBox);
		return ' ' + ele;
	} else {
		return "&nbsp;";
	}
};

ns.admin.formatInitColumn = function ( cellvalue, options, rowObject )
{
	if (rowObject.map.CanInitRow == 'TRUE' && rowObject.map.InitCheckBox != null) {
		var ele = ns.admin.getInitTag(rowObject.map.InitCheckBox);
		return ' ' + ele;
	} else {
		return ns.admin.getOnlyCheckBoxTag("disabled");// construct disabled checbox
	}
	
};

ns.admin.formatVerifyInitColumn = function ( cellvalue, options, rowObject )
{
	if (rowObject.map.CanInitRow == 'TRUE' && rowObject.map.InitCheckBox != null) {
		var ele = ns.admin.getVerifyInitTag(rowObject.map.InitCheckBox);
		return ' ' + ele;
	} else {
		return ns.admin.getOnlyCheckBoxTag("disabled");// construct disabled checbox
	}
	
}; 

ns.admin.formatLimit1Column = function(cellvalue, options, rowObject) {
	var htmlStr = "";
	htmlStr += "<div><br>";
	if (rowObject.map.transactionLimit != null) {
		htmlStr += ns.admin.getTextBoxTag(rowObject.map.transactionLimit);
	}else{
		htmlStr += ns.admin.getOnlyTextBoxTag("disabled");
		htmlStr += "<span align='left' nowrap='' class='columndata'>"+ ns.admin.label_per_transaction+"</span>";
	}
	htmlStr += "<br><br>";
	if (rowObject.map.weekLimit != null) {
		htmlStr += ns.admin.getTextBoxTag(rowObject.map.weekLimit);
	}else{
		htmlStr += ns.admin.getOnlyTextBoxTag("disabled");
		htmlStr += "<span align='left' nowrap='' class='columndata'>"+ ns.admin.label_per_week+"</span>";
	}
	htmlStr += "</div>";
	return htmlStr;
};

ns.admin.formatVerifyLimit1Column = function(cellvalue, options, rowObject) {
	var htmlStr = "";
	htmlStr += "<div><br>";
	if (rowObject.map.transactionLimit != null) {
		htmlStr += ns.admin.getTextTag(rowObject.map.transactionLimit);
	}else{
		htmlStr += "<span align='left' nowrap='' class='columndata'>" + ns.admin.label_no_limit_per_transaction+ "</span>";		
	}
	htmlStr += "<br><br>";
	if (rowObject.map.weekLimit != null) {
		htmlStr += ns.admin.getTextTag(rowObject.map.weekLimit);
	}else{
		htmlStr += "<span align='left' nowrap='' class='columndata'>"+ ns.admin.label_no_limit_per_week +"</span>";			
	}
	htmlStr += "<br><br></div>";
	return htmlStr;
};

ns.admin.formatExceedWithApproval1Column = function(cellvalue, options, rowObject) {
	var htmlStr = "";
	if (rowObject.map.transactionExceed != null) {
		htmlStr += ns.admin.getCheckBoxTag(rowObject.map.transactionExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	htmlStr += "<span align=\"left\" nowrap=\"\" class=\"columndata\">";
	if(rowObject.map.transactionLimitDisplay != null) {
		htmlStr += "(" + rowObject.map.transactionLimitDisplay + ")";
	} else {
		htmlStr += ns.admin.label_no_maximum;
	}
	htmlStr += "</span>";
	htmlStr += "<br><br>";
	if (rowObject.map.weekExceed != null) {
		htmlStr += ns.admin.getCheckBoxTag(rowObject.map.weekExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	if(rowObject.map.weekLimitDisplay != null) {
		htmlStr += "(" + rowObject.map.weekLimitDisplay + ")";
	} else {
		htmlStr += ns.admin.label_no_maximum;
	}
	return htmlStr;
};

ns.admin.formatVerifyExceedWithApproval1Column = function(cellvalue, options, rowObject) {
	var htmlStr = "";
	if (rowObject.map.transactionExceed != null) {
		htmlStr += ns.admin.getVerifyCheckBoxTag(rowObject.map.transactionExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	htmlStr += "<br><br>";
	if (rowObject.map.weekExceed != null) {
		htmlStr += ns.admin.getVerifyCheckBoxTag(rowObject.map.weekExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	return htmlStr;
};

ns.admin.formatLimit2Column = function(cellvalue, options, rowObject) {
	
	var htmlStr = "";
	htmlStr += "<div><br>";
	if (rowObject.map.dayLimit != null) {
		htmlStr += ns.admin.getTextBoxTag(rowObject.map.dayLimit);
	}else{
		htmlStr += ns.admin.getOnlyTextBoxTag("disabled");
		htmlStr += "<span align='left' nowrap='' class='columndata'>" + ns.admin.label_per_day + "</span>";
	}
	htmlStr += "<br><br>";
	if (rowObject.map.monthLimit != null) {
		htmlStr += ns.admin.getTextBoxTag(rowObject.map.monthLimit);
	}else{
		htmlStr += ns.admin.getOnlyTextBoxTag("disabled");
		htmlStr += "<span align='left' nowrap='' class='columndata'>" + ns.admin.label_per_month + "</span>";
	}
	htmlStr += "<br><br></div>";
	return htmlStr;
};

ns.admin.formatVerifyLimit2Column = function(cellvalue, options, rowObject) {
	
	var htmlStr = "";
	htmlStr += "<div><br>";
	if (rowObject.map.dayLimit != null) {
		htmlStr += ns.admin.getTextTag(rowObject.map.dayLimit);
	}else{
		htmlStr += "<span align='left' nowrap='' class='columndata'>" + ns.admin.label_no_limit_per_day + "</span>";
	}
	htmlStr += "<br><br>";
	if (rowObject.map.monthLimit != null) {
		htmlStr += ns.admin.getTextTag(rowObject.map.monthLimit);
	}else{
		htmlStr += "<span align='left' nowrap='' class='columndata'>" + ns.admin.label_no_limit_per_month + "</span>";
	}
	htmlStr += "<br><br></div>";
	return htmlStr;
};

ns.admin.formatExceedWithApproval2Column = function(cellvalue, options, rowObject) {
	var htmlStr = "";
	if (rowObject.map.dayExceed != null) {
		htmlStr += ns.admin.getCheckBoxTag(rowObject.map.dayExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	htmlStr += "<span align=\"left\" nowrap=\"\" class=\"columndata\">";
	if(rowObject.map.dayLimitDisplay != null) {
		htmlStr += "(" + rowObject.map.dayLimitDisplay + ")";
	} else {
		htmlStr += ns.admin.label_no_maximum;
	}
	htmlStr += "</span>";
	htmlStr += "<br><br>";
	if (rowObject.map.monthExceed != null) {
		htmlStr += ns.admin.getCheckBoxTag(rowObject.map.monthExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	htmlStr += "<span align=\"left\" nowrap=\"\" class=\"columndata\">";
	if(rowObject.map.monthLimitDisplay != null) {
		htmlStr += "(" + rowObject.map.monthLimitDisplay + ")";
	} else {
		htmlStr += ns.admin.label_no_maximum;
	}
	htmlStr += "</span>";
	return htmlStr;
};

ns.admin.formatVerifyExceedWithApproval2Column = function(cellvalue, options, rowObject) {
	var htmlStr = "";
	if (rowObject.map.dayExceed != null) {
		htmlStr += ns.admin.getVerifyCheckBoxTag(rowObject.map.dayExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	htmlStr += "<br><br>";
	if (rowObject.map.monthExceed != null) {
		htmlStr += ns.admin.getVerifyCheckBoxTag(rowObject.map.monthExceed);
	}else{
		htmlStr += ns.admin.getOnlyCheckBoxTag("disabled");
	}
	return htmlStr;
};

/* Validates Account Access Permissions on current Page before moving to next page */
ns.admin.validateAccountAccessPermissionsBeforePaging = function (currentPage,nextPage) {
	removeValidationErrors();
	var urlString = "/cb/pages/user/editAccountAccessPermissions-verifyPage.action";
	$.ajax({
		url: urlString,
		async: false,
		type:"POST",
		data: $("#permLimitFrm").serialize(),
		success: function(data){
			if(data.length > 0) {
				$("#accountAccessPermissionsGrid").jqGrid('setGridParam', {
	                	page:currentPage
	                });
				//trigger verify action				
				$('#permAcctAccessVerifyButtonID').click();
			} else {
				//get the selected rownum
				var rowNumberSelected = $('#gbox_accountAccessPermissionsGrid .ui-pg-selbox :selected').val();
				//set the selected rownum
				$("#accountAccessPermissionsGrid").jqGrid('setGridParam', {rowNum:rowNumberSelected});
				//move to next page
               $('#accountAccessPermissionsGrid').trigger("reloadGrid",[{page:nextPage}]);
			}
		}
	});
};

/* Validates Account Access Permissions on current Page before sorting the page */
ns.admin.validateAccountAccessPermissionsBeforeSorting = function () {
	removeValidationErrors();
	var urlString = "/cb/pages/user/editAccountAccessPermissions-verifyPage.action";
	var hasErrors = "false";
	$.ajax({
		url: urlString,
		async: false,
		type:"POST",
		data: $("#permLimitFrm").serialize(),
		success: function(data){
			if(data.length > 0) {
				hasErrors = "true";
				//trigger verify action
				$('#permAcctAccessVerifyButtonID').click();
			}
		}
	});
	return hasErrors;
};

ns.admin.label_per_day = (typeof js_admin_label_per_day == "undefined") ? "per day" : js_admin_label_per_day;
ns.admin.label_per_month = (typeof js_admin_label_per_month == "undefined") ? "per month" : js_admin_label_per_month;
ns.admin.label_per_transaction = (typeof js_admin_label_per_transaction == "undefined") ? "per transaction" : js_admin_label_per_transaction;
ns.admin.label_per_week = (typeof js_admin_label_per_week == "undefined") ? "per week" : js_admin_label_per_week;
ns.admin.label_no_limit_per_transaction = (typeof js_admin_label_no_limit_per_transaction == "undefined") ? "no limit per transaction" : js_admin_label_no_limit_per_transaction;
ns.admin.label_no_limit_per_week = (typeof js_admin_label_no_limit_per_week == "undefined") ? "no limit per week" : js_admin_label_no_limit_per_week;
ns.admin.label_no_limit_per_day = (typeof js_admin_label_no_limit_per_day == "undefined") ? "no limit per day" : js_admin_label_no_limit_per_day;
ns.admin.label_no_limit_per_month = (typeof js_admin_label_no_limit_per_month == "undefined") ? "no limit per month" : js_admin_label_no_limit_per_month;
ns.admin.label_no_maximum = (typeof js_admin_label_no_maximum == "undefined") ? "(no maximum)" : js_admin_label_no_maximum;

$.subscribe('selectDefaultDivision', function(event, data)
{
	value = $('#dashBoardDivisionIdNew').val();
	label = $('#dashBoardDivisionNameNew').val();
	var validDivision = 1;
	var count = $("#adminDivisionGrid").jqGrid('getGridParam', 'reccount');
	if(value == undefined && count > 0){
		for(validDivision;validDivision<=count;validDivision++){
			if($('#adminDivisionGrid').jqGrid('getCell', validDivision,'map.IsAnAdminOf') == "true"){
			value = $('#adminDivisionGrid').jqGrid('getCell', validDivision,'groupId');
			label = $('#adminDivisionGrid').jqGrid('getCell', validDivision,'groupName');	
			break;
			}
		}				
	}
	
	if(value != undefined){
		$('#divisionMoreOptions').attr("style","display:inline");
		$('.selectBoxHolder').attr("style","display:inline");
		$('#locationSummaryMenuLink').attr("style","display:block");
		$('#addLocationLink').attr("style","display:inline-block");
		$("#dashBoardDivisionId").lookupbox('destroy');
		var optionDiv ="<option selected value="+value+">"+label+"</option>";
		$('#dashBoardDivisionId').html('');
		$('#dashBoardDivisionId').append(optionDiv);
		$("#dashBoardDivisionId").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"size":"30",
			"source":"/cb/pages/user/DivisionLookupBoxAction.action?CollectionName=Descendants&dualApprovalMode=2"
		});	
	}else{
		$('#divisionMoreOptions').attr("style","display:none");
		$('#locationSummaryMenuLink').attr("style","display:none");
	}
	//Set the default selected divisionid
	ns.admin.defaultSelectedDivisionId = value;
	ns.admin.defaultSelectedDivisionName = label;
});