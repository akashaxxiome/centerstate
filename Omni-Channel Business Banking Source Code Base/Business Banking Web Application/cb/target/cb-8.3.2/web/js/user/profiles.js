/**
 * This JS provides the utility for handling select menu widget given by Jquery select menu. 
 * It works on top of jquery select menu.
 * Usage Example : 
 * Create the object of Wizard by using new call. 
 * var myObj = new selectWizard();
 * Then call the public init method which takes html id of the select element.
 * myObj.init("htmlId");
 * we can also pass optional config options which will be used by jquery selectmenu.
 * Say, if we want to set the width to 250.
 * we will call: myObj.init("htmlId", {width: 250});
 * 
 * We can call various method listed in the wizard API directly after init. 
 * Like: myObj.getSelectedVal() will return the current selected value.
 * See the below wizard for various methods.
 * 
 * Important, if we make any config changes and select option changes we need to call myObj.apply() method in order 
 * to see the changes take effect.
 *  
 *  */
//Start of API //
var selectWizard = function(){
	var element = null;
	var defaultConfig = {width: 150};
	return{
		//Creates a new template object with given html template string and data object.
		init: function(selectId, config){
			element = $("#" + selectId);
			$.extend(defaultConfig, config);
			element.selectmenu(defaultConfig);
			return element;
		},
		//Returns the value of the selected item.
		getSelectedVal: function(){
			return element.val();			
		},
		//Clears all the options from the select dropwdown.
		clearOptions: function() {
			element.find('option').remove().end();
		},
		//Adds the option to the select object element.
		addOption: function(name, value){
			var option = "<option value='" + value + "'>" +  name + "</option>";
			$(option).appendTo(element);
		},
		apply: 	function() {
			element.selectmenu();
		},
		setValue:	function(value) {
			element.selectmenu("value", value);
		},
		addChangeEvent: function(event) {
			element.selectmenu({change: event});
		},
		disable:	function() {
			element.selectmenu("disable");
		},
		populateOptionJSON:	function(json) {
			var i, 
			profile,
			profileOptions = "";
			this.clearOptions();
			this.addOption("Select a profile", "-1");
			for (i = 0; i < json.length; i++) {
				profile = json[i];
				if(profile.crossChannelProfileId == -1) continue;
				this.addOption(profile.crossChannelProfileName, profile.crossChannelProfileId);
			};
			this.apply();
		},
		//This utility method is useful for entitlement related select boxes which
		//returns the channel profile based on the profile id.
		getEntitledProfileById:	function(profileId){
			var entitledProfile;
			for (i = 0; i < ns.users.entitlementProfile.length; i++) {
				profile = ns.users.entitlementProfile[i];
				if(profile.crossChannelProfileId === profileId){
					entitledProfile = profile;
				}
			};
			return entitledProfile;
		}
	}
}
//END of API //

//Basic functions.
//Populates the different channels select menu based on profile object
function populatePerChannelProfiles(oProfile){
	var key,
	sChannelName,
	oChannelProfile,
	aChannelProfiles,
	aChannels,
	sChannelProfileOptions = "";
	
	aChannels = oProfile.channelProfileList;
	
	for(key in aChannels){
		sChannelName = key;
		aChannelProfiles = aChannels[key];
		
		sChannelProfileOptions = "";
		//Creating and initiliazing the select box from Wizard.
		var perSelectObj = new selectWizard();
		perSelectObj.init( "selectEntitlementGroup_" + sChannelName );
		//Clearing old options if any.
		perSelectObj.clearOptions();
		//Adding default option to the select box.
		perSelectObj.addOption("Select", "-1");
		//Adding the profiles to the select box.
		for (var i = 0; i < aChannelProfiles.length; i++) {
			oChannelProfile = aChannelProfiles[i];
			if(oChannelProfile == undefined) continue;
			perSelectObj.addOption(oChannelProfile.profileName, oChannelProfile.profileId);
		};
		//Now important calling the apply method to make sure changes are applited to selectmenu.
		perSelectObj.apply();
	}
	
};

//Clearing the channels profiles for per channels.
function clearChannelProfiles(oProfile) {
	var sChannelName;
	var aChannelProfiles;
	var aChannels = oProfile.channelProfileList;
	for(key in aChannels){
		sChannelName = key;
		aChannelProfiles = aChannels[key];
		var perSelectObj = new selectWizard();
		perSelectObj.init( "selectEntitlementGroup_" + sChannelName );
		perSelectObj.clearOptions();
		perSelectObj.addOption("Select", "-1");
		perSelectObj.apply();
	}
}
/**
 * Adding the user profiles based upon the cross channel selected. 
 * According to the selected Cross channel populating the info of the child profiles 
 * in the list. 
 * Also adding and selecting the individual profiles in the per profiles select boxes for form submission.
 * @param oProfile
 */
function populateCrossChannelProfiles(oProfile) {
	var aChannels;
	$("#crossChannelChildId").empty();
	
	clearChannelProfiles(oProfile);
	if(crossSel != undefined) {
		aChannels = oProfile.channelProfileList;
		for(key in aChannels){
			sChannelName = key;
			aChannelProfiles = aChannels[key];
			for (var i = 0; i < aChannelProfiles.length; i++) {
				oChannelProfile = aChannelProfiles[i];
				
				if(oChannelProfile.channelId == undefined ) continue;
				var desc = getChannelDescription(oChannelProfile.channelId);
				html = "<li><span>" + oChannelProfile.channelId /*+ " : " + oChannelProfile.profileName + "</li>"*/;
				html += " :&nbsp&nbsp" + desc + "</span></li>";
				var perSelectObj = new selectWizard();
				perSelectObj.init( "selectEntitlementGroup_" + sChannelName );
				perSelectObj.addOption(oChannelProfile.profileName, oChannelProfile.profileId);
				perSelectObj.apply();
				$("#crossChannelChildId").append(html);
				perSelectObj.setValue(oChannelProfile.profileId);
			}
		}
	}
}

/**
 * Method to handle cross channel profile change.
 * @param event
 * @param data
 */
function crossProfileChange(event, data) {
	var selectedItem,
	profileId,
	oProfile;
	//selectedItem = data.item;
	//profileId = selectedItem.value;
	profileId = data.value;
	$("#XError").remove();
	populateChannelsForProfile(profileId);
}

function populateChannelsForProfile(profileId) {
	$.ajax({
		type: "POST",
		url: channelUrl,
		data: {sharedProfileId : profileId},
		success: function(response){
			$("#crossChannelChildId").html(response);
	   }
	});
	
}

function showCrossChannel() {
	$(".markerChannels").each(function() {
		$(this).css('display', 'none');
	});
	$("#crossProfilesId").show();
}

function hideCrossChannel() {
	$(".markerChannels").each(function() {
		$(this).css('display', 'block');
	});
	$("#crossProfilesId").hide();
}

/**
 * This method will be used in case Edit User where we need to populate the pre selected profiles.
 * If the user has any pre selected profile the JSON data will have additional object data of
 * selEntitlementProfilesMap which will have all the profiles associated with the user.
 * 
 * @param oProfiles
 */
function checkSelectedProfile(oProfiles) {
	var profile;
	var selProfileMap;
	var perProfile;
	for (i = 0; i < oProfiles.length; i++) {
		profile = oProfiles[i];
		if(profile.selEntitlementProfilesMap) {
			//Retrieve the selEntitlementProfilesMap from JSON.
			selProfileMap = profile.selEntitlementProfilesMap;
			var crossX = selProfileMap["X"];
			if(crossX) {
			//It's cross channel so process it accordingly.
				crossSel.setValue(crossX);
				perProfile = crossSel.getEntitledProfileById(crossX);
				populateCrossChannelProfiles(perProfile);
				$("#crossXRadioId").prop("checked", true);
				showCrossChannel();
			}else {
				//Per channel.
				perProfile = crossSel.getEntitledProfileById("-1");
				populatePerChannelProfiles(perProfile);
				//Per channel is selected.
				var webSelProfile = selProfileMap["Web"];
				var smsSelProfile = selProfileMap["Sms"];
				var mobileSelProfile = selProfileMap["Mobile"];
				if(webSelProfile) {
					$("#selectEntitlementGroup_Web").selectmenu();
					$("#selectEntitlementGroup_Web").selectmenu("value", webSelProfile);
				}
				if(smsSelProfile) {
					$("#selectEntitlementGroup_Sms").selectmenu();
					$("#selectEntitlementGroup_Sms").selectmenu("value", smsSelProfile);
				}
				if(mobileSelProfile) {
					$("#selectEntitlementGroup_Mobile").selectmenu();
					$("#selectEntitlementGroup_Mobile").selectmenu("value", mobileSelProfile);
				}
				$("#perChannelRadioId").prop("checked", true);
				hideCrossChannel();
			}
		}else {
			//If the user doesn't have any profile associated with him then select the cross channel
			//by default.
			$("#crossXRadioId").click();
		}
	}
}

function getChannelDescription(channelId) {
	var data = ns.users.channelList;
	var description;
	for(var key in data) {
		if(data.hasOwnProperty(key)){
			if(channelId == data[key].id) {
				description = data[key].description;
			}
		}
	}
	return description;
}