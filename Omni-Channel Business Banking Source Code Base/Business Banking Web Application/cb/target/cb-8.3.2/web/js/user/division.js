ns.division.setup = function()
{
	ns.common.refreshDashboard('dbDivisionSummary');
	
}
$(document).ready(function()
{
	$.debug(false);
	ns.division.setup();
});

$.subscribe('errorDeleteLocation', function(event,data) {  
	ns.common.closeDialog('#deleteLocationDialogID');
});

$.subscribe('cancelDivisionForm', function(event, data){
	ns.admin.closeDivisionDetails();
	ns.common.hideStatus();
	ns.division.reloadPreDivision(function(){
		// check if data is undefined or not, as in case of operations from grid we have used result and hence we will not have data.
		// in this case we need to set is to done
		var buttonType = 'done';
		if(data != undefined){
			buttonType = $('#'+data.id).attr('buttontype');
			if($.trim(buttonType) == ''){
				buttonType = 'done';
			}
		}
		ns.common.showSummaryOnSuccess("summary",buttonType);
		$.publish('cleanupDivisionDetails');
		ns.common.selectDashboardItem("divisionsLink");
	});
});
		
$.subscribe('redefineLocation', function(event, data)
{
	var isDA = $('#isDA').val();
	if($.trim(isDA) == "true"){
		ns.division.reloadPreLocation();
		ns.division.reloadPreDivision(ns.division.divisionDropDownReload);
	}
});

ns.division.divisionDropDownReload = function(){
	$.ajax({
		url:  "/cb/pages/user/DivisionLookupBoxAction.action?CollectionName=Descendants&dualApprovalMode=2&isBlank=true",
		success: function(data) {
			if(data.optionsModel.length > 0){
				$("#dashBoardDivisionId").lookupbox('destroy');
				value = data.optionsModel[0].id;
				label = data.optionsModel[0].label;
				var optionDiv ="<option selected value="+value+">"+label+"</option>";
				$('#dashBoardDivisionId').html('');
				$('#dashBoardDivisionId').append(optionDiv);
				$("#dashBoardDivisionId").lookupbox({
					"controlType":"server",
					"collectionKey":"1",
					"size":"30",
					"source":"/cb/pages/user/DivisionLookupBoxAction.action?CollectionName=Descendants&dualApprovalMode=2"
				});
				$('.selectBoxHolder').attr("style","display:inline");
				$('#addLocationLink').attr("style","display:inline-block");
				ns.division.reloadPreLocation();
			}else{
				// as the user has no permission to make changes to location for which he has no admin rights we clear the grid data
				setTimeout(function(){
					$('#adminLocationGrid').jqGrid('clearGridData');
				},100);
				$('#dashBoardDivisionId').html('');
				$('.selectBoxHolder').attr("style","display:none");
				$('#addLocationLink').attr("style","display:none");
				ns.common.selectDashboardItem($(".summaryLabelCls:visible").prop('id'));
			}
		},
		error: function() {
		}
	});
};

ns.division.reloadPreDivision = function(successFunction)
{
	$.ajax({
		  type: "GET",
		  url: "/cb/pages/jsp/user/inc/corpadmindiv-pre.jsp",
		  success: function(data) {
				$("#tempDivIDForMethodNsAdminReloadDivisions").html(data);
				if( ns.admin.pendingApprovalDivisionsNum == "0" ){
					$('#division_PendingApprovalTab').attr('style','display:none');
				} else {
					$('#division_PendingApprovalTab').removeAttr('style');
				}
				
				// Call extra Success function, if provided.	
				if(successFunction && (typeof successFunction === 'function')){
					successFunction.call();
				}
		  }
	});
};

ns.division.reloadPreLocation = function()
{
	var url="/cb/pages/jsp/user/corpadminlocations-selected.jsp?EditDivision_GroupId="+$('#dashBoardDivisionId').val();
	$.ajax({
		  type: "GET",
		  url: url,
		  success: function(data) {
		  }
	});
};