	ns.billpay.showPendingApprovalTab = true;
	ns.billpay.showPendingApprovalTabOnCancel = false;
	ns.billpay.showPendingApprovalTabOnSearch = false;
	ns.billpay.pendingApprovalRecordCount = 0;
	ns.billpay.isLoadBillPayTemplate = false;

	$.subscribe('pendingApprovalBillpayGridCompleteEvents', function(event,data) {
		ns.billpay.showPendingApprovalTabOnLoadCount+=1;
		ns.billpay.handleGridEventsAndSetWidth();

		//Add additional row(memo information)
		$("#pendingApprovalBillpayGridId").data("isMemoAdded",false);
		ns.billpay.AddPendingApprovalShowMemoButton("pendingApprovalBillpayGridId");
		ns.billpay.addApprovalsRowsForPendingApprovalBillpay("#pendingApprovalBillpayGridId");
	});


	$.subscribe('pendingBillpayGridCompleteEvents', function(event,data) {
		// Check whether pending approval payments are present or not and based on number of records it will hide/show records.
		if (ns.home.appUserType == "Consumers" || ns.home.appUserType == "Small Business") {
			if (ns.billpay.showPendingApprovalTab === true) {
				if (ns.billpay.pendingApprovalRecordCount > 0) {
					$("#approving.gridTabDropdownItem").show();
				} else {
					//var selectedIndex = $("#billpayGridTabs").tabs('option', 'active');
					//selectedIndex = selectedIndex == 0 ? 1 : selectedIndex;
					//$("#billpayGridTabs").tabs('option','active', selectedIndex);
				}
			}
		}

		ns.billpay.handleGridEventsAndSetWidth();

		//Add additional row(approval information)
		$("#pendingBillpayGridId").data("isMemoAdded",false);
		ns.billpay.AddPendingShowMemoButton("pendingBillpayGridId");
	});

	$.subscribe('completedBillpayGridCompleteEvents', function(event,data) {
		ns.billpay.handleGridEventsAndSetWidth();

		//Add additional row(approval information)
		$("#completedBillpayGridId").data("isMemoAdded",false);
		ns.billpay.AddCompletedShowMemoButton("completedBillpayGridId");
	});

	$.subscribe('singleBillpayTempGridCompleteEvents', function(event,data) {
		ns.billpay.handleTempGridEventsAndSetWidth();
	});

	$.subscribe('multiBillpayTempGridCompleteEvents', function(event,data) {
		ns.billpay.handleTempGridEventsAndSetWidth();
	});

	$.subscribe('billpayPayeeGridCompleteEvents', function(event,data) {
		ns.common.resizeWidthOfGrids();
	});

	$.subscribe('billpayBankGridCompleteEvents', function(event,data) {
		if( $("#billpayBanksDiv").length > 0 ){
			grid_width= $("#billpayBanksDiv").width()  - 20;
			$("#billpayBanksGridId").jqGrid('setGridWidth', grid_width, true);
		}
	});

	$.subscribe('cancelSingleBillpayComplete', function(event,data) {
		ns.billpay.showPendingApprovalTabOnCancel=true;
		$.publish("reloadSelectedBillpayGridTab");
		ns.common.closeDialog('#deleteBillpayDialogID');
	});

	$.subscribe('skipSingleBillpayComplete', function(event,data) {
		ns.billpay.showPendingApprovalTabOnCancel=true;
		$.publish("reloadSelectedBillpayGridTab");
		ns.common.closeDialog('#skipBillpayDialogID');
	});

	$.subscribe('cancelSingleBillpayTemplateComplete', function(event,data) {
		ns.common.closeDialog('#deleteBillpayTempDialogID');
	});

	//After the Billpay templates is deleted, the grid summary should be reloaded.
	$.subscribe('cancelSingleBillpayTemplateSuccessTopics', function(event,data) {
		ns.billpay.reloadSingleBillpayTemplatesSummaryGrid();
		ns.common.showStatus();
	});

	$.subscribe('cancelMultipleBillpayTemplateComplete', function(event,data) {
		ns.common.closeDialog('#deleteBillpayTempDialogID');
	});

	$.subscribe('cancelMultipleBillpayTemplateSuccessTopics', function(event,data) {
		ns.billpay.reloadMultipleBillpayTemplatesSummaryGrid();
		ns.common.showStatus();
	});

	$.subscribe('cancelBillpayPayeeComplete', function(event,data) {
		$('#billpayPayeeGridId').trigger("reloadGrid");
		ns.common.closeDialog('#deletePayeeDialogID');
		ns.common.showStatus();
	});

	$.subscribe('sendInquiryBillpayFormSuccess', function(event,data) {
		ns.common.closeDialog("#inquiryBillpayDialogID");
		//Display the result message on the status bar.
		ns.common.showStatus();
        $.publish('refreshMessageSmallPanel');
	});

	$.subscribe('singleTemplatesGridCompleteEvents', function(event,data) {
		customTypeColumnForTemplate("#singleBillpayTemplatesGridID");
		customFromAccountNickNameColumnForTemplate("#singleBillpayTemplatesGridID");
		customToAccountNickNameColumnForTemplate("#singleBillpayTemplatesGridID");
		ns.billpay.addSubmittedForRowsForBillpayTemplates("#singleBillpayTemplatesGridID");
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
		//display estimated amount indicator for single template
		ns.billpay.displayEstimatedAmountIndicator("#singleBillpayTemplatesGridID");
	});

	$.subscribe('multileTemplatesGridCompleteEvents', function(event,data) {
		customTypeColumnForTemplate("#multipleBillpayTemplatesGridID");
		customFromAccountNickNameColumnForTemplate("#multipleBillpayTemplatesGridID");
		customToAccountNickNameColumnForTemplate("#multipleBillpayTemplatesGridID");
		ns.billpay.addSubmittedForRowsForBillpayTemplates("#multipleBillpayTemplatesGridID");
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
		//display estimated amount indicator for multiple template
		ns.billpay.displayEstimatedAmountIndicator("#multipleBillpayTemplatesGridID");
	});


	//After the Billpay is deleted, the grid summary should be reloaded.
	$.subscribe('cancelBillpaySuccessTopics', function(event,data) {

		ns.common.showStatus();
		ns.billpay.reloadActiveTab();
	});


	ns.billpay.handleGridEventsAndSetWidth = function (){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	}

	ns.billpay.handleTempGridEventsAndSetWidth = function (){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	}

	ns.billpay.formatPendingApprovalBillpayActionLinks = function (cellvalue, options, rowObject) {
		//cellvalue is the ID of the Billpay.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.billpay.viewBillpayDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' href='#' onClick='ns.billpay.editSingleBillpay(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.billpay.deleteBillpay(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
		var skip  = "<a title='" +js_skip+ "' href='#' onClick='ns.billpay.skipBillpay(\""+ rowObject.map.SkipURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-skipRecurring'></span></a>";
		var inquiry  = "<a title='" +js_inquiry+ "' href='#' onClick='ns.billpay.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canEdit"] == "false") {
			edit = "";
		}
		if(rowObject["canDelete"] == "false") {
			del = "";
		}
		if(rowObject["canSkip"] == "false" || rowObject["recModel"] == true) {
			skip = "";

		}
		if(rowObject.map.canInquire == "false") {
			inquiry = "";
		}
		//if resultOfApproval="Deletion", and status didnot equal "15", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["status"]!="15")
		{
			del = "";
            edit = "";
			skip = "";
		}
		if(rowObject["resultOfApproval"] == "Skip")
		{
			skip = "";
		}
		return ' ' + view + ' ' + edit + ' '  + del + ' '  + skip + ' ' + inquiry;
	};

	ns.billpay.formatPendingBillpayActionLinks = function (cellvalue, options, rowObject) {
		//cellvalue is the ID of the Billpay.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.billpay.viewBillpayDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.billpay.editSingleBillpay(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.billpay.deleteBillpay(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
		var skip  = "<a title='" +js_skip+ "' href='#' onClick='ns.billpay.skipBillpay(\""+ rowObject.map.SkipURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-skipRecurring'></span></a>";
		var inquiry  = "<a title='" +js_inquiry+ "' href='#' onClick='ns.billpay.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canEdit"] == "false") {
		edit = "";

		}
		if(rowObject["canDelete"] == "false") {
		del = "";

		}
		if(rowObject["canSkip"] == "false") {
			skip = "";

		}
		if(rowObject.map.canInquire == "false") {
	         inquiry = "";
     	}

		return ' ' + view + ' ' + edit + ' '  + del + ' '  + skip + ' ' + inquiry;
	};

	ns.billpay.formatCompletedBillpayActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the Billpay.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.billpay.viewBillpayDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var inquiry  = "<a title='" +js_inquiry+ "' href='#' onClick='ns.billpay.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject.map.canInquire == "false") {
			inquiry = "";

		}
		return ' ' + view + ' ' + inquiry;
	};

	ns.billpay.formatMultiBillpayTemplatesActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the Billpay.
		var view = "<a class='' title='" +js_view+ "' href='#' onClick='ns.billpay.viewMultipleBillpayTemplate(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var load = "<a class='' title='" +js_load+ "' href='#' onClick='ns.billpay.loadMultipleBillpayTemplate(\""+ rowObject.map.LoadURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
		var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.billpay.editMultipleBillpayTemplate(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.billpay.deleteMultipleBillpayTemplate(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";

		if(rowObject["map"].canLoad == "false" ) {
			load = "";
		}
		if(rowObject["map"].canEdit == "false") {
			edit = "";
		}
		if(rowObject["map"].canDelete == "false") {
			del = "";
		}

		return view + ' ' + load + ' ' + edit + ' '  + del  ;
	};

	ns.billpay.formatSingleBillpayTemplatesActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the Billpay.
		var payementType = rowObject["paymentType"];
		var view = "<a class='' title='" +js_view+ "' href='#' onClick='ns.billpay.viewBillpayDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var load = "<a class='' title='" +js_load+ "' href='#' onClick='ns.billpay.loadSingleBillpayTemplate(\""+ rowObject.map.LoadURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
		var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.billpay.editSingleBillpayTemplate(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.billpay.deleteSingleBillpayTemplate(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";

		if(rowObject["map"].canLoad == "false" ) {
			load = "";
		}
		if(rowObject["map"].canEdit == "false") {
			edit = "";
		}
		if(rowObject["map"].canDelete == "false") {
			del = "";
		}

		return view + '' + load + ' ' + edit + ' '  + del  ;
	};

	ns.billpay.formatBillpayPayeesActionLinks = function (cellvalue, options, rowObject) {
		//cellvalue is the ID of the Payee.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.billpay.viewBillpayPayee(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.billpay.editBillpayPayee(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.billpay.deleteBillpayPayee(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

		return view + ' ' + edit + ' '  + del  ;
	};

	ns.billpay.formatBillpayBanksActionLinks = function (cellvalue, options, rowObject) {
		var insert = "<a class='' title='" +js_insert+ "' href='#' onClick='ns.billpay.insertBillpayBank(\""+ rowObject.achRoutingNumber +"\", \"Bank\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		return insert  ;
	};

	ns.billpay.formatBillpayBanksNameLinks = function (cellvalue, options, rowObject) {
		var insert = "<a class='ui-button' title='" +js_insert+ "' href='#' onClick='ns.billpay.insertBillpayBank(\""+ rowObject.achRoutingNumber +"\", \"Bank\");'>"+ rowObject.institutionName +"</a>";
		return insert  ;
	};

	ns.billpay.formatPeyeeColumn = function (cellvalue, options, rowObject){
		return "Multiple";
	};

	//Formatter of Frequency column
	ns.billpay.formatBillpayFrequencyColumn = function (cellvalue, options, rowObject){

		if(cellvalue == undefined ){
			if( rowObject.frequency == undefined ){
				return "None";
			} else {
				return rowObject.frequency;  //rowObject.frequency has value, means the Billpay is a RecBillpay.
			}
		} else {
			return cellvalue;
		}
	};

	//Formatter of Amount column.
	ns.billpay.formatAmountColumn = function ( cellvalue, options, rowObject ){
		var amount = cellvalue + ' ' + rowObject["amountValue"].currencyCode;
		return amount;
	};

	ns.billpay.formaAccountAndPayee = function ( cellvalue, options, rowObject ){
		var from =  ns.common.HTMLEncode( rowObject.account.accountDisplayText );
		var to = rowObject.payeeName;
		var payeeNickName =  rowObject.payee.nickName;
		var userAccountNumber = rowObject.payee.userAccountNumber;
		var nickNameAndAccNumber=payeeNickName;

		if(userAccountNumber!=undefined && userAccountNumber!=""){
			nickNameAndAccNumber=nickNameAndAccNumber+" -- "+userAccountNumber;
		}
		return "<span class='formatCombinedAccountColumn'>"+from + "</span><span class='gridColumnIcon'><span class='ffiUiMiscIco ffiUiIco-misc-multilinerowdataindicatorico'></span>" +to+" ( "+nickNameAndAccNumber+" ) "+"</span>";
	};

	//Formatter of multiple template summary Amount column
	ns.billpay.formatMultipleTemplateAmountColumn = function ( cellvalue, options, rowObject ){
		var amount = "";
		if( cellvalue == "" || rowObject["amountValue"].currencyStringNoSymbol == '0.00'){
			amount = "--";
		} else {
			amount = rowObject["amountValue"].currencyStringNoSymbol + ' ' + rowObject["amountValue"].currencyCode;
		}
		return amount;
	};

	//Formatter of multiple templates status column.
	ns.billpay.formatMultipleTemplateCommonStatusNameColumn = function ( cellvalue, options, rowObject ){
		if( cellvalue == undefined || cellvalue =="" ){
			return "--";
		} else {
			return cellvalue;
		}
	};

	ns.billpay.formatApproversInfo = function(cellvalue, options, rowObject) {
		var approverNameInfo="";
		var approverNameOrUserName="";
		var approverGroup="";
		var approverInfos=rowObject.approverInfos;
			if(approverInfos!=undefined)
			{
				for(var i=0;i<approverInfos.length;i++){
				var approverInfo = approverInfos[i];
				if(i!=0)
				{
				approverNameInfo="," + approverNameInfo
				}

				var approverName = approverInfo.approverName;
				var approverIsGroup=approverInfo.approverIsGroup;

				if( approverIsGroup){
					approverGroup = "(" + js_group + ")";
				} else {
					approverGroup = "";
				}
				approverNameOrUserName = approverName + approverGroup;
				approverNameInfo=approverNameOrUserName+approverNameInfo;
				}
			}
			return approverNameInfo;
};

	//Add approval information in the pending approval summary gird.
	ns.billpay.addApprovalsRowsForPendingApprovalBillpay = function (gridID){
		if( gridID = "#pendingApprovalBillpayGridId" ){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			var approvalsOrReject = "";
			var approvalerNameOrRejectReason = "";
			var rejectedOrSubmitedBy = "";
			var approverNameOrUserName = "";
			var approverGroup = "";
			var submittedForTitle = js_submitted_for;

			for(var i=0;i<ids.length;i++){
				var rowId = ids[i];
				var approverName = ns.common.HTMLEncode($('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'approverName'));
				var userName = ns.common.HTMLEncode($('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'userName'));
				var approverIsGroup = $('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'approverIsGroup');
				var status = $('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'status');
				var rejectReason = $('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'rejectReason');
				var approverIsGroup = $('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'approverIsGroup');
				var approverNameInfo = ns.common.HTMLEncode($('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'approverInfos'));
				var submittedFor = $('#pendingApprovalBillpayGridId').jqGrid('getCell', rowId, 'displayTextRoA');
				//if status is 15, means that the Billpay is rejected.
				var currentID = "#pendingApprovalBillpayGridId #" + rowId;


				if( status == 15 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approvalerNameOrRejectReason = "No Reason Provided";
					} else{
						approvalerNameOrRejectReason = ns.common.spcialSymbolConvertion( rejectReason );
					}

					if(approverNameInfo != ""){
	                    var className = "pltrow1"
	                        if(rowId % 2 == 0){
	                            className = "pdkrow";
	                        }
	                            className = 'ui-widget-content jqgrow ui-row-ltr';
	                            /*$(currentID).after('<tr class=\"' + className + '\"><td colspan="10" align="center" style="overflow:auto;">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
	                            '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>' + approvalsOrReject + ' &nbsp&nbsp&nbsp  </b>' + approvalerNameOrRejectReason +'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>' + rejectedOrSubmitedBy  + '</b> &nbsp&nbsp&nbsp ' +  approverNameOrUserName + '&nbsp</td></tr>');*/

	                            $(currentID).after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameInfo,approvalsOrReject,approvalerNameOrRejectReason,className,10));
	                    } else {
	                            var className = "pltrow1"
	                            if(rowId % 2 == 0){
	                                className = "pdkrow";
	                            }
	                            className = 'ui-widget-content jqgrow ui-row-ltr';

	                            $(currentID).after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
	                        }
	                    //if status is not 15
				} else {
					approvalsOrReject = js_approval_waiting_on;
					approvalerNameOrRejectReason = approverNameInfo;
					rejectedOrSubmitedBy = js_submitted_by;
					approverNameOrUserName = userName;

					if(approverNameInfo != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
						//remove the grid class, and add FF class.
						//$(currentID).removeClass();
						className = 'ui-widget-content jqgrow ui-row-ltr';
						$(currentID).after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));
					} else {
	                    var className = "pltrow1";
	                    if(rowId % 2 == 0){
	                        className = "pdkrow";
	                    }
	                    //remove the grid class, and add FF class.

	                    //QTS 638252: if a transaction (ACH, Wires, Billpay, billpay, cashcon) is in approvals,
	                    // is then edited, then values are modified so it exceeds the limits with no approval
	                    // OR exceeds bank limits, it will get an error.  If the transaction is then cancelled,
	                    // the transaction is still in the "Approvals Summary" window, but not in approvals
	                    // because it was cancelled and not resubmitted.  Added the following error message to the
	                    // "Approvals Summary" window, "Submission to approvals FAILED, please resubmit or delete"
	                    // so the transaction can be edited to start submission again.
	                    //$(currentID).removeClass();
	                    className = 'ui-widget-content jqgrow ui-row-ltr';
	                    //$(currentID).addClass(className);  <tr class=\"' + className + '\">
	                    //$(currentID).after('<tr class=\"' + className + '\"><td colspan="2">&nbsp</td><td colspan="2"><b>' + approvalsOrReject + ' &nbsp&nbsp&nbsp  </b>' + approvalerNameOrRejectReason +
	                    //'</td><td colspan="3"><b>' + rejectedOrSubmitedBy  + '</b> &nbsp&nbsp&nbsp ' +  approverNameOrUserName + '</td><td colspan="4">&nbsp</td></tr>');
	                    $(currentID).after('<tr class=\"' + className + ' skipRowActionItemRender\"><td colspan="10" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
	                    '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>');
	                }
				}
			}
		}
	};

	//Add "Submitted For" information in the Billpay templates summary gird.
	ns.billpay.addSubmittedForRowsForBillpayTemplates = function (gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		var submittedForTitle = js_submitted_for;
		var className = 'ui-widget-content jqgrow ui-row-ltr';

		var insertedRowCount = 0;
		var toInsertRowNum = 0;

		for(var i=0;i<ids.length;i++){

			var rowId = parseInt(ids[i]);

			var resultOfApproval = "";
			resultOfApproval = $(gridID).jqGrid('getCell', rowId, 'displayTextRoA');

			if( gridID == '#singleBillpayTemplatesGridID' && $(gridID).jqGrid('getCell', rowId, 'status') == "8" ){
				toInsertRowNum = rowId + insertedRowCount;
				var currentID = "#singleBillpayTemplatesGridID tr:nth-child(" + toInsertRowNum + ")";
				$(currentID).after("<tr class=\"" + className + "\"><td colspan=\"9\" align=\"center\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
					"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>" + submittedForTitle + " &nbsp&nbsp&nbsp  </b>" + resultOfApproval + "</td></tr>");
				++insertedRowCount;
			} else if( gridID == '#multipleBillpayTemplatesGridID'
				&& ( $(gridID).jqGrid('getCell', rowId, 'commonStatusName')=="Approval Pending"
						|| $(gridID).jqGrid('getCell', rowId, 'commonStatusName')=="--") )
			{
				toInsertRowNum = rowId + insertedRowCount;
				resultOfApproval = "Processing";
				var currentID  = "#multipleBillpayTemplatesGridID tr:nth-child(" + toInsertRowNum + ")";

				$(currentID).after("<tr class=\"" + className + "\"><td colspan=\"8\" align=\"center\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
					"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>" + submittedForTitle + " &nbsp&nbsp&nbsp  </b>" + resultOfApproval + "</td></tr>");
				++insertedRowCount;
			}
		}
	};



	ns.billpay.customAccountNicknameColumn = function(cellvalue, options, rowObject) {

			//Construct "From Account Nickname" column.
			var _fromAccountDisplayText = rowObject["account"].displayText;
			var _fromAccountNickName = rowObject["account"].nickName;
			var _fromAccountCurrencyCode = rowObject["account"].currencyCode;
			var _fromAccountName = _fromAccountDisplayText;
			if( _fromAccountNickName != undefined  &&  _fromAccountNickName != "" ){
				_fromAccountName = _fromAccountName + " - " + _fromAccountNickName;
			}
			if( _fromAccountCurrencyCode != undefined  &&  _fromAccountCurrencyCode != "" ){
				_fromAccountName = _fromAccountName + " - " + _fromAccountCurrencyCode;
			}
			return  _fromAccountName;

	}


	//Reload Billpay summary jgrid..
	ns.billpay.reloadBillpaySummaryGrid = function ( gridId ){
		ns.common.setFirstGridPage('#pendingApprovalBillpayGridId');
		ns.common.setFirstGridPage('#pendingBillpayGridId');
		ns.common.setFirstGridPage('#completedBillpayGridId');
		$('#pendingApprovalBillpayGridId').trigger("reloadGrid");
		$('#pendingBillpayGridId').trigger("reloadGrid");
		$('#completedBillpayGridId').trigger("reloadGrid");
		ns.billpay.reloadHomePendingPaymentsSummaryGrid();
	};

	//Reload the pending payments portlet grid on home page
	ns.billpay.reloadHomePendingPaymentsSummaryGrid = function(){
		ns.common.setFirstGridPage('#pendingPaymentsSummaryID');
		$('#pendingPaymentsSummaryID').trigger("reloadGrid");
	};

	ns.billpay.reloadActiveTab = function(){
		$(".gridSummaryContainerCls").each(function(i){
			// checks if style is block for which grid type
			if($(this).attr('style').indexOf('block')!=-1){
		        var gridId = $(this).attr('gridId');
		        ns.common.setFirstGridPage('#'+gridId);
		        $('#'+gridId).trigger("reloadGrid");
			}
	    });

		ns.billpay.reloadHomePendingPaymentsSummaryGrid();
	};
	//Reload single Billpay templates summary jgrid..
	ns.billpay.reloadSingleBillpayTemplatesSummaryGrid = function ( gridId ){
		ns.common.setFirstGridPage('#singleBillpayTempGridId');
		$('#singleBillpayTempGridId').trigger("reloadGrid");
	};

	//Reload multiple Billpay templates summary jgrid..
	ns.billpay.reloadMultipleBillpayTemplatesSummaryGrid = function ( gridId ){
		ns.common.setFirstGridPage('#multiBillpayTempGridId');
		$('#multiBillpayTempGridId').trigger("reloadGrid");
	};

	//Reload single and multiple Billpay templates summary grid.
	ns.billpay.reloadSingleAndMultipleBillpayTemplatesGrid = function (){
		$("#billpayTempGridTabs ul li ").each(function(index){
		    if($(this).hasClass('ui-tabs-active')){
		    	if (index == 0) {
		    		ns.common.setFirstGridPage('#singleBillpayTempGridId');
		    		$('#singleBillpayTempGridId').trigger("reloadGrid");
				} else if (index == 1) {
					ns.common.setFirstGridPage('#multiBillpayTempGridId');
					$('#multiBillpayTempGridId').trigger("reloadGrid");
				}
		    }
		})
	};

	//Reload payee summary grid.
	ns.billpay.reloadBillpayPayeeSummaryGrid = function () {
		ns.common.setFirstGridPage('#billpayPayeeGridId');
		$('#billpayPayeeGridId').trigger("reloadGrid");
	};

	//view Billpay templates.
	ns.billpay.loadBillpayTemplate = function ( billpayID, collectionName ){
		ns.billpay.viewBillpayDetails( billpayID, collectionName );
	};
	//edit Billpay template.
	ns.billpay.editSingleBillpayTemplate = function ( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#inputDiv').html(data);
				ns.billpay.loadBillpayPageForSelectedTemplate();

				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				$("#billpayTempGridTabs").hide();
				ns.common.selectDashboardItem("addSingleTemplateLink");
			}
		});
	};

	//delete Billpay template.
	ns.billpay.deleteSingleBillpayTemplate = function( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteBillpayTempDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.loadSingleBillpayTemplate = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#summary').empty();
				$('#inputDiv').html(data);
				ns.billpay.loadBillpayPageForSelectedTemplate();

				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				$("#billpayTempGridTabs").hide();
				ns.common.refreshDashboard('dbBillPaySummary');
				ns.billpay.isLoadBillPayTemplate = true;

				ns.common.selectDashboardItem("addSingleBillpayLink");
			}
		});
	};

	ns.billpay.loadMultipleBillpayTemplate = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#summary').empty();
				$('#inputDiv').html(data);
				ns.billpay.loadBillpayPageForSelectedTemplate();

				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				$("#billpayTempGridTabs").hide();
				ns.common.refreshDashboard('dbBillPaySummary');
				ns.billpay.isLoadBillPayTemplate = true;
				ns.common.selectDashboardItem("addMultipleBillpayLink");
			}
		});
	};

	//The function is used by above two load template functions : loadSingleBillpayTemplate() and loadMultipleBillpayTemplate().
	ns.billpay.loadBillpayPageForSelectedTemplate = function(){
		//$.log('About to load form! ');
		$('#details').hide();
		$('#quicksearchcriteria').hide();

		ns.billpay.foldPortlet('summary');

		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		 $(".selecteBeneficiaryMessage").hide();
		 $('#expandBillpayPayee').removeClass('hidden');

        ns.billpay.gotoStep(1);
		$('#details').slideDown();
	};

	//edit multiple Billpay template.
	ns.billpay.editMultipleBillpayTemplate = function( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#inputDiv').html(data);
				ns.billpay.loadBillpayPageForSelectedTemplate();
				$('#BillPayFileImportEntryLink').attr('style','display:none');
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				$("#billpayTempGridTabs").hide();
				ns.common.selectDashboardItem("addMultipleTemplateLink");
			}
		});
	};

	//inquire Billpay .
	ns.billpay.inquireTransaction = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#inquiryBillpayDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.viewBillpayDetails = function ( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewBillpayDetailsDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.deleteBillpay = function ( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteBillpayDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.skipBillpay = function ( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#skipBillpayDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.deleteMultipleBillpayTemplate = function (urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteBillpayTempDialogID').html(data).dialog('open');
			}
		});
	};


	ns.billpay.editSingleBillpay = function ( urlString ){
		//destory delete dialog if it is not present
		if(ns.common.isInitialized($("#innerDeleteBillpayDialog"),'ui-dialog')){
			ns.common.destroyDialog('innerDeleteBillpayDialog');
		}

		$.publish("editSingleBillpayLink");
		$.ajax({
			url: urlString,
			success: function(data){
				$.publish("loadBillpayFormComplete");
				$('#inputDiv').html(data);

				// called here to change the header from add to edit when page loads
				ns.billpay.loadBillpayPageForSelectedTemplate();
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				$("#billpayGridTabs").hide();
				ns.common.selectDashboardItem("addSingleBillpayLink");
			}
		});

	};

	ns.billpay.viewBillpayPayee = function ( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewPayeeDetailsDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.editBillpayPayee = function ( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#inputDiv').html(data);
				ns.billpay.loadBillpayPageForSelectedTemplate();

				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				$("#billpayPayeeGridTabs").hide();
				ns.common.selectDashboardItem("addBillpayPayeeLink");
			},
			complete: function(data){
				$('#detailsPortletWizardStatusId').show();
			}
		});
	};

	ns.billpay.deleteBillpayPayee = function (urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#deletePayeeDialogID').html(data).dialog('open');
			}
		});
	};

	ns.billpay.insertBillpayBank = function ( bankID, collectionName){
		ns.common.closeDialog('#searchBankingDialogID');
		$("#payeeRouteBankIdentifier").attr("value",bankID);
		$("#bankIdentifier").attr("value",bankID);
	};


	//Displays an estimated amount indicator
	ns.billpay.displayEstimatedAmountIndicator = function ( gridId ){

		var ids = jQuery(gridId).jqGrid('getDataIDs');
		for(var i = 0; i < ids.length; i ++){
			var id = ids[i];
			var _isAmountEstimated = $(gridId).jqGrid('getCell', id, 'isAmountEstimated');
			var _isToAmountEstimated = $(gridId).jqGrid('getCell', id, 'isToAmountEstimated');
			if(_isAmountEstimated == "true" || _isToAmountEstimated == "true"){
				$('#singleBillpayTemplatesGridID_pager_right').html('&#8776; indicates an estimated amount&nbsp;&nbsp;');
				break;
			}
		}
	};

	// Script to add grid button for showing and hiding memo field

	// Pending Approval - Script to add grid button for showing and hiding memo field
	ns.billpay.AddPendingApprovalShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'pendingApprovalShowMemo',
			caption: js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;
				if(!isMemoAdded){
					ns.billpay.addMemoRows(gridId, gridId+"pendingApprovalMemo");
				}
				var buttonLabel = $("#"+gridId+"pendingApprovalShowMemo"+" div");
				var buttonId = "#"+gridId+"pendingApprovalShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingApprovalMemo]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingApprovalMemo]").show();
				}
			}
		}
		$("#" + gridId + "pendingApprovalShowMemo").remove();
		ns.common.addGridButton("#"+gridId, buttonConfig);
	}

	// Pending - Script to add grid button for showing and hiding memo field
	ns.billpay.AddPendingShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'pendingShowMemo',
			caption:js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;
				if(!isMemoAdded){
					ns.billpay.addMemoRows(gridId, gridId+"pendingMemo");
				}
				var buttonLabel = $("#"+gridId+"pendingShowMemo"+" div");
				var buttonId = "#"+gridId+"pendingShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingMemo]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingMemo]").show();
				}
				ns.common.resizeWidthOfGrids();
			}
		}
		$("#" + gridId + "pendingShowMemo").remove();
		ns.common.addGridButton("#"+gridId, buttonConfig);
	}

	// Pending - Script to add grid button for showing and hiding memo field
	ns.billpay.AddCompletedShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'completedShowMemo',
			caption:js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;
				if(!isMemoAdded){
					ns.billpay.addMemoRows(gridId, gridId+"completedMemo");
				}
				var buttonLabel = $("#"+gridId+"completedShowMemo"+" div");
				var buttonId = "#"+gridId+"completedShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"completedMemo]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"completedMemo]").show();
				}
			}
		}
		$("#" + gridId + "completedShowMemo").remove();
		ns.common.addGridButton("#"+gridId, buttonConfig);
	}

	// Function to add Memo rows
	ns.billpay.addMemoRows = function(gridID, memoRowType) {
		var memoCells = $("td[aria-describedby=" + gridID +"_memo]");
		$.each(memoCells,function(i,memoCell){
			var memo = $(memoCell).html();
			if(memo){
				var parentRow = $(memoCell).parent();
				var className = parentRow.attr("class");
				$(parentRow).after('<tr rowType=\"'+memoRowType+'\"  class=\"' + className + '\"><td colspan="10"><div class="memo"><b>'+js_memoDescr_Start+':</b>' + memo + '</div></td></tr>');
			};
		});

		$("#" + gridID).data("isMemoAdded",true);
	}

	//Formatter of Amount column.
	ns.billpay.formatNumberPayments = function ( cellvalue, options, rowObject ){
		if(cellvalue=='999'){
			return "--";
		}else{
			return cellvalue;
		}
	};

	//Formatter of Transfer Status column
	ns.billpay.formatBillpayStatusColumn = function(cellvalue, options, rowObject){
		if(cellvalue == 13){//PMS_BATCH_INPROCESS
			return js_status_in_process;
		}else if(cellvalue == 12){//PMS_FUNDS_ALLOCATED
			return js_status_in_process;
		}else if(cellvalue == 24){//PMS_BPW_SKIPPED
			return js_status_skip;
		}else{//other status
			return js_status_pending;
		}
	};

	//Function to show multiple template details.
	ns.billpay.viewMultipleBillpayTemplate = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewBillpayMultiTempDialogID').html(data).dialog('open');
			}
		});
	};
