xcopy /y target\tmp\com\sap\banking\commonconfig\config\custom\validations\applications\xml\validations_loanApplicationContext.xml target\tmp\com\sap\banking\commonconfig\config\custom\validations\loanprocess\xml\

xcopy /y target\tmp\com\sap\banking\commonconfig\config\custom\validations\applications\xml\validations_termDeposit.xml target\tmp\com\sap\banking\commonconfig\config\custom\validations\termdeposit\xml\


ECHO Copied termdeposit and loanprocess validation xml 


del target\tmp\com\sap\banking\commonconfig\config\custom\validations\applications\xml\validations_loanApplicationContext.xml
del target\tmp\com\sap\banking\commonconfig\config\custom\validations\applications\xml\validations_termDeposit.xml

ECHO Deleted termdeposit and loanprocess validation xml from applications 
