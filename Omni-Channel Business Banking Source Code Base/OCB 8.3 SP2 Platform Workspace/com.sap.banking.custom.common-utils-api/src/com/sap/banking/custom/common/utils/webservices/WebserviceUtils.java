package com.sap.banking.custom.common.utils.webservices;

import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.handler.Handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ffusion.beans.Balance;
import com.ffusion.beans.DateTime;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.banking.Transaction;
import com.ffusion.beans.banking.Transactions;
import com.ffusion.beans.common.Currency;
import com.ffusion.beans.stoppayments.StopCheck;
import com.ffusion.beans.user.User;
import com.sap.banking.custom.common.utils.accountsutils.AccountTypeManagerUtils;
import com.sap.banking.custom.common.utils.core.CoreUtils;
import com.sap.banking.custom.webservices.customerinquiry.CustomerInquiry;
import com.sap.banking.custom.webservices.customerinquiry.CustomerInquiryResponse;
import com.sap.banking.custom.webservices.customerinquiry.CustomerInquiry_Service;
import com.sap.banking.custom.webservices.customerportfolioinquiry.CustPortInq.OLDMODTYP;
import com.sap.banking.custom.webservices.customerportfolioinquiry.CustPortInqResponse;
import com.sap.banking.custom.webservices.customerportfolioinquiry.CustomerPortfolioInquiry;
import com.sap.banking.custom.webservices.customerportfolioinquiry.CustomerPortfolioInquiry_Service;
import com.sap.banking.custom.webservices.ddsvstopadd.DDSVStopAdd;
import com.sap.banking.custom.webservices.ddsvstopadd.DDSVStopAddResponse.MESSAGE;
import com.sap.banking.custom.webservices.ddsvstopadd.DDSVStopAdd_Service;
import com.sap.banking.custom.webservices.transactioninquiry.TransactionInquiry;
import com.sap.banking.custom.webservices.transactioninquiry.TransactionInquiryResponse;
import com.sap.banking.custom.webservices.transactioninquiry.TransactionInquiryResponse.ResponseData.TRANSACTION;
import com.sap.banking.custom.webservices.transactioninquiry.TransactionInquiry_Service;

public class WebserviceUtils {
	
	private static Logger m_logger = LoggerFactory.getLogger("WebserviceUtils");
	
	public static Object CreateWebservice(String webServiceName, Class<?> serviceClass, String endpointURL, LogMessageHandler logMessageHandler) throws Exception {
		
		Object _result = null;
		
		try{			
			Object _service =  serviceClass.newInstance();
			if(_service != null){
			   Method _method = serviceClass.getMethod("get" + webServiceName + "Port");
			   if(_method != null){
				  Object _servicePort = _method.invoke(_service);
				  if(_servicePort != null){
					  URL _endpointURL = new URL(endpointURL);
					  BindingProvider _bindingProvider = (BindingProvider) _servicePort;
					  Binding _binding = _bindingProvider.getBinding();
					  List<Handler> _handlerChain = _binding.getHandlerChain();
					  _handlerChain.add(logMessageHandler);
					  _binding.setHandlerChain(_handlerChain);
					    
					  _bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,_endpointURL.toString());
					 _result = _servicePort;
				  }
			   }
			}			
		}catch(Exception ex){
		  throw ex;	
		}
     
		return _result;
	}
	
//	public static Accounts processAccounts(String bankId, int directoryId, Accounts accounts , User user) {
	
	
	
	public static Accounts processAccounts(String bankId, int directoryId, Accounts accounts) {	
		Accounts _result = null;
		if(accounts == null){
		   accounts = new Accounts();	
		}
			
		String _customerPortfolioInquieryEndpointURL = System.getProperty("centerstate.customerportfolioinquiry.ws.url");
		
		try {
			LogMessageHandler _logMessageHandler = new LogMessageHandler();
			CustomerPortfolioInquiry _webService = (CustomerPortfolioInquiry)WebserviceUtils.CreateWebservice("CustomerPortfolioInquiry", 
					                                CustomerPortfolioInquiry_Service.class, 
					                                _customerPortfolioInquieryEndpointURL,
					                                _logMessageHandler);
			
			if(_webService != null){
			   Holder<String> _SEQUENCE = new  Holder<String>();
			   Holder<CustPortInqResponse.ResponseData> _holderResonse = new Holder<CustPortInqResponse.ResponseData>();
			   OLDMODTYP _OLDMODTYP = new OLDMODTYP();
			   String _TAXIDNBR = "";
			   String _CUSTKEY = ""; //user.getCustId(); //TODO CUSTKEY customer-accounts-transactions filters
			   String _INTERFACE = "";
			   String _OWNERTYPE = "";
			   String _RETRNNACAP = "";
			   String _SRCHDIRCT = "";
			   String _SRCHAPPLCD = "";
			   String _SRCHACTNBR = "";
			   String _VWUNDPREL = "";
			   String _NBRRECTORT = "";
			   String _OLBUSERID = "";
			   String _DDAVAILBAL = "";
			   String _SVAVAILBAL = "";
			   String _INCLRROD = "";
			   String _APPLCD = "";
			   String _PRODTYPE = "";
			   _webService.custPortInq("VENDORID", _SEQUENCE, _TAXIDNBR, _CUSTKEY, _INTERFACE, _OWNERTYPE, 
					   				   _RETRNNACAP, _SRCHDIRCT, _SRCHAPPLCD, _SRCHACTNBR, _VWUNDPREL, _NBRRECTORT,
					   				   _OLBUSERID, _DDAVAILBAL, _SVAVAILBAL, _INCLRROD, _APPLCD, _PRODTYPE, 
					   				   _OLDMODTYP, _holderResonse);
				    
			    Account _account = null;
			    
			    //Accounts
			    if(_holderResonse.value.getACCOUNT() != null){
			    	int _accountMappingID = AccountTypeManagerUtils.getAccountMappingID(
			    			               _holderResonse.value.getACCOUNT().getAPPLCD(), 
			                               _holderResonse.value.getACCOUNT().getPRODTYPE());
				    
			    	_account = accounts.create(bankId, _holderResonse.value.getACCOUNT().getACTNBR() + "-" + 
			    						       _accountMappingID, _holderResonse.value.getACCOUNT().getACTNBR(), _accountMappingID);
	
					//SET ACCOUNT INFO
					_account.setDirectoryID(directoryId);			  			 
					_account.setNickName(_holderResonse.value.getACCOUNT().getACTDESC());
					_account.setAccountDisplayText(_holderResonse.value.getACCOUNT().getACTDESC());
					//_account.setAccountGroup(1);
					_account.setAccountGroup(_accountMappingID);
					_account.setBankName("Center State");
					_account.setCoreAccount("1");
					_account.setStrippedAccountNumber(_holderResonse.value.getACCOUNT().getACTNBR());					
					_account.setCurrencyCode("USD");
					_account.setPersonalAccount("0");
					_account.setPositivePay("1");
					_account.setPrimaryAccount("1");
					_account.setBicAccount("");
					_account.setContactId(20);
					_account.setShowPreviousDayOpeningLedger("Y");
					_account.setZBAFlag("B");
					_account.setRoutingNum("Routing Num");
					
					//Values
					_account.put("HIDE", "0");			    
					_account.put("REG_RETRIEVAL_DATE", new DateTime(Locale.getDefault()));
					_account.put("REG_DEFAULT", "1");
					_account.put("REG_ENABLED", "1");
					
					//Transactions
//					Transactions _transactions = new Transactions();
//					_account.setTransactions(_transactions);
					
					//BALANCE
					Balance availableAcc = new Balance();					
					Balance closingBalance = new Balance();
					availableAcc.setAmount(_holderResonse.value.getACCOUNT().getAVAILBAL());
					closingBalance.setAmount(_holderResonse.value.getACCOUNT().getCURRBAL());
					_account.setAvailableBalance(availableAcc);
					_account.setCurrentBalance(closingBalance);
					_account.setClosingBalance(closingBalance);
					

					
					
			    }
			    
			    //Savings Accounts
			    if(_holderResonse.value.getDEPACCOUNT() != null){ 
			    	int _accountMappingID = AccountTypeManagerUtils.getAccountMappingID(
			    			                _holderResonse.value.getDEPACCOUNT().getAPPLCD(), 
                                            _holderResonse.value.getDEPACCOUNT().getPRODTYPE());
			    	
			    	_account = accounts.create(bankId, _holderResonse.value.getDEPACCOUNT().getACTNBR() + "-" + 
			    							   _accountMappingID, _holderResonse.value.getDEPACCOUNT().getACTNBR(), 1);
			    	
					//SET ACCOUNT INFO
					_account.setDirectoryID(directoryId);			  			 
					_account.setNickName(_holderResonse.value.getDEPACCOUNT().getACTDESC());
					_account.setAccountDisplayText(_holderResonse.value.getDEPACCOUNT().getACTDESC());
					//_account.setAccountGroup(2);
					_account.setAccountGroup(_accountMappingID);
					_account.setBankName("Center State");
					_account.setCoreAccount("1");
					_account.setStrippedAccountNumber(_holderResonse.value.getDEPACCOUNT().getACTNBR());					
					_account.setCurrencyCode("USD");
					_account.setPersonalAccount("0");
					_account.setPositivePay("1");
					_account.setPrimaryAccount("1");
					_account.setBicAccount("");
					_account.setContactId(20);
					_account.setShowPreviousDayOpeningLedger("Y");
					_account.setZBAFlag("B");
					_account.setRoutingNum("Routing Num");
					
					//Values
					_account.put("HIDE", "0");			    
					_account.put("REG_RETRIEVAL_DATE", new DateTime(Locale.getDefault()));
					_account.put("REG_DEFAULT", "1");
					_account.put("REG_ENABLED", "1");
					
					//Transactions
//					Transactions _transactions = new Transactions();
//					_account.setTransactions(_transactions);
					
					//BALANCE
					Balance availableAcc = new Balance();					
					Balance closingBalance = new Balance();
					availableAcc.setAmount(_holderResonse.value.getDEPACCOUNT().getAVAILBAL());
					closingBalance.setAmount(_holderResonse.value.getDEPACCOUNT().getCURRBAL());
					_account.setAvailableBalance(availableAcc);
					_account.setCurrentBalance(closingBalance);
					_account.setClosingBalance(closingBalance);
					

			    }
			    
			    //Loans Accounts
			    if(_holderResonse.value.getLOANACCOUNT() != null){ //Loans
			    	int _accountMappingID = AccountTypeManagerUtils.getAccountMappingID(
			    			                _holderResonse.value.getLOANACCOUNT().getAPPLCD(), 
                                            _holderResonse.value.getLOANACCOUNT().getPRODTYPE());
			    	
			    	_account = accounts.create(bankId, _holderResonse.value.getLOANACCOUNT().getACTNBR() + 
			    			                   "-" + _accountMappingID, _holderResonse.value.getLOANACCOUNT().getACTNBR(), _accountMappingID);
			    	
					//SET ACCOUNT INFO
					_account.setDirectoryID(directoryId);			  			 
					_account.setNickName(_holderResonse.value.getLOANACCOUNT().getACTDESC());
					_account.setAccountDisplayText(_holderResonse.value.getLOANACCOUNT().getACTDESC());
					//_account.setAccountGroup(4);
					_account.setAccountGroup(_accountMappingID);
					_account.setBankName("Center State");
					_account.setCoreAccount("1");
					_account.setStrippedAccountNumber(_holderResonse.value.getLOANACCOUNT().getACTNBR());					
					_account.setCurrencyCode("USD");
					_account.setPersonalAccount("0");
					_account.setPositivePay("1");
					_account.setPrimaryAccount("1");
					_account.setBicAccount("");
					_account.setContactId(20);
					_account.setShowPreviousDayOpeningLedger("Y");
					_account.setZBAFlag("B");
					_account.setRoutingNum("Routing Num");
					
					//Values
					_account.put("HIDE", "0");			    
					_account.put("REG_RETRIEVAL_DATE", new DateTime(Locale.getDefault()));
					_account.put("REG_DEFAULT", "1");
					_account.put("REG_ENABLED", "1");
					
					//Transactions
//					Transactions _transactions = new Transactions();
//					_account.setTransactions(_transactions);
					
					//BALANCE
					Balance availableAcc = new Balance();					
					Balance closingBalance = new Balance();
					availableAcc.setAmount(_holderResonse.value.getLOANACCOUNT().getAVAILBAL());
					closingBalance.setAmount(_holderResonse.value.getLOANACCOUNT().getCURRBAL());
					_account.setAvailableBalance(availableAcc);
					_account.setCurrentBalance(closingBalance);
					_account.setClosingBalance(closingBalance);
					

			    }
			    
			   _result = accounts;
			   
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
		return _result;
	}

	public static Transactions processTransactions(Account account) {
		
		Transactions _result = null;		
		String _transactionInquiryEndpointURL = System.getProperty("centerstate.transactioninquiry.ws.url");
		
		try {
			LogMessageHandler _logMessageHandler = new LogMessageHandler();
			TransactionInquiry _webService = (TransactionInquiry)CreateWebservice("TransactionInquiry", 
					                          TransactionInquiry_Service.class, 
					                          _transactionInquiryEndpointURL, _logMessageHandler);
			if(_webService != null){
			   String _VENDORID = "";
			   String _APPLCD = "";
			   String _ACTNBR = "";
			   String _DTSELTOPT = "";
			   String _BEGINDATE = "";
			   String _ENDDATE = "";
			   String _FIRSTNEXT = "";
			   String _NBRRECTORT = "";
			   String _SEQOPT = "";
			   String _REQTYPE = "";
			   String _RTNMEMOPST = "";
			   String _RTNPCKGPST = "";
			   Holder<String> _SEQUENCE = new  Holder<String>();
			   Holder<TransactionInquiryResponse.ResponseData> _holderResonse = new Holder<TransactionInquiryResponse.ResponseData>();
			   
			   _webService.transactionInquiry(_VENDORID, _SEQUENCE, _APPLCD, _ACTNBR, _DTSELTOPT, _BEGINDATE, 
				   	                          _ENDDATE, _FIRSTNEXT, _NBRRECTORT, _SEQOPT, _REQTYPE, _RTNMEMOPST, 
						                      _RTNPCKGPST, _holderResonse);
				
			   Transactions _transactions = new Transactions();
			   if(_holderResonse.value != null){
			      List<TRANSACTION> _transactionsResponse = _holderResonse.value.getTRANSACTION();
			      if(_transactionsResponse != null){
			    	 for(TRANSACTION _value:_transactionsResponse){
			    		 Transaction _transaction = _transactions.create();
			    		 SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyyMMdd");
			    		 
			    		 _transaction.setID(_value.getOLBTRANID());
			    		 _transaction.setType(3); //Check
			    		 _transaction.setSubType(406);
			    		 _transaction.setDescription(_value.getOLBTRANID()+"|"+_value.getTRANCDX());
			    		 _transaction.setReferenceNumber(_value.getSEQNBR());
			    		 _transaction.setMemo("");
			    		 _transaction.setDate(new DateTime(_dateFormat.parse(_value.getTRANDATE()), Locale.getDefault()));
			    		 _transaction.setValueDate(new DateTime(_dateFormat.parse(_value.getTRANDATE()), Locale.getDefault()));
			    		 					    			    					    		 			    		
					     _transaction.setPostingDate(_transaction.getROPostingDate());
					     _transaction.setDueDate(_transaction.getRODueDate());
					     _transaction.setProcessingDate(_transaction.getROProcessingDate());					     
					     _transaction.setCategory(1);
					     _transaction.setAmount(new Currency(_value.getTRANAMT(),Locale.getDefault()));
					     _transaction.setRunningBalance(new Currency(_value.getCURRBAL(),Locale.getDefault()));
					     			    		
			    		 _transaction.setTransactionIndex( Integer.valueOf(_value.getTRANCDX()));
			    		 _transaction.setFixedDepositRate(1);
			    		 _transaction.setInstrumentNumber("1");
			    		 _transaction.setInstrumentBankName("InstrumentBankName");
			    		 _transaction.setPayeePayor("PayeePayor");
			    		 _transaction.setPayorNum("PayorNum");
			    		 _transaction.setOrigUser("OrigUser");
			    		 _transaction.setPONum("PONum");
			    		 _transaction.setImmediateAvailAmount(new Currency("999999999",Locale.getDefault()));
			    		 _transaction.setOneDayAvailAmount(new Currency("999999999",Locale.getDefault()));
			    		 _transaction.setMoreThanOneDayAvailAmount(new Currency("999999999",Locale.getDefault()));
			    		 _transaction.setBankReferenceNumber(_value.getORIGSEQNBR());
			    		 _transaction.setCustomerReferenceNumber("CustomerReferenceNumber");
			    		 _transaction.setDataSourceLoadTime(_transaction.getRODataSourceLoadTime());
			    		 _transaction.setDataClassification("DataClassification");
			    		 _transaction.setAccount(account);
			    		 
			    		 Map<String, String> m = new HashMap<String, String>();
			    		 m.put("accIndex", _value.getTRANCDX());
			    		 _transaction.setMap(m);
			    	 }
			      }
			   }
			   _result = _transactions;
			   //account.setTransactionList(_result);
			   //account.setTransactions(_result); //TODO Check the transactionList for each Account
			   
			}
		} catch (Exception e) {			
			e.printStackTrace();
		}	
		
		return _result;
	}

	public static User processCustomer() {
		
		User _result = new User();		
		String _transactionInquiryEndpointURL = System.getProperty("centerstate.customerinquiry.ws.url");
		
		try {
			LogMessageHandler _logMessageHandler = new LogMessageHandler();
			CustomerInquiry _webService = (CustomerInquiry)CreateWebservice("CustomerInquiry", 
										   CustomerInquiry_Service.class, 
					                       _transactionInquiryEndpointURL, _logMessageHandler);
			if(_webService != null){
			   Holder<CustomerInquiryResponse.ResponseData> _holderResonse = new Holder<CustomerInquiryResponse.ResponseData>();
			   Holder<String> _SEQUENCE = new  Holder<String>();
			   String _VENDORID = "";
			   String _CUSTKEY = "";
			   String _RETRNNACAP = "";
			   _webService.customerInquiry(_VENDORID, _SEQUENCE, _CUSTKEY, _RETRNNACAP, _holderResonse);
				
			   if(_holderResonse.value != null){
				  _result.setFirstName(_holderResonse.value.getFIRSTNAME());
				  _result.setLastName(_holderResonse.value.getLASTNAME());
				  _result.setStreet(_holderResonse.value.getNALINE1() + " " + _holderResonse.value.getNALINE2());
				  _result.setStreet2(_holderResonse.value.getNALINE3() + " " + _holderResonse.value.getNALINE4());
				  _result.setCustId(_holderResonse.value.getCUSTKEY());
					
				  //Set Core User
				  CoreUtils.setCoreUser(_result);
			   }
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return _result;
	}	

	public static boolean processStopPayments(StopCheck stopCheck) {
		
		final String _thisMethod = "WebserviceUtils.processStopPayments";
		String _stopPaymentsEndpointURL = System.getProperty("centerstate.stoppayments.ws.url");
		boolean _result = false;
		
		try {
			LogMessageHandler _logMessageHandler = new LogMessageHandler();
			DDSVStopAdd _webService = (DDSVStopAdd)CreateWebservice("DDSVStopAdd", DDSVStopAdd_Service.class, 
									   _stopPaymentsEndpointURL, _logMessageHandler);
			
			if(_webService != null){
			   Holder<MESSAGE> _MESSAGE = new Holder<>();
			   Holder<String> _SEQUENCE =  new Holder<>();
			   String _VENDORID = "";
			   String _APPLCD = "";
			   String _ACTNBR = stopCheck.getAccountID();
			   String _TRANCD = "";
			   String _AMOUNT = stopCheck.getAmount();
			   String _EXPDATE = "";
			   String _CHECKNBR = stopCheck.getCheckNumbers();
			   String _CHECKDATE = stopCheck.getCheckDate();
			   String _PAYEEDESC = stopCheck.getPayeeName();
			   String _AMTLIMIT = "";
			   String _LOWCHKRNG = "";
			   String _HGHCHKRNG = "";
			   String _COMPANYID = "";
			   String _NBRFLTDAYS = "";
			   String _USERID = CoreUtils.getCoreUser().getCustId();
			   String _ORUSERID = "";
			   String _WRKSTNID = "";
			   String _MODECODE = "";
			   String _DLTMCHCD = "";
			   String _REQTYPE = "";
			   String _CHGPYMTFEE = "";
			   String _CHGPYMTAMT = "";
			   String _INDIVID = "";
			   String _RETRNRESN = stopCheck.getReason();
			   String _DTDEATH = "";
			   String _ELEMENTSRC = "";
			   String _SUBSRC = "";
			   String _OVRDFLG = "";
			   String _STPHLDTYP = "";
				
			   _webService.ddsvStopAdd(_VENDORID, _SEQUENCE, _APPLCD, _ACTNBR, _TRANCD, _AMOUNT, _EXPDATE, _CHECKNBR, _CHECKDATE, _PAYEEDESC, _AMTLIMIT, 
						               _LOWCHKRNG, _HGHCHKRNG, _COMPANYID, _NBRFLTDAYS, _USERID, _ORUSERID, _WRKSTNID, _MODECODE, _DLTMCHCD, _REQTYPE,
						               _CHGPYMTFEE, _CHGPYMTAMT, _INDIVID, _RETRNRESN, _DTDEATH, _ELEMENTSRC, _SUBSRC, _OVRDFLG, _STPHLDTYP, _MESSAGE);
			   
			   
			   if(_MESSAGE.value != null){
				   
				   String _message = _MESSAGE.value.getMESSAGETEXT();
				   m_logger.debug(_message);
				   //m_logger.error("NOT ERROR: " + _thisMethod + " : " + _message);
				   
				   System.out.println("<MESSAGE TEXT> " + _message);
				   System.out.println("<ACCOUNT NUMBER> " + stopCheck.getAccountID());
				   System.out.println("<AMOUNT> " + stopCheck.getAmount());
				   System.out.println("<CHECK NUMBERS> " + stopCheck.getCheckNumbers());
				   System.out.println("<DATE> " + stopCheck.getCheckDate());
				   System.out.println("<PAYEE NAME> " + stopCheck.getPayeeName());
				   System.out.println("<REASON> " + stopCheck.getReason());
				   
				  
				  if ("completed successfully".equals(_message.toLowerCase())){
					  _result = true;
				  }
			   }
			}
		
		}catch(Exception ex){
			
		}
		
		return _result;
	}
}
