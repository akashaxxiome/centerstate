package com.sap.banking.custom.common.utils.webservices;

import java.io.Serializable;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


public class CustomerInquiryAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2263017053101256647L;
	private String m_xmlRaw = null;
	private Document m_xmlDocument = null;
	private final String m_type = "ACCOUNT";
	
	public CustomerInquiryAccount(String xmlDocument) {
		m_xmlRaw = xmlDocument;
		
		try{
			DocumentBuilderFactory _factory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder _builder = _factory.newDocumentBuilder();
		    InputSource _is = new InputSource(new StringReader(m_xmlRaw));		    
		    m_xmlDocument = _builder.parse(_is);
		    
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public String getXmlRaw() {
		return m_xmlRaw;
	}
	
	public String getSEQUENCE() {
		final String _type = "SEQUENCE";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public String getTOTALACCTS() {
		final String _type = "TOTALACCTS";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public String getPENDINGCNT() {
		final String _type = "PENDINGCNT";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public String getREADCNT() {
		final String _type = "READCNT";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
	}
	
	public String getBRDCASTCNT() {
		final String _type = "BRDCASTCNT";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public String getRECSRETURNED() {
		final String _type = "RECSRETURNED";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public String getMOREDATA() {
		final String _type = "MOREDATA";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public Object getUNDPRELEXT() {
		final String _type = "UNDPRELEXT";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
	
	public String getGROUPCD() {
		final String _type = "GROUPCD";
		String _result = "";
		
		if(m_xmlDocument != null){
			NodeList _node = m_xmlDocument.getElementsByTagName(_type);
			if(_node != null && _node.getLength() > 0){
		       _result = _node.item(0).getTextContent();
			}
		}
		
		return _result;
    }
}
