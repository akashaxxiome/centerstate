package com.sap.banking.custom.common.utils.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ffusion.beans.user.BusinessEmployee;
import com.ffusion.beans.user.User;

public class CoreUtils {

	private static User m_user = null;
	private static BusinessEmployee m_employee = null;
	private static Logger m_logger = LoggerFactory.getLogger("CoreUtils");
	
	public static User getCoreUser() {
		return m_user;
	}
	
	public static void setCoreUser(User user) {
		m_user = user;
	}
	
	public static BusinessEmployee getCoreBusinessEmployee() {
		return m_employee;
	}
	
	public static void setBusinessEmployee(BusinessEmployee employee) {
		m_employee = employee;
	}
}
