package com.sap.banking.custom.common.utils.accountsutils;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.ffusion.beans.accounts.Account;


public class AccountTypeManagerUtils {

	private static Logger m_logger = LoggerFactory.getLogger(AccountTypeManagerUtils.class);
	private static Document m_xmlDocument = null;
	
	public static int getAccountMappingID(String applicationCode, String productType) {
		
		final String _thisMethod = "AccountTypeUtilsImpl.getAccountMappingCode";
		int _result = 15;
		
		try{
			if(applicationCode != null && productType != null && m_xmlDocument != null){			  
			   XPathFactory _xPathfactory = XPathFactory.newInstance();
			   XPath _xpath = _xPathfactory.newXPath();
			   String _xpathExpression = "//accounttype[@productcode='PRODTYPE'][@applicationcode='APPLCD']/@accounttypemapping";
			   _xpathExpression = _xpathExpression.replace("APPLCD", applicationCode);
			   _xpathExpression = _xpathExpression.replace("PRODTYPE", productType);
			   XPathExpression _expr = _xpath.compile(_xpathExpression);
			   String _value = (String)_expr.evaluate(m_xmlDocument, XPathConstants.STRING);
			   if(_value != null && !"".equals(_value.trim())){
				  _result = Integer.valueOf(_value);
			   }
			}
		}catch(Exception ex){
			m_logger.error(_thisMethod + " " + ex.getMessage());
		}
		return _result;
	}
	
	public static boolean isDespositAccount(String applicationCode, String productType) {
		
		final String _thisMethod = "AccountTypeUtilsImpl.isDespositAccount";
		boolean _result = false;
		
		int _value = getAccountMappingID(applicationCode, productType);
		if(_value == 1 || _value == 2 || _value == 14 || _value == 15){
		   _result = true;	
		}
		return _result;
	}
	
	public static boolean isLoanAccount(String applicationCode, String productType) {
		
		final String _thisMethod = "AccountTypeUtilsImpl.isLoanAccount";
		boolean _result = false;
		
		int _value = getAccountMappingID(applicationCode, productType);
		if(_value == 4 || _value == 5 || _value == 7 || _value == 13){
		   _result = true;	
		}
		return _result;
	}

	public static boolean isAssetAccount(String applicationCode, String productType) {
	
		final String _thisMethod = "AccountTypeUtilsImpl.isAssetAccount";
		boolean _result = false;
		
		int _value = getAccountMappingID(applicationCode, productType);
		if(_value == 12){
		   _result = true;	
		}
		return _result;
	}

	public static boolean isDespositAccount(Account account) {
		
		final String _thisMethod = "AccountTypeUtilsImpl.isDespositAccount";
		boolean _result = false;
		
		int _value = account.getAccountGroup();
		if(_value == 1 || _value == 2 || _value == 14 || _value == 15){
		   _result = true;	
		}
		return _result;
	}
	
	public static boolean isLoanAccount(Account account) {
		
		final String _thisMethod = "AccountTypeUtilsImpl.isLoanAccount";
		boolean _result = false;
		
		int _value = account.getAccountGroup();
		if(_value == 4 || _value == 5 || _value == 7 || _value == 13){
		   _result = true;	
		}
		return _result;
	}

	public static boolean isAssetAccount(Account account) {
	
		final String _thisMethod = "AccountTypeUtilsImpl.isAssetAccount";
		boolean _result = false;
		
		int _value = account.getAccountGroup();
		if(_value == 12){
		   _result = true;	
		}
		return _result;
	}
	
	public static List<Account> filterDespositAccounts(List<Account> accounts) {
		
		List<Account> _result = new ArrayList();
		
		if(accounts != null){
		   for(Account _account:accounts){
			   if(isDespositAccount(_account)){
				  _result.add(_account); 
			   }
		   }
		}
		return _result;
	}
	
	public static List<Account> filterLoanAccounts(List<Account> accounts) {
		
		List<Account> _result = new ArrayList();
		
		if(accounts != null){
		   for(Account _account:accounts){
			   if(isLoanAccount(_account)){
				  _result.add(_account); 
			   }
		   }
		}
		return _result;
	}

	public static List<Account> filterAssetAccounts(List<Account> accounts) {
		
		List<Account> _result = new ArrayList();
		
		if(accounts != null){
		   for(Account _account:accounts){
			   if(isAssetAccount(_account)){
				  _result.add(_account); 
			   }
		   }
		}
		return _result;
	}

	public static void setXmlDocument(Document document){
		m_xmlDocument = document;
	}
}
