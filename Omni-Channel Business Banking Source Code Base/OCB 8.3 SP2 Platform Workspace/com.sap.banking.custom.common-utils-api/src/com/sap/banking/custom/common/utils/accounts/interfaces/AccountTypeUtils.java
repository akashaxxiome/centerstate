package com.sap.banking.custom.common.utils.accounts.interfaces;

public abstract interface AccountTypeUtils {

	public abstract void loadAccountsTypes();
}
