package com.sap.banking.custom.common.utils.webservices;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.Collections;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class LogMessageHandler implements SOAPHandler<SOAPMessageContext> {

	private String m_xmlResponse = null;
	
	@Override
	public Set<QName> getHeaders() {
	    return Collections.emptySet();
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
	    SOAPMessage msg = context.getMessage();
	    try {
//	        msg.writeTo(System.out);
	        ByteArrayOutputStream _stream = new ByteArrayOutputStream();
	        msg.writeTo(_stream);
	        m_xmlResponse = new String(_stream.toByteArray(), "utf-8");	        
	    } catch (Exception ex) {	        
	    	ex.printStackTrace();
	    } 
	    return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
	    return true;
	}

	@Override
	public void close(MessageContext context) {
	}
	
	public String getXmlResponse() {
		return m_xmlResponse;
	}
	
	public Document getXmlDocumentResponse() {
		
		Document _xmlDocument = null;
		
		try{
			DocumentBuilderFactory _factory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder _builder = _factory.newDocumentBuilder();
		    InputSource _is = new InputSource(new StringReader(m_xmlResponse));		    
		    _xmlDocument = _builder.parse(_is);		    
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return _xmlDocument;
	}
}
