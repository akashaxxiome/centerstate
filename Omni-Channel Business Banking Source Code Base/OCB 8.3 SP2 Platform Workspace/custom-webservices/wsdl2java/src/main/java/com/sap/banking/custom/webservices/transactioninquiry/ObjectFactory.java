
package com.sap.banking.custom.webservices.transactioninquiry;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sap.banking.custom.webservices.transactioninquiry package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sap.banking.custom.webservices.transactioninquiry
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionInquiryResponse }
     * 
     */
    public TransactionInquiryResponse createTransactionInquiryResponse() {
        return new TransactionInquiryResponse();
    }

    /**
     * Create an instance of {@link TransactionInquiryResponse.ResponseData }
     * 
     */
    public TransactionInquiryResponse.ResponseData createTransactionInquiryResponseResponseData() {
        return new TransactionInquiryResponse.ResponseData();
    }

    /**
     * Create an instance of {@link TransactionInquiry_Type }
     * 
     */
    public TransactionInquiry_Type createTransactionInquiry_Type() {
        return new TransactionInquiry_Type();
    }

    /**
     * Create an instance of {@link TransactionInquiryResponse.ResponseData.TRANSACTION }
     * 
     */
    public TransactionInquiryResponse.ResponseData.TRANSACTION createTransactionInquiryResponseResponseDataTRANSACTION() {
        return new TransactionInquiryResponse.ResponseData.TRANSACTION();
    }

}
