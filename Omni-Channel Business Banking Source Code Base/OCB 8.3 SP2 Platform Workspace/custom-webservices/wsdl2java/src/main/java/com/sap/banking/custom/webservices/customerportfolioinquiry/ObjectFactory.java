
package com.sap.banking.custom.webservices.customerportfolioinquiry;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sap.banking.custom.webservices.customerportfolioinquiry package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sap.banking.custom.webservices.customerportfolioinquiry
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustPortInq }
     * 
     */
    public CustPortInq createCustPortInq() {
        return new CustPortInq();
    }

    /**
     * Create an instance of {@link CustPortInqResponse }
     * 
     */
    public CustPortInqResponse createCustPortInqResponse() {
        return new CustPortInqResponse();
    }

    /**
     * Create an instance of {@link CustPortInqResponse.ResponseData }
     * 
     */
    public CustPortInqResponse.ResponseData createCustPortInqResponseResponseData() {
        return new CustPortInqResponse.ResponseData();
    }

    /**
     * Create an instance of {@link CustPortInq.OLDMODTYP }
     * 
     */
    public CustPortInq.OLDMODTYP createCustPortInqOLDMODTYP() {
        return new CustPortInq.OLDMODTYP();
    }

    /**
     * Create an instance of {@link CustPortInqResponse.ResponseData.ACCOUNT }
     * 
     */
    public CustPortInqResponse.ResponseData.ACCOUNT createCustPortInqResponseResponseDataACCOUNT() {
        return new CustPortInqResponse.ResponseData.ACCOUNT();
    }

    /**
     * Create an instance of {@link CustPortInqResponse.ResponseData.TIMEACCOUNT }
     * 
     */
    public CustPortInqResponse.ResponseData.TIMEACCOUNT createCustPortInqResponseResponseDataTIMEACCOUNT() {
        return new CustPortInqResponse.ResponseData.TIMEACCOUNT();
    }

    /**
     * Create an instance of {@link CustPortInqResponse.ResponseData.DEPACCOUNT }
     * 
     */
    public CustPortInqResponse.ResponseData.DEPACCOUNT createCustPortInqResponseResponseDataDEPACCOUNT() {
        return new CustPortInqResponse.ResponseData.DEPACCOUNT();
    }

    /**
     * Create an instance of {@link CustPortInqResponse.ResponseData.LOANACCOUNT }
     * 
     */
    public CustPortInqResponse.ResponseData.LOANACCOUNT createCustPortInqResponseResponseDataLOANACCOUNT() {
        return new CustPortInqResponse.ResponseData.LOANACCOUNT();
    }

    /**
     * Create an instance of {@link CustPortInqResponse.ResponseData.MTGACCOUNT }
     * 
     */
    public CustPortInqResponse.ResponseData.MTGACCOUNT createCustPortInqResponseResponseDataMTGACCOUNT() {
        return new CustPortInqResponse.ResponseData.MTGACCOUNT();
    }

}
