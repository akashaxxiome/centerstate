
package com.sap.banking.custom.webservices.customerportfolioinquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VENDORID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TAXIDNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTKEY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INTERFACE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RETRNNACAP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SRCHDIRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SRCHAPPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SRCHACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VWUNDPREL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NBRRECTORT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OLBUSERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DDAVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SVAVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INCLRROD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OLDMODTYP">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MODTYP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MODTYPDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorid",
    "sequence",
    "taxidnbr",
    "custkey",
    "_interface",
    "ownertype",
    "retrnnacap",
    "srchdirct",
    "srchapplcd",
    "srchactnbr",
    "vwundprel",
    "nbrrectort",
    "olbuserid",
    "ddavailbal",
    "svavailbal",
    "inclrrod",
    "applcd",
    "prodtype",
    "oldmodtyp"
})
@XmlRootElement(name = "CustPortInq")
public class CustPortInq {

    @XmlElement(name = "VENDORID", required = true)
    protected String vendorid;
    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "TAXIDNBR", required = true)
    protected String taxidnbr;
    @XmlElement(name = "CUSTKEY", required = true)
    protected String custkey;
    @XmlElement(name = "INTERFACE", required = true)
    protected String _interface;
    @XmlElement(name = "OWNERTYPE", required = true)
    protected String ownertype;
    @XmlElement(name = "RETRNNACAP", required = true)
    protected String retrnnacap;
    @XmlElement(name = "SRCHDIRCT", required = true)
    protected String srchdirct;
    @XmlElement(name = "SRCHAPPLCD", required = true)
    protected String srchapplcd;
    @XmlElement(name = "SRCHACTNBR", required = true)
    protected String srchactnbr;
    @XmlElement(name = "VWUNDPREL", required = true)
    protected String vwundprel;
    @XmlElement(name = "NBRRECTORT", required = true)
    protected String nbrrectort;
    @XmlElement(name = "OLBUSERID", required = true)
    protected String olbuserid;
    @XmlElement(name = "DDAVAILBAL", required = true)
    protected String ddavailbal;
    @XmlElement(name = "SVAVAILBAL", required = true)
    protected String svavailbal;
    @XmlElement(name = "INCLRROD", required = true)
    protected String inclrrod;
    @XmlElement(name = "APPLCD", required = true)
    protected String applcd;
    @XmlElement(name = "PRODTYPE", required = true)
    protected String prodtype;
    @XmlElement(name = "OLDMODTYP", required = true)
    protected CustPortInq.OLDMODTYP oldmodtyp;

    /**
     * Gets the value of the vendorid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVENDORID() {
        return vendorid;
    }

    /**
     * Sets the value of the vendorid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVENDORID(String value) {
        this.vendorid = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the taxidnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXIDNBR() {
        return taxidnbr;
    }

    /**
     * Sets the value of the taxidnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXIDNBR(String value) {
        this.taxidnbr = value;
    }

    /**
     * Gets the value of the custkey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTKEY() {
        return custkey;
    }

    /**
     * Sets the value of the custkey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTKEY(String value) {
        this.custkey = value;
    }

    /**
     * Gets the value of the interface property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERFACE() {
        return _interface;
    }

    /**
     * Sets the value of the interface property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERFACE(String value) {
        this._interface = value;
    }

    /**
     * Gets the value of the ownertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOWNERTYPE() {
        return ownertype;
    }

    /**
     * Sets the value of the ownertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOWNERTYPE(String value) {
        this.ownertype = value;
    }

    /**
     * Gets the value of the retrnnacap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRETRNNACAP() {
        return retrnnacap;
    }

    /**
     * Sets the value of the retrnnacap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRETRNNACAP(String value) {
        this.retrnnacap = value;
    }

    /**
     * Gets the value of the srchdirct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCHDIRCT() {
        return srchdirct;
    }

    /**
     * Sets the value of the srchdirct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCHDIRCT(String value) {
        this.srchdirct = value;
    }

    /**
     * Gets the value of the srchapplcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCHAPPLCD() {
        return srchapplcd;
    }

    /**
     * Sets the value of the srchapplcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCHAPPLCD(String value) {
        this.srchapplcd = value;
    }

    /**
     * Gets the value of the srchactnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCHACTNBR() {
        return srchactnbr;
    }

    /**
     * Sets the value of the srchactnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCHACTNBR(String value) {
        this.srchactnbr = value;
    }

    /**
     * Gets the value of the vwundprel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVWUNDPREL() {
        return vwundprel;
    }

    /**
     * Sets the value of the vwundprel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVWUNDPREL(String value) {
        this.vwundprel = value;
    }

    /**
     * Gets the value of the nbrrectort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNBRRECTORT() {
        return nbrrectort;
    }

    /**
     * Sets the value of the nbrrectort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNBRRECTORT(String value) {
        this.nbrrectort = value;
    }

    /**
     * Gets the value of the olbuserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOLBUSERID() {
        return olbuserid;
    }

    /**
     * Sets the value of the olbuserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOLBUSERID(String value) {
        this.olbuserid = value;
    }

    /**
     * Gets the value of the ddavailbal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDDAVAILBAL() {
        return ddavailbal;
    }

    /**
     * Sets the value of the ddavailbal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDDAVAILBAL(String value) {
        this.ddavailbal = value;
    }

    /**
     * Gets the value of the svavailbal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSVAVAILBAL() {
        return svavailbal;
    }

    /**
     * Sets the value of the svavailbal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSVAVAILBAL(String value) {
        this.svavailbal = value;
    }

    /**
     * Gets the value of the inclrrod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINCLRROD() {
        return inclrrod;
    }

    /**
     * Sets the value of the inclrrod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINCLRROD(String value) {
        this.inclrrod = value;
    }

    /**
     * Gets the value of the applcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPPLCD() {
        return applcd;
    }

    /**
     * Sets the value of the applcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPPLCD(String value) {
        this.applcd = value;
    }

    /**
     * Gets the value of the prodtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODTYPE() {
        return prodtype;
    }

    /**
     * Sets the value of the prodtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODTYPE(String value) {
        this.prodtype = value;
    }

    /**
     * Gets the value of the oldmodtyp property.
     * 
     * @return
     *     possible object is
     *     {@link CustPortInq.OLDMODTYP }
     *     
     */
    public CustPortInq.OLDMODTYP getOLDMODTYP() {
        return oldmodtyp;
    }

    /**
     * Sets the value of the oldmodtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustPortInq.OLDMODTYP }
     *     
     */
    public void setOLDMODTYP(CustPortInq.OLDMODTYP value) {
        this.oldmodtyp = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MODTYP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MODTYPDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "modtyp",
        "modtypdesc"
    })
    public static class OLDMODTYP {

        @XmlElement(name = "MODTYP", required = true)
        protected String modtyp;
        @XmlElement(name = "MODTYPDESC", required = true)
        protected String modtypdesc;

        /**
         * Gets the value of the modtyp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMODTYP() {
            return modtyp;
        }

        /**
         * Sets the value of the modtyp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMODTYP(String value) {
            this.modtyp = value;
        }

        /**
         * Gets the value of the modtypdesc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMODTYPDESC() {
            return modtypdesc;
        }

        /**
         * Sets the value of the modtypdesc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMODTYPDESC(String value) {
            this.modtypdesc = value;
        }

    }

}
