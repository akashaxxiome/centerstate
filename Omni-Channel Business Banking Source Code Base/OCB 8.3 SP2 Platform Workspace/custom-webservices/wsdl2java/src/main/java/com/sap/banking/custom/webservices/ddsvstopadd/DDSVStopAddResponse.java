
package com.sap.banking.custom.webservices.ddsvstopadd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MESSAGE">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MESSAGEID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MESSAGETYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MESSAGETEXT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sequence",
    "message"
})
@XmlRootElement(name = "DDSVStopAddResponse")
public class DDSVStopAddResponse {

    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "MESSAGE", required = true)
    protected DDSVStopAddResponse.MESSAGE message;

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link DDSVStopAddResponse.MESSAGE }
     *     
     */
    public DDSVStopAddResponse.MESSAGE getMESSAGE() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link DDSVStopAddResponse.MESSAGE }
     *     
     */
    public void setMESSAGE(DDSVStopAddResponse.MESSAGE value) {
        this.message = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MESSAGEID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MESSAGETYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MESSAGETEXT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "messageid",
        "messagetype",
        "messagetext"
    })
    public static class MESSAGE {

        @XmlElement(name = "MESSAGEID", required = true)
        protected String messageid;
        @XmlElement(name = "MESSAGETYPE", required = true)
        protected String messagetype;
        @XmlElement(name = "MESSAGETEXT", required = true)
        protected String messagetext;

        /**
         * Gets the value of the messageid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGEID() {
            return messageid;
        }

        /**
         * Sets the value of the messageid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGEID(String value) {
            this.messageid = value;
        }

        /**
         * Gets the value of the messagetype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGETYPE() {
            return messagetype;
        }

        /**
         * Sets the value of the messagetype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGETYPE(String value) {
            this.messagetype = value;
        }

        /**
         * Gets the value of the messagetext property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGETEXT() {
            return messagetext;
        }

        /**
         * Sets the value of the messagetext property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGETEXT(String value) {
            this.messagetext = value;
        }

    }

}
