
package com.sap.banking.custom.webservices.customerportfolioinquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TOTALACCTS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PENDINGCNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="READCNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BRDCASTCNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ACCOUNT">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BRNNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT03" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT05" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT07" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE03" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE05" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE07" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC02" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TIMEACCOUNT">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTINTPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TOTHOLDS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ANTICPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT05" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT07" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NXTINTPYM1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTINTPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTRNWLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE07" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NXTINTPYM2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PLANNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PLANTYPE" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DEPACCOUNT">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTINTAMTP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HOLDAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CHKDEPTDY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TBNKFLTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TCUSTFLTAM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CRLMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSCURBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="COLLBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMTLSTDEP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CTFAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RUNCURBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTODDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVBGSTMDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVEDSTMDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVBGSTMDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVEDSTMDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVBGSTMDT3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVEDSTMDT3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PVBGSTMDT4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PASSBOOKCD" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="STMTCYCCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ENCOPTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ENCOPTNTC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="LNCRLIMIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DYNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDSVAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ODLIMITBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="LOANACCOUNT">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SYSTEMTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TOTTRNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURESCRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PYMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPERDIEM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORGLNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CALCLNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ESCROWAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LATECHGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PASTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CALCAMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="UNCLTCGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NXTDUEDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTPYMTDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTBILLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTRNWLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NXTDUEDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTPROCESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MSTACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRCHSYSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DYNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDSVAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ODLIMITBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="MTGACCOUNT">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SYSTEMTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BORHSTPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ESCROWBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="YTDTAXPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PREVYRTXPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="YTDESCINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PIPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="REMESCAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LATECHGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TOTPSTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TOTAMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="UNCLTCGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NXTSCHPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTPYMTDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LSTBILLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE05" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURPYMTDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURPYMTDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC02" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="ORGPRINBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RECS_RETURNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MOREDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sequence",
    "responseData"
})
@XmlRootElement(name = "CustPortInqResponse")
public class CustPortInqResponse {

    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "ResponseData", required = true)
    protected CustPortInqResponse.ResponseData responseData;

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CustPortInqResponse.ResponseData }
     *     
     */
    public CustPortInqResponse.ResponseData getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustPortInqResponse.ResponseData }
     *     
     */
    public void setResponseData(CustPortInqResponse.ResponseData value) {
        this.responseData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TOTALACCTS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PENDINGCNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="READCNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BRDCASTCNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ACCOUNT">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BRNNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT02" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT03" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT05" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT06" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT07" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT08" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT11" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE02" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE03" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE05" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE06" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE07" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC02" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TIMEACCOUNT">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTINTPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TOTHOLDS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ANTICPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT05" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT06" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT07" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT08" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT11" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NXTINTPYM1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTINTPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTRNWLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE06" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE07" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NXTINTPYM2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PLANNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PLANTYPE" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DEPACCOUNT">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTINTAMTP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOLDAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CHKDEPTDY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TBNKFLTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TCUSTFLTAM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CRLMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSCURBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="COLLBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMTLSTDEP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CTFAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RUNCURBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTODDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVBGSTMDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVEDSTMDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVBGSTMDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVEDSTMDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVBGSTMDT3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVEDSTMDT3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PVBGSTMDT4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PASSBOOKCD" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="STMTCYCCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ENCOPTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ENCOPTNTC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="LNCRLIMIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DYNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDSVAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ODLIMITBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="LOANACCOUNT">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SYSTEMTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TOTTRNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURESCRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PYMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPERDIEM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORGLNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CALCLNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ESCROWAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LATECHGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PASTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CALCAMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="UNCLTCGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NXTDUEDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTPYMTDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTBILLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTRNWLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NXTDUEDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTPROCESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MSTACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRCHSYSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DYNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDSVAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ODLIMITBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="MTGACCOUNT">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SYSTEMTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BORHSTPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ESCROWBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="YTDTAXPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PREVYRTXPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="YTDESCINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PIPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REMESCAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LATECHGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TOTPSTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TOTAMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="UNCLTCGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NXTSCHPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTPYMTDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LSTBILLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE05" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURPYMTDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURPYMTDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC02" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ORGPRINBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RECS_RETURNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MOREDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "totalaccts",
        "pendingcnt",
        "readcnt",
        "brdcastcnt",
        "account",
        "timeaccount",
        "depaccount",
        "loanaccount",
        "mtgaccount",
        "recsreturned",
        "moredata"
    })
    public static class ResponseData {

        @XmlElement(name = "TOTALACCTS", required = true)
        protected String totalaccts;
        @XmlElement(name = "PENDINGCNT", required = true)
        protected String pendingcnt;
        @XmlElement(name = "READCNT", required = true)
        protected String readcnt;
        @XmlElement(name = "BRDCASTCNT", required = true)
        protected String brdcastcnt;
        @XmlElement(name = "ACCOUNT", required = true)
        protected CustPortInqResponse.ResponseData.ACCOUNT account;
        @XmlElement(name = "TIMEACCOUNT", required = true)
        protected CustPortInqResponse.ResponseData.TIMEACCOUNT timeaccount;
        @XmlElement(name = "DEPACCOUNT", required = true)
        protected CustPortInqResponse.ResponseData.DEPACCOUNT depaccount;
        @XmlElement(name = "LOANACCOUNT", required = true)
        protected CustPortInqResponse.ResponseData.LOANACCOUNT loanaccount;
        @XmlElement(name = "MTGACCOUNT", required = true)
        protected CustPortInqResponse.ResponseData.MTGACCOUNT mtgaccount;
        @XmlElement(name = "RECS_RETURNED", required = true)
        protected String recsreturned;
        @XmlElement(name = "MOREDATA", required = true)
        protected String moredata;

        /**
         * Gets the value of the totalaccts property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOTALACCTS() {
            return totalaccts;
        }

        /**
         * Sets the value of the totalaccts property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOTALACCTS(String value) {
            this.totalaccts = value;
        }

        /**
         * Gets the value of the pendingcnt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPENDINGCNT() {
            return pendingcnt;
        }

        /**
         * Sets the value of the pendingcnt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPENDINGCNT(String value) {
            this.pendingcnt = value;
        }

        /**
         * Gets the value of the readcnt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getREADCNT() {
            return readcnt;
        }

        /**
         * Sets the value of the readcnt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setREADCNT(String value) {
            this.readcnt = value;
        }

        /**
         * Gets the value of the brdcastcnt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBRDCASTCNT() {
            return brdcastcnt;
        }

        /**
         * Sets the value of the brdcastcnt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBRDCASTCNT(String value) {
            this.brdcastcnt = value;
        }

        /**
         * Gets the value of the account property.
         * 
         * @return
         *     possible object is
         *     {@link CustPortInqResponse.ResponseData.ACCOUNT }
         *     
         */
        public CustPortInqResponse.ResponseData.ACCOUNT getACCOUNT() {
            return account;
        }

        /**
         * Sets the value of the account property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustPortInqResponse.ResponseData.ACCOUNT }
         *     
         */
        public void setACCOUNT(CustPortInqResponse.ResponseData.ACCOUNT value) {
            this.account = value;
        }

        /**
         * Gets the value of the timeaccount property.
         * 
         * @return
         *     possible object is
         *     {@link CustPortInqResponse.ResponseData.TIMEACCOUNT }
         *     
         */
        public CustPortInqResponse.ResponseData.TIMEACCOUNT getTIMEACCOUNT() {
            return timeaccount;
        }

        /**
         * Sets the value of the timeaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustPortInqResponse.ResponseData.TIMEACCOUNT }
         *     
         */
        public void setTIMEACCOUNT(CustPortInqResponse.ResponseData.TIMEACCOUNT value) {
            this.timeaccount = value;
        }

        /**
         * Gets the value of the depaccount property.
         * 
         * @return
         *     possible object is
         *     {@link CustPortInqResponse.ResponseData.DEPACCOUNT }
         *     
         */
        public CustPortInqResponse.ResponseData.DEPACCOUNT getDEPACCOUNT() {
            return depaccount;
        }

        /**
         * Sets the value of the depaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustPortInqResponse.ResponseData.DEPACCOUNT }
         *     
         */
        public void setDEPACCOUNT(CustPortInqResponse.ResponseData.DEPACCOUNT value) {
            this.depaccount = value;
        }

        /**
         * Gets the value of the loanaccount property.
         * 
         * @return
         *     possible object is
         *     {@link CustPortInqResponse.ResponseData.LOANACCOUNT }
         *     
         */
        public CustPortInqResponse.ResponseData.LOANACCOUNT getLOANACCOUNT() {
            return loanaccount;
        }

        /**
         * Sets the value of the loanaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustPortInqResponse.ResponseData.LOANACCOUNT }
         *     
         */
        public void setLOANACCOUNT(CustPortInqResponse.ResponseData.LOANACCOUNT value) {
            this.loanaccount = value;
        }

        /**
         * Gets the value of the mtgaccount property.
         * 
         * @return
         *     possible object is
         *     {@link CustPortInqResponse.ResponseData.MTGACCOUNT }
         *     
         */
        public CustPortInqResponse.ResponseData.MTGACCOUNT getMTGACCOUNT() {
            return mtgaccount;
        }

        /**
         * Sets the value of the mtgaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustPortInqResponse.ResponseData.MTGACCOUNT }
         *     
         */
        public void setMTGACCOUNT(CustPortInqResponse.ResponseData.MTGACCOUNT value) {
            this.mtgaccount = value;
        }

        /**
         * Gets the value of the recsreturned property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRECSRETURNED() {
            return recsreturned;
        }

        /**
         * Sets the value of the recsreturned property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRECSRETURNED(String value) {
            this.recsreturned = value;
        }

        /**
         * Gets the value of the moredata property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMOREDATA() {
            return moredata;
        }

        /**
         * Sets the value of the moredata property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMOREDATA(String value) {
            this.moredata = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BRNNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT02" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT03" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT05" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT06" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT07" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT08" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT11" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE02" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE03" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE05" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE06" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE07" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC02" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "undprelext",
            "groupcd",
            "groupseq",
            "applcd",
            "actdesc",
            "actnbr",
            "prodtype",
            "shortname",
            "officer",
            "brnnbr",
            "reltype",
            "primaryflg",
            "txrspent",
            "pctowned",
            "ownertype",
            "interflg",
            "ivrflg",
            "curintrate",
            "actstatus",
            "currbal",
            "availbal",
            "payoffamt",
            "intpdytd",
            "intpdlstyr",
            "intaccrued",
            "dtopened",
            "dtclosed",
            "dtlststmt",
            "amount01",
            "amount02",
            "amount03",
            "amount04",
            "amount05",
            "amount06",
            "amount07",
            "amount08",
            "amount09",
            "amount10",
            "amount11",
            "amount12",
            "date01",
            "date02",
            "date03",
            "date04",
            "date05",
            "date06",
            "date07",
            "date08",
            "misc01",
            "misc02",
            "misc03",
            "future01",
            "porttotflg",
            "pctalwdflg",
            "ocdstatus",
            "confflag",
            "cashmgmt1",
            "cashmgmt2",
            "regdrstint",
            "thirdparty",
            "preauttrct",
            "option1",
            "option2",
            "billpayflg",
            "amount13",
            "rrsselflg",
            "dyavailflg",
            "ddavailflg",
            "lnavailflg",
            "odlimitflg",
            "custfltflg",
            "bnkfltflg",
            "holdsflg",
            "ckpdtdyflg",
            "amount14",
            "amount15",
            "amount16",
            "amount17",
            "amount18",
            "date09",
            "date10",
            "misc04",
            "misc05",
            "misc06",
            "misc07"
        })
        public static class ACCOUNT {

            @XmlElement(name = "UNDPRELEXT", required = true)
            protected Object undprelext;
            @XmlElement(name = "GROUPCD", required = true)
            protected String groupcd;
            @XmlElement(name = "GROUPSEQ", required = true)
            protected String groupseq;
            @XmlElement(name = "APPLCD", required = true)
            protected String applcd;
            @XmlElement(name = "ACTDESC", required = true)
            protected String actdesc;
            @XmlElement(name = "ACTNBR", required = true)
            protected String actnbr;
            @XmlElement(name = "PRODTYPE", required = true)
            protected String prodtype;
            @XmlElement(name = "SHORTNAME", required = true)
            protected String shortname;
            @XmlElement(name = "OFFICER", required = true)
            protected Object officer;
            @XmlElement(name = "BRNNBR", required = true)
            protected String brnnbr;
            @XmlElement(name = "RELTYPE", required = true)
            protected String reltype;
            @XmlElement(name = "PRIMARYFLG", required = true)
            protected String primaryflg;
            @XmlElement(name = "TXRSPENT", required = true)
            protected String txrspent;
            @XmlElement(name = "PCTOWNED", required = true)
            protected String pctowned;
            @XmlElement(name = "OWNERTYPE", required = true)
            protected String ownertype;
            @XmlElement(name = "INTERFLG", required = true)
            protected String interflg;
            @XmlElement(name = "IVRFLG", required = true)
            protected String ivrflg;
            @XmlElement(name = "CURINTRATE", required = true)
            protected String curintrate;
            @XmlElement(name = "ACTSTATUS", required = true)
            protected String actstatus;
            @XmlElement(name = "CURRBAL", required = true)
            protected String currbal;
            @XmlElement(name = "AVAILBAL", required = true)
            protected String availbal;
            @XmlElement(name = "PAYOFFAMT", required = true)
            protected String payoffamt;
            @XmlElement(name = "INTPDYTD", required = true)
            protected String intpdytd;
            @XmlElement(name = "INTPDLSTYR", required = true)
            protected String intpdlstyr;
            @XmlElement(name = "INTACCRUED", required = true)
            protected String intaccrued;
            @XmlElement(name = "DTOPENED", required = true)
            protected String dtopened;
            @XmlElement(name = "DTCLOSED", required = true)
            protected String dtclosed;
            @XmlElement(name = "DTLSTSTMT", required = true)
            protected String dtlststmt;
            @XmlElement(name = "AMOUNT01", required = true)
            protected String amount01;
            @XmlElement(name = "AMOUNT02", required = true)
            protected String amount02;
            @XmlElement(name = "AMOUNT03", required = true)
            protected String amount03;
            @XmlElement(name = "AMOUNT04", required = true)
            protected String amount04;
            @XmlElement(name = "AMOUNT05", required = true)
            protected String amount05;
            @XmlElement(name = "AMOUNT06", required = true)
            protected String amount06;
            @XmlElement(name = "AMOUNT07", required = true)
            protected String amount07;
            @XmlElement(name = "AMOUNT08", required = true)
            protected String amount08;
            @XmlElement(name = "AMOUNT09", required = true)
            protected String amount09;
            @XmlElement(name = "AMOUNT10", required = true)
            protected String amount10;
            @XmlElement(name = "AMOUNT11", required = true)
            protected String amount11;
            @XmlElement(name = "AMOUNT12", required = true)
            protected String amount12;
            @XmlElement(name = "DATE01", required = true)
            protected String date01;
            @XmlElement(name = "DATE02", required = true)
            protected String date02;
            @XmlElement(name = "DATE03", required = true)
            protected String date03;
            @XmlElement(name = "DATE04", required = true)
            protected String date04;
            @XmlElement(name = "DATE05", required = true)
            protected String date05;
            @XmlElement(name = "DATE06", required = true)
            protected String date06;
            @XmlElement(name = "DATE07", required = true)
            protected String date07;
            @XmlElement(name = "DATE08", required = true)
            protected String date08;
            @XmlElement(name = "MISC01", required = true)
            protected String misc01;
            @XmlElement(name = "MISC02", required = true)
            protected Object misc02;
            @XmlElement(name = "MISC03", required = true)
            protected Object misc03;
            @XmlElement(name = "FUTURE01", required = true)
            protected Object future01;
            @XmlElement(name = "PORTTOTFLG", required = true)
            protected Object porttotflg;
            @XmlElement(name = "PCTALWDFLG", required = true)
            protected Object pctalwdflg;
            @XmlElement(name = "OCDSTATUS", required = true)
            protected String ocdstatus;
            @XmlElement(name = "CONFFLAG", required = true)
            protected Object confflag;
            @XmlElement(name = "CASHMGMT1", required = true)
            protected String cashmgmt1;
            @XmlElement(name = "CASHMGMT2", required = true)
            protected String cashmgmt2;
            @XmlElement(name = "REGDRSTINT", required = true)
            protected Object regdrstint;
            @XmlElement(name = "THIRDPARTY", required = true)
            protected String thirdparty;
            @XmlElement(name = "PREAUTTRCT", required = true)
            protected String preauttrct;
            @XmlElement(name = "OPTION1", required = true)
            protected Object option1;
            @XmlElement(name = "OPTION2", required = true)
            protected Object option2;
            @XmlElement(name = "BILLPAYFLG", required = true)
            protected Object billpayflg;
            @XmlElement(name = "AMOUNT13", required = true)
            protected String amount13;
            @XmlElement(name = "RRSSELFLG", required = true)
            protected Object rrsselflg;
            @XmlElement(name = "DYAVAILFLG", required = true)
            protected Object dyavailflg;
            @XmlElement(name = "DDAVAILFLG", required = true)
            protected Object ddavailflg;
            @XmlElement(name = "LNAVAILFLG", required = true)
            protected Object lnavailflg;
            @XmlElement(name = "ODLIMITFLG", required = true)
            protected Object odlimitflg;
            @XmlElement(name = "CUSTFLTFLG", required = true)
            protected Object custfltflg;
            @XmlElement(name = "BNKFLTFLG", required = true)
            protected Object bnkfltflg;
            @XmlElement(name = "HOLDSFLG", required = true)
            protected Object holdsflg;
            @XmlElement(name = "CKPDTDYFLG", required = true)
            protected Object ckpdtdyflg;
            @XmlElement(name = "AMOUNT14", required = true)
            protected String amount14;
            @XmlElement(name = "AMOUNT15", required = true)
            protected String amount15;
            @XmlElement(name = "AMOUNT16", required = true)
            protected String amount16;
            @XmlElement(name = "AMOUNT17", required = true)
            protected String amount17;
            @XmlElement(name = "AMOUNT18", required = true)
            protected String amount18;
            @XmlElement(name = "DATE09", required = true)
            protected String date09;
            @XmlElement(name = "DATE10", required = true)
            protected String date10;
            @XmlElement(name = "MISC04", required = true)
            protected Object misc04;
            @XmlElement(name = "MISC05", required = true)
            protected Object misc05;
            @XmlElement(name = "MISC06", required = true)
            protected Object misc06;
            @XmlElement(name = "MISC07", required = true)
            protected Object misc07;

            /**
             * Gets the value of the undprelext property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getUNDPRELEXT() {
                return undprelext;
            }

            /**
             * Sets the value of the undprelext property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setUNDPRELEXT(Object value) {
                this.undprelext = value;
            }

            /**
             * Gets the value of the groupcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPCD() {
                return groupcd;
            }

            /**
             * Sets the value of the groupcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPCD(String value) {
                this.groupcd = value;
            }

            /**
             * Gets the value of the groupseq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPSEQ() {
                return groupseq;
            }

            /**
             * Sets the value of the groupseq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPSEQ(String value) {
                this.groupseq = value;
            }

            /**
             * Gets the value of the applcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAPPLCD() {
                return applcd;
            }

            /**
             * Sets the value of the applcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAPPLCD(String value) {
                this.applcd = value;
            }

            /**
             * Gets the value of the actdesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTDESC() {
                return actdesc;
            }

            /**
             * Sets the value of the actdesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTDESC(String value) {
                this.actdesc = value;
            }

            /**
             * Gets the value of the actnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTNBR() {
                return actnbr;
            }

            /**
             * Sets the value of the actnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTNBR(String value) {
                this.actnbr = value;
            }

            /**
             * Gets the value of the prodtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRODTYPE() {
                return prodtype;
            }

            /**
             * Sets the value of the prodtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRODTYPE(String value) {
                this.prodtype = value;
            }

            /**
             * Gets the value of the shortname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSHORTNAME() {
                return shortname;
            }

            /**
             * Sets the value of the shortname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSHORTNAME(String value) {
                this.shortname = value;
            }

            /**
             * Gets the value of the officer property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOFFICER() {
                return officer;
            }

            /**
             * Sets the value of the officer property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOFFICER(Object value) {
                this.officer = value;
            }

            /**
             * Gets the value of the brnnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBRNNBR() {
                return brnnbr;
            }

            /**
             * Sets the value of the brnnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBRNNBR(String value) {
                this.brnnbr = value;
            }

            /**
             * Gets the value of the reltype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRELTYPE() {
                return reltype;
            }

            /**
             * Sets the value of the reltype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRELTYPE(String value) {
                this.reltype = value;
            }

            /**
             * Gets the value of the primaryflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRIMARYFLG() {
                return primaryflg;
            }

            /**
             * Sets the value of the primaryflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRIMARYFLG(String value) {
                this.primaryflg = value;
            }

            /**
             * Gets the value of the txrspent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTXRSPENT() {
                return txrspent;
            }

            /**
             * Sets the value of the txrspent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTXRSPENT(String value) {
                this.txrspent = value;
            }

            /**
             * Gets the value of the pctowned property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPCTOWNED() {
                return pctowned;
            }

            /**
             * Sets the value of the pctowned property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPCTOWNED(String value) {
                this.pctowned = value;
            }

            /**
             * Gets the value of the ownertype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOWNERTYPE() {
                return ownertype;
            }

            /**
             * Sets the value of the ownertype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOWNERTYPE(String value) {
                this.ownertype = value;
            }

            /**
             * Gets the value of the interflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERFLG() {
                return interflg;
            }

            /**
             * Sets the value of the interflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERFLG(String value) {
                this.interflg = value;
            }

            /**
             * Gets the value of the ivrflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRFLG() {
                return ivrflg;
            }

            /**
             * Sets the value of the ivrflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRFLG(String value) {
                this.ivrflg = value;
            }

            /**
             * Gets the value of the curintrate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURINTRATE() {
                return curintrate;
            }

            /**
             * Sets the value of the curintrate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURINTRATE(String value) {
                this.curintrate = value;
            }

            /**
             * Gets the value of the actstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTSTATUS() {
                return actstatus;
            }

            /**
             * Sets the value of the actstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTSTATUS(String value) {
                this.actstatus = value;
            }

            /**
             * Gets the value of the currbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURRBAL() {
                return currbal;
            }

            /**
             * Sets the value of the currbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURRBAL(String value) {
                this.currbal = value;
            }

            /**
             * Gets the value of the availbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAVAILBAL() {
                return availbal;
            }

            /**
             * Sets the value of the availbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAVAILBAL(String value) {
                this.availbal = value;
            }

            /**
             * Gets the value of the payoffamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPAYOFFAMT() {
                return payoffamt;
            }

            /**
             * Sets the value of the payoffamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPAYOFFAMT(String value) {
                this.payoffamt = value;
            }

            /**
             * Gets the value of the intpdytd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDYTD() {
                return intpdytd;
            }

            /**
             * Sets the value of the intpdytd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDYTD(String value) {
                this.intpdytd = value;
            }

            /**
             * Gets the value of the intpdlstyr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDLSTYR() {
                return intpdlstyr;
            }

            /**
             * Sets the value of the intpdlstyr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDLSTYR(String value) {
                this.intpdlstyr = value;
            }

            /**
             * Gets the value of the intaccrued property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTACCRUED() {
                return intaccrued;
            }

            /**
             * Sets the value of the intaccrued property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTACCRUED(String value) {
                this.intaccrued = value;
            }

            /**
             * Gets the value of the dtopened property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTOPENED() {
                return dtopened;
            }

            /**
             * Sets the value of the dtopened property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTOPENED(String value) {
                this.dtopened = value;
            }

            /**
             * Gets the value of the dtclosed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTCLOSED() {
                return dtclosed;
            }

            /**
             * Sets the value of the dtclosed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTCLOSED(String value) {
                this.dtclosed = value;
            }

            /**
             * Gets the value of the dtlststmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTLSTSTMT() {
                return dtlststmt;
            }

            /**
             * Sets the value of the dtlststmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTLSTSTMT(String value) {
                this.dtlststmt = value;
            }

            /**
             * Gets the value of the amount01 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT01() {
                return amount01;
            }

            /**
             * Sets the value of the amount01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT01(String value) {
                this.amount01 = value;
            }

            /**
             * Gets the value of the amount02 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT02() {
                return amount02;
            }

            /**
             * Sets the value of the amount02 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT02(String value) {
                this.amount02 = value;
            }

            /**
             * Gets the value of the amount03 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT03() {
                return amount03;
            }

            /**
             * Sets the value of the amount03 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT03(String value) {
                this.amount03 = value;
            }

            /**
             * Gets the value of the amount04 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT04() {
                return amount04;
            }

            /**
             * Sets the value of the amount04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT04(String value) {
                this.amount04 = value;
            }

            /**
             * Gets the value of the amount05 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT05() {
                return amount05;
            }

            /**
             * Sets the value of the amount05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT05(String value) {
                this.amount05 = value;
            }

            /**
             * Gets the value of the amount06 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT06() {
                return amount06;
            }

            /**
             * Sets the value of the amount06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT06(String value) {
                this.amount06 = value;
            }

            /**
             * Gets the value of the amount07 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT07() {
                return amount07;
            }

            /**
             * Sets the value of the amount07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT07(String value) {
                this.amount07 = value;
            }

            /**
             * Gets the value of the amount08 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT08() {
                return amount08;
            }

            /**
             * Sets the value of the amount08 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT08(String value) {
                this.amount08 = value;
            }

            /**
             * Gets the value of the amount09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT09() {
                return amount09;
            }

            /**
             * Sets the value of the amount09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT09(String value) {
                this.amount09 = value;
            }

            /**
             * Gets the value of the amount10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT10() {
                return amount10;
            }

            /**
             * Sets the value of the amount10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT10(String value) {
                this.amount10 = value;
            }

            /**
             * Gets the value of the amount11 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT11() {
                return amount11;
            }

            /**
             * Sets the value of the amount11 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT11(String value) {
                this.amount11 = value;
            }

            /**
             * Gets the value of the amount12 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT12() {
                return amount12;
            }

            /**
             * Sets the value of the amount12 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT12(String value) {
                this.amount12 = value;
            }

            /**
             * Gets the value of the date01 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE01() {
                return date01;
            }

            /**
             * Sets the value of the date01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE01(String value) {
                this.date01 = value;
            }

            /**
             * Gets the value of the date02 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE02() {
                return date02;
            }

            /**
             * Sets the value of the date02 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE02(String value) {
                this.date02 = value;
            }

            /**
             * Gets the value of the date03 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE03() {
                return date03;
            }

            /**
             * Sets the value of the date03 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE03(String value) {
                this.date03 = value;
            }

            /**
             * Gets the value of the date04 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE04() {
                return date04;
            }

            /**
             * Sets the value of the date04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE04(String value) {
                this.date04 = value;
            }

            /**
             * Gets the value of the date05 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE05() {
                return date05;
            }

            /**
             * Sets the value of the date05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE05(String value) {
                this.date05 = value;
            }

            /**
             * Gets the value of the date06 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE06() {
                return date06;
            }

            /**
             * Sets the value of the date06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE06(String value) {
                this.date06 = value;
            }

            /**
             * Gets the value of the date07 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE07() {
                return date07;
            }

            /**
             * Sets the value of the date07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE07(String value) {
                this.date07 = value;
            }

            /**
             * Gets the value of the date08 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE08() {
                return date08;
            }

            /**
             * Sets the value of the date08 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE08(String value) {
                this.date08 = value;
            }

            /**
             * Gets the value of the misc01 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMISC01() {
                return misc01;
            }

            /**
             * Sets the value of the misc01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMISC01(String value) {
                this.misc01 = value;
            }

            /**
             * Gets the value of the misc02 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC02() {
                return misc02;
            }

            /**
             * Sets the value of the misc02 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC02(Object value) {
                this.misc02 = value;
            }

            /**
             * Gets the value of the misc03 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC03() {
                return misc03;
            }

            /**
             * Sets the value of the misc03 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC03(Object value) {
                this.misc03 = value;
            }

            /**
             * Gets the value of the future01 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getFUTURE01() {
                return future01;
            }

            /**
             * Sets the value of the future01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setFUTURE01(Object value) {
                this.future01 = value;
            }

            /**
             * Gets the value of the porttotflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPORTTOTFLG() {
                return porttotflg;
            }

            /**
             * Sets the value of the porttotflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPORTTOTFLG(Object value) {
                this.porttotflg = value;
            }

            /**
             * Gets the value of the pctalwdflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPCTALWDFLG() {
                return pctalwdflg;
            }

            /**
             * Sets the value of the pctalwdflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPCTALWDFLG(Object value) {
                this.pctalwdflg = value;
            }

            /**
             * Gets the value of the ocdstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCDSTATUS() {
                return ocdstatus;
            }

            /**
             * Sets the value of the ocdstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCDSTATUS(String value) {
                this.ocdstatus = value;
            }

            /**
             * Gets the value of the confflag property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getCONFFLAG() {
                return confflag;
            }

            /**
             * Sets the value of the confflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setCONFFLAG(Object value) {
                this.confflag = value;
            }

            /**
             * Gets the value of the cashmgmt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT1() {
                return cashmgmt1;
            }

            /**
             * Sets the value of the cashmgmt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT1(String value) {
                this.cashmgmt1 = value;
            }

            /**
             * Gets the value of the cashmgmt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT2() {
                return cashmgmt2;
            }

            /**
             * Sets the value of the cashmgmt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT2(String value) {
                this.cashmgmt2 = value;
            }

            /**
             * Gets the value of the regdrstint property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getREGDRSTINT() {
                return regdrstint;
            }

            /**
             * Sets the value of the regdrstint property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setREGDRSTINT(Object value) {
                this.regdrstint = value;
            }

            /**
             * Gets the value of the thirdparty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTHIRDPARTY() {
                return thirdparty;
            }

            /**
             * Sets the value of the thirdparty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTHIRDPARTY(String value) {
                this.thirdparty = value;
            }

            /**
             * Gets the value of the preauttrct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPREAUTTRCT() {
                return preauttrct;
            }

            /**
             * Sets the value of the preauttrct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPREAUTTRCT(String value) {
                this.preauttrct = value;
            }

            /**
             * Gets the value of the option1 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION1() {
                return option1;
            }

            /**
             * Sets the value of the option1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION1(Object value) {
                this.option1 = value;
            }

            /**
             * Gets the value of the option2 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION2() {
                return option2;
            }

            /**
             * Sets the value of the option2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION2(Object value) {
                this.option2 = value;
            }

            /**
             * Gets the value of the billpayflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBILLPAYFLG() {
                return billpayflg;
            }

            /**
             * Sets the value of the billpayflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBILLPAYFLG(Object value) {
                this.billpayflg = value;
            }

            /**
             * Gets the value of the amount13 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT13() {
                return amount13;
            }

            /**
             * Sets the value of the amount13 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT13(String value) {
                this.amount13 = value;
            }

            /**
             * Gets the value of the rrsselflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getRRSSELFLG() {
                return rrsselflg;
            }

            /**
             * Sets the value of the rrsselflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setRRSSELFLG(Object value) {
                this.rrsselflg = value;
            }

            /**
             * Gets the value of the dyavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getDYAVAILFLG() {
                return dyavailflg;
            }

            /**
             * Sets the value of the dyavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setDYAVAILFLG(Object value) {
                this.dyavailflg = value;
            }

            /**
             * Gets the value of the ddavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getDDAVAILFLG() {
                return ddavailflg;
            }

            /**
             * Sets the value of the ddavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setDDAVAILFLG(Object value) {
                this.ddavailflg = value;
            }

            /**
             * Gets the value of the lnavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getLNAVAILFLG() {
                return lnavailflg;
            }

            /**
             * Sets the value of the lnavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setLNAVAILFLG(Object value) {
                this.lnavailflg = value;
            }

            /**
             * Gets the value of the odlimitflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getODLIMITFLG() {
                return odlimitflg;
            }

            /**
             * Sets the value of the odlimitflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setODLIMITFLG(Object value) {
                this.odlimitflg = value;
            }

            /**
             * Gets the value of the custfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getCUSTFLTFLG() {
                return custfltflg;
            }

            /**
             * Sets the value of the custfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setCUSTFLTFLG(Object value) {
                this.custfltflg = value;
            }

            /**
             * Gets the value of the bnkfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBNKFLTFLG() {
                return bnkfltflg;
            }

            /**
             * Sets the value of the bnkfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBNKFLTFLG(Object value) {
                this.bnkfltflg = value;
            }

            /**
             * Gets the value of the holdsflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getHOLDSFLG() {
                return holdsflg;
            }

            /**
             * Sets the value of the holdsflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setHOLDSFLG(Object value) {
                this.holdsflg = value;
            }

            /**
             * Gets the value of the ckpdtdyflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getCKPDTDYFLG() {
                return ckpdtdyflg;
            }

            /**
             * Sets the value of the ckpdtdyflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setCKPDTDYFLG(Object value) {
                this.ckpdtdyflg = value;
            }

            /**
             * Gets the value of the amount14 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT14() {
                return amount14;
            }

            /**
             * Sets the value of the amount14 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT14(String value) {
                this.amount14 = value;
            }

            /**
             * Gets the value of the amount15 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT15() {
                return amount15;
            }

            /**
             * Sets the value of the amount15 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT15(String value) {
                this.amount15 = value;
            }

            /**
             * Gets the value of the amount16 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT16() {
                return amount16;
            }

            /**
             * Sets the value of the amount16 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT16(String value) {
                this.amount16 = value;
            }

            /**
             * Gets the value of the amount17 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT17() {
                return amount17;
            }

            /**
             * Sets the value of the amount17 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT17(String value) {
                this.amount17 = value;
            }

            /**
             * Gets the value of the amount18 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT18() {
                return amount18;
            }

            /**
             * Sets the value of the amount18 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT18(String value) {
                this.amount18 = value;
            }

            /**
             * Gets the value of the date09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE09() {
                return date09;
            }

            /**
             * Sets the value of the date09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE09(String value) {
                this.date09 = value;
            }

            /**
             * Gets the value of the date10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE10() {
                return date10;
            }

            /**
             * Sets the value of the date10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE10(String value) {
                this.date10 = value;
            }

            /**
             * Gets the value of the misc04 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC04() {
                return misc04;
            }

            /**
             * Sets the value of the misc04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC04(Object value) {
                this.misc04 = value;
            }

            /**
             * Gets the value of the misc05 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC05() {
                return misc05;
            }

            /**
             * Sets the value of the misc05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC05(Object value) {
                this.misc05 = value;
            }

            /**
             * Gets the value of the misc06 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC06() {
                return misc06;
            }

            /**
             * Sets the value of the misc06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC06(Object value) {
                this.misc06 = value;
            }

            /**
             * Gets the value of the misc07 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC07() {
                return misc07;
            }

            /**
             * Sets the value of the misc07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC07(Object value) {
                this.misc07 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTINTAMTP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOLDAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CHKDEPTDY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TBNKFLTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TCUSTFLTAM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CRLMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSCURBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="COLLBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMTLSTDEP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CTFAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RUNCURBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTODDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVBGSTMDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVEDSTMDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVBGSTMDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVEDSTMDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVBGSTMDT3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVEDSTMDT3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PVBGSTMDT4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PASSBOOKCD" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="STMTCYCCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ENCOPTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ENCOPTNTC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="LNCRLIMIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DYNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDSVAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ODLIMITBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "undprelext",
            "groupcd",
            "groupseq",
            "applcd",
            "actdesc",
            "actnbr",
            "prodtype",
            "shortname",
            "officer",
            "branch",
            "reltype",
            "primaryflg",
            "txrspent",
            "pctowned",
            "ownertype",
            "interflg",
            "ivrflg",
            "curintrate",
            "actstatus",
            "currbal",
            "availbal",
            "payoffamt",
            "intpdytd",
            "intpdlstyr",
            "intaccrued",
            "dtopened",
            "dtclosed",
            "dtlststmt",
            "lstintamtp",
            "holdamt",
            "chkdeptdy",
            "tbnkfltamt",
            "tcustfltam",
            "crlmtamt",
            "rrscurbal",
            "collbal",
            "amtlstdep",
            "ctfavabal",
            "runcurbal",
            "amount12",
            "lstoddt",
            "pvbgstmdt1",
            "pvedstmdt1",
            "pvbgstmdt2",
            "pvedstmdt2",
            "pvbgstmdt3",
            "pvedstmdt3",
            "pvbgstmdt4",
            "misc01",
            "passbookcd",
            "stmtcyccd",
            "future01",
            "porttotflg",
            "pctalwdflg",
            "ocdstatus",
            "confflag",
            "cashmgmt1",
            "cashmgmt2",
            "regdrstint",
            "thirdparty",
            "preauttrct",
            "encoptstmt",
            "encoptntc",
            "billpayflg",
            "lncrlimit",
            "rrsselflg",
            "dyavailflg",
            "ddavailflg",
            "lnavailflg",
            "odlimitflg",
            "custfltflg",
            "bnkfltflg",
            "holdsflg",
            "ckpdtdyflg",
            "rrsavabal",
            "dynavabal",
            "ddsvavabal",
            "lnavabal",
            "odlimitbal",
            "date09",
            "date10",
            "misc04",
            "misc05",
            "misc06",
            "misc07"
        })
        public static class DEPACCOUNT {

            @XmlElement(name = "UNDPRELEXT", required = true)
            protected Object undprelext;
            @XmlElement(name = "GROUPCD", required = true)
            protected String groupcd;
            @XmlElement(name = "GROUPSEQ", required = true)
            protected String groupseq;
            @XmlElement(name = "APPLCD", required = true)
            protected String applcd;
            @XmlElement(name = "ACTDESC", required = true)
            protected String actdesc;
            @XmlElement(name = "ACTNBR", required = true)
            protected String actnbr;
            @XmlElement(name = "PRODTYPE", required = true)
            protected String prodtype;
            @XmlElement(name = "SHORTNAME", required = true)
            protected String shortname;
            @XmlElement(name = "OFFICER", required = true)
            protected Object officer;
            @XmlElement(name = "BRANCH", required = true)
            protected String branch;
            @XmlElement(name = "RELTYPE", required = true)
            protected String reltype;
            @XmlElement(name = "PRIMARYFLG", required = true)
            protected String primaryflg;
            @XmlElement(name = "TXRSPENT", required = true)
            protected String txrspent;
            @XmlElement(name = "PCTOWNED", required = true)
            protected String pctowned;
            @XmlElement(name = "OWNERTYPE", required = true)
            protected String ownertype;
            @XmlElement(name = "INTERFLG", required = true)
            protected String interflg;
            @XmlElement(name = "IVRFLG", required = true)
            protected String ivrflg;
            @XmlElement(name = "CURINTRATE", required = true)
            protected String curintrate;
            @XmlElement(name = "ACTSTATUS", required = true)
            protected String actstatus;
            @XmlElement(name = "CURRBAL", required = true)
            protected String currbal;
            @XmlElement(name = "AVAILBAL", required = true)
            protected String availbal;
            @XmlElement(name = "PAYOFFAMT", required = true)
            protected String payoffamt;
            @XmlElement(name = "INTPDYTD", required = true)
            protected String intpdytd;
            @XmlElement(name = "INTPDLSTYR", required = true)
            protected String intpdlstyr;
            @XmlElement(name = "INTACCRUED", required = true)
            protected String intaccrued;
            @XmlElement(name = "DTOPENED", required = true)
            protected String dtopened;
            @XmlElement(name = "DTCLOSED", required = true)
            protected String dtclosed;
            @XmlElement(name = "DTLSTSTMT", required = true)
            protected String dtlststmt;
            @XmlElement(name = "LSTINTAMTP", required = true)
            protected String lstintamtp;
            @XmlElement(name = "HOLDAMT", required = true)
            protected String holdamt;
            @XmlElement(name = "CHKDEPTDY", required = true)
            protected String chkdeptdy;
            @XmlElement(name = "TBNKFLTAMT", required = true)
            protected String tbnkfltamt;
            @XmlElement(name = "TCUSTFLTAM", required = true)
            protected String tcustfltam;
            @XmlElement(name = "CRLMTAMT", required = true)
            protected String crlmtamt;
            @XmlElement(name = "RRSCURBAL", required = true)
            protected String rrscurbal;
            @XmlElement(name = "COLLBAL", required = true)
            protected String collbal;
            @XmlElement(name = "AMTLSTDEP", required = true)
            protected String amtlstdep;
            @XmlElement(name = "CTFAVABAL", required = true)
            protected String ctfavabal;
            @XmlElement(name = "RUNCURBAL", required = true)
            protected String runcurbal;
            @XmlElement(name = "AMOUNT12", required = true)
            protected String amount12;
            @XmlElement(name = "LSTODDT", required = true)
            protected String lstoddt;
            @XmlElement(name = "PVBGSTMDT1", required = true)
            protected String pvbgstmdt1;
            @XmlElement(name = "PVEDSTMDT1", required = true)
            protected String pvedstmdt1;
            @XmlElement(name = "PVBGSTMDT2", required = true)
            protected String pvbgstmdt2;
            @XmlElement(name = "PVEDSTMDT2", required = true)
            protected String pvedstmdt2;
            @XmlElement(name = "PVBGSTMDT3", required = true)
            protected String pvbgstmdt3;
            @XmlElement(name = "PVEDSTMDT3", required = true)
            protected String pvedstmdt3;
            @XmlElement(name = "PVBGSTMDT4", required = true)
            protected String pvbgstmdt4;
            @XmlElement(name = "MISC01", required = true)
            protected String misc01;
            @XmlElement(name = "PASSBOOKCD", required = true)
            protected Object passbookcd;
            @XmlElement(name = "STMTCYCCD", required = true)
            protected String stmtcyccd;
            @XmlElement(name = "FUTURE01", required = true)
            protected Object future01;
            @XmlElement(name = "PORTTOTFLG", required = true)
            protected Object porttotflg;
            @XmlElement(name = "PCTALWDFLG", required = true)
            protected Object pctalwdflg;
            @XmlElement(name = "OCDSTATUS", required = true)
            protected String ocdstatus;
            @XmlElement(name = "CONFFLAG", required = true)
            protected String confflag;
            @XmlElement(name = "CASHMGMT1", required = true)
            protected String cashmgmt1;
            @XmlElement(name = "CASHMGMT2", required = true)
            protected String cashmgmt2;
            @XmlElement(name = "REGDRSTINT", required = true)
            protected String regdrstint;
            @XmlElement(name = "THIRDPARTY", required = true)
            protected String thirdparty;
            @XmlElement(name = "PREAUTTRCT", required = true)
            protected String preauttrct;
            @XmlElement(name = "ENCOPTSTMT", required = true)
            protected String encoptstmt;
            @XmlElement(name = "ENCOPTNTC", required = true)
            protected String encoptntc;
            @XmlElement(name = "BILLPAYFLG", required = true)
            protected Object billpayflg;
            @XmlElement(name = "LNCRLIMIT", required = true)
            protected String lncrlimit;
            @XmlElement(name = "RRSSELFLG", required = true)
            protected String rrsselflg;
            @XmlElement(name = "DYAVAILFLG", required = true)
            protected String dyavailflg;
            @XmlElement(name = "DDAVAILFLG", required = true)
            protected String ddavailflg;
            @XmlElement(name = "LNAVAILFLG", required = true)
            protected String lnavailflg;
            @XmlElement(name = "ODLIMITFLG", required = true)
            protected String odlimitflg;
            @XmlElement(name = "CUSTFLTFLG", required = true)
            protected String custfltflg;
            @XmlElement(name = "BNKFLTFLG", required = true)
            protected String bnkfltflg;
            @XmlElement(name = "HOLDSFLG", required = true)
            protected String holdsflg;
            @XmlElement(name = "CKPDTDYFLG", required = true)
            protected String ckpdtdyflg;
            @XmlElement(name = "RRSAVABAL", required = true)
            protected String rrsavabal;
            @XmlElement(name = "DYNAVABAL", required = true)
            protected String dynavabal;
            @XmlElement(name = "DDSVAVABAL", required = true)
            protected String ddsvavabal;
            @XmlElement(name = "LNAVABAL", required = true)
            protected String lnavabal;
            @XmlElement(name = "ODLIMITBAL", required = true)
            protected String odlimitbal;
            @XmlElement(name = "DATE09", required = true)
            protected String date09;
            @XmlElement(name = "DATE10", required = true)
            protected String date10;
            @XmlElement(name = "MISC04", required = true)
            protected Object misc04;
            @XmlElement(name = "MISC05", required = true)
            protected Object misc05;
            @XmlElement(name = "MISC06", required = true)
            protected Object misc06;
            @XmlElement(name = "MISC07", required = true)
            protected Object misc07;

            /**
             * Gets the value of the undprelext property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getUNDPRELEXT() {
                return undprelext;
            }

            /**
             * Sets the value of the undprelext property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setUNDPRELEXT(Object value) {
                this.undprelext = value;
            }

            /**
             * Gets the value of the groupcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPCD() {
                return groupcd;
            }

            /**
             * Sets the value of the groupcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPCD(String value) {
                this.groupcd = value;
            }

            /**
             * Gets the value of the groupseq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPSEQ() {
                return groupseq;
            }

            /**
             * Sets the value of the groupseq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPSEQ(String value) {
                this.groupseq = value;
            }

            /**
             * Gets the value of the applcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAPPLCD() {
                return applcd;
            }

            /**
             * Sets the value of the applcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAPPLCD(String value) {
                this.applcd = value;
            }

            /**
             * Gets the value of the actdesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTDESC() {
                return actdesc;
            }

            /**
             * Sets the value of the actdesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTDESC(String value) {
                this.actdesc = value;
            }

            /**
             * Gets the value of the actnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTNBR() {
                return actnbr;
            }

            /**
             * Sets the value of the actnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTNBR(String value) {
                this.actnbr = value;
            }

            /**
             * Gets the value of the prodtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRODTYPE() {
                return prodtype;
            }

            /**
             * Sets the value of the prodtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRODTYPE(String value) {
                this.prodtype = value;
            }

            /**
             * Gets the value of the shortname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSHORTNAME() {
                return shortname;
            }

            /**
             * Sets the value of the shortname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSHORTNAME(String value) {
                this.shortname = value;
            }

            /**
             * Gets the value of the officer property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOFFICER() {
                return officer;
            }

            /**
             * Sets the value of the officer property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOFFICER(Object value) {
                this.officer = value;
            }

            /**
             * Gets the value of the branch property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBRANCH() {
                return branch;
            }

            /**
             * Sets the value of the branch property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBRANCH(String value) {
                this.branch = value;
            }

            /**
             * Gets the value of the reltype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRELTYPE() {
                return reltype;
            }

            /**
             * Sets the value of the reltype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRELTYPE(String value) {
                this.reltype = value;
            }

            /**
             * Gets the value of the primaryflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRIMARYFLG() {
                return primaryflg;
            }

            /**
             * Sets the value of the primaryflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRIMARYFLG(String value) {
                this.primaryflg = value;
            }

            /**
             * Gets the value of the txrspent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTXRSPENT() {
                return txrspent;
            }

            /**
             * Sets the value of the txrspent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTXRSPENT(String value) {
                this.txrspent = value;
            }

            /**
             * Gets the value of the pctowned property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPCTOWNED() {
                return pctowned;
            }

            /**
             * Sets the value of the pctowned property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPCTOWNED(String value) {
                this.pctowned = value;
            }

            /**
             * Gets the value of the ownertype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOWNERTYPE() {
                return ownertype;
            }

            /**
             * Sets the value of the ownertype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOWNERTYPE(String value) {
                this.ownertype = value;
            }

            /**
             * Gets the value of the interflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERFLG() {
                return interflg;
            }

            /**
             * Sets the value of the interflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERFLG(String value) {
                this.interflg = value;
            }

            /**
             * Gets the value of the ivrflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRFLG() {
                return ivrflg;
            }

            /**
             * Sets the value of the ivrflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRFLG(String value) {
                this.ivrflg = value;
            }

            /**
             * Gets the value of the curintrate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURINTRATE() {
                return curintrate;
            }

            /**
             * Sets the value of the curintrate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURINTRATE(String value) {
                this.curintrate = value;
            }

            /**
             * Gets the value of the actstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTSTATUS() {
                return actstatus;
            }

            /**
             * Sets the value of the actstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTSTATUS(String value) {
                this.actstatus = value;
            }

            /**
             * Gets the value of the currbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURRBAL() {
                return currbal;
            }

            /**
             * Sets the value of the currbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURRBAL(String value) {
                this.currbal = value;
            }

            /**
             * Gets the value of the availbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAVAILBAL() {
                return availbal;
            }

            /**
             * Sets the value of the availbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAVAILBAL(String value) {
                this.availbal = value;
            }

            /**
             * Gets the value of the payoffamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPAYOFFAMT() {
                return payoffamt;
            }

            /**
             * Sets the value of the payoffamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPAYOFFAMT(String value) {
                this.payoffamt = value;
            }

            /**
             * Gets the value of the intpdytd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDYTD() {
                return intpdytd;
            }

            /**
             * Sets the value of the intpdytd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDYTD(String value) {
                this.intpdytd = value;
            }

            /**
             * Gets the value of the intpdlstyr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDLSTYR() {
                return intpdlstyr;
            }

            /**
             * Sets the value of the intpdlstyr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDLSTYR(String value) {
                this.intpdlstyr = value;
            }

            /**
             * Gets the value of the intaccrued property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTACCRUED() {
                return intaccrued;
            }

            /**
             * Sets the value of the intaccrued property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTACCRUED(String value) {
                this.intaccrued = value;
            }

            /**
             * Gets the value of the dtopened property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTOPENED() {
                return dtopened;
            }

            /**
             * Sets the value of the dtopened property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTOPENED(String value) {
                this.dtopened = value;
            }

            /**
             * Gets the value of the dtclosed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTCLOSED() {
                return dtclosed;
            }

            /**
             * Sets the value of the dtclosed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTCLOSED(String value) {
                this.dtclosed = value;
            }

            /**
             * Gets the value of the dtlststmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTLSTSTMT() {
                return dtlststmt;
            }

            /**
             * Sets the value of the dtlststmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTLSTSTMT(String value) {
                this.dtlststmt = value;
            }

            /**
             * Gets the value of the lstintamtp property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTINTAMTP() {
                return lstintamtp;
            }

            /**
             * Sets the value of the lstintamtp property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTINTAMTP(String value) {
                this.lstintamtp = value;
            }

            /**
             * Gets the value of the holdamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOLDAMT() {
                return holdamt;
            }

            /**
             * Sets the value of the holdamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOLDAMT(String value) {
                this.holdamt = value;
            }

            /**
             * Gets the value of the chkdeptdy property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCHKDEPTDY() {
                return chkdeptdy;
            }

            /**
             * Sets the value of the chkdeptdy property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCHKDEPTDY(String value) {
                this.chkdeptdy = value;
            }

            /**
             * Gets the value of the tbnkfltamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTBNKFLTAMT() {
                return tbnkfltamt;
            }

            /**
             * Sets the value of the tbnkfltamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTBNKFLTAMT(String value) {
                this.tbnkfltamt = value;
            }

            /**
             * Gets the value of the tcustfltam property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTCUSTFLTAM() {
                return tcustfltam;
            }

            /**
             * Sets the value of the tcustfltam property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTCUSTFLTAM(String value) {
                this.tcustfltam = value;
            }

            /**
             * Gets the value of the crlmtamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCRLMTAMT() {
                return crlmtamt;
            }

            /**
             * Sets the value of the crlmtamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCRLMTAMT(String value) {
                this.crlmtamt = value;
            }

            /**
             * Gets the value of the rrscurbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSCURBAL() {
                return rrscurbal;
            }

            /**
             * Sets the value of the rrscurbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSCURBAL(String value) {
                this.rrscurbal = value;
            }

            /**
             * Gets the value of the collbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCOLLBAL() {
                return collbal;
            }

            /**
             * Sets the value of the collbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCOLLBAL(String value) {
                this.collbal = value;
            }

            /**
             * Gets the value of the amtlstdep property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMTLSTDEP() {
                return amtlstdep;
            }

            /**
             * Sets the value of the amtlstdep property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMTLSTDEP(String value) {
                this.amtlstdep = value;
            }

            /**
             * Gets the value of the ctfavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCTFAVABAL() {
                return ctfavabal;
            }

            /**
             * Sets the value of the ctfavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCTFAVABAL(String value) {
                this.ctfavabal = value;
            }

            /**
             * Gets the value of the runcurbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRUNCURBAL() {
                return runcurbal;
            }

            /**
             * Sets the value of the runcurbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRUNCURBAL(String value) {
                this.runcurbal = value;
            }

            /**
             * Gets the value of the amount12 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT12() {
                return amount12;
            }

            /**
             * Sets the value of the amount12 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT12(String value) {
                this.amount12 = value;
            }

            /**
             * Gets the value of the lstoddt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTODDT() {
                return lstoddt;
            }

            /**
             * Sets the value of the lstoddt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTODDT(String value) {
                this.lstoddt = value;
            }

            /**
             * Gets the value of the pvbgstmdt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVBGSTMDT1() {
                return pvbgstmdt1;
            }

            /**
             * Sets the value of the pvbgstmdt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVBGSTMDT1(String value) {
                this.pvbgstmdt1 = value;
            }

            /**
             * Gets the value of the pvedstmdt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVEDSTMDT1() {
                return pvedstmdt1;
            }

            /**
             * Sets the value of the pvedstmdt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVEDSTMDT1(String value) {
                this.pvedstmdt1 = value;
            }

            /**
             * Gets the value of the pvbgstmdt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVBGSTMDT2() {
                return pvbgstmdt2;
            }

            /**
             * Sets the value of the pvbgstmdt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVBGSTMDT2(String value) {
                this.pvbgstmdt2 = value;
            }

            /**
             * Gets the value of the pvedstmdt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVEDSTMDT2() {
                return pvedstmdt2;
            }

            /**
             * Sets the value of the pvedstmdt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVEDSTMDT2(String value) {
                this.pvedstmdt2 = value;
            }

            /**
             * Gets the value of the pvbgstmdt3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVBGSTMDT3() {
                return pvbgstmdt3;
            }

            /**
             * Sets the value of the pvbgstmdt3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVBGSTMDT3(String value) {
                this.pvbgstmdt3 = value;
            }

            /**
             * Gets the value of the pvedstmdt3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVEDSTMDT3() {
                return pvedstmdt3;
            }

            /**
             * Sets the value of the pvedstmdt3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVEDSTMDT3(String value) {
                this.pvedstmdt3 = value;
            }

            /**
             * Gets the value of the pvbgstmdt4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPVBGSTMDT4() {
                return pvbgstmdt4;
            }

            /**
             * Sets the value of the pvbgstmdt4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPVBGSTMDT4(String value) {
                this.pvbgstmdt4 = value;
            }

            /**
             * Gets the value of the misc01 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMISC01() {
                return misc01;
            }

            /**
             * Sets the value of the misc01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMISC01(String value) {
                this.misc01 = value;
            }

            /**
             * Gets the value of the passbookcd property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPASSBOOKCD() {
                return passbookcd;
            }

            /**
             * Sets the value of the passbookcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPASSBOOKCD(Object value) {
                this.passbookcd = value;
            }

            /**
             * Gets the value of the stmtcyccd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTMTCYCCD() {
                return stmtcyccd;
            }

            /**
             * Sets the value of the stmtcyccd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTMTCYCCD(String value) {
                this.stmtcyccd = value;
            }

            /**
             * Gets the value of the future01 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getFUTURE01() {
                return future01;
            }

            /**
             * Sets the value of the future01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setFUTURE01(Object value) {
                this.future01 = value;
            }

            /**
             * Gets the value of the porttotflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPORTTOTFLG() {
                return porttotflg;
            }

            /**
             * Sets the value of the porttotflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPORTTOTFLG(Object value) {
                this.porttotflg = value;
            }

            /**
             * Gets the value of the pctalwdflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPCTALWDFLG() {
                return pctalwdflg;
            }

            /**
             * Sets the value of the pctalwdflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPCTALWDFLG(Object value) {
                this.pctalwdflg = value;
            }

            /**
             * Gets the value of the ocdstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCDSTATUS() {
                return ocdstatus;
            }

            /**
             * Sets the value of the ocdstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCDSTATUS(String value) {
                this.ocdstatus = value;
            }

            /**
             * Gets the value of the confflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCONFFLAG() {
                return confflag;
            }

            /**
             * Sets the value of the confflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCONFFLAG(String value) {
                this.confflag = value;
            }

            /**
             * Gets the value of the cashmgmt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT1() {
                return cashmgmt1;
            }

            /**
             * Sets the value of the cashmgmt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT1(String value) {
                this.cashmgmt1 = value;
            }

            /**
             * Gets the value of the cashmgmt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT2() {
                return cashmgmt2;
            }

            /**
             * Sets the value of the cashmgmt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT2(String value) {
                this.cashmgmt2 = value;
            }

            /**
             * Gets the value of the regdrstint property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGDRSTINT() {
                return regdrstint;
            }

            /**
             * Sets the value of the regdrstint property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGDRSTINT(String value) {
                this.regdrstint = value;
            }

            /**
             * Gets the value of the thirdparty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTHIRDPARTY() {
                return thirdparty;
            }

            /**
             * Sets the value of the thirdparty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTHIRDPARTY(String value) {
                this.thirdparty = value;
            }

            /**
             * Gets the value of the preauttrct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPREAUTTRCT() {
                return preauttrct;
            }

            /**
             * Sets the value of the preauttrct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPREAUTTRCT(String value) {
                this.preauttrct = value;
            }

            /**
             * Gets the value of the encoptstmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getENCOPTSTMT() {
                return encoptstmt;
            }

            /**
             * Sets the value of the encoptstmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setENCOPTSTMT(String value) {
                this.encoptstmt = value;
            }

            /**
             * Gets the value of the encoptntc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getENCOPTNTC() {
                return encoptntc;
            }

            /**
             * Sets the value of the encoptntc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setENCOPTNTC(String value) {
                this.encoptntc = value;
            }

            /**
             * Gets the value of the billpayflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBILLPAYFLG() {
                return billpayflg;
            }

            /**
             * Sets the value of the billpayflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBILLPAYFLG(Object value) {
                this.billpayflg = value;
            }

            /**
             * Gets the value of the lncrlimit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNCRLIMIT() {
                return lncrlimit;
            }

            /**
             * Sets the value of the lncrlimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNCRLIMIT(String value) {
                this.lncrlimit = value;
            }

            /**
             * Gets the value of the rrsselflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSSELFLG() {
                return rrsselflg;
            }

            /**
             * Sets the value of the rrsselflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSSELFLG(String value) {
                this.rrsselflg = value;
            }

            /**
             * Gets the value of the dyavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDYAVAILFLG() {
                return dyavailflg;
            }

            /**
             * Sets the value of the dyavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDYAVAILFLG(String value) {
                this.dyavailflg = value;
            }

            /**
             * Gets the value of the ddavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDAVAILFLG() {
                return ddavailflg;
            }

            /**
             * Sets the value of the ddavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDAVAILFLG(String value) {
                this.ddavailflg = value;
            }

            /**
             * Gets the value of the lnavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNAVAILFLG() {
                return lnavailflg;
            }

            /**
             * Sets the value of the lnavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNAVAILFLG(String value) {
                this.lnavailflg = value;
            }

            /**
             * Gets the value of the odlimitflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getODLIMITFLG() {
                return odlimitflg;
            }

            /**
             * Sets the value of the odlimitflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setODLIMITFLG(String value) {
                this.odlimitflg = value;
            }

            /**
             * Gets the value of the custfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCUSTFLTFLG() {
                return custfltflg;
            }

            /**
             * Sets the value of the custfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCUSTFLTFLG(String value) {
                this.custfltflg = value;
            }

            /**
             * Gets the value of the bnkfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBNKFLTFLG() {
                return bnkfltflg;
            }

            /**
             * Sets the value of the bnkfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBNKFLTFLG(String value) {
                this.bnkfltflg = value;
            }

            /**
             * Gets the value of the holdsflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOLDSFLG() {
                return holdsflg;
            }

            /**
             * Sets the value of the holdsflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOLDSFLG(String value) {
                this.holdsflg = value;
            }

            /**
             * Gets the value of the ckpdtdyflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCKPDTDYFLG() {
                return ckpdtdyflg;
            }

            /**
             * Sets the value of the ckpdtdyflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCKPDTDYFLG(String value) {
                this.ckpdtdyflg = value;
            }

            /**
             * Gets the value of the rrsavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSAVABAL() {
                return rrsavabal;
            }

            /**
             * Sets the value of the rrsavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSAVABAL(String value) {
                this.rrsavabal = value;
            }

            /**
             * Gets the value of the dynavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDYNAVABAL() {
                return dynavabal;
            }

            /**
             * Sets the value of the dynavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDYNAVABAL(String value) {
                this.dynavabal = value;
            }

            /**
             * Gets the value of the ddsvavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDSVAVABAL() {
                return ddsvavabal;
            }

            /**
             * Sets the value of the ddsvavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDSVAVABAL(String value) {
                this.ddsvavabal = value;
            }

            /**
             * Gets the value of the lnavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNAVABAL() {
                return lnavabal;
            }

            /**
             * Sets the value of the lnavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNAVABAL(String value) {
                this.lnavabal = value;
            }

            /**
             * Gets the value of the odlimitbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getODLIMITBAL() {
                return odlimitbal;
            }

            /**
             * Sets the value of the odlimitbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setODLIMITBAL(String value) {
                this.odlimitbal = value;
            }

            /**
             * Gets the value of the date09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE09() {
                return date09;
            }

            /**
             * Sets the value of the date09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE09(String value) {
                this.date09 = value;
            }

            /**
             * Gets the value of the date10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE10() {
                return date10;
            }

            /**
             * Sets the value of the date10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE10(String value) {
                this.date10 = value;
            }

            /**
             * Gets the value of the misc04 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC04() {
                return misc04;
            }

            /**
             * Sets the value of the misc04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC04(Object value) {
                this.misc04 = value;
            }

            /**
             * Gets the value of the misc05 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC05() {
                return misc05;
            }

            /**
             * Sets the value of the misc05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC05(Object value) {
                this.misc05 = value;
            }

            /**
             * Gets the value of the misc06 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC06() {
                return misc06;
            }

            /**
             * Sets the value of the misc06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC06(Object value) {
                this.misc06 = value;
            }

            /**
             * Gets the value of the misc07 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC07() {
                return misc07;
            }

            /**
             * Sets the value of the misc07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC07(Object value) {
                this.misc07 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SYSTEMTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TOTTRNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURESCRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PYMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPERDIEM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORGLNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CALCLNAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ESCROWAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LATECHGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PASTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CALCAMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="UNCLTCGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NXTDUEDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTPYMTDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTBILLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTRNWLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NXTDUEDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTPROCESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MSTACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRCHSYSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DYNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDSVAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LNAVABAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ODLIMITBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "undprelext",
            "groupcd",
            "groupseq",
            "applcd",
            "actdesc",
            "actnbr",
            "systemtype",
            "shortname",
            "officer",
            "branch",
            "reltype",
            "primaryflg",
            "txrspent",
            "pctowned",
            "ownertype",
            "interflg",
            "ivrflg",
            "curintrate",
            "actstatus",
            "currbal",
            "availbal",
            "payoffamt",
            "intpdytd",
            "intpdlstyr",
            "intaccrued",
            "dtopened",
            "dtclosed",
            "dtlststmt",
            "tottrnamt",
            "curescrbal",
            "pymtamt",
            "intperdiem",
            "orglnamt",
            "calclnamt",
            "escrowamt",
            "curpymtdue",
            "latechgdue",
            "pastdue",
            "calcamtdue",
            "uncltcgdue",
            "nxtduedt",
            "lstpymtdt",
            "curmtrydt",
            "lstbilldt",
            "lstrnwldt",
            "nxtduedt2",
            "dtprocess",
            "date08",
            "mstactnbr",
            "trchsysflg",
            "prodtype",
            "future01",
            "porttotflg",
            "pctalwdflg",
            "ocdstatus",
            "confflag",
            "cashmgmt1",
            "cashmgmt2",
            "regdrstint",
            "thirdparty",
            "preauttrct",
            "option1",
            "option2",
            "billpayflg",
            "amount13",
            "rrsselflg",
            "dyavailflg",
            "ddavailflg",
            "lnavailflg",
            "odlimitflg",
            "custfltflg",
            "bnkfltflg",
            "holdsflg",
            "ckpdtdyflg",
            "rrsavabal",
            "dynavabal",
            "ddsvavabal",
            "lnavabal",
            "odlimitbal",
            "date09",
            "date10",
            "misc04",
            "misc05",
            "misc06",
            "misc07"
        })
        public static class LOANACCOUNT {

            @XmlElement(name = "UNDPRELEXT", required = true)
            protected Object undprelext;
            @XmlElement(name = "GROUPCD", required = true)
            protected String groupcd;
            @XmlElement(name = "GROUPSEQ", required = true)
            protected String groupseq;
            @XmlElement(name = "APPLCD", required = true)
            protected String applcd;
            @XmlElement(name = "ACTDESC", required = true)
            protected String actdesc;
            @XmlElement(name = "ACTNBR", required = true)
            protected String actnbr;
            @XmlElement(name = "SYSTEMTYPE", required = true)
            protected String systemtype;
            @XmlElement(name = "SHORTNAME", required = true)
            protected String shortname;
            @XmlElement(name = "OFFICER", required = true)
            protected String officer;
            @XmlElement(name = "BRANCH", required = true)
            protected String branch;
            @XmlElement(name = "RELTYPE", required = true)
            protected String reltype;
            @XmlElement(name = "PRIMARYFLG", required = true)
            protected String primaryflg;
            @XmlElement(name = "TXRSPENT", required = true)
            protected String txrspent;
            @XmlElement(name = "PCTOWNED", required = true)
            protected String pctowned;
            @XmlElement(name = "OWNERTYPE", required = true)
            protected String ownertype;
            @XmlElement(name = "INTERFLG", required = true)
            protected String interflg;
            @XmlElement(name = "IVRFLG", required = true)
            protected String ivrflg;
            @XmlElement(name = "CURINTRATE", required = true)
            protected String curintrate;
            @XmlElement(name = "ACTSTATUS", required = true)
            protected String actstatus;
            @XmlElement(name = "CURRBAL", required = true)
            protected String currbal;
            @XmlElement(name = "AVAILBAL", required = true)
            protected String availbal;
            @XmlElement(name = "PAYOFFAMT", required = true)
            protected String payoffamt;
            @XmlElement(name = "INTPDYTD", required = true)
            protected String intpdytd;
            @XmlElement(name = "INTPDLSTYR", required = true)
            protected String intpdlstyr;
            @XmlElement(name = "INTACCRUED", required = true)
            protected String intaccrued;
            @XmlElement(name = "DTOPENED", required = true)
            protected String dtopened;
            @XmlElement(name = "DTCLOSED", required = true)
            protected String dtclosed;
            @XmlElement(name = "DTLSTSTMT", required = true)
            protected String dtlststmt;
            @XmlElement(name = "TOTTRNAMT", required = true)
            protected String tottrnamt;
            @XmlElement(name = "CURESCRBAL", required = true)
            protected String curescrbal;
            @XmlElement(name = "PYMTAMT", required = true)
            protected String pymtamt;
            @XmlElement(name = "INTPERDIEM", required = true)
            protected String intperdiem;
            @XmlElement(name = "ORGLNAMT", required = true)
            protected String orglnamt;
            @XmlElement(name = "CALCLNAMT", required = true)
            protected String calclnamt;
            @XmlElement(name = "ESCROWAMT", required = true)
            protected String escrowamt;
            @XmlElement(name = "CURPYMTDUE", required = true)
            protected String curpymtdue;
            @XmlElement(name = "LATECHGDUE", required = true)
            protected String latechgdue;
            @XmlElement(name = "PASTDUE", required = true)
            protected String pastdue;
            @XmlElement(name = "CALCAMTDUE", required = true)
            protected String calcamtdue;
            @XmlElement(name = "UNCLTCGDUE", required = true)
            protected String uncltcgdue;
            @XmlElement(name = "NXTDUEDT", required = true)
            protected String nxtduedt;
            @XmlElement(name = "LSTPYMTDT", required = true)
            protected String lstpymtdt;
            @XmlElement(name = "CURMTRYDT", required = true)
            protected String curmtrydt;
            @XmlElement(name = "LSTBILLDT", required = true)
            protected String lstbilldt;
            @XmlElement(name = "LSTRNWLDT", required = true)
            protected String lstrnwldt;
            @XmlElement(name = "NXTDUEDT2", required = true)
            protected String nxtduedt2;
            @XmlElement(name = "DTPROCESS", required = true)
            protected String dtprocess;
            @XmlElement(name = "DATE08", required = true)
            protected String date08;
            @XmlElement(name = "MSTACTNBR", required = true)
            protected String mstactnbr;
            @XmlElement(name = "TRCHSYSFLG", required = true)
            protected String trchsysflg;
            @XmlElement(name = "PRODTYPE", required = true)
            protected String prodtype;
            @XmlElement(name = "FUTURE01", required = true)
            protected Object future01;
            @XmlElement(name = "PORTTOTFLG", required = true)
            protected Object porttotflg;
            @XmlElement(name = "PCTALWDFLG", required = true)
            protected Object pctalwdflg;
            @XmlElement(name = "OCDSTATUS", required = true)
            protected String ocdstatus;
            @XmlElement(name = "CONFFLAG", required = true)
            protected String confflag;
            @XmlElement(name = "CASHMGMT1", required = true)
            protected String cashmgmt1;
            @XmlElement(name = "CASHMGMT2", required = true)
            protected String cashmgmt2;
            @XmlElement(name = "REGDRSTINT", required = true)
            protected Object regdrstint;
            @XmlElement(name = "THIRDPARTY", required = true)
            protected String thirdparty;
            @XmlElement(name = "PREAUTTRCT", required = true)
            protected String preauttrct;
            @XmlElement(name = "OPTION1", required = true)
            protected Object option1;
            @XmlElement(name = "OPTION2", required = true)
            protected Object option2;
            @XmlElement(name = "BILLPAYFLG", required = true)
            protected Object billpayflg;
            @XmlElement(name = "AMOUNT13", required = true)
            protected String amount13;
            @XmlElement(name = "RRSSELFLG", required = true)
            protected String rrsselflg;
            @XmlElement(name = "DYAVAILFLG", required = true)
            protected String dyavailflg;
            @XmlElement(name = "DDAVAILFLG", required = true)
            protected String ddavailflg;
            @XmlElement(name = "LNAVAILFLG", required = true)
            protected String lnavailflg;
            @XmlElement(name = "ODLIMITFLG", required = true)
            protected String odlimitflg;
            @XmlElement(name = "CUSTFLTFLG", required = true)
            protected String custfltflg;
            @XmlElement(name = "BNKFLTFLG", required = true)
            protected String bnkfltflg;
            @XmlElement(name = "HOLDSFLG", required = true)
            protected String holdsflg;
            @XmlElement(name = "CKPDTDYFLG", required = true)
            protected String ckpdtdyflg;
            @XmlElement(name = "RRSAVABAL", required = true)
            protected String rrsavabal;
            @XmlElement(name = "DYNAVABAL", required = true)
            protected String dynavabal;
            @XmlElement(name = "DDSVAVABAL", required = true)
            protected String ddsvavabal;
            @XmlElement(name = "LNAVABAL", required = true)
            protected String lnavabal;
            @XmlElement(name = "ODLIMITBAL", required = true)
            protected String odlimitbal;
            @XmlElement(name = "DATE09", required = true)
            protected String date09;
            @XmlElement(name = "DATE10", required = true)
            protected String date10;
            @XmlElement(name = "MISC04", required = true)
            protected Object misc04;
            @XmlElement(name = "MISC05", required = true)
            protected Object misc05;
            @XmlElement(name = "MISC06", required = true)
            protected Object misc06;
            @XmlElement(name = "MISC07", required = true)
            protected Object misc07;

            /**
             * Gets the value of the undprelext property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getUNDPRELEXT() {
                return undprelext;
            }

            /**
             * Sets the value of the undprelext property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setUNDPRELEXT(Object value) {
                this.undprelext = value;
            }

            /**
             * Gets the value of the groupcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPCD() {
                return groupcd;
            }

            /**
             * Sets the value of the groupcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPCD(String value) {
                this.groupcd = value;
            }

            /**
             * Gets the value of the groupseq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPSEQ() {
                return groupseq;
            }

            /**
             * Sets the value of the groupseq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPSEQ(String value) {
                this.groupseq = value;
            }

            /**
             * Gets the value of the applcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAPPLCD() {
                return applcd;
            }

            /**
             * Sets the value of the applcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAPPLCD(String value) {
                this.applcd = value;
            }

            /**
             * Gets the value of the actdesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTDESC() {
                return actdesc;
            }

            /**
             * Sets the value of the actdesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTDESC(String value) {
                this.actdesc = value;
            }

            /**
             * Gets the value of the actnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTNBR() {
                return actnbr;
            }

            /**
             * Sets the value of the actnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTNBR(String value) {
                this.actnbr = value;
            }

            /**
             * Gets the value of the systemtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSYSTEMTYPE() {
                return systemtype;
            }

            /**
             * Sets the value of the systemtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSYSTEMTYPE(String value) {
                this.systemtype = value;
            }

            /**
             * Gets the value of the shortname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSHORTNAME() {
                return shortname;
            }

            /**
             * Sets the value of the shortname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSHORTNAME(String value) {
                this.shortname = value;
            }

            /**
             * Gets the value of the officer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOFFICER() {
                return officer;
            }

            /**
             * Sets the value of the officer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOFFICER(String value) {
                this.officer = value;
            }

            /**
             * Gets the value of the branch property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBRANCH() {
                return branch;
            }

            /**
             * Sets the value of the branch property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBRANCH(String value) {
                this.branch = value;
            }

            /**
             * Gets the value of the reltype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRELTYPE() {
                return reltype;
            }

            /**
             * Sets the value of the reltype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRELTYPE(String value) {
                this.reltype = value;
            }

            /**
             * Gets the value of the primaryflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRIMARYFLG() {
                return primaryflg;
            }

            /**
             * Sets the value of the primaryflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRIMARYFLG(String value) {
                this.primaryflg = value;
            }

            /**
             * Gets the value of the txrspent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTXRSPENT() {
                return txrspent;
            }

            /**
             * Sets the value of the txrspent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTXRSPENT(String value) {
                this.txrspent = value;
            }

            /**
             * Gets the value of the pctowned property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPCTOWNED() {
                return pctowned;
            }

            /**
             * Sets the value of the pctowned property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPCTOWNED(String value) {
                this.pctowned = value;
            }

            /**
             * Gets the value of the ownertype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOWNERTYPE() {
                return ownertype;
            }

            /**
             * Sets the value of the ownertype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOWNERTYPE(String value) {
                this.ownertype = value;
            }

            /**
             * Gets the value of the interflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERFLG() {
                return interflg;
            }

            /**
             * Sets the value of the interflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERFLG(String value) {
                this.interflg = value;
            }

            /**
             * Gets the value of the ivrflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRFLG() {
                return ivrflg;
            }

            /**
             * Sets the value of the ivrflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRFLG(String value) {
                this.ivrflg = value;
            }

            /**
             * Gets the value of the curintrate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURINTRATE() {
                return curintrate;
            }

            /**
             * Sets the value of the curintrate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURINTRATE(String value) {
                this.curintrate = value;
            }

            /**
             * Gets the value of the actstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTSTATUS() {
                return actstatus;
            }

            /**
             * Sets the value of the actstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTSTATUS(String value) {
                this.actstatus = value;
            }

            /**
             * Gets the value of the currbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURRBAL() {
                return currbal;
            }

            /**
             * Sets the value of the currbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURRBAL(String value) {
                this.currbal = value;
            }

            /**
             * Gets the value of the availbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAVAILBAL() {
                return availbal;
            }

            /**
             * Sets the value of the availbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAVAILBAL(String value) {
                this.availbal = value;
            }

            /**
             * Gets the value of the payoffamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPAYOFFAMT() {
                return payoffamt;
            }

            /**
             * Sets the value of the payoffamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPAYOFFAMT(String value) {
                this.payoffamt = value;
            }

            /**
             * Gets the value of the intpdytd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDYTD() {
                return intpdytd;
            }

            /**
             * Sets the value of the intpdytd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDYTD(String value) {
                this.intpdytd = value;
            }

            /**
             * Gets the value of the intpdlstyr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDLSTYR() {
                return intpdlstyr;
            }

            /**
             * Sets the value of the intpdlstyr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDLSTYR(String value) {
                this.intpdlstyr = value;
            }

            /**
             * Gets the value of the intaccrued property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTACCRUED() {
                return intaccrued;
            }

            /**
             * Sets the value of the intaccrued property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTACCRUED(String value) {
                this.intaccrued = value;
            }

            /**
             * Gets the value of the dtopened property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTOPENED() {
                return dtopened;
            }

            /**
             * Sets the value of the dtopened property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTOPENED(String value) {
                this.dtopened = value;
            }

            /**
             * Gets the value of the dtclosed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTCLOSED() {
                return dtclosed;
            }

            /**
             * Sets the value of the dtclosed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTCLOSED(String value) {
                this.dtclosed = value;
            }

            /**
             * Gets the value of the dtlststmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTLSTSTMT() {
                return dtlststmt;
            }

            /**
             * Sets the value of the dtlststmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTLSTSTMT(String value) {
                this.dtlststmt = value;
            }

            /**
             * Gets the value of the tottrnamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTOTTRNAMT() {
                return tottrnamt;
            }

            /**
             * Sets the value of the tottrnamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTOTTRNAMT(String value) {
                this.tottrnamt = value;
            }

            /**
             * Gets the value of the curescrbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURESCRBAL() {
                return curescrbal;
            }

            /**
             * Sets the value of the curescrbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURESCRBAL(String value) {
                this.curescrbal = value;
            }

            /**
             * Gets the value of the pymtamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPYMTAMT() {
                return pymtamt;
            }

            /**
             * Sets the value of the pymtamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPYMTAMT(String value) {
                this.pymtamt = value;
            }

            /**
             * Gets the value of the intperdiem property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPERDIEM() {
                return intperdiem;
            }

            /**
             * Sets the value of the intperdiem property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPERDIEM(String value) {
                this.intperdiem = value;
            }

            /**
             * Gets the value of the orglnamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORGLNAMT() {
                return orglnamt;
            }

            /**
             * Sets the value of the orglnamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORGLNAMT(String value) {
                this.orglnamt = value;
            }

            /**
             * Gets the value of the calclnamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCALCLNAMT() {
                return calclnamt;
            }

            /**
             * Sets the value of the calclnamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCALCLNAMT(String value) {
                this.calclnamt = value;
            }

            /**
             * Gets the value of the escrowamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getESCROWAMT() {
                return escrowamt;
            }

            /**
             * Sets the value of the escrowamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setESCROWAMT(String value) {
                this.escrowamt = value;
            }

            /**
             * Gets the value of the curpymtdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURPYMTDUE() {
                return curpymtdue;
            }

            /**
             * Sets the value of the curpymtdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURPYMTDUE(String value) {
                this.curpymtdue = value;
            }

            /**
             * Gets the value of the latechgdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLATECHGDUE() {
                return latechgdue;
            }

            /**
             * Sets the value of the latechgdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLATECHGDUE(String value) {
                this.latechgdue = value;
            }

            /**
             * Gets the value of the pastdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPASTDUE() {
                return pastdue;
            }

            /**
             * Sets the value of the pastdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPASTDUE(String value) {
                this.pastdue = value;
            }

            /**
             * Gets the value of the calcamtdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCALCAMTDUE() {
                return calcamtdue;
            }

            /**
             * Sets the value of the calcamtdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCALCAMTDUE(String value) {
                this.calcamtdue = value;
            }

            /**
             * Gets the value of the uncltcgdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUNCLTCGDUE() {
                return uncltcgdue;
            }

            /**
             * Sets the value of the uncltcgdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUNCLTCGDUE(String value) {
                this.uncltcgdue = value;
            }

            /**
             * Gets the value of the nxtduedt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNXTDUEDT() {
                return nxtduedt;
            }

            /**
             * Sets the value of the nxtduedt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNXTDUEDT(String value) {
                this.nxtduedt = value;
            }

            /**
             * Gets the value of the lstpymtdt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTPYMTDT() {
                return lstpymtdt;
            }

            /**
             * Sets the value of the lstpymtdt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTPYMTDT(String value) {
                this.lstpymtdt = value;
            }

            /**
             * Gets the value of the curmtrydt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURMTRYDT() {
                return curmtrydt;
            }

            /**
             * Sets the value of the curmtrydt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURMTRYDT(String value) {
                this.curmtrydt = value;
            }

            /**
             * Gets the value of the lstbilldt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTBILLDT() {
                return lstbilldt;
            }

            /**
             * Sets the value of the lstbilldt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTBILLDT(String value) {
                this.lstbilldt = value;
            }

            /**
             * Gets the value of the lstrnwldt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTRNWLDT() {
                return lstrnwldt;
            }

            /**
             * Sets the value of the lstrnwldt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTRNWLDT(String value) {
                this.lstrnwldt = value;
            }

            /**
             * Gets the value of the nxtduedt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNXTDUEDT2() {
                return nxtduedt2;
            }

            /**
             * Sets the value of the nxtduedt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNXTDUEDT2(String value) {
                this.nxtduedt2 = value;
            }

            /**
             * Gets the value of the dtprocess property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTPROCESS() {
                return dtprocess;
            }

            /**
             * Sets the value of the dtprocess property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTPROCESS(String value) {
                this.dtprocess = value;
            }

            /**
             * Gets the value of the date08 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE08() {
                return date08;
            }

            /**
             * Sets the value of the date08 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE08(String value) {
                this.date08 = value;
            }

            /**
             * Gets the value of the mstactnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMSTACTNBR() {
                return mstactnbr;
            }

            /**
             * Sets the value of the mstactnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMSTACTNBR(String value) {
                this.mstactnbr = value;
            }

            /**
             * Gets the value of the trchsysflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRCHSYSFLG() {
                return trchsysflg;
            }

            /**
             * Sets the value of the trchsysflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRCHSYSFLG(String value) {
                this.trchsysflg = value;
            }

            /**
             * Gets the value of the prodtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRODTYPE() {
                return prodtype;
            }

            /**
             * Sets the value of the prodtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRODTYPE(String value) {
                this.prodtype = value;
            }

            /**
             * Gets the value of the future01 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getFUTURE01() {
                return future01;
            }

            /**
             * Sets the value of the future01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setFUTURE01(Object value) {
                this.future01 = value;
            }

            /**
             * Gets the value of the porttotflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPORTTOTFLG() {
                return porttotflg;
            }

            /**
             * Sets the value of the porttotflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPORTTOTFLG(Object value) {
                this.porttotflg = value;
            }

            /**
             * Gets the value of the pctalwdflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPCTALWDFLG() {
                return pctalwdflg;
            }

            /**
             * Sets the value of the pctalwdflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPCTALWDFLG(Object value) {
                this.pctalwdflg = value;
            }

            /**
             * Gets the value of the ocdstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCDSTATUS() {
                return ocdstatus;
            }

            /**
             * Sets the value of the ocdstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCDSTATUS(String value) {
                this.ocdstatus = value;
            }

            /**
             * Gets the value of the confflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCONFFLAG() {
                return confflag;
            }

            /**
             * Sets the value of the confflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCONFFLAG(String value) {
                this.confflag = value;
            }

            /**
             * Gets the value of the cashmgmt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT1() {
                return cashmgmt1;
            }

            /**
             * Sets the value of the cashmgmt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT1(String value) {
                this.cashmgmt1 = value;
            }

            /**
             * Gets the value of the cashmgmt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT2() {
                return cashmgmt2;
            }

            /**
             * Sets the value of the cashmgmt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT2(String value) {
                this.cashmgmt2 = value;
            }

            /**
             * Gets the value of the regdrstint property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getREGDRSTINT() {
                return regdrstint;
            }

            /**
             * Sets the value of the regdrstint property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setREGDRSTINT(Object value) {
                this.regdrstint = value;
            }

            /**
             * Gets the value of the thirdparty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTHIRDPARTY() {
                return thirdparty;
            }

            /**
             * Sets the value of the thirdparty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTHIRDPARTY(String value) {
                this.thirdparty = value;
            }

            /**
             * Gets the value of the preauttrct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPREAUTTRCT() {
                return preauttrct;
            }

            /**
             * Sets the value of the preauttrct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPREAUTTRCT(String value) {
                this.preauttrct = value;
            }

            /**
             * Gets the value of the option1 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION1() {
                return option1;
            }

            /**
             * Sets the value of the option1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION1(Object value) {
                this.option1 = value;
            }

            /**
             * Gets the value of the option2 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION2() {
                return option2;
            }

            /**
             * Sets the value of the option2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION2(Object value) {
                this.option2 = value;
            }

            /**
             * Gets the value of the billpayflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBILLPAYFLG() {
                return billpayflg;
            }

            /**
             * Sets the value of the billpayflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBILLPAYFLG(Object value) {
                this.billpayflg = value;
            }

            /**
             * Gets the value of the amount13 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT13() {
                return amount13;
            }

            /**
             * Sets the value of the amount13 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT13(String value) {
                this.amount13 = value;
            }

            /**
             * Gets the value of the rrsselflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSSELFLG() {
                return rrsselflg;
            }

            /**
             * Sets the value of the rrsselflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSSELFLG(String value) {
                this.rrsselflg = value;
            }

            /**
             * Gets the value of the dyavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDYAVAILFLG() {
                return dyavailflg;
            }

            /**
             * Sets the value of the dyavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDYAVAILFLG(String value) {
                this.dyavailflg = value;
            }

            /**
             * Gets the value of the ddavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDAVAILFLG() {
                return ddavailflg;
            }

            /**
             * Sets the value of the ddavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDAVAILFLG(String value) {
                this.ddavailflg = value;
            }

            /**
             * Gets the value of the lnavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNAVAILFLG() {
                return lnavailflg;
            }

            /**
             * Sets the value of the lnavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNAVAILFLG(String value) {
                this.lnavailflg = value;
            }

            /**
             * Gets the value of the odlimitflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getODLIMITFLG() {
                return odlimitflg;
            }

            /**
             * Sets the value of the odlimitflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setODLIMITFLG(String value) {
                this.odlimitflg = value;
            }

            /**
             * Gets the value of the custfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCUSTFLTFLG() {
                return custfltflg;
            }

            /**
             * Sets the value of the custfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCUSTFLTFLG(String value) {
                this.custfltflg = value;
            }

            /**
             * Gets the value of the bnkfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBNKFLTFLG() {
                return bnkfltflg;
            }

            /**
             * Sets the value of the bnkfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBNKFLTFLG(String value) {
                this.bnkfltflg = value;
            }

            /**
             * Gets the value of the holdsflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOLDSFLG() {
                return holdsflg;
            }

            /**
             * Sets the value of the holdsflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOLDSFLG(String value) {
                this.holdsflg = value;
            }

            /**
             * Gets the value of the ckpdtdyflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCKPDTDYFLG() {
                return ckpdtdyflg;
            }

            /**
             * Sets the value of the ckpdtdyflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCKPDTDYFLG(String value) {
                this.ckpdtdyflg = value;
            }

            /**
             * Gets the value of the rrsavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSAVABAL() {
                return rrsavabal;
            }

            /**
             * Sets the value of the rrsavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSAVABAL(String value) {
                this.rrsavabal = value;
            }

            /**
             * Gets the value of the dynavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDYNAVABAL() {
                return dynavabal;
            }

            /**
             * Sets the value of the dynavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDYNAVABAL(String value) {
                this.dynavabal = value;
            }

            /**
             * Gets the value of the ddsvavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDSVAVABAL() {
                return ddsvavabal;
            }

            /**
             * Sets the value of the ddsvavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDSVAVABAL(String value) {
                this.ddsvavabal = value;
            }

            /**
             * Gets the value of the lnavabal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNAVABAL() {
                return lnavabal;
            }

            /**
             * Sets the value of the lnavabal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNAVABAL(String value) {
                this.lnavabal = value;
            }

            /**
             * Gets the value of the odlimitbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getODLIMITBAL() {
                return odlimitbal;
            }

            /**
             * Sets the value of the odlimitbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setODLIMITBAL(String value) {
                this.odlimitbal = value;
            }

            /**
             * Gets the value of the date09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE09() {
                return date09;
            }

            /**
             * Sets the value of the date09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE09(String value) {
                this.date09 = value;
            }

            /**
             * Gets the value of the date10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE10() {
                return date10;
            }

            /**
             * Sets the value of the date10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE10(String value) {
                this.date10 = value;
            }

            /**
             * Gets the value of the misc04 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC04() {
                return misc04;
            }

            /**
             * Sets the value of the misc04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC04(Object value) {
                this.misc04 = value;
            }

            /**
             * Gets the value of the misc05 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC05() {
                return misc05;
            }

            /**
             * Sets the value of the misc05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC05(Object value) {
                this.misc05 = value;
            }

            /**
             * Gets the value of the misc06 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC06() {
                return misc06;
            }

            /**
             * Sets the value of the misc06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC06(Object value) {
                this.misc06 = value;
            }

            /**
             * Gets the value of the misc07 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC07() {
                return misc07;
            }

            /**
             * Sets the value of the misc07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC07(Object value) {
                this.misc07 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SYSTEMTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BORHSTPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ESCROWBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="YTDTAXPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PREVYRTXPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="YTDESCINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PIPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REMESCAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURPYMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LATECHGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TOTPSTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TOTAMTDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="UNCLTCGDUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NXTSCHPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTPYMTDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTBILLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE05" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURPYMTDT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURPYMTDT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE08" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC02" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ORGPRINBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "undprelext",
            "groupcd",
            "groupseq",
            "applcd",
            "actdesc",
            "actnbr",
            "systemtype",
            "shortname",
            "officer",
            "branch",
            "reltype",
            "primaryflg",
            "txrspent",
            "pctowned",
            "ownertype",
            "interflg",
            "ivrflg",
            "curintrate",
            "actstatus",
            "currbal",
            "availbal",
            "payoffamt",
            "intpdytd",
            "intpdlstyr",
            "intaccrued",
            "dtopened",
            "dtclosed",
            "dtlststmt",
            "borhstpymt",
            "escrowbal",
            "ytdtaxpd",
            "prevyrtxpd",
            "ytdescint",
            "pipymtdue",
            "remescamt",
            "curpymtdue",
            "latechgdue",
            "totpstdue",
            "totamtdue",
            "uncltcgdue",
            "nxtschpymt",
            "lstpymtdt",
            "curmtrydt",
            "lstbilldt",
            "date05",
            "curpymtdt1",
            "curpymtdt2",
            "date08",
            "misc01",
            "misc02",
            "misc03",
            "future01",
            "porttotflg",
            "pctalwdflg",
            "ocdstatus",
            "confflag",
            "cashmgmt1",
            "cashmgmt2",
            "regdrstint",
            "thirdparty",
            "preauttrct",
            "option1",
            "option2",
            "billpayflg",
            "orgprinbal",
            "rrsselflg",
            "dyavailflg",
            "ddavailflg",
            "lnavailflg",
            "odlimitflg",
            "custfltflg",
            "bnkfltflg",
            "holdsflg",
            "ckpdtdyflg",
            "amount14",
            "amount15",
            "amount16",
            "amount17",
            "amount18",
            "date09",
            "date10",
            "misc04",
            "misc05",
            "misc06",
            "misc07"
        })
        public static class MTGACCOUNT {

            @XmlElement(name = "UNDPRELEXT", required = true)
            protected Object undprelext;
            @XmlElement(name = "GROUPCD", required = true)
            protected String groupcd;
            @XmlElement(name = "GROUPSEQ", required = true)
            protected String groupseq;
            @XmlElement(name = "APPLCD", required = true)
            protected String applcd;
            @XmlElement(name = "ACTDESC", required = true)
            protected String actdesc;
            @XmlElement(name = "ACTNBR", required = true)
            protected String actnbr;
            @XmlElement(name = "SYSTEMTYPE", required = true)
            protected String systemtype;
            @XmlElement(name = "SHORTNAME", required = true)
            protected String shortname;
            @XmlElement(name = "OFFICER", required = true)
            protected String officer;
            @XmlElement(name = "BRANCH", required = true)
            protected String branch;
            @XmlElement(name = "RELTYPE", required = true)
            protected String reltype;
            @XmlElement(name = "PRIMARYFLG", required = true)
            protected String primaryflg;
            @XmlElement(name = "TXRSPENT", required = true)
            protected String txrspent;
            @XmlElement(name = "PCTOWNED", required = true)
            protected String pctowned;
            @XmlElement(name = "OWNERTYPE", required = true)
            protected String ownertype;
            @XmlElement(name = "INTERFLG", required = true)
            protected String interflg;
            @XmlElement(name = "IVRFLG", required = true)
            protected String ivrflg;
            @XmlElement(name = "CURINTRATE", required = true)
            protected String curintrate;
            @XmlElement(name = "ACTSTATUS", required = true)
            protected String actstatus;
            @XmlElement(name = "CURRBAL", required = true)
            protected String currbal;
            @XmlElement(name = "AVAILBAL", required = true)
            protected String availbal;
            @XmlElement(name = "PAYOFFAMT", required = true)
            protected String payoffamt;
            @XmlElement(name = "INTPDYTD", required = true)
            protected String intpdytd;
            @XmlElement(name = "INTPDLSTYR", required = true)
            protected String intpdlstyr;
            @XmlElement(name = "INTACCRUED", required = true)
            protected String intaccrued;
            @XmlElement(name = "DTOPENED", required = true)
            protected String dtopened;
            @XmlElement(name = "DTCLOSED", required = true)
            protected String dtclosed;
            @XmlElement(name = "DTLSTSTMT", required = true)
            protected String dtlststmt;
            @XmlElement(name = "BORHSTPYMT", required = true)
            protected String borhstpymt;
            @XmlElement(name = "ESCROWBAL", required = true)
            protected String escrowbal;
            @XmlElement(name = "YTDTAXPD", required = true)
            protected String ytdtaxpd;
            @XmlElement(name = "PREVYRTXPD", required = true)
            protected String prevyrtxpd;
            @XmlElement(name = "YTDESCINT", required = true)
            protected String ytdescint;
            @XmlElement(name = "PIPYMTDUE", required = true)
            protected String pipymtdue;
            @XmlElement(name = "REMESCAMT", required = true)
            protected String remescamt;
            @XmlElement(name = "CURPYMTDUE", required = true)
            protected String curpymtdue;
            @XmlElement(name = "LATECHGDUE", required = true)
            protected String latechgdue;
            @XmlElement(name = "TOTPSTDUE", required = true)
            protected String totpstdue;
            @XmlElement(name = "TOTAMTDUE", required = true)
            protected String totamtdue;
            @XmlElement(name = "UNCLTCGDUE", required = true)
            protected String uncltcgdue;
            @XmlElement(name = "NXTSCHPYMT", required = true)
            protected String nxtschpymt;
            @XmlElement(name = "LSTPYMTDT", required = true)
            protected String lstpymtdt;
            @XmlElement(name = "CURMTRYDT", required = true)
            protected String curmtrydt;
            @XmlElement(name = "LSTBILLDT", required = true)
            protected String lstbilldt;
            @XmlElement(name = "DATE05", required = true)
            protected String date05;
            @XmlElement(name = "CURPYMTDT1", required = true)
            protected String curpymtdt1;
            @XmlElement(name = "CURPYMTDT2", required = true)
            protected String curpymtdt2;
            @XmlElement(name = "DATE08", required = true)
            protected String date08;
            @XmlElement(name = "MISC01", required = true)
            protected String misc01;
            @XmlElement(name = "MISC02", required = true)
            protected Object misc02;
            @XmlElement(name = "MISC03", required = true)
            protected Object misc03;
            @XmlElement(name = "FUTURE01", required = true)
            protected Object future01;
            @XmlElement(name = "PORTTOTFLG", required = true)
            protected Object porttotflg;
            @XmlElement(name = "PCTALWDFLG", required = true)
            protected Object pctalwdflg;
            @XmlElement(name = "OCDSTATUS", required = true)
            protected String ocdstatus;
            @XmlElement(name = "CONFFLAG", required = true)
            protected String confflag;
            @XmlElement(name = "CASHMGMT1", required = true)
            protected String cashmgmt1;
            @XmlElement(name = "CASHMGMT2", required = true)
            protected String cashmgmt2;
            @XmlElement(name = "REGDRSTINT", required = true)
            protected Object regdrstint;
            @XmlElement(name = "THIRDPARTY", required = true)
            protected String thirdparty;
            @XmlElement(name = "PREAUTTRCT", required = true)
            protected String preauttrct;
            @XmlElement(name = "OPTION1", required = true)
            protected Object option1;
            @XmlElement(name = "OPTION2", required = true)
            protected Object option2;
            @XmlElement(name = "BILLPAYFLG", required = true)
            protected Object billpayflg;
            @XmlElement(name = "ORGPRINBAL", required = true)
            protected String orgprinbal;
            @XmlElement(name = "RRSSELFLG", required = true)
            protected String rrsselflg;
            @XmlElement(name = "DYAVAILFLG", required = true)
            protected String dyavailflg;
            @XmlElement(name = "DDAVAILFLG", required = true)
            protected String ddavailflg;
            @XmlElement(name = "LNAVAILFLG", required = true)
            protected String lnavailflg;
            @XmlElement(name = "ODLIMITFLG", required = true)
            protected String odlimitflg;
            @XmlElement(name = "CUSTFLTFLG", required = true)
            protected String custfltflg;
            @XmlElement(name = "BNKFLTFLG", required = true)
            protected String bnkfltflg;
            @XmlElement(name = "HOLDSFLG", required = true)
            protected String holdsflg;
            @XmlElement(name = "CKPDTDYFLG", required = true)
            protected String ckpdtdyflg;
            @XmlElement(name = "AMOUNT14", required = true)
            protected String amount14;
            @XmlElement(name = "AMOUNT15", required = true)
            protected String amount15;
            @XmlElement(name = "AMOUNT16", required = true)
            protected String amount16;
            @XmlElement(name = "AMOUNT17", required = true)
            protected String amount17;
            @XmlElement(name = "AMOUNT18", required = true)
            protected String amount18;
            @XmlElement(name = "DATE09", required = true)
            protected String date09;
            @XmlElement(name = "DATE10", required = true)
            protected String date10;
            @XmlElement(name = "MISC04", required = true)
            protected Object misc04;
            @XmlElement(name = "MISC05", required = true)
            protected Object misc05;
            @XmlElement(name = "MISC06", required = true)
            protected Object misc06;
            @XmlElement(name = "MISC07", required = true)
            protected Object misc07;

            /**
             * Gets the value of the undprelext property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getUNDPRELEXT() {
                return undprelext;
            }

            /**
             * Sets the value of the undprelext property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setUNDPRELEXT(Object value) {
                this.undprelext = value;
            }

            /**
             * Gets the value of the groupcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPCD() {
                return groupcd;
            }

            /**
             * Sets the value of the groupcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPCD(String value) {
                this.groupcd = value;
            }

            /**
             * Gets the value of the groupseq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPSEQ() {
                return groupseq;
            }

            /**
             * Sets the value of the groupseq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPSEQ(String value) {
                this.groupseq = value;
            }

            /**
             * Gets the value of the applcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAPPLCD() {
                return applcd;
            }

            /**
             * Sets the value of the applcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAPPLCD(String value) {
                this.applcd = value;
            }

            /**
             * Gets the value of the actdesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTDESC() {
                return actdesc;
            }

            /**
             * Sets the value of the actdesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTDESC(String value) {
                this.actdesc = value;
            }

            /**
             * Gets the value of the actnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTNBR() {
                return actnbr;
            }

            /**
             * Sets the value of the actnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTNBR(String value) {
                this.actnbr = value;
            }

            /**
             * Gets the value of the systemtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSYSTEMTYPE() {
                return systemtype;
            }

            /**
             * Sets the value of the systemtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSYSTEMTYPE(String value) {
                this.systemtype = value;
            }

            /**
             * Gets the value of the shortname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSHORTNAME() {
                return shortname;
            }

            /**
             * Sets the value of the shortname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSHORTNAME(String value) {
                this.shortname = value;
            }

            /**
             * Gets the value of the officer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOFFICER() {
                return officer;
            }

            /**
             * Sets the value of the officer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOFFICER(String value) {
                this.officer = value;
            }

            /**
             * Gets the value of the branch property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBRANCH() {
                return branch;
            }

            /**
             * Sets the value of the branch property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBRANCH(String value) {
                this.branch = value;
            }

            /**
             * Gets the value of the reltype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRELTYPE() {
                return reltype;
            }

            /**
             * Sets the value of the reltype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRELTYPE(String value) {
                this.reltype = value;
            }

            /**
             * Gets the value of the primaryflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRIMARYFLG() {
                return primaryflg;
            }

            /**
             * Sets the value of the primaryflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRIMARYFLG(String value) {
                this.primaryflg = value;
            }

            /**
             * Gets the value of the txrspent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTXRSPENT() {
                return txrspent;
            }

            /**
             * Sets the value of the txrspent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTXRSPENT(String value) {
                this.txrspent = value;
            }

            /**
             * Gets the value of the pctowned property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPCTOWNED() {
                return pctowned;
            }

            /**
             * Sets the value of the pctowned property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPCTOWNED(String value) {
                this.pctowned = value;
            }

            /**
             * Gets the value of the ownertype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOWNERTYPE() {
                return ownertype;
            }

            /**
             * Sets the value of the ownertype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOWNERTYPE(String value) {
                this.ownertype = value;
            }

            /**
             * Gets the value of the interflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERFLG() {
                return interflg;
            }

            /**
             * Sets the value of the interflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERFLG(String value) {
                this.interflg = value;
            }

            /**
             * Gets the value of the ivrflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRFLG() {
                return ivrflg;
            }

            /**
             * Sets the value of the ivrflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRFLG(String value) {
                this.ivrflg = value;
            }

            /**
             * Gets the value of the curintrate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURINTRATE() {
                return curintrate;
            }

            /**
             * Sets the value of the curintrate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURINTRATE(String value) {
                this.curintrate = value;
            }

            /**
             * Gets the value of the actstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTSTATUS() {
                return actstatus;
            }

            /**
             * Sets the value of the actstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTSTATUS(String value) {
                this.actstatus = value;
            }

            /**
             * Gets the value of the currbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURRBAL() {
                return currbal;
            }

            /**
             * Sets the value of the currbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURRBAL(String value) {
                this.currbal = value;
            }

            /**
             * Gets the value of the availbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAVAILBAL() {
                return availbal;
            }

            /**
             * Sets the value of the availbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAVAILBAL(String value) {
                this.availbal = value;
            }

            /**
             * Gets the value of the payoffamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPAYOFFAMT() {
                return payoffamt;
            }

            /**
             * Sets the value of the payoffamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPAYOFFAMT(String value) {
                this.payoffamt = value;
            }

            /**
             * Gets the value of the intpdytd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDYTD() {
                return intpdytd;
            }

            /**
             * Sets the value of the intpdytd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDYTD(String value) {
                this.intpdytd = value;
            }

            /**
             * Gets the value of the intpdlstyr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDLSTYR() {
                return intpdlstyr;
            }

            /**
             * Sets the value of the intpdlstyr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDLSTYR(String value) {
                this.intpdlstyr = value;
            }

            /**
             * Gets the value of the intaccrued property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTACCRUED() {
                return intaccrued;
            }

            /**
             * Sets the value of the intaccrued property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTACCRUED(String value) {
                this.intaccrued = value;
            }

            /**
             * Gets the value of the dtopened property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTOPENED() {
                return dtopened;
            }

            /**
             * Sets the value of the dtopened property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTOPENED(String value) {
                this.dtopened = value;
            }

            /**
             * Gets the value of the dtclosed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTCLOSED() {
                return dtclosed;
            }

            /**
             * Sets the value of the dtclosed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTCLOSED(String value) {
                this.dtclosed = value;
            }

            /**
             * Gets the value of the dtlststmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTLSTSTMT() {
                return dtlststmt;
            }

            /**
             * Sets the value of the dtlststmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTLSTSTMT(String value) {
                this.dtlststmt = value;
            }

            /**
             * Gets the value of the borhstpymt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBORHSTPYMT() {
                return borhstpymt;
            }

            /**
             * Sets the value of the borhstpymt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBORHSTPYMT(String value) {
                this.borhstpymt = value;
            }

            /**
             * Gets the value of the escrowbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getESCROWBAL() {
                return escrowbal;
            }

            /**
             * Sets the value of the escrowbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setESCROWBAL(String value) {
                this.escrowbal = value;
            }

            /**
             * Gets the value of the ytdtaxpd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getYTDTAXPD() {
                return ytdtaxpd;
            }

            /**
             * Sets the value of the ytdtaxpd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setYTDTAXPD(String value) {
                this.ytdtaxpd = value;
            }

            /**
             * Gets the value of the prevyrtxpd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPREVYRTXPD() {
                return prevyrtxpd;
            }

            /**
             * Sets the value of the prevyrtxpd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPREVYRTXPD(String value) {
                this.prevyrtxpd = value;
            }

            /**
             * Gets the value of the ytdescint property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getYTDESCINT() {
                return ytdescint;
            }

            /**
             * Sets the value of the ytdescint property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setYTDESCINT(String value) {
                this.ytdescint = value;
            }

            /**
             * Gets the value of the pipymtdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPIPYMTDUE() {
                return pipymtdue;
            }

            /**
             * Sets the value of the pipymtdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPIPYMTDUE(String value) {
                this.pipymtdue = value;
            }

            /**
             * Gets the value of the remescamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREMESCAMT() {
                return remescamt;
            }

            /**
             * Sets the value of the remescamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREMESCAMT(String value) {
                this.remescamt = value;
            }

            /**
             * Gets the value of the curpymtdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURPYMTDUE() {
                return curpymtdue;
            }

            /**
             * Sets the value of the curpymtdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURPYMTDUE(String value) {
                this.curpymtdue = value;
            }

            /**
             * Gets the value of the latechgdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLATECHGDUE() {
                return latechgdue;
            }

            /**
             * Sets the value of the latechgdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLATECHGDUE(String value) {
                this.latechgdue = value;
            }

            /**
             * Gets the value of the totpstdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTOTPSTDUE() {
                return totpstdue;
            }

            /**
             * Sets the value of the totpstdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTOTPSTDUE(String value) {
                this.totpstdue = value;
            }

            /**
             * Gets the value of the totamtdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTOTAMTDUE() {
                return totamtdue;
            }

            /**
             * Sets the value of the totamtdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTOTAMTDUE(String value) {
                this.totamtdue = value;
            }

            /**
             * Gets the value of the uncltcgdue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUNCLTCGDUE() {
                return uncltcgdue;
            }

            /**
             * Sets the value of the uncltcgdue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUNCLTCGDUE(String value) {
                this.uncltcgdue = value;
            }

            /**
             * Gets the value of the nxtschpymt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNXTSCHPYMT() {
                return nxtschpymt;
            }

            /**
             * Sets the value of the nxtschpymt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNXTSCHPYMT(String value) {
                this.nxtschpymt = value;
            }

            /**
             * Gets the value of the lstpymtdt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTPYMTDT() {
                return lstpymtdt;
            }

            /**
             * Sets the value of the lstpymtdt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTPYMTDT(String value) {
                this.lstpymtdt = value;
            }

            /**
             * Gets the value of the curmtrydt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURMTRYDT() {
                return curmtrydt;
            }

            /**
             * Sets the value of the curmtrydt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURMTRYDT(String value) {
                this.curmtrydt = value;
            }

            /**
             * Gets the value of the lstbilldt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTBILLDT() {
                return lstbilldt;
            }

            /**
             * Sets the value of the lstbilldt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTBILLDT(String value) {
                this.lstbilldt = value;
            }

            /**
             * Gets the value of the date05 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE05() {
                return date05;
            }

            /**
             * Sets the value of the date05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE05(String value) {
                this.date05 = value;
            }

            /**
             * Gets the value of the curpymtdt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURPYMTDT1() {
                return curpymtdt1;
            }

            /**
             * Sets the value of the curpymtdt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURPYMTDT1(String value) {
                this.curpymtdt1 = value;
            }

            /**
             * Gets the value of the curpymtdt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURPYMTDT2() {
                return curpymtdt2;
            }

            /**
             * Sets the value of the curpymtdt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURPYMTDT2(String value) {
                this.curpymtdt2 = value;
            }

            /**
             * Gets the value of the date08 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE08() {
                return date08;
            }

            /**
             * Sets the value of the date08 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE08(String value) {
                this.date08 = value;
            }

            /**
             * Gets the value of the misc01 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMISC01() {
                return misc01;
            }

            /**
             * Sets the value of the misc01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMISC01(String value) {
                this.misc01 = value;
            }

            /**
             * Gets the value of the misc02 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC02() {
                return misc02;
            }

            /**
             * Sets the value of the misc02 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC02(Object value) {
                this.misc02 = value;
            }

            /**
             * Gets the value of the misc03 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC03() {
                return misc03;
            }

            /**
             * Sets the value of the misc03 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC03(Object value) {
                this.misc03 = value;
            }

            /**
             * Gets the value of the future01 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getFUTURE01() {
                return future01;
            }

            /**
             * Sets the value of the future01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setFUTURE01(Object value) {
                this.future01 = value;
            }

            /**
             * Gets the value of the porttotflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPORTTOTFLG() {
                return porttotflg;
            }

            /**
             * Sets the value of the porttotflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPORTTOTFLG(Object value) {
                this.porttotflg = value;
            }

            /**
             * Gets the value of the pctalwdflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPCTALWDFLG() {
                return pctalwdflg;
            }

            /**
             * Sets the value of the pctalwdflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPCTALWDFLG(Object value) {
                this.pctalwdflg = value;
            }

            /**
             * Gets the value of the ocdstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCDSTATUS() {
                return ocdstatus;
            }

            /**
             * Sets the value of the ocdstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCDSTATUS(String value) {
                this.ocdstatus = value;
            }

            /**
             * Gets the value of the confflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCONFFLAG() {
                return confflag;
            }

            /**
             * Sets the value of the confflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCONFFLAG(String value) {
                this.confflag = value;
            }

            /**
             * Gets the value of the cashmgmt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT1() {
                return cashmgmt1;
            }

            /**
             * Sets the value of the cashmgmt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT1(String value) {
                this.cashmgmt1 = value;
            }

            /**
             * Gets the value of the cashmgmt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT2() {
                return cashmgmt2;
            }

            /**
             * Sets the value of the cashmgmt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT2(String value) {
                this.cashmgmt2 = value;
            }

            /**
             * Gets the value of the regdrstint property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getREGDRSTINT() {
                return regdrstint;
            }

            /**
             * Sets the value of the regdrstint property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setREGDRSTINT(Object value) {
                this.regdrstint = value;
            }

            /**
             * Gets the value of the thirdparty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTHIRDPARTY() {
                return thirdparty;
            }

            /**
             * Sets the value of the thirdparty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTHIRDPARTY(String value) {
                this.thirdparty = value;
            }

            /**
             * Gets the value of the preauttrct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPREAUTTRCT() {
                return preauttrct;
            }

            /**
             * Sets the value of the preauttrct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPREAUTTRCT(String value) {
                this.preauttrct = value;
            }

            /**
             * Gets the value of the option1 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION1() {
                return option1;
            }

            /**
             * Sets the value of the option1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION1(Object value) {
                this.option1 = value;
            }

            /**
             * Gets the value of the option2 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION2() {
                return option2;
            }

            /**
             * Sets the value of the option2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION2(Object value) {
                this.option2 = value;
            }

            /**
             * Gets the value of the billpayflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBILLPAYFLG() {
                return billpayflg;
            }

            /**
             * Sets the value of the billpayflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBILLPAYFLG(Object value) {
                this.billpayflg = value;
            }

            /**
             * Gets the value of the orgprinbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORGPRINBAL() {
                return orgprinbal;
            }

            /**
             * Sets the value of the orgprinbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORGPRINBAL(String value) {
                this.orgprinbal = value;
            }

            /**
             * Gets the value of the rrsselflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSSELFLG() {
                return rrsselflg;
            }

            /**
             * Sets the value of the rrsselflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSSELFLG(String value) {
                this.rrsselflg = value;
            }

            /**
             * Gets the value of the dyavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDYAVAILFLG() {
                return dyavailflg;
            }

            /**
             * Sets the value of the dyavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDYAVAILFLG(String value) {
                this.dyavailflg = value;
            }

            /**
             * Gets the value of the ddavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDAVAILFLG() {
                return ddavailflg;
            }

            /**
             * Sets the value of the ddavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDAVAILFLG(String value) {
                this.ddavailflg = value;
            }

            /**
             * Gets the value of the lnavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNAVAILFLG() {
                return lnavailflg;
            }

            /**
             * Sets the value of the lnavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNAVAILFLG(String value) {
                this.lnavailflg = value;
            }

            /**
             * Gets the value of the odlimitflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getODLIMITFLG() {
                return odlimitflg;
            }

            /**
             * Sets the value of the odlimitflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setODLIMITFLG(String value) {
                this.odlimitflg = value;
            }

            /**
             * Gets the value of the custfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCUSTFLTFLG() {
                return custfltflg;
            }

            /**
             * Sets the value of the custfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCUSTFLTFLG(String value) {
                this.custfltflg = value;
            }

            /**
             * Gets the value of the bnkfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBNKFLTFLG() {
                return bnkfltflg;
            }

            /**
             * Sets the value of the bnkfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBNKFLTFLG(String value) {
                this.bnkfltflg = value;
            }

            /**
             * Gets the value of the holdsflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOLDSFLG() {
                return holdsflg;
            }

            /**
             * Sets the value of the holdsflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOLDSFLG(String value) {
                this.holdsflg = value;
            }

            /**
             * Gets the value of the ckpdtdyflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCKPDTDYFLG() {
                return ckpdtdyflg;
            }

            /**
             * Sets the value of the ckpdtdyflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCKPDTDYFLG(String value) {
                this.ckpdtdyflg = value;
            }

            /**
             * Gets the value of the amount14 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT14() {
                return amount14;
            }

            /**
             * Sets the value of the amount14 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT14(String value) {
                this.amount14 = value;
            }

            /**
             * Gets the value of the amount15 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT15() {
                return amount15;
            }

            /**
             * Sets the value of the amount15 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT15(String value) {
                this.amount15 = value;
            }

            /**
             * Gets the value of the amount16 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT16() {
                return amount16;
            }

            /**
             * Sets the value of the amount16 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT16(String value) {
                this.amount16 = value;
            }

            /**
             * Gets the value of the amount17 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT17() {
                return amount17;
            }

            /**
             * Sets the value of the amount17 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT17(String value) {
                this.amount17 = value;
            }

            /**
             * Gets the value of the amount18 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT18() {
                return amount18;
            }

            /**
             * Sets the value of the amount18 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT18(String value) {
                this.amount18 = value;
            }

            /**
             * Gets the value of the date09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE09() {
                return date09;
            }

            /**
             * Sets the value of the date09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE09(String value) {
                this.date09 = value;
            }

            /**
             * Gets the value of the date10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE10() {
                return date10;
            }

            /**
             * Sets the value of the date10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE10(String value) {
                this.date10 = value;
            }

            /**
             * Gets the value of the misc04 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC04() {
                return misc04;
            }

            /**
             * Sets the value of the misc04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC04(Object value) {
                this.misc04 = value;
            }

            /**
             * Gets the value of the misc05 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC05() {
                return misc05;
            }

            /**
             * Sets the value of the misc05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC05(Object value) {
                this.misc05 = value;
            }

            /**
             * Gets the value of the misc06 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC06() {
                return misc06;
            }

            /**
             * Sets the value of the misc06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC06(Object value) {
                this.misc06 = value;
            }

            /**
             * Gets the value of the misc07 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC07() {
                return misc07;
            }

            /**
             * Sets the value of the misc07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC07(Object value) {
                this.misc07 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="UNDPRELEXT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="GROUPCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GROUPSEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRODTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OFFICER" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RELTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PRIMARYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TXRSPENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PCTOWNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OWNERTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTERFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IVRFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURINTRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ACTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AVAILBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PAYOFFAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDYTD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTPDLSTYR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="INTACCRUED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTOPENED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTCLOSED" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DTLSTSTMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTINTPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TOTHOLDS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ANTICPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT05" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT06" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT07" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT08" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT11" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT12" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NXTINTPYM1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTINTPYMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURMTRYDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LSTRNWLDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE06" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE07" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NXTINTPYM2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PLANNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PLANTYPE" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC03" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="FUTURE01" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PORTTOTFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="PCTALWDFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OCDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CONFFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CASHMGMT2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGDRSTINT" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="THIRDPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PREAUTTRCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OPTION1" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="OPTION2" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="BILLPAYFLG" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="AMOUNT13" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RRSSELFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DYAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LNAVAILFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ODLIMITFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CUSTFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BNKFLTFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOLDSFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CKPDTDYFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT14" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT15" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT16" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT17" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AMOUNT18" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE09" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MISC04" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC05" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC06" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="MISC07" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "undprelext",
            "groupcd",
            "groupseq",
            "applcd",
            "actdesc",
            "actnbr",
            "prodtype",
            "shortname",
            "officer",
            "branch",
            "reltype",
            "primaryflg",
            "txrspent",
            "pctowned",
            "ownertype",
            "interflg",
            "ivrflg",
            "curintrate",
            "actstatus",
            "currbal",
            "availbal",
            "payoffamt",
            "intpdytd",
            "intpdlstyr",
            "intaccrued",
            "dtopened",
            "dtclosed",
            "dtlststmt",
            "lstintpd",
            "totholds",
            "anticpen",
            "amount04",
            "amount05",
            "amount06",
            "amount07",
            "amount08",
            "amount09",
            "amount10",
            "amount11",
            "amount12",
            "nxtintpym1",
            "lstintpymt",
            "curmtrydt",
            "date04",
            "lstrnwldt",
            "date06",
            "date07",
            "nxtintpym2",
            "plannbr",
            "plantype",
            "misc03",
            "future01",
            "porttotflg",
            "pctalwdflg",
            "ocdstatus",
            "confflag",
            "cashmgmt1",
            "cashmgmt2",
            "regdrstint",
            "thirdparty",
            "preauttrct",
            "option1",
            "option2",
            "billpayflg",
            "amount13",
            "rrsselflg",
            "dyavailflg",
            "ddavailflg",
            "lnavailflg",
            "odlimitflg",
            "custfltflg",
            "bnkfltflg",
            "holdsflg",
            "ckpdtdyflg",
            "amount14",
            "amount15",
            "amount16",
            "amount17",
            "amount18",
            "date09",
            "date10",
            "misc04",
            "misc05",
            "misc06",
            "misc07"
        })
        public static class TIMEACCOUNT {

            @XmlElement(name = "UNDPRELEXT", required = true)
            protected Object undprelext;
            @XmlElement(name = "GROUPCD", required = true)
            protected String groupcd;
            @XmlElement(name = "GROUPSEQ", required = true)
            protected String groupseq;
            @XmlElement(name = "APPLCD", required = true)
            protected String applcd;
            @XmlElement(name = "ACTDESC", required = true)
            protected String actdesc;
            @XmlElement(name = "ACTNBR", required = true)
            protected String actnbr;
            @XmlElement(name = "PRODTYPE", required = true)
            protected String prodtype;
            @XmlElement(name = "SHORTNAME", required = true)
            protected String shortname;
            @XmlElement(name = "OFFICER", required = true)
            protected Object officer;
            @XmlElement(name = "BRANCH", required = true)
            protected String branch;
            @XmlElement(name = "RELTYPE", required = true)
            protected String reltype;
            @XmlElement(name = "PRIMARYFLG", required = true)
            protected String primaryflg;
            @XmlElement(name = "TXRSPENT", required = true)
            protected String txrspent;
            @XmlElement(name = "PCTOWNED", required = true)
            protected String pctowned;
            @XmlElement(name = "OWNERTYPE", required = true)
            protected String ownertype;
            @XmlElement(name = "INTERFLG", required = true)
            protected String interflg;
            @XmlElement(name = "IVRFLG", required = true)
            protected String ivrflg;
            @XmlElement(name = "CURINTRATE", required = true)
            protected String curintrate;
            @XmlElement(name = "ACTSTATUS", required = true)
            protected String actstatus;
            @XmlElement(name = "CURRBAL", required = true)
            protected String currbal;
            @XmlElement(name = "AVAILBAL", required = true)
            protected String availbal;
            @XmlElement(name = "PAYOFFAMT", required = true)
            protected String payoffamt;
            @XmlElement(name = "INTPDYTD", required = true)
            protected String intpdytd;
            @XmlElement(name = "INTPDLSTYR", required = true)
            protected String intpdlstyr;
            @XmlElement(name = "INTACCRUED", required = true)
            protected String intaccrued;
            @XmlElement(name = "DTOPENED", required = true)
            protected String dtopened;
            @XmlElement(name = "DTCLOSED", required = true)
            protected String dtclosed;
            @XmlElement(name = "DTLSTSTMT", required = true)
            protected String dtlststmt;
            @XmlElement(name = "LSTINTPD", required = true)
            protected String lstintpd;
            @XmlElement(name = "TOTHOLDS", required = true)
            protected String totholds;
            @XmlElement(name = "ANTICPEN", required = true)
            protected String anticpen;
            @XmlElement(name = "AMOUNT04", required = true)
            protected String amount04;
            @XmlElement(name = "AMOUNT05", required = true)
            protected String amount05;
            @XmlElement(name = "AMOUNT06", required = true)
            protected String amount06;
            @XmlElement(name = "AMOUNT07", required = true)
            protected String amount07;
            @XmlElement(name = "AMOUNT08", required = true)
            protected String amount08;
            @XmlElement(name = "AMOUNT09", required = true)
            protected String amount09;
            @XmlElement(name = "AMOUNT10", required = true)
            protected String amount10;
            @XmlElement(name = "AMOUNT11", required = true)
            protected String amount11;
            @XmlElement(name = "AMOUNT12", required = true)
            protected String amount12;
            @XmlElement(name = "NXTINTPYM1", required = true)
            protected String nxtintpym1;
            @XmlElement(name = "LSTINTPYMT", required = true)
            protected String lstintpymt;
            @XmlElement(name = "CURMTRYDT", required = true)
            protected String curmtrydt;
            @XmlElement(name = "DATE04", required = true)
            protected String date04;
            @XmlElement(name = "LSTRNWLDT", required = true)
            protected String lstrnwldt;
            @XmlElement(name = "DATE06", required = true)
            protected String date06;
            @XmlElement(name = "DATE07", required = true)
            protected String date07;
            @XmlElement(name = "NXTINTPYM2", required = true)
            protected String nxtintpym2;
            @XmlElement(name = "PLANNBR", required = true)
            protected String plannbr;
            @XmlElement(name = "PLANTYPE", required = true)
            protected Object plantype;
            @XmlElement(name = "MISC03", required = true)
            protected Object misc03;
            @XmlElement(name = "FUTURE01", required = true)
            protected Object future01;
            @XmlElement(name = "PORTTOTFLG", required = true)
            protected Object porttotflg;
            @XmlElement(name = "PCTALWDFLG", required = true)
            protected Object pctalwdflg;
            @XmlElement(name = "OCDSTATUS", required = true)
            protected String ocdstatus;
            @XmlElement(name = "CONFFLAG", required = true)
            protected String confflag;
            @XmlElement(name = "CASHMGMT1", required = true)
            protected String cashmgmt1;
            @XmlElement(name = "CASHMGMT2", required = true)
            protected String cashmgmt2;
            @XmlElement(name = "REGDRSTINT", required = true)
            protected Object regdrstint;
            @XmlElement(name = "THIRDPARTY", required = true)
            protected String thirdparty;
            @XmlElement(name = "PREAUTTRCT", required = true)
            protected String preauttrct;
            @XmlElement(name = "OPTION1", required = true)
            protected Object option1;
            @XmlElement(name = "OPTION2", required = true)
            protected Object option2;
            @XmlElement(name = "BILLPAYFLG", required = true)
            protected Object billpayflg;
            @XmlElement(name = "AMOUNT13", required = true)
            protected String amount13;
            @XmlElement(name = "RRSSELFLG", required = true)
            protected String rrsselflg;
            @XmlElement(name = "DYAVAILFLG", required = true)
            protected String dyavailflg;
            @XmlElement(name = "DDAVAILFLG", required = true)
            protected String ddavailflg;
            @XmlElement(name = "LNAVAILFLG", required = true)
            protected String lnavailflg;
            @XmlElement(name = "ODLIMITFLG", required = true)
            protected String odlimitflg;
            @XmlElement(name = "CUSTFLTFLG", required = true)
            protected String custfltflg;
            @XmlElement(name = "BNKFLTFLG", required = true)
            protected String bnkfltflg;
            @XmlElement(name = "HOLDSFLG", required = true)
            protected String holdsflg;
            @XmlElement(name = "CKPDTDYFLG", required = true)
            protected String ckpdtdyflg;
            @XmlElement(name = "AMOUNT14", required = true)
            protected String amount14;
            @XmlElement(name = "AMOUNT15", required = true)
            protected String amount15;
            @XmlElement(name = "AMOUNT16", required = true)
            protected String amount16;
            @XmlElement(name = "AMOUNT17", required = true)
            protected String amount17;
            @XmlElement(name = "AMOUNT18", required = true)
            protected String amount18;
            @XmlElement(name = "DATE09", required = true)
            protected String date09;
            @XmlElement(name = "DATE10", required = true)
            protected String date10;
            @XmlElement(name = "MISC04", required = true)
            protected Object misc04;
            @XmlElement(name = "MISC05", required = true)
            protected Object misc05;
            @XmlElement(name = "MISC06", required = true)
            protected Object misc06;
            @XmlElement(name = "MISC07", required = true)
            protected Object misc07;

            /**
             * Gets the value of the undprelext property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getUNDPRELEXT() {
                return undprelext;
            }

            /**
             * Sets the value of the undprelext property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setUNDPRELEXT(Object value) {
                this.undprelext = value;
            }

            /**
             * Gets the value of the groupcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPCD() {
                return groupcd;
            }

            /**
             * Sets the value of the groupcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPCD(String value) {
                this.groupcd = value;
            }

            /**
             * Gets the value of the groupseq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGROUPSEQ() {
                return groupseq;
            }

            /**
             * Sets the value of the groupseq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGROUPSEQ(String value) {
                this.groupseq = value;
            }

            /**
             * Gets the value of the applcd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAPPLCD() {
                return applcd;
            }

            /**
             * Sets the value of the applcd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAPPLCD(String value) {
                this.applcd = value;
            }

            /**
             * Gets the value of the actdesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTDESC() {
                return actdesc;
            }

            /**
             * Sets the value of the actdesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTDESC(String value) {
                this.actdesc = value;
            }

            /**
             * Gets the value of the actnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTNBR() {
                return actnbr;
            }

            /**
             * Sets the value of the actnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTNBR(String value) {
                this.actnbr = value;
            }

            /**
             * Gets the value of the prodtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRODTYPE() {
                return prodtype;
            }

            /**
             * Sets the value of the prodtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRODTYPE(String value) {
                this.prodtype = value;
            }

            /**
             * Gets the value of the shortname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSHORTNAME() {
                return shortname;
            }

            /**
             * Sets the value of the shortname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSHORTNAME(String value) {
                this.shortname = value;
            }

            /**
             * Gets the value of the officer property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOFFICER() {
                return officer;
            }

            /**
             * Sets the value of the officer property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOFFICER(Object value) {
                this.officer = value;
            }

            /**
             * Gets the value of the branch property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBRANCH() {
                return branch;
            }

            /**
             * Sets the value of the branch property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBRANCH(String value) {
                this.branch = value;
            }

            /**
             * Gets the value of the reltype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRELTYPE() {
                return reltype;
            }

            /**
             * Sets the value of the reltype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRELTYPE(String value) {
                this.reltype = value;
            }

            /**
             * Gets the value of the primaryflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRIMARYFLG() {
                return primaryflg;
            }

            /**
             * Sets the value of the primaryflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRIMARYFLG(String value) {
                this.primaryflg = value;
            }

            /**
             * Gets the value of the txrspent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTXRSPENT() {
                return txrspent;
            }

            /**
             * Sets the value of the txrspent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTXRSPENT(String value) {
                this.txrspent = value;
            }

            /**
             * Gets the value of the pctowned property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPCTOWNED() {
                return pctowned;
            }

            /**
             * Sets the value of the pctowned property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPCTOWNED(String value) {
                this.pctowned = value;
            }

            /**
             * Gets the value of the ownertype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOWNERTYPE() {
                return ownertype;
            }

            /**
             * Sets the value of the ownertype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOWNERTYPE(String value) {
                this.ownertype = value;
            }

            /**
             * Gets the value of the interflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERFLG() {
                return interflg;
            }

            /**
             * Sets the value of the interflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERFLG(String value) {
                this.interflg = value;
            }

            /**
             * Gets the value of the ivrflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRFLG() {
                return ivrflg;
            }

            /**
             * Sets the value of the ivrflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRFLG(String value) {
                this.ivrflg = value;
            }

            /**
             * Gets the value of the curintrate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURINTRATE() {
                return curintrate;
            }

            /**
             * Sets the value of the curintrate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURINTRATE(String value) {
                this.curintrate = value;
            }

            /**
             * Gets the value of the actstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACTSTATUS() {
                return actstatus;
            }

            /**
             * Sets the value of the actstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACTSTATUS(String value) {
                this.actstatus = value;
            }

            /**
             * Gets the value of the currbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURRBAL() {
                return currbal;
            }

            /**
             * Sets the value of the currbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURRBAL(String value) {
                this.currbal = value;
            }

            /**
             * Gets the value of the availbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAVAILBAL() {
                return availbal;
            }

            /**
             * Sets the value of the availbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAVAILBAL(String value) {
                this.availbal = value;
            }

            /**
             * Gets the value of the payoffamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPAYOFFAMT() {
                return payoffamt;
            }

            /**
             * Sets the value of the payoffamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPAYOFFAMT(String value) {
                this.payoffamt = value;
            }

            /**
             * Gets the value of the intpdytd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDYTD() {
                return intpdytd;
            }

            /**
             * Sets the value of the intpdytd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDYTD(String value) {
                this.intpdytd = value;
            }

            /**
             * Gets the value of the intpdlstyr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTPDLSTYR() {
                return intpdlstyr;
            }

            /**
             * Sets the value of the intpdlstyr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTPDLSTYR(String value) {
                this.intpdlstyr = value;
            }

            /**
             * Gets the value of the intaccrued property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTACCRUED() {
                return intaccrued;
            }

            /**
             * Sets the value of the intaccrued property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTACCRUED(String value) {
                this.intaccrued = value;
            }

            /**
             * Gets the value of the dtopened property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTOPENED() {
                return dtopened;
            }

            /**
             * Sets the value of the dtopened property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTOPENED(String value) {
                this.dtopened = value;
            }

            /**
             * Gets the value of the dtclosed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTCLOSED() {
                return dtclosed;
            }

            /**
             * Sets the value of the dtclosed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTCLOSED(String value) {
                this.dtclosed = value;
            }

            /**
             * Gets the value of the dtlststmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDTLSTSTMT() {
                return dtlststmt;
            }

            /**
             * Sets the value of the dtlststmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDTLSTSTMT(String value) {
                this.dtlststmt = value;
            }

            /**
             * Gets the value of the lstintpd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTINTPD() {
                return lstintpd;
            }

            /**
             * Sets the value of the lstintpd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTINTPD(String value) {
                this.lstintpd = value;
            }

            /**
             * Gets the value of the totholds property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTOTHOLDS() {
                return totholds;
            }

            /**
             * Sets the value of the totholds property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTOTHOLDS(String value) {
                this.totholds = value;
            }

            /**
             * Gets the value of the anticpen property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getANTICPEN() {
                return anticpen;
            }

            /**
             * Sets the value of the anticpen property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setANTICPEN(String value) {
                this.anticpen = value;
            }

            /**
             * Gets the value of the amount04 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT04() {
                return amount04;
            }

            /**
             * Sets the value of the amount04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT04(String value) {
                this.amount04 = value;
            }

            /**
             * Gets the value of the amount05 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT05() {
                return amount05;
            }

            /**
             * Sets the value of the amount05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT05(String value) {
                this.amount05 = value;
            }

            /**
             * Gets the value of the amount06 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT06() {
                return amount06;
            }

            /**
             * Sets the value of the amount06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT06(String value) {
                this.amount06 = value;
            }

            /**
             * Gets the value of the amount07 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT07() {
                return amount07;
            }

            /**
             * Sets the value of the amount07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT07(String value) {
                this.amount07 = value;
            }

            /**
             * Gets the value of the amount08 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT08() {
                return amount08;
            }

            /**
             * Sets the value of the amount08 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT08(String value) {
                this.amount08 = value;
            }

            /**
             * Gets the value of the amount09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT09() {
                return amount09;
            }

            /**
             * Sets the value of the amount09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT09(String value) {
                this.amount09 = value;
            }

            /**
             * Gets the value of the amount10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT10() {
                return amount10;
            }

            /**
             * Sets the value of the amount10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT10(String value) {
                this.amount10 = value;
            }

            /**
             * Gets the value of the amount11 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT11() {
                return amount11;
            }

            /**
             * Sets the value of the amount11 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT11(String value) {
                this.amount11 = value;
            }

            /**
             * Gets the value of the amount12 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT12() {
                return amount12;
            }

            /**
             * Sets the value of the amount12 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT12(String value) {
                this.amount12 = value;
            }

            /**
             * Gets the value of the nxtintpym1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNXTINTPYM1() {
                return nxtintpym1;
            }

            /**
             * Sets the value of the nxtintpym1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNXTINTPYM1(String value) {
                this.nxtintpym1 = value;
            }

            /**
             * Gets the value of the lstintpymt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTINTPYMT() {
                return lstintpymt;
            }

            /**
             * Sets the value of the lstintpymt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTINTPYMT(String value) {
                this.lstintpymt = value;
            }

            /**
             * Gets the value of the curmtrydt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURMTRYDT() {
                return curmtrydt;
            }

            /**
             * Sets the value of the curmtrydt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURMTRYDT(String value) {
                this.curmtrydt = value;
            }

            /**
             * Gets the value of the date04 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE04() {
                return date04;
            }

            /**
             * Sets the value of the date04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE04(String value) {
                this.date04 = value;
            }

            /**
             * Gets the value of the lstrnwldt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLSTRNWLDT() {
                return lstrnwldt;
            }

            /**
             * Sets the value of the lstrnwldt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLSTRNWLDT(String value) {
                this.lstrnwldt = value;
            }

            /**
             * Gets the value of the date06 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE06() {
                return date06;
            }

            /**
             * Sets the value of the date06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE06(String value) {
                this.date06 = value;
            }

            /**
             * Gets the value of the date07 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE07() {
                return date07;
            }

            /**
             * Sets the value of the date07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE07(String value) {
                this.date07 = value;
            }

            /**
             * Gets the value of the nxtintpym2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNXTINTPYM2() {
                return nxtintpym2;
            }

            /**
             * Sets the value of the nxtintpym2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNXTINTPYM2(String value) {
                this.nxtintpym2 = value;
            }

            /**
             * Gets the value of the plannbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPLANNBR() {
                return plannbr;
            }

            /**
             * Sets the value of the plannbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPLANNBR(String value) {
                this.plannbr = value;
            }

            /**
             * Gets the value of the plantype property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPLANTYPE() {
                return plantype;
            }

            /**
             * Sets the value of the plantype property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPLANTYPE(Object value) {
                this.plantype = value;
            }

            /**
             * Gets the value of the misc03 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC03() {
                return misc03;
            }

            /**
             * Sets the value of the misc03 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC03(Object value) {
                this.misc03 = value;
            }

            /**
             * Gets the value of the future01 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getFUTURE01() {
                return future01;
            }

            /**
             * Sets the value of the future01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setFUTURE01(Object value) {
                this.future01 = value;
            }

            /**
             * Gets the value of the porttotflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPORTTOTFLG() {
                return porttotflg;
            }

            /**
             * Sets the value of the porttotflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPORTTOTFLG(Object value) {
                this.porttotflg = value;
            }

            /**
             * Gets the value of the pctalwdflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPCTALWDFLG() {
                return pctalwdflg;
            }

            /**
             * Sets the value of the pctalwdflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPCTALWDFLG(Object value) {
                this.pctalwdflg = value;
            }

            /**
             * Gets the value of the ocdstatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCDSTATUS() {
                return ocdstatus;
            }

            /**
             * Sets the value of the ocdstatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCDSTATUS(String value) {
                this.ocdstatus = value;
            }

            /**
             * Gets the value of the confflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCONFFLAG() {
                return confflag;
            }

            /**
             * Sets the value of the confflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCONFFLAG(String value) {
                this.confflag = value;
            }

            /**
             * Gets the value of the cashmgmt1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT1() {
                return cashmgmt1;
            }

            /**
             * Sets the value of the cashmgmt1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT1(String value) {
                this.cashmgmt1 = value;
            }

            /**
             * Gets the value of the cashmgmt2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCASHMGMT2() {
                return cashmgmt2;
            }

            /**
             * Sets the value of the cashmgmt2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCASHMGMT2(String value) {
                this.cashmgmt2 = value;
            }

            /**
             * Gets the value of the regdrstint property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getREGDRSTINT() {
                return regdrstint;
            }

            /**
             * Sets the value of the regdrstint property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setREGDRSTINT(Object value) {
                this.regdrstint = value;
            }

            /**
             * Gets the value of the thirdparty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTHIRDPARTY() {
                return thirdparty;
            }

            /**
             * Sets the value of the thirdparty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTHIRDPARTY(String value) {
                this.thirdparty = value;
            }

            /**
             * Gets the value of the preauttrct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPREAUTTRCT() {
                return preauttrct;
            }

            /**
             * Sets the value of the preauttrct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPREAUTTRCT(String value) {
                this.preauttrct = value;
            }

            /**
             * Gets the value of the option1 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION1() {
                return option1;
            }

            /**
             * Sets the value of the option1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION1(Object value) {
                this.option1 = value;
            }

            /**
             * Gets the value of the option2 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOPTION2() {
                return option2;
            }

            /**
             * Sets the value of the option2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOPTION2(Object value) {
                this.option2 = value;
            }

            /**
             * Gets the value of the billpayflg property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBILLPAYFLG() {
                return billpayflg;
            }

            /**
             * Sets the value of the billpayflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBILLPAYFLG(Object value) {
                this.billpayflg = value;
            }

            /**
             * Gets the value of the amount13 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT13() {
                return amount13;
            }

            /**
             * Sets the value of the amount13 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT13(String value) {
                this.amount13 = value;
            }

            /**
             * Gets the value of the rrsselflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRRSSELFLG() {
                return rrsselflg;
            }

            /**
             * Sets the value of the rrsselflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRRSSELFLG(String value) {
                this.rrsselflg = value;
            }

            /**
             * Gets the value of the dyavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDYAVAILFLG() {
                return dyavailflg;
            }

            /**
             * Sets the value of the dyavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDYAVAILFLG(String value) {
                this.dyavailflg = value;
            }

            /**
             * Gets the value of the ddavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDAVAILFLG() {
                return ddavailflg;
            }

            /**
             * Sets the value of the ddavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDAVAILFLG(String value) {
                this.ddavailflg = value;
            }

            /**
             * Gets the value of the lnavailflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLNAVAILFLG() {
                return lnavailflg;
            }

            /**
             * Sets the value of the lnavailflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLNAVAILFLG(String value) {
                this.lnavailflg = value;
            }

            /**
             * Gets the value of the odlimitflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getODLIMITFLG() {
                return odlimitflg;
            }

            /**
             * Sets the value of the odlimitflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setODLIMITFLG(String value) {
                this.odlimitflg = value;
            }

            /**
             * Gets the value of the custfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCUSTFLTFLG() {
                return custfltflg;
            }

            /**
             * Sets the value of the custfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCUSTFLTFLG(String value) {
                this.custfltflg = value;
            }

            /**
             * Gets the value of the bnkfltflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBNKFLTFLG() {
                return bnkfltflg;
            }

            /**
             * Sets the value of the bnkfltflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBNKFLTFLG(String value) {
                this.bnkfltflg = value;
            }

            /**
             * Gets the value of the holdsflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOLDSFLG() {
                return holdsflg;
            }

            /**
             * Sets the value of the holdsflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOLDSFLG(String value) {
                this.holdsflg = value;
            }

            /**
             * Gets the value of the ckpdtdyflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCKPDTDYFLG() {
                return ckpdtdyflg;
            }

            /**
             * Sets the value of the ckpdtdyflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCKPDTDYFLG(String value) {
                this.ckpdtdyflg = value;
            }

            /**
             * Gets the value of the amount14 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT14() {
                return amount14;
            }

            /**
             * Sets the value of the amount14 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT14(String value) {
                this.amount14 = value;
            }

            /**
             * Gets the value of the amount15 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT15() {
                return amount15;
            }

            /**
             * Sets the value of the amount15 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT15(String value) {
                this.amount15 = value;
            }

            /**
             * Gets the value of the amount16 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT16() {
                return amount16;
            }

            /**
             * Sets the value of the amount16 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT16(String value) {
                this.amount16 = value;
            }

            /**
             * Gets the value of the amount17 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT17() {
                return amount17;
            }

            /**
             * Sets the value of the amount17 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT17(String value) {
                this.amount17 = value;
            }

            /**
             * Gets the value of the amount18 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAMOUNT18() {
                return amount18;
            }

            /**
             * Sets the value of the amount18 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAMOUNT18(String value) {
                this.amount18 = value;
            }

            /**
             * Gets the value of the date09 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE09() {
                return date09;
            }

            /**
             * Sets the value of the date09 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE09(String value) {
                this.date09 = value;
            }

            /**
             * Gets the value of the date10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATE10() {
                return date10;
            }

            /**
             * Sets the value of the date10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATE10(String value) {
                this.date10 = value;
            }

            /**
             * Gets the value of the misc04 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC04() {
                return misc04;
            }

            /**
             * Sets the value of the misc04 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC04(Object value) {
                this.misc04 = value;
            }

            /**
             * Gets the value of the misc05 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC05() {
                return misc05;
            }

            /**
             * Sets the value of the misc05 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC05(Object value) {
                this.misc05 = value;
            }

            /**
             * Gets the value of the misc06 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC06() {
                return misc06;
            }

            /**
             * Sets the value of the misc06 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC06(Object value) {
                this.misc06 = value;
            }

            /**
             * Gets the value of the misc07 property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMISC07() {
                return misc07;
            }

            /**
             * Sets the value of the misc07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMISC07(Object value) {
                this.misc07 = value;
            }

        }

    }

}
