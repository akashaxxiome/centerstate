
package com.sap.banking.custom.webservices.transactioninquiry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TRANSACTION" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TRANDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANMOD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CHECKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRACENBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BATCHIDENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SEQNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TELLERBRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TELLERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESCS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC8" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC9" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANDESC11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EFFDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EFFRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MEMOPSTIND" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTPRIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTRESV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTINS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTLCHG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTFEE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTREB" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRNAMTUNA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TRANCDX" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DRCRFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IMAGEFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGSEQNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OLBTRANID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGPROCDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGBNKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGSUBSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGBATCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ORIGRLSAPP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RECS_RETURNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MOREDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sequence",
    "responseData"
})
@XmlRootElement(name = "TransactionInquiryResponse")
public class TransactionInquiryResponse {

    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "ResponseData", required = true)
    protected TransactionInquiryResponse.ResponseData responseData;

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionInquiryResponse.ResponseData }
     *     
     */
    public TransactionInquiryResponse.ResponseData getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionInquiryResponse.ResponseData }
     *     
     */
    public void setResponseData(TransactionInquiryResponse.ResponseData value) {
        this.responseData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TRANSACTION" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TRANDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANMOD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CHECKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRACENBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BATCHIDENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SEQNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TELLERBRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TELLERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESCS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC5" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC6" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC7" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC8" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC9" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANDESC11" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EFFDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EFFRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MEMOPSTIND" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTPRIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTRESV" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTINS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTLCHG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTFEE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTREB" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRNAMTUNA" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TRANCDX" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DRCRFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IMAGEFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGSEQNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OLBTRANID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGPROCDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGBNKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGSUBSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGBATCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ORIGRLSAPP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RECS_RETURNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MOREDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transaction",
        "recsreturned",
        "moredata"
    })
    public static class ResponseData {

        @XmlElement(name = "TRANSACTION", required = true)
        protected List<TransactionInquiryResponse.ResponseData.TRANSACTION> transaction;
        @XmlElement(name = "RECS_RETURNED", required = true)
        protected String recsreturned;
        @XmlElement(name = "MOREDATA", required = true)
        protected String moredata;

        /**
         * Gets the value of the transaction property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the transaction property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTRANSACTION().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TransactionInquiryResponse.ResponseData.TRANSACTION }
         * 
         * 
         */
        public List<TransactionInquiryResponse.ResponseData.TRANSACTION> getTRANSACTION() {
            if (transaction == null) {
                transaction = new ArrayList<TransactionInquiryResponse.ResponseData.TRANSACTION>();
            }
            return this.transaction;
        }

        /**
         * Gets the value of the recsreturned property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRECSRETURNED() {
            return recsreturned;
        }

        /**
         * Sets the value of the recsreturned property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRECSRETURNED(String value) {
            this.recsreturned = value;
        }

        /**
         * Gets the value of the moredata property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMOREDATA() {
            return moredata;
        }

        /**
         * Sets the value of the moredata property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMOREDATA(String value) {
            this.moredata = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TRANDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANMOD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CHECKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRACENBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BATCHIDENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SEQNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TELLERBRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TELLERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESCS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC5" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC6" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC7" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC8" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC9" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANDESC11" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EFFDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EFFRATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CURRBAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MEMOPSTIND" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTPRIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTINT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTRESV" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTINS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTLCHG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTFEE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTREB" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRNAMTUNA" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TRANCDX" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DRCRFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IMAGEFLAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGSEQNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OLBTRANID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGPROCDT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGBNKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGSUBSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGBATCH" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ORIGRLSAPP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "trandate",
            "trancd",
            "tranmod",
            "tranamt",
            "checknbr",
            "tracenbr",
            "batchident",
            "seqnbr",
            "tellerbrn",
            "tellerid",
            "trandesc",
            "trandescs",
            "trandesc3",
            "trandesc4",
            "trandesc5",
            "trandesc6",
            "trandesc7",
            "trandesc8",
            "trandesc9",
            "trandesc10",
            "trandesc11",
            "effdate",
            "effrate",
            "currbal",
            "memopstind",
            "trnamtprin",
            "trnamtint",
            "trnamtresv",
            "trnamtins",
            "trnamtesc",
            "trnamtlchg",
            "trnamtfee",
            "trnamtreb",
            "trnamtuna",
            "trancdx",
            "drcrflag",
            "imageflag",
            "origseqnbr",
            "olbtranid",
            "origprocdt",
            "origbnknbr",
            "origsrc",
            "origsubsrc",
            "origbatch",
            "origrlsapp"
        })
        public static class TRANSACTION {

            @XmlElement(name = "TRANDATE", required = true)
            protected String trandate;
            @XmlElement(name = "TRANCD", required = true)
            protected String trancd;
            @XmlElement(name = "TRANMOD", required = true)
            protected String tranmod;
            @XmlElement(name = "TRANAMT", required = true)
            protected String tranamt;
            @XmlElement(name = "CHECKNBR", required = true)
            protected String checknbr;
            @XmlElement(name = "TRACENBR", required = true)
            protected String tracenbr;
            @XmlElement(name = "BATCHIDENT", required = true)
            protected String batchident;
            @XmlElement(name = "SEQNBR", required = true)
            protected String seqnbr;
            @XmlElement(name = "TELLERBRN", required = true)
            protected String tellerbrn;
            @XmlElement(name = "TELLERID", required = true)
            protected String tellerid;
            @XmlElement(name = "TRANDESC", required = true)
            protected String trandesc;
            @XmlElement(name = "TRANDESCS", required = true)
            protected String trandescs;
            @XmlElement(name = "TRANDESC3", required = true)
            protected String trandesc3;
            @XmlElement(name = "TRANDESC4", required = true)
            protected String trandesc4;
            @XmlElement(name = "TRANDESC5", required = true)
            protected String trandesc5;
            @XmlElement(name = "TRANDESC6", required = true)
            protected String trandesc6;
            @XmlElement(name = "TRANDESC7", required = true)
            protected String trandesc7;
            @XmlElement(name = "TRANDESC8", required = true)
            protected String trandesc8;
            @XmlElement(name = "TRANDESC9", required = true)
            protected String trandesc9;
            @XmlElement(name = "TRANDESC10", required = true)
            protected String trandesc10;
            @XmlElement(name = "TRANDESC11", required = true)
            protected String trandesc11;
            @XmlElement(name = "EFFDATE", required = true)
            protected String effdate;
            @XmlElement(name = "EFFRATE", required = true)
            protected String effrate;
            @XmlElement(name = "CURRBAL", required = true)
            protected String currbal;
            @XmlElement(name = "MEMOPSTIND", required = true)
            protected String memopstind;
            @XmlElement(name = "TRNAMTPRIN", required = true)
            protected String trnamtprin;
            @XmlElement(name = "TRNAMTINT", required = true)
            protected String trnamtint;
            @XmlElement(name = "TRNAMTRESV", required = true)
            protected String trnamtresv;
            @XmlElement(name = "TRNAMTINS", required = true)
            protected String trnamtins;
            @XmlElement(name = "TRNAMTESC", required = true)
            protected String trnamtesc;
            @XmlElement(name = "TRNAMTLCHG", required = true)
            protected String trnamtlchg;
            @XmlElement(name = "TRNAMTFEE", required = true)
            protected String trnamtfee;
            @XmlElement(name = "TRNAMTREB", required = true)
            protected String trnamtreb;
            @XmlElement(name = "TRNAMTUNA", required = true)
            protected String trnamtuna;
            @XmlElement(name = "TRANCDX", required = true)
            protected String trancdx;
            @XmlElement(name = "DRCRFLAG", required = true)
            protected String drcrflag;
            @XmlElement(name = "IMAGEFLAG", required = true)
            protected String imageflag;
            @XmlElement(name = "ORIGSEQNBR", required = true)
            protected String origseqnbr;
            @XmlElement(name = "OLBTRANID", required = true)
            protected String olbtranid;
            @XmlElement(name = "ORIGPROCDT", required = true)
            protected String origprocdt;
            @XmlElement(name = "ORIGBNKNBR", required = true)
            protected String origbnknbr;
            @XmlElement(name = "ORIGSRC", required = true)
            protected String origsrc;
            @XmlElement(name = "ORIGSUBSRC", required = true)
            protected String origsubsrc;
            @XmlElement(name = "ORIGBATCH", required = true)
            protected String origbatch;
            @XmlElement(name = "ORIGRLSAPP", required = true)
            protected String origrlsapp;

            /**
             * Gets the value of the trandate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDATE() {
                return trandate;
            }

            /**
             * Sets the value of the trandate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDATE(String value) {
                this.trandate = value;
            }

            /**
             * Gets the value of the trancd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANCD() {
                return trancd;
            }

            /**
             * Sets the value of the trancd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANCD(String value) {
                this.trancd = value;
            }

            /**
             * Gets the value of the tranmod property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANMOD() {
                return tranmod;
            }

            /**
             * Sets the value of the tranmod property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANMOD(String value) {
                this.tranmod = value;
            }

            /**
             * Gets the value of the tranamt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANAMT() {
                return tranamt;
            }

            /**
             * Sets the value of the tranamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANAMT(String value) {
                this.tranamt = value;
            }

            /**
             * Gets the value of the checknbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCHECKNBR() {
                return checknbr;
            }

            /**
             * Sets the value of the checknbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCHECKNBR(String value) {
                this.checknbr = value;
            }

            /**
             * Gets the value of the tracenbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRACENBR() {
                return tracenbr;
            }

            /**
             * Sets the value of the tracenbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRACENBR(String value) {
                this.tracenbr = value;
            }

            /**
             * Gets the value of the batchident property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBATCHIDENT() {
                return batchident;
            }

            /**
             * Sets the value of the batchident property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBATCHIDENT(String value) {
                this.batchident = value;
            }

            /**
             * Gets the value of the seqnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSEQNBR() {
                return seqnbr;
            }

            /**
             * Sets the value of the seqnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSEQNBR(String value) {
                this.seqnbr = value;
            }

            /**
             * Gets the value of the tellerbrn property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTELLERBRN() {
                return tellerbrn;
            }

            /**
             * Sets the value of the tellerbrn property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTELLERBRN(String value) {
                this.tellerbrn = value;
            }

            /**
             * Gets the value of the tellerid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTELLERID() {
                return tellerid;
            }

            /**
             * Sets the value of the tellerid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTELLERID(String value) {
                this.tellerid = value;
            }

            /**
             * Gets the value of the trandesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC() {
                return trandesc;
            }

            /**
             * Sets the value of the trandesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC(String value) {
                this.trandesc = value;
            }

            /**
             * Gets the value of the trandescs property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESCS() {
                return trandescs;
            }

            /**
             * Sets the value of the trandescs property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESCS(String value) {
                this.trandescs = value;
            }

            /**
             * Gets the value of the trandesc3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC3() {
                return trandesc3;
            }

            /**
             * Sets the value of the trandesc3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC3(String value) {
                this.trandesc3 = value;
            }

            /**
             * Gets the value of the trandesc4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC4() {
                return trandesc4;
            }

            /**
             * Sets the value of the trandesc4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC4(String value) {
                this.trandesc4 = value;
            }

            /**
             * Gets the value of the trandesc5 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC5() {
                return trandesc5;
            }

            /**
             * Sets the value of the trandesc5 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC5(String value) {
                this.trandesc5 = value;
            }

            /**
             * Gets the value of the trandesc6 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC6() {
                return trandesc6;
            }

            /**
             * Sets the value of the trandesc6 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC6(String value) {
                this.trandesc6 = value;
            }

            /**
             * Gets the value of the trandesc7 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC7() {
                return trandesc7;
            }

            /**
             * Sets the value of the trandesc7 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC7(String value) {
                this.trandesc7 = value;
            }

            /**
             * Gets the value of the trandesc8 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC8() {
                return trandesc8;
            }

            /**
             * Sets the value of the trandesc8 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC8(String value) {
                this.trandesc8 = value;
            }

            /**
             * Gets the value of the trandesc9 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC9() {
                return trandesc9;
            }

            /**
             * Sets the value of the trandesc9 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC9(String value) {
                this.trandesc9 = value;
            }

            /**
             * Gets the value of the trandesc10 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC10() {
                return trandesc10;
            }

            /**
             * Sets the value of the trandesc10 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC10(String value) {
                this.trandesc10 = value;
            }

            /**
             * Gets the value of the trandesc11 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANDESC11() {
                return trandesc11;
            }

            /**
             * Sets the value of the trandesc11 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANDESC11(String value) {
                this.trandesc11 = value;
            }

            /**
             * Gets the value of the effdate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEFFDATE() {
                return effdate;
            }

            /**
             * Sets the value of the effdate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEFFDATE(String value) {
                this.effdate = value;
            }

            /**
             * Gets the value of the effrate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEFFRATE() {
                return effrate;
            }

            /**
             * Sets the value of the effrate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEFFRATE(String value) {
                this.effrate = value;
            }

            /**
             * Gets the value of the currbal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCURRBAL() {
                return currbal;
            }

            /**
             * Sets the value of the currbal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCURRBAL(String value) {
                this.currbal = value;
            }

            /**
             * Gets the value of the memopstind property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMEMOPSTIND() {
                return memopstind;
            }

            /**
             * Sets the value of the memopstind property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMEMOPSTIND(String value) {
                this.memopstind = value;
            }

            /**
             * Gets the value of the trnamtprin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTPRIN() {
                return trnamtprin;
            }

            /**
             * Sets the value of the trnamtprin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTPRIN(String value) {
                this.trnamtprin = value;
            }

            /**
             * Gets the value of the trnamtint property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTINT() {
                return trnamtint;
            }

            /**
             * Sets the value of the trnamtint property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTINT(String value) {
                this.trnamtint = value;
            }

            /**
             * Gets the value of the trnamtresv property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTRESV() {
                return trnamtresv;
            }

            /**
             * Sets the value of the trnamtresv property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTRESV(String value) {
                this.trnamtresv = value;
            }

            /**
             * Gets the value of the trnamtins property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTINS() {
                return trnamtins;
            }

            /**
             * Sets the value of the trnamtins property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTINS(String value) {
                this.trnamtins = value;
            }

            /**
             * Gets the value of the trnamtesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTESC() {
                return trnamtesc;
            }

            /**
             * Sets the value of the trnamtesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTESC(String value) {
                this.trnamtesc = value;
            }

            /**
             * Gets the value of the trnamtlchg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTLCHG() {
                return trnamtlchg;
            }

            /**
             * Sets the value of the trnamtlchg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTLCHG(String value) {
                this.trnamtlchg = value;
            }

            /**
             * Gets the value of the trnamtfee property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTFEE() {
                return trnamtfee;
            }

            /**
             * Sets the value of the trnamtfee property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTFEE(String value) {
                this.trnamtfee = value;
            }

            /**
             * Gets the value of the trnamtreb property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTREB() {
                return trnamtreb;
            }

            /**
             * Sets the value of the trnamtreb property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTREB(String value) {
                this.trnamtreb = value;
            }

            /**
             * Gets the value of the trnamtuna property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRNAMTUNA() {
                return trnamtuna;
            }

            /**
             * Sets the value of the trnamtuna property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRNAMTUNA(String value) {
                this.trnamtuna = value;
            }

            /**
             * Gets the value of the trancdx property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTRANCDX() {
                return trancdx;
            }

            /**
             * Sets the value of the trancdx property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTRANCDX(String value) {
                this.trancdx = value;
            }

            /**
             * Gets the value of the drcrflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDRCRFLAG() {
                return drcrflag;
            }

            /**
             * Sets the value of the drcrflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDRCRFLAG(String value) {
                this.drcrflag = value;
            }

            /**
             * Gets the value of the imageflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIMAGEFLAG() {
                return imageflag;
            }

            /**
             * Sets the value of the imageflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIMAGEFLAG(String value) {
                this.imageflag = value;
            }

            /**
             * Gets the value of the origseqnbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGSEQNBR() {
                return origseqnbr;
            }

            /**
             * Sets the value of the origseqnbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGSEQNBR(String value) {
                this.origseqnbr = value;
            }

            /**
             * Gets the value of the olbtranid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOLBTRANID() {
                return olbtranid;
            }

            /**
             * Sets the value of the olbtranid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOLBTRANID(String value) {
                this.olbtranid = value;
            }

            /**
             * Gets the value of the origprocdt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGPROCDT() {
                return origprocdt;
            }

            /**
             * Sets the value of the origprocdt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGPROCDT(String value) {
                this.origprocdt = value;
            }

            /**
             * Gets the value of the origbnknbr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGBNKNBR() {
                return origbnknbr;
            }

            /**
             * Sets the value of the origbnknbr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGBNKNBR(String value) {
                this.origbnknbr = value;
            }

            /**
             * Gets the value of the origsrc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGSRC() {
                return origsrc;
            }

            /**
             * Sets the value of the origsrc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGSRC(String value) {
                this.origsrc = value;
            }

            /**
             * Gets the value of the origsubsrc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGSUBSRC() {
                return origsubsrc;
            }

            /**
             * Sets the value of the origsubsrc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGSUBSRC(String value) {
                this.origsubsrc = value;
            }

            /**
             * Gets the value of the origbatch property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGBATCH() {
                return origbatch;
            }

            /**
             * Sets the value of the origbatch property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGBATCH(String value) {
                this.origbatch = value;
            }

            /**
             * Gets the value of the origrlsapp property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORIGRLSAPP() {
                return origrlsapp;
            }

            /**
             * Sets the value of the origrlsapp property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORIGRLSAPP(String value) {
                this.origrlsapp = value;
            }

        }

    }

}
