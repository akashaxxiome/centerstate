package com.sap.banking.custom.webservices.customerinquiry;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.0.0-milestone2
 * 2017-04-26T11:33:29.489-03:00
 * Generated source version: 3.0.0-milestone2
 * 
 */
@WebService(targetNamespace = "http://webservices.custom.banking.sap.com/CustomerInquiry/", name = "CustomerInquiry")
@XmlSeeAlso({ObjectFactory.class})
public interface CustomerInquiry {

    @WebMethod(operationName = "CustomerInquiry", action = "http://webservices.custom.banking.sap.com/CustomerInquiry/CustomerInquiry")
    @RequestWrapper(localName = "CustomerInquiry", targetNamespace = "http://webservices.custom.banking.sap.com/CustomerInquiry/", className = "com.sap.banking.custom.webservices.customerinquiry.CustomerInquiry_Type")
    @ResponseWrapper(localName = "CustomerInquiryResponse", targetNamespace = "http://webservices.custom.banking.sap.com/CustomerInquiry/", className = "com.sap.banking.custom.webservices.customerinquiry.CustomerInquiryResponse")
    public void customerInquiry(
        @WebParam(name = "VENDORID", targetNamespace = "")
        java.lang.String vendorid,
        @WebParam(mode = WebParam.Mode.INOUT, name = "SEQUENCE", targetNamespace = "")
        javax.xml.ws.Holder<java.lang.String> sequence,
        @WebParam(name = "CUSTKEY", targetNamespace = "")
        java.lang.String custkey,
        @WebParam(name = "RETRNNACAP", targetNamespace = "")
        java.lang.String retrnnacap,
        @WebParam(mode = WebParam.Mode.OUT, name = "ResponseData", targetNamespace = "")
        javax.xml.ws.Holder<com.sap.banking.custom.webservices.customerinquiry.CustomerInquiryResponse.ResponseData> responseData
    );
}
