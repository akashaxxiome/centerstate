
package com.sap.banking.custom.webservices.customerinquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VENDORID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTKEY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RETRNNACAP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorid",
    "sequence",
    "custkey",
    "retrnnacap"
})
@XmlRootElement(name = "CustomerInquiry")
public class CustomerInquiry_Type {

    @XmlElement(name = "VENDORID", required = true)
    protected String vendorid;
    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "CUSTKEY", required = true)
    protected String custkey;
    @XmlElement(name = "RETRNNACAP", required = true)
    protected String retrnnacap;

    /**
     * Gets the value of the vendorid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVENDORID() {
        return vendorid;
    }

    /**
     * Sets the value of the vendorid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVENDORID(String value) {
        this.vendorid = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the custkey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTKEY() {
        return custkey;
    }

    /**
     * Sets the value of the custkey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTKEY(String value) {
        this.custkey = value;
    }

    /**
     * Gets the value of the retrnnacap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRETRNNACAP() {
        return retrnnacap;
    }

    /**
     * Sets the value of the retrnnacap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRETRNNACAP(String value) {
        this.retrnnacap = value;
    }

}
