
package com.sap.banking.custom.webservices.transactioninquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VENDORID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DTSELTOPT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BEGINDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ENDDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FIRSTNEXT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NBRRECTORT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SEQOPT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="REQTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RTNMEMOPST" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RTNPCKGPST" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorid",
    "sequence",
    "applcd",
    "actnbr",
    "dtseltopt",
    "begindate",
    "enddate",
    "firstnext",
    "nbrrectort",
    "seqopt",
    "reqtype",
    "rtnmemopst",
    "rtnpckgpst"
})
@XmlRootElement(name = "TransactionInquiry")
public class TransactionInquiry_Type {

    @XmlElement(name = "VENDORID", required = true)
    protected String vendorid;
    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "APPLCD", required = true)
    protected String applcd;
    @XmlElement(name = "ACTNBR", required = true)
    protected String actnbr;
    @XmlElement(name = "DTSELTOPT", required = true)
    protected String dtseltopt;
    @XmlElement(name = "BEGINDATE", required = true)
    protected String begindate;
    @XmlElement(name = "ENDDATE", required = true)
    protected String enddate;
    @XmlElement(name = "FIRSTNEXT", required = true)
    protected String firstnext;
    @XmlElement(name = "NBRRECTORT", required = true)
    protected String nbrrectort;
    @XmlElement(name = "SEQOPT", required = true)
    protected String seqopt;
    @XmlElement(name = "REQTYPE", required = true)
    protected String reqtype;
    @XmlElement(name = "RTNMEMOPST", required = true)
    protected String rtnmemopst;
    @XmlElement(name = "RTNPCKGPST", required = true)
    protected String rtnpckgpst;

    /**
     * Gets the value of the vendorid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVENDORID() {
        return vendorid;
    }

    /**
     * Sets the value of the vendorid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVENDORID(String value) {
        this.vendorid = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the applcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPPLCD() {
        return applcd;
    }

    /**
     * Sets the value of the applcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPPLCD(String value) {
        this.applcd = value;
    }

    /**
     * Gets the value of the actnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTNBR() {
        return actnbr;
    }

    /**
     * Sets the value of the actnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTNBR(String value) {
        this.actnbr = value;
    }

    /**
     * Gets the value of the dtseltopt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDTSELTOPT() {
        return dtseltopt;
    }

    /**
     * Sets the value of the dtseltopt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDTSELTOPT(String value) {
        this.dtseltopt = value;
    }

    /**
     * Gets the value of the begindate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBEGINDATE() {
        return begindate;
    }

    /**
     * Sets the value of the begindate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBEGINDATE(String value) {
        this.begindate = value;
    }

    /**
     * Gets the value of the enddate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENDDATE() {
        return enddate;
    }

    /**
     * Sets the value of the enddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENDDATE(String value) {
        this.enddate = value;
    }

    /**
     * Gets the value of the firstnext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIRSTNEXT() {
        return firstnext;
    }

    /**
     * Sets the value of the firstnext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIRSTNEXT(String value) {
        this.firstnext = value;
    }

    /**
     * Gets the value of the nbrrectort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNBRRECTORT() {
        return nbrrectort;
    }

    /**
     * Sets the value of the nbrrectort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNBRRECTORT(String value) {
        this.nbrrectort = value;
    }

    /**
     * Gets the value of the seqopt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQOPT() {
        return seqopt;
    }

    /**
     * Sets the value of the seqopt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQOPT(String value) {
        this.seqopt = value;
    }

    /**
     * Gets the value of the reqtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREQTYPE() {
        return reqtype;
    }

    /**
     * Sets the value of the reqtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREQTYPE(String value) {
        this.reqtype = value;
    }

    /**
     * Gets the value of the rtnmemopst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRTNMEMOPST() {
        return rtnmemopst;
    }

    /**
     * Sets the value of the rtnmemopst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRTNMEMOPST(String value) {
        this.rtnmemopst = value;
    }

    /**
     * Gets the value of the rtnpckgpst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRTNPCKGPST() {
        return rtnpckgpst;
    }

    /**
     * Sets the value of the rtnpckgpst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRTNPCKGPST(String value) {
        this.rtnpckgpst = value;
    }

}
