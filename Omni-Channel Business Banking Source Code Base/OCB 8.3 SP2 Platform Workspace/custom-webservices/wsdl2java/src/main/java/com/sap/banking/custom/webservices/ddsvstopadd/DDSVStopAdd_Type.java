
package com.sap.banking.custom.webservices.ddsvstopadd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VENDORID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="APPLCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ACTNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TRANCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXPDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHECKNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHECKDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PAYEEDESC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMTLIMIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LOWCHKRNG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HGHCHKRNG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="COMPANYID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NBRFLTDAYS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ORUSERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WRKSTNID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MODECODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DLTMCHCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="REQTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHGPYMTFEE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHGPYMTAMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INDIVID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RETRNRESN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DTDEATH" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ELEMENTSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SUBSRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OVRDFLG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STPHLDTYP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorid",
    "sequence",
    "applcd",
    "actnbr",
    "trancd",
    "amount",
    "expdate",
    "checknbr",
    "checkdate",
    "payeedesc",
    "amtlimit",
    "lowchkrng",
    "hghchkrng",
    "companyid",
    "nbrfltdays",
    "userid",
    "oruserid",
    "wrkstnid",
    "modecode",
    "dltmchcd",
    "reqtype",
    "chgpymtfee",
    "chgpymtamt",
    "individ",
    "retrnresn",
    "dtdeath",
    "elementsrc",
    "subsrc",
    "ovrdflg",
    "stphldtyp"
})
@XmlRootElement(name = "DDSVStopAdd")
public class DDSVStopAdd_Type {

    @XmlElement(name = "VENDORID", required = true)
    protected String vendorid;
    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "APPLCD", required = true)
    protected String applcd;
    @XmlElement(name = "ACTNBR", required = true)
    protected String actnbr;
    @XmlElement(name = "TRANCD", required = true)
    protected String trancd;
    @XmlElement(name = "AMOUNT", required = true)
    protected String amount;
    @XmlElement(name = "EXPDATE", required = true)
    protected String expdate;
    @XmlElement(name = "CHECKNBR", required = true)
    protected String checknbr;
    @XmlElement(name = "CHECKDATE", required = true)
    protected String checkdate;
    @XmlElement(name = "PAYEEDESC", required = true)
    protected String payeedesc;
    @XmlElement(name = "AMTLIMIT", required = true)
    protected String amtlimit;
    @XmlElement(name = "LOWCHKRNG", required = true)
    protected String lowchkrng;
    @XmlElement(name = "HGHCHKRNG", required = true)
    protected String hghchkrng;
    @XmlElement(name = "COMPANYID", required = true)
    protected String companyid;
    @XmlElement(name = "NBRFLTDAYS", required = true)
    protected String nbrfltdays;
    @XmlElement(name = "USERID", required = true)
    protected String userid;
    @XmlElement(name = "ORUSERID", required = true)
    protected String oruserid;
    @XmlElement(name = "WRKSTNID", required = true)
    protected String wrkstnid;
    @XmlElement(name = "MODECODE", required = true)
    protected String modecode;
    @XmlElement(name = "DLTMCHCD", required = true)
    protected String dltmchcd;
    @XmlElement(name = "REQTYPE", required = true)
    protected String reqtype;
    @XmlElement(name = "CHGPYMTFEE", required = true)
    protected String chgpymtfee;
    @XmlElement(name = "CHGPYMTAMT", required = true)
    protected String chgpymtamt;
    @XmlElement(name = "INDIVID", required = true)
    protected String individ;
    @XmlElement(name = "RETRNRESN", required = true)
    protected String retrnresn;
    @XmlElement(name = "DTDEATH", required = true)
    protected String dtdeath;
    @XmlElement(name = "ELEMENTSRC", required = true)
    protected String elementsrc;
    @XmlElement(name = "SUBSRC", required = true)
    protected String subsrc;
    @XmlElement(name = "OVRDFLG", required = true)
    protected String ovrdflg;
    @XmlElement(name = "STPHLDTYP", required = true)
    protected String stphldtyp;

    /**
     * Gets the value of the vendorid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVENDORID() {
        return vendorid;
    }

    /**
     * Sets the value of the vendorid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVENDORID(String value) {
        this.vendorid = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the applcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPPLCD() {
        return applcd;
    }

    /**
     * Sets the value of the applcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPPLCD(String value) {
        this.applcd = value;
    }

    /**
     * Gets the value of the actnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTNBR() {
        return actnbr;
    }

    /**
     * Sets the value of the actnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTNBR(String value) {
        this.actnbr = value;
    }

    /**
     * Gets the value of the trancd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRANCD() {
        return trancd;
    }

    /**
     * Sets the value of the trancd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRANCD(String value) {
        this.trancd = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMOUNT(String value) {
        this.amount = value;
    }

    /**
     * Gets the value of the expdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPDATE() {
        return expdate;
    }

    /**
     * Sets the value of the expdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPDATE(String value) {
        this.expdate = value;
    }

    /**
     * Gets the value of the checknbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHECKNBR() {
        return checknbr;
    }

    /**
     * Sets the value of the checknbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHECKNBR(String value) {
        this.checknbr = value;
    }

    /**
     * Gets the value of the checkdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHECKDATE() {
        return checkdate;
    }

    /**
     * Sets the value of the checkdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHECKDATE(String value) {
        this.checkdate = value;
    }

    /**
     * Gets the value of the payeedesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYEEDESC() {
        return payeedesc;
    }

    /**
     * Sets the value of the payeedesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYEEDESC(String value) {
        this.payeedesc = value;
    }

    /**
     * Gets the value of the amtlimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMTLIMIT() {
        return amtlimit;
    }

    /**
     * Sets the value of the amtlimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMTLIMIT(String value) {
        this.amtlimit = value;
    }

    /**
     * Gets the value of the lowchkrng property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOWCHKRNG() {
        return lowchkrng;
    }

    /**
     * Sets the value of the lowchkrng property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOWCHKRNG(String value) {
        this.lowchkrng = value;
    }

    /**
     * Gets the value of the hghchkrng property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHGHCHKRNG() {
        return hghchkrng;
    }

    /**
     * Sets the value of the hghchkrng property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHGHCHKRNG(String value) {
        this.hghchkrng = value;
    }

    /**
     * Gets the value of the companyid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMPANYID() {
        return companyid;
    }

    /**
     * Sets the value of the companyid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMPANYID(String value) {
        this.companyid = value;
    }

    /**
     * Gets the value of the nbrfltdays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNBRFLTDAYS() {
        return nbrfltdays;
    }

    /**
     * Sets the value of the nbrfltdays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNBRFLTDAYS(String value) {
        this.nbrfltdays = value;
    }

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSERID() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERID(String value) {
        this.userid = value;
    }

    /**
     * Gets the value of the oruserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORUSERID() {
        return oruserid;
    }

    /**
     * Sets the value of the oruserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORUSERID(String value) {
        this.oruserid = value;
    }

    /**
     * Gets the value of the wrkstnid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWRKSTNID() {
        return wrkstnid;
    }

    /**
     * Sets the value of the wrkstnid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWRKSTNID(String value) {
        this.wrkstnid = value;
    }

    /**
     * Gets the value of the modecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODECODE() {
        return modecode;
    }

    /**
     * Sets the value of the modecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODECODE(String value) {
        this.modecode = value;
    }

    /**
     * Gets the value of the dltmchcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDLTMCHCD() {
        return dltmchcd;
    }

    /**
     * Sets the value of the dltmchcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDLTMCHCD(String value) {
        this.dltmchcd = value;
    }

    /**
     * Gets the value of the reqtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREQTYPE() {
        return reqtype;
    }

    /**
     * Sets the value of the reqtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREQTYPE(String value) {
        this.reqtype = value;
    }

    /**
     * Gets the value of the chgpymtfee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGPYMTFEE() {
        return chgpymtfee;
    }

    /**
     * Sets the value of the chgpymtfee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGPYMTFEE(String value) {
        this.chgpymtfee = value;
    }

    /**
     * Gets the value of the chgpymtamt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGPYMTAMT() {
        return chgpymtamt;
    }

    /**
     * Sets the value of the chgpymtamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGPYMTAMT(String value) {
        this.chgpymtamt = value;
    }

    /**
     * Gets the value of the individ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDIVID() {
        return individ;
    }

    /**
     * Sets the value of the individ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDIVID(String value) {
        this.individ = value;
    }

    /**
     * Gets the value of the retrnresn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRETRNRESN() {
        return retrnresn;
    }

    /**
     * Sets the value of the retrnresn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRETRNRESN(String value) {
        this.retrnresn = value;
    }

    /**
     * Gets the value of the dtdeath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDTDEATH() {
        return dtdeath;
    }

    /**
     * Sets the value of the dtdeath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDTDEATH(String value) {
        this.dtdeath = value;
    }

    /**
     * Gets the value of the elementsrc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELEMENTSRC() {
        return elementsrc;
    }

    /**
     * Sets the value of the elementsrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELEMENTSRC(String value) {
        this.elementsrc = value;
    }

    /**
     * Gets the value of the subsrc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBSRC() {
        return subsrc;
    }

    /**
     * Sets the value of the subsrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBSRC(String value) {
        this.subsrc = value;
    }

    /**
     * Gets the value of the ovrdflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOVRDFLG() {
        return ovrdflg;
    }

    /**
     * Sets the value of the ovrdflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOVRDFLG(String value) {
        this.ovrdflg = value;
    }

    /**
     * Gets the value of the stphldtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTPHLDTYP() {
        return stphldtyp;
    }

    /**
     * Sets the value of the stphldtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTPHLDTYP(String value) {
        this.stphldtyp = value;
    }

}
