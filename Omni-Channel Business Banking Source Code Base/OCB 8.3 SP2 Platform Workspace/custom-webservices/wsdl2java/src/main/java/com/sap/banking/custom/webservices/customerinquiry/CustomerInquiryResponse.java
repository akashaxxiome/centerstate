
package com.sap.banking.custom.webservices.customerinquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CUSTKEY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NALINE1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NALINE2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NALINE3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NALINE4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NALINE5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NALINE6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FIRSTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MIDDLENAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="LASTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NPERLINE1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NPERLINE2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TAXIDNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TAXIDTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AREACODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PHONENBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSIDERCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="VIPCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SERVICEMBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RECS_RETURNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MOREDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sequence",
    "responseData"
})
@XmlRootElement(name = "CustomerInquiryResponse")
public class CustomerInquiryResponse {

    @XmlElement(name = "SEQUENCE", required = true)
    protected String sequence;
    @XmlElement(name = "ResponseData", required = true)
    protected CustomerInquiryResponse.ResponseData responseData;

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerInquiryResponse.ResponseData }
     *     
     */
    public CustomerInquiryResponse.ResponseData getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInquiryResponse.ResponseData }
     *     
     */
    public void setResponseData(CustomerInquiryResponse.ResponseData value) {
        this.responseData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CUSTKEY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NALINE1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NALINE2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NALINE3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NALINE4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NALINE5" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NALINE6" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FIRSTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MIDDLENAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LASTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NPERLINE1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NPERLINE2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SHORTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TAXIDNBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TAXIDTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AREACODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PHONENBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSIDERCD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="VIPCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SERVICEMBR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RECS_RETURNED" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MOREDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custkey",
        "naline1",
        "naline2",
        "naline3",
        "naline4",
        "naline5",
        "naline6",
        "firstname",
        "middlename",
        "lastname",
        "nperline1",
        "nperline2",
        "shortname",
        "taxidnbr",
        "taxidtype",
        "areacode",
        "phonenbr",
        "dob",
        "insidercd",
        "vipcode",
        "servicembr",
        "recsreturned",
        "moredata"
    })
    public static class ResponseData {

        @XmlElement(name = "CUSTKEY", required = true)
        protected String custkey;
        @XmlElement(name = "NALINE1", required = true)
        protected String naline1;
        @XmlElement(name = "NALINE2", required = true)
        protected String naline2;
        @XmlElement(name = "NALINE3", required = true)
        protected String naline3;
        @XmlElement(name = "NALINE4", required = true)
        protected String naline4;
        @XmlElement(name = "NALINE5", required = true)
        protected String naline5;
        @XmlElement(name = "NALINE6", required = true)
        protected String naline6;
        @XmlElement(name = "FIRSTNAME", required = true)
        protected String firstname;
        @XmlElement(name = "MIDDLENAME", required = true)
        protected String middlename;
        @XmlElement(name = "LASTNAME", required = true)
        protected String lastname;
        @XmlElement(name = "NPERLINE1", required = true)
        protected String nperline1;
        @XmlElement(name = "NPERLINE2", required = true)
        protected String nperline2;
        @XmlElement(name = "SHORTNAME", required = true)
        protected String shortname;
        @XmlElement(name = "TAXIDNBR", required = true)
        protected String taxidnbr;
        @XmlElement(name = "TAXIDTYPE", required = true)
        protected String taxidtype;
        @XmlElement(name = "AREACODE", required = true)
        protected String areacode;
        @XmlElement(name = "PHONENBR", required = true)
        protected String phonenbr;
        @XmlElement(name = "DOB", required = true)
        protected String dob;
        @XmlElement(name = "INSIDERCD", required = true)
        protected String insidercd;
        @XmlElement(name = "VIPCODE", required = true)
        protected String vipcode;
        @XmlElement(name = "SERVICEMBR", required = true)
        protected String servicembr;
        @XmlElement(name = "RECS_RETURNED", required = true)
        protected String recsreturned;
        @XmlElement(name = "MOREDATA", required = true)
        protected String moredata;

        /**
         * Gets the value of the custkey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUSTKEY() {
            return custkey;
        }

        /**
         * Sets the value of the custkey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUSTKEY(String value) {
            this.custkey = value;
        }

        /**
         * Gets the value of the naline1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNALINE1() {
            return naline1;
        }

        /**
         * Sets the value of the naline1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNALINE1(String value) {
            this.naline1 = value;
        }

        /**
         * Gets the value of the naline2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNALINE2() {
            return naline2;
        }

        /**
         * Sets the value of the naline2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNALINE2(String value) {
            this.naline2 = value;
        }

        /**
         * Gets the value of the naline3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNALINE3() {
            return naline3;
        }

        /**
         * Sets the value of the naline3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNALINE3(String value) {
            this.naline3 = value;
        }

        /**
         * Gets the value of the naline4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNALINE4() {
            return naline4;
        }

        /**
         * Sets the value of the naline4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNALINE4(String value) {
            this.naline4 = value;
        }

        /**
         * Gets the value of the naline5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNALINE5() {
            return naline5;
        }

        /**
         * Sets the value of the naline5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNALINE5(String value) {
            this.naline5 = value;
        }

        /**
         * Gets the value of the naline6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNALINE6() {
            return naline6;
        }

        /**
         * Sets the value of the naline6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNALINE6(String value) {
            this.naline6 = value;
        }

        /**
         * Gets the value of the firstname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFIRSTNAME() {
            return firstname;
        }

        /**
         * Sets the value of the firstname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFIRSTNAME(String value) {
            this.firstname = value;
        }

        /**
         * Gets the value of the middlename property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMIDDLENAME() {
            return middlename;
        }

        /**
         * Sets the value of the middlename property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMIDDLENAME(String value) {
            this.middlename = value;
        }

        /**
         * Gets the value of the lastname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLASTNAME() {
            return lastname;
        }

        /**
         * Sets the value of the lastname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLASTNAME(String value) {
            this.lastname = value;
        }

        /**
         * Gets the value of the nperline1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNPERLINE1() {
            return nperline1;
        }

        /**
         * Sets the value of the nperline1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNPERLINE1(String value) {
            this.nperline1 = value;
        }

        /**
         * Gets the value of the nperline2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNPERLINE2() {
            return nperline2;
        }

        /**
         * Sets the value of the nperline2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNPERLINE2(String value) {
            this.nperline2 = value;
        }

        /**
         * Gets the value of the shortname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTNAME() {
            return shortname;
        }

        /**
         * Sets the value of the shortname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTNAME(String value) {
            this.shortname = value;
        }

        /**
         * Gets the value of the taxidnbr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTAXIDNBR() {
            return taxidnbr;
        }

        /**
         * Sets the value of the taxidnbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTAXIDNBR(String value) {
            this.taxidnbr = value;
        }

        /**
         * Gets the value of the taxidtype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTAXIDTYPE() {
            return taxidtype;
        }

        /**
         * Sets the value of the taxidtype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTAXIDTYPE(String value) {
            this.taxidtype = value;
        }

        /**
         * Gets the value of the areacode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAREACODE() {
            return areacode;
        }

        /**
         * Sets the value of the areacode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAREACODE(String value) {
            this.areacode = value;
        }

        /**
         * Gets the value of the phonenbr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPHONENBR() {
            return phonenbr;
        }

        /**
         * Sets the value of the phonenbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPHONENBR(String value) {
            this.phonenbr = value;
        }

        /**
         * Gets the value of the dob property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOB() {
            return dob;
        }

        /**
         * Sets the value of the dob property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOB(String value) {
            this.dob = value;
        }

        /**
         * Gets the value of the insidercd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSIDERCD() {
            return insidercd;
        }

        /**
         * Sets the value of the insidercd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSIDERCD(String value) {
            this.insidercd = value;
        }

        /**
         * Gets the value of the vipcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIPCODE() {
            return vipcode;
        }

        /**
         * Sets the value of the vipcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIPCODE(String value) {
            this.vipcode = value;
        }

        /**
         * Gets the value of the servicembr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSERVICEMBR() {
            return servicembr;
        }

        /**
         * Sets the value of the servicembr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSERVICEMBR(String value) {
            this.servicembr = value;
        }

        /**
         * Gets the value of the recsreturned property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRECSRETURNED() {
            return recsreturned;
        }

        /**
         * Sets the value of the recsreturned property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRECSRETURNED(String value) {
            this.recsreturned = value;
        }

        /**
         * Gets the value of the moredata property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMOREDATA() {
            return moredata;
        }

        /**
         * Sets the value of the moredata property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMOREDATA(String value) {
            this.moredata = value;
        }

    }

}
