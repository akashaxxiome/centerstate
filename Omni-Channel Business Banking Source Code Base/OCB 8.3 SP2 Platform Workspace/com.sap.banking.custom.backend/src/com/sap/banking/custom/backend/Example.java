package com.sap.banking.custom.backend;

import static org.osgi.service.log.LogService.LOG_INFO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.osgi.service.log.LogService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Example can implement the Interface exposed from API project
 * 
 * e.g. ExampleI interface can remain in example.api bundle then Example will provide the service implementation of
 * ExmpaleI interface
 *
 */
public class Example {

	@Autowired
	private LogService logger;

	@PostConstruct
	public void initialize() {
		logger.log(LOG_INFO, "Initialized Example Service");
	}

	@PreDestroy
	public void shutdown() {
		logger.log(LOG_INFO, "Stopping Example Service");
	}
	
	
	//TODO provide methods or implementation of the class
	
	
}
