package com.sap.banking.custom.backend;

import com.ffusion.banksim.BankSim;
import com.ffusion.banksim.interfaces.BSException;
import com.ffusion.banksim.interfaces.BankingBackend;
import com.ffusion.banksim.interfaces.RealTimeBankingBackend;
import com.ffusion.beans.Bank;
import com.ffusion.beans.Contact;
import com.ffusion.beans.DateTime;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.AccountHistories;
import com.ffusion.beans.accounts.AccountHistory;
import com.ffusion.beans.accounts.AccountSummaries;
import com.ffusion.beans.accounts.AccountSummary;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.accounts.AssetAcctSummary;
import com.ffusion.beans.accounts.CreditCardAcctSummary;
import com.ffusion.beans.accounts.DepositAcctSummary;
import com.ffusion.beans.accounts.ExtendedAccountSummaries;
import com.ffusion.beans.accounts.ExtendedAccountSummary;
import com.ffusion.beans.accounts.FixedDepositInstrument;
import com.ffusion.beans.accounts.FixedDepositInstruments;
import com.ffusion.beans.accounts.LoanAcctSummary;
import com.ffusion.beans.banking.Transaction;
import com.ffusion.beans.banking.Transactions;
import com.ffusion.beans.banking.Transfer;
import com.ffusion.beans.common.Currency;
import com.ffusion.beans.messages.Message;
import com.ffusion.beans.user.User;
import com.ffusion.util.beans.PagingContext;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class CustomRealTimeBankingBackendImpl implements RealTimeBankingBackend {

	@Override
	public void addAccount(User arg0, Account arg1) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAccounts(User arg0, Account[] arg1) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBPWTransfer(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5,
			String arg6) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBPWTransfer(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5,
			String arg6, String arg7, String arg8, int arg9) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBank(Bank arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBankAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBanks(Bank[] arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCustomer(User arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCustomers(User[] arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMailMessage(User arg0, Message arg1) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Transfer addTransfer(Transfer arg0, int arg1) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transfer[] addTransfers(Transfer[] arg0, int[] arg1) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changePIN(String arg0, String arg1) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closePagedTransactions(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteBank(Bank arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteBankAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteCustomer(User arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Account getAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transactions getAccountTransactions(Account arg0, PagingContext arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getAccounts(User arg0) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bank getBank(String arg0) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getBankAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getBankingBackendType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExtendedAccountSummaries getExtendedSummary(Account arg0, Calendar arg1, Calendar arg2, HashMap arg3)
			throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transactions getFDInstrumentTransactions(FixedDepositInstrument arg0, Calendar arg1, Calendar arg2,
			HashMap arg3) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FixedDepositInstruments getFixedDepositInstruments(Account arg0, Calendar arg1, Calendar arg2, HashMap arg3)
			throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountHistories getHistory(Account arg0, Calendar arg1, Calendar arg2, HashMap arg3) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountHistories getHistory(Accounts arg0, Calendar arg1, Calendar arg2, String arg3, HashMap arg4)
			throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getMailMessages(User arg0) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getNextPage(Account arg0, int arg1) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getNextPage(Account arg0, int arg1, int arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transactions getNextTransactions(Account arg0, PagingContext arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberOfTransactions(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Transactions getPagedTransactions(Account arg0, PagingContext arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getPrevPage(Account arg0, int arg1) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getPrevPage(Account arg0, int arg1, int arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transactions getPreviousTransactions(Account arg0, PagingContext arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transactions getSpecificPage(Account arg0, PagingContext arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountSummaries getSummary(Account arg0, Calendar arg1, Calendar arg2, HashMap arg3) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountSummaries getSummary(Accounts arg0, Calendar arg1, Calendar arg2, String arg3, HashMap arg4)
			throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transaction getTransactionById(Account arg0, String arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getTransactions(Account arg0, Calendar arg1, Calendar arg2) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isInitialized() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void openPagedTransactions(Account arg0, Calendar arg1, Calendar arg2) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openPagedTransactions(Account arg0, PagingContext arg1, HashMap arg2) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPassword(User arg0, String arg1) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User signOn(String arg0, String arg1) throws BSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateAccounts(Account[] arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBank(Bank arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBankAccount(Account arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBanks(Bank[] arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCustomer(User arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCustomers(User[] arg0) throws BSException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFixedDepositInstrument(FixedDepositInstrument arg0, HashMap arg1) throws BSException {
		// TODO Auto-generated method stub
		
	}

}
