package com.sap.banking.custom.common.utils.accounts;

import java.io.InputStream;
import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import com.ffusion.util.ResourceUtil;
import com.sap.banking.custom.common.utils.accounts.interfaces.AccountTypeUtils;
import com.sap.banking.custom.common.utils.accountsutils.AccountTypeManagerUtils;


public class AccountTypeUtilsImpl implements AccountTypeUtils {

	private static Logger m_logger = LoggerFactory.getLogger(AccountTypeUtilsImpl.class);
	private static Document m_xmlDocument = null;
	
	@PostConstruct
	public void loadAccountsTypes() {
		
		final String _thisMethod = "AccountTypeUtilsImpl.loadAccountsTypes";
		
		System.out.println("******************* Start Executing loadAccountsType *******************");
	
		try{
			InputStream _file = ResourceUtil.getResourceAsStream(new Object(), "AccountTypesData.xml");
			DocumentBuilderFactory _factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder _builder = _factory.newDocumentBuilder();
			InputSource _is = new InputSource(_file);
			m_xmlDocument = _builder.parse(_is);
		    AccountTypeManagerUtils.setXmlDocument(m_xmlDocument);
		    System.out.println("******************* End Executing loadAccountsType *******************");
		}catch(Exception ex){
			m_logger.error(_thisMethod + " " + ex.getMessage());
		}
	}
}
