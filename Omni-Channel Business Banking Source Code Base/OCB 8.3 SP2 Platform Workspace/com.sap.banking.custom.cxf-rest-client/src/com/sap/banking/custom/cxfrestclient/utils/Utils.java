package com.sap.banking.custom.cxfrestclient.utils;

import java.util.Base64;
import java.util.Collections;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class Utils {

	private static Logger logger = LoggerFactory.getLogger(Utils.class);
	
	public static boolean isTestRESTWSAvailable() {
		
		boolean _result = false;
		String _thisMethod = "Utils.isTestRESTWSAvailable";
		String _value = System.getProperty("restws.mock.available");
		
		if(_value != null && "true".equals(_value.toLowerCase())){
		   _result = true;
		   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Test REST WS is available");
		}
		else {
		   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Test REST WS is NOT available");	
		}
		return _result;
	}
	
	public static boolean isPOCAvailable() {
	
		boolean _result = false;
		String _thisMethod = "Utils.isPOCAvailable";
		String _value = System.getProperty("coastcapital.poc.available");
		
		if(_value != null && "true".equals(_value.toLowerCase())){
		   _result = true;
		   
		   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Coast Capital POC is available");
		}
		else {
		   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Coast Capital POC is NOT available");
		}
		return _result;
	}
	
	public static WebClient getTestWebClient(String path) {
		
		WebClient _result = null;
		String _thisMethod = "Utils.getTestWebClient";
		
		try{
			String _baseURL = System.getProperty("restws.mock.baseurl");
			
			WebClient _webClient = WebClient.create(_baseURL, 
					               Collections.singletonList(new JacksonJsonProvider()))
	                               .path(path).accept(MediaType.APPLICATION_JSON_TYPE);
	        
	        WebClient.getConfig(_webClient).getHttpConduit().getClient().setReceiveTimeout(100000);
	        _webClient.type(MediaType.APPLICATION_JSON_TYPE);
	        _result = _webClient;
	        
	        logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - BASE URL: " + _baseURL);
	        logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - PATH: " + path);
	        
		}catch(Exception ex){
			logger.debug("Test REST WebClient created");
			logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Test REST WebClient created");
			logger.error(_thisMethod + " - " + ex.getMessage());
		}
        return _result;
	}

	public static WebClient getPOCWebClient(String path) {
		
		String _id = System.getProperty("coastcapital.poc.authentication.basic.id");
		String _secret = System.getProperty("coastcapital.poc.authentication.basic.secret");
		String _baseURL = System.getProperty("coastcapital.poc.restws.baseurl");
		logger.error("IS DEBUG NOT ERROR: Using getPOCWebClient" );
		return getPOCWebClientGeneric(path, _id, _secret, _baseURL);
	}
	
	public static WebClient getPOCWebClientCustomers(String path) {
		
		String _id = System.getProperty("coastcapital.poc.customers.authentication.basic.id");
		String _secret = System.getProperty("coastcapital.poc.customers.authentication.basic.secret");
		String _baseURL = System.getProperty("coastcapital.poc.restws.customers.baseurl");
		 logger.error("IS DEBUG NOT ERROR: Using getPOCWebClientCustomers" );
		return getPOCWebClientGeneric(path, _id, _secret, _baseURL);
	}

	private static WebClient getPOCWebClientGeneric(String path, String _id, String _secret, String _baseURL) {
		WebClient _result = null;
		String _thisMethod = "Utils.getPOCWebClientGeneric";
		
		try{
	 		String _credentials = Base64.getEncoder().encodeToString((_id + ":" + _secret).getBytes());
			WebClient _webClient = WebClient.create(_baseURL, 
					               Collections.singletonList(new JacksonJsonProvider()))
	                               .path(path).accept(MediaType.APPLICATION_JSON_TYPE);
	        
	        WebClient.getConfig(_webClient).getHttpConduit().getClient().setReceiveTimeout(100000);
	        _webClient.header("Authorization","Basic " + _credentials);
	        _webClient.type(MediaType.APPLICATION_JSON_TYPE);
	        _result = _webClient;
	        
	        logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - BASE URL: " + _baseURL);
	        logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - PATH: " + path);
	        
		}catch(Exception ex){
			logger.error(_thisMethod + " - " + ex.getMessage());
		}
        return _result;
	}
	
	
}
