package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.interfaces;

import java.util.List;

import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateCustomerRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateMembershipRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateMembershipResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateProfileRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateProfileResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateShareAccountRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateShareAccountResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CustomTransaction;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.GetCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.UpdateCustomerRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.UpdateCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.ValidateSINResponse;


public abstract interface CoastCapitalPOC {

	public abstract String echoTest();
	
	public abstract List<CustomTransaction> getTransactionHistoryByAccountId(String accountId, String postingDate, String postingDateOperand);
	
	public abstract GetCustomerResponse getCustomerById(String customerId);
	
	public List<GetCustomerResponse> getCustomers(String membership, String pan, String sin, String lastName,
    											  String firstName, String accountNumber);
	
	public CreateCustomerResponse createCustomer(CreateCustomerRequest customer);
	
	public UpdateCustomerResponse updateCustomer(String customerId, UpdateCustomerRequest customer);
	
	public CreateShareAccountResponse createShareAccount(String customerId, CreateShareAccountRequest shareAccount);
	
	public CreateMembershipResponse createMembership(String customerId, CreateMembershipRequest membership);
	
	public CreateProfileResponse createProfile(CreateProfileRequest profile);
	
	public ValidateSINResponse validateSIN(String sin);
}
