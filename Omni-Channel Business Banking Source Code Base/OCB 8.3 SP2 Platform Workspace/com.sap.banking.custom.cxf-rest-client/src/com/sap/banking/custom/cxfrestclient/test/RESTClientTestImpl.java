package com.sap.banking.custom.cxfrestclient.test;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;

import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.interfaces.CoastCapitalPOCRESTApi;
import com.sap.banking.custom.cxfrestclient.test.interfaces.RESTClientTest;

public class RESTClientTestImpl implements RESTClientTest {

	public String echoTest() {
		
		String _result = "";
		
		String _baseURL = "http://localhost:8090/com.sap.banking.custom.rest.backend/services";		
		CoastCapitalPOCRESTApi _IBankingAccountsBackendService = 
				JAXRSClientFactory.create(_baseURL, CoastCapitalPOCRESTApi.class);
		
		if(_IBankingAccountsBackendService != null) {
		   _result = _IBankingAccountsBackendService.echoTest();
		}
		
		return _result;
	}
}
