package com.sap.banking.custom.cxfrestclient.coastcapitalpoc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.interfaces.CoastCapitalPOC;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.interfaces.CoastCapitalPOCRESTApi;
import com.sap.banking.custom.cxfrestclient.utils.Utils;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateCustomerRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateMembershipRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateMembershipResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateProfileRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateProfileResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateShareAccountRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CreateShareAccountResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CustomTransaction;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.GetCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.UpdateCustomerRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.UpdateCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.ValidateSINResponse;

public class CoastCapitalPOCRESTClientImpl implements CoastCapitalPOC {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public String echoTest() {
		String _result = "";		
		String _baseURL = "http://localhost:8090/com.sap.banking.custom.rest.backend/services";		
		CoastCapitalPOCRESTApi _IBankingAccountsBackendService = 
				JAXRSClientFactory.create(_baseURL, CoastCapitalPOCRESTApi.class);
		
		if(_IBankingAccountsBackendService != null) {
		   _result = _IBankingAccountsBackendService.echoTest();
		}
		
		return _result;
	}

	@Override
	public List<CustomTransaction> getTransactionHistoryByAccountId(String accountId, String postingDate,
			String postingDateOperand) {
		
		List<CustomTransaction> _transactionList = new ArrayList<CustomTransaction>();
		String _thisMethod = "CoastCapitalPOCRESTClientImpl.getTransactionHistoryByAccountId";
		
		try {
			
			WebClient _webClient = null;
			if(Utils.isPOCAvailable()) {
			   _webClient = Utils.getPOCWebClient("accounts/" + accountId + "/transactions");			  
			   
			   logger.debug("Coast Capital POC REST WebClient created");
			   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Coast Capital POC REST WebClient created");
			}
			else if(Utils.isTestRESTWSAvailable()) {
			   _webClient = Utils.getTestWebClient("accounts/" + accountId + "/transactions");
			   			   
			   logger.debug("Test REST WebClient created");
			   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Test REST WebClient created");
			}
			
			
			if(_webClient != null) {
			   _webClient.query("postingDate", postingDate);
			   _webClient.query("postingDateOperand", postingDateOperand);	
			   Collection<? extends CustomTransaction> _data = _webClient.getCollection(CustomTransaction.class);
			   if(_data != null) {
			      _transactionList.addAll(_data);
			      
			      logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - TRANSACTIONS COUNT: " + _transactionList.size());
			   }
			   else {
				  logger.error(_thisMethod + " - " + "NULL is retrieved from ACCOUNTS TRANSACTIONS"); 
			   }
			}
			else {
				logger.error(_thisMethod + " - " + "REST WebClient not created");	
			}
		}
		catch(Exception ex) {
			logger.error(_thisMethod + " - " + ex.getMessage());
		}		
		
		return _transactionList;
	}

	@Override
	public GetCustomerResponse getCustomerById(String customerId) {

		GetCustomerResponse _customer = new GetCustomerResponse();
		String _thisMethod = "CoastCapitalPOCRESTClientImpl.getCustomerById";
		
		try {
		
			WebClient _webClient = null;
			if(Utils.isPOCAvailable()) {
			   _webClient = Utils.getPOCWebClientCustomers("customers/" + customerId);
			   logger.debug(_thisMethod + " - Coast Capital POC REST WebClient created");
			   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + "Coast Capital POC REST WebClient created");
			}
			else if(Utils.isTestRESTWSAvailable()) {
			   _webClient = Utils.getTestWebClient("customers/" + customerId);
			   logger.debug(_thisMethod + "- Test REST WebClient created");
			   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Test REST WebClient created");
			}
			
			if(_webClient != null){
			   _customer = _webClient.get(GetCustomerResponse.class);
			   if(_customer != null) {
		          logger.debug("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customer.getProfile());
		         
		          logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customer.getName());
		          logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customer.getPreferredName());
		          logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customer.getDateOfBirth());
			   }
			}
			else {
			   logger.error(_thisMethod + " - " + "REST WebClient not created");	
			}
		}
		catch(Exception ex){
			logger.error(_thisMethod + " - " + ex.getMessage());
		}
		
		return _customer;
	}

	@Override
	public List<GetCustomerResponse> getCustomers(String membership, String pan, String sin, String lastName,
			String firstName, String accountNumber) {
		
		List<GetCustomerResponse> customerList = new ArrayList<GetCustomerResponse>();
		
        		
		return customerList;
	}

	@Override
	public CreateCustomerResponse createCustomer(CreateCustomerRequest customer) {
		
		CreateCustomerResponse customerResponse = new CreateCustomerResponse();
		
			
		return customerResponse;
	}

	@Override
	public CreateShareAccountResponse createShareAccount(String customerId, CreateShareAccountRequest shareAccount) {
		
		CreateShareAccountResponse createShareAccountResponse = new CreateShareAccountResponse();
		
		
		return createShareAccountResponse;
	}

	@Override
	public CreateMembershipResponse createMembership(String customerId, CreateMembershipRequest membership) {
		
		CreateMembershipResponse createMembershipResponse = new CreateMembershipResponse();
		
		
		
		return createMembershipResponse;
	}

	@Override
	public CreateProfileResponse createProfile(CreateProfileRequest profile) {
		
		CreateProfileResponse profileResponse = new CreateProfileResponse();
				return profileResponse;
	}

	@Override
	public ValidateSINResponse validateSIN(String sin) {
		ValidateSINResponse validateSINResponse = new ValidateSINResponse();
		
		return validateSINResponse;
	}

	@Override
	public UpdateCustomerResponse updateCustomer(String customerId, UpdateCustomerRequest customer) {
		String _thisMethod = "CoastCapitalPOCRESTClientImpl.updateCustomer";
		
		UpdateCustomerResponse _customerResponse = null;
		try {
		
			WebClient _webClient = null;
			if(Utils.isPOCAvailable()) {
			   _webClient = Utils.getPOCWebClientCustomers("customers/" + customerId);
			   logger.debug(_thisMethod + " - Coast Capital POC REST WebClient created");
			   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + "Coast Capital POC REST WebClient created");
			}
			else if(Utils.isTestRESTWSAvailable()) {
			   _webClient = Utils.getTestWebClient("customers/" + customerId);
			   logger.debug(_thisMethod + "- Test REST WebClient created");
			   logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - Test REST WebClient created");
			}
			
			if(_webClient != null){
			   _customerResponse = _webClient.put(customer, UpdateCustomerResponse.class);
			   if(_customerResponse != null) {
		          logger.debug("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customerResponse.getProfileId());
		         
		          logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customerResponse.getShortName());
		          logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customerResponse.getFamilyName());
		          logger.error("IS DEBUG NOT ERROR: " + _thisMethod + " - " + "CUSTOMER PROFILE: " + _customerResponse.getDateOfBirth());
			   }
			}
			else {
			   logger.error(_thisMethod + " - " + "REST WebClient not created");	
			}
		}
		catch(Exception ex){
			logger.error(_thisMethod + " - " + ex.getMessage());
		}
		
		return _customerResponse;
	}

}
