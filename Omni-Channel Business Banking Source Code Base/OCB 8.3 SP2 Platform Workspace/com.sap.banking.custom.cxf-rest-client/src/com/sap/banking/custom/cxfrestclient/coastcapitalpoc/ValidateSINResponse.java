package com.sap.banking.custom.cxfrestclient.coastcapitalpoc;

public class ValidateSINResponse {

	private boolean validSin;

	public boolean isValidSin() {
		return validSin;
	}

	public void setValidSin(boolean validSin) {
		this.validSin = validSin;
	}
	
}
