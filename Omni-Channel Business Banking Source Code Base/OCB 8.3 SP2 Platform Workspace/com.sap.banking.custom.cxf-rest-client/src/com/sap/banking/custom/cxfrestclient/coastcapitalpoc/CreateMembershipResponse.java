package com.sap.banking.custom.cxfrestclient.coastcapitalpoc;

public class CreateMembershipResponse {

	private String messageId;
	private String memberId;
	private String memberPostalCode;
	private String memberCustomerId;
	private String memberRole;
	private int memberSector;
	private int memberIndustry;
	private int memberNoOfSigners;
	private String memberName;
	private String memberAddress;
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberPostalCode() {
		return memberPostalCode;
	}
	public void setMemberPostalCode(String memberPostalCode) {
		this.memberPostalCode = memberPostalCode;
	}
	public String getMemberCustomerId() {
		return memberCustomerId;
	}
	public void setMemberCustomerId(String memberCustomerId) {
		this.memberCustomerId = memberCustomerId;
	}
	public String getMemberRole() {
		return memberRole;
	}
	public void setMemberRole(String memberRole) {
		this.memberRole = memberRole;
	}
	public int getMemberSector() {
		return memberSector;
	}
	public void setMemberSector(int memberSector) {
		this.memberSector = memberSector;
	}
	public int getMemberIndustry() {
		return memberIndustry;
	}
	public void setMemberIndustry(int memberIndustry) {
		this.memberIndustry = memberIndustry;
	}
	public int getMemberNoOfSigners() {
		return memberNoOfSigners;
	}
	public void setMemberNoOfSigners(int memberNoOfSigners) {
		this.memberNoOfSigners = memberNoOfSigners;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberAddress() {
		return memberAddress;
	}
	public void setMemberAddress(String memberAddress) {
		this.memberAddress = memberAddress;
	}
	
}
