package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.interfaces;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateCustomerRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateMembershipRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateMembershipResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateProfileRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateProfileResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateShareAccountRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CreateShareAccountResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.CustomTransaction;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.GetCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.UpdateCustomerRequest;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.UpdateCustomerResponse;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.ValidateSINResponse;

import java.util.List;

@Path("/bankingservices")
@Produces("application/json")
@Consumes("application/json")
public interface CoastCapitalPOCRESTApi {

	@GET
    @Path("/echotest")
    public String echoTest();
	
	@GET
    @Path("/getaccountsbyid/{id}")
    public String getAccountsById(@PathParam("id") int id);
	
	@GET
    @Path("/accounts/{accountId}/transactions")
    public List<CustomTransaction> getTransactionHistoryByAccountId(@PathParam("accountId") String accountId,
    		@QueryParam("postingDate") String postingDate, @QueryParam("postingDateOperand") String postingDateOperand);
	
	@GET
    @Path("/customers/{customerId}")
    public GetCustomerResponse getCustomerById(@PathParam("customerId") String customerId);

	@GET
    @Path("/customers")
    public List<GetCustomerResponse> getCustomers(@QueryParam("membership") String membership,
    		@QueryParam("pan") String pan, @QueryParam("sin") String sin, @QueryParam("lastName") String lastName,
    		@QueryParam("firstName") String firstName, @QueryParam("accountNumber") String accountNumber);
	
	@POST
	@Path("/customers")
	public CreateCustomerResponse createCustomer(CreateCustomerRequest customer);

	@PUT
	@Path("/customers/{customerId}")
	@Consumes("application/json")
	public UpdateCustomerResponse updateCustomer( @PathParam("customerId") String customerId,
			UpdateCustomerRequest businessPartner);
	
	@PUT
	@Path("/customers/{customerId}/share-accounts")
	public CreateShareAccountResponse createShareAccount( @PathParam("customerId") String customerId,
			CreateShareAccountRequest shareAccount);
	
	@PUT
	@Path("/customers/{customerId}/memberships")
	public CreateMembershipResponse createMembership( @PathParam("customerId") String customerId,
			CreateMembershipRequest membership);

	@POST
	@Path("/profiles")
	public CreateProfileResponse createProfile(CreateProfileRequest profile);

	@GET
	@Path("sins/{sin}")
	public ValidateSINResponse validateSIN( @PathParam("sin") String sin);
}
