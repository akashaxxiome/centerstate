#*SAP Omni Channel Banking Customization Workspace*

## OCB Customization Template provides simple and efficient way to customize Omni Channel Banking features.

###  Prerequisites

- SAP Omni Channel Banking 8.3 SP02
    - Platform WAR
    - CB WAR
- JDK 8
- Eclipse IDE

#GIT Hub repository for OCB Sample templates
https://github.com/sachinb4u/ocb-customization-template 

Trouble Shooting

--------- Set up Gradle with gradlew command ------------------
1. for first time, use gradlew command. It will initiate the gradelw installation.
2. In case gradlww command is  are not able to download then try setting the proxy 
	set JAVA_OPTS=-Dhttp.proxyHost=proxy -Dhttp.proxyPort=8080 -Dhttps.proxyHost=proxy -Dhttps.proxyPort=8080 
3. In case you are using smp jvm, then you may get below exception
		################################################
		With SAP JVM gradle gives following error 
		C:\p4s\AE\Release\CB83SP03\modules>gradlew clean
		FAILURE: Build failed with an exception.
		* Where:
		Settings file 'C:\p4s\AE\Release\CB83SP03\modules\settings.gradle' line: 19
		* What went wrong:
		A problem occurred evaluating settings 'modules'.
		> Could not resolve all dependencies for configuration 'classpath'.
		   > Could not resolve biz.aQute.bnd:biz.aQute.bnd.gradle:3.2.0.
			 Required by:
				 unspecified:unspecified:unspecified
			  > Could not resolve biz.aQute.bnd:biz.aQute.bnd.gradle:3.2.0.
				 > Could not get resource 'https://jcenter.bintray.com/biz/aQute/bnd/biz.aQute.bnd.gradle/3.2.0/biz.aQute.bnd.gradle-3.2.0.pom'.
					> Could not GET 'https://jcenter.bintray.com/biz/aQute/bnd/biz.aQute.bnd.gradle/3.2.0/biz.aQute.bnd.gradle-3.2.0.pom'.
					   > sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested
		target
		* Try:
		Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

		BUILD FAILED
		###########################################################################

	Resolving build issue:-

	To resolve this issue need to import GeoTrust GLOBAL CA certificate into SAP JVM
	1.open certmgr.msc from Run.
	2.Go to Third-Party Root Certification Autiorities and click on Certificates
	3.Right Click on GeoTrust GLOBAL CA>All Task>Export
	4.save certificate with �geotrustca.cer�
	5.Open Command prompt and run below command to import above exported certificate into SAP JVM
		  C:\SAP\MobilePlatform3\sapjvm_7\jre\bin\keytool -import -alias geotrustcacert -keystore C:\SAP\MobilePlatform3\sapjvm_7\jre\lib\security\cacerts -file C:\Users\I017769\Desktop\geotrustca.cer

--------- Set up Gradle with gradlew command END------------------		  

4. Instead of using gradlew, you may also install gradle separately 
	For running Gradle, firstly add the environment variable GRADLE_HOME. This should point to the unpacked files from the Gradle website. Next add GRADLE_HOME/bin to your PATH environment variable. Usually, this is sufficient to run Gradle.




