package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

import java.util.List;

public class CreateCustomerRequest {
	
	private String clientMessageId;
	private String title;
	private String givenNames;
	private String familyName;
	private String sin;
	private String gender;
	private String dateOfBirth;
	private String residence;
	private String residenceStatus;
	private String accountOfficer;
	private List<Identification> identifications;
	private List<PhoneNumber> phoneNumbers;
	private String preferredContactMethod;
	private String employmentStatus;
    private String legalAddress;
    private String legalCity;
    private String legalProvince;
    private String legalCountry;
    private String legalPostalCode;
	private String mailingAddress;
    private String mailingCity;
    private String mailingProvince;
    private String mailingCountry;
    private String mailingPostalCode;
	public String getClientMessageId() {
		return clientMessageId;
	}
	public void setClientMessageId(String clientMessageId) {
		this.clientMessageId = clientMessageId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGivenNames() {
		return givenNames;
	}
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getSin() {
		return sin;
	}
	public void setSin(String sin) {
		this.sin = sin;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getResidence() {
		return residence;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public String getResidenceStatus() {
		return residenceStatus;
	}
	public void setResidenceStatus(String residenceStatus) {
		this.residenceStatus = residenceStatus;
	}
	public String getAccountOfficer() {
		return accountOfficer;
	}
	public void setAccountOfficer(String accountOfficer) {
		this.accountOfficer = accountOfficer;
	}
	public List<Identification> getIdentifications() {
		return identifications;
	}
	public void setIdentifications(List<Identification> identifications) {
		this.identifications = identifications;
	}
	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public String getPreferredContactMethod() {
		return preferredContactMethod;
	}
	public void setPreferredContactMethod(String preferredContactMethod) {
		this.preferredContactMethod = preferredContactMethod;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public String getLegalAddress() {
		return legalAddress;
	}
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}
	public String getLegalCity() {
		return legalCity;
	}
	public void setLegalCity(String legalCity) {
		this.legalCity = legalCity;
	}
	public String getLegalProvince() {
		return legalProvince;
	}
	public void setLegalProvince(String legalProvince) {
		this.legalProvince = legalProvince;
	}
	public String getLegalCountry() {
		return legalCountry;
	}
	public void setLegalCountry(String legalCountry) {
		this.legalCountry = legalCountry;
	}
	public String getLegalPostalCode() {
		return legalPostalCode;
	}
	public void setLegalPostalCode(String legalPostalCode) {
		this.legalPostalCode = legalPostalCode;
	}
	public String getMailingAddress() {
		return mailingAddress;
	}
	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	public String getMailingProvince() {
		return mailingProvince;
	}
	public void setMailingProvince(String mailingProvince) {
		this.mailingProvince = mailingProvince;
	}
	public String getMailingCountry() {
		return mailingCountry;
	}
	public void setMailingCountry(String mailingCountry) {
		this.mailingCountry = mailingCountry;
	}
	public String getMailingPostalCode() {
		return mailingPostalCode;
	}
	public void setMailingPostalCode(String mailingPostalCode) {
		this.mailingPostalCode = mailingPostalCode;
	}   
	
}
