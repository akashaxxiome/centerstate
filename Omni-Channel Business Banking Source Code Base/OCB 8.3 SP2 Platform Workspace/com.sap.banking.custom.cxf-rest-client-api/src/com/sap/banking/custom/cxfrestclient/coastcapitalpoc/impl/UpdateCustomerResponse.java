package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

import java.util.List;

public class UpdateCustomerResponse {
	
	private String messageId;
	private String title;
	private String givenNames;
	private String familyName;
	private String sin;
	private String gender;
	private String dateOfBirth;
	private String residence;
	private String residenceStatus;
	private String accountOfficer;
	private List<Identification> identifications;
	private List<PhoneNumber> phoneNumbers;
	private String preferredContactMethod;
	private String employmentStatus;
    private String legalAddress;
    private String legalCity;
    private String legalProvince;
    private String legalCountry;
    private String legalPostalCode;
    private String profileId;
    private String mnemonic;
    private String shortName;
    private String name1;
    private String name2;
    private String country;
    private int sector;
    private int industry;
    private String nationality;
    private int  customerStatus;
    private String language;
    private String companyBook;
    private String residenceRegion;
    private String phone1;
    private String customerSince;
    private String customerType;
    private String acctStatementOption;
    private String branchId;
    private String accountId;
    private int accountCategory;
    private String accountTitle;
    private String accountOpeningDate;
    private String accountCurrency;
    private String accountStatus;
    private int accountIntendedUse;
    private String accountThirdParty;
    private String memberId;
    private String memberPostalCode;
    private String memberCustomerId;
    private String memberRole;
    private int memberSector;
    private int memberIndustry;
    private int memberNoOfSigners;
	private String memberName;
	private String memberAddress;
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGivenNames() {
		return givenNames;
	}
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getSin() {
		return sin;
	}
	public void setSin(String sin) {
		this.sin = sin;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getResidence() {
		return residence;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public String getResidenceStatus() {
		return residenceStatus;
	}
	public void setResidenceStatus(String residenceStatus) {
		this.residenceStatus = residenceStatus;
	}
	public String getAccountOfficer() {
		return accountOfficer;
	}
	public void setAccountOfficer(String accountOfficer) {
		this.accountOfficer = accountOfficer;
	}
	public List<Identification> getIdentifications() {
		return identifications;
	}
	public void setIdentifications(List<Identification> identifications) {
		this.identifications = identifications;
	}
	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public String getPreferredContactMethod() {
		return preferredContactMethod;
	}
	public void setPreferredContactMethod(String preferredContactMethod) {
		this.preferredContactMethod = preferredContactMethod;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public String getLegalAddress() {
		return legalAddress;
	}
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}
	public String getLegalCity() {
		return legalCity;
	}
	public void setLegalCity(String legalCity) {
		this.legalCity = legalCity;
	}
	public String getLegalProvince() {
		return legalProvince;
	}
	public void setLegalProvince(String legalProvince) {
		this.legalProvince = legalProvince;
	}
	public String getLegalCountry() {
		return legalCountry;
	}
	public void setLegalCountry(String legalCountry) {
		this.legalCountry = legalCountry;
	}
	public String getLegalPostalCode() {
		return legalPostalCode;
	}
	public void setLegalPostalCode(String legalPostalCode) {
		this.legalPostalCode = legalPostalCode;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getSector() {
		return sector;
	}
	public void setSector(int sector) {
		this.sector = sector;
	}
	public int getIndustry() {
		return industry;
	}
	public void setIndustry(int industry) {
		this.industry = industry;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public int getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(int customerStatus) {
		this.customerStatus = customerStatus;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCompanyBook() {
		return companyBook;
	}
	public void setCompanyBook(String companyBook) {
		this.companyBook = companyBook;
	}
	public String getResidenceRegion() {
		return residenceRegion;
	}
	public void setResidenceRegion(String residenceRegion) {
		this.residenceRegion = residenceRegion;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getCustomerSince() {
		return customerSince;
	}
	public void setCustomerSince(String customerSince) {
		this.customerSince = customerSince;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getAcctStatementOption() {
		return acctStatementOption;
	}
	public void setAcctStatementOption(String acctStatementOption) {
		this.acctStatementOption = acctStatementOption;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public int getAccountCategory() {
		return accountCategory;
	}
	public void setAccountCategory(int accountCategory) {
		this.accountCategory = accountCategory;
	}
	public String getAccountTitle() {
		return accountTitle;
	}
	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}
	public String getAccountOpeningDate() {
		return accountOpeningDate;
	}
	public void setAccountOpeningDate(String accountOpeningDate) {
		this.accountOpeningDate = accountOpeningDate;
	}
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public int getAccountIntendedUse() {
		return accountIntendedUse;
	}
	public void setAccountIntendedUse(int accountIntendedUse) {
		this.accountIntendedUse = accountIntendedUse;
	}
	public String getAccountThirdParty() {
		return accountThirdParty;
	}
	public void setAccountThirdParty(String accountThirdParty) {
		this.accountThirdParty = accountThirdParty;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberPostalCode() {
		return memberPostalCode;
	}
	public void setMemberPostalCode(String memberPostalCode) {
		this.memberPostalCode = memberPostalCode;
	}
	public String getMemberCustomerId() {
		return memberCustomerId;
	}
	public void setMemberCustomerId(String memberCustomerId) {
		this.memberCustomerId = memberCustomerId;
	}
	public String getMemberRole() {
		return memberRole;
	}
	public void setMemberRole(String memberRole) {
		this.memberRole = memberRole;
	}
	public int getMemberSector() {
		return memberSector;
	}
	public void setMemberSector(int memberSector) {
		this.memberSector = memberSector;
	}
	public int getMemberIndustry() {
		return memberIndustry;
	}
	public void setMemberIndustry(int memberIndustry) {
		this.memberIndustry = memberIndustry;
	}
	public int getMemberNoOfSigners() {
		return memberNoOfSigners;
	}
	public void setMemberNoOfSigners(int memberNoOfSigners) {
		this.memberNoOfSigners = memberNoOfSigners;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberAddress() {
		return memberAddress;
	}
	public void setMemberAddress(String memberAddress) {
		this.memberAddress = memberAddress;
	}   
	   
}
