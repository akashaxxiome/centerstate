package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

public class Identification {
	
	private String custIdType;
	private String custIdNumber;
	private String custIdProvinceIssue;
	private String custIdCountryIssue;
	
	
	public String getCustIdType() {
		return custIdType;
	}
	public void setCustIdType(String custIdType) {
		this.custIdType = custIdType;
	}
	public String getCustIdNumber() {
		return custIdNumber;
	}
	public void setCustIdNumber(String custIdNumber) {
		this.custIdNumber = custIdNumber;
	}
	public String getCustIdProvinceIssue() {
		return custIdProvinceIssue;
	}
	public void setCustIdProvinceIssue(String custIdProvinceIssue) {
		this.custIdProvinceIssue = custIdProvinceIssue;
	}
	public String getCustIdCountryIssue() {
		return custIdCountryIssue;
	}
	public void setCustIdCountryIssue(String custIdCountryIssue) {
		this.custIdCountryIssue = custIdCountryIssue;
	}

}
