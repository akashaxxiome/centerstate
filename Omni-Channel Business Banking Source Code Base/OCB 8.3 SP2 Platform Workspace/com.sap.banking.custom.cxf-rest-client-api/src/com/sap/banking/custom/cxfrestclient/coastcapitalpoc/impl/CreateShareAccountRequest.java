package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

public class CreateShareAccountRequest {
	
	private String clientMessageId;

	public String getClientMessageId() {
		return clientMessageId;
	}

	public void setClientMessageId(String clientMessageId) {
		this.clientMessageId = clientMessageId;
	}
	
}
