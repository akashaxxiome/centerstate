package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

public class PhoneNumber {

	private String phoneNumType;
	private String phoneNumber;
	
	
	public String getPhoneNumType() {
		return phoneNumType;
	}
	public void setPhoneNumType(String phoneNumType) {
		this.phoneNumType = phoneNumType;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
