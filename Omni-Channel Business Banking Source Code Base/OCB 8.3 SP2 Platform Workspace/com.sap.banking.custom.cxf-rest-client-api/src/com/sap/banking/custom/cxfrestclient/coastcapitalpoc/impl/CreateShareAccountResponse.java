package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

public class CreateShareAccountResponse {

	private String messageId;
	private String accountId;
	private String customerId;
	private int accountCategory;
	private String accountTitle;
	private String accountOpeningDate;
	private String accountCurrency;
	private String accountStatus;
	private int accountIntendedUse;
	private String accountThirdParty;
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public int getAccountCategory() {
		return accountCategory;
	}
	public void setAccountCategory(int accountCategory) {
		this.accountCategory = accountCategory;
	}
	public String getAccountTitle() {
		return accountTitle;
	}
	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}
	public String getAccountOpeningDate() {
		return accountOpeningDate;
	}
	public void setAccountOpeningDate(String accountOpeningDate) {
		this.accountOpeningDate = accountOpeningDate;
	}
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public int getAccountIntendedUse() {
		return accountIntendedUse;
	}
	public void setAccountIntendedUse(int accountIntendedUse) {
		this.accountIntendedUse = accountIntendedUse;
	}
	public String getAccountThirdParty() {
		return accountThirdParty;
	}
	public void setAccountThirdParty(String accountThirdParty) {
		this.accountThirdParty = accountThirdParty;
	}
	
}
