package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

import java.math.BigDecimal;

public class CustomTransaction {
	
	private String postingDate;
	private String valueDate;
    private String transactionDescription;
    private String accountRefNum;
    private String transactionStatus;
    private String reference;
    private String debitAmount;
    private String creditAmount;
	private String balance;
    private String tellerReference;
    private String userId;
    private String userName;
    private String chequeNumber;
    
    
    public String getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public String getTransactionDescription() {
		return transactionDescription;
	}
	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}
	public String getAccountRefNum() {
		return accountRefNum;
	}
	public void setAccountRefNum(String accountRefNum) {
		this.accountRefNum = accountRefNum;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}
	public String getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getTellerReference() {
		return tellerReference;
	}
	public void setTellerReference(String tellerReference) {
		this.tellerReference = tellerReference;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

}
