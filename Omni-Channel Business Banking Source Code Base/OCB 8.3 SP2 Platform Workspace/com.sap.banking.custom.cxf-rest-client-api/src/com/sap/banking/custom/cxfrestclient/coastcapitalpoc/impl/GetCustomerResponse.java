package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

import java.util.List;

public class GetCustomerResponse {
	
	private String profile;
	private String status;
    private List<String> memberships;
    private String name;
    private String preferredName;
    private int age;
    private String dateOfBirth;
	private String gender;
    private String memberSector;
    private String memberIndustry;
    private String profileStatus;
    private int primaryCardHolder;
    private int garnishFlag;
    private String addressStreet;
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getMemberships() {
		return memberships;
	}
	public void setMemberships(List<String> memberships) {
		this.memberships = memberships;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPreferredName() {
		return preferredName;
	}
	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMemberSector() {
		return memberSector;
	}
	public void setMemberSector(String memberSector) {
		this.memberSector = memberSector;
	}
	public String getMemberIndustry() {
		return memberIndustry;
	}
	public void setMemberIndustry(String memberIndustry) {
		this.memberIndustry = memberIndustry;
	}
	public String getProfileStatus() {
		return profileStatus;
	}
	public void setProfileStatus(String profileStatus) {
		this.profileStatus = profileStatus;
	}
	public int getPrimaryCardHolder() {
		return primaryCardHolder;
	}
	public void setPrimaryCardHolder(int primaryCardHolder) {
		this.primaryCardHolder = primaryCardHolder;
	}
	public int getGarnishFlag() {
		return garnishFlag;
	}
	public void setGarnishFlag(int garnishFlag) {
		this.garnishFlag = garnishFlag;
	}
	public String getAddressStreet() {
		return addressStreet;
	}
	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}
     
}
