package com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl;

public class CreateMembershipRequest {
	
	private String clientMessageId;

	public String getClientMessageId() {
		return clientMessageId;
	}

	public void setClientMessageId(String clientMessageId) {
		this.clientMessageId = clientMessageId;
	}
	
}
