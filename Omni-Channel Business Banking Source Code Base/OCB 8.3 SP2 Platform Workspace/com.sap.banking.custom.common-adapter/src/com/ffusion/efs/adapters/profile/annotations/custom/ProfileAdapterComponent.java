package com.ffusion.efs.adapters.profile.annotations.custom;

import java.lang.annotation.Annotation;
import org.springframework.stereotype.Component;

@Component
public @interface ProfileAdapterComponent
{
  String value() default "";
}