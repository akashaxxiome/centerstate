package com.sap.banking.user.annotations2;

import java.lang.annotation.Annotation;
import org.springframework.stereotype.Component;

@Component
public @interface UserComponent
{
  String value() default "";
}
