package com.sap.banking.user.bo;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.entitlementprofile.RetailUserProfiles;
import com.ffusion.beans.user.User;
import com.ffusion.csil.beans.entitlements.Channel;
import com.ffusion.csil.beans.entitlements.Entitlement;
import com.ffusion.csil.beans.entitlements.EntitlementGroup;
import com.ffusion.csil.beans.entitlements.EntitlementGroupMember;
import com.ffusion.csil.beans.entitlements.EntitlementProfile;
import com.ffusion.csil.beans.entitlements.EntitlementTypePropertyList;
import com.ffusion.csil.beans.entitlements.EntitlementTypePropertyLists;
import com.ffusion.efs.adapters.entitlements.interfaces.EntitlementsSetting;
import com.ffusion.efs.adapters.profile.exception.ProfileException;
import com.ffusion.services.customer.profile.interfaces.CustomerService;
import com.sap.banking.account.bo.interfaces.AccountDetail;
import com.sap.banking.account.bo.interfaces.BankingAccounts;
import com.sap.banking.account.exception.AccountException;
import com.sap.banking.accountconfig.beans.AccountSearchCriteria;
import com.sap.banking.common.beans.ErrorDetail;
import com.sap.banking.common.beans.ValidationResult;
import com.sap.banking.common.beans.entitlements.GroupEntitlementSearchBean;
import com.sap.banking.common.beans.paging.PagedResult;
import com.sap.banking.common.bo.interfaces.BusinessFlowObject;
import com.sap.banking.commonutil.beans.entitlements.EntitlementGroupInfo;
import com.sap.banking.commonutil.beans.entitlements.EntitlementInfo;
import com.sap.banking.commonutil.beans.entitlements.EntitlementInfos;
import com.sap.banking.commonutil.beans.entitlements.Features;
import com.sap.banking.entitlements.bo.interfaces.Entitlements;
import com.sap.banking.entitlements.bo.interfaces.EntitlementsAdmin;
import com.sap.banking.entitlements.bo.interfaces.EntitlementsChannel;
import com.sap.banking.entitlements.bo.interfaces.EntitlementsProfile;
import com.sap.banking.entitlements.bo.interfaces.GroupEntitlements;
import com.sap.banking.entitlements.exception.EntitlementsException;
import com.sap.banking.user.annotations2.UserComponent;
import com.sap.banking.user.beans.PersonalChannelProfile;
import com.sap.banking.user.beans.PersonalChannelProfiles;
import com.sap.banking.user.beans.PersonalProfile;
import com.sap.banking.user.beans.ProfileFeaturesSearchCriteria;
import com.sap.banking.user.exception.UserException;
import com.sap.banking.user.interceptors.entitlements.annotations.UserAdminEntitled;
import com.sap.banking.user.interceptors.entitlements.annotations.UserEntitled;
import com.sap.banking.userconfig.bo.interfaces.UserValidationConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;




@UserComponent
public class CustomRetailUserImpl
  extends UserImpl
  implements BusinessFlowObject, com.sap.banking.user.bo.interfaces.RetailUser
{
  @Autowired
  protected GroupEntitlements groupEntitlementsBO;
  @Autowired
  protected AccountDetail accountDetailBO;
  @Autowired
  protected EntitlementsAdmin entitlementsAdminBO;
  @Autowired
  protected BankingAccounts bankingAccountsBO;
  @Autowired
  @Qualifier("retailUserProfileRef")
  protected com.sap.banking.user.bo.interfaces.RetailUserProfile retailUserProfileBO;
  
  @UserAdminEntitled(messageKey="MODIFY_USER_ENTITLEMENT_FAILED")
  public User changePassword(SecureUser sUser, User user, Map<String, Object> extra)
    throws UserException
  {
    return super.changePassword(sUser, user, extra);
  }
  
  @UserEntitled
  @Deprecated
  public User getUserById(SecureUser sUser, User user, Map<String, Object> extra)
    throws UserException
  {
    String thisMethod = "User.getUserById";
    
    User returnUser = super.getUserById(sUser, user, extra);
    return returnUser;
  }
  

  @UserAdminEntitled(messageKey="MODIFY_USER_ENTITLEMENT_FAILED")
  public User setUpSecurityQuestions(SecureUser secureUser, User user, Map<String, Object> extra)
    throws UserException
  {
    String thisMethod = "UserImpl.setUpSecurityQuestions";
    return super.setUpSecurityQuestions(secureUser, user, extra);
  }
  
  public SecureUser syncBackendUser(SecureUser sUser, User loggedUserInfo, Map<String, Object> extraMap)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.syncBackendUser() : ";
    
    if (loggedUserInfo == null) {
      throw new UserException(16002);
    }
    

    User backendUserInfo = super.getUserInfoFromBackend(sUser, sUser.getId(), (String)loggedUserInfo.get("PROCESSOR_PIN"), extraMap);
    
    if (backendUserInfo == null) {
      throw new UserException(16002);
    }
    

    String loggedUserFieldValues = getConsumerUserStr(loggedUserInfo);
    

    String backEndUserFieldValues = getConsumerUserStr(backendUserInfo);
    


    if (!loggedUserFieldValues.equals(backEndUserFieldValues))
    {

      backendUserInfo.setId(loggedUserInfo.getId());
      
      Map<String, Object> extra = new HashMap();
      

      ValidationResult validationResult = null;
      try
      {
        validationResult = validateUser(sUser, backendUserInfo, "EditBackendConsumerUser");
        
        ValidationResult zipCodeValidationResult = validateUserZipCode(sUser, backendUserInfo, extra);
        
        if ((zipCodeValidationResult != null) && (zipCodeValidationResult.isValidationFailed())) {
          validationResult.getValidationErrors().putAll(zipCodeValidationResult.getValidationErrors());
          validationResult.getErrorDetails().putAll(zipCodeValidationResult.getErrorDetails());
        }
        
        ValidationResult ssnValidationResult = validateUserSSN(sUser, backendUserInfo, extra);
        
        if ((ssnValidationResult != null) && (ssnValidationResult.isValidationFailed())) {
          validationResult.getValidationErrors().putAll(ssnValidationResult.getValidationErrors());
          validationResult.getErrorDetails().putAll(ssnValidationResult.getErrorDetails());
        }
        
        ValidationResult phoneValidationResult = validateUserPhone(sUser, backendUserInfo, extra);
        
        if ((phoneValidationResult != null) && (phoneValidationResult.isValidationFailed())) {
          validationResult.getValidationErrors().putAll(phoneValidationResult.getValidationErrors());
          validationResult.getErrorDetails().putAll(phoneValidationResult.getErrorDetails());
        }
      }
      catch (UserException userException) {
        this.logger.error(thisMethod + userException);
      } catch (Exception ex) {
        this.logger.error(thisMethod + ex);
      }
      
      if ((validationResult != null) && (validationResult.isValidationFailed())) {
        this.logger.debug(thisMethod + "Validation errors : " + validationResult.getErrorDetails());
      } else if ((validationResult == null) || (!validationResult.isValidationFailed())) {
        boolean isSyncError = false;
        try {
          this.userAdminService.syncBackendUser(sUser, backendUserInfo, extraMap);
        } catch (ProfileException ex) {
          isSyncError = true;
          this.logger.error(thisMethod + "Error in Sync up back end user details : " + ex);
        }
        
        if (!isSyncError)
        {
          loggedUserInfo.setFirstName(backendUserInfo.getFirstName());
          loggedUserInfo.setMiddleName(backendUserInfo.getMiddleName());
          loggedUserInfo.setLastName(backendUserInfo.getLastName());
          loggedUserInfo.setTitle(backendUserInfo.getTitle());
          loggedUserInfo.setStreet(backendUserInfo.getStreet());
          loggedUserInfo.setStreet2(backendUserInfo.getStreet2());
          loggedUserInfo.setCity(backendUserInfo.getCity());
          loggedUserInfo.setState(backendUserInfo.getState());
          loggedUserInfo.setStateCode(backendUserInfo.getStateCode());
          loggedUserInfo.setCountry(backendUserInfo.getCountry());
          loggedUserInfo.setZipCode(backendUserInfo.getZipCode());
          loggedUserInfo.setEmail(backendUserInfo.getEmail());
          loggedUserInfo.setPhone(backendUserInfo.getPhone());
          loggedUserInfo.setPhone2(backendUserInfo.getPhone2());
          loggedUserInfo.setDataPhone(backendUserInfo.getDataPhone());
          loggedUserInfo.setFaxPhone(backendUserInfo.getFaxPhone());
          loggedUserInfo.setSSN(backendUserInfo.getSSN());
          loggedUserInfo.setGender(backendUserInfo.getGender());
          sUser.put("USER", loggedUserInfo);
        }
      }
    }
    return sUser;
  }
  
  @UserEntitled
  public PersonalProfile getUserPersonalProfile(SecureUser sUser, String profileId, Map<String, Object> extraMap)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.getUserPersonalProfile() : ";
    PersonalProfile personalPermissionProfile = null;
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(null, Integer.toString(sUser.getProfileID()), null);
      GroupEntitlementSearchBean groupEntitlementSearchBean = new GroupEntitlementSearchBean();
      groupEntitlementSearchBean.setGroupId(entProfile.getProfileIdValue());
      List<String> categoryList = new ArrayList();
      categoryList.add("per personalconsumer");
      groupEntitlementSearchBean.setCategory(categoryList);
      EntitlementGroupInfo entitlementGroup = this.groupEntitlementsBO.getGroupEntitlementsAndLimits(sUser, groupEntitlementSearchBean, extraMap);
      personalPermissionProfile = new PersonalProfile();
      personalPermissionProfile.setId(entProfile.getProfileId());
      personalPermissionProfile.setEntitlements(entitlementGroup.getEntitlements());
      
      EntitlementInfos accEntitlements = getAccountEntitlements(sUser, groupEntitlementSearchBean, extraMap);
      personalPermissionProfile.getEntitlements().addAll(accEntitlements);
      personalPermissionProfile.setId(profileId);
    }
    catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    return personalPermissionProfile;
  }
  












  public PersonalProfile getSecondaryUserProfile(SecureUser sUser, String secondaryUserId, Map<String, Object> extraMap)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.getSecondaryUserProfile() : ";
    PersonalProfile personalPermissionProfile = null;
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(sUser, Integer.toString(sUser.getProfileID()), extraMap);
      GroupEntitlementSearchBean groupEntitlementSearchBean = new GroupEntitlementSearchBean();
      groupEntitlementSearchBean.setMemberId(secondaryUserId);
      groupEntitlementSearchBean.setGroupId(entProfile.getProfileIdValue());
      List<String> categoryList = new ArrayList();
      categoryList.add("per personalconsumer");
      groupEntitlementSearchBean.setCategory(categoryList);
      EntitlementGroupInfo entitlementGroup = this.groupEntitlementsBO.getGroupEntitlementsAndLimits(sUser, groupEntitlementSearchBean, extraMap);
      personalPermissionProfile = new PersonalProfile();
      personalPermissionProfile.setId(entProfile.getProfileId());
      personalPermissionProfile.setEntitlements(entitlementGroup.getEntitlements());
      
      EntitlementInfos accEntitlements = getAccountEntitlements(sUser, groupEntitlementSearchBean, extraMap);
      personalPermissionProfile.getEntitlements().addAll(accEntitlements);
    }
    catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    return personalPermissionProfile;
  }
  
  @UserEntitled
  public PersonalChannelProfiles getUserPersonalChannelProfiles(SecureUser sUser, String profileId, Map<String, Object> extraMap)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.getUserPersonalChannelProfiles() : ";
    PersonalChannelProfiles personalChannelProfiles = new PersonalChannelProfiles();
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(null, Integer.toString(sUser.getProfileID()), null);
      GroupEntitlementSearchBean groupEntitlementSearchBean = new GroupEntitlementSearchBean();
      groupEntitlementSearchBean.setGroupId(entProfile.getProfileIdValue());
      List<Channel> listChannels = getEntitlementsChannelBO().getEntitledChannels(sUser, Integer.valueOf(entProfile.getProfileIdValue()), extraMap);
      if ((listChannels != null) && (!listChannels.isEmpty())) {
        List<String> categoryList = new ArrayList();
        categoryList.add("per personalconsumer");
        groupEntitlementSearchBean.setCategory(categoryList);
        for (Channel channel : listChannels) {
          personalChannelProfiles.add(getUserPersonalChannelProfileByChannel(sUser, channel.getId(), extraMap));
        }
      }
    } catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    return personalChannelProfiles;
  }
  
  @UserEntitled
  public PersonalChannelProfile getUserPersonalChannelProfileByChannel(SecureUser sUser, String channelId, Map<String, Object> extraMap)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.getUserPersonalChannelProfileByChannel() : ";
    
    GroupEntitlementSearchBean groupEntitlementSearchBean = new GroupEntitlementSearchBean();
    PersonalChannelProfile personalChannelProfile = getUserPersonalChannelProfileByChannel(sUser, channelId, groupEntitlementSearchBean, extraMap);
    
    return personalChannelProfile;
  }
  
  protected PersonalChannelProfile getUserPersonalChannelProfileByChannel(SecureUser sUser, String channelId, GroupEntitlementSearchBean groupEntitlementSearchBean, Map<String, Object> extraMap) throws UserException
  {
    String thisMethod = "In RetailUserImpl.getUserPersonalChannelProfileByChannel() : ";
    
    PersonalChannelProfile personalChannelProfile = new PersonalChannelProfile();
    PersonalProfile personalProfile = new PersonalProfile();
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(null, Integer.toString(sUser.getProfileID()), null);
      personalProfile.setId(entProfile.getProfileId());
      boolean entitled = getEntitlementsChannelBO().isEntitledChannel(entProfile.getProfileIdValue(), channelId);
      if (entitled)
      {
        EntitlementProfile entitlementProfile = getEntitlementsProfileBO().getPersonalChannelProfileOfMember(sUser, Integer.toString(sUser.getProfileID()), channelId, extraMap);
        if ((entitlementProfile == null) || (entitlementProfile.getProfileIdValue() < 1))
        {

          EntitlementGroup channelGroup = createPersonalChannelGroup(sUser, Integer.toString(sUser.getProfileID()), personalProfile, channelId, extraMap);
          if (channelGroup == null) {
            this.logger.error("Unable to create ChannelGroup for Personal Channel Profile : - channel=" + channelId);
            throw new UserException(3034);
          }
          groupEntitlementSearchBean.setGroupId(channelGroup.getGroupId());
        } else {
          groupEntitlementSearchBean.setGroupId(entitlementProfile.getProfileIdValue());
        }
        

        EntitlementInfos entitlementInfos = getPersonalGroupEntitlements(sUser, groupEntitlementSearchBean, extraMap);
        personalChannelProfile.setChannel(channelId);
        personalChannelProfile.setEntitlements(entitlementInfos);
      } else {
        String validationMessage = this.userValidationConfig.getValidationMessageDescription(sUser, "Invalid.PersonalChannel.Id");
        ValidationResult vr = new ValidationResult();
        HashMap<String, String> errors = new HashMap(2);
        HashMap<String, ErrorDetail> errorDetails = new HashMap();
        errors.put("channeId", validationMessage);
        ErrorDetail errorDetail = new ErrorDetail("Invalid.PersonalChannel.Id", validationMessage);
        errorDetails.put("channeId", errorDetail);
        vr.setValidationFailed(true);
        vr.setErrorDetails(errorDetails);
        vr.setValidationErrors(errors);
        UserException userEx = new UserException(-2);
        userEx.setValidationResult(vr);
        userEx.setValidationFailed(true);
        throw userEx;
      }
    } catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    return personalChannelProfile;
  }
  
  @UserEntitled
  public PagedResult<EntitlementInfo> getUserPersonalChannelProfileEntitlements(SecureUser sUser, GroupEntitlementSearchBean searchCriteria, Map<String, Object> extraMap)
    throws UserException
  {
    PersonalChannelProfile personalChannelProfile = new PersonalChannelProfile();
    String channel = null;
    try {
      channel = (String)getValueByIdFieldName("PersonalChannelProfile", "Channel");
    } catch (Exception e) {
      String validationMessage = this.userValidationConfig.getValidationMessageDescription(sUser, "Invalid.PersonalChannel.Id");
      ValidationResult vr = new ValidationResult();
      HashMap<String, String> errors = new HashMap(2);
      HashMap<String, ErrorDetail> errorDetails = new HashMap();
      errors.put("channeId", validationMessage);
      ErrorDetail errorDetail = new ErrorDetail("Invalid.PersonalChannel.Id", validationMessage);
      errorDetails.put("channeId", errorDetail);
      vr.setValidationFailed(true);
      vr.setErrorDetails(errorDetails);
      vr.setValidationErrors(errors);
      UserException userEx = new UserException(-2);
      userEx.setValidationResult(vr);
      throw userEx;
    }
    if ((channel == null) || (channel.isEmpty()))
    {
      String validationMessage = this.userValidationConfig.getValidationMessageDescription(sUser, "Invalid.PersonalChannel.Id");
      ValidationResult vr = new ValidationResult();
      HashMap<String, String> errors = new HashMap(2);
      HashMap<String, ErrorDetail> errorDetails = new HashMap();
      errors.put("channeId", validationMessage);
      ErrorDetail errorDetail = new ErrorDetail("Invalid.PersonalChannel.Id", validationMessage);
      errorDetails.put("channeId", errorDetail);
      vr.setValidationFailed(true);
      vr.setValidationErrors(errors);
      vr.setErrorDetails(errorDetails);
      UserException userEx = new UserException(-2);
      userEx.setValidationResult(vr);
      throw userEx;
    }
    personalChannelProfile = getUserPersonalChannelProfileByChannel(sUser, channel, searchCriteria, extraMap);
    PagedResult<EntitlementInfo> pagedResult = new PagedResult();
    if (personalChannelProfile.getEntitlements() != null) {
      pagedResult.setPagedList(personalChannelProfile.getEntitlements());
    }
    
    return pagedResult;
  }
  
  private EntitlementGroup createPersonalChannelGroup(SecureUser secureUser, String memberId, PersonalProfile personalProfile, String channelId, Map<String, Object> extra) throws EntitlementsException {
    return getEntitlementsProfileBO().createPersonalChannelGroup(secureUser, memberId, personalProfile.getId(), channelId, extra);
  }
  
  private EntitlementInfos getPersonalGroupEntitlements(SecureUser sUser, GroupEntitlementSearchBean groupEntitlementSearchBean, Map<String, Object> extra) throws UserException
  {
    EntitlementInfos entitlementInfos = null;
    
    String thisMethod = "In RetailUserImpl.getPersonalGroupEntitlements() : ";
    try {
      List<String> categoryList = new ArrayList();
      categoryList.add("per personalconsumer");
      groupEntitlementSearchBean.setCategory(categoryList);
      EntitlementGroupInfo entitlementGroup = this.groupEntitlementsBO.getGroupEntitlementsAndLimits(sUser, groupEntitlementSearchBean, extra);
      entitlementInfos = entitlementGroup.getEntitlements();
      if (entitlementInfos == null) {
        entitlementInfos = new EntitlementInfos();
      }
      EntitlementInfos accEntitlements = getAccountEntitlements(sUser, groupEntitlementSearchBean, extra);
      entitlementInfos.addAll(accEntitlements);
    } catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    EntitlementGroupInfo entitlementGroup;
    return entitlementInfos;
  }
  
  @UserEntitled
  public PersonalProfile modifyUserPersonalPermission(SecureUser sUser, PersonalProfile personalPermissionProfile, Map<String, Object> extra)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.modifyUserPersonalPermission() : ";
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(null, Integer.toString(sUser.getProfileID()), null);
      personalPermissionProfile.setId(entProfile.getProfileId());
      EntitlementGroupInfo entGroupWithEntitlements = new EntitlementGroupInfo();
      entGroupWithEntitlements.setGroupId(entProfile.getProfileIdValue());
      entGroupWithEntitlements.setEntitlements(personalPermissionProfile.getEntitlements());
      entGroupWithEntitlements.setEntGroupType("ConsumerPersonalProfile");
      this.groupEntitlementsBO.modifyGroupEntitlements(sUser, entGroupWithEntitlements, extra);
    } catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    return personalPermissionProfile;
  }
  
  @UserEntitled
  public PersonalChannelProfile modifyUserPersonalChannelPermission(SecureUser sUser, PersonalChannelProfile personalPermissionProfile, Map<String, Object> extra)
    throws UserException
  {
    String thisMethod = "In RetailUserImpl.modifyUserPersonalPermission() : ";
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(null, Integer.toString(sUser.getProfileID()), null);
      PersonalProfile personalProfile = new PersonalProfile();
      personalProfile.setId(entProfile.getProfileId());
      String channel = personalPermissionProfile.getChannel();
      boolean invalidChannel = false;
      if (channel == null) {
        invalidChannel = true;
      } else {
        boolean entitled = getEntitlementsChannelBO().isEntitledChannel(entProfile.getProfileIdValue(), channel);
        if (!entitled) {
          invalidChannel = true;
        }
      }
      if (invalidChannel) {
        String validationMessage = this.userValidationConfig.getValidationMessageDescription(sUser, "Invalid.PersonalChannel.Id");
        ValidationResult vr = new ValidationResult();
        HashMap<String, String> errors = new HashMap(2);
        HashMap<String, ErrorDetail> errorDetails = new HashMap();
        errors.put("channeId", validationMessage);
        ErrorDetail errorDetail = new ErrorDetail("Invalid.PersonalChannel.Id", validationMessage);
        errorDetails.put("channeId", errorDetail);
        vr.setValidationFailed(true);
        vr.setErrorDetails(errorDetails);
        vr.setValidationErrors(errors);
        UserException userEx = new UserException(-2);
        userEx.setValidationResult(vr);
        throw userEx;
      }
      
      String memeberId = Integer.toString(sUser.getProfileID());
      entProfile = getEntitlementsProfileBO().getPersonalChannelProfileOfMember(sUser, memeberId, channel, extra);
      if (entProfile == null)
      {
        EntitlementGroup channelGroup = createPersonalChannelGroup(sUser, Integer.toString(sUser.getProfileID()), personalProfile, channel, extra);
        if (channelGroup == null) {
          this.logger.error("Unable to create ChannelGroup for Personal Channel Profile : - channel=" + channel);
          throw new UserException(3034);
        }
        entProfile = new EntitlementProfile();
        entProfile.setChannelId(channel);
        entProfile.setProfileId(Integer.toString(channelGroup.getGroupId()));
        entProfile.setParentProfileId(Integer.toString(channelGroup.getParentId()));
      }
      EntitlementGroupInfo entGroupWithEntitlements = new EntitlementGroupInfo();
      entGroupWithEntitlements.setGroupId(entProfile.getProfileIdValue());
      entGroupWithEntitlements.setEntitlements(personalPermissionProfile.getEntitlements());
      entGroupWithEntitlements.setEntGroupType("ConsumerPersonalProfile");
      this.groupEntitlementsBO.modifyGroupEntitlements(sUser, entGroupWithEntitlements, extra);
    } catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    return personalPermissionProfile;
  }
  

  @UserEntitled
  public com.sap.banking.user.beans.RetailUser assignProfileToUser(SecureUser sUser, com.sap.banking.user.beans.RetailUser retailUser, Map<String, Object> extraMap)
    throws UserException
  {
    RetailUserProfiles profile = retailUser.getRetailUserProfiles();
    if ((profile != null) && (!profile.isEmpty())) {
      retailUser.put("X", ((com.ffusion.beans.entitlementprofile.RetailUserProfile)profile.get(0)).getProfileEntGroupIdValue());
    } else {
      ValidationResult vr = new ValidationResult();
      HashMap<String, String> errors = new HashMap(2);
      HashMap<String, ErrorDetail> errorDetails = new HashMap();
      ErrorDetail errorDetail = null;
      errors.put("retailUserProfile", "Profile is required.");
      vr.setValidationErrors(errors);
      errorDetail = new ErrorDetail("RetailUserProfile.required", "Profile is required.");
      errorDetails.put("retailUserProfile", errorDetail);
      vr.setValidationFailed(true);
      vr.setErrorDetails(errorDetails);
      UserException userEx = new UserException(-2);
      userEx.setValidationResult(vr);
      throw userEx;
    }
    super.processEntitlementProfiles(sUser, retailUser, extraMap);
    
    return retailUser;
  }
  

  @UserEntitled
  public com.sap.banking.user.beans.RetailUser getUserById(SecureUser sUser, com.sap.banking.user.beans.RetailUser user, Map<String, Object> extra)
    throws UserException
  {
    String thisMethod = "RetailUser.getUserById";
    
    User returnUser = super.getUserById(sUser, user, extra);
    com.sap.banking.user.beans.RetailUser returnRetailuser = new com.sap.banking.user.beans.RetailUser();
    returnRetailuser.set(returnUser);
    return returnRetailuser;
  }
  
  @UserEntitled
  public com.sap.banking.user.beans.RetailUser modifyUserProfile(SecureUser sUser, com.sap.banking.user.beans.RetailUser user, Map<String, Object> extra)
    throws UserException
  {
    String thisMethod = "RetailUser.modifyUserProfile";
    user.setCustomerType(Integer.toString(2));
    EntitlementsAdmin entAdminBO = getEntitlementsAdminBO();
    
    boolean selfModification = false;
    EntitlementGroupMember member;
    try {
      member = entAdminBO.getMember(entAdminBO.getEntitlementGroupMember(user.getSecureUser()));
      
      if (member.isUsingEntProfile()) {
        if (sUser.getProfileID() == Integer.parseInt(member.getId())) {
          selfModification = true;
        }
        if (user.getEntitlementGroupId() <= 0) {
          int entGroupId = member.getEntitlementGroupId();
          EntitlementGroup entGroup = entAdminBO.getEntitlementGroup(entGroupId);
          if ("ChannelProfile".equalsIgnoreCase(entGroup.getEntGroupType())) {
            entGroupId = entGroup.getParentId();
          }
          user.setEntitlementGroupId(entGroupId);
        }
        if (!selfModification) {
          validateProfileAssignment(sUser, user, extra);
        }
      }
    } catch (EntitlementsException e) {
      this.logger.error(" Error in getting EntitlementGroupMember in method :" + thisMethod + " for user " + user.getId());
      throw new UserException(e);
    }
    
    User returnUser = super.modifyUser(sUser, user, extra, "modifyUserProfile");
    com.sap.banking.user.beans.RetailUser returnRetailuser = new com.sap.banking.user.beans.RetailUser();
    returnRetailuser.set(returnUser);
    RetailUserProfiles retailUserProfiles = user.getRetailUserProfiles();
    returnRetailuser.setRetailUserProfiles(retailUserProfiles);
    if ((member.isUsingEntProfile()) && (!selfModification)) {
      assignProfileToUser(sUser, returnRetailuser, extra);
    }
    return returnRetailuser;
  }
  



  @Deprecated
  public User modifyUserProfile(SecureUser sUser, User user, Map<String, Object> extra)
    throws UserException
  {
    return super.modifyUserProfile(sUser, user, extra);
  }
  


  @UserEntitled
  public com.sap.banking.user.beans.RetailUser addUser(SecureUser sUser, com.sap.banking.user.beans.RetailUser user, Map<String, Object> extraMap)
    throws UserException
  {
    if (this.entitlementsSetting.isEnableProfileBasedEntitlement()) {
      validateProfileAssignment(sUser, user, extraMap);
    } else {
      user.setUsingEntProfiles(Boolean.FALSE.booleanValue());
      if (user.getEntitlementGroupId() != 0) {
        user.setServicesPackageId(user.getEntitlementGroupId());
      }
    }
    user.setCustomerType(Integer.toString(2));
    User returnUser = super.addUser(sUser, user, extraMap);
    
    com.sap.banking.user.beans.RetailUser returnRetailuser = new com.sap.banking.user.beans.RetailUser();
    returnRetailuser.set(returnUser);
    RetailUserProfiles retailUserProfiles = user.getRetailUserProfiles();
    returnRetailuser.setRetailUserProfiles(retailUserProfiles);
    if (this.entitlementsSetting.isEnableProfileBasedEntitlement()) {
      assignProfileToUser(sUser, returnRetailuser, extraMap);
    }
    return returnRetailuser;
  }
  



  @Deprecated
  public User addUser(SecureUser sUser, User user, Map<String, Object> extraMap)
    throws UserException
  {
    return super.addUser(sUser, user, extraMap);
  }
  
  @UserEntitled
  public PagedResult<EntitlementInfo> getUserPersonalProfileEntitlements(SecureUser sUser, GroupEntitlementSearchBean searchCriteria, Map<String, Object> extraMap)
    throws UserException
  {
    String thisMethod = "UserImpl.getUserPersonalProfileEntitlements";
    
    EntitlementGroupInfo entitlementGroup = null;
    try {
      EntitlementProfile entProfile = getEntitlementsProfileBO().getPersonalProfileOfMember(null, Integer.toString(sUser.getProfileID()), null);
      try {
        String secondaryUserId = (String)getValueByIdFieldName("SecondaryUserProfile", "Id");
        if ((secondaryUserId != null) && (!secondaryUserId.isEmpty())) {
          searchCriteria.setMemberId(secondaryUserId);
          
          User secondaryUser = new User();
          secondaryUser.setId(secondaryUserId);
          secondaryUser = super.getUserById(sUser, secondaryUser, extraMap);
          if (com.sap.banking.user.beans.RetailUser.STATUS_DELETED.equals(secondaryUser.getAccountStatus())) {
            throw new UserException(thisMethod, 3012, "Secondary User with Id " + secondaryUserId + " not found. Please provide correct Secondary User ID.");
          }
          

          User primaryUser = getPrimaryUser(sUser, secondaryUser, extraMap);
          if (!primaryUser.getId().equalsIgnoreCase(Integer.toString(sUser.getProfileID())))
          {
            throw new UserException(thisMethod, 20001, "Secondary User with Id " + secondaryUserId + " doesnot belong to Primary User " + sUser.getProfileID());
          }
        } else {
          String primaryUserId = (String)getValueByIdFieldName("PersonalProfile", "Id");
          if ((primaryUserId != null) && (!primaryUserId.isEmpty()) && 
            (!primaryUserId.equalsIgnoreCase(Integer.toString(sUser.getProfileID())))) {
            throw new UserException(thisMethod, 20001);
          }
        }
      }
      catch (UserException e)
      {
        throw e;
      } catch (Exception e) {
        this.logger.error(e.getMessage(), e);
      }
      
      searchCriteria.setGroupId(entProfile.getProfileIdValue());
      List<String> categoryList = new ArrayList();
      categoryList.add("per personalconsumer");
      searchCriteria.setCategory(categoryList);
      entitlementGroup = this.groupEntitlementsBO.getGroupEntitlementsAndLimits(sUser, searchCriteria, extraMap);
      if (((searchCriteria.getObjectId() == null) || ("Account".equalsIgnoreCase(searchCriteria.getObjectId()))) && 
        (entitlementGroup.getEntitlements() != null) && (!entitlementGroup.getEntitlements().isEmpty())) {
        EntitlementInfos entitlementInfos = entitlementGroup.getEntitlements();
        Iterator<EntitlementInfo> entInfItr = entitlementInfos.iterator();
        EntitlementInfos accountEntitlementInfos = new EntitlementInfos();
        while (entInfItr.hasNext()) {
          EntitlementInfo entitlementInfo = (EntitlementInfo)entInfItr.next();
          if (entitlementInfo.isEntitled()) {
            GroupEntitlementSearchBean accountEntSearchBean = new GroupEntitlementSearchBean();
            accountEntSearchBean.setGroupId(searchCriteria.getGroupId());
            accountEntSearchBean.setOperationName(entitlementInfo.getOperationName());
            accountEntSearchBean.setObjectType("Account");
            accountEntSearchBean.setObjectId(searchCriteria.getObjectId());
            accountEntSearchBean.setMemberId(searchCriteria.getMemberId());
            EntitlementInfos accEntitlements = getAccountEntitlements(sUser, accountEntSearchBean, extraMap);
            if ((accEntitlements != null) && (!accEntitlements.isEmpty())) {
              accountEntitlementInfos.addAll(accEntitlements);
            }
          }
        }
        if (!accountEntitlementInfos.isEmpty()) {
          entitlementInfos.addAll(accountEntitlementInfos);
        }
      }
    }
    catch (EntitlementsException e) {
      if (e.validationFailed) {
        UserException uExcpetion = new UserException(e.getServiceError());
        uExcpetion.setValidationResult(e.getValidationResult());
        uExcpetion.setValidationFailed(true);
        throw uExcpetion;
      }
      this.logger.error(thisMethod, e);
      throw new UserException(e);
    }
    
    PagedResult<EntitlementInfo> pagedResult = new PagedResult();
    pagedResult.setPagedList(entitlementGroup.getEntitlements());
    
    return pagedResult;
  }
  
  private EntitlementInfos getAccountEntitlements(SecureUser sUser, GroupEntitlementSearchBean searchCriteria, Map<String, Object> extraMap) throws UserException {
    String methodName = "RetailUserImpl.getAccountEntitlements";
    EntitlementInfos entitlementInfos = new EntitlementInfos();
    AccountSearchCriteria accountSearchCriteria = new AccountSearchCriteria();
    SecureUser accountUser = sUser;
    String operationName = searchCriteria.getOperationName();
    String objectType = searchCriteria.getObjectType();
    String objectId = searchCriteria.getObjectId();
    try {
      if ((objectType != null) && (!"Account".equalsIgnoreCase(objectType))) {
        return new EntitlementInfos();
      }
      

      boolean externalAccount = false;
      if ((operationName != null) && (!"Access".equalsIgnoreCase(operationName))) {
        boolean accountApplicable = isAccountEntitlementApplicable(operationName);
        if (!accountApplicable) {
          return new EntitlementInfos();
        }
        
        externalAccount = isExtAccountEntitlementApplicable(operationName);
      }
      List<Account> accounts = this.bankingAccountsBO.getBankingAccounts(accountUser, accountSearchCriteria, extraMap);
      
      if ((accounts != null) && (accounts.size() > 0)) {
        Iterator<Account> accountItr = accounts.iterator();
        while (accountItr.hasNext()) {
          Account account = (Account)accountItr.next();
          if ((externalAccount) || 
            (!"2".equalsIgnoreCase(account.getCoreAccount())))
          {



            EntitlementGroupInfo accEntGroup = null;
            String accId = getEntitlementsAdminBO().getEntitlementObjectId(account);
            if ((objectId == null) || (objectId.equalsIgnoreCase(accId)))
            {



              List<String> categoryList = new ArrayList();
              categoryList.add("per personalconsumer");
              searchCriteria.setCategory(categoryList);
              Entitlement ent = new Entitlement("Access", "Account", accId);
              boolean isEntitled = false;
              if ((searchCriteria.getMemberId() != null) && (!searchCriteria.getMemberId().isEmpty())) {
                EntitlementGroupMember groupMember = new EntitlementGroupMember();
                EntitlementGroup entGroup = this.entitlementsAdminBO.getEntitlementGroup(searchCriteria.getGroupId());
                int entGroupId = searchCriteria.getGroupId();
                if ("ConsumerPersonalProfile".equalsIgnoreCase(entGroup.getEntGroupType()))
                {
                  EntitlementGroupMember egm = getEntitlementsProfileBO().getMemberByPersonalProfileId(null, Integer.toString(entGroup.getGroupId()), null);
                  entGroupId = egm.getEntitlementGroupId();
                }
                groupMember.setEntitlementGroupId(entGroupId);
                groupMember.setId(searchCriteria.getMemberId());
                groupMember.setMemberType("USER");
                groupMember.setMemberSubType("1");
                isEntitled = getEntitlementsBO().checkEntitlement(groupMember, ent);
              } else {
                isEntitled = getEntitlementsBO().checkEntitlement(searchCriteria.getGroupId(), ent);
              }
              if (isEntitled) {
                searchCriteria.setObjectId(accId);
                searchCriteria.setObjectType("Account");
                if (!"Access".equalsIgnoreCase(searchCriteria.getOperationName())) {
                  categoryList.add("per account");
                  if (externalAccount) {
                    categoryList.add("per external account");
                  }
                  
                  accEntGroup = this.groupEntitlementsBO.getGroupEntitlementsAndLimits(sUser, searchCriteria, extraMap);
                  if ((accEntGroup.getEntitlements() != null) && (!accEntGroup.getEntitlements().isEmpty())) {
                    addAccountPropertiesToEntitlementInfos(sUser, accEntGroup.getEntitlements(), account);
                    entitlementInfos.addAll(accEntGroup.getEntitlements());
                  }
                } else {
                  ent.setEntitled(true);
                  EntitlementInfo entInfo = new EntitlementInfo(ent);
                  addAccountPropertiesToEntitlementInfo(sUser, entInfo, account);
                  entitlementInfos.add(entInfo);
                }
              }
              else if ("Access".equalsIgnoreCase(searchCriteria.getOperationName())) {
                ent.setEntitled(false);
                EntitlementInfo entInfo = new EntitlementInfo(ent);
                addAccountPropertiesToEntitlementInfo(sUser, entInfo, account);
                entitlementInfos.add(entInfo);
              }
            }
          }
        }
      }
    } catch (AccountException e) { this.logger.error(methodName, e);
      throw new UserException(e);
    } catch (EntitlementsException e) {
      this.logger.error(methodName, e);
      throw new UserException(e);
    }
    return entitlementInfos;
  }
  
  private void addAccountPropertiesToEntitlementInfos(SecureUser sUser, EntitlementInfos entitlementInfos, Account account)
  {
    if ((entitlementInfos != null) && (!entitlementInfos.isEmpty())) {
      Iterator<EntitlementInfo> entInfoItr = entitlementInfos.iterator();
      while (entInfoItr.hasNext()) {
        addAccountPropertiesToEntitlementInfo(sUser, (EntitlementInfo)entInfoItr.next(), account);
      }
    }
  }
  
  private void addAccountPropertiesToEntitlementInfo(SecureUser sUser, EntitlementInfo entitlementInfo, Account account)
  {
    if (entitlementInfo.getEntitlementProperties() == null) {
      entitlementInfo.setEntitlementProperties(new EntitlementTypePropertyLists());
    }
    EntitlementTypePropertyLists entTypePropLists = entitlementInfo.getEntitlementProperties();
    EntitlementTypePropertyList entTypePropList = entTypePropLists.getByOperationName(entitlementInfo.getOperationName());
    
    if (entTypePropList == null) {
      entTypePropList = new EntitlementTypePropertyList(entitlementInfo.getOperationName());
    }
    
    if (entTypePropList.getPropertiesMap() == null) {
      entTypePropList.setPropertiesMap(new HashMap());
    }
    
    entTypePropList.getPropertiesMap().putAll(getAccountProperties(sUser, account));
  }
  
  private HashMap getAccountProperties(SecureUser sUser, Account account) {
    HashMap accountPropertyMap = new HashMap();
    accountPropertyMap.put("AccountDisplayTextNickNameCurrency", account.getNickName());
    return accountPropertyMap;
  }
  
  private boolean isAccountEntitlementApplicable(String operationName) throws EntitlementsException
  {
    boolean accountApplicable = false;
    
    EntitlementTypePropertyList propList = this.entitlementsAdminBO.getEntitlementTypeWithProperties(operationName);
    HashMap propsMap = propList.getPropertiesMap();
    
    if (propsMap.containsKey("category")) {
      Object catValue = propsMap.get("category");
      if ((catValue instanceof List)) {
        List list = (List)catValue;
        if ((list != null) && (!list.isEmpty()) && 
          (list.contains("per personalconsumer")) && (list.contains("per account"))) {
          accountApplicable = true;
        }
      }
    }
    
    return accountApplicable;
  }
  
  private boolean isExtAccountEntitlementApplicable(String operationName) throws EntitlementsException
  {
    boolean accountApplicable = false;
    
    EntitlementTypePropertyList propList = this.entitlementsAdminBO.getEntitlementTypeWithProperties(operationName);
    HashMap propsMap = propList.getPropertiesMap();
    
    if (propsMap.containsKey("category")) {
      Object catValue = propsMap.get("category");
      if ((catValue instanceof List)) {
        List list = (List)catValue;
        if ((list != null) && (!list.isEmpty()) && 
          (list.contains("per consumer")) && (list.contains("per external account"))) {
          accountApplicable = true;
        }
      }
    }
    
    return accountApplicable;
  }
  
  @UserEntitled
  public Features getRetailUserFeatures(SecureUser sUser, Map<String, Object> extraMap)
    throws UserException
  {
    String methodName = "RetailUserImpl.getRetailUserFeatures";
    Features features = new Features();
    


    boolean usingEntProfile = false;
    try {
      EntitlementGroupMember member = this.entitlementsAdminBO.getEntitlementGroupMember(sUser);
      member = this.entitlementsAdminBO.getMember(member);
      usingEntProfile = this.entitlementsAdminBO.isUsingEntitlementProfiles(member.getEntitlementGroupId());
      if (usingEntProfile) {
        int groupId = member.getEntitlementGroupId();
        ProfileFeaturesSearchCriteria searchBean = new ProfileFeaturesSearchCriteria();
        EntitlementGroup entGroup = this.entitlementsAdminBO.getEntitlementGroup(member.getEntitlementGroupId());
        if ("ChannelProfile".equalsIgnoreCase(entGroup.getEntGroupType())) {
          groupId = entGroup.getParentId();
          entGroup = this.entitlementsAdminBO.getEntitlementGroup(groupId);
        }
        if ("ConsumerProfile".equalsIgnoreCase(entGroup.getEntGroupType())) {
          searchBean.setProfileId(Integer.toString(entGroup.getGroupId()));
          features = this.retailUserProfileBO.getRetailUserProfileFeatures(sUser, searchBean, extraMap);
        } else {
          throw new UserException(2);
        }
        

      }
      else
      {
        EntitlementGroup entGroup = this.entitlementsAdminBO.getParentGroupByGroupType(member.getEntitlementGroupId(), "UserType");
        if ((entGroup != null) && ("Consumers".equalsIgnoreCase(entGroup.getGroupName()))) {
          getEntitlementsProfileBO().getFeatures(sUser, entGroup.getGroupId(), extraMap);
          features = getEntitlementsProfileBO().getFeatures(sUser, entGroup.getGroupId(), extraMap);
        }
      }
    } catch (EntitlementsException e) {
      this.logger.error(methodName, e);
      throw new UserException(e);
    }
    return features;
  }
  


  @UserEntitled
  public com.sap.banking.user.beans.RetailUser deleteUser(SecureUser sUser, com.sap.banking.user.beans.RetailUser user, Map<String, Object> extraMap)
    throws UserException
  {
    com.sap.banking.user.beans.RetailUser deleteUser = getUserById(sUser, user, extraMap);
    deleteUser.setAccountStatus(com.sap.banking.user.beans.RetailUser.STATUS_DELETED);
    modifyUser(sUser, deleteUser, extraMap);
    return deleteUser;
  }
}
