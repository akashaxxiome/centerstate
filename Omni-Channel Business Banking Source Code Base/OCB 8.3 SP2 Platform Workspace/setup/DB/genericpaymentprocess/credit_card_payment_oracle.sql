create table credit_card_rec_payment (
	crdt_crd_rec_pymnt_id VARCHAR2 (32),
	tracking_id VARCHAR2(32) not null,
	account_id VARCHAR2(48) not null,
	credit_card_account_id VARCHAR2(48) not null,
	amount VARCHAR2(32) not null,
	currency VARCHAR2(3) not null,
	status VARCHAR2(40) not null,
	created_date DATE not null,
	payment_date DATE not null,
	schedule_id VARCHAR2(48),
	process_id VARCHAR2 (50),
	result_of_approval VARCHAR2(100),
	action VARCHAR2 (40),
	locale_lang VARCHAR2(2) not null,
	locale_country VARCHAR2(3) not null,
	num_payments NUMBER not null,
	frequency NUMBER not null,
	end_date DATE not null,
	submitted_by VARCHAR2(32) not null
);

ALTER TABLE credit_card_rec_payment ADD  PRIMARY KEY (crdt_crd_rec_pymnt_id);

create table credit_card_payment (
	crdt_crd_pymnt_id VARCHAR2 (32),
	tracking_id VARCHAR2(32) not null,
	account_id VARCHAR2(48) not null,
	credit_card_account_id VARCHAR2(48) not null,
	amount VARCHAR2(32) not null,
	currency VARCHAR2(3) not null,
	status VARCHAR2(40) not null,
	created_date DATE not null,
	payment_date DATE not null,
	schedule_id VARCHAR2(48),
	process_id VARCHAR2 (50),
	result_of_approval VARCHAR2(100),
	action VARCHAR2 (40),
	locale_lang VARCHAR2(2) not null,
	locale_country VARCHAR2(3) not null,
	crdt_crd_rec_pymnt_id VARCHAR2(32),
	submitted_by VARCHAR2(32) not null,
	rec_id VARCHAR2(32),
	instance_num NUMBER
);

ALTER TABLE credit_card_payment ADD  PRIMARY KEY (crdt_crd_pymnt_id);

ALTER TABLE credit_card_payment ADD FOREIGN KEY (crdt_crd_rec_pymnt_id) REFERENCES credit_card_rec_payment (crdt_crd_rec_pymnt_id);

INSERT INTO entitlement_list values( 'Credit Card Payment' );

INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'cross account' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per account' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per user' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per company' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per division' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per group' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per business' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per package' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'module', 'yes' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'module_name', 'Credit Card Payment' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'display name', 'Credit Card Payment' );   
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'component', 'Payments & Transfers' );

INSERT INTO entitlement_list values( 'Credit Card Payment (admin)' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )
VALUES( 'Credit Card Payment', 'admin partner', 'Credit Card Payment (admin)' );


INSERT INTO limit_list values ('Credit Card Payment');

INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'cross account' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per account' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per user' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per company' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per division' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per group' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per business' );

INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'component', 'Payments & Transfers' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'display parent', 'Credit Card Payment' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'control parent', 'Credit Card Payment' );

INSERT INTO REQ_APPR_LIST values ('Credit Card Payment');

INSERT INTO SCH_InstructionType( FIId,RouteID,InstructionType,HandlerClassName,SchedulerStartTime,SchedulerInterval,DispatchStartTime,DispatchInterval,ResubmitEventSupported,ChkTmAutoRecover,FileBasedRecovery,ActiveFlag,Category,Memo ) 
VALUES('1000',0,'CCPMTTRN','com.ffusion.ffs.bpw.handler.GenericPaymentHandler','01:00 AM',1440,'01:30 AM',1440,1,0,0,1,'Other','Credit Card Payment to Backend');

INSERT INTO SCH_InstructionType( FIId,RouteID,InstructionType,HandlerClassName,SchedulerStartTime,SchedulerInterval,DispatchStartTime,DispatchInterval,ResubmitEventSupported,ChkTmAutoRecover,FileBasedRecovery,ActiveFlag,Category,Memo ) 
VALUES('1000',0,'CCRECPMTTRN','com.ffusion.ffs.bpw.handler.GenericRecPaymentHandler','01:00 AM',1440,'01:30 AM',1440,1,0,0,1,'Other','Credit Card Payment to Backend');

