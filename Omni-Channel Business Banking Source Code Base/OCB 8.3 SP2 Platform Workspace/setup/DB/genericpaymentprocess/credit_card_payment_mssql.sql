create table credit_card_rec_payment (
	crdt_crd_rec_pymnt_id VARCHAR (32) not null,
	tracking_id VARCHAR(32) not null,
	account_id VARCHAR(48) not null,
	credit_card_account_id VARCHAR(48) not null,
	amount VARCHAR(32) not null,
	currency VARCHAR(3) not null,
	status VARCHAR(40) not null,
	created_date DATETIME not null,
	payment_date DATETIME not null,
	schedule_id VARCHAR(48),
	process_id VARCHAR (50),
	result_of_approval VARCHAR(100),
	action VARCHAR (40),
	locale_lang VARCHAR(2) not null,
	locale_country VARCHAR(3) not null,
	num_payments INTEGER not null,
	frequency INTEGER not null,
	end_date DATETIME not null,
	submitted_by VARCHAR(32) not null
);

ALTER TABLE credit_card_rec_payment ADD  PRIMARY KEY (crdt_crd_rec_pymnt_id);

insert into sequences (sequence_name, sequence_id) values ('crdt_crd_rec_pymnt_id_seq', 1);

create table credit_card_payment (
	crdt_crd_pymnt_id VARCHAR (32) not null,
	tracking_id VARCHAR(32) not null,
	account_id VARCHAR(48) not null,
	credit_card_account_id VARCHAR(48) not null,
	amount VARCHAR(32) not null,
	currency VARCHAR(3) not null,
	status VARCHAR(40) not null,
	created_date DATETIME not null,
	payment_date DATETIME not null,
	schedule_id VARCHAR(48),
	process_id VARCHAR (50),
	result_of_approval VARCHAR(100),
	action VARCHAR (40),
	locale_lang VARCHAR(2) not null,
	locale_country VARCHAR(3) not null,
	crdt_crd_rec_pymnt_id VARCHAR(32),
	submitted_by VARCHAR(32) not null,
	rec_id VARCHAR(32),
	instance_num INTEGER
);

ALTER TABLE credit_card_payment ADD  PRIMARY KEY (crdt_crd_pymnt_id);

insert into sequences (sequence_name, sequence_id) values ('crdt_crd_pymnt_id_seq', 1);

ALTER TABLE credit_card_payment ADD FOREIGN KEY (crdt_crd_rec_pymnt_id) REFERENCES credit_card_rec_payment (crdt_crd_rec_pymnt_id);

INSERT INTO entitlement_list values( 'Credit Card Payment' );

INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'cross account' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per account' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per user' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per company' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per division' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per group' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per business' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per package' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'module', 'yes' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'module_name', 'Credit Card Payment' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'display name', 'Credit Card Payment' );   
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'component', 'Payments & Transfers' );

INSERT INTO entitlement_list values( 'Credit Card Payment (admin)' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )
VALUES( 'Credit Card Payment', 'admin partner', 'Credit Card Payment (admin)' );


INSERT INTO limit_list values ('Credit Card Payment');

INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'cross account' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per account' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per user' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per company' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per division' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per group' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'category', 'per business' );

INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'component', 'Payments & Transfers' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'display parent', 'Credit Card Payment' );
INSERT INTO limit_type_props ( operation_name, prop_name, prop_value ) VALUES ( 'Credit Card Payment', 'control parent', 'Credit Card Payment' );

INSERT INTO REQ_APPR_LIST values ('Credit Card Payment');

INSERT INTO SCH_InstructionType( FIId,RouteID,InstructionType,HandlerClassName,SchedulerStartTime,SchedulerInterval,DispatchStartTime,DispatchInterval,ResubmitEventSupported,ChkTmAutoRecover,FileBasedRecovery,ActiveFlag,Category,Memo ) 
VALUES('1000',0,'CCPMTTRN','com.ffusion.ffs.bpw.handler.GenericPaymentHandler','01:00 AM',1440,'01:30 AM',1440,1,0,0,1,'Other','Credit Card Payment to Backend');

INSERT INTO SCH_InstructionType( FIId,RouteID,InstructionType,HandlerClassName,SchedulerStartTime,SchedulerInterval,DispatchStartTime,DispatchInterval,ResubmitEventSupported,ChkTmAutoRecover,FileBasedRecovery,ActiveFlag,Category,Memo ) 
VALUES('1000',0,'CCRECPMTTRN','com.ffusion.ffs.bpw.handler.GenericRecPaymentHandler','01:00 AM',1440,'01:30 AM',1440,1,0,0,1,'Other','Credit Card Payment to Backend');

