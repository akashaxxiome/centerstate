INSERT INTO entitlement_list values( 'Custom Internal Transfer' );

INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per account' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per user' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per company' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per division' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per group' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per business' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per consumer' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'per package' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'category', 'cross account' );


------------------------------------------------------------------------------------


INSERT INTO entitlement_list values( 'Custom Internal Transfer (admin)' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value)VALUES( 'Custom Internal Transfer', 'admin partner', 'Custom Internal Transfer (admin)' );


INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Payees', 'control parent', 'Custom Internal Transfer' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Payees', 'display parent', 'Custom Internal Transfer' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'component', 'Payments & Transfers' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'display name', 'Custom Internal Transfer' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'module', 'yes' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'module_name', 'Transfers' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'control parent', 'Transfers' );
INSERT INTO ent_type_props ( operation_name, prop_name, prop_value )VALUES ( 'Custom Internal Transfer', 'display parent', 'Transfers' );   





