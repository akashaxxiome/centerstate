package com.ffusion.services.account.profile.custom;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.business.Business;
import com.ffusion.beans.business.Businesses;
import com.ffusion.beans.user.User;
import com.ffusion.beans.user.Users;
import com.ffusion.csil.CSILException;
import com.ffusion.csil.beans.entitlements.Entitlement;
import com.ffusion.csil.beans.entitlements.EntitlementGroup;
import com.ffusion.csil.beans.entitlements.EntitlementGroupMember;
import com.ffusion.csil.beans.entitlements.EntitlementProfile;
import com.ffusion.csil.beans.entitlements.Entitlements;
import com.ffusion.efs.adapters.profile.exception.ProfileException;
import com.ffusion.efs.adapters.profile.interfaces.AccountAdapter;
import com.ffusion.efs.adapters.profile.interfaces.BusinessAdapter;
import com.ffusion.efs.adapters.profile.interfaces.CustomerAdapter;
import com.ffusion.efs.adapters.profile.interfaces.ObjectPropertyAdapter;
import com.ffusion.services.account.profile.interfaces.AccountService;
import com.sap.banking.common.annotations.Cacheable;
import com.sap.banking.entitlements.bo.interfaces.EntitlementsAdmin;
import com.sap.banking.entitlements.bo.interfaces.EntitlementsProfile;
import com.sap.banking.entitlements.exception.EntitlementsException;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@com.ffusion.services.account.profile.annotations.AccountService("customProfileAccountsService")
public class CustomAccountServiceImpl implements AccountService {

	  protected static final String USD_CURRENCY_CODE = "USD";
	  protected static final String PROP_RESTRICTED_CALCULATIONS = "Restricted Calculations";
	  private static final Logger logger = LoggerFactory.getLogger(CustomAccountServiceImpl.class);
	  





	  protected static final String[] m_restrictedEntitlementNamesExternal = { "Transfers To", "Transfers From", "Payments", "ACHBatch", "CCD + TXP", "CCD + DED", "Wires", "Wire Templates Create", "Wire Templates Delete", "Wire Templates Approval", "Wires Create", "Wire Domestic", "Wire International", "Wire Drawdown", "Wire FED", "Wire Book", "Stops", "Positive Pay", "Reverse Positive Pay", "Cash Con - Disbursement Request", "Cash Con - Deposit Entry", "AccountAggregation" };
	  




























	  protected static final String[] m_nonUSDRestrictedEntitlementNamesExternal = { "External Transfers To", "External Transfers To Unverified Account" };
	  








	  protected static final String[] m_restrictedEntitlementNamesCore = { "External Transfers To", "External Transfers To Unverified Account" };
	  




	  protected static final String[][] m_restrictedEntitlementNames = { m_restrictedEntitlementNamesExternal, m_nonUSDRestrictedEntitlementNamesExternal, m_restrictedEntitlementNamesCore };
	  


	  protected static final int ENT_INDEX_EXTERNAL = 0;
	  


	  protected static final int ENT_INDEX_EXTERNAL_NON_USD = 1;
	  


	  protected static final int ENT_INDEX_CORE = 2;
	  


	  @Autowired
	  private CustomerAdapter customerAdapter;
	  


	  @Autowired
	  private BusinessAdapter businessAdapter;
	  


	  @Autowired
	  private AccountAdapter accountAdapter;
	  


	  @Autowired
	  private ObjectPropertyAdapter objectPropertyAdapter;
	  


	  @Autowired
	  EntitlementsProfile entitlementsProfileBO;
	  


	  @Autowired
	  EntitlementsAdmin entitlementsAdminBO;
	  


	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public Account addAccount(SecureUser sUser, Account account, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      Account acc = null;
	      boolean sbUser = false;
	      
	      if ((sUser.getBusinessID() != 0) && (sUser.getUserType() == 1))
	      {
	        acc = accountAdapter.addAccount(account, sUser.getBusinessID());
	        sbUser = true;
	      }
	      else {
	        acc = accountAdapter.addAccount(account, sUser.getProfileID());
	      }
	      




	      mapAccount(acc);
	      



	      if (!sbUser)
	      {
	        User primaryUser = new User();
	        primaryUser.setId(String.valueOf(sUser.getProfileID()));
	        primaryUser = getUserDetails(sUser, primaryUser);
	        int entGroupId = 0;
	        if (primaryUser.isUsingEntProfiles()) {
	          EntitlementGroupMember primaryMember = primaryUser.getEntitlementGroupMember();
	          primaryMember.setChannelId("Web");
	          EntitlementProfile defProfile = entitlementsProfileBO.getEntitlementProfileByMember(primaryMember);
	          if (defProfile == null) {
	            throw new ProfileException("AccountServiceImpl.addAccount", 3800, "No entitlement profile found for channel: Web");
	          }
	          
	          entGroupId = defProfile.getProfileIdValue();
	          primaryUser.setEntitlementGroupId(entGroupId);
	        } else {
	          entGroupId = primaryUser.getEntitlementGroupId();
	        }
	        Users secondaryUsers = customerAdapter.getSecondaryUsers(sUser, primaryUser, extra);
	        User loopUser = null;
	        for (Iterator i = secondaryUsers.iterator(); i.hasNext();) {
	          loopUser = (User)i.next();
	          loopUser.setEntitlementGroupId(entGroupId);
	        }
	        Accounts accounts = new Accounts();
	        accounts.add(account);
	        
	        entitlementsAdminBO.addSecondaryUserAccountRestrictions(sUser, secondaryUsers, false, accounts);
	      }
	      














	      return acc;
	    } catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  










	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public void modifyAccount(SecureUser sUser, Account account, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      if ((sUser.getBusinessID() != 0) && (sUser.getUserType() == 1)) {
	        accountAdapter.modifyAccount(account, sUser.getBusinessID());
	      } else {
	        accountAdapter.modifyAccount(account, sUser.getProfileID());
	      }
	    } catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  















	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public void modifyAccountById(SecureUser sUser, int directoryId, Account account, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      accountAdapter.modifyAccount(account, directoryId);
	    } catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  















	  private void deleteAccountObjectProperties(SecureUser sUser, Account account, int directoryId, HashMap extra)
	    throws ProfileException
	  {
	    String accountId = account.getID();
	    StringBuffer objectId = new StringBuffer();
	    int objectType = 3;
	    
	    if ((accountId != null) && (accountId.length() > 0))
	    {

	      objectId.append(account.getBankID());
	      objectId.append(":");
	      objectId.append(account.getRoutingNum());
	      objectId.append(":");
	      objectId.append(account.getID());
	      
	      objectPropertyAdapter.deleteObjectProperty(objectType, objectId.toString(), "Restricted Calculations");
	    }
	    else
	    {
	      Accounts accountsToRemove = getAccountsById(sUser, directoryId, extra);
	      
	      if (accountsToRemove != null) {
	        Iterator accountsIter = accountsToRemove.iterator();
	        while (accountsIter.hasNext()) {
	          Account accToRemove = (Account)accountsIter.next();
	          
	          objectId.append(accToRemove.getBankID());
	          objectId.append(":");
	          objectId.append(accToRemove.getRoutingNum());
	          objectId.append(":");
	          objectId.append(accToRemove.getID());
	          

	          objectPropertyAdapter.deleteObjectProperty(objectType, objectId.toString(), "Restricted Calculations");
	          
	          objectId.setLength(0);
	        }
	      }
	    }
	  }
	  























	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public void deleteAccount(SecureUser sUser, Account account, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      if ((sUser.getBusinessID() != 0) && (sUser.getUserType() == 1))
	      {

	        deleteAccountObjectProperties(sUser, account, sUser.getBusinessID(), extra);
	        accountAdapter.deleteAccount(account, sUser.getBusinessID());
	      }
	      else {
	        deleteAccountObjectProperties(sUser, account, sUser.getProfileID(), extra);
	        accountAdapter.deleteAccount(account, sUser.getProfileID());
	      }
	    }
	    catch (Exception ex)
	    {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  























	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public void deleteAccount(SecureUser sUser, Account account, int directoryId, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      deleteAccountObjectProperties(sUser, account, directoryId, extra);
	      
	      accountAdapter.deleteAccount(account, directoryId);
	    }
	    catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  











	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public Accounts mergeAccounts(SecureUser sUser, Accounts accounts, int directoryId, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      return accountAdapter.mergeAccounts(accounts, directoryId);
	    } catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  






	  public Account getAccount(SecureUser sUser, HashMap extra)
	    throws ProfileException
	  {
	    throw new ProfileException(3800);
	  }
	  







	  public Account getAccountAddress(SecureUser sUser, Account account, HashMap extra)
	    throws ProfileException
	  {
	    accountAdapter.getAccountAddress(account);
	    return account;
	  }
	  







	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public Accounts getAccounts(SecureUser sUser, HashMap extra)
	    throws ProfileException
	  {
	    if ((sUser.getBusinessID() != 0) && (sUser.getUserType() == 1))
	    {
	      return getAccountsById(sUser, sUser.getBusinessID(), extra);
	    }
	    
	    return getAccountsById(sUser, sUser.getPrimaryUserID(), extra);
	  }
	  










	  public Accounts searchAccounts(SecureUser sUser, Account account, int id, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      return accountAdapter.searchAccounts(account, id);
	    }
	    catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  







	  public Accounts getAccounts(SecureUser sUser, Account searchAcct, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      Accounts retAccounts = null;
	      
	      if ((sUser.getBusinessID() != 0) && (sUser.getUserType() == 1)) {
	        retAccounts = accountAdapter.getAccounts(searchAcct, sUser.getBusinessID());
	      } else if (sUser.getProfileID() != sUser.getPrimaryUserID())
	      {
	        retAccounts = accountAdapter.getAccounts(searchAcct, sUser.getPrimaryUserID());
	      }
	      else {
	        retAccounts = accountAdapter.getAccounts(searchAcct, sUser.getProfileID());
	      }
	      


	      mapAccounts(retAccounts);
	      setAccountFilters(retAccounts);
	      
	      return retAccounts;
	    } catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  












	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public Accounts getAccountsById(SecureUser sUser, int directoryId, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      Accounts retAccounts = accountAdapter.getAccountsById(directoryId);
	      
	      /*
	      OutputStream file = new FileOutputStream("C://TEMP//BankingAccounts.dat");
	      OutputStream buffer = new BufferedOutputStream(file);
	      ObjectOutput output = new ObjectOutputStream(buffer);
	      output.writeObject(retAccounts);
	      */	     
	      
	      retAccounts.setLocale(sUser.getLocaleLanguage());
	      


	      mapAccounts(retAccounts);
	      setAccountFilters(retAccounts);
	      
	      return retAccounts;
	    }
	    catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  






	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public Accounts getAccountsByBusinessEmployee(SecureUser sUser, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      Accounts retAccounts = accountAdapter.getAccountsByBusinessEmployee(sUser);
	      


	      mapAccounts(retAccounts);
	      setAccountFilters(retAccounts);
	      
	      return retAccounts;
	    }
	    catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  
















	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.AccountService8Interceptor")
	  public void addAccounts(SecureUser user, Accounts accounts, int directoryId, HashMap extra)
	    throws ProfileException
	  {
	    try
	    {
	      accountAdapter.addAccounts(accounts, directoryId);
	      



	      mapAccounts(accounts);
	      


	      if (user.getProfileID() == user.getPrimaryUserID())
	      {

	        User primaryUser = new User();
	        primaryUser.setId(String.valueOf(user.getProfileID()));
	        primaryUser = getUserDetails(user, primaryUser);
	        int entGroupId = 0;
	        if (primaryUser.isUsingEntProfiles()) {
	          EntitlementGroupMember pmember = primaryUser.getEntitlementGroupMember();
	          pmember.setChannelId("Web");
	          EntitlementProfile defProfile = entitlementsProfileBO.getEntitlementProfileByMember(pmember);
	          if (defProfile == null) {
	            throw new ProfileException("AccountServiceImpl.addAccounts", 3800, "No entitlement profile found for channel: Web");
	          }
	          
	          entGroupId = defProfile.getProfileIdValue();
	          primaryUser.setEntitlementGroupId(entGroupId);
	        } else {
	          entGroupId = primaryUser.getEntitlementGroupId();
	        }
	        Users secondaryUsers = customerAdapter.getSecondaryUsers(user, primaryUser, extra);
	        User loopUser = null;
	        for (Iterator i = secondaryUsers.iterator(); i.hasNext();) {
	          loopUser = (User)i.next();
	          loopUser.setEntitlementGroupId(entGroupId);
	        }
	        entitlementsAdminBO.addSecondaryUserAccountRestrictions(user, secondaryUsers, false, accounts);
	      }
	      


	      if (user.getUserType() == 2)
	      {

	        User primaryUser = new User();
	        primaryUser.setId(String.valueOf(directoryId));
	        try
	        {
	          primaryUser = getUserDetails(user, primaryUser);
	          if (User.CUSTOMER_TYPE_CONSUMER.equals(primaryUser.getCustomerType()))
	          {

	            SecureUser sUser = primaryUser.getSecureUser();
	            
	            Users secondaryUsers = customerAdapter.getSecondaryUsers(sUser, primaryUser, extra);
	            
	            entitlementsAdminBO.addSecondaryUserAccountRestrictions(sUser, secondaryUsers, false, accounts);
	          }
	        }
	        catch (Exception localException1) {}
	      }
	      


	      if ((extra == null) || (extra.get("SKIPRESTRICTIONS") == null)) {
	        updateRestrictedAccountEntitlements(user, accounts, directoryId, true, extra);
	      }
	    }
	    catch (Exception ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	  }
	  



	  public int initialize()
	    throws ProfileException
	  {
	    return 2;
	  }
	  






	  protected int mapError(Exception ex)
	  {
	    int mappedError = 2;
	    
	    ex.printStackTrace(System.out);
	    
	    if ((ex instanceof ProfileException))
	    {
	      ProfileException pe = (ProfileException)ex;
	      logger.error("mapProfileException: " + pe.where + " : " + pe.why + " : " + pe.code);
	      
	      switch (pe.code) {
	      case 0: 
	        mappedError = 0;
	        break;
	      case 1: 
	        mappedError = 3501;
	        break;
	      default: 
	        mappedError = 2;
	      }
	      
	    }
	    return mappedError;
	  }
	  





	  protected void mapAccounts(Accounts accs)
	  {
	    Iterator accIterator = accs.iterator();
	    
	    while (accIterator.hasNext())
	    {
	      Account acc = (Account)accIterator.next();
	      

	      mapAccount(acc);
	    }
	  }
	  





	  protected void mapAccount(Account acc)
	  {
	    acc.setAccountGroup(mapAccountType(acc.getTypeValue()));
	  }
	  







	  protected int mapAccountType(int accType)
	  {
	    return Account.getAccountGroupFromType(accType);
	  }
	  




	  protected void setAccountFilters(Accounts accs)
	  {
	    Iterator accIterator = accs.iterator();
	    while (accIterator.hasNext()) {
	      Account acc = (Account)accIterator.next();
	      acc.setFilterable("TransferTo");
	      acc.setFilterable("TransferFrom");
	      acc.setFilterable("Transactions");
	      acc.setFilterable("BillPay");
	      acc.setFilterable("TransferTemplate");
	    }
	  }
	  























	  protected void updateRestrictedAccountEntitlements(SecureUser user, Accounts accounts, int directoryId, boolean addMode, HashMap extra)
	    throws ProfileException
	  {
	    Entitlements[] restrictedEntitlements = generateEntitlements();
	    EntitlementGroupMember member = null;
	    try {
	      member = entitlementsAdminBO.getEntitlementGroupMember(user);
	    } catch (EntitlementsException e1) {
	      logger.debug("Unable to get entitlement group member", e1);
	    }
	    EntitlementGroup businessGroup = null;
	    Entitlement restrictedEntitlement = null;
	    
	    try
	    {
	      businessGroup = getBusinessEntitlementGroup(directoryId);
	      
	      if (businessGroup != null)
	      {
	        int businessGroupID = businessGroup.getGroupId();
	        
	        for (Iterator acctsItr = accounts.iterator(); acctsItr.hasNext();)
	        {
	          Account currAccount = (Account)acctsItr.next();
	          String currAccountObjectID = entitlementsAdminBO.getEntitlementObjectId(currAccount);
	          

	          if ((currAccount.getCoreAccount().equals("0")) || 
	            (currAccount.getCoreAccount().equals("2")))
	          {
	            for (Iterator entsItr = restrictedEntitlements[0].iterator(); entsItr.hasNext();)
	            {
	              restrictedEntitlement = (Entitlement)entsItr.next();
	              restrictedEntitlement.setObjectId(currAccountObjectID);
	            }
	            
	            if (addMode)
	            {
	              entitlementsAdminBO.addRestrictedEntitlements(member, businessGroupID, restrictedEntitlements[0]);

	            }
	            else
	            {
	              entitlementsAdminBO.removeRestrictedEntitlements(member, businessGroupID, restrictedEntitlements[0]);
	            }
	            


	            if (!"USD".equals(currAccount.getCurrencyCode()))
	            {
	              Iterator entsItr = restrictedEntitlements[1].iterator();
	              while (entsItr.hasNext())
	              {
	                restrictedEntitlement = (Entitlement)entsItr.next();
	                restrictedEntitlement.setObjectId(currAccountObjectID);
	              }
	              
	              if (addMode)
	              {
	                entitlementsAdminBO.addRestrictedEntitlements(member, businessGroupID, restrictedEntitlements[1]);

	              }
	              else
	              {
	                entitlementsAdminBO.removeRestrictedEntitlements(member, businessGroupID, restrictedEntitlements[1]);
	              }
	              
	            }
	            

	          }
	          else if (currAccount.getCoreAccount().equals("1"))
	          {
	            for (Iterator entsItr = restrictedEntitlements[2].iterator(); entsItr.hasNext();)
	            {
	              restrictedEntitlement = (Entitlement)entsItr.next();
	              restrictedEntitlement.setObjectId(currAccountObjectID);
	            }
	            
	            if (addMode)
	            {
	              entitlementsAdminBO.addRestrictedEntitlements(member, businessGroupID, restrictedEntitlements[2]);

	            }
	            else
	            {
	              entitlementsAdminBO.removeRestrictedEntitlements(member, businessGroupID, restrictedEntitlements[2]);
	            }
	          }
	        }
	      }
	    }
	    catch (Exception e) {
	      int businessGroupID;
	      Iterator acctsItr;
	      throw new ProfileException(2, e);
	    }
	  }
	  



















	  protected Entitlements[] generateEntitlements()
	  {
	    Entitlements[] result = new Entitlements[m_restrictedEntitlementNames.length];
	    
	    Entitlement restrictedEntitlement = null;
	    int setNum = 0;
	    
	    for (int i = 0; i < m_restrictedEntitlementNames.length; i++)
	    {
	      result[i] = new Entitlements();
	      
	      for (int j = 0; j < m_restrictedEntitlementNames[i].length; j++)
	      {
	        restrictedEntitlement = new Entitlement();
	        restrictedEntitlement.setOperationName(m_restrictedEntitlementNames[i][j]);
	        restrictedEntitlement.setObjectType("Account");
	        result[i].add(restrictedEntitlement);
	      }
	    }
	    try {
	      if (entitlementsAdminBO.entitlementTypeExists("Sample Bank Report - AcctSummary")) {
	        restrictedEntitlement = new Entitlement();
	        restrictedEntitlement.setOperationName("Sample Bank Report - AcctSummary");
	        restrictedEntitlement.setObjectType("Account");
	        result[0].add(restrictedEntitlement);
	      }
	    }
	    catch (CSILException localCSILException) {}
	    

	    return result;
	  }
	  













	  protected EntitlementGroup getBusinessEntitlementGroup(int businessID)
	    throws Exception
	  {
	    EntitlementGroup result = null;
	    Business business = null;
	    

	    business = businessAdapter.getBusiness(businessID);
	    
	    if (business != null)
	    {

	      EntitlementGroupMember member = entitlementsAdminBO.getMember(business.getEntitlementGroupMember());
	      

	      result = entitlementsAdminBO.getEntitlementGroup(member.getEntitlementGroupId());
	    }
	    
	    return result;
	  }
	  







	  public ArrayList getRestrictedCalculations(SecureUser sUser, Account acc, HashMap extra)
	    throws ProfileException
	  {
	    int objectType = 3;
	    

	    StringBuffer objId = new StringBuffer(acc.getBankID());
	    objId.append(":");
	    objId.append(acc.getRoutingNum());
	    objId.append(":");
	    objId.append(acc.getID());
	    String objectId = objId.toString();
	    
	    String rs = objectPropertyAdapter.getObjectPropertyValue(objectType, objectId, "Restricted Calculations");
	    
	    ArrayList typeCodes = new ArrayList();
	    if (rs != null)
	    {
	      String delimiter = ",";
	      StringTokenizer tokens = new StringTokenizer(rs, delimiter);
	      while (tokens.hasMoreTokens()) {
	        typeCodes.add(tokens.nextToken());
	      }
	    }
	    return typeCodes;
	  }
	  






	  public HashMap getAllRestrictedCalculations(SecureUser sUser, HashMap extra)
	    throws ProfileException
	  {
	    return objectPropertyAdapter.getObjectPropertyValueMap(3, "Restricted Calculations");
	  }
	  






	  public int getAllRestrictedCalculationsCount(SecureUser sUser, HashMap extra)
	    throws ProfileException
	  {
	    return objectPropertyAdapter.getObjectPropertyCount(3, "Restricted Calculations");
	  }
	  








	  public void saveRestrictedCalculations(SecureUser sUser, Account acc, ArrayList typeCodes, HashMap extra)
	    throws ProfileException
	  {
	    int objectType = 3;
	    String objectId = acc.getBankID() + ":" + acc.getRoutingNum() + ":" + acc.getID();
	    Iterator codes = typeCodes.iterator();
	    StringBuffer commaSeparatedList = new StringBuffer();
	    String comma = ",";
	    boolean initial = true;
	    
	    while (codes.hasNext()) {
	      if (initial) {
	        commaSeparatedList.append((String)codes.next());
	        initial = false;
	      } else {
	        String nextCode = (String)codes.next();
	        commaSeparatedList.append(comma);
	        commaSeparatedList.append(nextCode);
	      }
	    }
	    
	    objectPropertyAdapter.modifyObjectProperty(objectType, objectId, "Restricted Calculations", commaSeparatedList.toString());
	  }
	  











	  public Businesses getBusinessesForAccount(SecureUser sUser, Account account, HashMap extra)
	    throws ProfileException
	  {
	    return accountAdapter.getBusinessesForAccount(account, extra);
	  }
	  



























	  public void fillAccountCollection(Accounts accounts, HashMap extra)
	    throws ProfileException
	  {
	    for (int pos = 0; pos < accounts.size(); pos++) {
	      Account account = (Account)accounts.get(pos);
	      
	      Accounts returnAccounts = accountAdapter.getAccounts(account, -1);
	      if (returnAccounts.size() > 0) {
	        account.set((Account)returnAccounts.get(0));
	      }
	    }
	  }
	  

	  public int getAccounts(Accounts accounts, Account searchAcct)
	  {
	    return 2;
	  }
	  

	  public int getAccountsById(Accounts accounts, int directoryId)
	  {
	    return 2;
	  }
	  

	  public int getAccountsByBusinessEmployee(Accounts accounts, SecureUser user)
	  {
	    return 2;
	  }
	  

	  public int initialize(String url)
	  {
	    return 2;
	  }
	  

	  public int getAccounts(Accounts accounts)
	  {
	    return 2;
	  }
	  

	  public int getAccount(Account account)
	  {
	    return 2;
	  }
	  

	  public int addAccount(Account account)
	  {
	    return 2;
	  }
	  

	  public int modifyAccount(Account account)
	  {
	    return 2;
	  }
	  

	  public int deleteAccount(Account account)
	  {
	    return 2;
	  }
	  



	  public void setInitURL(String url) {}
	  


	  public int signOn(String userID, String password)
	  {
	    return 2;
	  }
	  

	  public int changePIN(String currentPin, String newPin)
	  {
	    return 2;
	  }
	  

	  public boolean signOn(SecureUser sUser, String userID, String password)
	    throws CSILException
	  {
	    return false;
	  }
	  
	  private User getUserDetails(SecureUser sUser, User user)
	    throws ProfileException
	  {
	    User retUsr = customerAdapter.getUserById(sUser, user, new HashMap());
	    try {
	      retUsr.setEntitlementGroupMember(entitlementsAdminBO.getEntitlementGroupMember(retUsr.getSecureUser()));
	    } catch (EntitlementsException e) {
	      logger.debug("Unable to get entitlement group member", e);
	    }
	    

	    try
	    {
	      EntitlementGroupMember member = entitlementsAdminBO.getMember(retUsr.getEntitlementGroupMember());
	      EntitlementGroup entGroup = entitlementsAdminBO.getEntitlementGroup(member.getEntitlementGroupId());
	      user.setEntitlementGroupId(entGroup.getGroupId());
	    } catch (EntitlementsException ex) {
	      throw new ProfileException(mapError(ex), ex);
	    }
	    
	    EntitlementGroupMember member;
	    return retUsr;
	  }	
}
