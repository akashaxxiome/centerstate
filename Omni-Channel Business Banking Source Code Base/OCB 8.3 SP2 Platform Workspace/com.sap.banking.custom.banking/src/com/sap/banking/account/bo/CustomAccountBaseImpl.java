package com.sap.banking.account.bo;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.AccountSummaries;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.banking.Transaction;
import com.ffusion.beans.banking.Transactions;
import com.ffusion.beans.common.Currency;
import com.ffusion.beans.util.StringUtil;
import com.ffusion.csil.CSILException;
import com.ffusion.csil.beans.entitlements.Entitlement;
import com.ffusion.services.banking.interfaces.BankingService;
import com.ffusion.services.exceptions.BankingException;
import com.ffusion.util.FilteredList;
import com.ffusion.util.beans.PagingContext;
import com.sap.banking.account.beans.AccountBalance;
import com.sap.banking.account.bo.interfaces.BankingAccounts;
import com.sap.banking.account.exception.AccountException;
import com.sap.banking.accountconfig.beans.AccountBalanceSearchCriteria;
import com.sap.banking.accountconfig.beans.AccountHistorySearchCriteria;
import com.sap.banking.accountconfig.beans.TransactionHistorySearchCriteria;
import com.sap.banking.accountconfig.bo.interfaces.AccountValidationConfig;
import com.sap.banking.accountconfig.exception.AccountConfigException;
import com.sap.banking.common.annotations.MemoryPagingSupport;
import com.sap.banking.common.beans.ValidationResult;
import com.sap.banking.common.beans.paging.PagedResult;
import com.sap.banking.common.bo.BaseImpl;
import com.sap.banking.common.bo.interfaces.CommonConfig;
import com.sap.banking.entitlements.bo.interfaces.Entitlements;
import com.sap.banking.entitlements.bo.interfaces.EntitlementsAdmin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class CustomAccountBaseImpl
extends BaseImpl
{
@Autowired
@Qualifier("bankingServiceRef")
protected BankingService bankingService;
@Autowired
@Qualifier("realtimeBankingServiceRef")
private BankingService realtimeBankingService;
@Autowired
@Qualifier("accountSummaryRef")
protected com.sap.banking.account.bo.interfaces.AccountSummary accountSummaryBO;
@Autowired
@Qualifier("bankingAccountsRef")
protected BankingAccounts bankingAccountsBO;
@Autowired
private Entitlements entitlementsBO;
@Autowired
private EntitlementsAdmin entitlementsAdminBo;
@Autowired
private AccountValidationConfig accountValidConfigBO;
@Autowired
private CommonConfig commonConfigBO;
private static final String SECURE_USER_KEY = "sUser";

public PagedResult<Transaction> getTransactions(SecureUser secureUser, AccountHistorySearchCriteria criteria, Map<String, Object> extra)
  throws AccountException
{
  String thisMethod = "AccountImpl.getTransactions";
  List<Transaction> transactions = null;
  String canInquire = "true";
  

  if (secureUser == null) {
    throw new AccountException(103);
  }
  
  if (criteria == null) {
    try
    {
      criteria = this.bankingAccountsBO.getDefaultAccountHistorySearchCriteria(secureUser, "defaultAccountHistorySearchCriteria", extra);

    }
    catch (AccountException accountException)
    {
      this.logger.error(accountException.getLocalizedMessage(), accountException);
      
      throw accountException;
    }
  }
  
  PagingContext pagingContext = criteria.getPagingContext();
  Account account = criteria.getAccount();
  String direction = pagingContext.getDirection();
  

  String operationName = null;
  

  if (secureUser.isCorporateUser()) {
    operationName = "getBusinessTransactions";
  } else {
    operationName = "getUserTransactions";
  }
  

  ValidationResult validationResult = validateAccountHistorySearchCriteria(secureUser, criteria, operationName);
  if ((validationResult != null) && (validationResult.isValidationFailed())) {
    AccountException accountException = new AccountException(-2);
    accountException.setValidationResult(validationResult);
    throw accountException;
  }
  

  extra = checkEmptyExtraMap(extra);
  
  debug(secureUser, thisMethod);
  

  extra.put("SERVICE", this.bankingService);
  

  if (!"Business".equalsIgnoreCase(secureUser.getAppType())) {
    extra.put("usePagination", "true");
  }
  
  try
  {
    extra.put("SecureUser", secureUser);
    

    boolean isBaSBackendEnable = this.commonConfigBO.isBasBackendEnabled();
    
    if ((criteria instanceof TransactionHistorySearchCriteria)) {
      if ((isBaSBackendEnable) && (secureUser.isConsumerUser()))
      {
        if ((criteria.getAccount() != null) && (criteria.getAccount().getID() != null) && (criteria.getAccount().getID().trim().length() > 0))
        {
          Account acct = this.bankingAccountsBO.getAccount(secureUser, criteria.getAccount(), extra);
          if (acct != null) {
            criteria.setAccount(acct);
          }
        }
        transactions = this.realtimeBankingService.getAccountTransactions(secureUser, criteria, extra);
      } else {
        transactions = this.bankingService.getTransactions(secureUser, (TransactionHistorySearchCriteria)criteria, extra);
      }
      
    }
    else if ((isBaSBackendEnable) && (secureUser.isConsumerUser()))
    {
      if ((criteria.getAccount() != null) && (criteria.getAccount().getID() != null) && (criteria.getAccount().getID().trim().length() > 0))
      {
        Account acct = this.bankingAccountsBO.getAccount(secureUser, criteria.getAccount(), extra);
        if (acct != null) {
          criteria.setAccount(acct);
        }
      }
      transactions = this.realtimeBankingService.getAccountTransactions(secureUser, criteria, extra);
    }
    else {
      transactions = this.bankingService.getSpecificPageOfTransactions(criteria.getAccount(), criteria.getPagingContext(), (HashMap)extra);
    }
    


    if ((transactions != null) && (transactions.size() > 0)) {
      Entitlement ENT_MESSAGES = new Entitlement("Messages", null, null);
      if (!this.entitlementsBO.checkEntitlement(this.entitlementsAdminBo.getEntitlementGroupMember(secureUser), ENT_MESSAGES))
      {
        canInquire = "false";
      }
      
      int tmpIndex = 1;
      for (Object object : transactions)
      {
        Transaction transaction = (Transaction)object;
        transaction.put("accIndex", String.valueOf(tmpIndex));
        tmpIndex += 1;
        transaction.put("canInquire", canInquire);
        

        if ((isBaSBackendEnable) && (secureUser.isConsumerUser())) {
          transaction.setDescription(transaction.getMemo());
        }
      }
      
      ((Transactions)transactions).setDateFormat(criteria.getDateFormat());
    }
    
    extra.remove("SecureUser");
  } catch (BankingException be) {
    this.logger.error(be.getLocalizedMessage(), be);
    throw new AccountException(be.getErrorCode(), be);
  } catch (CSILException csile) {
    this.logger.error(csile.getLocalizedMessage(), csile);
    throw new AccountException(22000, csile.code, csile);
  } catch (Exception exception) {
    this.logger.error(exception.getLocalizedMessage(), exception);
    throw new AccountException(63536, -3, exception);
  }
  PagedResult<Transaction> pagedTransaction = new PagedResult();
  pagedTransaction.addAll(transactions);
  return pagedTransaction;
}










@MemoryPagingSupport
protected PagedResult<AccountBalance> getAccountBalances(SecureUser secureUser, AccountBalanceSearchCriteria criteria, Map<String, Object> extra)
  throws AccountException
{
  String thisMethod = "DepositAccountImpl.getDepositAccountBalance";
  List<AccountBalance> accountsBalance = new ArrayList();
  

  if (secureUser == null) {
    throw new AccountException(103);
  }
  if (criteria == null) {
    try {
      criteria = this.bankingAccountsBO.getDefaultAccountBalanceSearchCriteria(secureUser, "defaultAccountBalanceSearchCriteria", extra);
    } catch (AccountException accountException) {
      this.logger.error(accountException.getLocalizedMessage(), accountException);
      throw accountException;
    }
  }
  if ((criteria.getCurrency() == null) || (criteria.getCurrency().isEmpty())) {
    criteria.setCurrency(secureUser.getBaseCurrency());
  }
  if ((criteria.getDataClassification() == null) || (criteria.getDataClassification().isEmpty())) {
    criteria.setDataClassification("P");
  }
  
  validateAccountBalanceSearchCriteriaCriteria(secureUser, criteria);
  

  extra = checkEmptyExtraMap(extra);
  debug(secureUser, thisMethod);
  try
  {
    Accounts accounts = (Accounts)this.bankingAccountsBO.getBankingAccounts(secureUser, criteria, extra);
    if (accounts.size() != 0) {
      AccountSummaries accountSummaries = null;
      Iterator<Account> acctIterator; if ((secureUser.isCorporateUser()) || ((secureUser.isConsumerUser()) && (criteria.isConsolidated()))) {
        if (secureUser.isConsumerUser()) {
          AccountSummaries nonCoreSummaries = new AccountSummaries();
          acctIterator = accounts.iterator();
          Accounts coreAccts = new Accounts();
          Accounts nonCoreAccts = new Accounts();
          while (acctIterator.hasNext()) {
            Account acct = (Account)acctIterator.next();
            if ("0".equals(acct.getCoreAccount())) {
              nonCoreAccts.add(acct);
            } else {
              coreAccts.add(acct);
            }
          }
          if ((nonCoreAccts != null) && (nonCoreAccts.size() > 0)) {
            nonCoreSummaries = this.accountSummaryBO.getCoreNonCoreAccountSummaries(secureUser, nonCoreAccts, null, null, criteria, (HashMap)extra);
          }
          if ((coreAccts != null) && (coreAccts.size() > 0)) {
            accountSummaries = this.accountSummaryBO.getCoreNonCoreAccountSummaries(secureUser, coreAccts, null, null, criteria, extra);
          }
          if (accountSummaries == null) {
            accountSummaries = new AccountSummaries();
          }
          accountSummaries.addAll(nonCoreSummaries);
        }
        else {
          accountSummaries = (AccountSummaries)this.accountSummaryBO.getAccountBalanceSummaries(secureUser, accounts, criteria, extra);
        }
      } else if (secureUser.isConsumerUser())
      {
        Accounts accts = new Accounts();
        accts.add(criteria.getAccount());
        accountSummaries = this.accountSummaryBO.getCoreNonCoreAccountSummaries(secureUser, accts, null, null, criteria, (HashMap)extra);
      }
      if ((criteria.getCurrency() != null) && (!criteria.getCurrency().equals(accountSummaries.getDisplayCurrency()))) {
        Accounts accs = new Accounts();
        for (Object object : accounts) {
          Account account = (Account)object;
          String number = account.getNumber();
          String bankId = account.getBankID();
          
          if (accs.size() == 0) {
            accs.add(account);
          } else {
            boolean isDuplicate = false;
            for (int i = 0; i < accs.size(); i++) {
              Account accnt = (Account)accs.get(i);
              String numberStr = accnt.getNumber();
              String bankIdStr = accnt.getBankID();
              if ((number.equals(numberStr)) && (bankId.equals(bankIdStr))) {
                isDuplicate = true;
                break;
              }
            }
            if (!isDuplicate) {
              accs.add(account);
            }
          }
        }
        accountsBalance = processAccountBalance(secureUser, criteria, accountSummaries, accs, extra);
      }
    }
  } catch (AccountException ae) {
    this.logger.error(ae.getLocalizedMessage(), ae);
    throw ae;
  } catch (Exception exception) {
    this.logger.error(exception.getLocalizedMessage(), exception);
    throw new AccountException(63536, -3, exception);
  }
  
  FilteredList accountsBalances = new FilteredList(accountsBalance);
  if (criteria.getSortedBy() != null) {
    accountsBalances.setSortedBy(criteria.getSortedBy());
  }
  
  PagedResult<AccountBalance> pagedAccountBalance = new PagedResult();
  pagedAccountBalance.setPagedList(accountsBalances);
  pagedAccountBalance.setSortColumnName(criteria.getSortColumnName());
  pagedAccountBalance.setIsAscending(criteria.getIsAscending());
  return pagedAccountBalance;
}










protected abstract List<AccountBalance> processAccountBalance(SecureUser paramSecureUser, AccountBalanceSearchCriteria paramAccountBalanceSearchCriteria, AccountSummaries paramAccountSummaries, Accounts paramAccounts, Map<String, Object> paramMap)
  throws AccountException;










protected ValidationResult validateAccountHistorySearchCriteria(SecureUser sUser, AccountHistorySearchCriteria accountHistorySearchCriteria, String operationName)
  throws AccountException
{
  String thisMethod = "AccountBaseImpl.validateAccountHistorySearchCriteria";
  ValidationResult validationResult = new ValidationResult();
  try
  {
    validationResult = this.accountValidConfigBO.validateAccountHistorySearchCriteria(sUser, accountHistorySearchCriteria, operationName, "BUSINESS_VALIDATION");
  } catch (AccountConfigException acException) {
    this.logger.error(thisMethod, acException);
    throw new AccountException(-2, acException);
  }
  return validationResult;
}







protected ValidationResult validateAccountBalanceSearchCriteriaCriteria(SecureUser sUser, AccountBalanceSearchCriteria accountBalanceSearchCriteria)
  throws AccountException
{
  String thisMethod = "AccountBaseImpl.validateAccountBalanceSearchCriteriaCriteria";
  ValidationResult validationResult = new ValidationResult();
  try
  {
    validationResult = this.accountValidConfigBO.validateAccountBalanceSearchCriteriaCriteria(sUser, accountBalanceSearchCriteria, "BUSINESS_VALIDATION");
  } catch (AccountConfigException acException) {
    this.logger.error(thisMethod, acException);
    throw new AccountException(-2, acException);
  }
  
  return validationResult;
}








protected String applyFXRate(String stringValue, AccountSummaries accountSummaries)
{
  String formattedValue = "0.00";
  stringValue = (stringValue == null) || (stringValue.isEmpty()) ? "0.00" : stringValue;
  com.ffusion.beans.accounts.AccountSummary accountSummary = (com.ffusion.beans.accounts.AccountSummary)accountSummaries.get(0);
  if (stringValue.equalsIgnoreCase("0.00")) {
    formattedValue = stringValue;
  } else {
    Float floatValue = new Float(Float.parseFloat(stringValue.replaceAll(",", "")) * accountSummary.getDisplayFXRate().floatValue());
    formattedValue = floatValue.toString();
    if ((formattedValue.indexOf(".") > 3) && (formattedValue.indexOf(".") <= 6)) {
      formattedValue = formattedValue.substring(0, formattedValue.indexOf(".") - 3) + "," + formattedValue.substring(formattedValue.indexOf(".") - 3);
    } else if ((formattedValue.indexOf(".") > 6) && (formattedValue.indexOf(".") <= 9)) {
      formattedValue = formattedValue.substring(0, formattedValue.indexOf(".") - 6) + "," + formattedValue.substring(formattedValue.indexOf(".") - 6, formattedValue.indexOf(".") - 3) + "," + formattedValue.substring(formattedValue.indexOf(".") - 3);
    }
  }
  return formattedValue;
}










public Transaction getTransactionById(SecureUser secureUser, Account account, String transId, Map<String, Object> extra)
  throws AccountException
{
  String thisMethod = "AccountImpl.getTransactions";
  Transaction transaction = null;
  try {
    if (secureUser == null) {
      throw new AccountException(103);
    }
    if ((transId == null) || (transId.isEmpty())) {
      throw new AccountException(18005);
    }
    if (account == null) {
      account = new Account();
      account.setLocale(secureUser.getLocale());
      account.setCurrencyCode(secureUser.getBaseCurrency());
    }
    
    HashMap<String, Object> extraParams = checkEmptyExtraMap(extra);
    if (!extraParams.containsKey("sUser")) {
      extraParams.put("sUser", secureUser);
    }
    
    debug(secureUser, thisMethod);
    transaction = this.bankingService.getTransactionById(account, transId, extraParams);
  } catch (AccountException ae) {
    this.logger.error(ae.getLocalizedMessage(), ae);
    throw ae;
  } catch (Exception e) {
    this.logger.error(e.getLocalizedMessage(), e);
    throw new AccountException(-3, e);
  }
  return transaction;
}





protected String amountFormatter(String valueToFormat)
  throws AccountException
{
  try
  {
    if ((valueToFormat != null) && (!valueToFormat.isEmpty())) {
      if (valueToFormat.indexOf(",") == -1) {
        if ((valueToFormat.indexOf(".") > 3) && (valueToFormat.indexOf(".") <= 6)) {
          valueToFormat = valueToFormat.substring(0, valueToFormat.indexOf(".") - 3) + "," + valueToFormat.substring(valueToFormat.indexOf(".") - 3);
        } else if ((valueToFormat.indexOf(".") > 6) && (valueToFormat.indexOf(".") <= 9)) {
          valueToFormat = valueToFormat.substring(0, valueToFormat.indexOf(".") - 6) + "," + valueToFormat.substring(valueToFormat.indexOf(".") - 6, valueToFormat.indexOf(".") - 3) + "," + valueToFormat.substring(valueToFormat.indexOf(".") - 3);
        }
      }
    } else {
      valueToFormat = "0.00";
    }
  } catch (Exception e) {
    this.logger.error(e.getLocalizedMessage(), e);
    throw new AccountException(-3, e);
  }
  return valueToFormat;
}







protected Transactions applyFilter(Transactions transactions, AccountHistorySearchCriteria searchCriteria)
{
  if ((searchCriteria instanceof TransactionHistorySearchCriteria)) {
    TransactionHistorySearchCriteria criteria = (TransactionHistorySearchCriteria)searchCriteria;
    Iterator itr = transactions.iterator();
    while (itr.hasNext()) {
      Transaction trxn = (Transaction)itr.next();
      String refStart = criteria.getReferenceStart() != null ? criteria.getReferenceStart().trim() : null;
      String refEnd = criteria.getReferenceEnd() != null ? criteria.getReferenceEnd().trim() : null;
      String amountMin = criteria.getAmountMinimum() != null ? criteria.getAmountMinimum().trim() : null;
      String amountMax = criteria.getAmountMaximum() != null ? criteria.getAmountMaximum().trim() : null;
      String description = criteria.getDescription() != null ? criteria.getDescription().trim() : null;
      String transType = criteria.getTransactionType();
      String compareDescription = trxn.getDescription() != null ? trxn.getDescription().trim() : null;
      double compareRefNumNumeric = 0.0D;
      double compareAmountNumeric = 0.0D;
      if (StringUtil.isNumeric(trxn.getReferenceNumber())) {
        compareRefNumNumeric = Double.parseDouble(trxn.getReferenceNumber());
      }
      if (trxn.getAmountValue() != null) {
        compareAmountNumeric = trxn.getAmountValue().doubleValue();
      }
      if ((compareRefNumNumeric != 0.0D) && (StringUtil.isNotEmpty(refStart)) && 
        (StringUtil.isNumeric(refStart))) {
        double refStartNumeric = Double.parseDouble(refStart);
        if (compareRefNumNumeric < refStartNumeric) {
          itr.remove();
          continue;
        }
      }
      
      if ((compareRefNumNumeric != 0.0D) && (StringUtil.isNotEmpty(refEnd)) && 
        (StringUtil.isNumeric(refEnd))) {
        double refEndNumeric = Double.parseDouble(refEnd);
        if (compareRefNumNumeric > refEndNumeric) {
          itr.remove();
          continue;
        }
      }
      
      if (compareAmountNumeric < 0.0D) {
        compareAmountNumeric = Math.abs(compareAmountNumeric);
      }
      if ((compareAmountNumeric != 0.0D) && (StringUtil.isNotEmpty(amountMin)) && 
        (StringUtil.isNumeric(amountMin))) {
        double amountMinNumeric = Double.parseDouble(amountMin);
        if (compareAmountNumeric < amountMinNumeric) {
          itr.remove();
          continue;
        }
      }
      
      if ((compareAmountNumeric != 0.0D) && (StringUtil.isNotEmpty(amountMax)) && 
        (StringUtil.isNumeric(amountMax))) {
        double amountMaxNumeric = Double.parseDouble(amountMax);
        if (compareAmountNumeric > amountMaxNumeric) {
          itr.remove();
          continue;
        }
      }
      

      if ((StringUtil.isNotEmpty(description)) && (
        (StringUtil.isEmpty(compareDescription)) || (!compareDescription.contains(description)))) {
        itr.remove();



      }
      else if ((transType != null) && (!transType.isEmpty()) && (!transType.equals("AllTransactionTypes"))) {
        transType = transType.trim();
        String[] typesSplitted = transType.split(",");
        boolean eligible = false;
        for (int i = 0; i < typesSplitted.length; i++) {
          String type = typesSplitted[i].trim();
          int trxnType = 0;
          if (StringUtil.isNumeric(type)) {
            trxnType = Integer.parseInt(type);
          }
          if (trxnType == trxn.getTypeValue()) {
            eligible = true;
            break;
          }
        }
        if (!eligible) {
          itr.remove();
        }
      }
    }
  }
  

  return transactions;
}
}

