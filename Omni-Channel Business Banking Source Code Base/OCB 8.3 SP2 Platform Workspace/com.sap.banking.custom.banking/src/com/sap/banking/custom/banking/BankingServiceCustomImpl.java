package com.sap.banking.custom.banking;

import com.ffusion.beans.FundsTransactions;
import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.AccountHistories;
import com.ffusion.beans.accounts.AccountSummaries;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.accounts.ExtendedAccountSummaries;
import com.ffusion.beans.accounts.FixedDepositInstrument;
import com.ffusion.beans.accounts.FixedDepositInstruments;
import com.ffusion.beans.banking.Transaction;
import com.ffusion.beans.banking.Transactions;
import com.ffusion.beans.banking.Transfer;
import com.ffusion.beans.banking.common.TransactionTypeInfo;
import com.ffusion.beans.business.Businesses;
import com.ffusion.beans.exttransfers.ExtTransferAccount;
import com.ffusion.beans.exttransfers.ExtTransferAccounts;
import com.ffusion.beans.reporting.ReportCriteria;
import com.ffusion.beans.user.User;
import com.ffusion.csil.CSILException;
import com.ffusion.dataconsolidator.adapter.interfaces.DCAdapter;
import com.ffusion.dataconsolidator.adapter.interfaces.DCUtil;
import com.ffusion.efs.adapters.profile.exception.ProfileException;
import com.ffusion.efs.adapters.profile.interfaces.AccountAdapter;
import com.ffusion.efs.adapters.profile.interfaces.CustomerAdapter;
import com.ffusion.reporting.interfaces.IReportResult;
import com.ffusion.services.account.profile.interfaces.AccountService;
import com.ffusion.services.aggregation.interfaces.AccountAggregationService;
import com.ffusion.services.banking.BaseAccountServiceImpl;
import com.ffusion.services.banking.interfaces.BankingService;
import com.ffusion.services.blockedaccts.interfaces.BlockedAccountsService;
import com.ffusion.services.dataconsolidator.DCServiceUtil;
import com.ffusion.services.exceptions.BankingException;
import com.ffusion.util.FFIUtilException;
import com.ffusion.util.TransactionTypeCache;
import com.ffusion.util.beans.PagingContext;
import com.ffusion.util.entitlements.util.EntitlementsUtil;
import com.ffusion.util.logging.DebugLog;
import com.sap.banking.account.exception.AccountException;
import com.sap.banking.accountconfig.beans.AccountHistorySearchCriteria;
import com.sap.banking.accountconfig.beans.TransactionHistorySearchCriteria;
import com.sap.banking.common.annotations.Cacheable;
import com.sap.banking.exttransfer.bo.interfaces.ExternalTransfer;
import com.sap.banking.exttransfer.exception.ExternalTransferException;
import com.sap.banking.reporting.bo.interfaces.Reporting;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;


public class BankingServiceCustomImpl  extends BaseAccountServiceImpl implements BankingService {

	  @Autowired
	  @Qualifier("externalTransferAccountRef")
	  private ExternalTransfer externalTransferAccountBO;
	  @Autowired
	  @Qualifier("bankingServiceRef")
	  private BankingService bankingService;
	  @Autowired
	  @Qualifier("blockedAccountsServiceRef")
	  private BlockedAccountsService blockedAccountsService;
	  @Autowired
	  @Qualifier("profileAccountsServiceRef")
	  private AccountService accountService;
	  @Autowired
	  @Qualifier("accountAggregationServiceRef")
	  private AccountAggregationService aggregationService;
	  private Logger logger = LoggerFactory.getLogger(getClass());
	  
	  private static final long serialVersionUID = 1L;
	  
	  static final String ERR_CANNOT_LOAD_CONFIG_XML = " Unable to load Banking Service configuration file bankingservice.xml";
	  
	  static final String ERR_CANNOT_PARSE_CONFIG_XML = " An error occurred while parsing the Banking Service configuration file bankingservice.xml";
	  
	  static final String ERR_CANNOT_LOAD_REALTIME_CLASS = " Unable to load Banking Service real-time class ";
	  
	  static final String ERR_CANNOT_LOAD_STORED_CLASS = " Unable to load Banking Service stored class ";
	  
	  static final String ERR_REALTIME_CLASS_INVALID_TYPE = " The following Banking Service realtime class could not be loaded as it does not implement Banking8: ";
	  static final String ERR_STORED_CLASS_INVALID_TYPE = " The following Banking Service stored class could not be loaded as it does not implement Banking8: ";
	  static final String ERR_CANNOT_INSTANTIATE_REALTIME_CLASS = " An error occurred while instantiating the Banking Service realtime class ";
	  static final String ERR_CANNOT_INSTANTIATE_STORED_CLASS = " An error occurred while instantiating the Banking Service stored class ";
	  static final String ERR_CANNOT_DETERMINE_USER_TYPE = " An error occurred while trying to determine whether user is of type consumer or corporate ";
	  private static final String SECURE_USER_KEY = "sUser";
	  private static final String STORED_BANKING_SERVICE = "STORED";
	  private static final String REALTIME_BANKING_SERVICE = "REALTIME";
	  protected BankingService _realtimeService;
	  protected BankingService _storedService;
	  protected BankingService _corporateService;
	  protected BankingService _consumerService;
	  private boolean _isConsumerServiceRealtime = true;
	  
	  private String _settings;
	  
	  public String url = null;
	  
	  public static final String CONFIG_FILEDIR = "banking";
	  private String consumerServiceType;
	  private String corporateServiceType;
	  @Value("${isAccountNickNameFromBackend}")
	  private String isAccountNickNameFromBackend;
	  @Autowired
	  private AccountAdapter accountAdapter;
	  
	  public String getConsumerServiceType()
	  {
	    return consumerServiceType;
	  }
	  
	  public void setConsumerServiceType(String consumerServiceType) {
	    this.consumerServiceType = consumerServiceType;
	  }
	  
	  public String getCorporateServiceType() {
	    return corporateServiceType;
	  }
	  
	  public void setCorporateServiceType(String corporateServiceType) {
	    this.corporateServiceType = corporateServiceType;
	  }
	  

	  public String getUrl()
	  {
	    return url;
	  }
	  
	  public void setUrl(String url) {
	    this.url = url;
	  }
	  


	  @Autowired
	  private DCAdapter dcAdapter;
	  

	  @Autowired
	  @Qualifier("dcUtilRef")
	  private DCUtil dcUtil;
	  

	  @Autowired
	  private DCServiceUtil dcServiceUtil;
	  
	  @Autowired
	  private CustomerAdapter customerAdapter;
	  
	  @Autowired
	  private Reporting reportingBO;
	  
	  public Reporting getReportingBO()
	  {
	    return reportingBO;
	  }
	  
	  public void setReportingBO(Reporting reportingBO) {
	    this.reportingBO = reportingBO;
	  }
	  
	  @PostConstruct
	  public int init()
	  {
	    return initialize(url);
	  }
	  
	  private boolean initializeStored = false;
	  private boolean initializeRealTime = false;
	  

	  public int initialize(String URL)
	  {
	    try
	    {
	      _corporateService = _storedService;
	      _consumerService = _realtimeService;
	      _isConsumerServiceRealtime = true;
	      
	      if ((consumerServiceType != null) && (consumerServiceType.equals("STORED"))) {
	        _consumerService = _storedService;
	        _isConsumerServiceRealtime = false;
	      }
	      
	      if ((corporateServiceType != null) && (corporateServiceType.equals("REALTIME"))) {
	        _corporateService = _realtimeService;
	      }
	      
	      DebugLog.log(Level.INFO, "Banking Service initialized");
	      return 0;
	    } catch (Exception e) {
	      if (!(e instanceof BankingException)) {
	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw);
	        e.printStackTrace(pw);
	        String errorMsg = "An error occurred while initializing the Banking Service: " + sw.toString();
	        DebugLog.log(Level.SEVERE, errorMsg);
	      } }
	    return 1;
	  }
	  
	  private boolean isCorporate(Account account, HashMap extra) throws BankingException
	  {
	    boolean ret = true;
	    
	    SecureUser user = (SecureUser)extra.get("sUser");
	    
	    if (user != null) {
	      if (user.getBusinessID() == 0) {
	        ret = false;
	      }
	    } else if (account != null) {
	      try {
	        Businesses busList = accountAdapter.getBusinessesForAccount(account, extra);
	        if (busList.size() == 0) {
	          ret = false;
	        }
	      } catch (ProfileException pe) {
	        throw new BankingException(474, " An error occurred while trying to determine whether user is of type consumer or corporate ", pe);
	      }
	    } else {
	      throw new BankingException(474);
	    }
	    
	    return ret;
	  }
	  
	  public int addAccount(Account account)
	  {
	    try {
	      if (isCorporate(account, new HashMap())) {
	        if ((_corporateService instanceof AccountService)) {
	          return ((AccountService)_corporateService).addAccount(account);
	        }
	        return 2;
	      }
	      
	      return 2;
	    }
	    catch (BankingException be) {
	      return be.getErrorCode();
	    }
	  }
	  
	  public int modifyAccount(Account account)
	  {
	    try {
	      if (isCorporate(account, new HashMap())) {
	        if ((_corporateService instanceof AccountService)) {
	          return ((AccountService)_corporateService).modifyAccount(account);
	        }
	        return 2;
	      }
	      
	      return 2;
	    }
	    catch (BankingException be) {
	      return be.getErrorCode();
	    }
	  }
	  
	  public int deleteAccount(Account account)
	  {
	    try {
	      if (isCorporate(account, new HashMap())) {
	        if ((_corporateService instanceof AccountService)) {
	          return ((AccountService)_corporateService).deleteAccount(account);
	        }
	        return 2;
	      }
	      
	      return 2;
	    }
	    catch (BankingException be) {
	      return be.getErrorCode();
	    }
	  }
	  
	  public int getAccount(Account account)
	  {
	    try {
	      if (isCorporate(account, new HashMap())) {
	        if ((_corporateService instanceof AccountService)) {
	          return ((AccountService)_corporateService).getAccount(account);
	        }
	        return 2;
	      }
	      
	      return 2;
	    }
	    catch (BankingException be) {
	      StringWriter sw = new StringWriter();
	      PrintWriter pw = new PrintWriter(sw);
	      be.printStackTrace(pw);
	      String errorMsg = "An error occurred while retreiving accounts from the Banking Service: " + sw.toString();
	      DebugLog.log(Level.SEVERE, errorMsg);
	      return be.getErrorCode();
	    }
	  }
	  
	  public HashMap getTransactionTypes(int application, HashMap extra) throws BankingException
	  {
	    try {
	      return TransactionTypeCache.getTransactionTypes(application, extra);
	    } catch (FFIUtilException e) {
	      BankingException e2 = new BankingException(e.getCode());
	      throw e2;
	    }
	  }
	  

	  public TransactionTypeInfo getTransactionTypeInfo(int transTypeId, HashMap extra)
	    throws BankingException
	  {
	    HashMap allTypes = getTransactionTypes(0, extra);
	    TransactionTypeInfo info = (TransactionTypeInfo)allTypes.get(new Integer(transTypeId));
	    return info;
	  }
	  
	  public FundsTransactions getPagedFundsTransactions(SecureUser sUser, PagingContext context, HashMap extra)
	    throws BankingException
	  {
	    return _realtimeService.getPagedFundsTransactions(sUser, context, extra);
	  }
	  

	  public Transactions getRecentTransactions(Account account, PagingContext context, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getRecentTransactions(account, context, extra);
	    }
	    return _consumerService.getRecentTransactions(account, context, extra);
	  }
	  


	  public int getNumTransactions(Account account, Calendar start, Calendar end, Properties searchCriteria, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getNumTransactions(account, start, end, searchCriteria, extra);
	    }
	    return _consumerService.getNumTransactions(account, start, end, searchCriteria, extra);
	  }
	  


	  public Date getEffectiveDate(SecureUser sUser, Transfer transfer, HashMap extra)
	    throws CSILException
	  {
	    return _realtimeService.getEffectiveDate(sUser, transfer, extra);
	  }
	  







	  public HashMap getTransactionTypes(Locale locale, HashMap extra)
	    throws BankingException
	  {
	    if (TransactionTypeCache.getTransactionTypeCache(locale) == null) {
	      throw new BankingException(5);
	    }
	    
	    return TransactionTypeCache.getTransactionTypeCache(locale);
	  }
	  

	  public ArrayList getTransactionTypeDescriptions(Locale locale, HashMap extra)
	    throws BankingException
	  {
	    HashMap allInfos = getTransactionTypes(0, null);
	    if ((allInfos == null) || (allInfos.isEmpty())) {
	      return new ArrayList(0);
	    }
	    ArrayList descriptionList = new ArrayList(allInfos.size());
	    Iterator iterator = allInfos.values().iterator();
	    

	    if (locale == null) {
	      while (iterator.hasNext())
	      {
	        TransactionTypeInfo info = (TransactionTypeInfo)iterator.next();
	        descriptionList.add(info.getDescription());
	      }
	    }
	    while (iterator.hasNext())
	    {
	      TransactionTypeInfo info = (TransactionTypeInfo)iterator.next();
	      descriptionList.add(info.getDescription(locale));
	    }
	    
	    return descriptionList;
	  }
	  
































	  public HashMap getTransactionTypes()
	    throws BankingException
	  {
	    return getTransactionTypes(null, null);
	  }
	  
	  public ArrayList getTransactionTypeDescriptions()
	    throws BankingException
	  {
	    return getTransactionTypeDescriptions(null, null);
	  }
	  

	  public FixedDepositInstruments getFixedDepositInstruments(Account account, Calendar start, Calendar end, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getFixedDepositInstruments(account, start, end, extra);
	    }
	    return _consumerService.getFixedDepositInstruments(account, start, end, extra);
	  }
	  


	  public void updateFixedDepositInstrument(FixedDepositInstrument inst, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(null, extra)) {
	      _corporateService.updateFixedDepositInstrument(inst, extra);
	    } else {
	      _consumerService.updateFixedDepositInstrument(inst, extra);
	    }
	  }
	  

	  public Transactions getRecentTransactions(Account account, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getRecentTransactions(account, extra);
	    }
	    return _consumerService.getRecentTransactions(account, extra);
	  }
	  


	  public Transactions getPagedTransactions(Account account, PagingContext pagingContext, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getPagedTransactions(account, pagingContext, extra);
	    }
	    PagingContext newPagingContext = translatePagingContextForDC(account, pagingContext, extra);
	    
	    return _consumerService.getPagedTransactions(account, newPagingContext, extra);
	  }
	  
	  private PagingContext translatePagingContextForDC(Account account, PagingContext pagingContext, HashMap extra)
	    throws BankingException
	  {
	    PagingContext newPagingContext = null;
	    
	    if (!_isConsumerServiceRealtime) {
	      newPagingContext = pagingContext != null ? (PagingContext)pagingContext.clone() : null;
	      
	      ReportCriteria criteria = (newPagingContext == null) || (newPagingContext.getMap() == null) ? null : (ReportCriteria)newPagingContext.getMap().get("ReportCriteria");
	      Properties searchCriteria = criteria == null ? null : criteria.getSearchCriteria();
	      

	      String rangeType = searchCriteria.getProperty("Date_Range_Type");
	      if ("Relative".equalsIgnoreCase(rangeType)) {
	        HashMap startDates = new HashMap();
	        HashMap endDates = new HashMap();
	        SecureUser sUser = (SecureUser)extra.get("SecureUser");
	        try {
	          reportingBO.calculateDateRange(sUser, null, criteria, startDates, endDates, extra);
	        } catch (CSILException ex) {
	          String errorMsg = "Unable to calculate the date range.";
	          DebugLog.log(errorMsg);
	          if (ex.getCode() == 64527) {
	            throw new BankingException(ex.getServiceError());
	          }
	          if (ex.getCode() == 63536) {
	            throw new BankingException(ex.getServiceError());
	          }
	          
	          throw new BankingException(ex.getCode());
	        }
	      }
	    }
	    else {
	      newPagingContext = pagingContext;
	    }
	    
	    return newPagingContext;
	  }
	  
	  public Transactions getNextTransactions(Account account, PagingContext pagingContext, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getNextTransactions(account, pagingContext, extra);
	    }
	    return _consumerService.getNextTransactions(account, pagingContext, extra);
	  }
	  


	  public Transactions getPreviousTransactions(Account account, PagingContext pagingContext, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getPreviousTransactions(account, pagingContext, extra);
	    }
	    return _consumerService.getPreviousTransactions(account, pagingContext, extra);
	  }
	  


	  public Transactions getFDInstrumentTransactions(FixedDepositInstrument inst, Calendar start, Calendar end, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(null, extra)) {
	      return _corporateService.getFDInstrumentTransactions(inst, start, end, extra);
	    }
	    return _consumerService.getFDInstrumentTransactions(inst, start, end, extra);
	  }
	  


	  public int getNumTransactions(Account account, Calendar start, Calendar end, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getNumTransactions(account, start, end, extra);
	    }
	    return _consumerService.getNumTransactions(account, start, end, extra);
	  }
	  


	  public AccountHistories getHistory(Account account, Calendar start, Calendar end, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getHistory(account, start, end, extra);
	    }
	    return _consumerService.getHistory(account, start, end, extra);
	  }
	  


	  public AccountSummaries getSummary(Account account, Calendar start, Calendar end, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getSummary(account, start, end, extra);
	    }
	    return _consumerService.getSummary(account, start, end, extra);
	  }
	  


	  public ExtendedAccountSummaries getExtendedSummary(Account account, Calendar start, Calendar end, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getExtendedSummary(account, start, end, extra);
	    }
	    return _consumerService.getExtendedSummary(account, start, end, extra);
	  }
	  


	  public IReportResult getReportData(SecureUser user, ReportCriteria criteria, HashMap extra)
	    throws BankingException, Exception
	  {
	    Properties reportOptions = criteria.getReportOptions();
	    String reportType = reportOptions.getProperty("REPORTTYPE");
	    
	    if ((reportType.equals("Transfer History Report")) || 
	      (reportType.equals("Transfer Detail Report")) || 
	      (reportType.equals("Pending Transfer Report")) || 
	      (reportType.equals("Transfer By Status"))) {
	      return _realtimeService.getReportData(user, criteria, extra);
	    }
	    if (user.getBusinessID() != 0) {
	      return _corporateService.getReportData(user, criteria, extra);
	    }
	    return _consumerService.getReportData(user, criteria, extra);
	  }
	  













































	  public void setSettings(String settings)
	  {
	    _settings = settings;
	  }
	  

	  public String getSettings()
	  {
	    return _settings;
	  }
	  

	  public int getAccounts(Accounts accounts, Map<String, Object> extraParam)
	  {
	    boolean useCorporate = true;
	    
	    if (accounts == null)
	    {
	      return 0;
	    }
	    
	    SecureUser sUser = accounts.getSecureUser();
	    if (sUser != null) {
	      if (sUser.getUserType() == 2)
	      {


	        return _realtimeService.getAccounts(accounts, extraParam); }
	      if ((sUser.getUserType() == 1) && (sUser.getBusinessID() == 0)) {
	        useCorporate = false;
	      } else if ((sUser.getUserType() == 1) && (sUser.getBusinessID() != 0)) {
	        useCorporate = true;
	      }
	    } else {
	      if (accounts.size() == 0) {
	        return 0;
	      }
	      Account account = (Account)accounts.get(0);
	      try {
	        useCorporate = isCorporate(account, new HashMap());
	      } catch (BankingException be) {
	        return be.getErrorCode();
	      }
	    }
	    


	    if (useCorporate) {
	      return _corporateService.getAccounts(accounts, extraParam);
	    }
	    
	    int ret = -1;
	    if (!_isConsumerServiceRealtime)
	    {
	      Accounts profileAccounts = null;
	      try {
	        profileAccounts = accountAdapter.getAccounts(accounts.getSecureUser());
	      } catch (ProfileException exc) {
	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw);
	        exc.printStackTrace(pw);
	        String errorMsg = "An error occurred while retreiving accounts from the Banking Service: " + sw.toString();
	        DebugLog.log(Level.SEVERE, errorMsg);
	        return 414;
	      }
	      
	      Iterator iterator = profileAccounts.iterator();
	      Account acct = null;
	      while (iterator.hasNext()) {
	        acct = (Account)iterator.next();
	        if (!accounts.contains(acct)) {
	          accounts.add(acct);
	        }
	      }
	      


	      ret = _consumerService.getAccounts(profileAccounts, extraParam);
	      Account account1 = null;
	      Account account2 = null;
	      iterator = profileAccounts.iterator();
	      while (iterator.hasNext()) {
	        account1 = (Account)iterator.next();
	        if (account1 != null) {
	          account2 = accounts.getByID(account1.getID());
	        } else {
	          account2 = null;
	        }
	        if (account2 != null) {
	          account2.setCurrentBalance(account1.getCurrentBalance());
	          account2.setAvailableBalance(account1.getAvailableBalance());
	          account2.setIntradayCurrentBalance(account1.getIntradayCurrentBalance());
	          account2.setIntradayAvailableBalance(account1.getIntradayAvailableBalance());
	          account2.putAll(account1.getHash());
	          setFilters(account2);
	        }
	      }
	    }
	    else {
	      ret = _consumerService.getAccounts(accounts, extraParam);
	    }
	    
	    return ret;
	  }
	  
	  @Cacheable(handler="com.sap.banking.account.interceptors.cache.BankingAccountsInterceptor.getBankingAccounts")
	  public Accounts getBankingAccounts(SecureUser secureUser, Map<String, Object> extraParam)
	    throws BankingException
	  {
	    String thisMethod = "BankingService.getBankingAccounts";
	    ExtTransferAccounts extTransferAccounts = null;
	    String originalFilterForAccounts = null;
	    Accounts bankingAccounts = null;
	    Accounts accounts = null;
	    HashMap<String, Object> extra = null;
	    
	    if ((extraParam instanceof HashMap)) {
	      extra = (HashMap)extraParam;
	    } else {
	      extra = new HashMap(extraParam);
	    }
	    if ((secureUser.getAppType() != null) && 
	      (secureUser.getAppType().equals("Business")))
	    {
	      int directoryId = secureUser.getBusinessID();
	      try {
	        bankingAccounts = accountService.getAccountsById(secureUser, directoryId, extra);
	        accounts = accountService.getAccounts(secureUser, extra);
	      } catch (ProfileException e) {
	        logger.error("BankingService.getBankingAccounts", e);
	        throw new BankingException(414);
	      }
	      

	      Account acct = null;
	      Account backendAcct = null;
	      
	      String saveFilter = bankingAccounts.getFilter();
	      bankingAccounts.setFilter("All");
	      accounts.setFilter("All");
	      
	      for (Iterator iter = bankingAccounts.iterator(); iter.hasNext();) {
	        acct = (Account)iter.next();
	        backendAcct = accounts.getByID(acct.getID());
	        





	        if (backendAcct != null) {
	          acct.setCurrentBalance(backendAcct.getCurrentBalance());
	          acct.setAvailableBalance(backendAcct.getAvailableBalance());
	          acct.setIntradayCurrentBalance(backendAcct.getIntradayCurrentBalance());
	          acct.setIntradayAvailableBalance(backendAcct.getIntradayAvailableBalance());
	          

	          acct.putAll(backendAcct.getHash());
	          
	          for (int j = 0; j < Account.acctTransactionFilters.length; j++) {
	            if (backendAcct.isFilterable(Account.acctTransactionFilters[j])) {
	              acct.setFilterable(Account.acctTransactionFilters[j]);
	            }
	          }
	          acct.setFilterable("TransferTemplate");
	        }
	      }
	      bankingAccounts.setFilter(saveFilter);
	      accounts.setFilter("All");
	      

	      if (bankingService.includeExtTransferAccounts(secureUser, extra)) {
	        try {
	          extra = EntitlementsUtil.allowEntitlementBypass(extra);
	          List<ExtTransferAccount> accountList = externalTransferAccountBO.getExtTransferAccounts(secureUser, null, extra);
	          extTransferAccounts = new ExtTransferAccounts();
	          extTransferAccounts.addAll(accountList);
	          extra = EntitlementsUtil.removeEntitlementBypass(extra);
	        }
	        catch (ExternalTransferException e) {
	          logger.error("BankingService.getBankingAccounts", e.getMessage());
	        }
	      }
	    }
	    else {
	      User user = null;
	      try {
	        if (secureUser.getUserType() == 2) {
	          user = new User();
	          user.setCustomerType(String.valueOf(1));
	        } else {
	          user = new User();
	          user.setId(String.valueOf(secureUser.getProfileID()));
	          user = customerAdapter.getUser(secureUser, user, null);
	        }
	      } catch (ProfileException e) {
	        throw new BankingException(414);
	      }
	      if (!user.getCustomerType().equals(String.valueOf(1)))
	      {
	        try
	        {
	          accounts = accountService.getAccounts(secureUser, extra);
	        } catch (ProfileException e) {
	          throw new BankingException(414);
	        }
	        
	        bankingAccounts = new Accounts(secureUser.getLocale());
	        bankingAccounts.setSecureUser(secureUser);
	        try {
	          bankingService.getAccounts(bankingAccounts, extraParam);
	        } catch (Exception e) {
	          int error = bankingService.signOn(secureUser.getBusinessName(), secureUser.getBusinessCIF());
	          if (error == 0)
	          {

	            bankingService.getAccounts(bankingAccounts, extraParam);
	          }
	        }
	        

	        try
	        {
	          if ((secureUser.getPrimaryUserID() == secureUser.getProfileID()) || (secureUser.getPrimaryUserID() != secureUser.getProfileID()))
	          {

	            mergeAccounts(secureUser, bankingAccounts, accounts, extra);
	            originalFilterForAccounts = accounts.getFilter();
	            

	            accounts.setFilter("COREACCOUNT=0,COREACCOUNT=2");
	            

	            copyAccountCollection(secureUser, accounts, bankingAccounts, false);
	            
	            accounts = bankingAccounts;
	            accounts.setFilter(originalFilterForAccounts);
	          }
	          

	          mergeBalances(accounts, bankingAccounts);
	        } catch (AccountException e) {
	          throw new BankingException(414);
	        }
	        originalFilterForAccounts = accounts.getFilter();
	        accounts.setFilter("COREACCOUNT=0");
	        accounts.setFilter(originalFilterForAccounts);
	      } else if (user.getCustomerType().equals(String.valueOf(1)))
	      {
	        int directoryId = secureUser.getBusinessID();
	        try {
	          bankingAccounts = accountService.getAccountsById(secureUser, directoryId, extra);
	          accounts = accountService.getAccounts(secureUser, extra);
	          mergeBalances(bankingAccounts, accounts);
	        } catch (Exception e) {
	          throw new BankingException(414);
	        }
	      }
	      
	      if (bankingService.includeExtTransferAccounts(secureUser, extra)) {
	        try {
	          extra = EntitlementsUtil.allowEntitlementBypass(extra);
	          List<ExtTransferAccount> extTransferAccountList = externalTransferAccountBO.getExtTransferAccounts(secureUser, null, extra);
	          extTransferAccounts = new ExtTransferAccounts();
	          extTransferAccounts.addAll(extTransferAccountList);
	          extra = EntitlementsUtil.removeEntitlementBypass(extra);
	        } catch (CSILException e) {
	          throw new BankingException(414);
	        }
	      }
	    }
	    

	    if ((accounts != null) && (extTransferAccounts != null)) {
	      Iterator iterator = accounts.iterator();
	      while (iterator.hasNext()) {
	        Account account = (Account)iterator.next();
	        ExtTransferAccount serviceAccount = extTransferAccounts.getByAccountNumberAndTypeAndRTN(account.getNumber(), account.getTypeValue(), account.getRoutingNum());
	        account.put("ExternalTransferACCOUNT", serviceAccount);
	        


	        if (null != serviceAccount) {
	          account.put("EXTACCTID", serviceAccount.getBpwID());
	          account.put("ACCTBANKRTNTYPE", serviceAccount.getAcctBankIDType());
	          account.setFilterable("ExternalTransferFrom");
	          account.setFilterable("ExternalTransferTo");
	          

	          account.setCountryCode(serviceAccount.getCountryCode());
	        }
	      }
	    }
	    
	    return accounts;
	  }
	  





	  protected void copyAccountCollection(SecureUser sUser, Accounts copyCollection, Accounts updateCollection, boolean clearDestinationCollection)
	    throws AccountException
	  {
	    String thisMethod = "BankingImpl.copyAccountCollection";
	    try {
	      if (copyCollection != null) {
	        if (updateCollection == null) {
	          updateCollection = new Accounts(sUser.getLocale());
	        }
	        
	        if (clearDestinationCollection) {
	          updateCollection.clear();
	        }
	        Iterator it = copyCollection.iterator();
	        while (it.hasNext()) {
	          Object obj = it.next();
	          updateCollection.add(obj);
	        }
	      }
	    } catch (Exception exception) {
	      logger.error(exception.getLocalizedMessage(), exception);
	      throw new AccountException(22000);
	    }
	  }
	  






	  protected void mergeBalances(Accounts bankingAccounts, Accounts accounts)
	    throws AccountException
	  {
	    String thisMethod = "BankingImpl.mergeBalances";
	    
	    if ((bankingAccounts == null) || (accounts == null)) {
	      throw new AccountException(18005);
	    }
	    
	    Account acct = null;
	    Account backendAcct = null;
	    
	    String saveFilter = bankingAccounts.getFilter();
	    bankingAccounts.setFilter("All");
	    accounts.setFilter("All");
	    
	    for (Iterator iter = bankingAccounts.iterator(); iter.hasNext();) {
	      acct = (Account)iter.next();
	      backendAcct = accounts.getByID(acct.getID());
	      





	      if (backendAcct != null) {
	        acct.setCurrentBalance(backendAcct.getCurrentBalance());
	        acct.setAvailableBalance(backendAcct.getAvailableBalance());
	        acct.setIntradayCurrentBalance(backendAcct.getIntradayCurrentBalance());
	        acct.setIntradayAvailableBalance(backendAcct.getIntradayAvailableBalance());
	        

	        acct.putAll(backendAcct.getHash());
	        


	        for (int j = 0; j < Account.acctTransactionFilters.length; j++) {
	          if (backendAcct.isFilterable(Account.acctTransactionFilters[j])) {
	            acct.setFilterable(Account.acctTransactionFilters[j]);
	          }
	        }
	        acct.setFilterable("TransferTemplate");
	      }
	    }
	    bankingAccounts.setFilter(saveFilter);
	    accounts.setFilter("All");
	  }
	  











	  protected void mergeAccounts(SecureUser sUser, Accounts primaryAccounts, Accounts secondaryAccounts, Map<String, Object> extraParam)
	    throws AccountException
	  {
	    String thisMethod = "BankingImpl.mergeAccounts";
	    
	    if ((primaryAccounts == null) || (secondaryAccounts == null)) {
	      throw new AccountException(18005);
	    }
	    
	    Iterator iterator = primaryAccounts.iterator();
	    try
	    {
	      while (iterator.hasNext()) {
	        Account account = (Account)iterator.next();
	        Account serviceAccount = secondaryAccounts.getByID(account.getID());
	        
	        if (serviceAccount == null) {
	          if ((account.getNickName() == null) || (account.getNickName().trim().length() == 0))
	            account.setNickName(account.getType());
	          account.set("HIDE", "0");
	          
	          if (account.getZBAFlag() == null) {
	            account.setZBAFlag("N");
	          }
	          
	          if (account.getShowPreviousDayOpeningLedger() == null) {
	            account.setShowPreviousDayOpeningLedger("Y");
	          }
	          
	          HashMap extra = null;
	          extra = new HashMap();
	          
	          if (sUser.getPrimaryUserID() == sUser.getProfileID())
	          {
	            String newStrippedAccountNumber = null;
	            String oldStrippedAccountNumber = null;
	            if (account != null) {
	              newStrippedAccountNumber = blockedAccountsService.getStrippedAccountNumber(account.getNumber(), extra);
	              oldStrippedAccountNumber = account.getStrippedAccountNumber();
	              
	              if (oldStrippedAccountNumber == null) {
	                account.setStrippedAccountNumber(newStrippedAccountNumber);
	              }
	              else if (!oldStrippedAccountNumber.equals(newStrippedAccountNumber)) {
	                AccountException ex = new AccountException(thisMethod, 50010);
	                logger.error(thisMethod, ex);
	                throw ex;
	              }
	            }
	            

	            account = accountService.addAccount(sUser, account, extra);
	          }
	          else
	          {
	            iterator.remove();
	          }
	        } else {
	          if ((serviceAccount.getNickName() == null) || (serviceAccount.getNickName().trim().length() == 0)) {
	            if ((account.getNickName() == null) || (account.getNickName().trim().length() == 0)) {
	              account.setNickName(serviceAccount.getType());
	            }
	          } else {
	            boolean isBackend = Boolean.valueOf(isAccountNickNameFromBackend).booleanValue();
	            if (!isBackend) {
	              account.setNickName(serviceAccount.getNickName());
	            }
	            else if ((account.getNickName() == null) || (account.getNickName().trim().length() == 0)) {
	              account.setNickName(serviceAccount.getNickName());
	            }
	          }
	          

	          if (account.getZBAFlag() == null) {
	            account.setZBAFlag(serviceAccount.getZBAFlag());
	            if (account.getZBAFlag() == null) {
	              account.setZBAFlag("N");
	            }
	          }
	          
	          if (account.getShowPreviousDayOpeningLedger() == null) {
	            account.setShowPreviousDayOpeningLedger(serviceAccount.getShowPreviousDayOpeningLedger());
	            if (account.getShowPreviousDayOpeningLedger() == null) {
	              account.setShowPreviousDayOpeningLedger("Y");
	            }
	          }
	          
	          HashMap hm = serviceAccount.getHash();
	          if (hm != null) {
	            Iterator iter = hm.entrySet().iterator();
	            while (iter.hasNext()) {
	              Map.Entry e = (Map.Entry)iter.next();
	              account.put((String)e.getKey(), e.getValue());
	            }
	          }
	        }
	        secondaryAccounts.removeByID(account.getID());
	      }
	    } catch (Exception exception) {
	      logger.error(exception.getLocalizedMessage(), exception);
	      throw new AccountException(22000);
	    }
	  }
	  

	  protected static void setFilters(Account account)
	  {
	    int accountType = account.getTypeValue();
	    account.setFilterable("Transactions");
	    
	    if (accountType == 1) {
	      account.setFilterable("TransferTo");
	      account.setFilterable("TransferFrom");
	      account.setFilterable("TransferTemplate");
	      account.setFilterable("BillPay");
	    }
	    else if (accountType == 2) {
	      account.setFilterable("TransferTo");
	      account.setFilterable("TransferFrom");
	      account.setFilterable("TransferTemplate");
	    }
	    else if (accountType == 3) {
	      account.setFilterable("TransferTo");
	      account.setFilterable("TransferFrom");
	      account.setFilterable("TransferTemplate");
	    }
	    else {
	      account.setFilterable("TransferTo");
	      account.setFilterable("TransferFrom");
	      account.setFilterable("TransferTemplate");
	      account.setFilterable("BillPay");
	    }
	  }
	  
	  public int getTransactions(Account account, Calendar startDate, Calendar endDate)
	  {
	    HashMap empty = new HashMap();
	    int error = 0;
	    try
	    {
	      if (isCorporate(account, empty)) {
	        _corporateService.getTransactions(account, startDate, endDate);
	      } else {
	        _consumerService.getTransactions(account, startDate, endDate);
	      }
	    } catch (BankingException be) {
	      error = be.getErrorCode();
	    }
	    
	    return error;
	  }
	  

	  public int getTransactions(Account account)
	  {
	    HashMap empty = new HashMap();
	    try
	    {
	      if (isCorporate(account, empty)) {
	        return _corporateService.getTransactions(account);
	      }
	      return _consumerService.getTransactions(account);
	    }
	    catch (BankingException be) {
	      return be.getErrorCode();
	    }
	  }
	  






































































	  public void setInitURL(String url)
	  {
	    initialize(url);
	  }
	  
	  /**
	   * @deprecated
	   */
	  public int signOn(String userID, String password)
	  {
	    return 2;
	  }
	  

	  public int changePIN(String currentPin, String newPin)
	  {
	    return _consumerService.changePIN(currentPin, newPin);
	  }
	  
	  public boolean signOn(SecureUser sUser, String userID, String password)
	    throws CSILException
	  {
	    if (sUser == null) {
	      throw new CSILException(18001);
	    }
	    

	    boolean ret1 = _realtimeService.signOn(sUser, userID, password);
	    boolean ret2 = false;
	    
	    if (sUser.getBusinessID() != 0) {
	      ret2 = _corporateService.signOn(sUser, userID, password);
	    }
	    else if (_isConsumerServiceRealtime)
	    {
	      ret2 = ret1;
	    } else {
	      ret2 = _consumerService.signOn(sUser, userID, password);
	    }
	    

	    return (ret1) && (ret2);
	  }
	  































































































	  public AccountSummaries getSummary(Accounts accounts, Calendar start, Calendar end, String batchSize, HashMap extra)
	    throws BankingException
	  {
	    Account account = null;
	    if ((accounts != null) && (accounts.size() > 0)) {
	      account = (Account)accounts.get(0);
	    }
	    if (isCorporate(account, extra)) {
	      return _corporateService.getSummary(accounts, start, end, batchSize, extra);
	    }
	    return _consumerService.getSummary(accounts, start, end, batchSize, extra);
	  }
	  
































































































	  public AccountHistories getHistory(Accounts accounts, Calendar start, Calendar end, String batchSize, HashMap extra)
	    throws BankingException
	  {
	    return _corporateService.getHistory(accounts, start, end, batchSize, extra);
	  }
	  
	  public void setStoredService(BankingService service) {
	    _storedService = service;
	  }
	  
	  public void setRealtimeService(BankingService realtimeService) {
	    _realtimeService = realtimeService;
	  }
	  




































































































	  public Transactions getSpecificPageOfTransactions(Account account, PagingContext pagingContext, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getSpecificPageOfTransactions(account, pagingContext, extra);
	    }
	    PagingContext newPagingContext = translatePagingContextForDC(account, pagingContext, extra);
	    return _consumerService.getSpecificPageOfTransactions(account, newPagingContext, extra);
	  }
	  








	  public Transaction getTransactionById(Account account, String id, HashMap extra)
	    throws BankingException
	  {
	    if (isCorporate(account, extra)) {
	      return _corporateService.getTransactionById(account, id, extra);
	    }
	    return _consumerService.getTransactionById(account, id, extra);
	  }
	  







	  public boolean includeExtTransferAccounts(SecureUser secureUser, Map<String, Object> extraParam)
	  {
	    if (secureUser.isCorporateUser()) {
	      return _corporateService.includeExtTransferAccounts(secureUser, extraParam);
	    }
	    return _consumerService.includeExtTransferAccounts(secureUser, extraParam);
	  }
	  


	  public Transactions getTransactions(SecureUser secureUser, TransactionHistorySearchCriteria criteria, Map<String, Object> extraParam)
	    throws BankingException
	  {
	    Transactions transactions;	   	   
	    
	    if (secureUser.isCorporateUser()) {
	      transactions = _corporateService.getTransactions(secureUser, criteria, extraParam);
	    }
	    else {
	      transactions = _consumerService.getTransactions(secureUser, criteria, extraParam);
	    }
	    return transactions;
	  }
	  







	  public boolean checkForUpdatedBalances(SecureUser secureUser, Map<String, Object> extraParam)
	  {
	    if (secureUser.isCorporateUser()) {
	      return _corporateService.checkForUpdatedBalances(secureUser, extraParam);
	    }
	    return _consumerService.checkForUpdatedBalances(secureUser, extraParam);
	  }
	  










	  public Transactions getAccountTransactions(SecureUser secureUser, AccountHistorySearchCriteria criteria, Map<String, Object> extraParam)
	    throws BankingException
	  {
	    throw new BankingException(0);
	  }
}
