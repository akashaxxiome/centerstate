package com.sap.banking.account.bo.interfaces;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.sap.banking.account.exception.AccountException;
import com.sap.banking.accountconfig.beans.AccountBalanceSearchCriteria;
import com.sap.banking.accountconfig.beans.AccountDisplayTextFormatConfig;
import com.sap.banking.accountconfig.beans.AccountHistorySearchCriteria;
import com.sap.banking.accountconfig.beans.AccountSearchCriteria;
import com.sap.banking.accountconfig.beans.AccountSummaryCriteria;
import com.sap.banking.accountconfig.beans.CashFlowSearchCriteria;
import com.sap.banking.accountconfig.beans.ConsolidatedBalanceSearchCriteria;
import com.sap.banking.accountconfig.beans.FavoriteAccountsSearchCriteria;
import com.sap.banking.accountconfig.beans.TransactionHistorySearchCriteria;
import com.sap.banking.accountconfig.constants.AccountDisplayTextFormatEnum;
import com.sap.banking.common.beans.ValidationResult;
import java.util.List;
import java.util.Map;

public abstract interface BankingAccountsCustom
{
  public abstract String getAccountDisplayText(SecureUser paramSecureUser, Account paramAccount, AccountDisplayTextFormatEnum paramAccountDisplayTextFormatEnum, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract List<Account> getBankingAccounts(SecureUser paramSecureUser, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract Account getBankingAccountById(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract List<Account> getBankingAccounts(SecureUser paramSecureUser, AccountSearchCriteria paramAccountSearchCriteria, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract List<Account> getAllBankingAccounts(SecureUser paramSecureUser, AccountSearchCriteria paramAccountSearchCriteria, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract List<Account> searchAccounts(SecureUser paramSecureUser, AccountSearchCriteria paramAccountSearchCriteria, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract Account getAccount(SecureUser paramSecureUser, Account paramAccount, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract Account getAccount(SecureUser paramSecureUser, Account paramAccount, AccountDisplayTextFormatEnum paramAccountDisplayTextFormatEnum, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract ValidationResult validateAccountSearchCriteria(SecureUser paramSecureUser, AccountSearchCriteria paramAccountSearchCriteria, String paramString)
    throws AccountException;
  
  public abstract AccountDisplayTextFormatConfig getAccountDisplayTextFormatConfig(SecureUser paramSecureUser, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract String getUserPreference(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract AccountSearchCriteria getDefaultAccountSearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract AccountBalanceSearchCriteria getDefaultAccountBalanceSearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract AccountHistorySearchCriteria getDefaultAccountHistorySearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract AccountSummaryCriteria getDefaultAccountSummaryCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract CashFlowSearchCriteria getDefaultCashFlowSearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract ConsolidatedBalanceSearchCriteria getDefaultConsolidatedBalanceSearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract FavoriteAccountsSearchCriteria getDefaultFavoriteAccountsSearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract TransactionHistorySearchCriteria getDefaultTransactionHistorySearchCriteria(SecureUser paramSecureUser, String paramString, Map<String, Object> paramMap)
    throws AccountException;
  
  public abstract boolean checkForUpdatedBalances(SecureUser paramSecureUser, Map<String, Object> paramMap);
}


/* Location:              C:\Users\tabare\Desktop\Libs\com.sap.banking.banking-api-8.3.2\com.sap.banking.banking-api-8.3.2.jar!\com\sap\banking\account\bo\interfaces\BankingAccounts.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */