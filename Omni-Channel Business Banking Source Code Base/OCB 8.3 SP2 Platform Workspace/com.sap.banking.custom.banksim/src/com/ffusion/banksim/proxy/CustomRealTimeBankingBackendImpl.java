package com.ffusion.banksim.proxy;

import com.ffusion.banksim.interfaces.BSException;
import com.ffusion.banksim.interfaces.BankingBackend;
import com.ffusion.banksim.interfaces.RealTimeBankingBackend;
import com.ffusion.beans.Bank;
import com.ffusion.beans.Contact;
import com.ffusion.beans.DateTime;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.AccountHistories;
import com.ffusion.beans.accounts.AccountHistory;
import com.ffusion.beans.accounts.AccountSummaries;
import com.ffusion.beans.accounts.AccountSummary;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.accounts.AssetAcctSummary;
import com.ffusion.beans.accounts.CreditCardAcctSummary;
import com.ffusion.beans.accounts.DepositAcctSummary;
import com.ffusion.beans.accounts.ExtendedAccountSummaries;
import com.ffusion.beans.accounts.ExtendedAccountSummary;
import com.ffusion.beans.accounts.FixedDepositInstrument;
import com.ffusion.beans.accounts.FixedDepositInstruments;
import com.ffusion.beans.accounts.LoanAcctSummary;
import com.ffusion.beans.banking.Transaction;
import com.ffusion.beans.banking.Transactions;
import com.ffusion.beans.banking.Transfer;
import com.ffusion.beans.common.Currency;
import com.ffusion.beans.messages.Message;
import com.ffusion.beans.reporting.ReportCriteria;
import com.ffusion.beans.reporting.ReportSortCriterion;
import com.ffusion.beans.user.User;
import com.ffusion.util.beans.PagingContext;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.impl.CustomTransaction;
import com.sap.banking.custom.cxfrestclient.coastcapitalpoc.interfaces.CoastCapitalPOC;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class CustomRealTimeBankingBackendImpl
  implements RealTimeBankingBackend
{
  @Autowired
  @Qualifier("bankingBackendRef")
  private BankingBackend bankingBackend;
  
  @Autowired
  @Qualifier("CoastCapitalPOCRESTClientRef")
  private CoastCapitalPOC CoastCapitalPOCRESTClient;
  
  public BankingBackend getBankingBackend()
  {
    return bankingBackend;
  }
  
  public void setBankingBackend(BankingBackend bankingBackend) {
    this.bankingBackend = bankingBackend;
  }
  
  public boolean isInitialized()
  {
    return bankingBackend.isInitialized();
  }
  
  public User signOn(String userID, String password) throws BSException
  {
    return bankingBackend.signOn(userID, password);
  }
  
  public void setPassword(User customer, String newPassword)
    throws BSException
  {
    bankingBackend.setPassword(customer, newPassword);
  }
  
  public void addBank(Bank bank)
    throws BSException
  {
    bankingBackend.addBank(bank);
  }
  
  public void addBanks(Bank[] banks) throws BSException
  {
    bankingBackend.addBanks(banks);
  }
  
  public Bank getBank(String name) throws BSException
  {
    return bankingBackend.getBank(name);
  }
  
  public void updateBank(Bank bank) throws BSException
  {
    bankingBackend.updateBank(bank);
  }
  
  public void updateBanks(Bank[] bank) throws BSException
  {
    bankingBackend.updateBanks(bank);
  }
  
  public void deleteBank(Bank bank) throws BSException
  {
    bankingBackend.deleteBank(bank);
  }
  
  public void addCustomer(User customer) throws BSException
  {
    bankingBackend.addCustomer(customer);
  }
  
  public void addCustomers(User[] customers) throws BSException
  {
    bankingBackend.addCustomers(customers);
  }
  
  public void updateCustomer(User customer)
    throws BSException
  {
    bankingBackend.updateCustomer(customer);
  }
  
  public void updateCustomers(User[] customers) throws BSException
  {
    bankingBackend.updateCustomers(customers);
  }
  
  public void deleteCustomer(User customer) throws BSException
  {
    bankingBackend.deleteCustomer(customer);
  }
  
  public void addAccount(User customer, Account toAdd) throws BSException
  {
    bankingBackend.addAccount(customer, toAdd);
  }
  
  public void addAccounts(User customer, Account[] toAdd) throws BSException
  {
    bankingBackend.addAccounts(customer, toAdd);
  }
  
  public Enumeration getAccounts(User customer) throws BSException
  {
	 String _value = System.getProperty("coastcapital.poc.available");
	 if(_value != null && "true".equals(_value.toLowerCase())) {
		 Vector vector = new Vector();
		 Accounts accounts = new Accounts();
		 
		 Enumeration accountsEnumeration =  bankingBackend.getAccounts(customer);
		 Account accountOld = (Account) accountsEnumeration.nextElement();
		 
		 Account account = accounts.create("10110001902600");
		 account.setNumber("10110001902600");
		 account.setID(account.getNumber());	 
	     
	     account.setBankName("Coast Capital Bank");
	     account.setCurrencyCode("CAD");
		 account.setNickName("POC Coast Capital");
	     account.setAccountDisplayText("POC Coast Capital");
	     
	     account.setRoutingNum(accountOld.getRoutingNum());
	     account.setBankID(accountOld.getBankID());
	     account.setStrippedAccountNumber(account.getNumber());
	     
	     account.setStatus(accountOld.getStatus());
	     account.setLocale(accountOld.getLocale());     
	     
	     account.setCurrentBalance(accountOld.getCurrentBalance());
	     account.setAvailableBalance(accountOld.getAvailableBalance());    
	
	     account.setPrimaryAccount(accountOld.getPrimaryAccount());
	     account.setCoreAccount(accountOld.getCoreAccount());
	     account.setPersonalAccount(accountOld.getPersonalAccount());
	     account.setPositivePay(accountOld.getPositivePay());
		 account.setType(2);
		 
		 account.setAccountGroup(accountOld.getAccountGroup());
		 account.setBicAccount(accountOld.getBicAccount());
		 account.setClosingBalance(accountOld.getClosingBalance());
		 account.setContact(accountOld.getContact());
		 account.setContactId(accountOld.getContactId());
		 account.setCustId(accountOld.getCustId());
		 account.setDateFormat(accountOld.getDateFormat());
		 account.setDirectoryID(accountOld.getDirectoryID());
		 account.setInterestRate(accountOld.getInterestRate());
		 account.setInternalAccountId(account.getNumber());
		 account.setIntradayAvailableBalance(accountOld.getIntradayAvailableBalance());
		 account.setIntradayCurrentBalance(accountOld.getIntradayCurrentBalance());
		 account.setIsFilterableCheck(accountOld.getIsFilterable());
		 account.setLocationID(accountOld.getLocationID());
		 account.setMap(accountOld.getMap());
		 account.setMaster(accountOld.isMaster());
		 account.setTrackingID(accountOld.getTrackingID());
		 account.setZBAFlag(accountOld.getZBAFlag());
		 account.setWithholdNonZeroBalanceSubAccounts(accountOld.getWithholdNonZeroBalanceSubAccounts());
		 
		 vector.add(account);
		 return vector.elements();
	 }
	 else {
		 return bankingBackend.getAccounts(customer);
  	 }
  }
  
  public Account getAccount(Account account) throws BSException
  {
    return bankingBackend.getAccount(account);
  }
  
  public void updateAccount(Account account) throws BSException
  {
    bankingBackend.updateAccount(account);
  }
  
  public void updateAccounts(Account[] accounts) throws BSException
  {
    bankingBackend.updateAccounts(accounts);
  }
  
  public void deleteAccount(Account account) throws BSException
  {
    bankingBackend.deleteAccount(account);
  }
  
  public Transfer addTransfer(Transfer transfer, int transType)
    throws BSException
  {
    return bankingBackend.addTransfer(transfer, transType);
  }
  

  public void addBPWTransfer(String bankId, String acctIdTo, String acctTypeTo, String acctIdFrom, String acctTypeFrom, String amount, String curDef)
    throws BSException
  {
    bankingBackend.addBPWTransfer(bankId, acctIdTo, acctTypeTo, acctIdFrom, acctTypeFrom, amount, curDef);
  }
  




  public void addBPWTransfer(String bankId, String acctIdTo, String acctTypeTo, String acctIdFrom, String acctTypeFrom, String amount, String curDef, String toAmount, String toAmtCurrency, int transType)
    throws BSException
  {
    bankingBackend.addBPWTransfer(bankId, acctIdTo, acctTypeTo, acctIdFrom, acctTypeFrom, amount, curDef, toAmount, toAmtCurrency, transType);
  }
  


  public Transfer[] addTransfers(Transfer[] transfers, int[] transType)
    throws BSException
  {
    return bankingBackend.addTransfers(transfers, transType);
  }
  
  public Enumeration getTransactions(Account account, Calendar startDate, Calendar endDate)
    throws BSException
  {
    return bankingBackend.getTransactions(account, startDate, endDate);
  }
  
  public void openPagedTransactions(Account account, Calendar startDate, Calendar endDate)
    throws BSException
  {
    bankingBackend.openPagedTransactions(account, startDate, endDate);
  }
  

  public void openPagedTransactions(Account account, PagingContext context, HashMap extra)
    throws BSException
  {
    bankingBackend.openPagedTransactions(account, context, extra);
  }
  
  public Transactions getPagedTransactions(Account account, PagingContext context, HashMap extra)
    throws BSException
  {
    return bankingBackend.getPagedTransactions(account, context, extra);
  }
  
  public Transactions getNextTransactions(Account account, PagingContext context, HashMap extra)
    throws BSException
  {
    return bankingBackend.getNextTransactions(account, context, extra);
  }
  

  public Transactions getPreviousTransactions(Account account, PagingContext context, HashMap extra)
    throws BSException
  {
    return bankingBackend.getPreviousTransactions(account, context, extra);
  }
  
  public void closePagedTransactions(Account account) throws BSException
  {
    bankingBackend.closePagedTransactions(account);
  }
  
  public int getNumberOfTransactions(Account account) throws BSException
  {
    return bankingBackend.getNumberOfTransactions(account);
  }
  


  public Enumeration getNextPage(Account account, int howMany)
    throws BSException
  {
    return bankingBackend.getNextPage(account, howMany);
  }
  
  public Enumeration getNextPage(Account account, int howMany, int nextIndex)
    throws BSException
  {
    return bankingBackend.getNextPage(account, howMany, nextIndex);
  }
  
  public Enumeration getPrevPage(Account account, int howMany)
    throws BSException
  {
    return bankingBackend.getPrevPage(account, howMany);
  }
  
  public Enumeration getPrevPage(Account account, int howMany, int prevIndex)
    throws BSException
  {
    return bankingBackend.getPrevPage(account, howMany, prevIndex);
  }
  
  public void addMailMessage(User customer, Message message)
    throws BSException
  {
    bankingBackend.addMailMessage(customer, message);
  }
  
  public Enumeration getMailMessages(User customer) throws BSException
  {
    return bankingBackend.getMailMessages(customer);
  }
  
  public Transactions getSpecificPage(Account account, PagingContext pagingContext, HashMap extra)
    throws BSException
  {
	    String _value = System.getProperty("coastcapital.poc.available");
		if(_value != null && "true".equals(_value.toLowerCase())) {
		    ReportSortCriterion reportShortCriterion = null;
			String startDate = null;
			String endDate = null;
			SimpleDateFormat searchFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			SimpleDateFormat searchFormatterClient = new SimpleDateFormat("yyyyMMdd");
			ReportCriteria reportCriteria = (ReportCriteria) pagingContext.getMap().get("ReportCriteria");
			if(reportCriteria != null){
				if(reportCriteria.getSortCriteria() != null && reportCriteria.getSortCriteria().size() > 0){
					reportShortCriterion = (ReportSortCriterion) reportCriteria.getSortCriteria().get(0);
				}
				if(reportCriteria.getSearchCriteria() != null){
					startDate = (String) reportCriteria.getSearchCriteria().get("StartDate");
					endDate = (String)  reportCriteria.getSearchCriteria().get("EndDate");
				}
			}	
			String startDateClient = null;
			Date startDateFormat = null;
			if(startDate != null){
				try {
					startDateFormat = searchFormatter.parse(startDate);
					startDateClient = searchFormatterClient.format(startDateFormat);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			String endDateClient = null;
			Date endDateFormat = null;
			if(endDate != null){
				try {
					endDateFormat = searchFormatter.parse(endDate);
					endDateClient = searchFormatterClient.format(endDateFormat);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			String postingDate = null;
			String postingDateOperand = null;
			boolean filterAfterNeeded = false;
			if(startDateClient != null && endDateClient != null){
				if(startDateClient.equals(endDateClient)){
					postingDateOperand = "=";
					postingDate = startDateClient;			
				}else{
					postingDateOperand = "<";
					Calendar calendar = Calendar.getInstance();
				    calendar.setTime(startDateFormat);
				    calendar.add(Calendar.DAY_OF_YEAR, -1);
					startDateFormat = calendar.getTime();
					startDateClient = searchFormatterClient.format(startDateFormat);
					postingDate = startDateClient;
					filterAfterNeeded = true;
				}
			}
			
			Transactions transactions = new Transactions();
			List<CustomTransaction> transactionHistoryByAccountId = CoastCapitalPOCRESTClient.getTransactionHistoryByAccountId(account.getNumber(), postingDate, postingDateOperand);
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yy");
			if(transactionHistoryByAccountId != null) {
				for (CustomTransaction customTransaction : transactionHistoryByAccountId) {
					Date date = null;
					try {
						date = formatter.parse(customTransaction.getPostingDate());
					} catch (ParseException e) {
						e.printStackTrace();
					}
							
					if(filterAfterNeeded && date.compareTo(endDateFormat) > 0){
						continue;
					}
					
					Transaction transaction = transactions.create();
					DateTime dateTime = new DateTime(date, account.getLocale());
					transaction.setDate(dateTime);
					transaction.setPostingDate(dateTime);
					transaction.setValueDate(dateTime);
					transaction.setReferenceNumber(customTransaction.getReference());
					transaction.setID(customTransaction.getReference());
					transaction.setTrackingID(customTransaction.getTellerReference());
					transaction.setCategory(1);
					transaction.setDescription(customTransaction.getTransactionDescription());
					transaction.setCustomerReferenceNumber(customTransaction.getAccountRefNum());
		
					System.out.println("customTransaction.getDebitAmount : " + customTransaction.getDebitAmount());
					System.out.println("customTransaction.getCreditAmount : " + customTransaction.getCreditAmount());
					
					if ("".equals(customTransaction.getDebitAmount()) || "0.00".equals(customTransaction.getDebitAmount())) {
						transaction.setIsCredit(true);						
						//transaction.setAmount(new BigDecimal(customTransaction.getCreditAmount()));
						
						//Set initial value
						transaction.setAmount(new BigDecimal("0"));
						
						String _amountCredit = customTransaction.getCreditAmount();
						if(_amountCredit != null && !"".equals(_amountCredit)){
						   _amountCredit = _amountCredit.replace(",", "");
						   System.out.println("_amountCredit : " + _amountCredit);
						   
						   try{
								BigDecimal _amountCreditValue = new BigDecimal(_amountCredit);
								
								//NumberFormat _formatter = NumberFormat.getInstance(new Locale("en_US"));
								//transaction.setAmount(formatter.format(_amountDebitValue.longValue()));
								
								DecimalFormatSymbols _symbols = DecimalFormatSymbols.getInstance();
								NumberFormat _formatter = new DecimalFormat("###,###.##", _symbols);
								transaction.setAmount(_formatter.format(_amountCreditValue));
							}catch(Exception ex){
								System.out.println("Exception: " + ex.getMessage());
							}
						}
						else {
							System.out.println("_amountCredit: IS NULL OR EMPTY ");							
						}
												
						//transaction.setAmount(new BigDecimal(_amountDebit));
						transaction.setType(4);
					} else if ("".equals(customTransaction.getCreditAmount()) || "0.00".equals(customTransaction.getDebitAmount())) {
						transaction.setIsCredit(false);
						//transaction.setAmount(new BigDecimal(customTransaction.getDebitAmount()).negate());
						
						//Set initial value
						transaction.setAmount(new BigDecimal("0"));
						
						String _amountDebit = customTransaction.getDebitAmount();
						if(_amountDebit != null && !"".equals(_amountDebit)){
							_amountDebit = _amountDebit.replace(",", "");
						    System.out.println("_amountDebit : " + _amountDebit);
						   
						   try{
								BigDecimal _amountDebitValue = new BigDecimal(_amountDebit).negate();
//								NumberFormat _formatter = NumberFormat.getInstance(new Locale("en_US"));
//								transaction.setAmount(formatter.format(_amountCreditValue.longValue()));
								
								DecimalFormatSymbols _symbols = DecimalFormatSymbols.getInstance();
								NumberFormat _formatter = new DecimalFormat("###,###.##", _symbols);
								transaction.setAmount(_formatter.format(_amountDebitValue));
							} catch(Exception ex) {
							    System.out.println("Exception: " + ex.getMessage());	
							}
						}
						else {
							System.out.println("_amountDebit: IS NULL OR EMPTY ");							
						}
											
						//transaction.setAmount(new BigDecimal(_amountCredit).negate());
											
						transaction.setType(5);
					}
		
					Currency balance = new Currency();
					balance.setAmount(customTransaction.getBalance());
					balance.setCurrencyCode(account.getCurrencyCode());
					transaction.setRunningBalance(balance);
					transaction.setAccount(account);
				}
			}
			
			if(reportShortCriterion != null && transactions.size() > 1)
				return sortTransactions(transactions, reportShortCriterion);
			else return transactions;
		}
		else {
			return bankingBackend.getSpecificPage(account, pagingContext, extra);
		}
  }
  
  private Transactions sortTransactions(Transactions transactions, ReportSortCriterion reportShortCriterion) {

		Transactions sortedList = transactions;

		switch (reportShortCriterion.getName()) {
		case "Date":
			if (reportShortCriterion.getAsc()) {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t1.getDateValue().getTime().compareTo(t2.getDateValue().getTime());
					}
				});
			} else {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t2.getDateValue().getTime().compareTo(t1.getDateValue().getTime());
					}
				});
			}
			break;

		case "TransactionType":
			if (reportShortCriterion.getAsc()) {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t1.getType().compareTo(t2.getType());
					}
				});
			} else {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t2.getType().compareTo(t1.getType());
					}
				});
			}
			break;

		case "Description":
			if (reportShortCriterion.getAsc()) {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t1.getDescription().compareTo(t2.getDescription());
					}
				});
			} else {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t2.getDescription().compareTo(t1.getDescription());
					}
				});
			}
			break;

		case "TransactionReferenceNumber":
			if (reportShortCriterion.getAsc()) {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t1.getReferenceNumber().compareTo(t2.getReferenceNumber());
					}
				});
			} else {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t2.getReferenceNumber().compareTo(t1.getReferenceNumber());
					}
				});
			}
			break;

		case "Amount":
			if (reportShortCriterion.getAsc()) {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t1.getAmount().compareTo(t2.getAmount());
					}
				});
			} else {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t2.getAmount().compareTo(t1.getAmount());
					}
				});
			}
			break;

		case "RunningBalance":
			if (reportShortCriterion.getAsc()) {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t1.getRunningBalance().compareTo(t2.getRunningBalance());
					}
				});
			} else {
				Collections.sort(transactions, new Comparator<Transaction>() {
					public int compare(Transaction t1, Transaction t2) {
						return t2.getRunningBalance().compareTo(t1.getRunningBalance());
					}
				});
			}
			break;

		default:
			break;
		}

		return sortedList;
	}

  public Transactions getAccountTransactions(Account account, PagingContext pagingContext, HashMap extra)
    throws BSException
  {
    return bankingBackend.getAccountTransactions(account, pagingContext, extra);
  }
  
  public Transaction getTransactionById(Account account, String transId, HashMap extra)
    throws BSException
  {
    return bankingBackend.getTransactionById(account, transId, extra);
  }
  



  public void changePIN(String currentPin, String newPin)
    throws BSException
  {}
  


  public FixedDepositInstruments getFixedDepositInstruments(Account account, Calendar start, Calendar end, HashMap extra)
    throws BSException
  {
    if (account == null) {
      throw new BSException(1011);
    }
    
    Locale locale = account.getLocale();
    DateTime instrumentDate = start != null ? new DateTime(start, locale) : new DateTime(locale);
    
    FixedDepositInstruments instruments = new FixedDepositInstruments();
    FixedDepositInstrument instrument = instruments.createFixedDepositInstrument();
    
    instrument.setAccountNumber(account.getNumber());
    instrument.setAccountID(account.getID());
    instrument.setBankID(account.getBankID());
    instrument.setRoutingNumber(account.getRoutingNum());
    instrument.setInstrumentNumber("10110001902600");
    instrument.setInstrumentBankName("Fusion Bank");
    instrument.setCurrency(account.getCurrencyCode());
    
    Currency principalAmt = new Currency("100.0", locale);
    instrument.setPrincipalAmount(principalAmt);
    
    instrument.setInterestRate(2.0355F);
    
    Currency accruedInterest = new Currency("200.0", locale);
    instrument.setAccruedInterest(accruedInterest);
    
    instrument.setMaturityDate(instrumentDate);
    
    Currency interestAtMaturity = new Currency("300.0", locale);
    instrument.setInterestAtMaturity(interestAtMaturity);
    
    Currency proceedsAtMaturity = new Currency("400.0", locale);
    instrument.setProceedsAtMaturity(proceedsAtMaturity);
    
    instrument.setValueDate(instrumentDate);
    instrument.setDaysInTerm(90);
    
    Currency restrictedAmt = new Currency("500.0", locale);
    instrument.setRestrictedAmount(restrictedAmt);
    
    instrument.setNumberOfRollovers(5);
    
    Contact stmtMailAddr1 = new Contact();
    stmtMailAddr1.setStreet("742 Evergreen Terrace");
    
    stmtMailAddr1.setCity("Springfield");
    stmtMailAddr1.setState("MA");
    stmtMailAddr1.setCountry("United States of America");
    stmtMailAddr1.setEmail("hsimpson@fox.com");
    stmtMailAddr1.setPhone("555-1234");
    



    stmtMailAddr1.setPreferredContactMethod("e-mail");
    instrument.setStatementMailingAddr1(stmtMailAddr1);
    
    Contact stmtMailAddr2 = new Contact();
    stmtMailAddr2.setStreet("24 Sussex Drive");
    
    stmtMailAddr2.setCity("Ottawa");
    stmtMailAddr2.setState("ON");
    stmtMailAddr2.setCountry("Canada");
    stmtMailAddr2.setEmail("pm@pm.gc.ca");
    stmtMailAddr2.setPhone("(613) 992-4211");
    
    stmtMailAddr2.setZipCode("K1A 0A2");
    
    stmtMailAddr2.setFaxPhone("(613) 941-6900");
    stmtMailAddr2.setPreferredContactMethod("phone");
    instrument.setStatementMailingAddr2(stmtMailAddr2);
    
    Contact stmtMailAddr3 = new Contact();
    stmtMailAddr3.setStreet("Skywalker Ranch");
    stmtMailAddr3.setStreet2("5858 Lucas Valley Rd.");
    stmtMailAddr3.setCity("Nicasio");
    stmtMailAddr3.setState("CA");
    stmtMailAddr3.setCountry("United States of Ameria");
    


    stmtMailAddr3.setZipCode("94946");
    

    stmtMailAddr3.setPreferredContactMethod("none");
    instrument.setStatementMailingAddr3(stmtMailAddr3);
    
    instrument.setSettlementInstructionType(1);
    instrument.setSettlementTargetRoutingNumber("55555");
    instrument.setSettlementTargetAccountNumber("12345");
    instrument.setDataDate(instrumentDate);
    
    instrument.setLocale(locale);
    instrument.setDateFormat(account.getDateFormat());
    
    return instruments;
  }
  
  public void updateFixedDepositInstrument(FixedDepositInstrument inst, HashMap extra)
    throws BSException
  {
    if (inst == null) {
      throw new BSException(1014);
    }
  }
  


  public Transactions getFDInstrumentTransactions(FixedDepositInstrument inst, Calendar start, Calendar end, HashMap extra)
    throws BSException
  {
    if (inst == null) {
      throw new BSException(1014);
    }
    
    Locale locale = inst.getLocale();
    DateTime transactionDate = start != null ? new DateTime(start, locale) : new DateTime(locale);
    


    Transactions transactions = new Transactions();
    
    Transaction tran1 = transactions.create();
    tran1.setID("12345");
    tran1.setType(1);
    tran1.setCategory(1);
    tran1.setDescription("Rollover Principal");
    tran1.setDate(transactionDate);
    tran1.setAmount("1000.0");
    tran1.setFixedDepositRate(2.2205F);
    tran1.setInstrumentNumber(inst.getInstrumentNumber());
    tran1.setInstrumentBankName(inst.getInstrumentBankName());
    
    Transaction tran2 = transactions.create();
    tran2.setID("67890");
    tran2.setType(1);
    tran2.setCategory(1);
    tran2.setDescription("Rollover Principal/Interest");
    tran2.setDate(transactionDate);
    tran2.setAmount("2000.0");
    tran2.setFixedDepositRate(3.14159F);
    tran2.setInstrumentNumber(inst.getInstrumentNumber());
    tran1.setInstrumentBankName(inst.getInstrumentBankName());
    
    transactions.setLocale(locale);
    transactions.setDateFormat(inst.getDateFormat());
    
    return transactions;
  }
  
  public AccountHistories getHistory(Account account, Calendar start, Calendar end, HashMap extra)
    throws BSException
  {
    if (account == null) {
      throw new BSException(1011);
    }
    
    Locale locale = account.getLocale();
    DateTime historyDate = start != null ? new DateTime(start, locale) : new DateTime(locale);
    


    AccountHistories histories = new AccountHistories();
    AccountHistory history = histories.create();
    
    history.setAccountNumber("10110001902600");
    //history.setAccountNumber(account.getNumber());
    history.setAccountID(account.getID());
    history.setBankID(account.getBankID());
    history.setRoutingNumber(account.getRoutingNum());
    history.setHistoryDate(historyDate);
    
    history.setOpeningLedger("27500");
    history.setAvgOpeningLedgerMTD("85055");
    history.setAvgOpeningLedgerYTD("54900");
    history.setClosingLedger("14955");
    history.setAvgClosingLedgerMTD("852900");
    history.setAvgMonth("27555");
    history.setAggregateBalAdjustment("1000");
    history.setAvgClosingLedgerYTDPrevMonth("85055");
    history.setAvgClosingLedgerYTD("14900");
    history.setCurrentLedger("52055");
    history.setNetPositionACH("857500");
    history.setOpenAvailSameDayACHDTC("1055");
    history.setOpeningAvail("5000");
    history.setAvgOpenAvailMTD("85055");
    history.setAvgOpenAvailYTD("22900");
    history.setAvgAvailPrevMonth("57555");
    history.setDisbursingOpeningAvailBal("85000");
    history.setClosingAvail("1055");
    history.setAvgClosingAvailMTD("5000");
    history.setAvgClosingAvailPrevMonth("852055");
    history.setAvgClosingAvailYTDPrevMonth("274900");
    history.setAvgClosingAvailYTD("5055");
    history.setLoanBal("85000");
    history.setTotalInvestmentPosition("1055");
    history.setCurrentAvailCRSSurpressed("52000");
    history.setCurrentAvail("857555");
    history.setAvgCurrentAvailMTD("14900");
    history.setAvgCurrentAvailYTD("5055");
    history.setTotalFloat("85000");
    history.setTargetBal("22055");
    history.setAdjustedBal("57500");
    history.setAdjustedBalMTD("85055");
    history.setAdjustedBalYTD("14900");
    history.setZeroDayFloat("5055");
    history.setOneDayFloat("852000");
    history.setFloatAdjusted("27555");
    history.setTwoOrMoreDayFloat("5000");
    history.setThreeOrMoreDayFloat("854955");
    history.setAdjustmentToBal("1000");
    history.setAvgAdjustmentToBalMTD("52055");
    history.setAvgAdjustmentToBalYTD("857500");
    history.setFourDayFloat("1055");
    history.setFiveDayFloat("54900");
    history.setSixDayFloat("85055");
    history.setAvgOneDayFloatMTD("22000");
    history.setAvgOneDayFloatYTD("57555");
    history.setAvgTwoDayFloatMTD("854900");
    history.setAvgTwoDayFloatYTD("1055");
    history.setTransferCalculation("5000");
    history.setTargetBalDeficiency("852955");
    history.setTotalFundingRequirement("27500");
    
    history.setLocale(account.getLocale());
    history.setDateFormat(account.getDateFormat());
    
    return histories;
  }
  
  public AccountSummaries getSummary(Account account, Calendar start, Calendar end, HashMap extra)
    throws BSException
  {
    if (account == null) {
      throw new BSException(1011);
    }
    
    Locale locale = account.getLocale();
    DateTime summaryDate = start != null ? new DateTime(start, locale) : new DateTime(locale);
    


    AccountSummaries summaries = new AccountSummaries();
    
    int group = account.getAccountGroup();
    int sysType = Account.getAccountSystemTypeFromGroup(group);
    int accountType = account.getTypeValue();
    
    AccountSummary summary = new AccountSummary();
    
    if (sysType == 2) {
      AssetAcctSummary assetSummary = new AssetAcctSummary();
      
      assetSummary.setBookValue(new Currency("3370", account.getCurrencyCode(), account.getLocale()));
      assetSummary.setMarketValue(new Currency("700", account.getCurrencyCode(), account.getLocale()));
      
      summary = assetSummary;
    } else if ((sysType == 1) || (accountType == 12)) {
      DepositAcctSummary depositSummary = new DepositAcctSummary();
      
      depositSummary.setTotalCredits(new Currency("52174", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalCreditAmtMTD(new Currency("500", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setCreditsNotDetailed(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setDepositsSubjectToFloat(new Currency("5370", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalAdjCreditsYTD(new Currency("2516", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalLockboxDeposits(new Currency("540", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebits(new Currency("52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebitAmtMTD(new Currency("733", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTodaysTotalDebits(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebitsLessWireAndCharge(new Currency("5370", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalAdjDebitsYTD(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebitsExcludeReturns(new Currency("500", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setImmedAvailAmt(new Currency("52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setOneDayAvailAmt(new Currency("507", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setMoreThanOneDayAvailAmt(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setAvailOverdraft(new Currency("5370", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setRestrictedCash(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setAccruedInterest(new Currency("450", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setAccruedDividend(new Currency("52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalOverdraftAmt(new Currency("400", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setNextOverdraftPmtDate(summaryDate);
      depositSummary.setInterestRate(1.2004F);
      depositSummary.setOpeningLedger(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setClosingLedger(new Currency("-501", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setCurrentAvailBal(new Currency("-52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setLedgerBal(new Currency("500", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setOneDayFloat(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTwoDayFloat(new Currency("5375", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalFloat(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setCurrentLedger(new Currency("52100", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setInterestYTD(new Currency("352", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setPriorYearInterest(new Currency("350", account.getCurrencyCode(), account.getLocale()));
      
      summary = depositSummary;
    } else if (sysType == 3) {
      LoanAcctSummary loanSummary = new LoanAcctSummary();
      
      loanSummary.setAvailCredit(new Currency("-5050", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setAmtDue(new Currency("370", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setInterestRate(5.4444F);
      loanSummary.setDueDate(summaryDate);
      loanSummary.setMaturityDate(summaryDate);
      loanSummary.setAccruedInterest(new Currency("911", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setOpeningBal(new Currency("700", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setCollateralDescription("Beach House");
      loanSummary.setPrincipalPastDue(new Currency("70045", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setInterestPastDue(new Currency("737", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setLateFees(new Currency("7035", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setNextPrincipalAmt(new Currency("700", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setNextInterestAmt(new Currency("7370", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setOpenDate(summaryDate);
      loanSummary.setCurrentBalance(new Currency("4000", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setNextPaymentDate(summaryDate);
      loanSummary.setNextPaymentAmt(new Currency("1050", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setInterestYTD(new Currency("200", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setPriorYearInterest(new Currency("180", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setLoanTerm("1");
      loanSummary.setTodaysPayoff(new Currency("50", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setPayoffGoodThru(summaryDate);
      
      summary = loanSummary;
    } else if (sysType == 4) {
      CreditCardAcctSummary ccSummary = new CreditCardAcctSummary();
      
      ccSummary.setAvailCredit(new Currency("50370", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setAmtDue(new Currency("50", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setInterestRate(10.766F);
      ccSummary.setDueDate(summaryDate);
      ccSummary.setCardHolderName("Brad Bowman");
      ccSummary.setCardExpDate(summaryDate);
      ccSummary.setCreditLimit(new Currency("5800", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setLastPaymentAmt(new Currency("7370", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setNextPaymentMinAmt(new Currency("6600", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setLastPaymentDate(summaryDate);
      ccSummary.setNextPaymentDue(summaryDate);
      ccSummary.setCurrentBalance(new Currency("43700", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setLastAdvanceDate(summaryDate);
      ccSummary.setLastAdvanceAmt(new Currency("2000", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setPayoffAmount(new Currency("6600", account.getCurrencyCode(), account.getLocale()));
      
      summary = ccSummary;
    }
    

    summary.setAccountNumber(account.getNumber());
    summary.setAccountID(account.getID());
    summary.setBankID(account.getBankID());
    summary.setRoutingNumber(account.getRoutingNum());
    
    summary.setSummaryDate(summaryDate);
    summary.setValueDate(summaryDate);
    
    summary.setLocale(account.getLocale());
    summary.setDateFormat(account.getDateFormat());
    
    summaries.add(summary);
    
    return summaries;
  }
  
  public ExtendedAccountSummaries getExtendedSummary(Account account, Calendar start, Calendar end, HashMap extra)
    throws BSException
  {
    if (account == null) {
      throw new BSException(1011);
    }
    
    Locale locale = account.getLocale();
    DateTime summaryDate = start != null ? new DateTime(start, locale) : new DateTime(locale);
    


    ExtendedAccountSummaries summaries = new ExtendedAccountSummaries();
    ExtendedAccountSummary summary = summaries.create();
    
    summary.setAccountNumber("10110001902600");
    //summary.setAccountNumber(account.getNumber());
    summary.setAccountID(account.getID());
    summary.setBankID(account.getBankID());
    summary.setRoutingNumber(account.getRoutingNum());
    summary.setSummaryDate(summaryDate);
    
    summary.setImmedAvailAmt(new Currency("500", account.getCurrencyCode(), account.getLocale()));
    summary.setOneDayAvailAmt(new Currency("1055", account.getCurrencyCode(), account.getLocale()));
    summary.setMoreThanOneDayAvailAmt(new Currency("10100", account.getCurrencyCode(), account.getLocale()));
    summary.setValueDateTime(summaryDate);
    summary.setAmt(new Currency("350075", account.getCurrencyCode(), account.getLocale()));
    summary.setSummaryType(1);
    
    summary.setLocale(account.getLocale());
    summary.setDateFormat(account.getDateFormat());
    
    return summaries;
  }
  

  public void addBankAccount(Account account)
    throws BSException
  {}
  

  public void deleteBankAccount(Account account)
    throws BSException
  {}
  

  public void updateBankAccount(Account account)
    throws BSException
  {
    bankingBackend.updateAccount(account);
  }
  


  public void getBankAccount(Account account)
    throws BSException
  {}
  

  public AccountHistories getHistory(Accounts accounts, Calendar start, Calendar end, String batchSize, HashMap extra)
    throws BSException
  {
    throw new BSException(13, "getHistory API not supported.");
  }
  
  public AccountSummaries getSummary(Accounts accounts, Calendar start, Calendar end, String batchSize, HashMap extra)
    throws BSException
  {
    AccountSummaries summaries = new AccountSummaries();
    for (int i = 0; i < accounts.size(); i++) {
      Account account = (Account)accounts.get(i);
      AccountSummaries sum = createSummary(account, start, end, extra);
      summaries.addAll(sum);
    }
    return summaries;
  }
  
  private AccountSummaries createSummary(Account account, Calendar start, Calendar end, HashMap extra) throws BSException
  {
    if (account == null) {
      throw new BSException(1011);
    }
    
    Locale locale = account.getLocale();
    DateTime summaryDate = start != null ? new DateTime(start, locale) : new DateTime(locale);
    


    AccountSummaries summaries = new AccountSummaries();
    
    int group = account.getAccountGroup();
    int sysType = Account.getAccountSystemTypeFromGroup(group);
    int accountType = account.getTypeValue();
    
    AccountSummary summary = new AccountSummary();
    
    if (sysType == 2) {
      AssetAcctSummary assetSummary = new AssetAcctSummary();
      
      assetSummary.setBookValue(new Currency("3370", account.getCurrencyCode(), account.getLocale()));
      assetSummary.setMarketValue(new Currency("700", account.getCurrencyCode(), account.getLocale()));
      
      summary = assetSummary;
    } else if ((sysType == 1) || (accountType == 12)) {
      DepositAcctSummary depositSummary = new DepositAcctSummary();
      
      depositSummary.setTotalCredits(new Currency("52174", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalCreditAmtMTD(new Currency("500", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setCreditsNotDetailed(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setDepositsSubjectToFloat(new Currency("5370", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalAdjCreditsYTD(new Currency("2516", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalLockboxDeposits(new Currency("540", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebits(new Currency("52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebitAmtMTD(new Currency("733", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTodaysTotalDebits(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebitsLessWireAndCharge(new Currency("5370", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalAdjDebitsYTD(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalDebitsExcludeReturns(new Currency("500", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setImmedAvailAmt(new Currency("52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setOneDayAvailAmt(new Currency("507", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setMoreThanOneDayAvailAmt(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setAvailOverdraft(new Currency("5370", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setRestrictedCash(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setAccruedInterest(new Currency("450", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setAccruedDividend(new Currency("52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalOverdraftAmt(new Currency("400", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setNextOverdraftPmtDate(summaryDate);
      depositSummary.setInterestRate(1.2004F);
      depositSummary.setOpeningLedger(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setClosingLedger(new Currency("-501", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setCurrentAvailBal(new Currency("-52310", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setLedgerBal(new Currency("500", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setOneDayFloat(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTwoDayFloat(new Currency("5375", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setTotalFloat(new Currency("2510", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setCurrentLedger(new Currency("52100", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setInterestYTD(new Currency("352", account.getCurrencyCode(), account.getLocale()));
      depositSummary.setPriorYearInterest(new Currency("350", account.getCurrencyCode(), account.getLocale()));
      
      summary = depositSummary;
    } else if (sysType == 3) {
      LoanAcctSummary loanSummary = new LoanAcctSummary();
      
      loanSummary.setAvailCredit(new Currency("-5050", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setAmtDue(new Currency("370", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setInterestRate(5.4444F);
      loanSummary.setDueDate(summaryDate);
      loanSummary.setMaturityDate(summaryDate);
      loanSummary.setAccruedInterest(new Currency("911", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setOpeningBal(new Currency("700", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setCollateralDescription("Beach House");
      loanSummary.setPrincipalPastDue(new Currency("70045", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setInterestPastDue(new Currency("737", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setLateFees(new Currency("7035", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setNextPrincipalAmt(new Currency("700", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setNextInterestAmt(new Currency("7370", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setOpenDate(summaryDate);
      loanSummary.setCurrentBalance(new Currency("4000", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setNextPaymentDate(summaryDate);
      loanSummary.setNextPaymentAmt(new Currency("1050", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setInterestYTD(new Currency("200", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setPriorYearInterest(new Currency("180", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setLoanTerm("1");
      loanSummary.setTodaysPayoff(new Currency("50", account.getCurrencyCode(), account.getLocale()));
      loanSummary.setPayoffGoodThru(summaryDate);
      
      summary = loanSummary;
    } else if (sysType == 4) {
      CreditCardAcctSummary ccSummary = new CreditCardAcctSummary();
      
      ccSummary.setAvailCredit(new Currency("50370", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setAmtDue(new Currency("50", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setInterestRate(10.766F);
      ccSummary.setDueDate(summaryDate);
      ccSummary.setCardHolderName("Brad Bowman");
      ccSummary.setCardExpDate(summaryDate);
      ccSummary.setCreditLimit(new Currency("5800", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setLastPaymentAmt(new Currency("7370", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setNextPaymentMinAmt(new Currency("6600", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setLastPaymentDate(summaryDate);
      ccSummary.setNextPaymentDue(summaryDate);
      ccSummary.setCurrentBalance(new Currency("43700", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setLastAdvanceDate(summaryDate);
      ccSummary.setLastAdvanceAmt(new Currency("2000", account.getCurrencyCode(), account.getLocale()));
      ccSummary.setPayoffAmount(new Currency("6600", account.getCurrencyCode(), account.getLocale()));
      
      summary = ccSummary;
    }
    
    summary.setAccountNumber("10110001902600");
    //summary.setAccountNumber(account.getNumber());
    summary.setAccountID(account.getID());
    summary.setBankID(account.getBankID());
    summary.setRoutingNumber(account.getRoutingNum());
    
    summary.setSummaryDate(summaryDate);
    summary.setValueDate(summaryDate);
    
    summary.setLocale(account.getLocale());
    summary.setDateFormat(account.getDateFormat());
    
    summaries.add(summary);
    
    return summaries;
  }
  
  public String getBankingBackendType() {
    return "banksim";
  }
  
  public void close() {}
}

/* Location:           C:\DEV\Eclipse_Neon\OCB 8.3 SP2 Platform Workspace\cnf\ocb\com.sap.banking.banksim\com.sap.banking.banksim-8.3.2.jar
 * Qualified Name:     com.ffusion.banksim.proxy.RealTimeBankingBackendImpl
 * Java Class Version: 7 (51.0)
 * JD-Core Version:    0.7.1
 */