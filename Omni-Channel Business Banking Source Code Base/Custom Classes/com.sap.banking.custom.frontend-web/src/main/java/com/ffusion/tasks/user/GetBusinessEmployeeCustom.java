package com.ffusion.tasks.user;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.user.BusinessEmployee;
import com.ffusion.beans.user.BusinessEmployees;
import com.ffusion.beans.user.User;
import com.ffusion.csil.CSILException;
import com.ffusion.struts.util.spring.SpringUtil;
import com.ffusion.tasks.BaseTask;
import com.ffusion.tasks.MapError;
import com.sap.banking.custom.common.utils.webservices.WebserviceUtils;
import com.sap.banking.user.bo.interfaces.BusinessUserAdmin;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

public class GetBusinessEmployeeCustom extends BaseTask implements UserTask {

	  private String _profileID = null;
	  private String searchEntitlementProfile = "FALSE";
	  protected String dataNotFoundURL = serviceErrorURL;
	  
	  public String process(HttpServlet servlet, HttpServletRequest req, HttpServletResponse res)
	  {
	    BusinessUserAdmin userAdminBO = (BusinessUserAdmin)SpringUtil.getBean(BusinessUserAdmin.class);
	    HttpSession session = req.getSession();
	    String nextURL = taskErrorURL;
	    
	    error = 0;
	    Map<String, Object> extra = new HashMap();
	    


	    SecureUser sUser = (SecureUser)session.getAttribute("SecureUser");
	    BusinessEmployee employee = new BusinessEmployee();
	    employee.setId(_profileID);
	    employee.set("BANK_ID", sUser.getBankID());
	    

	    extra.put("ENTITLEMENTPROFILE", new Boolean(searchEntitlementProfile.equalsIgnoreCase("true")));
	    


	    BusinessEmployees employees = null;
	    try
	    {
	      employees = userAdminBO.getBusinessEmployees(sUser, employee, extra);
	    } catch (CSILException e) {
	      error = MapError.mapError(e);
	      nextURL = serviceErrorURL;
	    }
	    
	    employee = employees.getByID(String.valueOf(_profileID));
	    

	    if ((error == 0) && (employee == null)) {
	      error = 3009;
	    }
	    

	    if (error == 0)
	    {
	      String _centerStateEnabled = System.getProperty("centerstate.customization.enabled");
	      if(_centerStateEnabled != null && "true".equals(_centerStateEnabled.toLowerCase())){
		     User _user = WebserviceUtils.processCustomer();
		     if(_user != null){
		    	 employee.setFirstName(_user.getFirstName()); 
		    	 employee.setLastName(_user.getLastName());
		    	 employee.setStreet(_user.getStreet());
		    	 employee.setStreet2(_user.getStreet2());
		    	 employee.setStreet3(_user.getStreet3());
		    	 employee.setStreet(_user.getStreet());
		    	 employee.setCustId(_user.getCustId());		    	
		    	 
		    	 session.setAttribute("CustomerInquiry", _user);
		      }
	      }

	      session.setAttribute("BusinessEmployee", employee);
	      

	      nextURL = successURL;
	    } else if (error == 3009)
	    {
	      if ((dataNotFoundURL != null) && (dataNotFoundURL.length() != 0)) {
	        nextURL = dataNotFoundURL;
	      }
	    }
	    
	    return nextURL;
	  }
	  
	  public void setProfileId(String profileID)
	  {
	    _profileID = profileID;
	  }
	  
	  public void setSearchEntitlementProfile(String searchEntProfile)
	  {
	    searchEntitlementProfile = searchEntProfile;
	  }

}
