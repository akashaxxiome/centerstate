package com.ffusion.struts.utils;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.util.KeyValue;
import com.ffusion.csil.CSILException;
import com.ffusion.struts.util.spring.SpringUtil;
import com.ffusion.tasks.BaseTask;
import com.ffusion.tasks.MapError;
import com.ffusion.tasks.user.UserTask;
import com.ffusion.util.ResourceUtil;
import com.ffusion.util.XMLTag;
import com.ffusion.util.logging.DebugLog;
import com.sap.banking.user.bo.interfaces.BusinessUserAdmin;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class GetNavigationSettingsCustom 
extends BaseTask
  implements UserTask
{
  public static final String RESOURCE_BUNDLE = "com.ffusion.beansresources.user.resources";
  public static final String MENU_TAB_PREFIX = "MENU_TAB.";
  public static final String DISPLAYED_MENU_TABS = "DisplayedMenuTabs";
  public static final String HIDDEN_MENU_TABS = "HiddenMenuTabs";
  public static final String START_PAGE = "StartPage";
  public static final String MENU_TAB_HOME = "home";
  public static final String MENU_TAB_ACCT_MGMT = "acctMgmt";
  public static final String MENU_TAB_CASH_MGMT = "cashMgmt";
  public static final String MENU_TAB_PMT_TRN = "pmtTran";
  public static final String MENU_TAB_ADMIN = "admin";
  public static final String MENU_TAB_APPROVALS = "approvals";
  public static final String MENU_TAB_SERVICE_CENTER = "serviceCenter";
  public static final String MENU_TAB_REPORTING = "reporting";
  public static final String MENU_TAB_PREFERENCES = "preferences";
  public static final String MENU_TAB_CONTACTUS = "contactus";
  private String[] consumerMenuTabs = { "home", "acctMgmt", "cashMgmt", "pmtTran", "serviceCenter", "preferences", "contactus"  };
  
  private String[] corporateMenuTabs = { "home", "acctMgmt", "cashMgmt", "pmtTran", "admin", "approvals", "reporting", "preferences", "contactus" };
  

  private ArrayList entitledMenuTabs = new ArrayList();


  public String process(HttpServlet servlet, HttpServletRequest req, HttpServletResponse res)
    throws IOException
  {
    BusinessUserAdmin userAdminBO = (BusinessUserAdmin)SpringUtil.getBean(BusinessUserAdmin.class);
    HttpSession session = req.getSession();
    String additionalSettingsData = null;
    error = 0;
    
    try
    {
      Locale locale = (Locale)session.getAttribute("java.util.Locale");
      
      String namespace = "NAVIGATION_SETTINGS";
      
      String[] allMenuTabs = null;
      
      SecureUser sUser = (SecureUser)session.getAttribute("SecureUser");
      if ((sUser.getAppType() != null) && (sUser.getAppType().equals("Business"))) {
        allMenuTabs = corporateMenuTabs;
      }
      else {
        allMenuTabs = consumerMenuTabs;
      }
      
      additionalSettingsData = userAdminBO.getAdditionalData(sUser, namespace, null);
      
      if ((additionalSettingsData != null) && (additionalSettingsData.length() > 0))
      {
        XMLTag data = new XMLTag();
        data.build(additionalSettingsData);
        
        XMLTag startPage = data.getContainedTag("START_PAGE");
        

        if (entitledMenuTabs.contains(startPage.getTagContent())) {
          session.setAttribute("StartPage", startPage.getTagContent());
        }
        else {
          session.setAttribute("StartPage", "home");
        }
        
        ArrayList displayedMenuTabs = new ArrayList();
        XMLTag xmlMenuTabs = data.getContainedTag("MENU_TABS");
        Iterator it = xmlMenuTabs.getContainedTagList().iterator();
        while (it.hasNext())
        {
          String tagID = ((XMLTag)it.next()).getTagContent();
          if (entitledMenuTabs.contains(tagID))
            displayedMenuTabs.add(getKeyValue(tagID, locale));
        }
        session.setAttribute("DisplayedMenuTabs", displayedMenuTabs);
        
        ArrayList hiddenMenuTabs = new ArrayList();
        for (int i = 0; i < allMenuTabs.length; i++)
        {
          boolean displayed = false;
          it = displayedMenuTabs.iterator();
          while (it.hasNext())
          {
            if (((KeyValue)it.next()).getKey().equals(allMenuTabs[i]))
            {
              displayed = true;
            }
          }
          

          if (!displayed)
          {
            if (entitledMenuTabs.contains(allMenuTabs[i]))
              hiddenMenuTabs.add(getKeyValue(allMenuTabs[i], locale));
          }
        }
        session.setAttribute("HiddenMenuTabs", hiddenMenuTabs);
      }
      else
      {
        session.setAttribute("StartPage", "home");
        session.setAttribute("DisplayedMenuTabs", getDefaultDisplayedTabs(locale, allMenuTabs));
        session.setAttribute("HiddenMenuTabs", new ArrayList());
      }
    }
    catch (CSILException ex)
    {
      error = MapError.mapError(ex);
      return serviceErrorURL;
    }
    catch (Throwable t)
    {
      DebugLog.throwing("GetNavigationSettings Task Exception: ", t);
      error = 3002;
      return serviceErrorURL;
    }
    
    return successURL;
  }
  

  public void setEntitledMenuTab(String tabId)
  {
    entitledMenuTabs.add(tabId);
  }
  

  protected KeyValue getKeyValue(String key, Locale locale)
  {
    return new KeyValue(key, ResourceUtil.getString("MENU_TAB." + key, "com.ffusion.beansresources.user.resources", locale));
  }
  

  protected ArrayList getDefaultDisplayedTabs(Locale locale, String[] allMenuTabs)
  {
    ArrayList tabs = new ArrayList();
    
    for (int i = 0; i < allMenuTabs.length; i++)
    {
      if (entitledMenuTabs.contains(allMenuTabs[i])) {
    	  tabs.add(getKeyValue(allMenuTabs[i], locale));
      }else{
    	  if(allMenuTabs[i]=="contactus"){
    		  tabs.add(new KeyValue(allMenuTabs[i],allMenuTabs[i])); 
    	  }  
      }
    }
    return tabs;
  }

}

/* Location:           C:\SVN\CS\Sprint1\DesarrolloCS-S1\cb\WebContent\WEB-INF\lib\com.sap.banking.frontend-web-8.3.2.jar
 * Qualified Name:     com.ffusion.tasks.user.GetNavigationSettings
 * Java Class Version: 7 (51.0)
 * JD-Core Version:    0.7.1
 */
