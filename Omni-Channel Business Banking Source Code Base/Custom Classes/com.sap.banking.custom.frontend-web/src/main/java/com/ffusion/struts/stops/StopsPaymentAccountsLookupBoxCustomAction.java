package com.ffusion.struts.stops;

import com.ffusion.beans.LookupBoxOption;
import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.ffusion.struts.common.AccountLookupBoxAction;
import com.ffusion.struts.util.spring.SpringUtil;
import com.ffusion.util.FilteredList;
import com.sap.banking.account.bo.interfaces.BankingAccounts;
import com.sap.banking.account.exception.AccountException;
import com.sap.banking.accountconfig.beans.AccountSearchCriteria;
import com.sap.banking.common.beans.paging.PagedResult;
import com.sap.banking.common.beans.paging.SortInfo;
import com.sap.banking.custom.common.utils.accountsutils.AccountTypeManagerUtils;
import com.sap.banking.stops.bo.interfaces.StopPaymentAccount;
import com.sap.banking.stops.exception.StopPaymentException;
import com.sap.banking.stopsconfig.bo.interfaces.StopsResourceConfig;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;













public class StopsPaymentAccountsLookupBoxCustomAction
  extends AccountLookupBoxAction
{
  protected AccountSearchCriteria accountSearchCriteria;
  @Autowired
  protected StopPaymentAccount stopPaymentAccountBO;
  @Autowired
  StopsResourceConfig stopsResourceConfig;
  protected boolean initialized = true;
  protected String initializedError;
  
  public StopsPaymentAccountsLookupBoxCustomAction()
  {
    initAction();
  }
  

  protected void setAccountSearchCriteria(AccountSearchCriteria accountSearchCriteria) {}
  

  public String execute()
    throws Exception
  {
    if (!this.initialized) {
      return errorResponse(this.initializedError);
    }
    
    String thisMethod = "StopsPaymentAccountsLookupBoxAction.execute";
    this.logger.debug("Entering " + thisMethod);
    try {
      HttpServletRequest request = getServletRequest();
      HttpSession session = request.getSession();
      


      setAccountSearchCriteria(this.accountSearchCriteria);
      
      String searchText = request.getParameter("term") != null ? request.getParameter("term") : "";
      
      SecureUser sUser = (SecureUser)session.getAttribute("SecureUser");
      Map<String, Object> extra = new HashMap();
      
      this.accountSearchCriteria.setSearchString(searchText);
      
      super.updatePageInfo(this.accountSearchCriteria);
      
      PagedResult<Account> pagedAccounts = this.stopPaymentAccountBO.searchStopPaymentAccounts(getSecureUser(), this.accountSearchCriteria, null);
      
      
      
      super.updatePageCounts(pagedAccounts.getPageInfo());
      
      FilteredList filteredAccounts = new FilteredList(AccountTypeManagerUtils.filterDespositAccounts(pagedAccounts.getPagedList()));
      
      //AccountTypeManagerUtils.filterDespositAccounts(pagedAccounts.getPagedList());
      
      setGridModel(filteredAccounts);
      

      setOptionsModel(getOptionList(filteredAccounts));
    }
    catch (StopPaymentException se) {
      this.logger.error(thisMethod, se.getLocalizedMessage());
      return errorResponse(se);
    } catch (Exception e) {
      this.logger.error(thisMethod, e.getLocalizedMessage());
      return errorResponse(e);
    }
    this.logger.debug("Leaving " + thisMethod);
    return "success";
  }
  
  protected SortInfo getSortingInfo() {
    SortInfo info = new SortInfo();
    String sortingIndex = getSidx();
    String sortOrderStr = getSord();
    if ((sortingIndex == null) || (sortingIndex.trim().length() == 0))
    {
      info.setSortColumnName("RoutingNum");
      info.setIsAscending(Boolean.TRUE);
      return info;
    }
    info.setSortColumnName(sortingIndex);
    
    if ("desc".equals(sortOrderStr)) {
      info.setIsAscending(Boolean.FALSE);
    }
    else
    {
      info.setIsAscending(Boolean.TRUE);
    }
    return info;
  }
  





  protected FilteredList getOptionList(List<Account> accounts)
  {
    FilteredList optionsList = new FilteredList();
    Iterator<Account> i = accounts.iterator();
    String displayText = null;
    while (i.hasNext()) {
      Account acc = (Account)i.next();
      String id = buildDropdownOptionValue(acc) + "," + acc.getCurrencyCode();
      displayText = acc.getSearchDisplayText();
      LookupBoxOption option = new LookupBoxOption(id, displayText, displayText);
      optionsList.add(option);
    }
    return optionsList;
  }
  



  protected String buildDropdownOptionValue(Account account)
  {
    return account.getID();
  }
  


  protected void doFilter(FilteredList collection)
    throws Exception
  {
    String thisMethod = "StopsPaymentAccountsLookupBoxAction.doFilter";
  }
  










  protected String getErrorDescription(String errorType, int errorCode)
  {
    return this.stopsResourceConfig.getErrorDescription(getSecureUser(), errorType, errorCode);
  }
  
  private void initAction()
  {
    BankingAccounts bankingAccountsBO = (BankingAccounts)SpringUtil.getBean(BankingAccounts.class);
    Map<String, Object> extra = new HashMap();
    try {
      this.accountSearchCriteria = bankingAccountsBO.getDefaultAccountSearchCriteria(getSecureUser(), "accountSearchCriteria", extra);
    } catch (AccountException accountException) {
      this.logger.error(accountException.getLocalizedMessage(), accountException);
      this.initialized = false;
      this.initializedError = getServiceNotAvailableMessage(accountException);
    } catch (Throwable e) {
      this.logger.error(e.getLocalizedMessage(), e);
      this.initialized = false;
      this.initializedError = getServiceNotAvailableMessage(e);
    }
  }
}

