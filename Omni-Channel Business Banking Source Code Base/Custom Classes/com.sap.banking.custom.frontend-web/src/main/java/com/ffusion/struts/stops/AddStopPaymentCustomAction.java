package com.ffusion.struts.stops;

import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.accounts.Accounts;
import com.ffusion.beans.messages.Message;
import com.ffusion.beans.stoppayments.StopCheck;
import com.ffusion.efs.adapters.profile.interfaces.MessageAdapter;
import com.ffusion.util.ResourceUtil;
import com.sap.banking.common.beans.ValidationResult;
import com.sap.banking.custom.common.utils.webservices.WebserviceUtils;
import com.sap.banking.stops.bo.interfaces.StopPaymentAccount;
import com.sap.banking.stops.bo.interfaces.Stops;

import java.util.HashMap;

import org.slf4j.Logger;









public class AddStopPaymentCustomAction
  extends StopPaymentBaseCustomAction
{
  public String init()
  {
    super.init();
    return "init";
  }
  


  public String execute()
  {
    try
    {
		//Call to the Web Service
    	boolean _responseStopPayments = WebserviceUtils.processStopPayments(stopCheck);
    	if(_responseStopPayments){
    		//Persist the stop payments in the DB just for now
        	stopsBO.addStopPayment(getSecureUser(), stopCheck, null);
    	}
    	else{
        	addActionError("Amount value is missing or invalid.");
    		return "error";
    	}
    } catch (Throwable throwable) {
      logger.error("Error:", throwable);
      return errorResponse(throwable);
    }
    return "success";
  }
  



  public void validateVerify()
  {
    logger.debug("AddStopPaymentAction.validate");
    try {
      stopCheck.setCheckNumbers(null);
      ValidationResult validationResult = validateStopPayment(stopCheck, "addStopPayment");
      if ((validationResult != null) && (validationResult.isValidationFailed())) {
        addErrors(validationResult);
      } else {
        Accounts accounts = (Accounts)stopPaymentAccountBO.getStopPaymentAccounts(getSecureUser(), null);
        if (accounts != null) {
          stopCheck.setAccount(null);
          Account theAccount = accounts.getByID(stopCheck.getAccountID());
          if (theAccount != null) {
            stopCheck.setAccount(theAccount);
            stopCheck.setCurrencyCode(theAccount.getCurrencyCode());
          }
        }
      }
    } catch (Throwable throwable) {
      logger.error("Error in validate verify:", throwable);
      addActionError(getText("common.error"));
    }
    
    if(stopCheck.getAmountValue().getIsZero() || stopCheck.getAmount().isEmpty()){
    	ValidationResult validationResultAmount = new ValidationResult();
    	validationResultAmount.setValidationFailed(true);
    	HashMap<String, String> validationErrors = new HashMap<String, String>();
    	validationErrors.put("amount", "Amount value is missing or invalid.");
		validationResultAmount.setValidationErrors(validationErrors );
    	addErrors(validationResultAmount);
    }
    
  }
  
  public String verify()
  {
    return "verify";
  }
  
  public String cancel() {
    return "none";
  }
}


/* Location:           C:\SVN\CS\GIT\centerstate\Omni-Channel Business Banking Source Code Base\Business Banking Web Application\cb\WebContent\WEB-INF\lib\com.sap.banking.frontend-web-8.3.2.jar
 * Qualified Name:     com.ffusion.struts.stops.AddStopPaymentAction
 * Java Class Version: 7 (51.0)
 * JD-Core Version:    0.7.1
 */