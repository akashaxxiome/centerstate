package com.ffusion.struts.account;

import com.ffusion.beans.banking.Transaction;
import com.ffusion.beans.banking.Transactions;
import com.ffusion.util.FilteredList;
import com.sap.banking.account.bo.interfaces.DepositAccount;
import com.sap.banking.accountconfig.beans.AccountHistorySearchCriteria;
import com.sap.banking.common.beans.paging.PagedResult;
import com.sap.banking.custom.common.utils.webservices.WebserviceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;

public class GetDepositAccountHistoryCustomAction extends AccountHistoryBaseAction {

	protected List<Transaction> gridModel;
	  
	  public String execute()
	  {
	    logger.debug("Entering GetDepositAccountHistoryAction.execute");
	    try {
	      HttpServletRequest request = getServletRequest();
	      HttpSession session = request.getSession();
	      String collectionName = request.getParameter("collectionName");
	      String operationFlag = request.getParameter("operationFlag");
	      Map<String, Object> extra = new HashMap();
	      String selectedAccountOption = accountHistorySearchCriteria.getSelectedAccountOption();
	      

	      if ("EXPORT".equals(operationFlag)) {
	        return "none";
	      }
	      

	      accountHistorySearchCriteria.setAccount(getAccountDetailsForSearch(selectedAccountOption, extra));
	      accountHistorySearchCriteria.setAccounts(null);
	      
	      updatePageInfo(accountHistorySearchCriteria);
	      PagedResult<Transaction> pagedTransaction = depositAccountBO.getTransactions(secureUser, accountHistorySearchCriteria, extra);
	      
	      String _centerStateEnabled = System.getProperty("centerstate.customization.enabled");
	      if(_centerStateEnabled != null && "true".equals(_centerStateEnabled.toLowerCase())){
	    	 pagedTransaction = new PagedResult<Transaction>();  
	         Transactions _transactions = WebserviceUtils.processTransactions(getAccountDetailsForSearch(selectedAccountOption, extra)); //TODO Check Account is the correct one
	         if(_transactions != null){
	        	pagedTransaction.addAll(_transactions);
	         }
	      }
	      
	      FilteredList transactions = new FilteredList(pagedTransaction.getPagedList());
	      updatePageCounts(pagedTransaction.getPageInfo());
	      
	      transactions.setLocale((Locale)session.getAttribute("java.util.Locale"));
	      

	      setGridModel(transactions);
	      updateDynamicURLs(transactions);
	      

	      session.setAttribute(collectionName, transactions);
	    } catch (Throwable throwable) {
	      logger.error(throwable.getLocalizedMessage(), throwable);
	      return errorResponse(throwable);
	    }
	    logger.debug("Leaving GetDepositAccountHistoryAction.execute");
	    return "success";
	  }
	  


	  public String getJSON()
	    throws Exception
	  {
	    return "success";
	  }
	  
	  public void setGridModel(List<Transaction> gridModel)
	  {
	    this.gridModel = gridModel;
	  }
	  
	  public List<Transaction> getGridModel() {
	    return gridModel;
	  }
}
