 package com.ffusion.struts.customAuthentication;
 
 import com.ffusion.beans.SecureUser;
 import com.ffusion.beans.bankemployee.BankEmployee;
 import com.ffusion.csil.CSILException;
 import com.ffusion.beans.user.User;
 import com.ffusion.csil.handlers.util.SignonSettings;
 import com.ffusion.struts.authentication.Authenticate;
 import com.ffusion.struts.authentication.AuthenticationBaseResourceAction;
 import com.ffusion.struts.util.spring.SpringUtil;
 import com.ffusion.tasks.MapError;
import com.ibm.db2.jcc.am.se;
import com.sap.banking.authentication.beans.MultiFactorInfoStatus;
 import com.sap.banking.authentication.beans.MultifactorInfo;
 import com.sap.banking.authentication.beans.MultifactorStatus;
 import com.sap.banking.authentication.bo.interfaces.Authentication;
 import com.sap.banking.authentication.exceptions.AuthenticationException;
 import com.sap.banking.authenticationconfig.bo.interfaces.AuthenticationValidationConfig;
 import com.sap.banking.authenticationconfig.exception.AuthenticationConfigException;
 import com.sap.banking.common.beans.ValidationResult;
 import com.sap.banking.common.exception.BLException;
 import com.sap.tc.logging.ConsoleLog;

 import java.util.ArrayList;
 import java.util.Enumeration;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Locale;
 import java.util.Map;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpSession;
 import org.apache.commons.lang.StringUtils;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import org.springframework.beans.factory.annotation.Autowired;
 
 
 
 
 public class customAuthenticate
   extends AuthenticationBaseResourceAction
 {
   private static final Logger logger = LoggerFactory.getLogger(Authenticate.class);
   
 
   @Autowired
   public SecureUser secureUser;
   
 
   @Autowired
   private Authentication authentication;
   
 
   @Autowired
   private com.sap.banking.business.bo.interfaces.Business businessBO;
   
 
   @Autowired
   private AuthenticationValidationConfig authenticationValidationConfigBO;
   
   public int authType = 1;
   
 
   private static Map<String, String> fieldNames = new HashMap();
   
 
   private static final String PWD_CHANGE_REQUIRED = "passwordChangeRequired";
   
 
   private static final String PWD_ALMOST_EXPIRED = "passwordAboutToExpire";
   
 
   private static final String PWD_QAS_REQUIRED = "passwordQAsRequired";
   
 
   private static final String TERMS_ACCEPTANCE = "needToAcceptTerms";
   
 
   private static final String CARRIER_TERMS_ACCEPTANCE = "needToAcceptCarrierTerms";
   
 
   private static final String LOGOUT = "logout";
   
 
 
   static
   {
     fieldNames.put("userName", "secureUser.userName");
     fieldNames.put("customerID", "secureUser.customerID");
     fieldNames.put("password", "secureUser.password");
     fieldNames.put("multifactorInfo", "secureUser.multifactorInfo");
     fieldNames.put("credential.challengequestion.required", "secureUser.multifactorInfo.challengequestion");
     fieldNames.put("credential.scratchcard.required", "secureUser.multifactorInfo.scratchcard");
     fieldNames.put("credential.otp.required", "secureUser.multifactorInfo.otp");
     fieldNames.put("credential.token.required", "secureUser.multifactorInfo.token");
   }
   
 
 
 
 
   public String init()
   {
     logger.debug(" ...... init called .. ");
     secureUser = ((SecureUser)SpringUtil.resetBean("SecureUser"));
     secureUser.setUserType(1);
     getServletRequest().getSession().setAttribute("User", secureUser);
     logger.debug(" ...... init Secure User and save  in the session.. ");
     return "none";
   }
   
 
 
   public void validateGetMultifactorInfo()
   {
     try
     {
       if (hasFieldErrors()) {
         clearFieldErrors();
       }
     }
     catch (Exception localException) {}
     
     ValidationResult validationResult = validateSecureUser(secureUser, "getMultifactorInfo");
     
     addFieldErrors(validationResult, fieldNames);
   }
   
 
 
 
 
   public String getMultifactorInfo()
   {	
		logger.info("This is my method!!!!");
	try
     {
       SecureUser user = authentication.initSecureUser(secureUser, null);
       secureUser.setAffiliateID	(user.getAffiliateID());
       
		secureUser.setMultifactorInfo(user.getMultifactorInfo());
       if (secureUser.getAppType() == null) {
         secureUser.setAppType(user.getAppType());
       }
       secureUser.setEntitlementID(user.getEntitlementID());
       secureUser.setBusinessID(user.getBusinessID());
       List<MultifactorInfo> multifactorInfos = user.getMultifactorInfo();
       if (multifactorInfos != null) {
         for (MultifactorInfo multifactorInfo : multifactorInfos) {
           logger.debug(" ................MultIfacrotInfo " + multifactorInfo);
         }
       }
       
       if ((secureUser.getAffiliateID() == null) || (secureUser.getAffiliateID().equalsIgnoreCase("0"))) {
         String secret = "";
         if ((secureUser.getMultifactorInfo() == null) || (secureUser.getMultifactorInfo().isEmpty())) {
           multifactorInfos = new ArrayList();
           MultifactorInfo next = new MultifactorInfo(4);
           if (secureUser.isConsumerUser()) {
             secret = "web/multilang/jsp/secretgrafx/efs/sample1.jpg";
           } else {
             secret = "web/multilang/jsp/secretgrafx/cb/sample1.jpg";
           }
           next.setChallenge(secret);
           multifactorInfos.add(next);
           secureUser.setMultifactorInfo((ArrayList)multifactorInfos);
         }
       }
       
       getServletRequest().getSession().setAttribute("appType", secureUser.getAppType());
       getServletRequest().getSession().setAttribute("User", user);
     } catch (AuthenticationException ex) {
       if (ex.isValidationFailed()) {
         addActionErrors(ex.getValidationResult());
       } else {
         addActionError("SE", MapError.mapError(ex, getServletRequest().getSession()));
       }
       return "input";
     } catch (Throwable e) {
       logger.error(e.getLocalizedMessage(), e);
       return "input";
     }
		//userName Validations
		if(secureUser.getUserName().length() < 4 || secureUser.getUserName() == null){			
			addActionError("Authentication failed - User ID must have at least four characters");
			logger.debug(" UserName Too Short LOGGER");
			return "input";
		}
		
		if (StringUtils.isAlphanumeric(secureUser.getUserName()) == false){
			addActionError("Authentication failed - User ID can't contain special characters");
			return "input";
		}
		
		/*boolean hasNonAlpha = secureUser.getUserName().matches("^.*[^a-zA-Z0-9 ].*$");
		if (hasNonAlpha == false){
			addActionError("Authentication failed - Username can't contain special characters");
			return "error";
		}*/
		
		
	
     return "success";
   }
   
 
 
   public void validateAuthenticate()
   {
     ValidationResult validationResult = validateSecureUser(secureUser, "authenticate");
     addFieldErrors(validationResult, fieldNames);
   }
   
 
 
 
 
   public String authenticate()
   {
     String ret = "success";
     HttpSession session = getServletRequest().getSession();
     
     SecureUser secureUserCache = null;
     try {
       logger.debug(" In the authenticate method ");
       
 
 
 
       HttpServletRequest request = getServletRequest();
       Object errorObj = request.getAttribute("ERROR");
       logger.debug(" Error Object in request is ::: " + errorObj);
       if ((errorObj != null) && ("true".equalsIgnoreCase(errorObj.toString()))) {
         AuthenticationException ex = (AuthenticationException)request.getAttribute("AuthenticationException");
         if (ex != null) {
           logger.debug(" Received AuthenticationException from request ");
           if ((ex.getServiceError() == 3030) || (ex.getServiceError() == 3031))
           {
             getServletRequest().getSession().setAttribute("appType", secureUser.getAppType());
             return "sessionConcurrentLoginError";
           }
           if (ex.isValidationFailed()) {
             addActionErrors(ex.getValidationResult());
           } else {
             int serviceError = ex.getServiceError();
             if (checkIfPasswordRelatedError(serviceError)) {
               secureUser.set("AUTHENTICATE", Integer.toString(serviceError));
               secureUser.getMap().put("AUTHENTICATE", Integer.toString(serviceError));
               SecureUser secureUserwithMFAINfo = authentication.getMultifactorInfo(secureUser, 1, new HashMap());
               if ((secureUserwithMFAINfo.getMultifactorInfo() != null) && (!secureUserwithMFAINfo.getMultifactorInfo().isEmpty())) {
                 secureUser.getMultifactorInfo().clear();
                 secureUser.getMultifactorInfo().addAll(secureUserwithMFAINfo.getMultifactorInfo());
               }
               
               logger.debug(" AUTHENTICATE id : " + secureUser.get("AUTHENTICATE"));
               
               if ((serviceError == 3007) || (serviceError == 3024))
               {
                 secureUser.remove("USER");
                 ret = "passwordChangeRequired";
               } else if (serviceError == 3018) {
                 secureUser.remove("USER");
                 ret = "passwordAboutToExpire";
               } else if (serviceError == 3023) {
                 secureUser.remove("USER");
                 secureUser.getMap().remove("USER");
                 ret = "passwordQAsRequired";
               }
             }
             else {
               addActionError("SE", MapError.mapError(ex, session));
             }
           }
           Boolean passwordValidations = this.validatePasswordPolicies(secureUser.getPassword());
           if(passwordValidations == true){
        	   return "input";
           }          
           if ((ret != "passwordChangeRequired") && (ret != "passwordAboutToExpire") && (ret != "passwordQAsRequired"))
           {
             return "error";
           }
         }
       }
       secureUserCache = authentication.login(secureUser, null);
       
       secureUser.set(secureUserCache);
       
 
       if (secureUserCache.getAgent() != null) {
         SecureUser agent = secureUser.getAgent();
         if (agent == null) {
           agent = secureUserCache.getAgent();
         } else if (secureUserCache.getAgent().getViewOnlyOBOValue()) {
           agent.setViewOnlyOBO(true);
         }
         secureUser.setAgent(agent);
       }
       session.setAttribute("SecureUser", secureUser);
       User user = (User)secureUser.get("USER");
       session.setAttribute("User", user);
       if (secureUser.getBusinessID() != 0)
       {
 
 
         com.ffusion.beans.business.Business business = businessBO.getBusinessById(secureUser, secureUser.getBusinessID(), new HashMap());
         session.setAttribute("Business", business);
       }
       
       if ((secureUser.getAgent() == null) && (
         (ret == "passwordChangeRequired") || (ret == "passwordAboutToExpire") || (ret == "passwordQAsRequired"))) {
         return ret;
       }
       
 
       if (secureUser.get("OBO") != null) {
         String oboLoginString = (String)secureUser.get("OBO");
         Boolean oboLogin = Boolean.FALSE;
         if ("true".equalsIgnoreCase(oboLoginString)) {
           oboLogin = Boolean.TRUE;
         }
         if (oboLogin.booleanValue()) {
           return "oboLogin";
         }
       }
     }
     catch (AuthenticationException ex) {
       if ((secureUser.getMultifactorInfo() == null) || (secureUser.getMultifactorInfo().isEmpty())) {
         try {
           SecureUser mfaSecureUser = authentication.getMultifactorInfo(secureUser, 1, null);
           if ((mfaSecureUser != null) && (mfaSecureUser.getMultifactorInfo() != null)) {
             secureUser.setMultifactorInfo(mfaSecureUser.getMultifactorInfo());
           }
         } catch (AuthenticationException e) {
           e.printStackTrace();
         }
       }
       if (ex.isValidationFailed()) {
         addActionErrors(ex.getValidationResult());
       } else {
         addActionError("SE", MapError.mapError(ex, session));
       }
       return "error";
     } catch (CSILException ce) {
       logger.error(ce.getLocalizedMessage(), ce);
       addActionError("SE", MapError.mapError(ce, session));
       return "error";
     } catch (Throwable e) {
       logger.error(e.getLocalizedMessage(), e);
       
       return "error"; }
     User user = new User();
     if ("2".equals(user.getAccountStatus())) {
       addActionError("TE", 100);
       return "error";
     }
     
 
 
 
     Integer authenticate = (Integer)secureUser.get("AUTHENTICATE");
     if (authenticate != null)
     {
 
       if (secureUser.getAgent() == null) {
         if ((authenticate.intValue() == 3007) || 
						(authenticate.intValue() == 3024)) {
           ret = "passwordChangeRequired";
         } else if (authenticate.intValue() == 3018) {
           ret = "passwordAboutToExpire";
         } else if (authenticate.intValue() == 3023) {
           ret = "passwordQAsRequired";
         } else {
           addActionError("SE", authenticate.intValue());
           return "error";
         }
       }
       
 
 
       if ((authenticate.intValue() != 3007) && (authenticate.intValue() != 3024) && 
         (authenticate.intValue() != 3018) && (authenticate.intValue() != 3023)) {
         secureUser.remove("AUTHENTICATE");
         secureUserCache.remove("AUTHENTICATE");
       }
     }
     if (("success".equals(ret)) && (secureUser.getAgent() == null) && (secureUser.getNeedToAcceptTerms())) {
       ret = "needToAcceptTerms";
     } else if (("success".equals(ret)) && (secureUser.getAgent() == null) && (!secureUser.isCarrierTermsAccepted())) {
       ret = "needToAcceptCarrierTerms";
     }
     
     return ret;
   }
   
 
 
 
 
   public String oboAuthenticate()
   {
     HashMap<String, String> params = new HashMap();
     HttpSession session = getServletRequest().getSession();
     
     init();
     addParametersToMap(params);
     
     session.setAttribute("LoginAppType", params.get("LoginAppType"));
     session.setAttribute("themeName", params.get("themeName"));
     session.setAttribute("OBOLocale", params.get("OBOLocale"));
     
     long timeStampThreshold = 0L;
     long timeStamp = 0L;
     try {
       timeStampThreshold = Long.parseLong((String)params.get("TimestampThreshold"));
     } catch (NumberFormatException nfe) {
       addActionError("TE", 100301);
       return "error";
     }
     try {
       timeStamp = Long.parseLong((String)params.get("Timestamp"), 16);
     } catch (NumberFormatException nfe) {
       timeStamp = -1L;
     }
     
 
     if ((timeStamp < 0L) || (timeStamp + timeStampThreshold < System.currentTimeMillis())) {
       addActionError("TE", 100300);
       return "error";
     }
     
     Locale locale = new Locale((String)params.get("OBOLocale"));
     try {
       HashMap extra = null;
       BankEmployee employee = new BankEmployee(locale);
       employee.setUserName((String)params.get("CSRUserName"));
       employee.setPassword((String)params.get("CSRPassword"));
       employee.setExtId((String)params.get("CSRExtId"));
       if (StringUtils.isNotBlank((String)params.get("OBOProfile"))) {
         employee.setOboProfileId(Integer.parseInt((String)params.get("OBOProfile")));
       }
       SecureUser csrSUser = new SecureUser();
       csrSUser.setUserName((String)params.get("CSRUserName"));
       csrSUser.setPassword((String)params.get("CSRPassword"));
       csrSUser.setUserType(2);
       logger.debug("OBOProfile id = " + (String)params.get("OBOProfile"));
       if (StringUtils.isNotBlank((String)params.get("OBOProfile"))) {
         csrSUser.setOboProfileId(Integer.parseInt((String)params.get("OBOProfile")));
       }
       if ("2".equals(params.get("OBOEnabled"))) {
         csrSUser.setViewOnlyOBO(true);
       }
       csrSUser.setLocale(locale);
       
       Authentication authenticationBO = (Authentication)SpringUtil.getBean(Authentication.class);
       Map map = csrSUser.getMap();
       if (map == null) {
         map = new HashMap();
         csrSUser.setMap(map);
       }
       map.put("CSRExtId", employee.getExtId());
       authenticationBO.signonBankEmployee(csrSUser, employee, extra);
       
       secureUser.setUserName((String)params.get("UserName"));
       secureUser.setPassword((String)params.get("Password"));
       secureUser.setBusinessCustId((String)params.get("CustomerID"));
       secureUser.setBankID((String)params.get("BankID"));
       secureUser.put("OBO", "true");
       secureUser.setAgent(csrSUser);
       if ((secureUser.getAppType() == null) || (secureUser.getAppType().length() > 0)) {
         secureUser.setAppType("Business");
       }
       
       return "redirectAuthenticate";
     } catch (BLException ble) {
       if (ble.isValidationFailed()) {
         addActionErrors(ble.getValidationResult());
       } else {
         addActionError("SE", MapError.mapError(ble, session));
       }
       logger.error(ble.getLocalizedMessage(), ble);
       return "error";
     } catch (Throwable e) {
       logger.error(e.getLocalizedMessage(), e);
       return errorResponse(e);
     }
   }
   
 
 
   public void validateMultifactorAuthenticate()
   {
     ValidationResult validationResult = validateSecureUser(secureUser, "authenticate");
     addFieldErrors(validationResult, fieldNames);
   }
   
 
 
 
 
   public String multiFactorAuthenticate()
   {
     String ret = "none";
     HttpSession session = getServletRequest().getSession();
     try {
       HttpServletRequest request = getServletRequest();
       Object errorObj = request.getAttribute("ERROR");
       logger.debug(" Error Object in request is ::: " + errorObj);
       MultiFactorInfoStatus multiFactorInfoStatus = authentication.getMFAStatus(secureUser, null);
       String mfaKey = multiFactorInfoStatus.getCurrentMfaKey();
       Map<String, MultifactorStatus> mfaStatusMap = multiFactorInfoStatus.getMfaStatus();
       if ((errorObj != null) && ("true".equalsIgnoreCase(errorObj.toString()))) {
         AuthenticationException ex = (AuthenticationException)request.getAttribute("AuthenticationException");
         if (ex != null) {
           if (ex.isValidationFailed()) {
             addActionErrors(ex.getValidationResult());
             return sendOutResponseMessage(getText("authentication_incorrect_credential"), 601);
           }
           
 
           if (mfaStatusMap != null) {
             MultifactorStatus mfaStatus = (MultifactorStatus)mfaStatusMap.get(mfaKey);
             if (mfaStatus.isFailed()) {
               if (mfaStatus.getFailedAttempts() >= SignonSettings.getMaxInvalidCredentialsBeforeLogout())
               {
 
                 return "logout";
               }
               addActionError("authenticate failed");
               return sendOutResponseMessage(getText("authentication_incorrect_credential"), 601);
             }
           }
           
 
 
           logger.debug(" Received AuthenticationException from request ");
           return "error";
         }
       }
     }
     catch (AuthenticationException ex) {
       logger.error(ex.getLocalizedMessage(), ex);
       addActionError("SE", MapError.mapError(ex, session));
       return "error";
     } catch (Throwable e) {
       logger.error(e.getLocalizedMessage(), e);
       return errorResponse(e);
     }
     return ret;
   }
   
 
 
   public void validateLogout()
   {
     ValidationResult validationResult = validateSecureUser(secureUser, "logout");
     addFieldErrors(validationResult, fieldNames);
   }
   
 
 
 
 
   public String logout()
   {
     String thisMethod = "Authenticate.logout";
     try {
       authentication.logout(secureUser, null);
     }
     catch (AuthenticationException ble) {
       logger.error(thisMethod, ble);
     } catch (Throwable e) {
       logger.error(e.getLocalizedMessage(), e);
       return errorResponse(e);
     }
     SpringUtil.deleteBean("SecureUser");
     return "success";
   }
   
 
 
 
 
   public int getAuthType()
   {
     return authType;
   }
   
 
 
 
 
 
   public void setAuthType(int authType)
   {
     this.authType = authType;
   }
   
 
 
 
 
   public SecureUser getSecureUser()
   {
     return secureUser;
   }
   
 
 
 
 
 
   public void setSecureUser(SecureUser secureUser)
   {
     this.secureUser = secureUser;
   }
   
 
 
 
 
 
   protected void addParametersToMap(HashMap<String, String> paramsMap)
   {
     HttpServletRequest request = getServletRequest();
     Enumeration<String> paramNames = request.getParameterNames();
     if (paramNames != null)
     {
       while (paramNames.hasMoreElements()) {
         String next = (String)paramNames.nextElement();
         if (!"CSRF_TOKEN".equals(next)) {
           paramsMap.put(next, request.getParameter(next));
         }
       }
     }
   }
   
 
 
 
 
 
 
 
 
   private ValidationResult validateSecureUser(SecureUser secureUser, String operationName)
   {
     ValidationResult validationResult = new ValidationResult();
     try
     {
       validationResult = authenticationValidationConfigBO.validateSecureUser(secureUser, operationName, "UI_VALIDATION");
     } catch (AuthenticationConfigException ex) {
       logger.error(ex.getLocalizedMessage(), ex);
       errorResponse(ex);
     }
     return validationResult;
   }
   
 
 
 
 
 
 
 
   private boolean checkIfPasswordRelatedError(int serviceError)
   {
     boolean passwordError = false;
     if ((serviceError == 3007) || (serviceError == 3024) || (serviceError == 3018) || (serviceError == 3023))
     {
       passwordError = true;
     }
     return passwordError;
   }
   
   private Boolean validatePasswordPolicies(String password){	   
	    
		//PasswordLength
		if(secureUser.getPassword().length() < 8){
			addActionError("Authentication failed - Password must contain at least 8 characters");
			return true;
		}
	   
		//Password validations
	   //Lower case
	   boolean hasLowercase = !password.equals(password.toUpperCase());
		if (hasLowercase == false){
			addActionError("Authentication failed - Password must contain at least one lower case");
			return true;
		}
		//UserName!=Password
		if (secureUser.getPassword().equals(secureUser.getUserName())){
			addActionError("Authentication failed - The UserName can't be used as Password");
			return true;
		}
	   //SpecialCharacters
		if(StringUtils.isAlphanumeric(secureUser.getPassword()) == true){			
			addActionError("Authentication failed - Password must contain at least one special character");
			return true;
		}
	   
		return false;
	}
   
 }
 
 	

/* Location:           C:\SAP\DEV\OCB 8.3 SP2 Corporate Banking Workspace\cb\WebContent\WEB-INF\lib\com.sap.banking.frontend-web-8.3.2.jar
 * Qualified Name:     com.ffusion.struts.authentication.Authenticate
 * Java Class Version: 7 (51.0)
 * JD-Core Version:    0.7.1
 */
