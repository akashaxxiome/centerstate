package com.ffusion.struts.stops;

import com.ffusion.beans.DateTime;
import com.ffusion.beans.SecureUser;
import com.ffusion.beans.accounts.Account;
import com.ffusion.beans.stoppayments.StopCheck;
import com.ffusion.beans.user.UserLocale;
import com.ffusion.struts.BaseAction;
import com.ffusion.struts.util.spring.SpringUtil;
import com.sap.banking.common.beans.ValidationResult;
import com.sap.banking.common.bo.interfaces.Util;
import com.sap.banking.stops.bo.interfaces.StopPaymentAccount;
import com.sap.banking.stops.bo.interfaces.Stops;
import com.sap.banking.stops.exception.StopPaymentException;
import com.sap.banking.stopsconfig.bo.interfaces.StopsResourceConfig;
import com.sap.banking.stopsconfig.bo.interfaces.StopsValidationConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;






public class StopPaymentBaseCustomAction
  extends BaseAction
{
  @Autowired
  protected Stops stopsBO;
  @Autowired
  protected StopPaymentAccount stopPaymentAccountBO;
  @Autowired
  protected Util utilBO;
  @Autowired
  @Qualifier("stopCheck")
  protected StopCheck stopCheck;
  @Autowired
  private StopsValidationConfig stopsValidationConfigBO;
  @Autowired
  StopsResourceConfig stopsResourceConfig;
  protected String[] stopReasons;
  protected List<Account> accountsList;
  @Autowired
  MessageSource stopWARMessageSource;
  
  public String init()
  {
    try
    {
      stopCheck = ((StopCheck)SpringUtil.resetBean("stopCheck"));
      stopCheck.setLocale(getSecureUser().getUserLocale().getLocale());
      stopCheck.getCheckDateValue().setFormat(getSecureUser().getUserLocale().getDateFormat());
      stopCheck.setDateFormat(getSecureUser().getUserLocale().getDateFormat());
    } catch (Throwable throwable) {
     logger.error("Error:", throwable);
      return errorResponse(throwable);
    }
    return "success";
  }
  


  protected StopCheck searchStopCheck(String stopCheckID)
    throws StopPaymentException
  {
    StopCheck result = stopsBO.getStopPaymentById(getSecureUser(), stopCheckID, null);
    return result;
  }
  


  public String[] getStopReasons()
  {
    if ((stopReasons == null) || (stopReasons.length == 0)) {
      try
      {
        stopReasons = utilBO.getResourceValues(getSecureUser(), "com.ffusion.beansresources.stoppayments.resources", new String[] { "StopPaymentReason0", "StopPaymentReason1", "StopPaymentReason2", "StopPaymentReason3","StopPaymentReason4" }, new HashMap());


      }
      catch (Throwable e)
      {


        logger.error("Error in processing delete Stop Check task", e);
        
       errorResponse(e);
        stopReasons = new String[0];
      }
    }
    return stopReasons;
  }
  
  protected ValidationResult validateStopPayment(StopCheck stopCheck, String operationName) {
    ValidationResult validationResult = new ValidationResult();
    try {
      validationResult = stopsValidationConfigBO.validateStopPayment(getSecureUser(), stopCheck, operationName, "UI_VALIDATION");
      
      ValidationResult stopReasonValidationResult = stopsBO.validateStopsReason(getSecureUser(), stopCheck, null);
      
      if ((stopReasonValidationResult != null) && (stopReasonValidationResult.isValidationFailed())) {
        validationResult.getValidationErrors().putAll(stopReasonValidationResult.getValidationErrors());
        validationResult.getErrorDetails().putAll(stopReasonValidationResult.getErrorDetails());
      }
    }
    catch (Throwable throwable) {
      logger.error("Error in validate verify:", throwable);
      errorResponse(throwable);
    }
    return validationResult;
  }
  










  protected String getErrorDescription(String errorType, int errorCode)
  {
    return stopsResourceConfig.getErrorDescription(getSecureUser(), errorType, errorCode);
  }
  


  public void setStopReasons(String[] stopReasons)
  {
    this.stopReasons = stopReasons;
  }
  


  public StopCheck getStopCheck()
  {
    return stopCheck;
  }
  


  public void setStopCheck(StopCheck stopCheck)
  {
    this.stopCheck = stopCheck;
  }
  


  public List<Account> getAccountsList()
  {
    if (accountsList == null) {
      init();
    }
    return accountsList;
  }
  


  public void setAccountsList(List<Account> accountsList)
  {
    this.accountsList = accountsList;
  }
}

