package com.ffusion.struts.user;

import com.ffusion.beans.SecureUser;
import com.ffusion.beans.user.User;
import com.ffusion.beans.util.CountryDefn;
import com.ffusion.beans.util.LanguageDefn;
import com.ffusion.beans.util.StateProvinceDefn;
import com.ffusion.csil.CSILException;
import com.ffusion.efs.adapters.profile.util.Profile;
import com.opensymphony.xwork2.ModelDriven;
import com.sap.banking.common.beans.ErrorDetail;
import com.sap.banking.common.beans.ValidationResult;
import com.sap.banking.common.bo.interfaces.CommonConfig;
import com.sap.banking.common.bo.interfaces.Util;
import com.sap.banking.common.exception.BLException;
import com.sap.banking.custom.common.utils.webservices.WebserviceUtils;
import com.sap.banking.initializers.interfaces.IInitialize;
import com.sap.banking.user.bo.interfaces.BusinessUserAdmin;
import com.sap.banking.user.bo.interfaces.RetailUser;
import com.sap.banking.user.exception.UserException;
import com.sap.banking.userconfig.bo.interfaces.UserValidationConfig;
import com.sap.banking.userconfig.exception.UserConfigException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.eclipse.gemini.blueprint.io.internal.OsgiUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class ModifyUserProfileCustomAction extends UserBaseResourceAction
implements ModelDriven<User> {

	private static final long serialVersionUID = 1L;
	  boolean isConsumerUser = false;
	  

	  @Autowired
	  RetailUser retailUserBO;
	  
	  @Autowired
	  SecureUser sUser;
	  
	  @Autowired
	  User userBean;
	  
	  @Autowired
	  UserValidationConfig userValidationConfig;
	  
	  @Autowired
	  protected Util utilBO;
	  
	  @Autowired
	  BusinessUserAdmin userAdminBO;
	  
	  @Autowired
	  CommonConfig commonConfigBO;
	  
	  @Autowired
	  IInitialize initialize;
	  
	  private List<LanguageDefn> languagesList;
	  
	  protected List<CountryDefn> countryList = null;
	  
	  protected List<StateProvinceDefn> stateList = null;
	  
	  protected boolean statesExists;
	  
	  protected Map<String, String> userFormFieldNames = new HashMap();
	  
	  private String currentPassword;
	  
	  private String newPassword;
	  private String newConfirmPassword;
	  private String newPasswordReminder;
	  private String confirmNewPasswordReminder;
	  private String newPasswordReminder2;
	  private String confirmNewPasswordReminder2;
	  private Boolean isQuickSearch = Boolean.valueOf(false);
	  

	  public static final String READ_AUDIT_LOGGING_ENABLED = "ReadAuditLoggingEnabled";
	  
	  protected String countryCode;
	  
	  private boolean isBaSBackendEnable = false;
	  
	  public String getNewPassword() {
	    return newPassword;
	  }
	  
	  public void setNewPassword(String newPassword) {
	    this.newPassword = newPassword;
	  }
	  
	  public String getNewConfirmPassword() {
	    return newConfirmPassword;
	  }
	  
	  public void setNewConfirmPassword(String newConfirmPassword) {
	    this.newConfirmPassword = newConfirmPassword;
	  }
	  
	  public String getCurrentPassword() {
	    return currentPassword;
	  }
	  
	  public void setCurrentPassword(String currentPassword) {
	    this.currentPassword = currentPassword;
	  }
	  
	  public String getNewPasswordReminder() {
	    return newPasswordReminder;
	  }
	  
	  public void setNewPasswordReminder(String newPasswordReminder) {
	    this.newPasswordReminder = newPasswordReminder;
	  }
	  
	  public String getConfirmNewPasswordReminder() {
	    return confirmNewPasswordReminder;
	  }
	  
	  public void setConfirmNewPasswordReminder(String confirmNewPasswordReminder) {
	    this.confirmNewPasswordReminder = confirmNewPasswordReminder;
	  }
	  
	  public String getNewPasswordReminder2() {
	    return newPasswordReminder2;
	  }
	  
	  public void setNewPasswordReminder2(String newPasswordReminder2) {
	    this.newPasswordReminder2 = newPasswordReminder2;
	  }
	  
	  public String getConfirmNewPasswordReminder2() {
	    return confirmNewPasswordReminder2;
	  }
	  
	  public void setConfirmNewPasswordReminder2(String confirmNewPasswordReminder2) {
	    this.confirmNewPasswordReminder2 = confirmNewPasswordReminder2;
	  }
	  
	  public String getCountryCode() {
	    return countryCode;
	  }
	  
	  public void setCountryCode(String countryCode) {
	    this.countryCode = countryCode;
	  }
	  
	  public List<LanguageDefn> getLanguagesList() {
	    return languagesList;
	  }
	  
	  public void setLanguagesList(List<LanguageDefn> languagesList) {
	    this.languagesList = languagesList;
	  }
	  
	  public List<CountryDefn> getCountryList() {
	    return countryList;
	  }
	  
	  public void setCountryList(List<CountryDefn> countryList) {
	    this.countryList = countryList;
	  }
	  
	  public List<StateProvinceDefn> getStateList() {
	    return stateList;
	  }
	  
	  public void setStateList(List<StateProvinceDefn> stateList) {
	    this.stateList = stateList;
	  }
	  
	  public boolean getStatesExists() {
	    return statesExists;
	  }
	  
	  public void setStatesExists(boolean statesExists) {
	    this.statesExists = statesExists;
	  }
	  


	  public boolean getIsBaSBackendEnable()
	  {
	    return isBaSBackendEnable;
	  }
	  


	  public void setIsBaSBackendEnable(boolean isBaSBackendEnable)
	  {
	    this.isBaSBackendEnable = isBaSBackendEnable;
	  }
	  
	  public String init()
	  {
	    HttpSession session = getServletRequest().getSession();
	    
	    if (sUser.isConsumerUser()) {
	      isConsumerUser = true;
	      
	      if ((sUser.getProfileID() == sUser.getPrimaryUserID()) || (sUser.getPrimaryUserCustID() == null))
	      {

	        isBaSBackendEnable = commonConfigBO.isBasBackendEnabled();
	      }
	    }
	    	   	    
	    userBean.setId(String.valueOf(sUser.getProfileID()));
	    
	    try
	    {
	      User userFromBackend = retailUserBO.getUserById(sUser, userBean, null);
	      
	      initializeFormData();
	      
	      userBean.set(userFromBackend);
	      
	      String _centerStateEnabled = System.getProperty("centerstate.customization.enabled");
	      if(_centerStateEnabled != null && "true".equals(_centerStateEnabled.toLowerCase())){
		     User _user = WebserviceUtils.processCustomer();
		     if(_user != null){
		        userBean.setFirstName(_user.getFirstName()); 
		        userBean.setLastName(_user.getLastName());
		        userBean.setStreet(_user.getStreet());
		        userBean.setStreet2(_user.getStreet2());
		        userBean.setStreet3(_user.getStreet3());
		        userBean.setStreet(_user.getStreet());
		        userBean.setCustId(_user.getCustId());	 
		     }
	      }
	      doReadAuditLogging();
	    }
	    catch (UserException ue)
	    {
	      logger.error("Error in get user, isValidationException: " + ue
	        .isValidationFailed() + "," + ue.getMessage());
	      
	      if (ue.isValidationFailed()) {
	        return errorValidationResponse(ue);
	      }
	      return errorResponse(ue);
	    }
	    catch (BLException ble)
	    {
	      logger.error(ble.getLocalizedMessage(), ble);
	      return errorResponse(ble);
	    } catch (Throwable e) {
	      logger.error(e.getLocalizedMessage(), e);
	      return errorResponse(e);
	    }
	    

	    if (isQuickSearch.booleanValue()) {
	      return "quickProfileUpdate";
	    }
	    return "input";
	  }
	  

	  protected void doReadAuditLogging()
	  {
	    HttpSession session = getServletRequest().getSession();
	    HashMap csilSettings = initialize.getSettings();
	    HashMap globalCsilSettings = null;
	    
	    if (csilSettings != null) {
	      globalCsilSettings = (HashMap)csilSettings.get("GLOBAL_SETTINGS");
	    }
	    
	    String isReadAuditLoggingEnabled = null;
	    
	    if (globalCsilSettings != null) {
	      isReadAuditLoggingEnabled = (String)globalCsilSettings.get("ReadAuditLoggingEnabled");
	    }
	    
	    if ((isReadAuditLoggingEnabled != null) && (isReadAuditLoggingEnabled.trim().equalsIgnoreCase("true")))
	    {
	      SecureUser sUser = (SecureUser)session.getAttribute("SecureUser");
	      
	      if ((sUser != null) && (sUser.getAgent() != null)) {
	        try {
	          userAdminBO.logViewUserPrefPersonalDetails(sUser, null);
	        } catch (UserException e) {
	          logger.error("ModifyUserProfileAction.doReadAuditLogging", e);
	        }
	      }
	    }
	  }
	  
	  public String getStatesForSelectedCountry()
	  {
	    try
	    {
	      setStateList(getStateForCountry(sUser, getCountryCode()));
	    } catch (UserException e) {
	      logger.error(e.getLocalizedMessage(), e);
	      return errorResponse(e);
	    }
	    
	    if ((getStateList() != null) && (getStateList().size() > 0)) {
	      statesExists = true;
	    }
	    
	    return "statesPage";
	  }
	  
	  protected void initializeFormData() throws BLException {
	    try {
	      Map<String, Object> extra = new HashMap();
	      
	      setLanguagesList(utilBO.getLanguageList(sUser, extra));
	      
	      setCountryList(utilBO.getCountryList(sUser, extra));
	      if ((getUser().getCountry() != null) && 
	        (!getUser().getCountry().equals("")))
	      {
	        setStateList(getStateForCountry(sUser, getUser().getCountry()));
	      }
	      else
	      {
	        setStateList(getStateForCountry(sUser, sUser.getLocale()
	          .getISO3Country()));
	      }
	      

	      if ((getStateList() != null) && (getStateList().size() > 0)) {
	        statesExists = true;
	      }
	    } catch (Throwable e) {
	      logger.error(e.getLocalizedMessage(), e);
	      errorResponse(e);
	    }
	  }
	  

	  protected List<StateProvinceDefn> getStateForCountry(SecureUser sUser, String countryCode)
	    throws UserException
	  {
	    List<StateProvinceDefn> list = new ArrayList();
	    try
	    {
	      Map<String, Object> extra = new HashMap();
	      list = utilBO.getStatesForCountry(sUser, countryCode, extra);
	    }
	    catch (CSILException csile) {
	      throw new UserException(csile);
	    } catch (Throwable e) {
	      logger.error(e.getLocalizedMessage(), e);
	      errorResponse(e);
	    }
	    
	    return list;
	  }
	  

	  public void validateVerify()
	  {
	    if ((getNewPassword() != null) && (!getNewPassword().trim().equals(""))) {
	      userBean.setPassword(getNewPassword());
	    } else {
	      userBean.setPassword(null);
	    }
	    
	    if ((getNewConfirmPassword() != null) && (!getNewConfirmPassword().trim().equals(""))) {
	      userBean.setConfirmPassword(getNewConfirmPassword());
	    } else {
	      userBean.setConfirmPassword(null);
	    }
	    
	    if (!StringUtils.isBlank(getNewPasswordReminder())) {
	      userBean.setPasswordReminder(getNewPasswordReminder());
	    } else {
	      userBean.setPasswordReminder(null);
	    }
	    if (!StringUtils.isBlank(getConfirmNewPasswordReminder())) {
	      userBean.setConfirmPasswordReminder(getConfirmNewPasswordReminder());
	    } else {
	      userBean.setConfirmPasswordReminder(null);
	    }
	    if (!StringUtils.isBlank(getNewPasswordReminder2())) {
	      userBean.setPasswordReminder2(getNewPasswordReminder2());
	    } else {
	      userBean.setPasswordReminder2(null);
	    }
	    if (!StringUtils.isBlank(getConfirmNewPasswordReminder2())) {
	      userBean.setConfirmPasswordReminder2(getConfirmNewPasswordReminder2());
	    } else {
	      userBean.setConfirmPasswordReminder2(null);
	    }
	    
	    validateUser(getSecureUser(), userBean, "modifyUserProfile");
	    

	    if (!getFieldErrors().isEmpty()) {}
	  }
	  



	  protected void validateUser(SecureUser secureUser, User user, String operationName)
	  {
	    ValidationResult validationResult = new ValidationResult();
	    
	    if (user.getUserName() != null) {
	      user.setUserName(user.getUserName().trim());
	    }
	    try
	    {
	      user.setOldPassword(getCurrentPassword());
	      validationResult = userValidationConfig.validateUser(secureUser, user, operationName);
	      
	      ValidationResult zipCodeValidationResult = userAdminBO.validateUserZipCode(sUser, user, null);
	      
	      if ((zipCodeValidationResult != null) && (zipCodeValidationResult.isValidationFailed())) {
	        validationResult.getValidationErrors().putAll(zipCodeValidationResult.getValidationErrors());
	        validationResult.getErrorDetails().putAll(zipCodeValidationResult.getErrorDetails());
	      }
	      
	      ValidationResult phoneValidationResult = userAdminBO.validateUserPhone(sUser, user, null);
	      
	      if ((phoneValidationResult != null) && (phoneValidationResult.isValidationFailed())) {
	        validationResult.getValidationErrors().putAll(phoneValidationResult.getValidationErrors());
	        validationResult.getErrorDetails().putAll(phoneValidationResult.getErrorDetails());
	      }
	      
	      ValidationResult passwordValidationResult = userAdminBO.validateUserPassword(sUser, user, null);
	      
	      if ((passwordValidationResult != null) && (passwordValidationResult.isValidationFailed())) {
	        validationResult.getValidationErrors().putAll(passwordValidationResult.getValidationErrors());
	        validationResult.getErrorDetails().putAll(passwordValidationResult.getErrorDetails());
	      }
	    }
	    catch (UserConfigException e1)
	    {
	      logger.error(e1.getMessage(), e1);
	    } catch (Throwable e) {
	      logger.error(e.getLocalizedMessage(), e);
	      errorResponse(e);
	    }
	    
	    Map<String, String> validationErrors = validationResult.getValidationErrors();
	    

	    if (((getNewPassword() != null) && (!getNewPassword().trim().equals(""))) || (
	      (getNewConfirmPassword() != null) && (!getNewConfirmPassword().trim().equals(""))))
	    {
	      if (validationErrors == null) {
	        validationErrors = new HashMap();
	      }
	      try
	      {
	        if (!sUser.getPassword().equals(Profile.checkEncrypt(getCurrentPassword(), "password", secureUser.getProfileID(), sUser.getUserType()))) {
	          String validationMessage = userValidationConfig.getValidationMessageDescription(sUser, "invalid.current.password");
	          validationErrors.put("currentPassword", validationMessage);
	          validationResult.getErrorDetails().put("currentPassword", new ErrorDetail("invalid.current.password", validationMessage));
	        }
	      } catch (Exception ex) {
	        logger.error(ex.getLocalizedMessage(), ex);
	        String validationMessage = userValidationConfig.getValidationMessageDescription(sUser, "invalid.current.password");
	        validationErrors.put("currentPassword", validationMessage);
	        validationResult.getErrorDetails().put("currentPassword", new ErrorDetail("invalid.current.password", validationMessage));
	      }
	      
	      if (getCurrentPassword().equals(getNewPassword())) {
	        String validationMessage = userValidationConfig.getValidationMessageDescription(sUser, "invalid.new.password");
	        validationErrors.put("password", validationMessage);
	        validationResult.getErrorDetails().put("password", new ErrorDetail("invalid.new.password", validationMessage));
	      }
	    }
	    
	    ValidationResult validationResult1 = null;
	    try
	    {
	      validationResult1 = retailUserBO.validateDuplicateUserId(sUser, user, null);
	    } catch (UserException e) {
	      logger.error(e.getMessage(), e);
	      errorResponse(e);
	    } catch (Throwable e) {
	      logger.error(e.getLocalizedMessage(), e);
	      errorResponse(e);
	    }
	    
	    if ((validationResult1 != null) && (validationResult1.isValidationFailed())) {
	      if (validationResult.getValidationErrors() == null) {
	        validationResult.setValidationErrors(new HashMap());
	      }
	      validationResult.getValidationErrors().putAll(validationResult1.getValidationErrors());
	      validationResult.getErrorDetails().putAll(validationResult1.getErrorDetails());
	    }
	    addErrors(validationResult);
	  }
	  

	  public String verify()
	  {
	    return execute();
	  }
	  
	  public String execute()
	  {
	    HttpSession session = getServletRequest().getSession();
	    try
	    {
	      retailUserBO.modifyUserProfile(sUser, userBean, null);
	      





	      if ((!sUser.getUserName().equals(userBean.getUserName())) || 
	        (!sUser.getPassword().equals(Profile.checkEncrypt(userBean.getPassword(), "password", getSecureUser().getProfileID(), sUser.getUserType()))))
	      {
	        SecureUser changedLoggedInUser = new SecureUser();
	        changedLoggedInUser.set(sUser);
	        
	        if (!sUser.getUserName().equals(userBean.getUserName())) {
	          changedLoggedInUser.setUserName(userBean.getUserName());
	        }
	        
	        if (!sUser.getPassword().equals(Profile.checkEncrypt(userBean.getPassword(), "password", getSecureUser().getProfileID(), sUser.getUserType()))) {
	          changedLoggedInUser.setPassword(userBean.getPassword());
	        }
	        
	        getServletRequest().getSession().setAttribute("SecureUser", changedLoggedInUser);
	      }
	      




	      session.setAttribute("TabToBeSelected", "0");
	      
	      session.setAttribute("ActualStartPage", session
	        .getAttribute("StartPage"));
	      if (sUser.isConsumerUser()) {
	        User user = (User)session.getAttribute("User");
	        if (user != null) {
	          user.setFirstName(userBean.getFirstName());
	          user.setLastName(userBean.getLastName());
	          session.setAttribute("User", user);
	        }
	      }
	    }
	    catch (UserException ue)
	    {
	      logger.error("Error in modify user profile, isValidationException: " + ue
	        .isValidationFailed() + "," + ue.getMessage());
	      
	      if (ue.isValidationFailed()) {
	        return errorValidationResponse(ue);
	      }
	      return errorResponse(ue);
	    }
	    catch (Throwable e) {
	      logger.error(e.getLocalizedMessage(), e);
	      return errorResponse(e);
	    }
	    

	    if (isQuickSearch.booleanValue()) {
	      session.setAttribute("StartPage", "home");
	      return "none";
	    }
	    session.setAttribute("StartPage", "home_prefs");
	    return "success";
	  }
	  

	  public User getModel()
	  {
	    return userBean;
	  }
	  
	  public User getUser() {
	    return userBean;
	  }
	  
	  public void setUser(User user) {
	    userBean = user;
	  }
	  
	  public boolean getIsConsumerUser() {
	    return isConsumerUser;
	  }
	  
	  public void setIsConsumerUser(boolean isConsumerUser) {
	    this.isConsumerUser = isConsumerUser;
	  }
	  


	  public Boolean getIsQuickSearch()
	  {
	    return isQuickSearch;
	  }
	  


	  public void setIsQuickSearch(Boolean isQuickSearch)
	  {
	    this.isQuickSearch = isQuickSearch;
	  }
}
